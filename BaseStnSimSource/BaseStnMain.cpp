#include <vcl.h>
#pragma hdrstop

#include "BaseStnMain.h"
#include "ApplUtils.h"
#include "Comm32.cpp"
#include "CUDPPort.cpp"

#pragma package(smart_init)
#pragma resource "*.dfm"


TMainForm *MainForm;

// All fields in a status packet are shown in the value list editor
// The following enums and key values are used to populate that control.
typedef enum {
    SPF_UPDATE_RATE,
    SPF_LAST_RESET_TYPE,
    SPF_RADIO_1_CHAN,
    SPF_RADIO_1_RSSI,
    SPF_RADIO_1_BIT_RATE,
    SPF_RADIO_2_CHAN,
    SPF_RADIO_2_RSSI,
    SPF_RADIO_2_BIT_RATE,
    SPF_RADIO_3_CHAN,
    SPF_RADIO_3_RSSI,
    SPF_RADIO_3_BIT_RATE,
    SPF_DIG_OUT_SETTING,
    SPF_FV_VER_1,
    SPF_FV_VER_2,
    SPF_FV_VER_3,
    SPF_FV_VER_4,
    SPF_USB_CTRL_SIGS,
    NBR_STATUS_PKT_FIELDS
} STATUS_PKT_FIELD;

static const UnicodeString PktFieldNames[NBR_STATUS_PKT_FIELDS] = {
    "Update Rate",
    "Last Reset Type",
    "Radio 1 Channel",
    "Radio 1 RSSI",
    "Radio 1 Bit Rate",
    "Radio 2 Channel",
    "Radio 2 RSSI",
    "Radio 2 Bit Rate",
    "Radio 3 Channel",
    "Radio 3 RSSI",
    "Radio 3 Bit Rate",
    "Digital Out Bits",
    "Firware Ver 1",
    "Firware Ver 2",
    "Firware Ver 3",
    "Firware Ver 4",
    "USB Ctrl Signals"
};


static const UnicodeString PktFieldDefaults[NBR_STATUS_PKT_FIELDS] = {
    "250",
    "0",
    "11",
    "0",
    "0",
    "25",
    "0",
    "0",
    "0",
    "0",
    "0",
    "0x00",
    "0x00",
    "0x00",
    "0x00",
    "0x00",
    "0x00",
};


static bool PacketValueToInt( UnicodeString valueText, int& value )
{
    // Convert to int safely. If prefixed with "0x", convert as hex.
    UnicodeString trimmedText = valueText.Trim();

    // Init default in case of failed conversion
    value = 0;

    try
    {
        if( trimmedText.Pos( "0x" ) == 1 )
        {
            UnicodeString valuePortion = trimmedText.Delete( 1, 2 );

            value = UnicodeString( "$" + valuePortion ).ToInt();
        }
        else
        {
            value = trimmedText.ToInt();
        }
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


static int PacketValueToInt( UnicodeString valueText )
{
    // A second version of the function that can be used inline
    int returnVal;

    if( PacketValueToInt( valueText, returnVal ) )
        return returnVal;
    else
        return 0;
}


//
// Class Functions
//

__fastcall TMainForm::TMainForm(TComponent* Owner) : TForm(Owner)
{
    // Initialize the status value editor
    FieldSelCombo->Items->Clear();

    for( int iItem = 0; iItem < NBR_STATUS_PKT_FIELDS; iItem++ )
    {
        CurrValuesEditor->InsertRow( PktFieldNames[iItem], PktFieldDefaults[iItem], true );
        FieldSelCombo->Items->Add( PktFieldNames[iItem] );
    }

    FieldSelCombo->ItemIndex = 0;

    // We need Winsock for this program
    if( !InitWinsock() )
        AddMemoEntry( "Warning: InitWinsock() failed" );
}


void __fastcall TMainForm::FormDestroy(TObject *Sender)
{
    ShutdownWinsock();
}


void __fastcall TMainForm::ConnectBtnClick(TObject *Sender)
{
    DisconnectBtnClick( DisconnectBtn );

    // Create the comm object
    if( SerialModeRB->Checked )
    {
        CCommPort::CONNECT_PARAMS portParams;

        portParams.iPortNumber   = CommPortEdit->Text.ToInt();
        portParams.dwLineBitRate = BitRateEdit->Text.ToInt();

        m_commObj = new CCommPort();

        if( m_commObj->Connect( &portParams ) == CCommObj::ERR_NONE )
        {
            AddMemoEntry( "COM" + CommPortEdit->Text + " opened" );
        }
    }
    else
    {
        // In UDP mode, we send packets to a destination address
        CUDPPort::CONNECT_PARAMS portParams;

        portParams.destAddr = HostIPEdit->Text;
        portParams.destPort = UDPPortEdit->Text.ToInt();
        portParams.portNbr  = 0;
        portParams.acceptBroadcasts = true;

        m_commObj = new CUDPPort();

        if( m_commObj->Connect( &portParams ) == CCommObj::ERR_NONE )
        {
            AddMemoEntry( "UDP" + HostIPEdit->Text + ":" + UDPPortEdit->Text + " opened" );
        }
    }

    if( m_commObj == NULL )
        return;

    if( m_commObj->isConnected )
    {
        ConnectBtn->Enabled    = false;
        DisconnectBtn->Enabled = true;

        // Enable timer
        SimTimer->Interval = CurrValuesEditor->Values[ PktFieldNames[SPF_UPDATE_RATE] ].ToIntDef( 10 );
        SimTimer->Enabled  = true;

        // Clear any data there may have been in the buffer
        m_buffCount     = 0;
        m_lastCmdSeqNbr = 0;
    }
    else
    {
        AddMemoEntry( "COM" + CommPortEdit->Text + " failed, code " + IntToStr( m_commObj->connectResult ) );

        delete m_commObj;
        m_commObj = NULL;
    }
}


void __fastcall TMainForm::DisconnectBtnClick(TObject *Sender)
{
    if( m_commObj != NULL )
    {
        delete m_commObj;
        m_commObj = NULL;
    }

    DisconnectBtn->Enabled = false;
    ConnectBtn->Enabled    = true;

    SimTimer->Enabled = false;
}


void __fastcall TMainForm::PauseBtnClick(TObject *Sender)
{
    if( PauseBtn->Tag == 1 )
    {
        PauseBtn->Tag     = 0;
        PauseBtn->Caption = "Pause";
    }
    else
    {
        PauseBtn->Tag     = 1;
        PauseBtn->Caption = "Resume";
    }
}


void __fastcall TMainForm::ClearMemoBtnClick(TObject *Sender)
{
    LogMemo->Lines->Clear();
}


void TMainForm::AddMemoEntry( UnicodeString newEntry )
{
    if( PauseBtn->Tag == 0 )
    {
        while( LogMemo->Lines->Count > 500 )
            LogMemo->Lines->Delete( 0 );

        LogMemo->Lines->Add( Time().TimeString() + " " + newEntry );
    }
}


void __fastcall TMainForm::SimTimerTimer(TObject *Sender)
{
    // On each tick, check for an inbound command and also output the current status.
    // Check for a command first, as this may change the status fields.
    if( m_commObj == NULL )
        return;

    m_buffCount += m_commObj->CommRecv( &(m_commBuff[m_buffCount]), COMM_BUFF_LEN - m_buffCount );

    WTTTS_PKT_HDR       pktHdr;
    BASE_STN_DATA_UNION pktData;

    if( HaveBaseStnCmdPkt( m_commBuff, m_buffCount, pktHdr, pktData ) )
    {
        // Log the packet
        AddMemoEntry( "Base Stn cmd received: " + IntToHex( pktHdr.pktType, 2 ) );

        // Remember this packet's sequence number
        m_lastCmdSeqNbr = pktHdr.seqNbr;

        // Process the command
        switch( pktHdr.pktType )
        {
            case BASE_STN_CMD_GET_STATUS:
                // Can ignore this - we send a packet anyways in this function
                break;

            case BASE_STN_CMD_SET_CHAN_NBR:
                switch( pktData.setChanPkt.radioNbr )
                {
                    case 0: CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_1_CHAN] ] = IntToStr( pktData.setChanPkt.newChanNbr );  break;
                    case 1: CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_2_CHAN] ] = IntToStr( pktData.setChanPkt.newChanNbr );  break;
                    case 2: CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_3_CHAN] ] = IntToStr( pktData.setChanPkt.newChanNbr );  break;
                }
                break;

            case BASE_STN_CMD_SET_CHAN_PARAM:
                // Not currently implemented
                break;

            case BASE_STN_CMD_SET_UPDATE_RATE:
                CurrValuesEditor->Values[ PktFieldNames[SPF_UPDATE_RATE] ] = IntToStr( pktData.setRatePkt.newUpdateRate );
                break;

            case BASE_STN_CMD_SET_PWR_STATE:
                // Not currently implemented
                break;

            case BASE_STN_CMD_SET_OUTPUTS:
                CurrValuesEditor->Values[ PktFieldNames[SPF_DIG_OUT_SETTING] ] = "0x" + IntToHex( pktData.setOutputsPkt.outputFlags, 2 );
                break;

            case BASE_STN_CMD_RESET_FIRMWARE:
                // Not currently implemented
                break;

            default:
                AddMemoEntry( "  Invalid cmd type: " + IntToHex( pktHdr.pktType, 2 ) );
                break;
        }
    }

    // If the update rate has changed, change our timer interval
    DWORD newInterval = CurrValuesEditor->Values[ PktFieldNames[SPF_UPDATE_RATE] ].ToIntDef( 10 );

    if( SimTimer->Interval != newInterval )
    {
        SimTimer->Interval = newInterval;

        // Repopulate editor, in case previous value was bogus
        CurrValuesEditor->Values[ PktFieldNames[SPF_UPDATE_RATE] ] = IntToStr( (int)newInterval );
    }

    // On every tick of this timer, we send a packet
    BASE_STATUS_STATUS_PKT statusPkt;
    memset( &statusPkt, 0, sizeof( BASE_STATUS_STATUS_PKT ) );

    statusPkt.radioInfo[0].chanNbr = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_1_CHAN] ] );
    statusPkt.radioInfo[0].rssi    = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_1_RSSI] ] );
    statusPkt.radioInfo[0].bitRate = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_1_BIT_RATE] ] );

    statusPkt.radioInfo[1].chanNbr = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_2_CHAN] ] );
    statusPkt.radioInfo[1].rssi    = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_2_RSSI] ] );
    statusPkt.radioInfo[1].bitRate = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_2_BIT_RATE] ] );

    statusPkt.radioInfo[2].chanNbr = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_3_CHAN] ] );
    statusPkt.radioInfo[2].rssi    = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_3_RSSI] ] );
    statusPkt.radioInfo[2].bitRate = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_RADIO_3_BIT_RATE] ] );

    statusPkt.lastResetType        = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_LAST_RESET_TYPE] ] );
    statusPkt.statusUpdateRate     = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_UPDATE_RATE] ] );
    statusPkt.outputStatus         = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_DIG_OUT_SETTING] ] );
    statusPkt.usbCtrlSignals       = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_USB_CTRL_SIGS] ] );

    statusPkt.fwVer[0]             = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_FV_VER_1] ] );
    statusPkt.fwVer[1]             = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_FV_VER_2] ] );
    statusPkt.fwVer[2]             = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_FV_VER_3] ] );
    statusPkt.fwVer[3]             = PacketValueToInt( CurrValuesEditor->Values[ PktFieldNames[SPF_FV_VER_4] ] );

    // Build the response packet now
    BYTE txBuffer[200];

    // Create header first
    WTTTS_PKT_HDR* pPktHdr = (WTTTS_PKT_HDR*)txBuffer;

    memset( pPktHdr, 0, sizeof( WTTTS_PKT_HDR ) );

    pPktHdr->pktHdr    = BASE_STN_HDR_RX;
    pPktHdr->pktType   = BASE_STN_RESP_STATUS;
    pPktHdr->seqNbr    = m_lastCmdSeqNbr;
    pPktHdr->timeStamp = 0;
    pPktHdr->dataLen   = sizeof( BASE_STATUS_STATUS_PKT );

    // Copy on status pkt payload
    BYTE* pData = &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

    memcpy( pData, &statusPkt, sizeof( BASE_STATUS_STATUS_PKT ) );

    // Calc CRC
    int packetHdrAndDataLen = sizeof( WTTTS_PKT_HDR ) + sizeof( BASE_STATUS_STATUS_PKT );

    txBuffer[ packetHdrAndDataLen ] = CalcWTTTSChecksum( txBuffer, packetHdrAndDataLen );

    // Now send the packet
    DWORD bytesSent = m_commObj->CommSend( txBuffer, packetHdrAndDataLen + 1 );

    if( bytesSent == packetHdrAndDataLen + 1 )
        AddMemoEntry( "Status pkt sent OK" );
    else
        AddMemoEntry( "Status pkt send failed" );
}


void __fastcall TMainForm::ChangeBtnClick(TObject *Sender)
{
    // The combo item index corresponds to a STATUS_PKT_FIELD enum.
    if( FieldSelCombo->ItemIndex < 0 )
    {
        Beep();
        return;
    }

    STATUS_PKT_FIELD fieldItem = (STATUS_PKT_FIELD) FieldSelCombo->ItemIndex;

    int testValue;

    if( !PacketValueToInt( NewValEdit->Text, testValue ) )
    {
        MessageDlg( "You have entered an invalid value.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    // On success, populate the edit with the trimmed text the user entered
    CurrValuesEditor->Values[ PktFieldNames[fieldItem] ] = NewValEdit->Text.Trim();
}


