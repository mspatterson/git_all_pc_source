object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Base Station Simulator'
  ClientHeight = 397
  ClientWidth = 711
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  DesignSize = (
    711
    397)
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 8
    Top = 166
    Width = 36
    Height = 13
    Caption = 'Activity'
  end
  object ConnectionGB: TGroupBox
    Left = 8
    Top = 8
    Width = 353
    Height = 151
    Caption = ' Data Port '
    TabOrder = 0
    object Label1: TLabel
      Left = 88
      Top = 17
      Width = 52
      Height = 13
      Caption = 'Comm Port'
    end
    object Label2: TLabel
      Left = 221
      Top = 17
      Width = 38
      Height = 13
      Caption = 'Bit Rate'
    end
    object Label3: TLabel
      Left = 88
      Top = 65
      Width = 67
      Height = 13
      Caption = 'Destination IP'
    end
    object Label4: TLabel
      Left = 221
      Top = 65
      Width = 43
      Height = 13
      Caption = 'UDP Port'
    end
    object CommPortEdit: TEdit
      Left = 88
      Top = 36
      Width = 121
      Height = 21
      NumbersOnly = True
      TabOrder = 0
      Text = '1'
    end
    object BitRateEdit: TEdit
      Left = 221
      Top = 38
      Width = 121
      Height = 21
      NumbersOnly = True
      TabOrder = 1
      Text = '57600'
    end
    object HostIPEdit: TEdit
      Left = 88
      Top = 84
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '127.0.0.1'
    end
    object UDPPortEdit: TEdit
      Left = 221
      Top = 84
      Width = 121
      Height = 21
      NumbersOnly = True
      TabOrder = 3
      Text = '62220'
    end
    object SerialModeRB: TRadioButton
      Left = 17
      Top = 37
      Width = 60
      Height = 19
      Caption = 'Serial'
      TabOrder = 4
    end
    object UDPModeRB: TRadioButton
      Left = 17
      Top = 85
      Width = 60
      Height = 19
      Caption = 'UDP'
      Checked = True
      TabOrder = 5
      TabStop = True
    end
    object ConnectBtn: TButton
      Left = 184
      Top = 115
      Width = 75
      Height = 25
      Caption = 'Connect'
      TabOrder = 6
      OnClick = ConnectBtnClick
    end
    object DisconnectBtn: TButton
      Left = 265
      Top = 115
      Width = 75
      Height = 25
      Caption = 'Disconnect'
      Enabled = False
      TabOrder = 7
      OnClick = DisconnectBtnClick
    end
  end
  object LogMemo: TMemo
    Left = 8
    Top = 185
    Width = 353
    Height = 175
    Anchors = [akLeft, akTop, akBottom]
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object PauseBtn: TButton
    Left = 206
    Top = 366
    Width = 74
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Pause'
    TabOrder = 2
    OnClick = PauseBtnClick
  end
  object ClearMemoBtn: TButton
    Left = 287
    Top = 366
    Width = 74
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Clear'
    TabOrder = 3
    OnClick = ClearMemoBtnClick
  end
  object GroupBox1: TGroupBox
    Left = 376
    Top = 8
    Width = 327
    Height = 381
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Status Info '
    TabOrder = 4
    DesignSize = (
      327
      381)
    object Label6: TLabel
      Left = 16
      Top = 309
      Width = 54
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'Select Field'
      ExplicitTop = 335
    end
    object Label7: TLabel
      Left = 16
      Top = 339
      Width = 50
      Height = 13
      Anchors = [akLeft, akBottom]
      Caption = 'New Value'
      ExplicitTop = 365
    end
    object CurrValuesEditor: TValueListEditor
      Left = 16
      Top = 22
      Width = 298
      Height = 274
      Anchors = [akLeft, akTop, akRight, akBottom]
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking, goFixedColClick]
      TabOrder = 0
      TitleCaptions.Strings = (
        'Field'
        'Value')
      ColWidths = (
        150
        142)
    end
    object FieldSelCombo: TComboBox
      Left = 80
      Top = 306
      Width = 209
      Height = 21
      Style = csDropDownList
      Anchors = [akLeft, akBottom]
      TabOrder = 1
    end
    object NewValEdit: TEdit
      Left = 80
      Top = 336
      Width = 121
      Height = 21
      Anchors = [akLeft, akBottom]
      TabOrder = 2
    end
    object ChangeBtn: TButton
      Left = 214
      Top = 334
      Width = 75
      Height = 25
      Anchors = [akLeft, akBottom]
      Caption = 'Change'
      TabOrder = 3
      OnClick = ChangeBtnClick
    end
  end
  object SimTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = SimTimerTimer
    Left = 96
    Top = 120
  end
end
