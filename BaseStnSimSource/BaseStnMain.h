//---------------------------------------------------------------------------
#ifndef BaseStnMainH
#define BaseStnMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include "CommObj.h"
#include "WTTTSProtocolUtils.h"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
    TGroupBox *ConnectionGB;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *Label4;
    TEdit *CommPortEdit;
    TEdit *BitRateEdit;
    TEdit *HostIPEdit;
    TEdit *UDPPortEdit;
    TRadioButton *SerialModeRB;
    TRadioButton *UDPModeRB;
    TButton *ConnectBtn;
    TButton *DisconnectBtn;
    TTimer *SimTimer;
    TLabel *Label5;
    TMemo *LogMemo;
    TButton *PauseBtn;
    TButton *ClearMemoBtn;
    TGroupBox *GroupBox1;
    TValueListEditor *CurrValuesEditor;
    TLabel *Label6;
    TComboBox *FieldSelCombo;
    TLabel *Label7;
    TEdit *NewValEdit;
    TButton *ChangeBtn;
    void __fastcall ConnectBtnClick(TObject *Sender);
    void __fastcall DisconnectBtnClick(TObject *Sender);
    void __fastcall PauseBtnClick(TObject *Sender);
    void __fastcall ClearMemoBtnClick(TObject *Sender);
    void __fastcall SimTimerTimer(TObject *Sender);
    void __fastcall ChangeBtnClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);

private:

    CCommObj*     m_commObj;

    #define COMM_BUFF_LEN  2048
    BYTE          m_commBuff[COMM_BUFF_LEN];
    DWORD         m_buffCount;

    BYTE          m_lastCmdSeqNbr;

    void AddMemoEntry( UnicodeString newEntry );

public:
    __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
