#include <vcl.h>
#pragma hdrstop

#include <shlobj.h>
#include <DateUtils.hpp>
#include <Registry.hpp>

#include "ApplUtils.h"


#pragma package(smart_init)


String ConvertAppVerIntToStr( int appVer )
{
    int buildVer   = ( appVer        ) & 0x000000FF;
    int releaseVer = ( appVer  >>  8 ) & 0x000000FF;
    int minorVer   = ( appVer  >> 16 ) & 0x000000FF;
    int majorVer   = ( appVer  >> 24 ) & 0x000000FF;

    return( IntToStr( majorVer ) + "." + IntToStr( minorVer ) + "." + IntToStr( releaseVer ) + "." + IntToStr( buildVer ) );
}


int ConvertAppVerStrToInt( AnsiString appVer )
{
    int buildVer;
    int releaseVer;
    int minorVer;
    int majorVer;

    int delimiterChar;

    delimiterChar = appVer.Pos( "." );
    majorVer = StrToInt( appVer.SubString( 1, delimiterChar - 1 ) );
    appVer   = appVer.Delete( 1, delimiterChar );

    delimiterChar = appVer.Pos( "." );
    minorVer = StrToInt( appVer.SubString( 1, delimiterChar - 1 ) );
    appVer   = appVer.Delete( 1, delimiterChar );

    delimiterChar = appVer.Pos( "." );
    releaseVer = StrToInt( appVer.SubString( 1, delimiterChar - 1 ) );
    appVer     = appVer.Delete( 1, delimiterChar );

    buildVer = StrToInt( appVer );

    return( ( majorVer << 24 ) | ( minorVer << 16 ) | ( releaseVer << 8 ) | ( buildVer ) );
}


bool GetApplVer( int& majorVer, int& minorVer, int& releaseVer, int& buildVer )
{
    BYTE * pFileInfo;
    DWORD  dwHandle;
    DWORD  dwVerInfoLength;
    UINT   fixedFileInfoLength;
    VS_FIXEDFILEINFO* pFixedFileInfo;

    dwVerInfoLength = GetFileVersionInfoSize( Application->ExeName.w_str(), &dwHandle );

    DWORD error = GetLastError();

    if( dwVerInfoLength > 0 )
    {
        pFileInfo = (BYTE *) new BYTE[dwVerInfoLength*2];  // Allocate some extra

        if( pFileInfo != NULL )
        {
            if( GetFileVersionInfo( Application->ExeName.w_str(), dwHandle, dwVerInfoLength, pFileInfo ) )
            {
                if( VerQueryValue( pFileInfo, String( "\\" ).w_str() /*get root info*/ , (LPVOID*)&pFixedFileInfo, &fixedFileInfoLength ) )
                {
                    majorVer   = pFixedFileInfo->dwFileVersionMS >> 16;
                    minorVer   = pFixedFileInfo->dwFileVersionMS &  0xFFFF ;
                    releaseVer = pFixedFileInfo->dwFileVersionLS >> 16;
                    buildVer   = pFixedFileInfo->dwFileVersionLS & 0xFFFF ;
                }
            }

            delete [] pFileInfo;

            return true;
        }
    }

    return false;
}


String GetApplVerAsString( void )
{
    int iMajorVer;
    int iMinorVer;
    int iReleaseVer;
    int iBuildVer;

    if( !GetApplVer( iMajorVer, iMinorVer, iReleaseVer, iBuildVer ) )
    {
        iMajorVer   = 0;
        iMinorVer   = 0;
        iReleaseVer = 0;
        iBuildVer   = 0;
    }

    String sResult;
    sResult.printf( L"%d.%d.%d.%d", iMajorVer, iMinorVer, iReleaseVer, iBuildVer );

    return sResult;
}


DWORD GetApplVer( void )
{
    int majorVer;
    int minorVer;
    int releaseVer;
    int buildVer;

    if( !GetApplVer( majorVer, minorVer, releaseVer, buildVer ) )
    {
        majorVer   = 0;
        minorVer   = 0;
        releaseVer = 0;
        buildVer   = 0;
    }

    return( ( majorVer << 24 ) | ( minorVer << 16 ) | ( releaseVer << 8 ) | ( buildVer ) );
}


int GetApplMajorVer( void )
{
    return( ( GetApplVer() >> 24 ) & 0x000000FF );
}


String GetExeDir( void )
{
    return( IncludeTrailingPathDelimiter( ExtractFilePath( ParamStr( 0 ) ) ) );
}


bool BrowseForFolder( String dialogTitle, String& folderName )
{
    // Note: CoInitialize() must have been called before calling this function
    wchar_t szDispName[MAX_PATH];

    BROWSEINFO browseInfo = { 0 };

    browseInfo.hwndOwner      = NULL;
    browseInfo.pidlRoot       = NULL;
    browseInfo.pszDisplayName = szDispName;
    browseInfo.lpszTitle      = dialogTitle.w_str();
    browseInfo.ulFlags        = BIF_NEWDIALOGSTYLE;
    browseInfo.lpfn           = NULL;
    browseInfo.lParam         = 0;
    browseInfo.iImage         = 0;

    PIDLIST_ABSOLUTE pIDLList = SHBrowseForFolder( &browseInfo );

    if( pIDLList == NULL )
    {
        // User cancelled out of dialog
        return false;
    }

    TCHAR path[MAX_PATH];

    if( !SHGetPathFromIDList( pIDLList, path ) )
    {
        // Something went wrong
        CoTaskMemFree( pIDLList );
        return false;
    }

    // Need to free the returned item, even though we don't use it
    CoTaskMemFree( pIDLList );

    folderName = path;

    return true;
}


String GetEnvironmentVariable( const String& sEnvVarName )
{
    // Returns the string associated with the environment variable. Returns
    // an empty string if the env var does not exist or an error occurs
    String sResult;

    // Do a test call to see how many bytes we need
    wchar_t wchTest[10];

    DWORD dwBytesNeeded = GetEnvironmentVariable( sEnvVarName.w_str(), wchTest, 10 );

    // Check for an error return
    if( dwBytesNeeded == 0 )
        return sResult;

    // Allocate the actual size needed
    wchar_t* pwchBuffer = new wchar_t[dwBytesNeeded+1];

    SecureZeroMemory( pwchBuffer, ( dwBytesNeeded+1 ) * sizeof( wchar_t ) );

    DWORD dwBytesFilled = GetEnvironmentVariable( sEnvVarName.w_str(), pwchBuffer, dwBytesNeeded );

    if( dwBytesFilled != 0 )
        sResult = pwchBuffer;

    delete [] pwchBuffer;

    return sResult;
}


String LocalTimeString( time_t utcTime )
{
    return UnixToDateTime( utcTime + TTimeZone::Local->UtcOffset.TotalSeconds ).TimeString();
}


String LocalTimeString( time_t utcTime, time_t utcOffset )
{
    return UnixToDateTime( utcTime + utcOffset ).TimeString();
}


String LocalDateTimeString( time_t utcTime )
{
    // Creates a string in local time of the passed values
    return UnixToDateTime( utcTime + TTimeZone::Local->UtcOffset.TotalSeconds ).DateTimeString();
}


String LocalDateTimeString( time_t utcTime, time_t utcOffset )
{
    // Creates a string in local time of the passed values
    return UnixToDateTime( utcTime + utcOffset ).DateTimeString();
}


// Time Zones key names
static const String HKLM_Software_MS_WinNT_CurrVer_TZ( "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Time Zones" );
static const String TZKey_Display( "Display" );
static const String TZKey_DSTText( "Dlt" );
static const String TZKey_StdText( "Std" );
static const String TZKey_TZI    ( "TZI" );

bool GetTimeZoneInfo( String& sTZDisplayName, TIME_ZONE_INFORMATION& timeZoneInformation, bool& isDST )
{
    // Fills the passed TIME_ZONE_INFORMATION struct with the info for the
    // time zone name string passed. This string must be one of the strings
    // known to Windows stored as the Display entry in one of the keys under the
    // HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones registry hive.
    // If sTZDisplayName is an empty string, the TZI for the current timezone is returned
    // and sTZDisplayName is populated with display string for the timezone.
    memset( &timeZoneInformation, 0, sizeof( TIME_ZONE_INFORMATION ) );

    isDST = false;

    bool bResult = false;

    if( sTZDisplayName.Length() == 0 )
    {
        int iResult = GetTimeZoneInformation( &timeZoneInformation );

        switch( iResult )
        {
            case TIME_ZONE_ID_DAYLIGHT:  isDST = true;    bResult = true;   break;
            case TIME_ZONE_ID_STANDARD:  isDST = false;   bResult = true;   break;
            case TIME_ZONE_ID_UNKNOWN:   isDST = false;   bResult = true;   break;
            default:                     isDST = false;   bResult = false;  break;
        }

        // Need dynamic time zone info now to get the display string for this Tz
        DYNAMIC_TIME_ZONE_INFORMATION dynTZI;
        memset( &dynTZI, 0, sizeof( DYNAMIC_TIME_ZONE_INFORMATION ) );

        if( GetDynamicTimeZoneInformation( &dynTZI ) != TIME_ZONE_ID_INVALID )
        {
            TRegistry *pRegistry = new TRegistry( KEY_READ );

            String sTZKey = HKLM_Software_MS_WinNT_CurrVer_TZ + "\\" + String( dynTZI.TimeZoneKeyName );

            try
            {
                // Set our root key
                pRegistry->RootKey = HKEY_LOCAL_MACHINE;

                // Open the base key
                pRegistry->OpenKeyReadOnly( sTZKey );

                sTZDisplayName = pRegistry->ReadString( TZKey_Display );

                pRegistry->CloseKey();
            }
            catch( ... )
            {
            }

            // Delete created objects.
            delete pRegistry;
        }
    }
    else
    {
        // Enumerate all keys under the Time Zones folder
        TStringList* pTZKeys = new TStringList();

        TRegistry *pRegistry = new TRegistry( KEY_READ );

        try
        {
            // Set our root key
            pRegistry->RootKey = HKEY_LOCAL_MACHINE;

            // Open the base key
            pRegistry->OpenKeyReadOnly( HKLM_Software_MS_WinNT_CurrVer_TZ );
            pRegistry->GetKeyNames( pTZKeys );
            pRegistry->CloseKey();

            // Now iterate through all keys
            for( int iKey = 0; iKey < pTZKeys->Count; iKey++ )
            {
                pRegistry->OpenKeyReadOnly( HKLM_Software_MS_WinNT_CurrVer_TZ + "\\" + pTZKeys->Strings[iKey] );

                String sValue;

                try {

                    sValue = pRegistry->ReadString( TZKey_Display );
                }
                catch( ... )
                {
                }

                if( sValue.CompareIC( sTZDisplayName ) == 0 )
                {
                    pRegistry->ReadBinaryData( TZKey_TZI, &timeZoneInformation, sizeof( TIME_ZONE_INFORMATION ) );
                    bResult = true;
                }

                pRegistry->CloseKey();

                if( bResult )
                    break;
            }

            pRegistry->CloseKey();
        }
        catch( ... )
        {
            bResult = false;
        }

        // Delete created objects. Note that the registry object destructor
        // closes any open keys for us.
        delete pTZKeys;
        delete pRegistry;
    }

    return bResult;
}


bool GetTimeZoneNames( TStringList* pTimeZoneList )
{
    // Fills the passed string list with all time zones known to Windows stored
    // as the Display entry under the registry hive:
    // HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones
    if( pTimeZoneList == NULL )
        return false;

    pTimeZoneList->Clear();

    bool bResult = true;

    // Enumerate all keys under the Time Zones folder
    TStringList* pTZKeys = new TStringList();

	TRegistry *pRegistry = new TRegistry( KEY_READ );

	try
    {
        // Set our root key
		pRegistry->RootKey = HKEY_LOCAL_MACHINE;

		// Open the base key
		pRegistry->OpenKeyReadOnly( HKLM_Software_MS_WinNT_CurrVer_TZ );
        pRegistry->GetKeyNames( pTZKeys );
        pRegistry->CloseKey();

        // Now iterate through all keys
        for( int iKey = 0; iKey < pTZKeys->Count; iKey++ )
        {
            pRegistry->OpenKeyReadOnly( HKLM_Software_MS_WinNT_CurrVer_TZ + "\\" + pTZKeys->Strings[iKey] );

            String sValue;

            try {

                sValue = pRegistry->ReadString( TZKey_Display );

                if( sValue.Length() == 0 )
                    sValue = pTZKeys->Strings[iKey] + ": no entry for Key " + TZKey_Display;
            }
            catch( ... )
            {
                sValue = pTZKeys->Strings[iKey] + ": bad type for Key " + TZKey_Display;
            }

            pRegistry->CloseKey();

            pTimeZoneList->Add( sValue );
        }

        pRegistry->CloseKey();
	}
	catch( ... )
    {
        bResult = false;
	}

    // Delete created objects. Note that the registry object destructor
    // closes any open keys for us.
    delete pTZKeys;
	delete pRegistry;

    return bResult;
}


/*******************************************************************************
Method:         TimeSince

Description:

Modifications:  18/07/14 AC  creation
*******************************************************************************/
DWORD TimeSince( DWORD startTime )
{
    // Return number of msecs that have elapsed since the passed time.
    // Watch for the case where the current time is less than the
    // start time (and therefore has wrapped).
    DWORD currTime = GetTickCount();

    if( currTime >= startTime )
        return currTime - startTime;

    // Time has wrapped in this case
    return 0xFFFFFFFF - startTime + currTime;
}


/*******************************************************************************
Method:         HaveTimeout

Description:    Check time out

Modifications:  18/07/14 AC  creation
*******************************************************************************/
bool HaveTimeout( DWORD startTime, DWORD timeoutValue )
{
    // Return true if a timeout has occurred. Because we have to support
    // Win XP, we can only use GetTickCount(), and not GetTickCount64().

    // Elminate simple cases first
    if( timeoutValue == 0 )
        return true;

    DWORD elapsedTime = TimeSince( startTime );

    return( elapsedTime > timeoutValue );
}


/*******************************************************************************
Method:         ConvertDelimitedString

Description:    Seperate the fields in the input string based on the given delimiter
                string. Results are passed back in the outputStrings arg. Also returns
                the number of items in the list through the default int return.

Modifications:  20/01/17 AM  Ported from other code
*******************************************************************************/
int ConvertDelimitedString( const String inputString, TStringList* outputStrings, const String delimiter )
{
    // If we have no output list, return 0
    if( outputStrings == NULL )
        return 0;

    // Get the delimiter length
    int delimLen = delimiter.Length();

    // If we have no delimiter to use, return 0
    if( delimLen == 0 )
        return 0;

    // Clear the return list
    outputStrings->Clear();

    AnsiString charsLeft = Trim( inputString );

    // Loop while we still have characters to process
    while( charsLeft.Length() > 0 )
    {
        // Get the position of the next delimiter
        int delimPos = charsLeft.Pos( delimiter );

        // If there are no more delimiters, we're done
        if( delimPos <= 0 )
        {
            outputStrings->Add( charsLeft );

            break;
        }

        // Add the last value to the output strings
        outputStrings->Add( Trim( charsLeft.SubString( 1, delimPos - 1 ) ) );

        // Remove the value that was just added from the base string
        charsLeft.Delete( 1, delimPos + delimLen - 1 );
    }

    // Finally, return the number of items in the list
    return outputStrings->Count;
}


void ShowModelessForm( TForm* aForm )
{
    // Show the passed form modelessly(on top of main form). Be sure
    // to restore the window to its normal size, regardless of where
    // it may have last been set.
    if( aForm == NULL )
        return;

    if( aForm->Visible )
    {
        // Form is currently showing somewhere. If it has been
        // minimized, restore it to is normal size. Otherwise,
        // make the form fully visible
        if( aForm->WindowState == wsMinimized )
            aForm->WindowState = wsNormal;
        else
            aForm->BringToFront();
    }
    else
    {
        // Form not showing, so just show it. If the state is
        // minimized, restore the state first.
        aForm->WindowState = wsNormal;
        aForm->Show();
    }
}

