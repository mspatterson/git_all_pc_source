#ifndef ApplUtilsH
    #define ApplUtilsH

    #include <time.h>

    String ConvertAppVerIntToStr( int appVer );
    int    ConvertAppVerStrToInt( AnsiString appVer );

    bool   GetApplVer( int& majorVer, int& minorVer, int& releaseVer, int& buildVer );
    DWORD  GetApplVer( void );
    int    GetApplMajorVer( void );
    String GetApplVerAsString( void );

    String GetExeDir( void );

    bool BrowseForFolder( String dialogTitle, String& folderName );
        // Note: CoInitialize() must have been called before calling this function

    String GetEnvironmentVariable( const String& sEnvVarName );
        // Returns the string associated with the environment variable. Returns
        // an empty string if the env var does not exist or an error occurs

    String LocalTimeString( time_t utcTime );
    String LocalTimeString( time_t utcTime, time_t utcOffset );
    String LocalDateTimeString( time_t utcTime );
    String LocalDateTimeString( time_t utcTime, time_t utcOffset = 0 );
        // All functions create a local time string representing the
        // passed UTC time. Methods passing only a utcTime parameter
        // use the current PC's local time zone setting to create a
        // time string. Methods passing a utcOffset create a local
        // time using the passed offset instead.

    bool GetTimeZoneInfo( String& sTimeZone, TIME_ZONE_INFORMATION& timeZoneInformation, bool& isDST );
        // Fills the passed TIME_ZONE_INFORMATION struct with the info for the
        // time zone name string passed. This string must be one of the strings
        // known to Windows stored as the Display entry in one of the keys under the
        // HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones registry hive.
        // If sTimeZone is an empty string, the TZI for the current timezone is returned
        // and sTimeZone is populated with display string for the timezone.

    bool GetTimeZoneNames( TStringList* pTimeZoneList );
        // Fills the passed string list with all time zones known to Windows stored
        // as the Display entry under the registry hive:
        // HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Time Zones

    DWORD TimeSince( DWORD startTime );
    bool  HaveTimeout( DWORD startTime, DWORD timeoutValue );
        // Timeout helper functions

    int ConvertDelimitedString( const String inputString, TStringList* outputStrings, const String delimiter );

    void ShowModelessForm( TForm* aForm );
        // Show the passed form modelessly(on top of main form). Be sure
        // to restore the window to its normal size, regardless of where
        // it may have last been set.

#endif
