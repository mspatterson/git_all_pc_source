#include <vcl.h>
#pragma hdrstop

#include <System.Math.hpp>

#include "Averaging.h"

#pragma package(smart_init)


//
// Public Variables
//

const String AvgMethodDesc[NBR_AVG_METHODS] = {
    L"None",
    L"Boxcar",
    L"Exponential",
};


const String AvgMethodParamDesc[NBR_AVG_METHODS] = {
    L"N/A",
    L"samples",
    L"alpha",
};


const String AvgLevelDesc[NBR_AVG_LEVELS] = {
    L"Off",
    L"Light",
    L"Medium",
    L"Heavy",
};


//
// This module implements various averaging classes. All
// are descendents of the abstract TAverage() class.
//

__fastcall TAverage::TAverage( float resetVariance )
{
    m_resetVariance = resetVariance;
}


__fastcall TAverage::~TAverage()
{
}


bool TAverage::InputInRange( float fNewValue )
{
    // Returns true if the new value is in range and we can
    // continue averaging. Returns false if the input is
    // out of range and the averaging should reset. Derived
    // classes call this function in their AddValue() method.

    // A variance of 0 means variance checking is disabled
    if( m_resetVariance == 0.0 )
        return true;

    // Calculate the current delta
    float deltaNew = fabs( fNewValue - m_avgValue );

    // Check if we're within the Variance range
    if( deltaNew <= m_resetVariance )
        return true;

    // Fall through means input is not in range
    return false;
}


bool TAverage::IsSameAverage( const AVERAGING_PARAMS& avg1, const AVERAGING_PARAMS& avg2 )
{
    // Returns true if the two params passed represent the same average
    if( avg1.avgMethod != avg2.avgMethod )
        return false;

    // Avg method matches. If the method is none, then we don't evaluate any of
    // the other parameters
    if( avg1.avgMethod == AM_NONE )
        return true;

    if( avg1.avgLevel != avg2.avgLevel )
        return false;

    if( !SameValue( avg1.param, avg2.param, (float)0.001 ) )
        return false;

    if( !SameValue( avg1.absVariance, avg2.absVariance, (float)0.001 ) )
        return false;

    // Fall through means the averages are the same
    return true;
}


//
// TNoAverage: class that provides no averaging on the value
//

__fastcall TNoAverage::TNoAverage() : TAverage( 0.0 )
{
    Reset();
}


__fastcall TNoAverage::~TNoAverage()
{
}


void TNoAverage::Reset( float fResetValue )
{
    m_avgValue  = fResetValue;
    m_haveValue = false;
}


void TNoAverage::AddValue( float newValue )
{
    m_avgValue  = newValue;
    m_haveValue = true;
}


bool TNoAverage::GetIsValid( void )
{
    return m_haveValue;
}


//
// TMovingAverage: implements a moving or 'boxcar' average
//

__fastcall TMovingAverage::TMovingAverage( int nbrItems, float resetVariance ) : TAverage( resetVariance )
{
    // Validate param and create the holding array
    if( nbrItems <= 0 )
        nbrItems = 1;

    m_pDataArray = new float[nbrItems];

    m_maxItems = nbrItems;

    Reset();
}


__fastcall TMovingAverage::~TMovingAverage()
{
    delete [] m_pDataArray;
}


void TMovingAverage::Reset( float fResetValue )
{
    for( int iItem = 0; iItem < m_maxItems; iItem++ )
        m_pDataArray[iItem] = fResetValue;

    m_nbrItems = 0;
    m_avgValue = fResetValue;
}


void TMovingAverage::AddValue( float newValue )
{
    // First check if the input is in range. If not, we have to
    // reset the average first
    if( !InputInRange( newValue ) )
        Reset( newValue );

    // If the queue if full shift down all the items in the array
    if( m_nbrItems == m_maxItems )
    {
        for( int iItem = 0; iItem < m_maxItems - 1; iItem++ )
            m_pDataArray[iItem] = m_pDataArray[iItem+1];

        m_nbrItems = m_maxItems - 1;
    }

    // Add the new value to the queue
    m_pDataArray[m_nbrItems] = newValue;
    m_nbrItems++;

    // Update the average value
    m_avgValue = 0.0;

    for( int iItem = 0; iItem < m_nbrItems; iItem++ )
        m_avgValue += m_pDataArray[iItem];

    m_avgValue /= m_nbrItems;
}


bool TMovingAverage::GetIsValid( void )
{
    // Report we have valid data if we have at least one value
    return( m_nbrItems > 0 );
}


// TExpAverage implements an exponential averaging method.
// The value of alpha given in the constructor must be
// in the range 0 <= alpha <= 1, and represents the portion
// of the current values that gets added to the running
// averaged value.

__fastcall TExpAverage::TExpAverage( float alpha, float resetVariance ) : TAverage( resetVariance )
{
    // Validate alpha. Alpha must be in the range of 0 <= alpha <= 1.
    // However, if alpha is > 1, use its inverse instead.
    if( alpha < 0.0 )
        m_alpha = 0.0;
    else if( alpha > 1.0 )
        m_alpha = 1.0 / alpha;
    else
        m_alpha = alpha;

    Reset();
}


__fastcall TExpAverage::~TExpAverage()
{
}


void TExpAverage::Reset( float fResetValue )
{
    m_avgValue  = fResetValue;
    m_haveValue = false;
}


void TExpAverage::AddValue( float newValue )
{
    // First check if the input is in range. If not, we have to
    // reset the average first
    if( !InputInRange( newValue ) )
        Reset( newValue );

    // Now process the value
    if( m_haveValue )
        m_avgValue = m_alpha * newValue + ( 1.0 - m_alpha ) * m_avgValue;
    else
        m_avgValue = newValue;

    m_haveValue = true;
}


bool TExpAverage::GetIsValid( void )
{
    return m_haveValue;
}

