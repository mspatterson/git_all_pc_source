#ifndef AveragingH
#define AveragingH

    //
    // This module implements a set of classes that perform various
    // types of averaging. Base class has a protected contructor and
    // so cannot be directly constructed.
    //


    //
    // Averaging Enums and Structs
    //

    typedef enum{
        AM_NONE,
        AM_BOXCAR,
        AM_EXPONENTIAL,
        NBR_AVG_METHODS
    } AVG_METHOD;

    typedef enum {
        AL_OFF,
        AL_LIGHT,
        AL_MEDIUM,
        AL_HEAVY,
        NBR_AVG_LEVELS
    } AVG_LEVEL;

    extern const String AvgMethodDesc[];
    extern const String AvgMethodParamDesc[];
    extern const String AvgLevelDesc[];

    typedef struct {
        AVG_METHOD avgMethod;
        AVG_LEVEL  avgLevel;
        float      param;
        float      absVariance;
    } AVERAGING_PARAMS;


    //
    // Averaging Classes
    //

    class TAverage : public TObject
    {
        private:

            // Reset logic control
            float  m_resetVariance;

        protected:
            virtual __fastcall TAverage( float resetVariance );

            float  m_avgValue;

            virtual bool GetIsValid( void ) = 0;

            bool   InputInRange( float fNewValue );
                // Returns true if the new value is in range and we can
                // continue averaging. Returns false if the input is
                // out of range and the averaging should reset. Derived
                // classes call this function in their AddValue() method.

        public:

            virtual __fastcall ~TAverage();

            virtual void Reset( float fResetValue = 0.0 ) = 0;
                // All descendant classes must implement a Reset method.
                // Performing a Reset clears the history from the average.

            virtual void AddValue( float newValue ) = 0;
                // All descendant classes must implement an AddValue method.
                // This method adds a new value to be averaged.

            __property float AverageValue = { read = m_avgValue };
                // All descendant classes must update the m_avgValue var

            __property bool IsValid = { read = GetIsValid };
                // Returns true when enough values have been added to
                // the average.

            static bool IsSameAverage( const AVERAGING_PARAMS& avg1, const AVERAGING_PARAMS& avg2 );
                // Returns true if the two params passed represent the same average
    };


    class TNoAverage : public TAverage
    {
        // TNoAverage does as it says - provides no averaging at all
        private:
            bool m_haveValue;

        protected:
            bool GetIsValid( void );

        public:
            virtual __fastcall TNoAverage();
            virtual __fastcall ~TNoAverage();

            void Reset( float fResetValue = 0.0 );
                // Resets the average

            void AddValue( float newValue );
                // Adds a new value to the average
    };


    class TMovingAverage : public TAverage
    {
        // TMovingAverage implements a sliding or boxcar averaging method
        private:

            float* m_pDataArray;
            int    m_maxItems;
            int    m_nbrItems;

        protected:
            bool GetIsValid( void );

        public:
            virtual __fastcall TMovingAverage( int nbrItems, float resetVariance );
            virtual __fastcall ~TMovingAverage();

            void Reset( float fResetValue = 0.0 );
                // Resets the moving average

            void AddValue( float newValue );
                // Adds a new value to the moving average

            __property int AverageSize  = { read = m_maxItems };
    };


    class TExpAverage : public TAverage
    {
        // TExpAverage implements an exponential averaging method.
        // The value of alpha given in the constructor must be
        // in the range 0 <= alpha <= 1, and represents the portion
        // of the current values that gets added to the running
        // averaged value.
        private:
            float m_alpha;
            bool  m_haveValue;

        protected:
            bool GetIsValid( void );

        public:
            virtual __fastcall TExpAverage( float alpha, float resetVariance );
            virtual __fastcall ~TExpAverage();

            void Reset( float fResetValue = 0.0 );
                // Resets the moving average

            void AddValue( float newValue );
                // Adds a new value to the moving average

            __property float Alpha = { read = m_alpha };
    };

#endif
