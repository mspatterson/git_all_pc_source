//************************************************************************
//
//  SubDeviceBaseClass.h: base class for all WTTS-like devices. This is
//       an abstract class. Therefore, descendent classes will need to
//       implemement a number of abstract functions.
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "BaseRadioDevice.h"
#include "CUDPPort.h"
#include "ApplUtils.h"

#pragma package(smart_init)


//
// Private Declarations
//

const DWORD StatusPktRecvTimeout = 2000;   // We should receive status packets at faster than this rate


// Enum list and variables for populating status packet info requests
typedef enum {
    BRSI_LAST_PKT_TIME,
    BRSI_RAD_0_CHAN,
    BRSI_RAD_0_RSSI,
    BRSI_RAD_0_BIT_RATE,
    BRSI_RAD_1_CHAN,
    BRSI_RAD_1_RSSI,
    BRSI_RAD_1_BIT_RATE,
    BRSI_OUTPUT_STATUS,
    BRSI_UPDATE_RATE,
    BRSI_FW_VER,
    BRSI_USB_SIGS,
    BRSI_RFU_0,
    BRSI_RFU_1,
    BRSI_LAST_RESET,
    NBR_BR_STATUS_ITEMS
} BR_STATUS_ITEM;

static const UnicodeString brStatusCaption[NBR_BR_STATUS_ITEMS] = {
    "Last Pkt Time",
    "TesTORK Radio Chan",
    "TesTORK Radio RSSI",
    "TesTORK Radio Bit Rate",
    "MPL Radio Chan",
    "MPL RSSI",
    "MPL Bit Rate",
    "Output Status",
    "Update Rate",
    "Firmware Ver",
    "USB Ctrl Sigs",
    "RFU 0",
    "RFU 1",
    "Last Reset",
};


//
// Class Implementation
//

__fastcall TBaseRadioDevice::TBaseRadioDevice( void )
{
    ClearCommStats();

    m_port.portType       = CT_UNUSED;
    m_port.connResult     = CCommObj::ERR_NONE;
    m_port.connResultText = "(closed)";

    m_bUsingUDP = false;    // Gets set to true if we're using UDP comms
    m_sBRIPAddr = "";       // If using UDP, this is the IP addr from which the last packet was received
    m_wBRIPPort = 0;        // If using UDP, this is the port from which the last packet was received

    // Init output switch control vars. Note that by default we don't
    // automatically send switch commands. But if we do auto-process
    // we want all switches to start in the 'off' state
    m_bAutoProcessSwitchCmds = false;
    m_lastSetSwTick          = 0;

    for( int iSw = 0; iSw < NBR_BASE_RADIO_OUTPUT_SWITCHES; iSw++ )
    {
        m_outputSwitchState[iSw] = eBS_Unknown;
        m_outputSwitchCmd[iSw]   = eBC_TurnOff;
    }

    // Finish comms init
    Flush();

    m_portLost = false;
}


void TBaseRadioDevice::Flush( void )
{
    // Descendant classes may extend this functionality
    m_tLastStatusPkt   = 0;
    m_dwStatusPktCount = 0;

    memset( &m_lastStatusPkt, 0, sizeof( BASE_STATUS_STATUS_PKT ) );
}


BYTE TBaseRadioDevice::GetRSSI( eBaseRadioNbr eRadio )
{
    if( ( eRadio < 0 ) || ( eRadio >= eBRN_NbrBaseRadios ) )
        return 0;

    return m_lastStatusPkt.radioInfo[eRadio].rssi;
}


BoolStatus TBaseRadioDevice::GetOutputStatus( int iSwNbr )
{
    // Validate switch number
    if( ( iSwNbr < 1 ) || ( iSwNbr > LAST_OUTPUT_SWITCH_NBR ) )
        return eBSt_Unknown;

    // If we're not receiving, we don't know what the status is
    if( !IsReceiving )
        return eBSt_Unknown;

    // Passed switch number is 1-based, account for that.
    iSwNbr--;

    // If we are auto processing, can report a full status
    if( m_bAutoProcessSwitchCmds )
        return GetBoolStatus( m_outputSwitchCmd[iSwNbr], m_outputSwitchState[iSwNbr] );

    // Fall through means we're not auto-processing. Return either
    // on or off based on the last status packet
    BYTE bySwBit = 1 << iSwNbr;

    if( m_lastStatusPkt.outputStatus & bySwBit )
        return eBSt_On;
    else
        return eBSt_Off;
}


bool TBaseRadioDevice::SetOutputSwitch( int iSwNbr, BoolCmd bcCmd )
{
    if( ( iSwNbr < 1 ) || ( iSwNbr > LAST_OUTPUT_SWITCH_NBR ) )
        return false;

    // Passed switch number is 1-based, account for that.
    iSwNbr--;

    m_outputSwitchCmd[iSwNbr] = bcCmd;

    return true;
}


bool TBaseRadioDevice::Connect( const COMMS_CFG& portCfg )
{
    if( portCfg.portType == CT_UNUSED )
    {
        m_port.connResultText = "port settings not specified";
        return false;
    }

    Flush();

    if( !CreateCommPort( portCfg, m_port ) )
        return false;

    if( portCfg.portType == CT_UDP )
        m_bUsingUDP = true;

    return true;
}


void TBaseRadioDevice::Disconnect( void )
{
    // Base class only disconnects and releases any created objects
    ReleaseCommPort( m_port );
}


bool TBaseRadioDevice::GetCommStats( PORT_STATS& portStats )
{
    portStats = m_port.stats;
    return true;
}


void TBaseRadioDevice::ClearCommStats( void )
{
    // Only the count members of the stats gets updated
    ClearPortStats( m_port.stats );
}


bool TBaseRadioDevice::InitPropertyList( TStringList* pCaptions, TStringList* pValues, const UnicodeString captionList[], int nbrCaptions )
{
    // Helper function that initializes the passed string lists used
    // to populate property editors.
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( !IsConnected )
        return false;

    // Add all of the captions first, with blank
    for( int iItem = 0; iItem < nbrCaptions; iItem++ )
    {
        pCaptions->Add( captionList[iItem] );
        pValues->Add( "" );
    }

    return true;
}


bool TBaseRadioDevice::ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, brStatusCaption, NBR_BR_STATUS_ITEMS ) )
        return false;

    if( IsConnected )
    {
        if( m_tLastStatusPkt > 0 )
        {
            pValues->Strings[BRSI_LAST_PKT_TIME] = LocalTimeString( m_tLastStatusPkt );

            // Note - we currently only report data for radio numbers 0 and 1.
            // Radio 2 is not implemented in the hardware
            pValues->Strings[BRSI_RAD_0_CHAN]     = IntToStr(      m_lastStatusPkt.radioInfo[0].chanNbr );
            pValues->Strings[BRSI_RAD_0_RSSI]     = IntToStr(      m_lastStatusPkt.radioInfo[0].rssi    );
            pValues->Strings[BRSI_RAD_0_BIT_RATE] = IntToStr( (int)m_lastStatusPkt.radioInfo[0].bitRate );

            pValues->Strings[BRSI_RAD_1_CHAN]     = IntToStr(      m_lastStatusPkt.radioInfo[1].chanNbr );
            pValues->Strings[BRSI_RAD_1_RSSI]     = IntToStr(      m_lastStatusPkt.radioInfo[1].rssi    );
            pValues->Strings[BRSI_RAD_1_BIT_RATE] = IntToStr( (int)m_lastStatusPkt.radioInfo[1].bitRate );

            pValues->Strings[BRSI_OUTPUT_STATUS]  = IntToHex( m_lastStatusPkt.outputStatus, 2 );
            pValues->Strings[BRSI_UPDATE_RATE]    = IntToStr( m_lastStatusPkt.statusUpdateRate );
            pValues->Strings[BRSI_FW_VER]         = IntToStr( m_lastStatusPkt.fwVer[0] ) + IntToStr( m_lastStatusPkt.fwVer[1] ) + IntToStr( m_lastStatusPkt.fwVer[2] ) + IntToStr( m_lastStatusPkt.fwVer[3] );
            pValues->Strings[BRSI_USB_SIGS]       = IntToHex( m_lastStatusPkt.usbCtrlSignals, 2 );
            pValues->Strings[BRSI_RFU_0]          = IntToHex( m_lastStatusPkt.byRFU[0], 2 );
            pValues->Strings[BRSI_RFU_1]          = IntToHex( m_lastStatusPkt.byRFU[1], 2 );

            switch( m_lastStatusPkt.lastResetType )
            {
                case 1:   pValues->Strings[BRSI_LAST_RESET] = "Power-up";   break;
                case 2:   pValues->Strings[BRSI_LAST_RESET] = "WDT";        break;
                case 3:   pValues->Strings[BRSI_LAST_RESET] = "Brown out";  break;
                default:  pValues->Strings[BRSI_LAST_RESET] = "Type " + IntToStr( m_lastStatusPkt.lastResetType );  break;
            }
        }
    }

    return true;
}


bool TBaseRadioDevice::GetLastStatusPkt( time_t& tLastPkt, BASE_STATUS_STATUS_PKT& lastStatus )
{
    if( !IsConnected )
        return false;

    if( m_tLastStatusPkt == 0 )
        return false;

    tLastPkt   = m_tLastStatusPkt;
    lastStatus = m_lastStatusPkt;

    return true;
}


//
// Null Base Radio Class
//

__fastcall TNullBaseRadio::TNullBaseRadio() : TBaseRadioDevice()
{
    m_bOutputState = 0;
}


bool TNullBaseRadio::Connect( const COMMS_CFG& portCfg )
{
    // Override base class functionality to fake out connecting to port
    // Ignore any passed settings.
    m_port.portType = CT_UNUSED;
    m_port.portObj  = NULL;
    m_port.connResult     = CCommObj::ERR_NONE;
    m_port.connResultText = "";

    m_port.stats.portType = CT_UNUSED;
    m_port.stats.portDesc = "(no device)";

    ClearCommStats();

    // For the NULL base radio, report a full RSSI for all RF channels
    m_lastStatusPkt.radioInfo[0].rssi = 255;
    m_lastStatusPkt.radioInfo[1].rssi = 255;
    m_lastStatusPkt.radioInfo[2].rssi = 255;

    return true;
}


//
// Real Base Radio Class
//

__fastcall TRealBaseRadio::TRealBaseRadio() : TBaseRadioDevice()
{
    m_lastCmdRxTime = 0;
}


UnicodeString TRealBaseRadio::GetDevStatus( void )
{
    if( IsConnected )
    {
        if( IsReceiving )
            return "Active";
        else
            return "Packet Timeout";
    }
    else
    {
        return "Not Connected";
    }
}


bool TRealBaseRadio::GetIsConnected( void )
{
    // Legacy sub only uses the data port
    if( m_port.portObj == NULL )
        return false;

    return m_port.portObj->isConnected;
}


bool TRealBaseRadio::GetDevIsRecving( void )
{
    // Can't be receiving if we're not connected
    if( !IsConnected )
        return false;

    // We're receiving if we haven't timed out on status packets
    return !HaveTimeout( m_lastCmdRxTime, StatusPktRecvTimeout );
}


void TRealBaseRadio::Flush( void )
{
    TBaseRadioDevice::Flush();

    m_rxBuffCount    = 0;
    m_lastRxByteTime = 0;
}


bool TRealBaseRadio::Update( void )
{
    if( !IsConnected )
        return false;

    BYTE pktType;
    BASE_STN_DATA_UNION pktData;

    bool havePkt = CheckForBRResp( pktType, pktData );

    if( havePkt )
    {
        if( pktType == BASE_STN_RESP_STATUS )
        {
            m_lastStatusPkt  = pktData.statusPkt;
            m_tLastStatusPkt = time( NULL );

            if( m_dwStatusPktCount < 0xFFFFFFFE )
                m_dwStatusPktCount++;

            UpdateOutputSwitches();
        }
    }

    // After checking for received data, confirm the port is still open
    if( m_port.portObj->portLost )
    {
        // This is not good!
        m_portLost = true;
        Disconnect();
    }

    return havePkt;
}


void TRealBaseRadio::UpdateOutputSwitches( void )
{
    BYTE byCurrSwBits = m_lastStatusPkt.outputStatus;

    // First, set the status array and update the command array
    for( int iSw = 0; iSw < NBR_BASE_RADIO_OUTPUT_SWITCHES; iSw++ )
    {
        BYTE bySwBit = ( 1 << iSw );

        m_outputSwitchState[iSw] = ( ( byCurrSwBits & bySwBit ) == 0 ) ? eBS_Off : eBS_On;

        // Clear the command state if the switch is in the correct state
        if( ( m_outputSwitchState[iSw] == eBS_Off ) && ( m_outputSwitchCmd[iSw] == eBC_TurnOff ) )
            m_outputSwitchCmd[iSw] = eBC_NoCmd;

        if( ( m_outputSwitchState[iSw] == eBS_On ) && ( m_outputSwitchCmd[iSw] == eBC_TurnOn ) )
            m_outputSwitchCmd[iSw] = eBC_NoCmd;
    }

    // If we are processing sw commands, do it now. The output command array will have
    // been updated above if a switch is in the commanded state, so if any remaining
    // element is in a turn on or turn off state then we need to send a command.
    if( m_bAutoProcessSwitchCmds )
    {
        // We set all switches in one command. So make a copy of the current state
        // of the switch bits and then update those based on the requested state.
        // If any switch bit needs to change, then we send the command.
        bool bUpdateSwBits = false;
        BYTE byNewSwBits   = m_lastStatusPkt.outputStatus;

        for( int iSw = 0; iSw < NBR_BASE_RADIO_OUTPUT_SWITCHES; iSw++ )
        {
            BYTE bySwBit = ( 1 << iSw );

            if( m_outputSwitchCmd[iSw] == eBC_TurnOn )
            {
                byNewSwBits |= bySwBit;
                bUpdateSwBits = true;
            }
            else if( m_outputSwitchCmd[iSw] == eBC_TurnOff )
            {
                byNewSwBits &= ~bySwBit;
                bUpdateSwBits = true;
            }
        }

        // Send the command if either the switches have changed or we have
        // to poke the radio again
        if( bUpdateSwBits || HaveTimeout( m_lastSetSwTick, 3000 ) )
        {
            SetOutputState( byNewSwBits );
            m_lastSetSwTick = GetTickCount();
        }
    }
}


TBaseRadioDevice::SET_RADIO_CHAN_RESULT TRealBaseRadio::SetRadioChannel( eBaseRadioNbr radioNbr, int newChanNbr )
{
    if( !IsConnected )
        return SRCR_CMD_FAILED;

    if( ( newChanNbr < 11 ) || ( newChanNbr > 26 ) )
        return SRCR_CMD_FAILED;

    if( ( radioNbr < 0 ) || ( radioNbr > eBRN_NbrBaseRadios ) )
        return SRCR_CMD_FAILED;

    // Check if the channel is in use. Use the last status packet for this
    // check, as long as we've received a status packet.
    if( m_tLastStatusPkt > 0 )
    {
        bool inUse = false;

        if( radioNbr == 0 )
        {
            if( m_lastStatusPkt.radioInfo[1].chanNbr == newChanNbr )
                inUse = true;
            else if( m_lastStatusPkt.radioInfo[2].chanNbr == newChanNbr )
                inUse = true;
        }
        else if( radioNbr == 1 )
        {
            if( m_lastStatusPkt.radioInfo[0].chanNbr == newChanNbr )
                inUse = true;
            else if( m_lastStatusPkt.radioInfo[2].chanNbr == newChanNbr )
                inUse = true;
        }
        else
        {
            if( m_lastStatusPkt.radioInfo[0].chanNbr == newChanNbr )
                inUse = true;
            else if( m_lastStatusPkt.radioInfo[1].chanNbr == newChanNbr )
                inUse = true;
        }

        if( inUse )
            return SRCR_IN_USE;
    }

    // Fall through means okay to send the command
    BASE_STN_DATA_UNION payloadData;
    payloadData.setChanPkt.radioNbr   = (BYTE)radioNbr;
    payloadData.setChanPkt.newChanNbr = (BYTE)newChanNbr;

    if( SendBRCmd( BASE_STN_CMD_SET_CHAN_NBR, payloadData, sizeof( BASE_STN_SET_RADIO_CHAN ) ) )
        return SRCR_SUCCESS;
    else
        return SRCR_CMD_FAILED;
}


bool TRealBaseRadio::GoToNextRadioChannel( eBaseRadioNbr radioNbr, const int chanList[], int nbrChans )
{
    // We need to be receiving for this command to work since we need to
    // know the channels currently in use.
    if( !IsReceiving )
        return false;

    // We currently only support changing the channels for radios 1 and 2.
    // The 3rd channel is RFU and not implemented.
    switch( radioNbr )
    {
        case eBRN_1:
        case eBRN_2:
            // We support these
            break;

        default:
            return false;
    }

    // Get the current channels in use. This comes from the status packet.
    // Note that the RFU channel is not currently implemented in hardware
    // so ignore its setting in this method.
    BYTE byOtherRadioChan;
    BYTE byCurrRadioChan;

    if( radioNbr == 0 )
    {
        byCurrRadioChan  = m_lastStatusPkt.radioInfo[0].chanNbr;
        byOtherRadioChan = m_lastStatusPkt.radioInfo[1].chanNbr;
    }
    else
    {
        byOtherRadioChan = m_lastStatusPkt.radioInfo[0].chanNbr;
        byCurrRadioChan  = m_lastStatusPkt.radioInfo[1].chanNbr;
    }

    // Look for the current channel number in the channel list array.
    // Initialize the new channel number to a default value in case
    // the current one happens to be bogus for some reason.
    int iNewChanIndex = 0;

    for( int iChanIndex = 0; iChanIndex < nbrChans; iChanIndex++ )
    {
        if( (BYTE)chanList[iChanIndex] == byCurrRadioChan )
        {
            if( iChanIndex + 1 < nbrChans )
                iNewChanIndex = iChanIndex + 1;
            else
                iNewChanIndex = 0;

            break;
        }
    }

    // If the new channel number is in use by the other channel, need
    // to move to the next channel
    if( (BYTE)chanList[iNewChanIndex] == byOtherRadioChan )
    {
        if( iNewChanIndex + 1 < nbrChans )
            iNewChanIndex++;
        else
            iNewChanIndex = 0;
    }

    // Now send the command
    BASE_STN_DATA_UNION payloadData;
    payloadData.setChanPkt.radioNbr   = (BYTE)radioNbr;
    payloadData.setChanPkt.newChanNbr = chanList[iNewChanIndex];

    return SendBRCmd( BASE_STN_CMD_SET_CHAN_NBR, payloadData, sizeof( BASE_STN_SET_RADIO_CHAN ) );
}


bool TRealBaseRadio::SetOutputState( BYTE newState )
{
    if( !IsConnected )
        return false;

    BASE_STN_DATA_UNION payloadData;
    payloadData.setOutputsPkt.outputFlags = newState;

    return SendBRCmd( BASE_STN_CMD_SET_OUTPUTS, payloadData, sizeof( BASE_STN_SET_OUTPUTS_PKT ) );
}


BYTE TRealBaseRadio::GetOutputState( void )
{
    // Return the last switch status received from the base radio
    return m_lastStatusPkt.outputStatus;
}


bool TRealBaseRadio::SendBRCmd( BYTE cmdType, BASE_STN_DATA_UNION& payloadData, BYTE payloadLen )
{
    if( !IsConnected )
        return false;

    // Allocate and clear command area
    BYTE txBuffer[MAX_BASE_STN_PKT_LEN];
    memset( txBuffer, 0, MAX_BASE_STN_PKT_LEN );

    WTTTS_PKT_HDR* pHdr  = (WTTTS_PKT_HDR*)txBuffer;

    // Setup header defaults
    pHdr->pktHdr    = BASE_STN_HDR_TX;
    pHdr->pktType   = cmdType;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = payloadLen;

    if( payloadLen > 0 )
    {
        BYTE* pPayload = &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pPayload, &payloadData, payloadLen );
    }

    int hdrAndPayLoadLen = sizeof(WTTTS_PKT_HDR) + payloadLen;

    txBuffer[hdrAndPayLoadLen] = CalcWTTTSChecksum( txBuffer, hdrAndPayLoadLen );

    DWORD pktLen = hdrAndPayLoadLen + 1;

    DWORD bytesSent = 0;

    if( m_bUsingUDP )
        bytesSent = ((CUDPPort*)m_port.portObj)->SendTo( txBuffer, pktLen, m_sBRIPAddr, m_wBRIPPort );
    else
        bytesSent = m_port.portObj->CommSend( txBuffer, pktLen );

    m_port.stats.bytesSent += bytesSent;

    if( bytesSent == pktLen )
        m_port.stats.cmdsSent++;

    return( bytesSent == pktLen );
}


bool TRealBaseRadio::CheckForBRResp( BYTE& pktType, BASE_STN_DATA_UNION& pktData )
{
    if( !IsConnected )
        return false;

    DWORD bytesRxd = 0;

    if( m_bUsingUDP )
        bytesRxd = ((CUDPPort*)m_port.portObj)->RecvFrom( &(m_rxBuffer[m_rxBuffCount]), MAX_BASE_STN_PKT_LEN - m_rxBuffCount, m_sBRIPAddr, m_wBRIPPort );
    else
        bytesRxd = m_port.portObj->CommRecv( &(m_rxBuffer[m_rxBuffCount]), MAX_BASE_STN_PKT_LEN - m_rxBuffCount );

    if( bytesRxd > 0 )
    {
        m_lastRxByteTime = GetTickCount();
        m_rxBuffCount   += bytesRxd;

        m_port.stats.bytesRecv += bytesRxd;
    }
    else
    {
        // No data received - check if any bytes in the buffer have gone stale
        if( m_rxBuffCount > 0 )
        {
            if( HaveTimeout( m_lastRxByteTime, 250 ) )
            {
                m_rxBuffCount = 0;
                m_port.stats.pktErrors++;
            }
        }
    }

    // Have the parser check for any responses from the unit.
    WTTTS_PKT_HDR pktHdr;

    if( HaveBaseStnRespPkt( m_rxBuffer, m_rxBuffCount, pktHdr, pktData ) )
    {
        pktType = pktHdr.pktType;

        m_lastCmdRxTime = GetTickCount();

        m_port.stats.tLastPkt = time( NULL );
        m_port.stats.respRecv++;

        return true;
    }

    // Fall through means no packet was received
    return false;
}



