//************************************************************************
//
//  BaseRadioDevice.h: base class for all base radio classes. Base class
//      is an abstract class. Two descendant classes are defined: a 'null'
//      class which allows the system to operate without being attached
//      to a base radio, and a 'real' class which requires being connected
//      to an actual base radio.
//
//************************************************************************
#ifndef BaseRadioDeviceH
#define BaseRadioDeviceH

    #include <time.h>

    #include "CommObj.h"
    #include "WTTTSProtocolUtils.h"
    #include "SerialPortUtils.h"
    #include "TescoShared.h"

    // Switches 1 and 2 are used for the interlocks. Switch 3 is used
    // for the PDS alarm. Switch 4 is used for an MPL alert. Use the
    // bit-flag defines to work with the raw bit-values in a base
    // radio packet. Use the 'nbr' defines to set switches individually.
    #define CDS_INTERLOCK_OUTPUT_SWITCH_BIT    0x01
    #define SLIPS_INTERLOCK_OUTPUT_SWITCH_BIT  0x02
    #define ALARM_OUTPUT3_SWITCH_BIT           0x04
    #define MPL_ALERT_OUTPUT_SWITCH_BIT        0x08

    #define CDS_INTERLOCK_OUTPUT_SWITCH_NBR    1
    #define SLIPS_INTERLOCK_OUTPUT_SWITCH_NBR  2
    #define PDS_ALARM_OUTPUT_SWITCH_NBR        3
    #define MPL_ALERT_OUTPUT_SWITCH_NBR        4
    #define LAST_OUTPUT_SWITCH_NBR             4
    #define NBR_BASE_RADIO_OUTPUT_SWITCHES     4


    class TBaseRadioDevice : public TObject {

      protected:

        DEVICE_PORT m_port;
        bool        m_portLost;

        // We use different comm object routines for UDP comms - these
        // flags track that info
        bool       m_bUsingUDP;    // True if we're using UDP comms
        AnsiString m_sBRIPAddr;    // If using UDP, this is the IP addr from which the last packet was received
        WORD       m_wBRIPPort;    // If using UDP, this is the port from which the last packet was received

        BASE_STATUS_STATUS_PKT m_lastStatusPkt;
        time_t                 m_tLastStatusPkt;
        DWORD                  m_dwStatusPktCount;

        bool                   m_bAutoProcessSwitchCmds;
        BoolState              m_outputSwitchState[NBR_BASE_RADIO_OUTPUT_SWITCHES];
        BoolCmd                m_outputSwitchCmd[NBR_BASE_RADIO_OUTPUT_SWITCHES];
        DWORD                  m_lastSetSwTick;

                __fastcall    TBaseRadioDevice( void );
                              // Can only construct descendant classes

        virtual void          Flush( void );

        virtual UnicodeString GetDevStatus( void ) = 0;
        virtual bool          GetIsConnected( void ) = 0;
        virtual bool          GetDevIsRecving( void ) = 0;

        virtual UnicodeString GetPortDesc( void )  { return m_port.stats.portDesc; }

        virtual BYTE          GetRSSI( eBaseRadioNbr eRadio );

                bool          InitPropertyList( TStringList* pCaptions, TStringList* pValues, const UnicodeString captionList[], int nbrCaptions );

        virtual BoolStatus    GetOutputStatus( int iSwNbr );

      public:

        virtual __fastcall ~TBaseRadioDevice() { Disconnect(); }

        __property UnicodeString DeviceStatus = { read = GetDevStatus };

        virtual bool Connect( const COMMS_CFG& portCfg );
        virtual void Disconnect( void );

        __property bool   IsConnected   = { read = GetIsConnected };
        __property String ConnectResult = { read = m_port.connResultText };

        __property bool   IsReceiving   = { read = GetDevIsRecving };

        __property String PortDesc      = { read = GetPortDesc };
        __property bool   PortLost      = { read = m_portLost };

        __property BYTE   RSSI[eBaseRadioNbr] = { read = GetRSSI };

        virtual bool Update( void ) = 0;
            // Descendant classes must implement this function. It will get called
            // regularly to allow the class to receive and process information.

        virtual bool GetCommStats( PORT_STATS& portStats );
        virtual void ClearCommStats( void );

        virtual bool ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues );
        virtual bool GetLastStatusPkt( time_t& tLastPkt, BASE_STATUS_STATUS_PKT& lastStatus );

        __property DWORD StatusPktCount = { read = m_dwStatusPktCount };

        //
        // Command handlers: descendant classes must implement each command
        //

        typedef enum {
           SRCR_SUCCESS,
           SRCR_NO_RADIO,
           SRCR_IN_USE,
           SRCR_CMD_FAILED,
           NBR_SET_RADIO_CHAN_RESULTS
        } SET_RADIO_CHAN_RESULT;

        virtual SET_RADIO_CHAN_RESULT SetRadioChannel( eBaseRadioNbr radioNbr, int iChanNbr ) = 0;
        virtual bool                  GoToNextRadioChannel( eBaseRadioNbr radioNbr, const int chanList[], int nbrChans ) = 0;
            // SetRadioChannel() will set a port's RF channel to the specified value.
            // GoToNextRadioChannel() will advance to the next free radio channel,
            // skipping over one that could be in use by the other port.

        virtual bool SetOutputState( BYTE newState ) = 0;
        virtual BYTE GetOutputState( void ) = 0;
            // Get / set the set of outputs as bit flags (D3-D0). These cause a
            // command to be immediately sent to the base radio.

        virtual bool SetOutputSwitch( int swNbr, BoolCmd bcCmd );
            // Allows for individual output switch control, but requires that
            // property AutoSwitchControl be set to true

        __property bool       AutoSwitchControl = { read = m_bAutoProcessSwitchCmds, write = m_bAutoProcessSwitchCmds };
        __property BoolStatus OutputStatus[int] = { read = GetOutputStatus };

      private:

    };

    // Null base class to be used when there is no base radio to communicate with
    class TNullBaseRadio : public TBaseRadioDevice
    {
      private:
          BYTE m_bOutputState;

      protected:
          virtual UnicodeString GetDevStatus   ( void ) { return "(no device)"; }
          virtual bool          GetDevIsRecving( void ) { return true; }
          virtual bool          GetIsConnected( void )  { return true; }

      public:

        virtual __fastcall TNullBaseRadio();

        virtual bool Connect( const COMMS_CFG& portCfg );
            // Override base class functionality to fake out connecting to port

        virtual void Flush( void ) {}
            // Nothing required for a flush

        virtual bool Update( void ) { return true; }
            // Update method for null class does nothing

        virtual bool GetCommStats( PORT_STATS& portStats ) { return false; }
            // Override base class functionality to return an error indication

        // Command handlers
        virtual SET_RADIO_CHAN_RESULT SetRadioChannel( eBaseRadioNbr radioNbr, int iChanNbr ) { return SRCR_SUCCESS; }
        virtual bool                  GoToNextRadioChannel( eBaseRadioNbr radioNbr, const int chanList[], int nbrChans ) { return true; }

        virtual bool SetOutputState( BYTE newState ) { m_bOutputState = newState; return true; }
        virtual BYTE GetOutputState( void )          { return m_bOutputState; }

    };


    // Real base radio class to be used to communicate to actual device
    class TRealBaseRadio : public TBaseRadioDevice
    {
      protected:

                BYTE          m_rxBuffer[MAX_BASE_STN_PKT_LEN];
                DWORD         m_rxBuffCount;
                DWORD         m_lastRxByteTime;
                DWORD         m_lastCmdRxTime;

                bool          SendBRCmd( BYTE cmdType, BASE_STN_DATA_UNION& payloadData, BYTE payloadLen );
                bool          CheckForBRResp( BYTE& pktType, BASE_STN_DATA_UNION& pktData );

        virtual void          Flush( void );

        virtual UnicodeString GetDevStatus( void );
        virtual bool          GetDevIsRecving( void );
        virtual bool          GetIsConnected( void );

      private:

        void UpdateOutputSwitches( void );

      public:

        virtual __fastcall TRealBaseRadio();

        virtual bool Update( void );

        // Command handlers
        virtual SET_RADIO_CHAN_RESULT SetRadioChannel( eBaseRadioNbr radioNbr, int iChanNbr );
        virtual bool                  GoToNextRadioChannel( eBaseRadioNbr radioNbr, const int chanList[], int nbrChans );

        virtual bool SetOutputState( BYTE newState );
        virtual BYTE GetOutputState( void );
    };

#endif

