//************************************************************************
//
//  TCPComm.cpp: Provides TCP-based tx and rx functions. This source file
//               must not contain any application specific code. Assumes
//               that the using process will call WSAStartup() and
//               WSACleanup()
//
//  Copyright (c) 2007, 2008 Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "CTCPPort.h"

//
// Function definitions
//

__fastcall CTCPPort::CTCPPort( void )
{
    tcpSrcSkt      = INVALID_SOCKET;

    bConnected     = FALSE;
    iConnectResult = ERR_NONE;

    dwBytesTx      = 0;
    dwBytesRx      = 0;
}


__fastcall CTCPPort::~CTCPPort()
{
    Disconnect();
}


CTCPPort::COMM_RESULT CTCPPort::Connect( void* pConnectParams )
{
    // Assume the user has passed our version of the connect params
    CONNECT_PARAMS* pParams = (CONNECT_PARAMS*)pConnectParams;

    Disconnect();

    tcpSrcSkt = socket( PF_INET, SOCK_STREAM, 0 );

    if( tcpSrcSkt == SOCKET_ERROR )
    {
        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }

    sockaddr_in DestAddr;

    ZeroMemory( &DestAddr, sizeof(sockaddr_in) );

    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = inet_addr( pParams->destAddr.c_str() );
    DestAddr.sin_port   = htons( pParams->portNbr );

    // The socket is blocking be default. That means we will block
    // inside the connect() call. But on return, we'll know for sure
    // if we have connected or not.
    if( connect( tcpSrcSkt, (struct sockaddr *)&DestAddr, sizeof(DestAddr) ) == SOCKET_ERROR )
    {
        CloseSocket( &tcpSrcSkt );

        iConnectResult = ERR_COMM_OPEN_FAIL;

        return iConnectResult;
    }

    // Set socket to be non-blocking
    unsigned long nonblocking = 1;

    if( ioctlsocket( tcpSrcSkt, FIONBIO, &nonblocking ) == SOCKET_ERROR )
    {
        CloseSocket( &tcpSrcSkt );

        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }

    // Force the stack to keep the connection alive. Apparently some stack
    // providers don't support this option, so ignore the return value.
    BOOL keepAlive = TRUE;
    setsockopt( tcpSrcSkt, SOL_SOCKET, SO_KEEPALIVE, (const char *)&keepAlive, sizeof(keepAlive) );

    // Fall through means socket successfully init'd
    iConnectResult = ERR_NONE;

    destAddr = pParams->destAddr;
    destPort = pParams->portNbr;

    dwBytesTx = 0;
    dwBytesRx = 0;

    bConnected = TRUE;

    return ERR_NONE;
}


void CTCPPort::Disconnect( void )
{
    // Close the socket, clear out stats, and connection state
    CloseSocket( &tcpSrcSkt );
    bConnected = FALSE;

    dwBytesTx = 0;
    dwBytesRx = 0;

    destAddr = "";
    destPort = 0;
}


DWORD CTCPPort::CommRecv( BYTE* byBuff, DWORD dwMaxBytes )
{
    // Tries to receive up to dwMaxBytes into pbyBuffer.
    // Returns the actual number of bytes read.

    // First, ensure we are connected and that the passed params
    // are valid.
    if( !bConnected )
    {
        return 0;
    }

    if( ( byBuff == NULL )
          ||
        ( dwMaxBytes == 0 )
      )
    {
        return 0;
    }

    // If the socket is open, check for data.
    int rxResult = recv( tcpSrcSkt, byBuff, dwMaxBytes, 0 );

    if( rxResult == SOCKET_ERROR )
    {
        int sockErr = WSAGetLastError();

        switch( sockErr )
        {
            case WSAEWOULDBLOCK:
                // No data waiting for us.
                break;

            case WSAECONNRESET:
                // Lost connection to listener (server)
                Disconnect();
                break;

            default:
                // Treat any other error as a bad thing.
                Disconnect();
                break;
        }
    }

    if( rxResult <= 0 )
    {
        return 0;
    }

    dwBytesRx += rxResult;

    return rxResult;
}


DWORD CTCPPort::CommSend( BYTE* byBuff, DWORD dwByteCount )
{
    // Try to send up to dwByteCount from byBuff.
    // Returns the actual number of bytes sent.

    // First, ensure we are connected and that the passed params
    // are valid.
    if( !bConnected )
        return 0;

    if( ( byBuff == NULL )
          ||
        ( dwByteCount == 0 )
      )
    {
        return 0;
    }

    // Now send the data.
    int bytesSent = send( tcpSrcSkt, byBuff, dwByteCount, 0 );

    if( bytesSent == SOCKET_ERROR )
    {
        if( WSAGetLastError() != WSAEWOULDBLOCK )
        {
            return 0;
        }
    }

    dwBytesTx += bytesSent;

    return bytesSent;
}


AnsiString __fastcall CTCPPort::FormatIPAddr( BYTE anAddr[] )
{
    AnsiString sResult;

    sResult.printf( "%u.%u.%u.%u", anAddr[0], anAddr[1], anAddr[2], anAddr[3] );

    return sResult;
}


bool __fastcall CTCPPort::ExtractIPAddr( AnsiString inputText, BYTE ipAddress[] )
{
    int addrIndex;
    int dotPos;
    AnsiString addrChars;

    // For ease of parsing, throw on a terminating "." on the input string.
    inputText = inputText + '.';

    for( addrIndex = 0; addrIndex < IP_ADDR_LEN; addrIndex++ )
    {
        dotPos = inputText.Pos( "." );

        if( dotPos == 0 )
        {
            return false;
        }

        // Get the chars up to the dot
        addrChars = inputText.SubString( 1, dotPos - 1 );

        // Delete those chars from the input string
        inputText.Delete( 1, dotPos );

        try
        {
            ipAddress[addrIndex] = StrToInt( addrChars );
        }
        catch( ... )
        {
            return false;
        }
    }

    return true;
}


void __fastcall CTCPPort::CloseSocket( SOCKET* aSocket )
{
    // Force a hard close on the socket.
    if( *aSocket != INVALID_SOCKET )
    {
        linger liSettings;

        liSettings.l_onoff  = 1;        // Turn linger on
        liSettings.l_linger = 0;        // Set timeout to 0

        setsockopt( *aSocket, SOL_SOCKET, SO_LINGER,
                   (const char FAR *)(&liSettings), sizeof( linger )
                  );

        closesocket( *aSocket );
        *aSocket = INVALID_SOCKET;
    }
}


void CTCPPort::EnableNoDelay( bool bIsEnabled )
{
    // Pass true to enable the 'no delay' feature in Winsock
    // (eg true means you are disabling Nagle algorithm)
    if( bConnected )
        return;

    char value = bIsEnabled ? 1 : 0;

    setsockopt( tcpSrcSkt, IPPROTO_TCP, TCP_NODELAY, &value, sizeof( value ) );
}

