//************************************************************************
//
//  CTCPPort.cpp: Provides TCP-based tx and rx functions. This source file
//               must not contain any application specific code. Assumes
//               that the using process will call WSAStartup() and
//               WSACleanup()
//
//  Copyright (c) 2017 Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************
#ifndef CTCPPortH
    #define CTCPPortH

    #include "CommObj.h"

    #include <winsock2.h>

    #define IP_ADDR_LEN   4
    #define MAX_ENET_PKT  1520

    class CTCPPort : public CCommObj {

      private:

        SOCKET tcpSrcSkt;

        AnsiString destAddr;
        WORD destPort;

        void __fastcall CloseSocket( SOCKET* aSocket );

      protected:

      public:

          __fastcall CTCPPort( void );
          virtual __fastcall ~CTCPPort();

        typedef struct
        {
            WORD portNbr;
            AnsiString destAddr;
        } CONNECT_PARAMS;

        COMM_RESULT Connect( void* pConnectParams );

        void Disconnect( void );

        DWORD CommRecv( BYTE* byBuff, DWORD dwMaxBytes );
        DWORD CommSend( BYTE* byBuff, DWORD dwNbrBytes );

        __property AnsiString DestAddr = { read = destAddr };
        __property WORD       DestPort = { read = destPort };

        static AnsiString __fastcall FormatIPAddr( BYTE anAddr[] );
        static bool       __fastcall ExtractIPAddr( AnsiString inputText, BYTE ipAddress[] );

        void EnableNoDelay( bool bIsEnabled );
            // Pass true to enable the 'no delay' feature in Winsock
            // (eg true means you are disabling Nagle algorithm)

    };

#endif
