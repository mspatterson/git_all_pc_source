//************************************************************************
//
//  TCPComm.cpp: Provides TCP-based tx and rx functions. This source file
//               must not contain any application specific code. Assumes
//               that the using process will call WSAStartup() and
//               WSACleanup()
//
//  Copyright (c) 2007, 2008 Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "CTCPServerPort.h"


//
// Public Data
//

const String CTCPServerPort::HostTCPStateText[eNBR_TCPServerStates] =
{
    "Not Listening",
    "Listening",
    "Connected",
};


//
// Class Implementation
//

__fastcall CTCPServerPort::CTCPServerPort( void )
{
    m_listenPortNbr = 0;
    m_tcpListenSkt  = INVALID_SOCKET;
    m_tcpOpenSkt    = INVALID_SOCKET;
    m_tcpState      = eTSS_NotListening;

    // Set a default buffer size to allocate on connect requests for now
    bConnected      = FALSE;
    iConnectResult  = ERR_NONE;

    m_destAddr = "";
    m_destPort = 0;

    dwBytesTx = 0;
    dwBytesRx = 0;
}


__fastcall CTCPServerPort::~CTCPServerPort()
{
    Disconnect();
}


CTCPServerPort::COMM_RESULT CTCPServerPort::Connect( void* pConnectParams )
{
    // Assume the user has passed our version of the connect params
    CONNECT_PARAMS* pParams = (CONNECT_PARAMS*)pConnectParams;

    Disconnect();

    m_tcpListenSkt = socket( PF_INET, SOCK_STREAM, 0 );

    if( m_tcpListenSkt == SOCKET_ERROR )
    {
        m_lastErrorMsg = "Could not create source socket, error " + IntToStr( WSAGetLastError() ) + "!";

        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }

    // Set socket to be non-blocking
    unsigned long nonblocking = 1;

    if( ioctlsocket( m_tcpListenSkt, FIONBIO, &nonblocking ) == SOCKET_ERROR )
    {
        CloseSocket( m_tcpListenSkt );

        m_lastErrorMsg = "Could not set socket to be non-blocking, error " + IntToStr( WSAGetLastError() ) + "!";

        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }

    // Bind socket to port
    struct sockaddr_in SrcAddr;
    ZeroMemory( &SrcAddr, sizeof( SrcAddr ) );

    SrcAddr.sin_family      = AF_INET;
    SrcAddr.sin_addr.s_addr = INADDR_ANY;
    SrcAddr.sin_port        = htons( pParams->listeningSocket );

    if( bind( m_tcpListenSkt, (struct sockaddr *)&SrcAddr, sizeof(SrcAddr) ) == SOCKET_ERROR )
    {
        CloseSocket( m_tcpListenSkt );

        m_lastErrorMsg = "Could not bind socket to port, error " + IntToStr( WSAGetLastError() ) + "!";

        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }

    // Finally, set the socket to listen for incoming connections.
    if( listen( m_tcpListenSkt, 100 /* max backlog */ ) == SOCKET_ERROR )
    {
        CloseSocket( m_tcpListenSkt );

        m_lastErrorMsg = "Winsock error: could not start listening on socket, error " + IntToStr( WSAGetLastError() ) + "!";

        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }

    // Fall through means socket successfully init'd
    iConnectResult = ERR_NONE;

    m_lastErrorMsg = "";

    m_tcpState = eTSS_Listening;

    m_listenPortNbr = pParams->listeningSocket;

    dwBytesTx = 0;
    dwBytesRx = 0;

    bConnected = TRUE;

    return ERR_NONE;
}


void CTCPServerPort::Disconnect( void )
{
    // Close the socket, clear out stats, and connection state
    // Be sure the other side knows we've hung up
    PurgePendingConnectRequests();

    CloseSocket( m_tcpOpenSkt );
    CloseSocket( m_tcpListenSkt );

    m_tcpState = eTSS_NotListening;

    bConnected = FALSE;

    m_lastErrorMsg = "";

    m_destAddr = "";
    m_destPort = 0;

    dwBytesTx = 0;
    dwBytesRx = 0;
}


void CTCPServerPort::ProcessConnectionRequests( void )
{
    // If listening, then checks for any connection requests. Will
    // fire a TTCPConnectEvent for each request until one is accepted.
    // If the server is in the connected state, then any pending
    // connect requests are rejected.
    switch( m_tcpState )
    {
        case eTSS_NotListening:
            // Nothing to do in this state
            break;

        case eTSS_Listening:
            // If we have a new connection, accept it
            if( CheckForConnectionRequests() )
                m_tcpState = eTSS_Connected;
            break;

        case eTSS_Connected:
            // We don't accept any new connections in this state
            PurgePendingConnectRequests();
            break;
    }
}


void CTCPServerPort::CloseConnection( void )
{
    // Closes the currently open data connection, and returns the
    // server to the listening state.
    CloseSocket( m_tcpOpenSkt );

    if( m_tcpListenSkt == INVALID_SOCKET )
        m_tcpState = eTSS_NotListening;
    else
        m_tcpState = eTSS_Listening;

    m_destAddr = "";
    m_destPort = 0;

    bConnected = false;

    dwBytesTx = 0;
    dwBytesRx = 0;
}


DWORD CTCPServerPort::CommRecv( BYTE* byBuff, DWORD dwMaxBytes )
{
    // Tries to receive up to dwMaxBytes into pbyBuffer.
    // Returns the actual number of bytes read.

    // First, ensure we are connected and that the passed params
    // are valid.
    if( !bConnected )
        return 0;

    if( ( byBuff == NULL ) || ( dwMaxBytes == 0 ) )
        return 0;

    // Try to receive data now
    int rxResult = recv( m_tcpOpenSkt, byBuff, dwMaxBytes, 0 );

    // If rxResult is zero, assume the far end has closed the connection.
    if( rxResult == 0 )
    {
        CloseConnection();
        return 0;
    }

    // Positive number indicates data received
    if( rxResult > 0 )
    {
        dwBytesRx += (DWORD)rxResult;

        return (DWORD)rxResult;
    }

    // Fall through means an error occurred
    DWORD bytesRcvd = (DWORD)rxResult;

    bool socketStillGood = true;

    int sockErr = WSAGetLastError();

    switch( sockErr )
    {
        case WSAEWOULDBLOCK:
            // No data waiting for us.
            bytesRcvd = 0;
            break;

        case WSAEMSGSIZE:
            // Data received, but truncated!
            bytesRcvd  = dwMaxBytes;
            dwBytesRx += dwMaxBytes;
            break;

        case WSAECONNRESET:
            // Lost connection to listener (server)
            socketStillGood = false;
            m_lastErrorMsg = "WSAGetLastError(): WSAECONNRESET";
            break;

        default:
            // Treat any other error as a bad thing.
            socketStillGood = false;
            m_lastErrorMsg = "WSAGetLastError(): " + IntToStr( sockErr );
            break;
    }

    if( socketStillGood )
        return bytesRcvd;

    // Fall through means socket is no longer good - close it
    CloseConnection();

    return 0;
}


DWORD CTCPServerPort::CommSend( BYTE* byBuff, DWORD dwByteCount )
{
    // Try to send up to dwByteCount from byBuff.
    // Returns the actual number of bytes sent.

    // First, ensure we are connected and that the passed params
    // are valid.
    if( !bConnected )
        return 0;

    if( ( byBuff == NULL ) || ( dwByteCount == 0 ) )
        return 0;

    if( m_tcpOpenSkt == INVALID_SOCKET )
        return 0;

    int bytesSent = send( m_tcpOpenSkt, byBuff, dwByteCount, 0 );

    if( bytesSent == SOCKET_ERROR )
    {
        int sockErr = WSAGetLastError();

        switch( sockErr )
        {
            case WSAECONNRESET:
                // Lost connection
                m_lastErrorMsg = "WSAGetLastError(): WSAECONNRESET";
                break;

            default:
                // Any other error is also a bad thing.
                m_lastErrorMsg = "WSAGetLastError(): " + IntToStr( sockErr );
                break;
        }

        // Close the port on error
        CloseConnection();

        return 0;
    }

    dwBytesTx += bytesSent;

    return bytesSent;
}


void CTCPServerPort::CloseSocket( SOCKET& aSocket )
{
    // Force a hard close on the socket.
    if( aSocket == INVALID_SOCKET )
        return;

    linger liSettings;

    liSettings.l_onoff  = 1;        // Turn linger on
    liSettings.l_linger = 0;        // Set timeout to 0

    setsockopt( aSocket, SOL_SOCKET, SO_LINGER, (const char FAR *)(&liSettings), sizeof( linger ) );

    closesocket( aSocket );

    aSocket = INVALID_SOCKET;
}


void CTCPServerPort::EnableNoDelay( bool bIsEnabled )
{
    // Pass true to enable the 'no delay' feature in Winsock
    // (eg true means you are disabling Nagle algorithm)
    if( bConnected )
        return;

    if( m_tcpOpenSkt == INVALID_SOCKET )
        return;

    char value = bIsEnabled ? 1 : 0;

    setsockopt( m_tcpOpenSkt, IPPROTO_TCP, TCP_NODELAY, &value, sizeof( value ) );
}


void CTCPServerPort::PurgePendingConnectRequests( void )
{
    // Check for new connection requests. If one is pending,
    // accept then close the socket immediately.
    if( m_tcpListenSkt == INVALID_SOCKET )
        return;

    while( 1 )
    {
        sockaddr fromAddr;
        int      fromLen = sizeof( sockaddr );

        SOCKET sNew = accept( m_tcpListenSkt, &fromAddr, &fromLen );

        if( sNew == INVALID_SOCKET )
            break;

        CloseSocket( sNew );
    }
}


bool CTCPServerPort::CheckForConnectionRequests( void )
{
    // Check for new connection requests. We only support one
    // open socket at a time, so if tcpOpenSkt is valid, then
    // don't check for a new socket.
    if( m_tcpOpenSkt != INVALID_SOCKET )
        return true;

    sockaddr fromAddr;
    int      fromLen = sizeof( sockaddr );

    SOCKET sNew = accept( m_tcpListenSkt, &fromAddr, &fromLen );

    if( sNew == INVALID_SOCKET )
        return false;

    // Someone is calling - get the caller's info
    sockaddr_in* pInAddr = (sockaddr_in*)&fromAddr;

    String callerAddr = inet_ntoa( pInAddr->sin_addr );
    WORD   callerPort = ntohs( pInAddr->sin_port );

    // If we have an on-connect callback, ask it if we should accept
    bool bOKToConnect = true;

    if( m_onConnectRequest )
        bOKToConnect = m_onConnectRequest( this, callerAddr, callerPort );

    if( bOKToConnect )
    {
        m_tcpOpenSkt = sNew;

        m_destAddr = callerAddr;
        m_destPort = callerPort;

        m_lastErrorMsg = "";

        bConnected = true;

        dwBytesTx = 0;
        dwBytesRx = 0;

        // Increase the default buffer size for the socket. Windows
        // default is 8192. We need room for 3 packets at about 6K
        // bytes each. Set the buffer size to 32K.
        int txBuffSize = 32768;

        if( setsockopt( m_tcpOpenSkt, SOL_SOCKET, SO_SNDBUF, (char *)&txBuffSize, sizeof(txBuffSize) ) == SOCKET_ERROR )
        {
            // Don't close the socket if this function fails for now
            m_lastErrorMsg = "Warning: could not set S__SNDBUF";
        }
    }
    else
    {
        // Client doesn't like the caller - kill the connection
        CloseSocket( sNew );
    }

    return( m_tcpOpenSkt != INVALID_SOCKET );
}

