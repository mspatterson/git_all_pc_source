//************************************************************************
//
//  CTCPServerPort.cpp: implements a single TCP port connection that
//        listens for an inbound connection and if that connection is
//        accepted by the client, then any future connection requests
//        are rejected until the current connection is closed.
//
//        Assumes that the using process will call WSAStartup() and
//        WSACleanup().
//
//  Copyright (c) 2017 Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************
#ifndef CTCPServerPortH
    #define CTCPServerPortH

    #include "CommObj.h"
    #include "IPUtils.h"

    #include <winsock2.h>


    typedef bool __fastcall (__closure *TTCPConnectEvent)( TObject* Sender, const String& callingIP, WORD wCallingPort );
        // Event callback triggeed when a connect request is received. Return true to accept the
        // request and add the socket to the list of open connections, or false to reject the
        // connection request.

    
    class CTCPServerPort : public CCommObj {
        
      public:

        typedef enum {
            eTSS_NotListening,
            eTSS_Listening,
            eTSS_Connected,
            eNBR_TCPServerStates
        } eTCPServerState;

        static const String HostTCPStateText[];

      private:

        WORD   m_listenPortNbr;
        SOCKET m_tcpListenSkt;
        SOCKET m_tcpOpenSkt;

        eTCPServerState m_tcpState;

        String  m_destAddr;      // Valid only when connected
        WORD    m_destPort;

        String  m_lastErrorMsg;

        TTCPConnectEvent m_onConnectRequest;

        void CloseSocket( SOCKET& aSocket );

        bool CheckForConnectionRequests( void );
        void PurgePendingConnectRequests( void );

      protected:

      public:

          __fastcall CTCPServerPort( void );
          virtual __fastcall ~CTCPServerPort();

        typedef struct
        {
            WORD listeningSocket;
        } CONNECT_PARAMS;

        COMM_RESULT Connect( void* pConnectParams );
            // Opens the listening socket passed in pConnectParams. Opens
            // the listening socket but does not process any incoming
            // connection requests.

        void Disconnect( void );
            // Closes the listening socket and any currently open connection

        void ProcessConnectionRequests( void );
            // If listening, then checks for any connection requests. Will
            // fire a TTCPConnectEvent for each request until one is accepted.
            // If the server is in the connected state, then any pending
            // connect requests are rejected.

        void CloseConnection( void );
            // Closes the currently open data connection, and returns the
            // server to the listening state.

        DWORD CommRecv( BYTE* byBuff, DWORD dwMaxBytes );
        DWORD CommSend( BYTE* byBuff, DWORD dwNbrBytes );
            // Generic comm send /receive handlers. Both methods will also
            // cause the internal state machine to update.

        __property TTCPConnectEvent OnConnectRequest = { read = m_onConnectRequest, write = m_onConnectRequest };
            // Event that is called back for each connection request that is pending. Events will occur
            // while ProcessConnectRequests() is being called.

        __property eTCPServerState ServerState = { read = m_tcpState };
            // Returns the current state of the server.

        __property String LastError = { read = m_lastErrorMsg };
            // Returns the last error message generated. Can also be a status
            // message in the event of a successful operation

        void EnableNoDelay( bool bIsEnabled );
            // Pass true to enable the 'no delay' feature in Winsock
            // (eg true means you are disabling Nagle algorithm)
    };

#endif
