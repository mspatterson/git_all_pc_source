//************************************************************************
//
//  CUDPPort.cpp: Provides UDP-based tx and rx functions. This source file
//               must not contain any application specific code.
//
//  Copyright (c) 2007, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "CUDPPort.h"


//
// Function definitions
//

__fastcall CUDPPort::CUDPPort( void )
{
    bConnected     = FALSE;
    iConnectResult = ERR_NONE;

    dwBytesTx      = 0;
    dwBytesRx      = 0;

    m_udpSrcSkt    = INVALID_SOCKET;
    m_maxDatagram  = MAX_ENET_PKT;

    m_dataLost     = false;

  //  bPortLost      = false;    // Currently this property is not implemented
}


__fastcall CUDPPort::~CUDPPort()
{
    Disconnect();
}


CUDPPort::COMM_RESULT CUDPPort::Connect( void* pConnectParams )
{
    // Assume the user has passed our version of the connect params
    CONNECT_PARAMS* pParams = (CONNECT_PARAMS*)pConnectParams;

    Disconnect();

    m_udpSrcSkt = socket( PF_INET, SOCK_DGRAM, IPPROTO_UDP );

    if( m_udpSrcSkt == SOCKET_ERROR )
    {
        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }

/* Commented out by KM. This was legacy code to try to allow to applications
   on the same computer to communicate via UDP. But for one-way comms, it makes
   more sense to open an unbound UDP port.

    // Reuse this address
    int i_optval = 1;

    if( setsockopt( udpSrcSkt, SOL_SOCKET, SO_REUSEADDR, (char *)&i_optval, sizeof(i_optval) ) == SOCKET_ERROR )
    {
        closesocket( udpSrcSkt );
        udpSrcSkt = INVALID_SOCKET;

        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }
*/

    // Set socket to be non-blocking
    unsigned long nonblocking = 1;

    if( ioctlsocket( m_udpSrcSkt, FIONBIO, &nonblocking ) == SOCKET_ERROR )
    {
        closesocket( m_udpSrcSkt );
        m_udpSrcSkt = INVALID_SOCKET;

        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }

    // Get max datagram length
    m_maxDatagram = MAX_ENET_PKT;

    int maxDgLen = sizeof( m_maxDatagram );

    if( getsockopt( m_udpSrcSkt, SOL_SOCKET, SO_MAX_MSG_SIZE, (char FAR*)&m_maxDatagram, &maxDgLen ) != SOCKET_ERROR )
    {
        if( m_maxDatagram > MAX_ENET_PKT )
            m_maxDatagram = MAX_ENET_PKT;
    }

    // Set the socket to accept broadcasts, if a broadcast address was passed
    if( pParams->acceptBroadcasts )
    {
        BOOL broadcast = TRUE;

        setsockopt( m_udpSrcSkt, SOL_SOCKET, SO_BROADCAST, (char FAR*)&broadcast, sizeof(broadcast) );
    }

    // Bind socket to port. If zero is given as the port number, Windows
    // will select an open UDP port for us
    struct sockaddr_in SrcAddr;
    ZeroMemory( &SrcAddr, sizeof( SrcAddr ) );

    SrcAddr.sin_family      = AF_INET;
    SrcAddr.sin_addr.s_addr = INADDR_ANY;
    SrcAddr.sin_port        = htons( pParams->portNbr );

    if( bind( m_udpSrcSkt, (struct sockaddr *)&SrcAddr, sizeof(SrcAddr) ) == SOCKET_ERROR )
    {
        closesocket( m_udpSrcSkt );
        m_udpSrcSkt = INVALID_SOCKET;

        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }

    // Fall through means socket successfully init'd
    iConnectResult = ERR_NONE;

    dwBytesTx = 0;
    dwBytesRx = 0;

    bConnected = TRUE;

    m_dataLost = false;

    m_destPort = pParams->destPort;
    m_destAddr = pParams->destAddr;

    if( m_destPort != 0 )
        sPortName = m_destAddr + ":" + IntToStr( m_destPort );
    else if( pParams->portNbr != 0 )
        sPortName = "127.0.0.1:" + IntToStr( pParams->portNbr );
    else
        sPortName = "127.0.0.1";

    return ERR_NONE;
}


void CUDPPort::Disconnect( void )
{
    // If we were connected, close the file handle and change the flag.
    if( bConnected )
    {
        CloseSocket( &m_udpSrcSkt );
        m_udpSrcSkt = INVALID_SOCKET;

        bConnected = FALSE;
    }
}


DWORD CUDPPort::CommRecv( BYTE* byBuff, DWORD dwMaxBytes )
{
    // Tries to receive up to dwMaxBytes into pbyBuffer.
    // Returns the actual number of bytes read.

    // First, ensure we are connected and that the passed params
    // are valid.
    if( !bConnected )
        return 0;

    if( ( byBuff == NULL ) || ( dwMaxBytes == 0 ) )
        return 0;

    // If the UDP socket is open, check for data.
    sockaddr fromAddr;
    int      fromLen = sizeof( sockaddr );

    int  rxResult = recvfrom( m_udpSrcSkt, byBuff, dwMaxBytes, 0, &fromAddr, &fromLen );

    if( rxResult == SOCKET_ERROR )
    {
        int sockErr = WSAGetLastError();

        switch( sockErr )
        {
            case WSAEWOULDBLOCK:
                // No data waiting for us.
                break;

            case WSAEMSGSIZE:
                // Data received, but truncated!
                m_dataLost = true;
                break;

            default:
                // Treat any other error as a bad thing.
                /* what to do here??? */
                break;
        }
    }

    if( rxResult <= 0 )
        return 0;

    dwBytesRx += rxResult;

    return rxResult;
}


DWORD CUDPPort::CommSend( const BYTE* byBuff, DWORD dwByteCount )
{
    return SendTo( byBuff, dwByteCount, m_destAddr, m_destPort );
}


DWORD CUDPPort::SendTo( const BYTE* byBuff, DWORD dwByteCount, const AnsiString& destAddr, WORD destPort )
{
    // Try to send up to dwByteCount from byBuff.
    // Returns the actual number of bytes sent.

    // First, ensure we are connected and that the passed params
    // are valid.
    if( !bConnected )
        return 0;

    if( ( byBuff == NULL ) || ( dwByteCount == 0 ) )
        return 0;

    // Now send the data.
    sockaddr_in sockAddr;
    ZeroMemory( &sockAddr, sizeof(sockaddr_in) );

    AnsiString asDestAddr( destAddr );

    sockAddr.sin_family = AF_INET;
    sockAddr.sin_port   = htons( destPort );

    sockAddr.sin_addr.s_addr = inet_addr( asDestAddr.c_str() );

    int bytesSent = sendto( m_udpSrcSkt, byBuff, dwByteCount, 0, (struct sockaddr*)&sockAddr, sizeof( sockAddr ) );

    if( bytesSent == SOCKET_ERROR )
        return 0;

    dwBytesTx += bytesSent;

    return bytesSent;
}


DWORD CUDPPort::RecvFrom( BYTE* byBuff, DWORD dwMaxBytes, AnsiString& srcAddr, WORD& srcPort )
{
    // Tries to receive up to dwMaxBytes into pbyBuffer.
    // Returns the actual number of bytes read.

    // First, ensure we are connected and that the passed params
    // are valid.
    if( !bConnected )
        return 0;

    if( ( byBuff == NULL ) || ( dwMaxBytes == 0 ) )
        return 0;

    // If the UDP socket is open, check for data.
    sockaddr_in fromAddr;
    int fromLen = sizeof( sockaddr_in );

    int  rxResult = recvfrom( m_udpSrcSkt, byBuff, dwMaxBytes, 0, (sockaddr*)&fromAddr, &fromLen );

    if( rxResult == SOCKET_ERROR )
    {
        int sockErr = WSAGetLastError();

        switch( sockErr )
        {
            case WSAEWOULDBLOCK:
                // No data waiting for us.
                break;

            case WSAEMSGSIZE:
                // Data received, but truncated!
                m_dataLost = true;
                break;

            default:
                // Treat any other error as a bad thing.
                /* what to do here??? */
                break;
        }
    }

    if( rxResult <= 0 )
        return 0;


    srcAddr = inet_ntoa( fromAddr.sin_addr );
    srcPort = ntohs( fromAddr.sin_port );

    dwBytesRx += rxResult;

    return rxResult;
}


void __fastcall CUDPPort::CloseSocket( SOCKET* aSocket )
{
    // Force a hard close on the socket.
    if( *aSocket != INVALID_SOCKET )
    {
        linger liSettings;

        liSettings.l_onoff  = 1;        // Turn linger on
        liSettings.l_linger = 0;        // Set timeout to 0

        setsockopt( *aSocket, SOL_SOCKET, SO_LINGER,
                   (const char FAR *)(&liSettings), sizeof( linger )
                  );

        closesocket( *aSocket );
        *aSocket = INVALID_SOCKET;
    }
}
