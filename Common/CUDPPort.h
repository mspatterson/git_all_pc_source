#ifndef CUDPPortH
#define CUDPPortH
//************************************************************************
//
//  Comm32.H: Provides serial handling for Microlynx 32 bit applications.
//            It is a common file for many applications. This source must
//            not contain any application specific code.
//
//  Copyright (c) 2003, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************

#include "CommObj.h"
#include "IPUtils.h"

#include <winsock2.h>


#define IP_ADDR_LEN   4
#define MAX_ENET_PKT  1520



class CUDPPort : public CCommObj {

  protected:

  public:

    __fastcall CUDPPort( void );
    virtual __fastcall ~CUDPPort();

    // Note: if portNbr is 0, then an unbound socket will be opened that
    // is only valid for sending data from, not receiving data to.
    // Params destPort and destAddr must be specified if CommSend()
    // is going to be used. Otherwise, if SendTo() is only ever used
    // then set destPort to 0 and destAddr to "".
    typedef struct
    {
        WORD portNbr;
        bool acceptBroadcasts;
        WORD destPort;
        AnsiString destAddr;
    } CONNECT_PARAMS;

    COMM_RESULT Connect( void* pConnectParams );

    void Disconnect( void );

    DWORD CommRecv(       BYTE* byBuff, DWORD dwMaxBytes );
    DWORD CommSend( const BYTE* byBuff, DWORD dwNbrBytes );

    DWORD SendTo  ( const BYTE* byBuff, DWORD dwBytesToSend, const AnsiString& destAddr, WORD  destPort );
    DWORD RecvFrom(       BYTE* byBuff, DWORD dwMaxBytes,          AnsiString& srcAddr,  WORD& srcPort );

  private:

    SOCKET m_udpSrcSkt;
    int    m_maxDatagram;
    bool   m_dataLost;

    WORD       m_destPort;
    AnsiString m_destAddr;

    void __fastcall CloseSocket( SOCKET* aSocket );

};

#endif

