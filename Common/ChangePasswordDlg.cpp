#include <vcl.h>
#pragma hdrstop

#include "ChangePasswordDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TChangePasswordForm *ChangePasswordForm;


__fastcall TChangePasswordForm::TChangePasswordForm(TComponent* Owner) : TForm(Owner)
{
}


bool TChangePasswordForm::ShowDlg( String& sNewPassword )
{
    // Create the form, set its props, and then show the dialog
    TChangePasswordForm* currForm = new TChangePasswordForm( NULL );

    bool okPressed = false;

    try
    {
        if( currForm->ShowModal() == mrOk )
        {
            sNewPassword = currForm->NewPassword;

            okPressed = true;
        }
    }
    catch( ... )
    {
    }

    delete currForm;

    return okPressed;

}


void __fastcall TChangePasswordForm::FormShow(TObject *Sender)
{
    // Reset form vars
    PasswordMatchLabel->Visible = false;
    BlanksLabel->Visible        = false;
    SaveButton->Enabled         = false;

    NewPasswordEdit->Clear();
    ConfirmPasswordEdit->Clear();

    ActiveControl = NewPasswordEdit;
}


void __fastcall TChangePasswordForm::PasswordChange(TObject *Sender)
{
    if( NewPasswordEdit->Text.Trim().IsEmpty() || ConfirmPasswordEdit->Text.Trim().IsEmpty() )
        SaveButton->Enabled = false;
    else
        SaveButton->Enabled = true;
}


void __fastcall TChangePasswordForm::SaveButtonClick(TObject *Sender)
{
    // Don't allow leading or trailing blanks
    if( NewPasswordEdit->Text.Trim() != NewPasswordEdit->Text )
    {
        // Have leading or trailing blanks
        PasswordMatchLabel->Visible = false;
        BlanksLabel->Visible        = true;

        ConfirmPasswordEdit->SelectAll();

        ActiveControl = NewPasswordEdit;

        return;
    }

    // First check that the new passwords match
    if( NewPasswordEdit->Text.Trim() != ConfirmPasswordEdit->Text.Trim() )
    {
        // Passwords don't match
        PasswordMatchLabel->Visible = true;
        BlanksLabel->Visible        = false;

        ConfirmPasswordEdit->SelectAll();

        ActiveControl = NewPasswordEdit;

        return;
    }

    // Fallthrough means we're good
    ModalResult = mrOk;
}


