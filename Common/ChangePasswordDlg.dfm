object ChangePasswordForm: TChangePasswordForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'New Password'
  ClientHeight = 139
  ClientWidth = 324
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  DesignSize = (
    324
    139)
  PixelsPerInch = 96
  TextHeight = 13
  object NewPasswordLabel: TLabel
    Left = 14
    Top = 15
    Width = 70
    Height = 13
    Caption = 'New Password'
  end
  object ConfirmPasswordLabel: TLabel
    Left = 14
    Top = 48
    Width = 86
    Height = 13
    Caption = 'Confirm Password'
  end
  object PasswordMatchLabel: TLabel
    Left = 120
    Top = 72
    Width = 113
    Height = 13
    Caption = 'Passwords must match!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object BlanksLabel: TLabel
    Left = 120
    Top = 72
    Width = 180
    Height = 13
    Caption = 'Leading or trailing blanks not allowed!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object SaveButton: TButton
    Left = 150
    Top = 102
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Save'
    Default = True
    Enabled = False
    TabOrder = 2
    OnClick = SaveButtonClick
  end
  object CancelButton: TButton
    Left = 237
    Top = 102
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object NewPasswordEdit: TEdit
    Left = 120
    Top = 12
    Width = 192
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    PasswordChar = '*'
    TabOrder = 0
    OnChange = PasswordChange
  end
  object ConfirmPasswordEdit: TEdit
    Left = 120
    Top = 45
    Width = 192
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    PasswordChar = '*'
    TabOrder = 1
    OnChange = PasswordChange
  end
end
