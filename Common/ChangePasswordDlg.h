#ifndef ChangePasswordDlgH
#define ChangePasswordDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>


class TChangePasswordForm : public TForm
{
__published:    // IDE-managed Components
    TButton *SaveButton;
    TButton *CancelButton;
    TLabel *NewPasswordLabel;
    TEdit *NewPasswordEdit;
    TEdit *ConfirmPasswordEdit;
    TLabel *ConfirmPasswordLabel;
    TLabel *PasswordMatchLabel;
    TLabel *BlanksLabel;
    void __fastcall PasswordChange(TObject *Sender);
    void __fastcall SaveButtonClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);

private:    // User declarations

    String m_sNewPW;

protected:

    __property String NewPassword = { read = m_sNewPW, write = m_sNewPW };

public:     // User declarations
    __fastcall TChangePasswordForm(TComponent* Owner);

    static bool ShowDlg( String& sNewPassword );

};

extern PACKAGE TChangePasswordForm *ChangePasswordForm;

#endif
