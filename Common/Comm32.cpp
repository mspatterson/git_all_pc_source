//************************************************************************
//
//  Comm32.cpp: Provides serial handling for Microlynx 32 bit applications.
//              It is a common file for many applications. This source must
//              not contain any application specific code.
//
//  Copyright (c) 2003, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "comm32.h"


//
// Function definitions
//

__fastcall CCommPort::CCommPort( void )
{
    bConnected     = FALSE;
    iConnectResult = ERR_NONE;
    bPortLost      = false;
    dwErrorFlags   = 0;
    dwBytesTx      = 0;
    dwBytesRx      = 0;

    cbInQueue   = 2048;
    cbOutQueue  = 2048;
    hCommHandle = NULL;

    // Clear out the comm stat object
    memset( (void*)&commStat, 0, sizeof( commStat ) );
}


__fastcall CCommPort::~CCommPort()
{
    Disconnect();
}


CCommPort::COMM_RESULT CCommPort::Connect( void* pConnectParams )
{
    // Assume the user has passed our version of the connect params
    CONNECT_PARAMS* pParams = (CONNECT_PARAMS*)pConnectParams;

    Disconnect();
    
    UnicodeString sComStr;
    sComStr.printf( L"\\\\.\\COM%d", pParams->iPortNumber );

    hCommHandle = CreateFile( sComStr.c_str(),
                              GENERIC_READ | GENERIC_WRITE,
                              0,
                              NULL,
                              OPEN_EXISTING,
                              0,
                              NULL /*hTemplateFile*/
                           );

    if( hCommHandle == INVALID_HANDLE_VALUE )
    {
        iConnectResult = ERR_COMM_OPEN_FAIL;
        return iConnectResult;
    }

    if( SetupComm( hCommHandle, cbInQueue, cbOutQueue ) == FALSE )
    {
        // Could not allocate the request buffers.
        CloseHandle( hCommHandle );

        iConnectResult = ERR_COMM_INV_BUFFER_LEN;
        return iConnectResult;
    }

    // Now initialize and set the DCB. Call GetCommState to set
    // the current members, then override the desired members. }

    if( GetCommState( hCommHandle, &dcbInfo ) == FALSE )
    {
        // Could not get comm state...
        CloseHandle( hCommHandle );

        iConnectResult = ERR_COMM_GET_COMM_STATE;
        return iConnectResult;
    }

    dcbInfo.BaudRate     = pParams->dwLineBitRate;
    dcbInfo.Parity       = 0;                    // No parity checking
    dcbInfo.XonLim       = 0;                    // No flow control
    dcbInfo.XoffLim      = 0;
    dcbInfo.ByteSize     = 8;                    // Always 8 bits / byte
    dcbInfo.Parity       = NOPARITY;             // Always no parity
    dcbInfo.StopBits     = ONESTOPBIT;           // Always one stop bit

    if( SetCommState( hCommHandle, &dcbInfo ) == FALSE )
    {
        CloseHandle( hCommHandle );
        
        iConnectResult = ERR_COMM_SET_COMM_STATE;
        return iConnectResult;
    }

    // All is well!
    bConnected = TRUE;

    sPortName = sComStr;

    return ERR_NONE;
}


void CCommPort::Disconnect( void )
{
    // If we were connected, close the file handle and change the flag.
    if( bConnected )
    {
        CloseHandle( hCommHandle );

        bConnected = FALSE;

        sPortName = "(port closed)";
    }
}


DWORD CCommPort::CommRecv( BYTE* byBuff, DWORD dwMaxBytes )
{
    // Tries to receive up to dwMaxBytes into pbyBuffer.
    // Returns the actual number of bytes read.
    COMSTAT currentStat;
    DWORD   dwErrors;
    DWORD   dwBytesRead;

    // First, ensure we are connected and that the passed params
    // are valid.
    if( !bConnected )
        return 0;

    if( ( byBuff == NULL )
          ||
        ( dwMaxBytes == 0 )
      )
    {
        return 0;
    }

    // Next, check for any errors. If this fails, assume that we've
    // lost the serial port.
    if( ClearCommError( hCommHandle, &dwErrors, &currentStat ) == FALSE )
    {
        bPortLost = true;
        Disconnect();

        return 0;
    }

    dwErrorFlags |= dwErrors;
    commStat = currentStat;

    // Now read from the port. Read up to dwMaxBytes from the
    // port. If there are no bytes waiting, skip the read
    // altogether, otherwise the OS will block us in the call. }
    if( commStat.cbInQue == 0 )
        return 0;

    if( commStat.cbInQue < dwMaxBytes )
         dwMaxBytes = commStat.cbInQue;

    dwBytesRead = 0;

    if( ReadFile( hCommHandle, byBuff, dwMaxBytes, &dwBytesRead, NULL ) == FALSE )
    {
        // Comm port is reporting an error.
        dwErrorFlags |= CE_IOE;
    }

    dwBytesRx += dwBytesRead;

    return dwBytesRead;
}


DWORD CCommPort::CommSend( const BYTE* byBuff, DWORD dwByteCount )
{
    // Try to send up to dwByteCount from byBuff.
    // Returns the actual number of bytes sent.
    COMSTAT   currentStat;
    DWORD     dwErrors;
    DWORD     dwBytesSent;

    // First, ensure we are connected and that the passed params
    // are valid.
    if( !bConnected )
        return 0;

    if( ( byBuff == NULL )
          ||
        ( dwByteCount == 0 )
      )
    {
        return 0;
    }

    // Next, check for any errors. If this fails, assume that we've
    // lost the serial port.
    if( ClearCommError( hCommHandle, &dwErrors, &currentStat ) == FALSE )
    {
        bPortLost = true;
        Disconnect();

        return 0;
    }

    dwErrorFlags |= dwErrors;
    commStat = currentStat;

    // Now send the data.
    if( WriteFile( hCommHandle, byBuff, dwByteCount, &dwBytesSent, NULL ) == FALSE )
    {
        // Comm port is reporting an error.
        dwErrorFlags |= CE_IOE;
    }

    dwBytesTx += dwBytesSent;

    return dwBytesSent;
}


DWORD CCommPort::GetCntlSignals( void )
{
    DWORD  ModemStatus  = 0;
    DWORD  SignalStatus = 0;

    // First, ensure we are connected.
    if( !bConnected )
        return 0;

    if( GetCommModemStatus( hCommHandle, &ModemStatus ) )
    {
        if( ModemStatus & MS_CTS_ON )
            SignalStatus |= COMM32_CTS_ON;

        if( ModemStatus & MS_DSR_ON )
            SignalStatus |= COMM32_DSR_ON;

        if( ModemStatus & MS_RLSD_ON )
            SignalStatus |= COMM32_DCD_ON;

        if( ModemStatus & MS_RING_ON )
            SignalStatus |= COMM32_RI_ON;
    }

    if( GetCommState( hCommHandle, &dcbInfo ) )
    {
        if( ModemStatus & dcbInfo.fDtrControl )
            SignalStatus |= COMM32_DTR_ON;

        if( ModemStatus & dcbInfo.fRtsControl )
            SignalStatus |= COMM32_RTS_ON;
    }

    return SignalStatus;
}


void CCommPort::SetDTR( bool bDTROn )
{
    // Ensure we are connected.
    if( bConnected )
    {
        dcbInfo.fDtrControl = bDTROn ? TRUE : FALSE;

        SetCommState( hCommHandle, &dcbInfo );
    }
}


void CCommPort::SetRTS( bool bRTSOn )
{
    // Ensure we are connected.
    if( bConnected )
    {
        dcbInfo.fRtsControl = bRTSOn ? TRUE : FALSE;

        SetCommState( hCommHandle, &dcbInfo );
    }
}

