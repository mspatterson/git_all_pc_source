//************************************************************************
//
//  CommObj.H: Base class for comm-like objects. Descendants can instantiate
//             comm services for serial ports, UDP, TCP, etc
//
//  Copyright (c) 2007, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************

#ifndef _CommObj_H

    #define _CommObj_H

    class CCommObj : public TObject {

      private:

      public:

        typedef enum {
            ERR_NONE,
            ERR_COMM_SET_COMM_STATE,
            ERR_COMM_INV_BUFFER_LEN,
            ERR_COMM_OPEN_FAIL,
            ERR_COMM_GET_COMM_STATE
        }  COMM_RESULT;

      protected:
        UnicodeString sPortName;
        BOOL          bConnected;
        COMM_RESULT   iConnectResult;
        BOOL          bPortLost;
        DWORD         dwBytesTx;
        DWORD         dwBytesRx;

      public:
          __fastcall CCommObj( void )    {}
          virtual __fastcall ~CCommObj() { Disconnect(); }

        virtual COMM_RESULT Connect( void* pConnectParams ) = 0;

        __property COMM_RESULT   connectResult     = { read = iConnectResult };
        __property BOOL          isConnected       = { read = bConnected     };
        __property BOOL          portLost          = { read = bPortLost      };
        __property UnicodeString portName          = { read = sPortName      };

        virtual void Disconnect( void ) = 0;

        virtual DWORD CommRecv(       BYTE* byBuff, DWORD dwMaxBytes ) = 0;
        virtual DWORD CommSend( const BYTE* byBuff, DWORD dwNbrBytes ) = 0;

        __property DWORD BytesSent = { read = dwBytesTx, write = dwBytesTx };
        __property DWORD BytesRecv = { read = dwBytesRx, write = dwBytesRx };
    };

#endif