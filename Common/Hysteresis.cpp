#include <vcl.h>
#pragma hdrstop

#include "Hysteresis.h"
#include "ApplUtils.h"

#pragma package(smart_init)


__fastcall THysteresis::THysteresis()
{
    // Set initial conditions
    m_dwOnDelay  = 0;
    m_dwOffDelay = 0;

    m_dwChangeTickCount = 0;

    m_isState = IS_OFF;
}


void THysteresis::SetState( INPUT_STATE newState )
{
    // Sets the state and latches the tick count of the event
    m_isState = newState;

    // Latch the transition time if the new state is a 'changing' state
    if( ( newState == IS_TURNING_ON ) || ( newState == IS_TURNING_OFF ) )
        m_dwChangeTickCount = GetTickCount();
    else
        m_dwChangeTickCount = 0;
}


void THysteresis::UpdateState( bool bIsActive )
{
    switch( m_isState )
    {
        case IS_OFF:
            // Input is not active. Check for input going active
            if( bIsActive )
                SetState( IS_TURNING_ON );
            break;

        case IS_TURNING_ON:
            // Input was turning on. Input value must continue to meet threshold criteria
            if( !bIsActive )
                SetState( IS_OFF );
            break;

        case IS_ON:
            // Input is active. Check for input going inactive
            if( !bIsActive )
                SetState( IS_TURNING_OFF );
            break;

        case IS_TURNING_OFF:
            // Input was turning off. Input value must continue to meet threshold criteria
            if( bIsActive )
                SetState( IS_ON );
            break;
    }
}


bool THysteresis::GetIsActive( void )
{
    switch( m_isState )
    {
        case IS_OFF:
            // Input is not active
            return false;

        case IS_TURNING_ON:
            // Input was turning on. See if we've met the on criteria
            if( HaveTimeout( m_dwChangeTickCount, m_dwOnDelay ) )
            {
                SetState( IS_ON );
                return true;
            }

            // Fall through means we are still not active
            return false;

        case IS_ON:
            // Input is active
            return true;

        case IS_TURNING_OFF:
            // Input was turning on. See if we've met the on criteria
            if( HaveTimeout( m_dwChangeTickCount, m_dwOnDelay ) )
            {
                SetState( IS_OFF );
                return false;
            }

            // Fall through means we are still active
            return true;
    }

    // Fall through - assume input not active
    return false;
}


__fastcall TBoolHysteresis::TBoolHysteresis( bool bOnCondition )
{
    // A simple boolean (switch) type hysteresis. The object transitions
    // to the 'on' condition when SetCurrentValue() is passed with a bool
    // value equal to that set in the constructor.
    m_bOnCondition = bOnCondition;
}


void TBoolHysteresis::SetCurrentValue( bool bState )
{
    UpdateState( bState == m_bOnCondition );
}


void TBoolHysteresis::SetCurrentValue( int iValue )
{
    // Integer version not supported by this class
    throw Exception( "TBoolHysteresis - passing unsupported value" );
}


__fastcall TIntHysteresis::TIntHysteresis( int iThreshold, THRESHOLD_TYPE ttDir )
{
    // Creates a hysteresis object with the passed threshold. If ttDir is
    // TT_VALUE_INCREASING the object is considered transitioning on if
    // the current value equals or exceeds iThreshold. If ttDir is
    // TT_VALUE_DECREASING then the object is considered transitioning on
    // if the current value equals or is below iThreshold.
    m_iThreshold = iThreshold;
    m_ttDir      = ttDir;
}


void TIntHysteresis::SetCurrentValue( bool bState )
{
    // Integer version not supported by this class
    throw Exception( "TIntHysteresis - passing unsupported value" );
}


void TIntHysteresis::SetCurrentValue( int iValue )
{
    UpdateState( ValueExceedsThreshold( iValue ) );
}


bool TIntHysteresis::ValueExceedsThreshold( int iValue )
{
    if( ( m_ttDir == TT_VALUE_INCREASING ) && ( iValue >= m_iThreshold ) )
        return true;

    if( ( m_ttDir == TT_VALUE_DECREASING ) && ( iValue <= m_iThreshold ) )
        return true;

    return false;
}

