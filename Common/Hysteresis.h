#ifndef HysteresisH
#define HysteresisH

class THysteresis : public TObject
{
  private:

    typedef enum {
      IS_OFF,
      IS_TURNING_ON,
      IS_ON,
      IS_TURNING_OFF
    } INPUT_STATE;

    INPUT_STATE m_isState;

    DWORD m_dwOnDelay;
    DWORD m_dwOffDelay;
        // Durations are in msecs, assumed to be OS tick counts

    DWORD m_dwChangeTickCount;
        // Tick count at time when the state went to either turning on
        // or turning off.

    void SetState( INPUT_STATE newState );
        // Sets the state and latches the tick count of the event

  protected:

    virtual __fastcall THysteresis();
      // This is an abstract virtual base class. Cannot construct
      // instances of it.

    void UpdateState( bool bIsActive );
        // Sets the state and latches the tick count of the event

    virtual bool GetIsActive( void );

  public:

    __property bool  IsActive = { read = GetIsActive };
        // Returns true if the input is active.

    __property DWORD OffToOnDelay = { read = m_dwOnDelay,  write = m_dwOnDelay  };
    __property DWORD OnToOffDelay = { read = m_dwOffDelay, write = m_dwOffDelay };
        // Control the amount of time required for an input to transition
        // from the off state to on state, and then from the on state to
        // the off state

    virtual void SetCurrentValue( bool bState ) = 0;
    virtual void SetCurrentValue( int  iValue ) = 0;
        // All derived classes must implement this method which is called to
        // give the object the current value of the input. Derived classes
        // need not write code for all SetCurrent...() handlers, only those
        // that are appropriate for its type. Unimplemented handlers must be
        // written to throw an exception if called.
};


class TBoolHysteresis : public THysteresis
{
  private:

  protected:
    bool m_bOnCondition;

  public:

    virtual __fastcall TBoolHysteresis( bool bOnCondition );
        // A simple boolean (switch) type hysteresis. The object transitions
        // to the 'on' condition when SetCurrentValue() is passed with a bool
        // value equal to that set in the constructor.

    void SetCurrentValue( bool bState );
    void SetCurrentValue( int  iValue );
};


class TIntHysteresis : public THysteresis
{
  public:

    typedef enum {
       TT_VALUE_INCREASING,
       TT_VALUE_DECREASING
    } THRESHOLD_TYPE;

  private:

  protected:
    int            m_iThreshold;
    THRESHOLD_TYPE m_ttDir;

    bool ValueExceedsThreshold( int iValue );

  public:

    virtual __fastcall TIntHysteresis( int iThreshold, THRESHOLD_TYPE ttDir );
        // Creates a hysteresis object with the passed threshold. If ttDir is
        // TT_VALUE_INCREASING the object is considered transitioning on if
        // the current value equals or exceeds iThreshold. If ttDir is
        // TT_VALUE_DECREASING then the object is considered transitioning on
        // if the current value equals or is below iThreshold.

    void SetCurrentValue( bool bState );
    void SetCurrentValue( int  iValue );
};


#endif
