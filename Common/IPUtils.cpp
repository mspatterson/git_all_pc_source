#include <vcl.h>
#pragma hdrstop

#include "IPUtils.h"

#pragma package(smart_init)


static String m_sHostAddr;


bool InitWinsock( void )
{
    // Initialize the WinSock DLL, and ensure that the version
    // it supports is compatible with the version we need.

    // Make required version: high = minor, low byte = major
    #define MajorVer_Reqd   1
    #define MinorVer_Reqd   1

    WORD wVersionRequested = ( MinorVer_Reqd << 8 ) | MajorVer_Reqd;

    // Initialize and register this application with WinSock
    WSADATA wsaData;

    short sReturn = WSAStartup( wVersionRequested, &wsaData );

    if( sReturn != 0 )
    {
        MessageDlg( "Winsock error: incompatible version!", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    // Check the WinSock version information
    if( wsaData.wVersion != wVersionRequested )
    {
        WSACleanup();

        MessageDlg( "Winsock error: incompatible version!", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    // Get this host's IP address - init the static var in case of error
    m_sHostAddr = "localhost";

    #define MAX_HOSTNAME_LEN   256
    char szHost[MAX_HOSTNAME_LEN];

    if( gethostname( szHost, MAX_HOSTNAME_LEN ) != SOCKET_ERROR )
    {
        struct hostent* pHostEnt = gethostbyname( szHost );

        if( pHostEnt != NULL )
        {
            m_sHostAddr = FormatIPAddr( pHostEnt->h_addr_list[0] );
        }
    }

    // Success! Remember the max
    return true;
}


void ShutdownWinsock( void )
{
    WSACleanup();
}


String FormatIPAddr( const BYTE anAddr[] )
{
    String sResult;

    sResult.printf( L"%.2u.%.2u.%.2u.%.2u", anAddr[0], anAddr[1], anAddr[2], anAddr[3] );

    return sResult;
}


bool ExtractIPAddr( const String& inputText, BYTE ipAddress[] )
{
    int addrIndex;
    int dotPos;
    String addrChars;

    // For ease of parsing, throw on a terminating "." on the input string.
    String sTempInput = inputText + '.';

    for( addrIndex = 0; addrIndex < IP_ADDR_LEN; addrIndex++ )
    {
        dotPos = sTempInput.Pos( "." );

        if( dotPos == 0 )
            return false;

        // Get the chars up to the dot
        addrChars = sTempInput.SubString( 1, dotPos - 1 );

        // Delete those chars from the input string
        sTempInput.Delete( 1, dotPos );

        try
        {
            ipAddress[addrIndex] = StrToInt( addrChars );
        }
        catch( ... )
        {
            return false;
        }
    }

    return true;
}


bool IsHexChar( char aChar )
{
    if( ( aChar >= '0' ) && ( aChar <= '9' ) )
        return true;

    if( ( aChar >= 'a' ) && ( aChar <= 'f' ) )
        return true;

    if( ( aChar >= 'A' ) && ( aChar <= 'F' ) )
        return true;

    // Fall through means not a valid hex char.
    return false;
}


BYTE HexToByte( char aChar )
{
    if( ( aChar >= '0' ) && ( aChar <= '9' ) )
        return aChar - '0';

    if( ( aChar >= 'a' ) && ( aChar <= 'f' ) )
        return aChar - 'a' + 10;

    if( ( aChar >= 'A' ) && ( aChar <= 'F' ) )
        return aChar - 'A' + 10;

    // Fall through means not a valid hex char.
    return 0;
}

