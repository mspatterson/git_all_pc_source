#ifndef IPUtilsH
#define IPUtilsH

    #ifndef IP_ADDR_LEN
        #define IP_ADDR_LEN   4
    #endif

    #ifndef MAX_ENET_PKT
        #define MAX_ENET_PKT  1520
    #endif

    bool InitWinsock( void );
        // Must be called before constructing any TCP/IP objects

    void ShutdownWinsock( void );
        // Must be called after destroying all TCP/IP objects

    String GetHostAddr( void );
        // Returns this PC's IP address. Valid if InitWinsock() returns true

    String FormatIPAddr( const BYTE anAddr[] );
    bool   ExtractIPAddr( AnsiString inputText, BYTE ipAddress[] );

    bool IsHexChar( char aChar );
    BYTE HexToByte( char aChar );

#endif
