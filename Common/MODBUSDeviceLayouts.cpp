#include <vcl.h>
#pragma hdrstop

#include <IniFiles.hpp>

#include "MODBUSDeviceLayouts.h"

#pragma package(smart_init)


//
// This module provides an interface to the device layouts file.
// The dev layout file describes the possible register ranages
// in a device and the attributes of each range.
//
// The file uses the ini-file format. Each section describes
// a particular set of registers.
//

const String RegSection( "Registers" );
const String RegDescEntry  ( "Reg%d_Desc" );
const String RegEnumEntry  ( "Reg%d_Enum" );
const String RegStartEntry ( "Reg%d_Start" );
const String RegSizeEntry  ( "Reg%d_Size" );
const String RegAccessEntry( "Reg%d_RDWR" );
const String RegOffsetEntry( "Reg%d_Offset" );


//
// Public Functions
//

int GetDeviceLayout( const String& devLayoutFile, DEVICE_REG_DESC* pDevRegs, int maxRegs )
{
    // Returns the registers defined in the passed device layout file.
    // Up to maxRegs of entries are populated and function returns number
    // of entries actually populated. If regRanges is NULL, maxRegs is
    // ignored and the function returns the number of regRange items required.
    if( !FileExists( devLayoutFile ) )
         return 0;

    TIniFile* layoutIni = NULL;

    int nbrRegs = 0;

    try
    {
        layoutIni = new TIniFile( devLayoutFile );

        // Ranges must be entered starting with 1 and without any gaps.
        while( true )
        {
            String regStartEntry;
            String regEnumEntry;
            String regDescEntry;
            String regSizeEntry;
            String regRWEntry;
            String regOffsetEntry;

            int currReg = nbrRegs + 1;

            regStartEntry.printf ( RegStartEntry.w_str(),  currReg );
            regEnumEntry.printf  ( RegEnumEntry.w_str(),   currReg );
            regDescEntry.printf  ( RegDescEntry.w_str(),   currReg );
            regSizeEntry.printf  ( RegSizeEntry.w_str(),   currReg );
            regRWEntry.printf    ( RegAccessEntry.w_str(), currReg );
            regOffsetEntry.printf( RegOffsetEntry.w_str(), currReg );

            // The following items are mandatory
            int regStart = layoutIni->ReadInteger( RegSection, regStartEntry, -1 );
            int regEnum  = layoutIni->ReadInteger( RegSection, regEnumEntry,  -1 );
            int regWidth = layoutIni->ReadInteger( RegSection, regSizeEntry,   0 );

            if( ( regStart < 0 ) || ( regEnum < 0 ) || ( regWidth <= 0 ) )
                break;

            // Range looks good. Save the data if we have slots free
            if( pDevRegs != NULL )
            {
                if( nbrRegs < maxRegs )
                {
                    DEVICE_REG_DESC* pCurrRegDesc = &( pDevRegs[nbrRegs] );

                    pCurrRegDesc->regStart     = (DWORD)regStart;
                    pCurrRegDesc->regEnumValue = regEnum;
                    pCurrRegDesc->regWidth     = regWidth;
                    pCurrRegDesc->displayName  = layoutIni->ReadString ( RegSection, regDescEntry,   "" );
                    pCurrRegDesc->addrOffset   = layoutIni->ReadInteger( RegSection, regOffsetEntry, 0  );

                    String sAccessType = layoutIni->ReadString( RegSection, regRWEntry, ""  ).UpperCase();

                    if( sAccessType == "RW" )
                        pCurrRegDesc->accessType = MAT_RD_WR;
                    else if( sAccessType == "W" )
                        pCurrRegDesc->accessType = MAT_WR;
                    else
                        pCurrRegDesc->accessType = MAT_RD;
                }
            }

            // Increment our count and continue searching
            nbrRegs++;
        }
    }
    catch( ... )
    {
    }

    if( layoutIni != NULL )
        delete layoutIni;

    return nbrRegs;
}


