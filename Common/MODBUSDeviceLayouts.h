//---------------------------------------------------------------------------
//
// This module provides an interface to the device layouts file.
// The dev layout file describes each register in a MODBUS device
// and the register attributes
//
// A layout file uses the ini-file format.
//
//---------------------------------------------------------------------------
#ifndef MODBUSDeviceLayoutsH
#define MODBUSDeviceLayoutsH

    #include "MODBUSTypes.h"

    typedef struct {
        String  displayName;      // Description of this register range
        int     regEnumValue;     // User-defined enum for this register
        DWORD   regStart;         // First register address in this range
        DWORD   regWidth;         // Reg width, in bits
        int     addrOffset;       // Offset for reg addr in MODBUS packets, should be 0 or -1
        MODBUS_ACCESS_TYPE accessType;
    } DEVICE_REG_DESC;

    int GetDeviceLayout( const String& devLayoutFile, DEVICE_REG_DESC* pDevRegs, int maxRegs );
        // Returns the registers defined in the passed device layout file.
        // Up to maxRegs of entries are populated and function returns number
        // of entries actually populated. If regRanges is NULL, maxRegs is
        // ignored and the function returns the number of regRange items required.

#endif
