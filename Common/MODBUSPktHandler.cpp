// ******************************************************************************
//
// MODBUSPktHandler.cpp: Modbus protocol module
//
// Copyright (c) 2013, Microlynx Systems Ltd
// ALL RIGHTS RESERVED
//
// This modules builds and parses binary RTU messages for MODBUS slave devices
//
// Date            Author      Comment
// ----------------------------------------------------------------
// 2011-05-19      John     Initial Implementation
// 2013-12-12      KM       C++ port, and remove device level code
//
// ******************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "MODBUSPktHandler.h"
#include "TypeDefs.h"

#pragma package(smart_init)

// ------------------------------------------------------------------------------
// CONSTANT & MACRO DEFINITIONS
// ------------------------------------------------------------------------------

#define MIN_MODBUS_PDU_LEN  ( 1 /*fcn*/ + 2 /*regAddr*/ + 2 /*nbrOrValue*/ )

#define BINARY_CRC_LEN   2


// ------------------------------------------------------------------------------
// PRIVATE DECLARATIONS
// ------------------------------------------------------------------------------

// MODBUS header defines (byte aligned)
#pragma pack(push, 1)

typedef struct {
    WORD wTransID;           // Big endian
    WORD wProtocolID;        // Big endian
    WORD wLength;            // Big endian
    BYTE byUnitID;
} MODBUS_MBAP_HDR;

typedef struct {
    BYTE bySlaveID;
} SERIAL_RTU_HDR;

typedef struct {
    BYTE byFcnCode;
    WORD wRegAddr;
    WORD wNbrRegs;
    BYTE byBytesToFollow;
} WRITE_MULTI_CMD_HDR;

typedef struct {
    BYTE byFcnCode;
    WORD wRegAddr;
    WORD wValue;
} WRITE_SINGLE_CMD_HDR;

typedef struct {
    BYTE byFcnCode;
    WORD wRegAddr;
    WORD wNbrRegsReqd;
} READ_CMD_HDR;

#pragma pack(pop)


// ------------------------------------------------------------------------------
// PRIVATE FUNCTION PROTOTYPES
// ------------------------------------------------------------------------------


// ------------------------------------------------------------------------------
// PUBLIC FUNCTIONS
// ------------------------------------------------------------------------------

// **************************************************************************
//
// Function: HaveModbusCommand
//
// Arguments:
// MODBUS_COMMAND_PKT& cmdPkt - pkt to get filled if buffer contains
//                              valid command
// BYTE* pBuffer   - buffer that contains received data
// WORD  nbrBytes  - bytes in pBuffer
// WORD  bytesUsed - number of bytes read out of pBuffer on success
//
// Returns: eParseMODBUSCmdResult enum value
//
// **************************************************************************
eParseMODBUSCmdResult HaveModbusCommand( eModbusCommType eCommType, const BYTE* pBuffer, WORD nbrBytes, WORD& bytesUsed,
                                         MODBUS_COMMAND_PKT& cmdPkt )
{
    // This parser really just checks that the passed packet has
    // a good CRC and if it does, transfers the data to cmdPkt

    // Clear return struct
    memset( &cmdPkt, 0, sizeof( MODBUS_COMMAND_PKT ) );

    bytesUsed = 0;

    // Make sure we have a buffer
    if( pBuffer == NULL )
        return ePMCR_NULLBuffer;

    // Check for the min packet length. That depends on the comm type
    // Determine packet segment lengths
    WORD wHdrLen;
    WORD wMinPDULen;
    WORD wCRCLen;

    MODBUS_MBAP_HDR* pMBAPHdr = NULL;
    SERIAL_RTU_HDR*  pSerHdr  = NULL;
    const BYTE*      pPayload = NULL;

    if( eCommType == eMCT_Serial )
    {
        wHdrLen     = 1; // Slave addr
        wMinPDULen  = MIN_MODBUS_PDU_LEN;
        wCRCLen     = sizeof( WORD );

        pSerHdr    = (SERIAL_RTU_HDR*)pBuffer;
        pPayload   = &( pBuffer[sizeof(SERIAL_RTU_HDR)] );
    }
    else
    {
        wHdrLen    = sizeof( MODBUS_MBAP_HDR );
        wMinPDULen = MIN_MODBUS_PDU_LEN;
        wCRCLen    = 0;

        pMBAPHdr   = (MODBUS_MBAP_HDR*)pBuffer;
        pPayload   = &( pBuffer[sizeof(MODBUS_MBAP_HDR)] );
    }

    WORD wMinPktLen = wHdrLen + wMinPDULen + wCRCLen;

    if( nbrBytes < wMinPktLen )
        return ePMCR_NotEnoughBytes;

    // Check length or CRC, depending on protocol. This calc will also
    // tell us how long the payload is. Payload starts at the function
    // code and ends at last byte before CRC (if present).
    WORD wPayloadLen;

    if( eCommType == eMCT_Serial )
    {
        WORD calcdCRC = CalcMODBUSCRC16( pBuffer, nbrBytes - BINARY_CRC_LEN );

        XFER_BUFFER xferBuff;

        xferBuff.byData[0] = pBuffer[nbrBytes - 2];
        xferBuff.byData[1] = pBuffer[nbrBytes - 1];

        if( xferBuff.wData[0] != calcdCRC )
            return ePMCR_CRCError;

        wPayloadLen = nbrBytes - sizeof( SERIAL_RTU_HDR ) - BINARY_CRC_LEN;

        // Set return struct values
        cmdPkt.slaveAddr = pSerHdr->bySlaveID;
        cmdPkt.fcnCode   = pPayload[0];
        cmdPkt.wTransID  = 0;
    }
    else
    {
        // Protocol ID must be zero
        if( pMBAPHdr->wProtocolID != 0 )
            return ePMCR_BadHeader;

        // Validate the length checks out. Length is the number of bytes
        // that follow that member, which includes the Unit ID byte in the
        // MBAP header.
        WORD wHeaderLenValue = EndianSwap( pMBAPHdr->wLength );

        // Value in header must be at least 1 byte for Unit ID + the
        // min PDU length
        const WORD wUnitIDLen = 1;

        if( wHeaderLenValue < wUnitIDLen + MIN_MODBUS_PDU_LEN )
            return ePMCR_BadHeader;

        // Make sure we have the number of bytes required
        if( nbrBytes < wHeaderLenValue + sizeof( MODBUS_MBAP_HDR ) - wUnitIDLen )
            return ePMCR_NotEnoughBytes;

        // Set the payload length
        wPayloadLen = wHeaderLenValue - wUnitIDLen;

        // Set return struct values
        cmdPkt.slaveAddr = pMBAPHdr->byUnitID;
        cmdPkt.fcnCode   = pPayload[0];
        cmdPkt.wTransID  = EndianSwap( pMBAPHdr->wTransID );
    }

    // Set the 'is broadcast' flag. Broadcasta are sent to slave addr of zero
    cmdPkt.isBroadcast = ( cmdPkt.slaveAddr == 0 ) ? true : false;

    // Header / CRC checks out. The format for 'write multi' commands
    // if different from read commands. Setup headers
    const WRITE_MULTI_CMD_HDR*  pWrMultiHdr  = (WRITE_MULTI_CMD_HDR*)pPayload;
    const WRITE_SINGLE_CMD_HDR* pWrSingleHdr = (WRITE_SINGLE_CMD_HDR*)pPayload;
    const READ_CMD_HDR*         pRdHdr       = (READ_CMD_HDR*)pPayload;

    switch( cmdPkt.fcnCode )
    {
        case eMCT_ReadCoil:
        case eMCT_ReadInput:
        case eMCT_ReadHoldingReg:
        case eMCT_ReadInputReg:
            cmdPkt.regAddress   = EndianSwap( pRdHdr->wRegAddr );
            cmdPkt.numRegs      = EndianSwap( pRdHdr->wNbrRegsReqd );
            cmdPkt.regValue     = 0;
            cmdPkt.nbrDataItems = 0;
            break;

        case eMCT_WriteCoil:
        case eMCT_WriteHoldingReg:
            cmdPkt.regAddress   = EndianSwap( pWrSingleHdr->wRegAddr );
            cmdPkt.numRegs      = 1;
            cmdPkt.regValue     = EndianSwap( pWrSingleHdr->wValue );
            cmdPkt.nbrDataItems = 0;
            break;

        case eMCT_WriteMultiCoils:
        case eMCT_WriteMultiRegs:
            cmdPkt.regAddress   = EndianSwap( pWrMultiHdr->wRegAddr );
            cmdPkt.nbrDataItems = EndianSwap( pWrMultiHdr->wNbrRegs );

            // Data to write follows write-multi header. Coil data is an
            // array of bytes, while reg endian is an endian-swapped
            // array of words
            if( cmdPkt.fcnCode == eMCT_WriteMultiCoils )
                cmdPkt.nbrDataItems = pWrMultiHdr->byBytesToFollow;
            else
                cmdPkt.nbrDataItems = pWrMultiHdr->byBytesToFollow / sizeof( WORD );

            // Note that byData and wData are a union, so it doesn't matter which
            // var we copy the data to
            memcpy( cmdPkt.byData, &( pPayload[ sizeof( WRITE_MULTI_CMD_HDR ) ] ), pWrMultiHdr->byBytesToFollow );

            break;

        default:
            // Don't support this command type!
            return ePMCR_FcnCode;
    }

    // Fall through means success - tell the user the number of bytes we used
    bytesUsed = wHdrLen + wPayloadLen + wCRCLen;

    return ePMCR_HavePacket;
}


// **************************************************************************
//
// Function: BuildModbusResponse
//
// Arguments:
// MODBUS_COMMAND_PKT& cmdPkt - pkt containing response data
// BYTE* pBuffer  - buffer to build packet in
// WORD  maxBytes - bytes available in pBuffer
//
// Returns: WORD value indicating the size of the packet built in pBuffer.
//          Returns 0 if any error occurs
//
// Description:  Formats a Modbus response
//
// **************************************************************************
WORD BuildModbusResponse( eModbusCommType eCommType, const MODBUS_RESPONSE_PKT& respPkt, BYTE* pBuffer, WORD maxBytes )
{
    // Validate params
    if( ( pBuffer == NULL ) || ( maxBytes == 0 ) )
        return 0;

    // Do initial validation on buffer length.
    WORD wHdrLen;
    WORD wCRCLen;

    MODBUS_MBAP_HDR* pMBAPHdr   = NULL;
    SERIAL_RTU_HDR*  pSerHdr    = NULL;
    BYTE*            pPayload = NULL;

    if( eCommType == eMCT_Serial )
    {
        wHdrLen = 1; // Slave addr
        wCRCLen = sizeof( WORD );

        pSerHdr  = (SERIAL_RTU_HDR*)pBuffer;
        pPayload = &( pBuffer[sizeof(SERIAL_RTU_HDR)] );
    }
    else
    {
        wHdrLen = sizeof( MODBUS_MBAP_HDR );
        wCRCLen = 0;

        pMBAPHdr = (MODBUS_MBAP_HDR*)pBuffer;
        pPayload = &( pBuffer[sizeof(MODBUS_MBAP_HDR)] );
    }

    // Determine PDU length. PDU always starts with the function code
    WORD wPDULen = 1;

    switch( respPkt.fcnCode )
    {
        case eMCT_ReadCoil:
        case eMCT_ReadInput:
            // Response consists of a byte for count and then an array of bytes
            wPDULen += 1 + respPkt.nbrDataItems;
            break;

        case eMCT_ReadHoldingReg:
        case eMCT_ReadInputReg:
            // Response consists of a byte for count and then an array of words
            wPDULen += 1 + respPkt.nbrDataItems * sizeof( WORD );
            break;

        case eMCT_WriteCoil:
        case eMCT_WriteHoldingReg:
            // Response consists of reg number and value written
            wPDULen += 2 * sizeof( WORD );
            break;

        case eMCT_WriteMultiCoils:
        case eMCT_WriteMultiRegs:
            // Response consists of reg number and number of regs written
            wPDULen += 2 * sizeof( WORD );
            break;

        default:
            // Don't support this function code!
            return 0;
    }

    // Now confirm we have the space required
    WORD wReqdPktLen = wHdrLen + wPDULen + wCRCLen;

    if( wReqdPktLen > maxBytes )
        return 0;

    // Buffer is big enough - populate it
    if( eCommType == eMCT_Serial )
    {
        pSerHdr->bySlaveID = respPkt.slaveAddr;
    }
    else
    {
        pMBAPHdr->byUnitID    = respPkt.slaveAddr;
        pMBAPHdr->wProtocolID = 0;  // Must be zero as per MODBUS spec
        pMBAPHdr->wTransID    = EndianSwap( respPkt.wTransID );

        // The length includes one byte of the MBAP header
        pMBAPHdr->wLength = EndianSwap( 1 + wPDULen );
    }

    // All payloads start with the function code
    pPayload[0] = respPkt.fcnCode;

    BYTE* pbyData = &( pPayload[1] );
    WORD* pwData  = (WORD*)pbyData;

    switch( respPkt.fcnCode )
    {
        case eMCT_ReadCoil:
        case eMCT_ReadInput:
            // Balance of payload consists of a byte for count, and then an array of bytes
            pbyData[0] = respPkt.nbrDataItems;
            memcpy( &(pbyData[1]), respPkt.byData, respPkt.nbrDataItems );
            break;

        case eMCT_ReadHoldingReg:
        case eMCT_ReadInputReg:
            // Balance of payload consists of a byte for count, and then an array of words
            pbyData[0] = respPkt.nbrDataItems * sizeof( WORD );
            memcpy( &(pbyData[1]), respPkt.wData, respPkt.nbrDataItems * sizeof( WORD ) );
            break;

        case eMCT_WriteCoil:
        case eMCT_WriteHoldingReg:
            // Response consists of reg number and value written
            pwData[0] = EndianSwap( respPkt.regAddress );
            pwData[1] = respPkt.regValue;
            break;

        case eMCT_WriteMultiCoils:
        case eMCT_WriteMultiRegs:
            // Response consists of reg number and number of regs written
            pwData[0] = EndianSwap( respPkt.regAddress );
            pwData[1] = EndianSwap( respPkt.nbrDataItems );
            break;

        default:
            // Should never happen at at this point
            return 0;
    }

    // Append CRC, if required
    if( eCommType == eMCT_Serial )
    {
        XFER_BUFFER calcdCRC;
        calcdCRC.wData[0] = CalcMODBUSCRC16( pBuffer, wReqdPktLen - BINARY_CRC_LEN );

        pBuffer[wReqdPktLen-2] = calcdCRC.byData[0];
        pBuffer[wReqdPktLen-1] = calcdCRC.byData[1];
    }

    return wReqdPktLen;
}


//**************************************************************************
//
//  Function: BuildModbusExceptionPkt
//
//  Arguments:
//            eModbusCommType       - link type
//            BYTE devAddr          - slave / unit ID
//            BYTE fcnCode          - failed function code
//            eMODBUSExceptionCode  - code to report
//            BYTE* pBuffer         - buffer to build packet in
//            WORD  maxBytes        - bytes available in pBuffer
//
//  Returns: WORD value indicating the size of the packet built in pBuffer.
//           Returns 0 if any error occurs
//
//  Description:  Formats a Modbus exception response.
//
//**************************************************************************
WORD BuildModbusExceptionPkt( eModbusCommType eCommType, BYTE devAddr, BYTE fcnCode, WORD wTransID, eMODBUSExceptionCode exceptCode, BYTE* pBuffer, WORD maxBytes )
{
    // Validate params
    if( ( pBuffer == NULL ) || ( maxBytes == 0 ) )
        return 0;

    // Do initial validation on buffer length.
    WORD wHdrLen;
    WORD wCRCLen;

    MODBUS_MBAP_HDR* pMBAPHdr   = NULL;
    SERIAL_RTU_HDR*  pSerHdr    = NULL;
    BYTE*            pPayload = NULL;

    if( eCommType == eMCT_Serial )
    {
        wHdrLen = 1; // Slave addr
        wCRCLen = sizeof( WORD );

        pSerHdr  = (SERIAL_RTU_HDR*)pBuffer;
        pPayload = &( pBuffer[sizeof(SERIAL_RTU_HDR)] );
    }
    else
    {
        wHdrLen = sizeof( MODBUS_MBAP_HDR );
        wCRCLen = 0;

        pMBAPHdr = (MODBUS_MBAP_HDR*)pBuffer;
        pPayload = &( pBuffer[sizeof(MODBUS_MBAP_HDR)] );
    }

    // PDU length for an exception is always two bytes: function code and exception code
    WORD wPDULen = 2;

    // Now confirm we have the space required
    WORD wReqdPktLen = wHdrLen + wPDULen + wCRCLen;

    if( wReqdPktLen > maxBytes )
        return 0;

    // Buffer is big enough - populate it
    if( eCommType == eMCT_Serial )
    {
        pSerHdr->bySlaveID = devAddr;
    }
    else
    {
        pMBAPHdr->byUnitID    = devAddr;
        pMBAPHdr->wProtocolID = 0;  // Must be zero as per MODBUS spec
        pMBAPHdr->wTransID    = EndianSwap( wTransID );

        // The length includes one byte of the MBAP header
        pMBAPHdr->wLength = EndianSwap( 1 + wPDULen );
    }

    // First byte of the payload is the function code with the exception bit set.
    // Second byte is the exception code
    pPayload[0] = 0x80 | fcnCode;
    pPayload[1] = exceptCode;

    // Append CRC, if required
    if( eCommType == eMCT_Serial )
    {
        XFER_BUFFER calcdCRC;
        calcdCRC.wData[0] = CalcMODBUSCRC16( pBuffer, wReqdPktLen - BINARY_CRC_LEN );

        pBuffer[wReqdPktLen-2] = calcdCRC.byData[0];
        pBuffer[wReqdPktLen-1] = calcdCRC.byData[1];
    }

    return wReqdPktLen;
}


