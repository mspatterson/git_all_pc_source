//******************************************************************************
//
//  MODBUSPktHandler.cpp: Modbus protocol module
//
//      Copyright (c) 2013, Microlynx Systems Ltd
//      ALL RIGHTS RESERVED
//
//  This modules builds and parses binary RTU messages for MODBUS slave devices
//
//  Date            Author      Comment
//  ----------------------------------------------------------------
//  2011-05-19      John     Initial Implementation
//  2013-12-12      KM       C++ port, and remove device level code
//
//******************************************************************************
#ifndef MODBUSPktHandlerH
#define MODBUSPktHandlerH

#include "MODBUSTypes.h"


//------------------------------------------------------------------------------
//  CONSTANT & MACRO DEFINITIONS
//------------------------------------------------------------------------------

#define MAX_MODBUS_DATA_LEN    256    // Max payload in packet, in bytes


//------------------------------------------------------------------------------
//  TYPEDEF DECLARATIONS
//------------------------------------------------------------------------------

// Because of the different types of implementations of MODBUS, not all of
// the following members of the command packet structure may be valid for
// a given command. Clients of this module must use the member in the
// context of the function code and specifics of protocol implementation.
// However, members with a * in their comments will always have a valid
// value.
typedef struct {
    BYTE  slaveAddr;        // * Slave address
    BYTE  fcnCode;          // * Request function code
    bool  isBroadcast;      // * True if address = 0 (broadcast), false otherwise
    WORD  wTransID;         //   For TCP connections, Trans ID as stored in header
    WORD  regAddress;       // * Register (or first register) to act on
    WORD  numRegs;          // * Nbr of regs / coils to act on
    WORD  regValue;         //   Value to set to reg / coil for single 16-bit reg operations
    BYTE  nbrDataItems;     //   Number of elements in either byData or wData

    union {
         BYTE byData[MAX_MODBUS_DATA_LEN];                  // data as bytes
         WORD wData [MAX_MODBUS_DATA_LEN / sizeof(WORD) ];  // data as words
    };

} MODBUS_COMMAND_PKT;

typedef struct {
    BYTE slaveAddr;        // Slave address
    BYTE fcnCode;          // Request function code
    WORD wTransID;         // For TCP connections, Trans ID to report in header
    WORD regAddress;       // Register (or first register) acted on
    WORD regValue;         // Value set to reg / coil for single 16-bit reg operations
    BYTE nbrDataItems;     // Number of elements in either byData or wData

    union {
         BYTE byData[MAX_MODBUS_DATA_LEN];                 // data as bytes
         WORD wData[MAX_MODBUS_DATA_LEN / sizeof(WORD) ];  // data as words
    };

} MODBUS_RESPONSE_PKT;


//------------------------------------------------------------------------------
//  PUBLIC FUNCTIONS PROTOTYPES
//------------------------------------------------------------------------------

//**************************************************************************
//
//  Function: HaveModbusCommand
//
//  Arguments:
//            MODBUS_COMMAND_PKT& cmdPkt - pkt to get filled if buffer contains
//                                         valid command
//            BYTE* pBuffer  - buffer that contains received data
//            WORD  nbrBytes - bytes in pBuffer
//
//  Returns: Enum describing parse result
//
//  Description: Checks if pBuffer has a packet and returns that packet
//               in cmdPkt if it does. Note that any data payload is
//               not endian swapped - caller must do that as appropriate
//
//**************************************************************************
typedef enum {
    ePMCR_NULLBuffer,
    ePMCR_NotEnoughBytes,
    ePMCR_TooManyBytes,
    ePMCR_CRCError,
    ePMCR_BadHeader,
    ePMCR_FcnCode,
    ePMCR_HavePacket,
    ePMCR_NbrParseMODBUSCmdResults
} eParseMODBUSCmdResult;


eParseMODBUSCmdResult HaveModbusCommand( eModbusCommType eCommType, const BYTE* pBuffer, WORD nbrBytes, WORD& bytesUsed, MODBUS_COMMAND_PKT& cmdPkt );


//**************************************************************************
//
//  Function: BuildModbusResponse
//
//  Arguments:
//            MODBUS_COMMAND_PKT& cmdPkt - pkt contains the response info
//            BYTE* pBuffer          - buffer to build packet in
//            WORD  maxBytes         - bytes available in pBuffer
//
//  Returns: WORD value indicating the size of the packet built in pBuffer.
//           Returns 0 if any error occurs
//
//  Description:  Formats a Modbus response. Note that any data payload is
//               not endian swapped - caller must do that as appropriate
//
//**************************************************************************
WORD BuildModbusResponse( eModbusCommType eCommType, const MODBUS_RESPONSE_PKT& respPkt, BYTE* pBuffer, WORD maxBytes );


//**************************************************************************
//
//  Function: BuildModbusExceptionPkt
//
//  Arguments:
//            eModbusCommType       - link type
//            BYTE devAddr          - slave / unit ID
//            BYTE fcnCode          - failed function code
//            eMODBUSExceptionCode  - code to report
//            BYTE* pBuffer         - buffer to build packet in
//            WORD  maxBytes        - bytes available in pBuffer
//
//  Returns: WORD value indicating the size of the packet built in pBuffer.
//           Returns 0 if any error occurs
//
//  Description:  Formats a Modbus exception response.
//
//**************************************************************************
WORD BuildModbusExceptionPkt( eModbusCommType eCommType, BYTE devAddr, BYTE fcnCode, WORD wTransID, eMODBUSExceptionCode exceptCode, BYTE* pBuffer, WORD maxBytes );

#endif
