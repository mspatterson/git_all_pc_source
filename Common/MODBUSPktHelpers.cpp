//
// Helper methods for parsing and creating MODBUS packets
//

#include <vcl.h>
#pragma hdrstop

#include <Math.hpp>

#include "MODBUSPktHelpers.h"
#include "TypeDefs.h"

#pragma package(smart_init)


//
// Public Functions
//

bool MODBUSRegTypeDescToEnum( const String& aDesc, MODBUS_REG_TYPE& itsType )
{
    String cleanDesc = aDesc.Trim();

    for( int iType = 0; iType < NBR_MODBUS_REG_TYPES; iType++ )
    {
        if( cleanDesc.CompareIC( MODBUSRegTypeDesc[iType] ) == 0 )
        {
            itsType = (MODBUS_REG_TYPE)iType;
            return true;
        }
    }

    return false;
}


String GetMODBUSRegValue( const MODBUS_REG_DEFINITION& regDef )
{
    String sResult;

    switch( regDef.mrtType )
    {
        case MRT_BOOL:
            if( regDef.regData.asBool == 0 )
                sResult = "0";
            else
                sResult = "1";
            break;

        case MRT_BYTE:
            sResult = IntToStr( regDef.regData.asByte );
            break;

        case MRT_CHAR:
            if( isprint( regDef.regData.asByte ) )
                sResult = "'" + AnsiString( (char)(regDef.regData.asByte) ) + "'";
            else
                sResult = "0x" + IntToHex( regDef.regData.asByte, 2 );
            break;

        case MRT_INT16:
            sResult = IntToStr( regDef.regData.asShortInt );
            break;

        case MRT_HEX16:
            sResult = "0x" + IntToHex( regDef.regData.asWORD, 4 );
            break;

        case MRT_WORD:
            sResult = IntToStr( regDef.regData.asWORD );
            break;

        case MRT_DWORD:
            sResult = IntToStr( (__int64)regDef.regData.asDWORD );
            break;

        case MRT_INT32:
            sResult = IntToStr( regDef.regData.asInt );
            break;

        case MRT_HEX32:
            sResult = "0x" + IntToHex( (__int64)regDef.regData.asDWORD, 8 );
            break;

        case MRT_FLOAT:
            sResult = FloatToStrF( regDef.regData.asFloat, ffFixed, 7, regDef.precision );
            break;

        default:
            sResult = "Bad type " + IntToStr( regDef.mrtType );
            break;
    }

    return sResult;
}


void SetMODBUSRegValue( REG_DATA_UNION& regData, const MODBUS_REG_DEFINITION& regDef, const String& newValue )
{
    regData.asDWORD = 0;

    // Various input types are accepted, including NaN for floats
    String cleanedString = Trim( newValue ).UpperCase();

    // First, check for textual input
    if( ( cleanedString == "OFF" ) || ( cleanedString == "FALSE" ) )
    {
        if( regDef.mrtType == MRT_FLOAT )
            regData.asFloat = 0.0;
        else
            regData.asDWORD = 0;

        return;
    }

    if( ( cleanedString == "ON" ) || ( cleanedString == "TRUE" ) )
    {
        if( regDef.mrtType == MRT_FLOAT )
            regData.asFloat = 1.0;
        else
            regData.asDWORD = 1;

        return;
    }

    if( cleanedString == "NAN" )
    {
        // In this case, assign NaN to the float member. This will look
        // like a non-sense value if the reg is anything but float
        regData.asFloat = NaN;

        return;
    }

    // Check for a hex value. Hex values are always loaded in literally
    // and may not make sense with respect to the data type for this reg.
    if( cleanedString.Pos( "0X" ) == 1 )
    {
        cleanedString.Delete( 1, 1 );
        cleanedString[1] = '$';

        regData.asDWORD = StrToInt( cleanedString );

        return;
    }

    // Check for char input. In this case we want to create an
    // AnsiString based on the original (non-uppercased) input.
    AnsiString asString( Trim( newValue ) );

    if( asString.Length() == 3 )
    {
        if( ( asString[1] == '\'' ) && ( asString[3] == '\'' ) )
        {
            regData.asByte = asString[2];

            return;
        }
    }

    // Try straight numeric conversions
    int intValue;

    if( TryStrToInt( cleanedString, intValue ) )
    {
        if( regDef.mrtType == MRT_FLOAT )
            regData.asFloat = intValue;
        else
            regData.asInt = intValue;

        return;
    }

    float fValue;

    if( TryStrToFloat( cleanedString, fValue ) )
    {
        if( regDef.mrtType == MRT_FLOAT )
            regData.asFloat = fValue;
        else
            regData.asInt = (int)fValue;

        return;
    }

    // Fall through means we don't support that string type (or that we don't
    // we haven't coded one of the MRT_ enums above)
    throw( Exception( "Invalid input" ) );
}


int MODBUSRegDataToBuff( const MODBUS_REG_DEFINITION& regDef, BYTE* pbyBuff, int regsRequested, int& regsRead, ENDIANESS wordOrder, ENDIANESS byteOrder )
{
    // Fills pbyBuff with data bytes representing this registers value.
    // Endian swaps as necessary. On error, returns zero. Otherwise
    // returns the number of bytes stored in pbyBuff and also returns
    // the number of registers read in regsRead.

    // Init params passed by ref in case of early return
    regsRead = 0;

    // We only support register widths of up to 32 bits for now
    XFER_BUFFER dataBuff;
    dataBuff.dwData = regDef.regData.asDWORD;

    // Data is stored in little-endian format by default. Endian
    // swap if necessary.
    if( byteOrder == HIGH_LOW )
        dataBuff.SwapBytes();

    // If either the physical reg with is 32 bits or the value
    // represented in the register(s) is 32 bits, also swap
    // the words.
    int regWidth = regDef.physRegWidth;

    if( regWidth == 0 )
        return 0;

    if( wordOrder == HIGH_LOW )
    {
        if( ( regDef.physRegWidth == 32 ) || ( regDef.physRegWidth * regDef.nbrPhysRegs == 32 ) )
            dataBuff.SwapWords();
    }

    // Determine how much data to return.
    int regsToRead;

    if( regsRequested > regDef.nbrPhysRegs )
        regsToRead = regDef.nbrPhysRegs;
    else
        regsToRead = regsRequested;

    int bytesToReturn = regsToRead * regDef.physRegWidth / 8 /*bits per byte*/;

    for( int iByte = 0; iByte < bytesToReturn; iByte++ )
        pbyBuff[iByte] = dataBuff.byData[iByte];

    regsRead = regsToRead;

    return bytesToReturn;
}


int MODBUSRegDataFromBuff( MODBUS_REG_DEFINITION& regDef, const BYTE* pbyBuff, int regsAvail, int& regsWritten, ENDIANESS wordOrder, ENDIANESS byteOrder )
{
    // Takes data from pbyBuff and saves it in this register. On error,
    // returns zero. Otherwise returns the number of bytes read from
    // pbyBuff and also returns the number of registers written in regsWritten.

    // Init params passed by ref in case of early return
    regsWritten = 0;

    // Determine how much data to return.
    int regsToWrite;

    if( regsAvail > regDef.nbrPhysRegs )
        regsToWrite = regDef.nbrPhysRegs;
    else
        regsToWrite = regsAvail;

    int bytesToUse = regsToWrite * regDef.physRegWidth / 8 /*bits per byte*/;

    // We only support register widths of up to 32 bits for now
    XFER_BUFFER dataBuff;
    dataBuff.dwData = 0;

    for( int iByte = 0; iByte < bytesToUse; iByte++ )
        dataBuff.byData[iByte] = pbyBuff[iByte];

    // Data is stored in little-endian format by default. Endian
    // swap if necessary.
    if( byteOrder == HIGH_LOW )
        dataBuff.SwapBytes();

    // If either the physical reg with is 32 bits or the value
    // represented in the register(s) is 32 bits, also swap
    // the words.
    int regWidth = regDef.physRegWidth;

    if( regWidth == 0 )
        return 0;

    if( wordOrder == HIGH_LOW )
    {
        if( ( regDef.physRegWidth == 32 ) || ( regWidth == 32 ) )
            dataBuff.SwapWords();
    }

    // Save the data now
    regDef.regData.asDWORD = dataBuff.dwData;

    // Indicate number of reg values used
    regsWritten = regsToWrite;

    return bytesToUse;
}
