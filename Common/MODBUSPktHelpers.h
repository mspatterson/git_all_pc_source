#ifndef MODBUSPktHelpersH
#define MODBUSPktHelpersH

    #include "MODBUSTypes.h"

    //
    // Helper methods for parsing and creating MODBUS packets
    //

    String GetMODBUSRegValue( const MODBUS_REG_DEFINITION& regDef );
    void   SetMODBUSRegValue( REG_DATA_UNION& regData, const MODBUS_REG_DEFINITION& regDef, const String& newValue );
        // Handlers to get / set the data in a register def to / from a string

    int MODBUSRegDataToBuff( const MODBUS_REG_DEFINITION& regDef, BYTE* pbyBuff, int regsRequested, int& regsRead, ENDIANESS wordOrder, ENDIANESS byteOrder );
        // Fills pbyBuff with data bytes representing this register's value.
        // Endian swaps as necessary. On error, returns zero. Otherwise
        // returns the number of bytes stored in pbyBuff and also returns
        // the number of registers read in regsRead.

    int MODBUSRegDataFromBuff( MODBUS_REG_DEFINITION& regDef, const BYTE* pbyBuff, int regsAvail, int& regsWritten, ENDIANESS wordOrder, ENDIANESS byteOrder );
        // Takes data from pbyBuff and saves it in this register. On error,
        // returns zero. Otherwise returns the number of bytes read from
        // pbyBuff and also returns the number of registers written in regsWritten.

#endif
