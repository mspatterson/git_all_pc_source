//
// Low-level MODBUS Type Definitions
//

#ifndef MODBUSTypesH
#define MODBUSTypesH

    typedef enum
    {
        eMCT_None,
        eMCT_Serial,
        eMCT_EnetUDP,
        eMCT_EnetTCP,
        eNbrModbusCommTypes
    } eModbusCommType;


    typedef enum {
      LOW_HIGH,
      HIGH_LOW
    } ENDIANESS;


    /* MODBUS commands:
    0x01 read coil: word first coil, word nbr coils; resp = array of bools
    0x02 read input: word first input, word nbr inputs; resp = array of words
    0x03 read holding reg: word first input, word nbr inputs; resp = array of words
    0x04 read input reg: word first input, word nbr inputs; resp = array of words
    0x05 write coil: word coil nbr, bool coil state
    0x06 write holding reg: word reg nbr, word value
    0x0F write multi coils: word first coil, word nbr coils, array bit values
    0x10 write mult regs: word first reg, word nbr regs, array word values
    */
    typedef enum {
        eMCT_ReadCoil        = 1,
        eMCT_ReadInput       = 2,
        eMCT_ReadHoldingReg  = 3,
        eMCT_ReadInputReg    = 4,
        eMCT_WriteCoil       = 5,
        eMCT_WriteHoldingReg = 6,
        eMCT_WriteMultiCoils = 15,
        eMCT_WriteMultiRegs  = 16,
    } eMODBUSCmdType;


    typedef enum {
        eMEC_NoError        = 0,
        eMEC_UnsupportedFcn = 1,
        eMEC_BadAddrRange   = 2,
        eMEC_BadRegCount    = 3,
        eMEC_OpFailed       = 4
    } eMODBUSExceptionCode;


    typedef enum {
        MAT_UNDEFINED = 0x00,
        MAT_RD        = 0x01,     // Access types defined this way so that they
        MAT_WR        = 0x02,     // can be used a bit-flags as well
        MAT_RD_WR     = 0x03
    } MODBUS_ACCESS_TYPE;


    typedef enum {
        MRT_UNKNOWN,
        MRT_BOOL,
        MRT_BYTE,
        MRT_CHAR,
        MRT_INT16,
        MRT_WORD,
        MRT_HEX16,
        MRT_INT32,
        MRT_DWORD,
        MRT_HEX32,
        MRT_FLOAT,
        NBR_MODBUS_REG_TYPES
    } MODBUS_REG_TYPE;

    extern const String MODBUSRegTypeDesc[NBR_MODBUS_REG_TYPES];

    bool MODBUSRegTypeDescToEnum( const String& aDesc, MODBUS_REG_TYPE& itsType );


    typedef union {
        BYTE  asBool;
        BYTE  asByte;
        INT16 asShortInt;
        int   asInt;
        WORD  asWORD;
        DWORD asDWORD;
        float asFloat;
        BYTE  byData[4];
        WORD  wData[2];
    } REG_DATA_UNION;

    typedef struct {
        DWORD              regNbr;           // User-defined register number
        int                userEnum;         // Enum used to link this field to a data source
        MODBUS_REG_TYPE    mrtType;          // Used to format data
        MODBUS_ACCESS_TYPE matAccType;       // Displayed in attribute field; indicates host's ability to rd/wr reg
        String             displayName;      // User-defined name for this register
        String             units;            // Displayed after value
        String             defValue;         // Initial value for register
        int                precision;        // Indicates number of places after DP to display for floats
        int                physRegWidth;     // Width of underlying physical regs, in bits
        int                nbrPhysRegs;      // Nbr of underlying physical regs required by this user-reg (calculated field)
        REG_DATA_UNION     regData;          // Storage for this register's actual data

        void Clear( void ) {
            this->regNbr      = 0;
            this->userEnum    = 0;
            this->mrtType     = MRT_WORD;
            this->matAccType  = MAT_RD;
            this->displayName = "";
            this->units       = "";
            this->defValue    = "";
            this->precision   = 0;
            this->physRegWidth = 16;
            this->nbrPhysRegs  = 1;
            this->regData.asDWORD = 0;
        }
    } MODBUS_REG_DEFINITION;


    //******************************************************************************
    //
    //  Function: CalcMODBUSCRC16
    //
    //  Arguments: pMsgData - pointer to binary message data in RTU mode
    //             DataLen - number of bytes in message data
    //
    //  Returns: 16 bit calculated CRC value
    //
    //  Description: This function is used to calculate the 16 bit CRC value for
    //               Modbus RTU (binary) mode data.
    //
    //******************************************************************************
    WORD CalcMODBUSCRC16( const BYTE* pMsgData, WORD DataLen );

    // Performs an endian swap of the bytes of the passed word
    WORD EndianSwap( WORD wInput );

#endif

