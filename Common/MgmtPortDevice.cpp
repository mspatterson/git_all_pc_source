#include <vcl.h>
#pragma hdrstop

#include "MgmtPortDevice.h"
#include "ApplUtils.h"

#pragma package(smart_init)


//
// Class Implementation
//

__fastcall TMgmtPortDevice::TMgmtPortDevice( void )
{
    ClearCommStats();

    m_port.portType       = CT_UNUSED;
    m_port.connResult     = CCommObj::ERR_NONE;
    m_port.connResultText = "(closed)";

    Flush();
}


void TMgmtPortDevice::Flush( void )
{
    // Descendant classes may extend this functionality
}


bool TMgmtPortDevice::GetIsConnected( void )
{
    // Legacy sub only uses the data port
    if( m_port.portObj == NULL )
        return false;

    return m_port.portObj->isConnected;
}


bool TMgmtPortDevice::GetCommStats( PORT_STATS& portStats )
{
    // If we have no port object, and the port type is not 'none',
    // then a connect error occurred. Return that to the caller.
    portStats = m_port.stats;

    if( m_port.portType != CT_UNUSED )
    {
        if( m_port.portObj == NULL )
            portStats.portDesc = "Failed: " + m_port.connResultText;
    }

    return true;
}


void TMgmtPortDevice::ClearCommStats( void )
{
    // Only the count members of the stats gets updated
    ClearPortStats( m_port.stats );
}


//
// Null Base Radio Class
//

__fastcall TNullMgmtPort::TNullMgmtPort() : TMgmtPortDevice()
{
}


bool TNullMgmtPort::Connect( const COMMS_CFG& portCfg )
{
    // Override base class functionality to fake out connecting to port
    // Ignore any passed settings.
    m_port.portType = CT_UNUSED;
    m_port.portObj  = NULL;
    m_port.connResult     = CCommObj::ERR_NONE;
    m_port.connResultText = "";

    m_port.stats.portType = CT_UNUSED;
    m_port.stats.portDesc = L"";

    ClearCommStats();

    return true;
}


void TNullMgmtPort::Disconnect( void )
{
    ClearCommStats();
}



//
// Real Base Radio Class
//

__fastcall TRealMgmtPort::TRealMgmtPort() : TMgmtPortDevice()
{
}


bool TRealMgmtPort::Connect( const COMMS_CFG& portCfg )
{
    if( portCfg.portType == CT_UNUSED )
    {
        m_port.connResultText = "port settings not specified";
        return false;
    }

    Flush();

    if( !CreateCommPort( portCfg, m_port ) )
        return false;

    // Port is available for use. Go through the 'unreset' sequence
    CCommPort* pCommPort = dynamic_cast<CCommPort*>( m_port.portObj );

    if( pCommPort == NULL )
        return false;

    pCommPort->SetRTS( false );
    pCommPort->SetDTR( false );
    Sleep( 50 );

    pCommPort->SetDTR( true );
    Sleep( 50 );

    pCommPort->SetDTR( false );
    Sleep( 50 );

    return true;
}


void TRealMgmtPort::Disconnect( void )
{
    // Base class only disconnects and releases any created objects
    ReleaseCommPort( m_port );

    ClearCommStats();
}


UnicodeString TRealMgmtPort::GetDevStatus( void )
{
    if( IsConnected )
        return "Connected";
    else
        return "Not connected";
}


void TRealMgmtPort::Flush( void )
{
    TMgmtPortDevice::Flush();
}


bool TRealMgmtPort::Update( void )
{
    if( !IsConnected )
        return false;

    return true;
}



