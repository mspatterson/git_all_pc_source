#ifndef MgmtPortDeviceH
#define MgmtPortDeviceH

    #include "CommObj.h"
    #include "WTTTSProtocolUtils.h"
    #include "SerialPortUtils.h"

    class TMgmtPortDevice : public TObject
    {
      protected:

        DEVICE_PORT m_port;

        virtual __fastcall    TMgmtPortDevice( void );
                              // Can only construct descendant classes

        virtual void          Flush( void );

        virtual UnicodeString GetDevStatus( void ) = 0;
        virtual bool          GetIsConnected( void );

        virtual UnicodeString GetPortDesc( void )  { return m_port.stats.portDesc; }

      public:

        virtual __fastcall ~TMgmtPortDevice() { Disconnect(); }

        __property UnicodeString DeviceStatus = { read = GetDevStatus };

        virtual bool Connect( const COMMS_CFG& portCfg ) = 0;
        virtual void Disconnect( void ) = 0;
            // Derived classes must implement appropriate handlers

        __property bool          IsConnected   = { read = GetIsConnected };
        __property UnicodeString ConnectResult = { read = m_port.connResultText };

        __property UnicodeString PortDesc      = { read = GetPortDesc };

        virtual bool Update( void ) = 0;
            // Descendant classes must implement this function. It will get called
            // regularly to allow the class to receive and process information.

        virtual bool GetCommStats( PORT_STATS& portStats );
        virtual void ClearCommStats( void );

      private:

    };

    // Null base class to be used when there is no base station to communicate with
    class TNullMgmtPort : public TMgmtPortDevice
    {
      protected:
          virtual UnicodeString GetDevStatus( void ) { return ""; }

      public:

        virtual __fastcall TNullMgmtPort();

        virtual bool Connect( const COMMS_CFG& portCfg );
        virtual void Disconnect( void );
            // Derived classes must implement these methods

        virtual void Flush( void ) {}
            // Nothing required for a flush

        virtual bool Update( void ) { return true; }
            // Update method for null class does nothing

        virtual bool GetCommStats( PORT_STATS& portStats ) { return false; }
            // Override base class functionality to return an error indication
    };


    // Real class to be used to communicate with actual device
    class TRealMgmtPort : public TMgmtPortDevice
    {
      protected:

        virtual void          Flush( void );

        virtual UnicodeString GetDevStatus( void );

      public:

        virtual __fastcall TRealMgmtPort();

        virtual bool Connect( const COMMS_CFG& portCfg );
        virtual void Disconnect( void );
            // Derived classes must implement these methods

        virtual bool Update( void );
    };

#endif
