#include <vcl.h>
#pragma hdrstop

#include "MovingAverage.h"

#pragma package(smart_init)

//
// This class implements an object that calculates a moving average
//

TMovingAverage::TMovingAverage( int nbrItems )
{
    // Validate param and create the holding array
    if( nbrItems <= 0 )
        nbrItems = 1;

    m_pDataArray = new float[nbrItems];

    m_maxItems = nbrItems;

    Reset();
}


__fastcall TMovingAverage::~TMovingAverage()
{
    delete [] m_pDataArray;
}


void TMovingAverage::Reset( void )
{
    for( int iItem = 0; iItem < m_maxItems; iItem++ )
        m_pDataArray[iItem] = 0.0;

    m_nbrItems = 0;
    m_avgValue = 0.0;
}


void TMovingAverage::AddValue( float newValue )
{
    // First, if the queue if full shift down all the items in the array
    if( m_nbrItems == m_maxItems )
    {
        for( int iItem = 0; iItem < m_maxItems - 1; iItem++ )
            m_pDataArray[iItem] = m_pDataArray[iItem+1];

        m_nbrItems = m_maxItems - 1;
    }

    // Add the new value to the queue
    m_pDataArray[m_nbrItems] = newValue;
    m_nbrItems++;

    // Update the average value
    m_avgValue = 0.0;

    for( int iItem = 0; iItem < m_nbrItems; iItem++ )
        m_avgValue += m_pDataArray[iItem];

    m_avgValue /= m_nbrItems;
}


