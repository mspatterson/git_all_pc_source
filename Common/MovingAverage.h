#ifndef MovingAverageH
#define MovingAverageH

    //
    // This class implements an object that calculates a moving average
    //

    class TMovingAverage : public TObject
    {
        private:

           float* m_pDataArray;
           int    m_maxItems;
           int    m_nbrItems;
           float  m_avgValue;

        public:
            virtual TMovingAverage( int nbrItems );
            virtual __fastcall ~TMovingAverage();

            void Reset( void );
                // Resets the moving average

            void AddValue( float newValue );
                // Adds a new value to the moving average

            __property float AverageValue = { read = m_avgValue };
            __property int   AverageSize  = { read = m_maxItems };
    };

#endif
