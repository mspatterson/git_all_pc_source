#include <vcl.h>
#pragma hdrstop

#include "NewBattDlg.h"
#include "WTTTSProtocolUtils.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TNewBattForm *NewBattForm;


__fastcall TNewBattForm::TNewBattForm(TComponent* Owner) : TForm(Owner)
{
    TypeCombo->Items->Clear();

    for( int iType = 0; iType < NBR_BATTERY_TYPES; iType++ )
        TypeCombo->Items->Add( GetBattTypeText( (BATTERY_TYPE)iType ) );

    TypeCombo->ItemIndex = BT_LITHIUM;
}


bool TNewBattForm::ShowDlg( BYTE& battType, WORD& capacity )
{
    // Create the form, set its props, and then show the dialog
    TNewBattForm* currForm = new TNewBattForm( NULL );

    bool okPressed = false;

    try
    {
        if( currForm->ShowModal() == mrOk )
        {
            battType = currForm->BattType;
            capacity = currForm->Capacity;

            okPressed = true;
        }
    }
    catch( ... )
    {
    }

    delete currForm;

    return okPressed;
}


void __fastcall TNewBattForm::FormShow(TObject *Sender)
{
    // On show, clear any previous settings
    TypeCombo->ItemIndex = 1;
    CapacityEdit->Text   = "0";
}


void __fastcall TNewBattForm::OKBtnClick(TObject *Sender)
{
    // Only validation required is for value entered in capacity edit
    int battCapacity = CapacityEdit->Text.ToIntDef( -1 );

    if( battCapacity <= 0 )
    {
        Beep();
        ActiveControl = CapacityEdit;
    }

    ModalResult = mrOk;
}


BYTE TNewBattForm::GetBattType( void )
{
    return TypeCombo->ItemIndex;
}


WORD TNewBattForm::GetBattCapacity( void )
{
    return CapacityEdit->Text.ToIntDef( 0 );
}

