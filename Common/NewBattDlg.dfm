object NewBattForm: TNewBattForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'New Battery Information'
  ClientHeight = 135
  ClientWidth = 230
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    230
    135)
  PixelsPerInch = 96
  TextHeight = 13
  object BattPropsGB: TGroupBox
    Left = 8
    Top = 8
    Width = 214
    Height = 87
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Battery Properties '
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 24
      Width = 24
      Height = 13
      Caption = 'Type'
    end
    object Label2: TLabel
      Left = 12
      Top = 56
      Width = 42
      Height = 13
      Caption = 'Capacity'
    end
    object Label3: TLabel
      Left = 172
      Top = 56
      Width = 21
      Height = 13
      Caption = 'mAh'
    end
    object TypeCombo: TComboBox
      Left = 67
      Top = 21
      Width = 93
      Height = 21
      Style = csDropDownList
      ItemIndex = 1
      TabOrder = 0
      Text = 'Lithium'
      Items.Strings = (
        'Unknown'
        'Lithium'
        'NiMH')
    end
    object CapacityEdit: TEdit
      Left = 67
      Top = 53
      Width = 93
      Height = 21
      NumbersOnly = True
      TabOrder = 1
      Text = '0'
    end
  end
  object OKBtn: TButton
    Left = 62
    Top = 103
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'O&K'
    Default = True
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 147
    Top = 103
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
