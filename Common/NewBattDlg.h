//---------------------------------------------------------------------------
#ifndef NewBattDlgH
#define NewBattDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TNewBattForm : public TForm
{
__published:	// IDE-managed Components
    TGroupBox *BattPropsGB;
    TButton *OKBtn;
    TButton *CancelBtn;
    TLabel *Label1;
    TLabel *Label2;
    TComboBox *TypeCombo;
    TEdit *CapacityEdit;
    TLabel *Label3;
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);

private:
    BYTE GetBattType( void );
    WORD GetBattCapacity( void );

public:
    __fastcall TNewBattForm(TComponent* Owner);

    static bool ShowDlg( BYTE& battType, WORD& capacity );

    __property BYTE BattType = { read = GetBattType };
    __property WORD Capacity = { read = GetBattCapacity };
};
//---------------------------------------------------------------------------
extern PACKAGE TNewBattForm *NewBattForm;
//---------------------------------------------------------------------------
#endif
