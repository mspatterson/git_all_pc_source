#include <vcl.h>
#pragma hdrstop

#include "PasswordInputDlg.h"
#include "ChangePasswordDlg.h"
#include "TesTorkManagerPWGen.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TPasswordInputForm *PasswordInputForm;


__fastcall TPasswordInputForm::TPasswordInputForm(TComponent* Owner) : TForm(Owner)
{
    // Clear any design time info from controls, and start with
    // error message not visitble.
    PWEdit->Text = "";
    PwdErrMsgLB->Visible = false ;

    // Clear new password result
    m_newPassword = "";
}


bool TPasswordInputForm::CheckPassword( const String& caption, const String& expectedPassword, String& newPassword )
{
    // Dynamically creates, then destroys, the password input dialog.
    // Returns true if the user enters a non-blank password and
    // presses OK, false otherwise.
    TPasswordInputForm* currForm = new TPasswordInputForm( NULL );

    bool okPressed = false;

    try
    {
        // Set the caption
        currForm->Caption = caption;

        // Save the expected password
        currForm->ExpectedPassword = expectedPassword;

        // Clear the possible new password return
        newPassword = "";

        if( currForm->ShowModal() == mrOk )
        {
            okPressed = true;

            newPassword = currForm->NewPassword;
        }
    }
    __finally
    {
        delete currForm;
    }

    return okPressed;
}


void __fastcall TPasswordInputForm::OkBtnClick(TObject *Sender)
{
    // Blank paaswords are not allowed
    if( PWEdit->Text.Trim().Length() == 0 )
    {
        MessageDlg( "Your password cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    // If the password entered was the backdoor password, force a password reset
    if( PWEdit->Text == GeneratePassword() )
    {
        if( ChangePasswordForm->ShowDlg( m_newPassword ) )
            ModalResult = mrOk;
        else
            ModalResult = mrCancel;

        return;
    }

    // Verify system admin password
    if( PWEdit->Text != m_expectedPassword )
    {
        // Select the bad password
        PWEdit->SelectAll();

        // Display error message
        PwdErrMsgLB->Visible = true;

        return;
    }


    // Fall through means correct password entered
    ModalResult = mrOk;
}
