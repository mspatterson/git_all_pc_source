object PasswordInputForm: TPasswordInputForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Enter Password'
  ClientHeight = 94
  ClientWidth = 311
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 13
    Top = 16
    Width = 50
    Height = 13
    Caption = 'Password:'
  end
  object PwdErrMsgLB: TLabel
    Left = 80
    Top = 40
    Width = 138
    Height = 13
    Caption = 'Incorrect password entered!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object PWEdit: TEdit
    Left = 80
    Top = 13
    Width = 217
    Height = 21
    PasswordChar = '*'
    TabOrder = 0
  end
  object OkBtn: TButton
    Left = 137
    Top = 63
    Width = 75
    Height = 25
    Caption = 'O&K'
    Default = True
    TabOrder = 1
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 222
    Top = 63
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
