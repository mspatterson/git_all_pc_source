#ifndef PasswordInputDlgH
#define PasswordInputDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>


class TPasswordInputForm : public TForm
{
__published:    // IDE-managed Components
    TLabel *Label1;
    TEdit *PWEdit;
    TButton *OkBtn;
    TButton *CancelBtn;
    TLabel *PwdErrMsgLB;
    void __fastcall OkBtnClick(TObject *Sender);

private:
    String m_expectedPassword;
    String m_newPassword;

protected:

    __property String ExpectedPassword = { read = m_expectedPassword, write = m_expectedPassword };
    __property String NewPassword      = { read = m_newPassword,      write = m_newPassword };

public:
    __fastcall TPasswordInputForm(TComponent* Owner);

    static bool CheckPassword( const String& caption, const String& expectedPassword, String& newPassword );
        // Dynamically creates and displays the form. Returns true if the password
        // entered matches expectedPassword. If true is returned and the password
        // was changed, newPassword will contain it. If newPassword is blank, then
        // the password was not changed

};

extern PACKAGE TPasswordInputForm *PasswordInputForm;

#endif
