#include <vcl.h>
#pragma hdrstop

#include "ProgressDialog.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TProgressDlg *ProgressDlg;


__fastcall TProgressDlg::TProgressDlg(TComponent* Owner) : TForm(Owner)
{
    bOkToClose = true;
}


void TProgressDlg::ShowProgressForm( const String& dlgCaption, bool canCancel )
{
    // Show the progress form modelessly.
    Caption = dlgCaption;

    // Set progress bar values - progress always shown in %
    ProgressBar1->Min      = 0;
    ProgressBar1->Position = 0;
    ProgressBar1->Max      = 100;

    PercentLabel->Caption  = "0%";
    MsgLabel->Caption      = "";

    // bOkToClose is set when the calling module wants to close this form
    bOkToClose = false;

    // CancelBtn tag is used to indicate if cancel has been pressed
    CancelBtn->Enabled = canCancel;
    CancelBtn->Tag     = 0;

    Show();
}


bool TProgressDlg::IsCancelled( void )
{
    return( CancelBtn->Tag == 1 );
}


void TProgressDlg::UpdateProgress( int newPos )
{
    if( newPos <= ProgressBar1->Max )
    {
        ProgressBar1->Position = newPos;
        PercentLabel->Caption  = IntToStr( newPos ) + "%";
    }

    Application->ProcessMessages();
}


void TProgressDlg::UpdateMsg( const String& newMsg )
{
    MsgLabel->Caption = newMsg;
    Application->ProcessMessages();
}


void TProgressDlg::CloseProgressForm( void )
{
    bOkToClose = true;
    Visible = false;
}


void __fastcall TProgressDlg::CancelBtnClick(TObject *Sender)
{
    CancelBtn->Tag = 1;
}


void __fastcall TProgressDlg::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    CanClose = bOkToClose;
}

