object ProgressDlg: TProgressDlg
  Left = 395
  Top = 306
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Caption'
  ClientHeight = 108
  ClientWidth = 349
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object PercentLabel: TLabel
    Left = 154
    Top = 29
    Width = 44
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = '0%'
  end
  object MsgLabel: TLabel
    Left = 15
    Top = 10
    Width = 15
    Height = 13
    Alignment = taCenter
    Caption = '     '
  end
  object ProgressBar1: TProgressBar
    Left = 15
    Top = 50
    Width = 321
    Height = 17
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 261
    Top = 76
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    OnClick = CancelBtnClick
  end
end
