//---------------------------------------------------------------------------
#ifndef ProgressDialogH
#define ProgressDialogH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TProgressDlg : public TForm
{
__published:	// IDE-managed Components
    TProgressBar *ProgressBar1;
    TButton *CancelBtn;
    TLabel *PercentLabel;
    TLabel *MsgLabel;
    void __fastcall CancelBtnClick(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);

private:
    bool bOkToClose;

public:
    __fastcall TProgressDlg(TComponent* Owner);

    void ShowProgressForm( const String& dlgCaption, bool canCancel );
    void CloseProgressForm( void );

    bool IsCancelled( void );
    void UpdateProgress( int newPos );
    void UpdateMsg( const String& newMsg );

    int  CurrPos( void ) { return ProgressBar1->Position; };
};
//---------------------------------------------------------------------------
extern PACKAGE TProgressDlg *ProgressDlg;
//---------------------------------------------------------------------------
#endif
