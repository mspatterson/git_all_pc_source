//************************************************************************
//
//  SerialPortStats.h: structures and helper functions for generic
//      serial port statistics
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "SerialPortUtils.h"
#include "Comm32.h"
#include "CUDPPort.h"
#include "USBPortDiscovery.h"

#pragma package(smart_init)


const UnicodeString CommTypeText[NBR_COMM_TYPES] = {
    L"USB",             // CT_USB
    L"Serial",          // CT_SERIAL
    L"UDP",             // CT_UDP
    L"Unused"           // CT_UNUSED
};


void ClearPortStats( PORT_STATS& portStats )
{
    portStats.tLastPkt   = 0;
    portStats.cmdsSent   = 0;
    portStats.respRecv   = 0;
    portStats.pktFlushes = 0;
    portStats.pktErrors  = 0;
    portStats.bytesSent  = 0;
    portStats.bytesRecv  = 0;
}


bool CreateCommPort( const COMMS_CFG& portCfg, DEVICE_PORT& subPort )
{
    // The following function creates a comm object from a passed COMMS_CFG struct
    // Winsock() must be initialized before calling this function. Function returns
    // a pointer to the comm object on success, NULL on failure.
    CCommObj* portObj = NULL;

    subPort.portObj    = NULL;
    subPort.connResult = CCommObj::ERR_COMM_OPEN_FAIL;

    subPort.stats.portType = CT_UNUSED;
    subPort.stats.portDesc = L"";

    ClearPortStats( subPort.stats );

    if( portCfg.portType == CT_UDP )
    {
        portObj = new CUDPPort();

        if( portObj != NULL )
        {
            CUDPPort::CONNECT_PARAMS connParams;

            // If the destAddr is blank, then we are opening a listening port
            // For legacy purposes, also check for the text "N/A"
            if( ( portCfg.portName.Length() == 0 ) || ( portCfg.portName.CompareIC( "N/A" ) == 0 ) )
            {
                // We are opening a socket at the given port number
                connParams.destAddr = "";
                connParams.destPort = 0;
                connParams.portNbr  = portCfg.portParam;
                connParams.acceptBroadcasts = true;
            }
            else
            {
                // We are given the address we want to be sending to
                connParams.destAddr = portCfg.portName;
                connParams.destPort = portCfg.portParam;
                connParams.portNbr  = 0;
                connParams.acceptBroadcasts = true;
            }

            subPort.connResult = portObj->Connect( &connParams );
        }
    }
    else if( ( portCfg.portType == CT_USB ) || ( portCfg.portType == CT_SERIAL ) )
    {
        // In both of these cases, we are opening a serial port. For USB though,
        // we find the comm port number using the USB name. Port number of 0
        // means device not found, < 0 means invalid port number
        int iPortNbr = 0;

        if( portCfg.portType == CT_USB )
        {
            AnsiString portStr;

            if( GetUSBPort( AnsiString( portCfg.portName ), portStr ) )
            {
                // Port number will be prefixed by "COM" - find that and extract
                // the port number.
                const AnsiString portText( "COM" );

                int commStrPos = portStr.Pos( portText );

                if( commStrPos > 0 )
                {
                    // Delete the leading "COM" chars, remaining chars should be a number
                    portStr = portStr.Delete( 1, commStrPos + portText.Length() - 1 );

                    iPortNbr = portStr.ToIntDef( -1 );
                }
            }
        }
        else
        {
            iPortNbr = portCfg.portName.ToIntDef( -1 );
        }

        if( iPortNbr > 0 )
        {
            portObj = new CCommPort();

            if( portObj != NULL )
            {
                CCommPort::CONNECT_PARAMS connParams;

                connParams.iPortNumber   = iPortNbr;
                connParams.dwLineBitRate = portCfg.portParam;

                subPort.connResult = portObj->Connect( &connParams );
            }
        }
        else if( iPortNbr == 0 )
        {
            subPort.connResultText = "Device not found";
        }
        else
        {
            subPort.connResultText = "Invalid port number";
        }
    }

    if( portObj != NULL )
    {
        switch( subPort.connResult )
        {
            case CCommObj::ERR_NONE:                 subPort.connResultText = "Success";           break;
            case CCommObj::ERR_COMM_SET_COMM_STATE:  subPort.connResultText = "Set state error";   break;
            case CCommObj::ERR_COMM_INV_BUFFER_LEN:  subPort.connResultText = "Set buffer error";  break;
            case CCommObj::ERR_COMM_OPEN_FAIL:       subPort.connResultText = "Open port error";   break;
            case CCommObj::ERR_COMM_GET_COMM_STATE:  subPort.connResultText = "Get state error";   break;
            default:                                 subPort.connResultText = "Error " + IntToStr( subPort.connResult );  break;
        }

        if( subPort.connResult != CCommObj::ERR_NONE )
        {
            delete portObj;
            portObj = NULL;
        }
    }

    subPort.portType = portCfg.portType;
    subPort.portObj  = portObj;

    // Init stats members that don't change
    subPort.stats.portType = portCfg.portType;
    subPort.stats.portDesc = GetCommPortDesc( subPort );

    if( portObj == NULL )
        return false;

    // Fall through means success
    return true;
}


void ReleaseCommPort( DEVICE_PORT& subPort )
{
    if( subPort.portObj != NULL )
    {
        subPort.portObj->Disconnect();

        delete subPort.portObj;
        subPort.portObj = NULL;
    }

    subPort.stats.portType = CT_UNUSED;
    subPort.stats.portDesc = L"";

    ClearPortStats( subPort.stats );
}


UnicodeString GetCommPortDesc( const DEVICE_PORT& subPort )
{
    UnicodeString sDesc;

    if( subPort.portObj != NULL )
    {
        sDesc = subPort.portObj->portName;

        // Serial ports will have a leading \\.\ string - delete that to make it prettier
        if( sDesc.Pos( "\\\\.\\" ) > 0 )
            sDesc = sDesc.Delete( 1, 4 );
    }
    else
    {
        // Port is not open - if the port is not used, this is not an error.
        // Otherwise, the port should be open.
        if( subPort.portType != CT_UNUSED )
            sDesc = L"(closed)";
    }

    return sDesc;
}

