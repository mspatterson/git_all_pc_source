//************************************************************************
//
//  SerialPortStats.h: structures and helper functions for generic
//      serial port statistics
//
//************************************************************************
#ifndef SerialPortUtilsH
#define SerialPortUtilsH

    #include <time.h>

    #include "Comm32.h"

    typedef enum {
        CT_USB,
        CT_SERIAL,
        CT_UDP,
        CT_UNUSED,
        NBR_COMM_TYPES
    } COMM_TYPE;

    extern const UnicodeString CommTypeText[NBR_COMM_TYPES];

    // Generic packet-oriented comms stats structure
    typedef struct {
        COMM_TYPE     portType;
        UnicodeString portDesc;
        time_t        tLastPkt;
        DWORD         cmdsSent;
        DWORD         respRecv;
        DWORD         pktFlushes;
        DWORD         pktErrors;
        DWORD         bytesSent;
        DWORD         bytesRecv;
    } PORT_STATS;

    void ClearPortStats( PORT_STATS& portStats );

    // Generic structure containing port information
    typedef struct {
        COMM_TYPE     portType;
        CCommObj*     portObj;
        PORT_STATS    stats;
        CCommObj::COMM_RESULT connResult;
        UnicodeString connResultText;
    } DEVICE_PORT;

    typedef struct {
        COMM_TYPE     portType;
        UnicodeString portName;    // "COMx", "USB", or IP dotted addres
        DWORD         portParam;   // bit rate or UDP port
    } COMMS_CFG;

    bool CreateCommPort( const COMMS_CFG& portCfg, DEVICE_PORT& subPort );
    void ReleaseCommPort( DEVICE_PORT& subPort );

    UnicodeString GetCommPortDesc( const DEVICE_PORT& subPort );

#endif

