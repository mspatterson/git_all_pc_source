//--------------------------------------------------------------------------
//
//  Specialized TCP Host-Side implementation that only supports
//  one client connection at a time.
//
//--------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <winsock2.h>

#include "TCPHost.h"

#pragma package(smart_init)


static WORD           m_listenPortNbr = 0;
static SOCKET         m_tcpListenSkt  = INVALID_SOCKET;
static SOCKET         m_tcpOpenSkt    = INVALID_SOCKET;
static TCP_HOST_STATE m_tcpState      = TS_NOT_LISTENING;

static UnicodeString  m_hostAddr;
static WORD           m_hostPort;

static UnicodeString  m_lastErrorMsg;

static void       CloseSocket( SOCKET* aSocket );
static AnsiString FormatIPAddr( BYTE anAddr[] );
static bool       CheckForConnectionRequests( void );
static bool       CheckForTCPData( PBYTE pRxBuffer, WORD buffLen, WORD& bytesRcvd );
static void       PurgePendingConnectRequests( void );


//
// Public Data
//

const UnicodeString HostTCPStateText[NBR_TCP_HOST_STATES] =
{
    "Not Listening",
    "Listening",
    "Connected",
};


//
// Public Functions
//

bool OpenHostTCPListeningPort( WORD listeningPortNbr )
{
    CloseHostTCPListeningPort();

    m_tcpListenSkt = socket( PF_INET, SOCK_STREAM, 0 );

    if( m_tcpListenSkt == SOCKET_ERROR )
    {
        m_lastErrorMsg = "Could not create source socket, error " + IntToStr( WSAGetLastError() ) + "!";
        return false;
    }

    // Set socket to be non-blocking
    unsigned long nonblocking = 1;

    if( ioctlsocket( m_tcpListenSkt, FIONBIO, &nonblocking ) == SOCKET_ERROR )
    {
        CloseSocket( &m_tcpListenSkt );

        m_lastErrorMsg = "Could not set socket to be non-blocking, error " + IntToStr( WSAGetLastError() ) + "!";
        return false;
    }

    // Bind socket to port
    struct sockaddr_in SrcAddr;
    ZeroMemory( &SrcAddr, sizeof( SrcAddr ) );

    SrcAddr.sin_family      = AF_INET;
    SrcAddr.sin_addr.s_addr = INADDR_ANY;
    SrcAddr.sin_port        = htons( listeningPortNbr );

    if( bind( m_tcpListenSkt, (struct sockaddr *)&SrcAddr, sizeof(SrcAddr) ) == SOCKET_ERROR )
    {
        CloseSocket( &m_tcpListenSkt );

        m_lastErrorMsg = "Could not bind socket to port, error " + IntToStr( WSAGetLastError() ) + "!";
        return false;
    }

    // Finally, set the socket to listen for incoming connections.
    if( listen( m_tcpListenSkt, 100 /* max backlog */ ) == SOCKET_ERROR )
    {
        CloseSocket( &m_tcpListenSkt );

        m_lastErrorMsg = "Winsock error: could not start listening on socket, error " + IntToStr( WSAGetLastError() ) + "!";
        return false;
    }

    m_tcpState = TS_LISTENING;
    m_lastErrorMsg = "Listening on port " + IntToStr( listeningPortNbr );

    m_listenPortNbr = listeningPortNbr;

    return true;
}


void CloseHostTCPListeningPort( void )
{
    // Be sure the other side knows we've hung up
    CloseSocket( &m_tcpListenSkt );
    CloseSocket( &m_tcpOpenSkt );

    m_hostAddr = "";
    m_hostPort = 0;

    m_tcpState = TS_NOT_LISTENING;
}


UnicodeString GetHostTCPPortLastError( void )
{
    return m_lastErrorMsg;
}


TCP_HOST_STATE GetHostTCPPortState( void )
{
    return m_tcpState;
}


TCP_HOST_STATE UpdateHostTCPPort( PBYTE pRxBuffer, WORD buffLen, WORD& bytesRcvd )
{
    bytesRcvd = 0;

    switch( m_tcpState )
    {
        case TS_NOT_LISTENING:
            // Listening port not opened. Do nothing
            break;

        case TS_LISTENING:
            // Listening port open. Check for connection requests
            if( CheckForConnectionRequests() )
                m_tcpState = TS_CONNECTED;
            break;

        case TS_CONNECTED:

            // Currently connected. Check for data or lost connection
            if( !CheckForTCPData( pRxBuffer, buffLen, bytesRcvd ) )
                CloseHostTCPDataPort();

            // While connected, we do not accept any other connections
            PurgePendingConnectRequests();

            break;
    }

    return m_tcpState;
}


UnicodeString GetHostTCPClientAddr( void )
{
    if( m_tcpState == TS_CONNECTED )
        return m_hostAddr + ":" + IntToStr( (int)m_hostPort );
    else
        return "(not connected)";
}


int SendHostTCPBuffer( PBYTE pBuffer, WORD buffLen )
{
    if( m_tcpOpenSkt == INVALID_SOCKET )
        return 0;

    int bytesSent = send( m_tcpOpenSkt, pBuffer, buffLen, 0 );

    if( bytesSent == SOCKET_ERROR )
    {
        int sockErr = WSAGetLastError();

        switch( sockErr )
        {
            case WSAECONNRESET:
                // Lost connection
                break;

            default:
                // Any other error is also a bad thing.
                break;
        }

        // Close the port on error
        CloseHostTCPDataPort();

        return 0;
    }

    return bytesSent;
}


void CloseHostTCPDataPort( void )
{
    CloseSocket( &m_tcpOpenSkt );

    if( m_tcpListenSkt == INVALID_SOCKET )
        m_tcpState = TS_NOT_LISTENING;
    else
        m_tcpState = TS_LISTENING;
}


//
// Private Functions
//

void CloseSocket( SOCKET* aSocket )
{
    // Force a hard close on the socket.
    if( *aSocket != INVALID_SOCKET )
    {
        linger liSettings;

        liSettings.l_onoff  = 1;        // Turn linger on
        liSettings.l_linger = 0;        // Set timeout to 0

        setsockopt( *aSocket, SOL_SOCKET, SO_LINGER,
                   (const char FAR *)(&liSettings), sizeof( linger )
                  );

        closesocket( *aSocket );
        *aSocket = INVALID_SOCKET;
    }
}


AnsiString FormatIPAddr( BYTE anAddr[] )
{
    AnsiString sResult;

    sResult.printf( "%.2u.%.2u.%.2u.%.2u", anAddr[0], anAddr[1], anAddr[2], anAddr[3] );

    return sResult;
}


void PurgePendingConnectRequests( void )
{
    // Check for new connection requests. If one is pending,
    // accept then close the socket immediately.
    while( 1 )
    {
        sockaddr fromAddr;
        int      fromLen = sizeof( sockaddr );

        SOCKET sNew = accept( m_tcpListenSkt, &fromAddr, &fromLen );

        if( sNew == INVALID_SOCKET )
            break;

        CloseSocket( &sNew );
    }
}


bool CheckForConnectionRequests( void )
{
    SOCKET   sNew;
    sockaddr fromAddr;
    int      fromLen = sizeof( sockaddr );

    // Check for new connection requests. We only support one
    // open socket at a time, so if tcpOpenSkt is valid, then
    // don't check for a new socket.
    if( m_tcpOpenSkt == INVALID_SOCKET )
    {
        sNew = accept( m_tcpListenSkt, &fromAddr, &fromLen );

        if( sNew != INVALID_SOCKET )
        {
            // Someone's calling us! Remember this socket for later use
            m_tcpOpenSkt = sNew;

            // Remember the caller's info
            sockaddr_in* pInAddr = (sockaddr_in*)&fromAddr;

            m_hostPort = ntohs( pInAddr->sin_port );
            m_hostAddr = inet_ntoa( pInAddr->sin_addr );

            // Increase the default buffer size for the socket. Windows
            // default is 8192. We need room for 3 packets at about 6K
            // bytes each. Set the buffer size to 32K.
            int txBuffSize = 32768;

            if( setsockopt( m_tcpOpenSkt, SOL_SOCKET, SO_SNDBUF, (char *)&txBuffSize, sizeof(txBuffSize) ) == SOCKET_ERROR )
            {
                // Don't close the socket if this function fails for now
                Beep();
            }

            return true;
        }
    }

    // Fall through means no connection pending
    return false;
}


bool CheckForTCPData( PBYTE pRxBuffer, WORD buffLen, WORD& bytesRcvd )
{
    // If the TCP socket is open, check for data. Return true while the
    // port appears to be open, whether data has been received or not.

    // Init return vars
    bytesRcvd = 0;

    // Must have a valid socket to do this
    if( m_tcpOpenSkt == INVALID_SOCKET )
        return false;

    if( ( buffLen == 0 ) || ( pRxBuffer == NULL ) )
        return false;

    // Try to receive data now
    int rxResult = recv( m_tcpOpenSkt, pRxBuffer, buffLen, 0 );

    // If rxResult is zero, assume the far end has closed the connection.
    if( rxResult == 0 )
    {
        CloseHostTCPDataPort();
        return false;
    }

    // Positive number indicates data received
    if( rxResult > 0 )
    {
        bytesRcvd = rxResult;
        return true;
    }

    // Fall through means an error occurred
    bool socketStillGood = true;

    int sockErr = WSAGetLastError();

    switch( sockErr )
    {
        case WSAEWOULDBLOCK:
            // No data waiting for us.
            break;

        case WSAEMSGSIZE:
            // Data received, but truncated!
            bytesRcvd = buffLen;
            break;

        case WSAECONNRESET:
            // Lost connection to listener (server)
            socketStillGood = false;
            break;

        default:
            // Treat any other error as a bad thing.
            socketStillGood = false;
            break;
    }

    if( !socketStillGood )
    {
        CloseHostTCPDataPort();
        return false;
    }

    // Fall through means socket still valid
    return true;
}

