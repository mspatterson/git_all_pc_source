//--------------------------------------------------------------------------
//
//  Specialized TCP Host-Side implementation that only supports
//  one client at a time.
//
//--------------------------------------------------------------------------
#ifndef TCPHostH
#define TCPHostH

    bool OpenHostTCPListeningPort( WORD listeningPortNbr );
    void CloseHostTCPListeningPort( void );

    String GetHostTCPPortLastError( void );

    typedef enum {
        TS_NOT_LISTENING,
        TS_LISTENING,
        TS_CONNECTED,
        NBR_TCP_HOST_STATES
    } TCP_HOST_STATE;

    extern const String HostTCPStateText[];

    TCP_HOST_STATE GetHostTCPPortState( void );
    TCP_HOST_STATE UpdateHostTCPPort( PBYTE pRxBuffer, WORD buffLen, WORD& bytesRcvd );

    String GetHostTCPClientAddr( void );

    int  SendHostTCPBuffer( PBYTE pBuffer, WORD buffLen );
    void CloseHostTCPDataPort( void );

#endif
