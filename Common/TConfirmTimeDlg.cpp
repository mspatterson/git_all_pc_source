#include <vcl.h>
#pragma hdrstop

#include "TConfirmTimeDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TConfirmTimeDlg *ConfirmTimeDlg;


__fastcall TConfirmTimeDlg::TConfirmTimeDlg(TComponent* Owner) : TForm(Owner)
{
    m_bBtnPressed = false;
}


TModalResult TConfirmTimeDlg::ShowConfimTimeDlg( TComponent* Owner )
{
    TConfirmTimeDlg* pDlg = NULL;

    TModalResult mrResult = mrNo;

    try {
        pDlg = new TConfirmTimeDlg( Owner );

        mrResult = pDlg->ShowModal();
    }
    catch( ... )
    {
    }

    if( pDlg != NULL )
        delete pDlg;

    return mrResult;
}


void __fastcall TConfirmTimeDlg::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    // Can only close if a user has pressed a button
    CanClose = m_bBtnPressed;

    if( !CanClose )
        Beep();
}


void __fastcall TConfirmTimeDlg::DateTimeCorrectBtnClick(TObject *Sender)
{
    m_bBtnPressed = true;
    ModalResult   = mrYes;
}


void __fastcall TConfirmTimeDlg::DateTimeIncorrectBtnClick(TObject *Sender)
{
    m_bBtnPressed = true;
    ModalResult   = mrNo;
}


void __fastcall TConfirmTimeDlg::UpdateTimerTimer(TObject *Sender)
{
    // On each timer tick, update the display panels
    DatePanel->Caption = Now().DateString();
    TimePanel->Caption = Now().TimeString();
}

