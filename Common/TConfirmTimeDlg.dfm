object ConfirmTimeDlg: TConfirmTimeDlg
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Confirm System Time'
  ClientHeight = 330
  ClientWidth = 537
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    537
    330)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 484
    Height = 19
    Alignment = taCenter
    AutoSize = False
    Caption = 'Please confirm the date and time displayed below are correct. '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 40
    Width = 484
    Height = 19
    Alignment = taCenter
    AutoSize = False
    Caption = 'If they are not correct. please update the computer'#39's date and '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 24
    Top = 64
    Width = 484
    Height = 19
    Alignment = taCenter
    AutoSize = False
    Caption = 'time before logging a job.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 24
    Top = 88
    Width = 484
    Height = 19
    Alignment = taCenter
    AutoSize = False
    Caption = 'You may need to be an Administrator to do that.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DatePanel: TPanel
    Left = 24
    Top = 129
    Width = 225
    Height = 41
    Caption = 'Oct 31, 2018'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object TimePanel: TPanel
    Left = 282
    Top = 129
    Width = 226
    Height = 41
    Caption = '07:00:00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object DateTimeCorrectBtn: TButton
    Left = 120
    Top = 199
    Width = 299
    Height = 49
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'Date and Time are Correct'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = DateTimeCorrectBtnClick
    ExplicitTop = 189
    ExplicitWidth = 289
  end
  object DateTimeIncorrectBtn: TButton
    Left = 120
    Top = 263
    Width = 299
    Height = 49
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'Date and Time are NOT Correct'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = DateTimeIncorrectBtnClick
    ExplicitTop = 253
    ExplicitWidth = 289
  end
  object UpdateTimer: TTimer
    Interval = 100
    OnTimer = UpdateTimerTimer
    Left = 472
    Top = 264
  end
end
