//---------------------------------------------------------------------------
#ifndef TConfirmTimeDlgH
#define TConfirmTimeDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TConfirmTimeDlg : public TForm
{
__published:	// IDE-managed Components
    TLabel *Label1;
    TLabel *Label2;
    TPanel *DatePanel;
    TPanel *TimePanel;
    TButton *DateTimeCorrectBtn;
    TButton *DateTimeIncorrectBtn;
    TLabel *Label3;
    TLabel *Label4;
    TTimer *UpdateTimer;
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
    void __fastcall DateTimeCorrectBtnClick(TObject *Sender);
    void __fastcall DateTimeIncorrectBtnClick(TObject *Sender);
    void __fastcall UpdateTimerTimer(TObject *Sender);
private:	// User declarations
    bool m_bBtnPressed;

public:		// User declarations
    __fastcall TConfirmTimeDlg(TComponent* Owner);

    static TModalResult ShowConfimTimeDlg( TComponent* Owner );
        // Creates the dialog and returns either mrYes or mrNo.
};
//---------------------------------------------------------------------------
extern PACKAGE TConfirmTimeDlg *ConfirmTimeDlg;
//---------------------------------------------------------------------------
#endif
