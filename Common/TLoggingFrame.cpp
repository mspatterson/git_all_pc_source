#include <vcl.h>
#pragma hdrstop

#include <Clipbrd.hpp>

#include "TLoggingFrame.h"


#pragma package(smart_init)
#pragma resource "*.dfm"


TLoggingFrame *LoggingFrame;


static const int MaxNbrMemoLines = 1000;


__fastcall TLoggingFrame::TLoggingFrame( TComponent* pOwner, TWinControl* pParent, const String& sCompName )
               : TFrame(pOwner)
{
    // Set basic properties first
    Parent = pParent;
    Name   = sCompName;

    // Make the paint box opaque for better performance
    ControlStyle << csOpaque;
    MemoPaintBox->ControlStyle << csOpaque;

    // The m_bUpdating controls whether we are updating the display. Start
    // with it false (eg, not in the middle of an update)
    m_bUpdating = false;

    // Reset longest line length
    m_iLongestLineLen = 0;

    // Start with the scrollbars disabled
    MemoVtScrollBar->Enabled = false;

    // Create the string list container
    m_pMemoStrings = new TStringList();
}


__fastcall TLoggingFrame::~TLoggingFrame( void )
{
    delete m_pMemoStrings;
}


void TLoggingFrame::AddLogMessage( const String& sMessage )
{
    String sOutputMsg = Now().TimeString() + " (" + UIntToStr( (UINT)GetTickCount() ) + ") " + sMessage;

    // Limit the number of strings in the log strings
    while( m_pMemoStrings->Count > MaxNbrMemoLines )
        m_pMemoStrings->Delete( 0 );

    m_pMemoStrings->Add( sOutputMsg );

    if( sOutputMsg.Length() > m_iLongestLineLen )
        m_iLongestLineLen = sOutputMsg.Length();

    if( PauseBtn->Caption == "Pause" )
        RefreshMemo();

    // Copy to console if selected
    if( CopyToConsoleCB->Checked )
        OutputDebugString( sOutputMsg.w_str() );

    // Output to file if selected
    if( LogToFileCB->Checked )
    {
        if( LogFileNameEdit->Text.Length() > 0 )
            LogLineToFile( LogFileNameEdit->Text, sOutputMsg );
    }
}


void TLoggingFrame::RefreshMemo( void )
{
    if( !m_bUpdating )
    {
        SetScrollbarParams();
        MemoPaintBox->Invalidate();
    }
}


void TLoggingFrame::SetScrollbarParams( void )
{
    int textHeight = MemoPaintBox->Canvas->TextHeight( "X" );
    int textWidth  = MemoPaintBox->Canvas->TextWidth( "X" );

    if( textHeight == 0 )
    {
        // Should never happen
        MemoVtScrollBar->Enabled = false;
        MemoVtScrollBar->SetParams( 0, 0, 100 );
    }
    else
    {
        int nbrVisibleLines = MemoPaintBox->Height / textHeight;

        if( nbrVisibleLines < m_pMemoStrings->Count )
        {
            MemoVtScrollBar->SetParams( MemoVtScrollBar->Position, 0, m_pMemoStrings->Count );
            MemoVtScrollBar->PageSize    = nbrVisibleLines;
            MemoVtScrollBar->LargeChange = nbrVisibleLines;

            MemoVtScrollBar->Enabled = true;
        }
        else
        {
            MemoVtScrollBar->Enabled = false;
            MemoVtScrollBar->SetParams( 0, 0, 100 );
        }
    }

    int iWidestString = textWidth * m_iLongestLineLen;

    if( iWidestString < MemoPaintBox->Width )
    {
        MemoHzScrollBar->Enabled = false;
        MemoHzScrollBar->SetParams( 0, 0, 100 );
    }
    else
    {
        MemoHzScrollBar->Enabled = true;
        MemoHzScrollBar->SetParams( MemoHzScrollBar->Position, 0, iWidestString * 10 );

        MemoHzScrollBar->PageSize    = 10;
        MemoHzScrollBar->LargeChange = 100;
    }
}


void __fastcall TLoggingFrame::MemoPaintBoxPaint(TObject *Sender)
{
    // Determine the range of strings to print
    int imageWidth  = MemoPaintBox->Width;
    int imageHeight = MemoPaintBox->Height;

    TCanvas* bmpCanvas = MemoPaintBox->Canvas;

    int iFirstLine = 0;
    int iLastLine  = 0;   // Output up to, but not including, this line index

    if( MemoVtScrollBar->Enabled )
    {
        // Watch for timing issues here - it is possible for the scrollbar
        // to have been disabled but this property not yet be updated.
        iFirstLine = MemoVtScrollBar->Position;
        iLastLine  = iFirstLine + MemoVtScrollBar->PageSize;

        if( iLastLine > m_pMemoStrings->Count )
            iLastLine = m_pMemoStrings->Count;

        if( iFirstLine > iLastLine )
        {
            if( iLastLine > 0 )
                iFirstLine = iLastLine - 1;
            else
                iFirstLine = 0;
        }
    }
    else
    {
        iFirstLine = 0;
        iLastLine  = m_pMemoStrings->Count;
    }

    // Create a temp bmp to work with
    TBitmap* pTempBmp = new TBitmap();

    pTempBmp->Width  = MemoPaintBox->Width;
    pTempBmp->Height = MemoPaintBox->Height;

    HDC targetDC = pTempBmp->Canvas->Handle;

    // Erase the background
    pTempBmp->Canvas->Brush->Color = clWindow;
    pTempBmp->Canvas->Brush->Style = bsSolid;

    pTempBmp->Canvas->Pen->Color = clBtnFace;
    pTempBmp->Canvas->Pen->Style = psSolid;
    pTempBmp->Canvas->Pen->Width = 1;

    pTempBmp->Canvas->FillRect( TRect( 0, 0, pTempBmp->Width, pTempBmp->Height ) );

    // Now draw all lines
    int iLineHeight = bmpCanvas->TextHeight( "X" );

    RECT textRect;

    textRect.left   = -MemoHzScrollBar->Position;
    textRect.top    = 0;
    textRect.bottom = iLineHeight;
    textRect.right  = imageWidth + MemoHzScrollBar->Position;

    for( int iLine = iFirstLine; iLine < iLastLine; iLine++ )
    {
        String sTextLine = m_pMemoStrings->Strings[iLine];

        DrawText( targetDC, sTextLine.w_str(), sTextLine.Length(), &textRect, DT_SINGLELINE | DT_VCENTER );

        textRect.top    += iLineHeight;
        textRect.bottom += iLineHeight;
    }

    // Copy the temp bmp to the screen
    bmpCanvas->Draw( 0, 0, pTempBmp );

    // Done with the temp bmp
    delete pTempBmp;
}


void __fastcall TLoggingFrame::MemoScrollBarChange(TObject *Sender)
{
    MemoPaintBox->Invalidate();
}


bool TLoggingFrame::LogLineToFile( const String& sFullName, const String& sLine )
{
    // Attempt to write to the log file.
    TFileStream* logStream = NULL;
    bool         logGood   = false;

    AnsiString sALine = sLine + "\r\n";

    try
    {
        if( !FileExists( sFullName ) )
        {
            // File doesn't exist. Create it and write the header.
            logStream = new TFileStream( sFullName, fmCreate );
        }
        else
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( sFullName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }

        logStream->Write( sALine.c_str(), sALine.Length() );

        logGood = true;
    }
    catch( ... )
    {
        logGood = false;
    }

    // Close the file (if it is open).
    if( logStream != NULL )
        delete logStream;

    return logGood;
}


bool TLoggingFrame::LogDirectoryExists( const String& sLogDirectory )
{
    if( sLogDirectory == "" )
        return false;

    if( DirectoryExists( sLogDirectory ) == false )
    {
        try
        {
            ForceDirectories( sLogDirectory );
        }
        catch( ... )
        {
        }
    }

    return DirectoryExists( sLogDirectory );
}


void __fastcall TLoggingFrame::CloseBtnClick(TObject *Sender)
{
    if( m_OnCloseRequest )
        m_OnCloseRequest( this );
}


void __fastcall TLoggingFrame::BrowseLogFileBtnClick(TObject *Sender)
{
    if( FileSaveDlg->Execute() )
        LogFileNameEdit->Text = FileSaveDlg->FileName;
}


void __fastcall TLoggingFrame::CopyToClipBtnClick(TObject *Sender)
{
    TStringList* pCopyStrings = new TStringList();

    pCopyStrings->Assign( m_pMemoStrings );

    // Limit the amount of text copies to the last 32000 bytes
    while( pCopyStrings->Text.Length() > 32000 )
        pCopyStrings->Delete( 0 );

    // Now copy the text to the clipboard
    TClipboard *pClipBoard = Clipboard();

    pClipBoard->AsText = pCopyStrings->Text;

    // Clean-up
    delete pCopyStrings;
}


void __fastcall TLoggingFrame::ClearBtnClick(TObject *Sender)
{
    m_pMemoStrings->Clear();
    m_iLongestLineLen = 0;

    RefreshMemo();
}


void __fastcall TLoggingFrame::PauseBtnClick(TObject *Sender)
{
    if( PauseBtn->Caption == "Pause" )
    {
        PauseBtn->Caption = "Resume";
    }
    else
    {
        PauseBtn->Caption = "Pause";
        RefreshMemo();
    }
}


void TLoggingFrame::SetEnableCloseBtn( bool bIsEnabled )
{
    CloseBtn->Enabled = bIsEnabled;
    CloseBtn->Visible = bIsEnabled;
}

