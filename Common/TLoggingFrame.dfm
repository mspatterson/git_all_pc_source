object LoggingFrame: TLoggingFrame
  Left = 0
  Top = 0
  Width = 615
  Height = 470
  TabOrder = 0
  DesignSize = (
    615
    470)
  object MemoPaintBox: TPaintBox
    Left = 8
    Top = 89
    Width = 583
    Height = 318
    Anchors = [akLeft, akTop, akRight, akBottom]
    OnPaint = MemoPaintBoxPaint
  end
  object OptionsGB: TGroupBox
    Left = 8
    Top = 8
    Width = 600
    Height = 75
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Options '
    TabOrder = 0
    DesignSize = (
      600
      75)
    object CopyToConsoleCB: TCheckBox
      Left = 12
      Top = 20
      Width = 145
      Height = 17
      Caption = 'Copy to Debug Console'
      TabOrder = 0
    end
    object LogToFileCB: TCheckBox
      Left = 12
      Top = 47
      Width = 141
      Height = 17
      Caption = 'Log to File'
      TabOrder = 1
    end
    object LogFileNameEdit: TEdit
      Left = 152
      Top = 45
      Width = 387
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 2
      ExplicitWidth = 336
    end
    object BrowseLogFileBtn: TButton
      Left = 550
      Top = 43
      Width = 43
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 3
      OnClick = BrowseLogFileBtnClick
      ExplicitLeft = 499
    end
  end
  object PauseBtn: TButton
    Left = 8
    Top = 436
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Pause'
    TabOrder = 1
    OnClick = PauseBtnClick
  end
  object CopyToClipBtn: TButton
    Left = 91
    Top = 436
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Copy to Clip'
    TabOrder = 2
    OnClick = CopyToClipBtnClick
  end
  object ClearBtn: TButton
    Left = 172
    Top = 436
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Clear'
    TabOrder = 3
    OnClick = ClearBtnClick
  end
  object CloseBtn: TButton
    Left = 533
    Top = 436
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    TabOrder = 4
    OnClick = CloseBtnClick
  end
  object MemoHzScrollBar: TScrollBar
    Left = 8
    Top = 408
    Width = 583
    Height = 17
    Anchors = [akLeft, akRight, akBottom]
    PageSize = 0
    TabOrder = 5
    OnChange = MemoScrollBarChange
  end
  object MemoVtScrollBar: TScrollBar
    Left = 591
    Top = 89
    Width = 17
    Height = 318
    Anchors = [akTop, akRight, akBottom]
    Kind = sbVertical
    PageSize = 0
    TabOrder = 6
    OnChange = MemoScrollBarChange
  end
  object FileSaveDlg: TFileSaveDialog
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'Text Files (*.txt)'
        FileMask = '*.txt'
      end
      item
        DisplayName = 'Log Files (*.log)'
        FileMask = '*.log'
      end
      item
        DisplayName = 'All Files (*.*)'
        FileMask = '*.*'
      end>
    Options = [fdoOverWritePrompt, fdoPickFolders, fdoPathMustExist]
    Title = 'Save Log File As'
    Left = 500
    Top = 8
  end
end
