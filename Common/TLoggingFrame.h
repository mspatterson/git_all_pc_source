//---------------------------------------------------------------------------
#ifndef TLoggingFrameH
#define TLoggingFrameH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TLoggingFrame : public TFrame
{
__published:	// IDE-managed Components
    TPaintBox *MemoPaintBox;
    TGroupBox *OptionsGB;
    TCheckBox *CopyToConsoleCB;
    TCheckBox *LogToFileCB;
    TEdit *LogFileNameEdit;
    TButton *BrowseLogFileBtn;
    TButton *PauseBtn;
    TButton *CopyToClipBtn;
    TButton *ClearBtn;
    TButton *CloseBtn;
    TScrollBar *MemoHzScrollBar;
    TScrollBar *MemoVtScrollBar;
    TFileSaveDialog *FileSaveDlg;
    void __fastcall CloseBtnClick(TObject *Sender);
    void __fastcall BrowseLogFileBtnClick(TObject *Sender);
    void __fastcall CopyToClipBtnClick(TObject *Sender);
    void __fastcall ClearBtnClick(TObject *Sender);
    void __fastcall PauseBtnClick(TObject *Sender);
    void __fastcall MemoPaintBoxPaint(TObject *Sender);
    void __fastcall MemoScrollBarChange(TObject *Sender);

private:

    String    sLogFileName;
    int       iLastLogTime;
    TDateTime tLogStartTime;

    bool      m_bUpdating;
        // True while the log memo is being updated

    int       m_iLongestLineLen;
        // Chars in longest line shown in log memo

    TStringList* m_pMemoStrings;
        // Strings currently being displayed in the memo

    TNotifyEvent m_OnCloseRequest;
        // Event called if Close button is clicked

    void RefreshMemo( void );
    void SetScrollbarParams( void );
        // Memo update methods

    bool GetLogToFile( void )        { return LogToFileCB->Checked;   }
    void SetLogToFile( bool bLogOn ) { LogToFileCB->Checked = bLogOn; }

    bool GetEnableCloseBtn( void )   { return CloseBtn->Visible; }
    void SetEnableCloseBtn( bool bIsEnabled );

public:
    __fastcall TLoggingFrame( TComponent* pOwner, TWinControl* pParent, const String& sCompName );
    virtual __fastcall ~TLoggingFrame( void );

    void AddLogMessage( const String& sMessage );

    __property bool LogToFile = { read = GetLogToFile, write = SetLogToFile };
        // Get / set to enable / disable logging of added messages to file.
        // This property does not affect calls to LogToFile.

    __property TDateTime LogStartTime = { read = tLogStartTime, write = tLogStartTime };
        // This date/time is used to generate the log file name for new logs

    __property bool EnableCloseBtn = { read = GetEnableCloseBtn, write = SetEnableCloseBtn };
        // Gets / sets the option of displaying a Close button

    __property TNotifyEvent OnClose = { read = m_OnCloseRequest, write = m_OnCloseRequest };
        // Event that is fired if Close btn is visible and has been clicked

    //
    // Generic file logging support utilities
    //

    static bool LogDirectoryExists( const String& sLogDirectory );
        // Checks for the passed dir. Attempts to create it if the dir
        // does not exist. Returns true if the dir exists (or was created)

    static bool LogLineToFile( const String& sFullName, const String& sLine );
        // Writes a line to the passed log file. Returns true on success

};
//---------------------------------------------------------------------------
extern PACKAGE TLoggingFrame *LoggingFrame;
//---------------------------------------------------------------------------
#endif
