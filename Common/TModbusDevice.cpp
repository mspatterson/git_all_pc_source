#include <vcl.h>
#pragma hdrstop

#include <IniFiles.hpp>

#include "TModbusDevice.h"
#include "MODBUSPktHelpers.h"
#include "TypeDefs.h"
#include "Comm32.h"
#include "CUDPPort.h"
#include "CTCPServerPort.h"
#include "ApplUtils.h"
#include "Logging.h"

#pragma package(smart_init)


//
// Private Functions
//

static void TrimStrings( TStrings* aList )
{
    // Helper function that trims all of the strings in the passed list
    for( int iString = 0; iString < aList->Count; iString++ )
        aList->Strings[iString] = Trim( aList->Strings[iString] );
}


//
// The Modbus register description uses an ini-file format. All registers
// are defined in one section with up to 6 entries per reg
//

const String RegSection( "Registers" );           // All registers are in this section
const String RegStartEntry  ( "Reg%d_Start" );     // Mandatory
const String RegEnumEntry   ( "Reg%d_Enum" );      // Mandatory
const String RegTypeEntry   ( "Reg%d_Type" );      // Mandatory, a MODBUS_REG_TYPE enum value
const String RegSizeEntry   ( "Reg%d_Size" );      // Optional, number of bits that make up the reg, default = 16; Can only be 16 or 32
const String RegAccessEntry ( "Reg%d_RDWR" );      // Optional, default = RW
const String RegDescEntry   ( "Reg%d_Name" );      // Optional, default = ""
const String RegUnitsEntry  ( "Reg%d_Units" );     // Optional, default = ""
const String RegDefaultEntry( "Reg%d_Default" );   // Optional, default = "", interpreted according to reg type
const String RegPrecEntry   ( "Reg%d_Prec" );      // Optional, precision in digits, default = 0


static int GetDeviceRegDefs( const String& devLayoutFile, MODBUS_REG_DEFINITION* pDevRegs, int maxRegs )
{
    // Returns the registers defined in the passed device layout file.
    // Up to maxRegs of entries are populated and function returns number
    // of entries actually populated. If regRanges is NULL, maxRegs is
    // ignored and the function returns the number of regRange items required.
    if( !FileExists( devLayoutFile ) )
         return 0;

    TIniFile* layoutIni = NULL;

    int nbrRegs = 0;

    try
    {
        layoutIni = new TIniFile( devLayoutFile );

        // Ranges must be entered starting with 1 and without any gaps.
        while( true )
        {
            String regStartEntry;
            String regEnumEntry;
            String regDescEntry;
            String regSizeEntry;
            String regTypeEntry;
            String regRWEntry;
            String regUnitsEntry;
            String regDefEntry;
            String regPrecEntry;

            int currReg = nbrRegs + 1;

            regStartEntry.printf ( RegStartEntry.w_str(),   currReg );
            regEnumEntry.printf  ( RegEnumEntry.w_str(),    currReg );
            regDescEntry.printf  ( RegDescEntry.w_str(),    currReg );
            regSizeEntry.printf  ( RegSizeEntry.w_str(),    currReg );
            regTypeEntry.printf  ( RegTypeEntry.w_str(),    currReg );
            regRWEntry.printf    ( RegAccessEntry.w_str(),  currReg );
            regUnitsEntry.printf ( RegUnitsEntry.w_str(),   currReg );
            regDefEntry.printf   ( RegDefaultEntry.w_str(), currReg );
            regPrecEntry.printf  ( RegPrecEntry.w_str(),    currReg );

            // The following items are mandatory
            int regStart = layoutIni->ReadInteger( RegSection, regStartEntry, -1 );
            int regEnum  = layoutIni->ReadInteger( RegSection, regEnumEntry,  -1 );
            int regType  = layoutIni->ReadInteger( RegSection, regTypeEntry,  MRT_UNKNOWN );

            if( ( regStart < 0 ) || ( regEnum < 0 ) )
                break;

            // Validate reg type enum
            DWORD sizeofUserReg = 0;

            switch( regType )
            {
                case MRT_BOOL:    sizeofUserReg =  1;    break;
                case MRT_BYTE:    sizeofUserReg =  8;    break;
                case MRT_CHAR:    sizeofUserReg =  8;    break;
                case MRT_INT16:   sizeofUserReg = 16;    break;
                case MRT_WORD:    sizeofUserReg = 16;    break;
                case MRT_HEX16:   sizeofUserReg = 16;    break;
                case MRT_INT32:   sizeofUserReg = 32;    break;
                case MRT_DWORD:   sizeofUserReg = 32;    break;
                case MRT_HEX32:   sizeofUserReg = 32;    break;
                case MRT_FLOAT:   sizeofUserReg = 32;    break;
            }

            if( sizeofUserReg == 0 )
                break;

            // Validate register width. Can only be 16 or 32, with 16 as the default
            int regWidth = layoutIni->ReadInteger( RegSection, regSizeEntry, 16 );

            if( ( regWidth != 16 ) && ( regWidth != 32 ) )
                break;

            // Entry looks good - save it if we have room
            if( pDevRegs != NULL )
            {
                if( nbrRegs < maxRegs )
                {
                    MODBUS_REG_DEFINITION* pCurrRegDef = &( pDevRegs[nbrRegs] );

                    pCurrRegDef->Clear();

                    pCurrRegDef->regNbr       = (DWORD)regStart;
                    pCurrRegDef->userEnum     = regEnum;
                    pCurrRegDef->physRegWidth = regWidth;
                    pCurrRegDef->mrtType      = (MODBUS_REG_TYPE)regType;
                    pCurrRegDef->displayName  = layoutIni->ReadString ( RegSection, regDescEntry,   "" );
                    pCurrRegDef->units        = layoutIni->ReadString ( RegSection, regUnitsEntry,  "" );
                    pCurrRegDef->precision    = layoutIni->ReadInteger( RegSection, regPrecEntry,   0  );

                    String sAccessType = layoutIni->ReadString( RegSection, regRWEntry, ""  ).UpperCase();

                    if( sAccessType == "RW" )
                        pCurrRegDef->matAccType = MAT_RD_WR;
                    else if( sAccessType == "W" )
                        pCurrRegDef->matAccType = MAT_WR;
                    else
                        pCurrRegDef->matAccType = MAT_RD;

                    // Save and set the default value now
                    pCurrRegDef->defValue = layoutIni->ReadString( RegSection, regDefEntry, "" );

                    if( pCurrRegDef->defValue.Length() > 0 )
                        SetMODBUSRegValue( pCurrRegDef->regData, *pCurrRegDef, pCurrRegDef->defValue );

                    // Calculate the number of physical registers this register
                    // takes up. Note that this math assumes that regWidth will
                    // be either 16 or 32
                    if( sizeofUserReg <= regWidth )
                        pCurrRegDef->nbrPhysRegs = 1;
                    else
                        pCurrRegDef->nbrPhysRegs = sizeofUserReg / regWidth;
                }
            }

            // Increment our count and continue searching
            nbrRegs++;
        }
    }
    catch( ... )
    {
    }

    if( layoutIni != NULL )
        delete layoutIni;

    return nbrRegs;
}



//
// Class Implementation
//

__fastcall TModbusDevice::TModbusDevice()
{
    // Set default comm params
    m_comms.eCommType = eMCT_None;
    m_comms.devAddr   = 0;
    m_comms.commObj   = NULL;

    m_comms.dwBuffSize    = 2048;
    m_comms.dwBuffCount   = 0;
    m_comms.dwRxTickCount = 0;

    m_comms.pBuff = new BYTE[m_comms.dwBuffSize];

    // Init reg defs
    m_regDefs    = NULL;
    m_nbrRegDefs = 0;

    // Init state
    m_eState = eMS_Initializing;
}


__fastcall TModbusDevice::~TModbusDevice()
{
    if( m_comms.commObj != NULL )
        delete m_comms.commObj;

    if( m_comms.pBuff != NULL )
        delete [] m_comms.pBuff;

    if( m_regDefs != NULL )
        delete [] m_regDefs;
}


bool TModbusDevice::InitializeDevice( const ModbusCommSettings& commsSettings, const String& deviceDescFile )
{
    if( m_comms.commObj != NULL )
    {
        delete m_comms.commObj;
        m_comms.commObj = NULL;
    }

    m_comms.dwBuffCount   = 0;
    m_comms.dwRxTickCount = 0;

    // Setup the comm params now
    CCommPort::CONNECT_PARAMS      serialConnParams;
    CUDPPort::CONNECT_PARAMS       UDPConnParams;
    CTCPServerPort::CONNECT_PARAMS TCPConnParams;

    void* pParams = NULL;

    switch( commsSettings.eCommType )
    {
        case eMCT_None:
            // Not an allowed type
            return false;

        case eMCT_Serial:
            {
                // Create the object and setup its params
                CCommPort* pCommPort = new CCommPort();
                m_comms.commObj = pCommPort;

                serialConnParams.iPortNumber   = commsSettings.iCommPort;
                serialConnParams.dwLineBitRate = commsSettings.dwSpeed;

                pParams = &serialConnParams;
            }
            break;

        case eMCT_EnetUDP:
            {
                // Create the object and setup its params
                CUDPPort* pCUDPPort = new CUDPPort();
                m_comms.commObj = pCUDPPort;

                // We'll want to create a listening UDP socket
                UDPConnParams.portNbr  = commsSettings.wIPPort;
                UDPConnParams.destPort = 0;
                UDPConnParams.destAddr = "";

                pParams = &UDPConnParams;
            }
            break;

        case eMCT_EnetTCP:
            {
                // Create the object and setup its params
                CTCPServerPort* pTCPPort = new CTCPServerPort();
                m_comms.commObj = pTCPPort;

                TCPConnParams.listeningSocket = commsSettings.wIPPort;

                pParams = &TCPConnParams;
            }
            break;

        default:
            return false;
    }

    if( m_comms.commObj->Connect( pParams ) != CCommObj::ERR_NONE )
    {
        m_eState = eMS_CommConnectErr;
        return false;
    }

    m_comms.eCommType = commsSettings.eCommType;

    if( m_comms.eCommType == eMCT_Serial )
        m_comms.devAddr = commsSettings.bySlaveAddr;
    else
        m_comms.devAddr = commsSettings.byUnitID;

    m_comms.bOffsetRegs = commsSettings.bOffsetRegs;

    // For now, fix the endianess as standard "big" endian
    m_comms.endByteOrder = HIGH_LOW;
    m_comms.endWordOrder = HIGH_LOW;

    // Client modules use this method to create a new device. First, validate
    // that the file exists
    if( !FileExists( deviceDescFile ) )
    {
        m_eState = eMS_DevDescError;

        MessageDlg( "The specified device description file does not exist.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    String errorInfo;

    if( !LoadRegsFromFile( deviceDescFile, errorInfo ) )
    {
        // No luck! Delete the temp device. LoadFromFile() would have
        // displayed an error message describing the problem.
        m_eState = eMS_DevDescError;

        MessageDlg( "The device description file could not be loaded (" + errorInfo + ").", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    // Device was successfully loaded. Listen for a connection
    m_eState = eMS_Listening;

    return true;
}


void TModbusDevice::Update( void )
{
    // Must be periodically called to process MODBUS commands and respond to them

    // First, we must have been initialized
    if( m_comms.eCommType == eMCT_None )
        return;

    // Check for a command. If we have one, then process it and send a response.
    // First, if there is data in the buffer flush it if its gone stale
    if( m_comms.dwBuffCount > 0 )
    {
        const DWORD MODBUSCmdTimeout = 250;

        if( HaveTimeout( m_comms.dwRxTickCount, MODBUSCmdTimeout ) )
            FlushRecvData();
    }

    // Special handling for TCP: we need to be checking for connection requests
    if( m_comms.eCommType == eMCT_EnetTCP )
    {
        CTCPServerPort* pTCPPort = (CTCPServerPort*)m_comms.commObj;

        pTCPPort->ProcessConnectionRequests();

        if( pTCPPort->ServerState == CTCPServerPort::eTSS_Connected )
            m_eState = eMS_Ready;
        else
            m_eState = eMS_Listening;
    }
    else
    {
        // With any other comms type we don't know when a server
        // connects with us, so for now just assume we're connected
        m_eState = eMS_Ready;
    }

    // Now check for new data. UDP ports are handled a bit differently as
    // those ports are connectionless
    AnsiString udpCmdSrcAddr;
    WORD       udpCmdSrcPort;

    bool  bBufferEmpty = ( m_comms.dwBuffCount == 0 );
    DWORD dwBytesRxd   = 0;

    BYTE* pRxBuff    = &( m_comms.pBuff[m_comms.dwBuffCount] );
    DWORD dwBuffSize = m_comms.dwBuffSize - m_comms.dwBuffCount;

    if( m_comms.eCommType == eMCT_EnetUDP )
    {
        CUDPPort* pCUDPPort = (CUDPPort*)m_comms.commObj;

        dwBytesRxd = pCUDPPort->RecvFrom( pRxBuff, dwBuffSize, udpCmdSrcAddr, udpCmdSrcPort );
    }
    else
    {
        dwBytesRxd = m_comms.commObj->CommRecv( pRxBuff, dwBuffSize );
    }

    // If we've received bytes, log them
    if( dwBytesRxd > 0 )
    {
        String sMsg = String( __FUNC__ ) + ", Rx " + IntToStr( (int)dwBytesRxd ) + " bytes: ";

        for( int iIndex = 0; iIndex < dwBytesRxd; iIndex++ )
        {
            int iByte = pRxBuff[m_comms.dwBuffCount + iIndex];
            sMsg += IntToHex( iByte, 2 ).SubString( 1, 2 ) + " ";
        }

        OutputDebugMsg( this, sMsg );
    }

    // If our buffer was empty, then timestamp the arrival of any new data
    if( ( dwBytesRxd > 0 ) && bBufferEmpty )
        m_comms.dwRxTickCount = GetTickCount();

    m_comms.dwBuffCount += dwBytesRxd;

    // If our buffer is empty, can exit now
    if( m_comms.dwBuffCount == 0 )
        return;

    // We have some data, try to make a packet out of it. Note that the parser will
    // try to interpret the entire buffer.
    MODBUS_COMMAND_PKT cmdPkt;
    WORD wBytesUsed;

    eParseMODBUSCmdResult parseResult = HaveModbusCommand( m_comms.eCommType, m_comms.pBuff, m_comms.dwBuffCount, wBytesUsed, cmdPkt );

    if( parseResult != ePMCR_HavePacket )
    {
         // Don't have a packet. Figure out what to do
        switch( parseResult )
        {
            case ePMCR_NULLBuffer:
                // Should never happen
                break;

            case ePMCR_NotEnoughBytes:
                // Need to wait for more data
                break;

            case ePMCR_BadHeader:
                // Unsupported protocol element
                {
                    String sMsg = String( __FUNC__ ) + ", Bad header received";
                    OutputDebugMsg( this, sMsg );
                }
                break;

            case ePMCR_FcnCode:
                // Unsupported protocol element
                {
                    String sMsg = String( __FUNC__ ) + ", Bad function code received";
                    OutputDebugMsg( this, sMsg );
                }
                break;

            case ePMCR_TooManyBytes:
                // Buffered data is too big to be a MODBUS command, so flush all the data
                FlushRecvData();
                break;

            case ePMCR_CRCError:
                // Buffered data has a CRC error. FLush the entire buffer. In a future release
                // we could try just shifting the buffer down by one byte and reparsing.
                FlushRecvData();
                break;

            default:
                // Unknown result! Turf the entire buffer
                FlushRecvData();
                break;
        }

        return;
    }

    // Fall through means we a packet. Process it. Note that we flush the entire
    // rx buffer when we get a command
    FlushRecvData();

    MODBUS_RESPONSE_PKT  respPkt;
    eMODBUSExceptionCode exceptCode;

    PROCESS_CMD_RESULT procResult = ProcessMODBUSCmd( Now(), cmdPkt, respPkt, exceptCode );

    switch( procResult )
    {
        case PCR_NOT_THIS_DEVICE:
           // Not our address - ignore this command
           break;

        case PCR_SUCCESS_NO_RESP:
        case PCR_EXCEPT_NO_RESP:
            // Was for us, no response necessary
            break;

        case PCR_SUCCESS_WITH_RESP:
            // Must send a reply
            {
                BYTE* pTxBuffer = new BYTE[m_comms.dwBuffSize];

                DWORD txBuffCount = BuildModbusResponse( m_comms.eCommType, respPkt, pTxBuffer, m_comms.dwBuffSize );

                DWORD bytesSent = 0;

                if( txBuffCount > 0 )
                {
                    // For UDP connections, must set dest addr/port
                    if( m_comms.eCommType == eMCT_EnetUDP )
                    {
                        CUDPPort* pCUDPPort = (CUDPPort*)m_comms.commObj;

                        bytesSent = pCUDPPort->SendTo( pTxBuffer, txBuffCount, udpCmdSrcAddr, udpCmdSrcPort );
                    }
                    else
                    {
                        bytesSent = m_comms.commObj->CommSend( pTxBuffer, txBuffCount );
                    }

                    /* validate bytesSent == txBuffCount */

                    String sMsg = String( __FUNC__ ) + ", Tx " + IntToStr( (int)txBuffCount ) + " bytes: ";

                    for( int iIndex = 0; iIndex < (int)txBuffCount; iIndex++ )
                    {
                        int iByte = pTxBuffer[iIndex];
                        sMsg += IntToHex( iByte, 2 ).SubString( 1, 2 ) + " ";
                    }

                    OutputDebugMsg( this, sMsg );
                }

                delete [] pTxBuffer;
            }
            break;

        case PCR_EXCEPT_WITH_RESP:
            // Must send a reply
            {
                BYTE* pTxBuffer = new BYTE[m_comms.dwBuffSize];

                DWORD txBuffCount = BuildModbusExceptionPkt( m_comms.eCommType, cmdPkt.slaveAddr, cmdPkt.fcnCode, cmdPkt.wTransID, exceptCode, pTxBuffer, m_comms.dwBuffSize );

                DWORD bytesSent = 0;

                if( txBuffCount > 0 )
                {
                    // For UDP connections, must set dest addr/port
                    if( m_comms.eCommType == eMCT_EnetUDP )
                    {
                        CUDPPort* pCUDPPort = (CUDPPort*)m_comms.commObj;

                        bytesSent = pCUDPPort->SendTo( pTxBuffer, txBuffCount, udpCmdSrcAddr, udpCmdSrcPort );
                    }
                    else
                    {
                        bytesSent = m_comms.commObj->CommSend( pTxBuffer, txBuffCount );
                    }

                    /* validate bytesSent == txBuffCount */
                }

                String sMsg = String( __FUNC__ ) + ", Tx exception code " + IntToHex( exceptCode, 2 );
                OutputDebugMsg( this, sMsg );

                delete [] pTxBuffer;
            }
            break;

        default:
            // Should never happen
            break;
    }
}



bool TModbusDevice::LoadRegsFromFile( const String& regDescFile, String& errorInfo )
{
    // Protected method that tries to initialize a device from a
    // device description file. Returns true on success.

    // First, confirm the file exists
    if( !FileExists( regDescFile ) )
    {
        errorInfo = "register layout file does not exist";
        return false;
    }

    // Get working register descriptions now
    if( m_regDefs != NULL )
        delete [] m_regDefs;

    m_regDefs = NULL;

    m_nbrRegDefs = GetDeviceRegDefs( regDescFile, NULL, 0 );

    if( m_nbrRegDefs == 0 )
    {
        String sNoRegsWarning = "Warning: no register definitions were found in this file.";

        MessageDlg( sNoRegsWarning, mtWarning, TMsgDlgButtons() << mbOK, 0 );

        return false;
    }

    // Create the number of registers required. Disable the logging when
    // calling GetRegDefsFromMapFile() to prevent a duplicate pass of the
    // registers from appearing there.
    m_regDefs = new MODBUS_REG_DEFINITION[m_nbrRegDefs];

    int regDefsRead = GetDeviceRegDefs( regDescFile, m_regDefs, m_nbrRegDefs );

    // Finally, check for register number overlap. Because the register
    // order as read cannot be guaranteed, compare all regdefs against
    // each other.
    for( int iReg = 0; iReg < regDefsRead; iReg++ )
    {
        DWORD testRegAddr = m_regDefs[iReg].regNbr;

        for( int testRegIndex = 0; testRegIndex < regDefsRead; testRegIndex++ )
        {
            // Don't test a reg against itself
            if( testRegIndex == iReg )
                continue;

            if( ( testRegAddr >= m_regDefs[testRegIndex].regNbr )
                  &&
                ( testRegAddr < m_regDefs[testRegIndex].regNbr + m_regDefs[testRegIndex].nbrPhysRegs )
              )
            {
                errorInfo = "the definitions for registers " + IntToStr( (__int64)testRegAddr ) +
                            " and " + IntToStr( (__int64)m_regDefs[testRegIndex].regNbr ) +
                            " overlap with each other";

                MessageDlg( "Error: " + errorInfo, mtError, TMsgDlgButtons() << mbOK, 0 );

                delete [] m_regDefs;
                m_regDefs = NULL;

                m_nbrRegDefs = 0;

                return false;
            }
        }
    }

    // Fall through means registers successfully loaded
    return true;
}


typedef enum {
    MFC_READ_COILS,
    MFC_READ_DISCRETE_INPUTS,
    MFC_READ_REGISTERS,
    MFC_READ_INPUT_REGISTERS,
    MFC_WRITE_COIL,
    MFC_WRITE_REGISTER,
    MFC_WRITE_COILS,
    MFC_WRITE_REGISTERS,
    NBR_MODBUS_FCN_CODES
} MODBUS_FCN_CODE;

typedef struct {
    BYTE  fcnCode;    // a supported function code
    bool  isBoolOp;   // true if this function is a boolean one
    bool  isWriteOp;  // true if this function is a write operation
} SUPPORTED_FCN_INFO;

static SUPPORTED_FCN_INFO SupportedFcnInfo[NBR_MODBUS_FCN_CODES] = {
//    fcnCode                isBoolOp  isWriteOp
    { eMCT_ReadCoil,         true,     false     }, // MFC_READ_COILS
    { eMCT_ReadInput,        true,     false     }, // MFC_READ_DISCRETE_INPUTS
    { eMCT_ReadHoldingReg,   false,    false     }, // MFC_READ_REGISTERS
    { eMCT_ReadInputReg,     false,    false     }, // MFC_READ_INPUT_REGISTERS
    { eMCT_WriteCoil,        true,     true      }, // MFC_WRITE_COIL
    { eMCT_WriteHoldingReg,  false,    true      }, // MFC_WRITE_REGISTER
    { eMCT_WriteMultiCoils,  true,     true      }, // MFC_WRITE_COILS
    { eMCT_WriteMultiRegs,   false,    true      }, // MFC_WRITE_REGISTERS
};


TModbusDevice::PROCESS_CMD_RESULT TModbusDevice::ProcessMODBUSCmd( TDateTime pktTime, const MODBUS_COMMAND_PKT& cmdPkt, MODBUS_RESPONSE_PKT& respPkt, eMODBUSExceptionCode& exceptCode )
{
    // Processes a command. Returns an enum value indicating the result
    // of the operation. If the function needs to send data, it will
    // respond with one of the "..._WITH_RESP" enums.
    exceptCode = eMEC_NoError;

    // Check if this is addressed for us
    if( ( cmdPkt.slaveAddr != m_comms.devAddr ) && !cmdPkt.isBroadcast )
        return PCR_NOT_THIS_DEVICE;

    // Check for support function
    MODBUS_FCN_CODE fcnCode = NBR_MODBUS_FCN_CODES;

    for( int iFcn = 0; iFcn < NBR_MODBUS_FCN_CODES; iFcn++ )
    {
        if( cmdPkt.fcnCode == SupportedFcnInfo[iFcn].fcnCode )
        {
            fcnCode = (MODBUS_FCN_CODE)iFcn;
            break;
        }
    }

    // Validate function code
    if( fcnCode == NBR_MODBUS_FCN_CODES )
    {
        // Exception - invalid function
        exceptCode = eMEC_UnsupportedFcn;
        return PCR_EXCEPT_WITH_RESP;
    }

    // Validate start address and (if applicable) address range.
    // Standard address ranges are:
    //
    //  Fcn        Standard MODBUS / Addr Range        Enron
    //  01         1-9999          / 0x0 - 0x270E      1001-2999 / 0x03E9 - 0x0BB7
    //  05         1-9999          / 0x0 - 0x270E      1001-2999 / 0x03E9 - 0x0BB7
    //  15         1-9999          / 0x0 - 0x270E      1001-2999 / 0x03E9 - 0x0BB7
    //  02         10001-19999     / 0x0 - 0x270E      Not implemented
    //  04         30001-39999     / 0x0 - 0x270E      Not implemented
    //  03         40001-49999     / 0x0 - 0x270E      3001-3999, 5001-5999, 7001-7999 / 0x0 - 0xFFFF
    //  06         40001-49999     / 0x0 - 0x270E      3001-3999, 5001-5999, 7001-7999 / 0x0 - 0xFFFF
    //  16         40001-49999     / 0x0 - 0x270E      3001-3999, 5001-5999, 7001-7999 / 0x0 - 0xFFFF

    // Standard MODBUS address varies by command type
    WORD stdRegOffset = 0;

    switch( fcnCode )
    {
        case MFC_READ_COILS:
        case MFC_WRITE_COIL:
        case MFC_WRITE_COILS:
            // No address translation required
            break;

        case MFC_READ_DISCRETE_INPUTS:
            stdRegOffset = 10000;
            break;

        case MFC_READ_INPUT_REGISTERS:
            stdRegOffset = 30000;
            break;

        case MFC_READ_REGISTERS:
        case MFC_WRITE_REGISTER:
        case MFC_WRITE_REGISTERS:
            stdRegOffset = 40000;
            break;
    }

    // Determine the address range now
    DWORD physAddrStart = cmdPkt.regAddress;
    DWORD physAddrEnd   = cmdPkt.regAddress;

    switch( fcnCode )
    {
        case MFC_READ_COILS:
        case MFC_READ_DISCRETE_INPUTS:
        case MFC_READ_REGISTERS:
        case MFC_READ_INPUT_REGISTERS:
        case MFC_WRITE_COILS:
        case MFC_WRITE_REGISTERS:
            physAddrEnd += cmdPkt.numRegs - 1;
            break;
    }

    // Apply any address offset now
    DWORD logAddrStart = physAddrStart + stdRegOffset;
    DWORD logAddrEnd   = physAddrEnd   + stdRegOffset;

    if( m_comms.bOffsetRegs )
    {
        logAddrStart++;
        logAddrEnd++;
    }

    // Validate register count. Only required for certain function codes.
    // Determine the max number of registers that can be transferred. Note
    // that we assume our device only supports 16-bit registers
    int maxNumBoolsForRead  = 2000;
    int maxNumRegsForRead   =  125;
    int maxNumBoolsForWrite = 1968;
    int maxNumRegsForWrite  =  123;

    WORD wTemp;

    switch( fcnCode )
    {
        case MFC_READ_COILS:
        case MFC_READ_DISCRETE_INPUTS:
            if( ( cmdPkt.numRegs == 0 ) || ( cmdPkt.numRegs > maxNumBoolsForRead ) )
            {
                exceptCode = eMEC_BadRegCount;
                return PCR_EXCEPT_WITH_RESP;
            }
            break;

        case MFC_READ_REGISTERS:
        case MFC_READ_INPUT_REGISTERS:
            if( ( cmdPkt.numRegs == 0 ) || ( cmdPkt.numRegs > maxNumRegsForRead ) )
            {
                exceptCode = eMEC_BadRegCount;
                return PCR_EXCEPT_WITH_RESP;
            }
            break;

        case MFC_WRITE_COILS:
            if( ( cmdPkt.numRegs == 0 ) || ( cmdPkt.numRegs > maxNumBoolsForWrite ) )
            {
                exceptCode = eMEC_BadRegCount;
                return PCR_EXCEPT_WITH_RESP;
            }

            wTemp = cmdPkt.numRegs / 8;

            if( ( cmdPkt.numRegs % 8 ) != 0 )
                wTemp++;

            if( cmdPkt.nbrDataItems != wTemp )
            {
                exceptCode = eMEC_BadRegCount;
                return PCR_EXCEPT_WITH_RESP;
            }

            break;

        case MFC_WRITE_REGISTERS:
            if( ( cmdPkt.numRegs == 0 ) || ( cmdPkt.numRegs > maxNumRegsForWrite ) )
            {
                exceptCode = eMEC_BadRegCount;
                return PCR_EXCEPT_WITH_RESP;
            }
            break;
    }

    // Validate data value. Only required for certain function codes
    switch( fcnCode )
    {
        case MFC_WRITE_COIL:
            if( ( cmdPkt.regValue != 0 ) && ( cmdPkt.regValue != 0xFF ) )
            {
                exceptCode = eMEC_OpFailed;
                return PCR_EXCEPT_WITH_RESP;
            }
            break;
    }

    // Init response packet
    memset( &respPkt, 0, sizeof( MODBUS_RESPONSE_PKT ) );

    respPkt.slaveAddr = m_comms.devAddr;
    respPkt.fcnCode   = cmdPkt.fcnCode;
    respPkt.wTransID  = cmdPkt.wTransID;

    BYTE byException = eMEC_NoError;
    BYTE byTemp;

    switch( fcnCode )
    {
        case MFC_READ_COILS:
        case MFC_READ_DISCRETE_INPUTS:

            // ReadBoolData will return the number of bytes == regs stored
            respPkt.nbrDataItems = ReadBoolData( respPkt.byData, logAddrStart, cmdPkt.numRegs, byException );
            break;

        case MFC_WRITE_COILS:

            WriteBoolData( cmdPkt.byData, cmdPkt.nbrDataItems, logAddrStart, cmdPkt.numRegs, byException );

            respPkt.regAddress = cmdPkt.regAddress;
            respPkt.regValue   = cmdPkt.regValue;
            break;

        case MFC_READ_REGISTERS:
        case MFC_READ_INPUT_REGISTERS:

            // ReadRegData will return the number of bytes read, need to respond with nbr of words written
            respPkt.nbrDataItems = ReadRegData( respPkt.byData, logAddrStart, cmdPkt.numRegs, byException ) / sizeof( WORD );
            break;

        case MFC_WRITE_COIL:

            if( cmdPkt.regValue == 0 )
                byTemp = 0;
            else
                byTemp = 1;

            WriteBoolData( &byTemp, 1, logAddrStart, 1, byException );

            respPkt.regAddress = cmdPkt.regAddress;
            respPkt.regValue   = cmdPkt.regValue;
            break;

        case MFC_WRITE_REGISTER:

            // For a write-single function, use the reg value array.
            // The register implementation will interpret that array
            // in the proper context.
            WriteRegData( (BYTE*)&cmdPkt.regValue, 1, logAddrStart, 1, byException );

            // The register value must be echoed back too, but the number of bytes
            // in that field will depend on the number of bytes sent by the host.
            respPkt.regAddress = cmdPkt.regAddress;
            respPkt.regValue   = cmdPkt.regValue;
            break;

        case MFC_WRITE_REGISTERS:

            // For write multiple, use the payload data area
            WriteRegData( cmdPkt.byData, cmdPkt.nbrDataItems, logAddrStart, cmdPkt.numRegs, byException );

            respPkt.regAddress   = cmdPkt.regAddress;
            respPkt.nbrDataItems = cmdPkt.nbrDataItems;
            break;
    }

    // Check for an error
    if( byException != eMEC_NoError )
    {
        exceptCode = (eMODBUSExceptionCode)byException;
        return PCR_EXCEPT_WITH_RESP;
    }

    // Fall through means success
    return PCR_SUCCESS_WITH_RESP;
}


BYTE TModbusDevice::ReadRegData( BYTE* pData, DWORD logAddrStart, BYTE numRegs, BYTE& byException )
{
    // Transfers data from the register range requested to the pData array.
    // Returns the number of bytes stored in pData. If 0 is returned,
    // then byException has an error to be reported back to the host.
    byException = eMEC_NoError;

    BYTE  bytesRead = 0;
    DWORD currAddr  = logAddrStart;
    int   regsLeft  = numRegs;
    BYTE* pBuffer   = pData;

    while( regsLeft > 0 )
    {
        int  regsRead;
        WORD bytesSaved;

        DATA_RESPONSE getResp = GetDataForHost( currAddr, regsLeft, pBuffer, regsRead, bytesSaved );

        switch( getResp )
        {
            case DR_SUCCESS:
                break;

            case DR_REG_DOESNT_EXIST:
                byException = eMEC_BadAddrRange;
                return 0;

            default:
                byException = eMEC_OpFailed;
                return 0;
        }

        // Fall through means we can continue transferring data. Advance
        // the pointer to the data buffer by the number of words saved
        pBuffer = &( pBuffer[bytesSaved] );

        // Increment the total number of bytes read
        bytesRead += bytesSaved;

        // Update the register vars
        regsLeft -= regsRead;
        currAddr += regsRead;
    }

    return bytesRead;
}


BYTE TModbusDevice::ReadBoolData( BYTE* pData, DWORD logAddrStart, BYTE numRegs, BYTE& byException )
{
    // Transfers data from the passed DEVICE_REG_BLOCK data array to the
    // pData array starting at logical address logAddrStart for numPRegs.
    // Returns the number of bytes stored in pData. This function is designed
    // to read boolean register types
    byException = eMEC_NoError;

    BYTE  bytesRead  = 0;
    BYTE  dataOffset = 0;
    DWORD currAddr   = logAddrStart;
    int   regsLeft   = numRegs;

    BYTE  byCurrByte = 0;
    BYTE  byCurrBit  = 0x01;
    int   bitCount   = 0;

    while( regsLeft > 0 )
    {
        bool bValue;

        DATA_RESPONSE getResp = GetBoolForHost( currAddr, bValue );

        switch( getResp )
        {
            case DR_SUCCESS:
                break;

            case DR_REG_DOESNT_EXIST:
                byException = eMEC_BadAddrRange;
                return 0;

            default:
                byException = eMEC_OpFailed;
                return 0;
        }

        // Turn on the bit, if required
        if( bValue )
            byCurrByte |= byCurrBit;

        bitCount++;

        // If we've filled up the byte, save it to the output array
        if( bitCount == 8 )
        {
            pData[dataOffset] = byCurrByte;
            dataOffset++;
            bytesRead++;

            byCurrByte = 0;
            byCurrBit  = 0x01;
            bitCount   = 0;
        }

        // Update the register vars
        regsLeft--;
        currAddr++;
    }

    // Save the last byte being built, if there were bits added to it
    if( bitCount != 0 )
    {
        pData[dataOffset] = byCurrByte;
        bytesRead++;
    }

    return bytesRead;
}


bool TModbusDevice::WriteRegData( const BYTE* pSrc, WORD wDataCount, DWORD logAddrStart, BYTE numRegs, BYTE& byException )
{
    // Transfers data from the passed pSrc array to the registers starting
    // at logical address logAddrStart for numRegs. If false is returned,
    // then byException has an error to be reported back to the host.
    byException = eMEC_NoError;

    DWORD currAddr  = logAddrStart;
    int   regsLeft  = numRegs;
    WORD  bytesLeft = wDataCount;

    const BYTE* pBuffer = pSrc;

    while( regsLeft > 0 )
    {
        int  regsWritten;
        WORD bytesUsedFromBuff;

        DATA_RESPONSE getResp = SetDataFromHost( currAddr, regsLeft, pBuffer, bytesLeft, regsWritten, bytesUsedFromBuff );

        switch( getResp )
        {
            case DR_SUCCESS:
                break;

            case DR_REG_DOESNT_EXIST:
                byException = eMEC_BadAddrRange;
                return false;

            case DR_NOT_ENOUGH_DATA:
                if( numRegs == 1 )
                    byException = eMEC_OpFailed;
                else
                    byException = eMEC_BadRegCount;
                return false;

            default:
                byException = eMEC_OpFailed;
                return false;
        }

        // Fall through means we can continue transferring data. Advance
        // the pointer to the data buffer by the number of words saved
        pBuffer = &( pBuffer[bytesUsedFromBuff] );

        bytesLeft -= bytesUsedFromBuff;

        // Update the register vars
        regsLeft -= regsWritten;
        currAddr += regsWritten;
    }

    return true;
}


bool TModbusDevice::WriteBoolData( const BYTE* pSrc, WORD wDataCount, DWORD logAddrStart, BYTE numRegs, BYTE& byException )
{
    // Transfers bool data from the passed pSrc array to the registers starting
    // at logical address logAddrStart for numRegs. If false is returned,
    // then byException has an error to be reported back to the host.
    int   dataOffset = 0;

    BYTE  byCurrBit  = 0x01;
    int   bitCount   = 0;

    DWORD currAddr  = logAddrStart;
    int   regsLeft  = numRegs;

    while( numRegs > 0 )
    {
        bool boolVal;

        if( ( pSrc[dataOffset] & byCurrBit ) == 0 )
            boolVal = false;
        else
            boolVal = true;

        DATA_RESPONSE getResp = SetBoolFromHost( currAddr, boolVal );

        switch( getResp )
        {
            case DR_SUCCESS:
                break;

            case DR_REG_DOESNT_EXIST:
                byException = eMEC_BadAddrRange;
                return false;

            case DR_NOT_ENOUGH_DATA:
                if( numRegs == 1 )
                    byException = eMEC_OpFailed;
                else
                    byException = eMEC_BadRegCount;
                return false;

            default:
                byException = eMEC_OpFailed;
                return false;
        }

        // Advance the reg info
        regsLeft--;
        currAddr++;

        // Advance our bit flag and data offset if required
        bitCount++;
        byCurrBit <<= 1;

        // If we've filled up the byte, save it to the output array
        if( bitCount == 8 )
        {
            dataOffset++;

            byCurrBit = 0x01;
            bitCount  = 0;
        }
    }

    return true;
}


void TModbusDevice::FlushRecvData( void )
{
    // Flush any pending data out of our port
    while( m_comms.commObj->CommRecv( m_comms.pBuff, m_comms.dwBuffSize ) )
        ;

    // Point to the start of the rx buffer
    m_comms.dwBuffCount   = 0;
    m_comms.dwRxTickCount = 0;
}


DWORD TModbusDevice::GetRespPDULen( int functionCode, WORD wNbrRegs )
{
    // The PDU length is independent of the underlying protocol type
    DWORD dwPDULen = 0;

    switch( functionCode )
    {
        case MFC_READ_REGISTERS:
        case MFC_READ_INPUT_REGISTERS:
            // Function code 0x03 - can read multiple regs
            // Function code 0x04 - can also read multiple regs
            dwPDULen = 1 /*fcn code*/ + 1 /*length byte*/ + wNbrRegs * sizeof( WORD );
            break;

        case MFC_WRITE_REGISTER:
            // Function code 0x06 - can only write one reg at a time
            dwPDULen = 1 /*fcn code*/ + sizeof( WORD ) /*reg nbr*/ + sizeof( WORD ) /*value*/;
            break;
    }

    return dwPDULen;
}

void TModbusDevice::ShiftRxBufferDown( DWORD dwBytes )
{
    // Remove the passed number of bytes from the head of the rx buffer.
    // If that amount is the same or more than what is currently in the
    // buffer, then the buffer will be empty.
    if( m_comms.dwBuffCount <= dwBytes )
    {
        m_comms.dwBuffCount = 0;
    }
    else
    {
        memmove( m_comms.pBuff, &(m_comms.pBuff[dwBytes]), dwBytes );

        m_comms.dwBuffCount -= dwBytes;
    }
}


TModbusDevice::DATA_RESPONSE TModbusDevice::GetDataForHost( DWORD dwRegNbr, int regsRequested, BYTE* pbyBuff, int& regsRead, WORD& bytesSaved )
{
    // Some validation first
    if( regsRequested <= 0 )
        return DR_INVALID_REQUEST;

    if( pbyBuff == NULL )
        return DR_INVALID_REQUEST;

    // Init returned vars
    regsRead   = 0;
    bytesSaved = 0;

    // Search for the requested reg
    for( int iReg = 0; iReg < m_nbrRegDefs; iReg++ )
    {
        MODBUS_REG_DEFINITION* pRegDef = &( m_regDefs[iReg] );

        if( pRegDef->regNbr == dwRegNbr )
        {
            // Check that we have read access to the register
            if( ( pRegDef->matAccType & MAT_RD ) == 0 )
                return DR_CANT_ACCESS;

            // Cannot use this function to access boolean data
            if( pRegDef->physRegWidth == 1 )
                return DR_INVALID_REQUEST;

            // Extract the data from the register. Register
            // object will endian swap are required.
            bytesSaved = MODBUSRegDataToBuff( *pRegDef, pbyBuff, regsRequested, regsRead, m_comms.endWordOrder, m_comms.endByteOrder );

            if( bytesSaved == 0 )
                return DR_INVALID_REQUEST;

            // Success!
            return DR_SUCCESS;
        }
    }

    // Fall through means register not found
    return DR_REG_DOESNT_EXIST;
}


TModbusDevice::DATA_RESPONSE TModbusDevice::SetDataFromHost( DWORD dwRegNbr, int regsAvail, const BYTE* pbyBuff, WORD wBytesInBuff, int& regsWritten, WORD& bytesUsed )
{
    // Some validation first
    if( regsAvail <= 0 )
        return DR_INVALID_REQUEST;

    if( pbyBuff == NULL )
        return DR_INVALID_REQUEST;

    // Init returned vars
    bytesUsed   = 0;
    regsWritten = 0;

    // Search for the requested reg
    for( int iReg = 0; iReg < m_nbrRegDefs; iReg++ )
    {
        MODBUS_REG_DEFINITION* pRegDef = &( m_regDefs[iReg] );

        if( pRegDef->regNbr == dwRegNbr )
        {
            // Check that we have read access to the register
            if( ( pRegDef->matAccType & MAT_WR ) == 0 )
                return DR_CANT_ACCESS;

            // Cannot use this function to access boolean data
            if( pRegDef->physRegWidth == 1 )
                return DR_INVALID_REQUEST;

            // Save data to the register. Register
            // object will endian swap are required.
            bytesUsed = MODBUSRegDataFromBuff( *pRegDef, pbyBuff, regsAvail, regsWritten, m_comms.endWordOrder, m_comms.endByteOrder );

            if( bytesUsed == 0 )
                return DR_INVALID_REQUEST;

            return DR_SUCCESS;
        }
    }

    // Fall through means register not found
    return DR_REG_DOESNT_EXIST;
}


TModbusDevice::DATA_RESPONSE TModbusDevice::GetBoolForHost( DWORD dwRegNbr, bool& bVal )
{
    for( int iReg = 0; iReg < m_nbrRegDefs; iReg++ )
    {
        MODBUS_REG_DEFINITION* pRegDef = &( m_regDefs[iReg] );

        if( pRegDef->regNbr == dwRegNbr )
        {
            // Check that this is a bool reg
            if( pRegDef->mrtType != MRT_BOOL )
                return DR_INVALID_REQUEST;

            // Check that we have read access to the register
            if( ( pRegDef->matAccType & MAT_RD ) == 0 )
                return DR_CANT_ACCESS;

            bVal = pRegDef->regData.asBool;

            return DR_SUCCESS;
        }
    }

    // Fall through means register not found
    return DR_REG_DOESNT_EXIST;
}


TModbusDevice::DATA_RESPONSE TModbusDevice::SetBoolFromHost( DWORD dwRegNbr, bool bVal )
{
    for( int iReg = 0; iReg < m_nbrRegDefs; iReg++ )
    {
        MODBUS_REG_DEFINITION* pRegDef = &( m_regDefs[iReg] );

        if( pRegDef->regNbr == dwRegNbr )
        {
            // Check that this is a bool reg
            if( pRegDef->mrtType != MRT_BOOL )
                return DR_INVALID_REQUEST;

            // Check that we have read access to the register
            if( ( pRegDef->matAccType & MAT_WR ) == 0 )
                return DR_CANT_ACCESS;

            pRegDef->regData.asBool = bVal;

            return DR_SUCCESS;
        }
    }

    // Fall through means register not found
    return DR_REG_DOESNT_EXIST;
}


DWORD __fastcall TModbusDevice::GetRegisterNbr( int iRegIndex )
{
    if( ( m_regDefs == NULL ) || ( iRegIndex < 0 ) || ( iRegIndex >= m_nbrRegDefs ) )
        return 0;

    return m_regDefs[iRegIndex].regNbr;
}


String __fastcall TModbusDevice::GetRegisterName( int iRegIndex )
{
    if( ( m_regDefs == NULL ) || ( iRegIndex < 0 ) || ( iRegIndex >= m_nbrRegDefs ) )
        return 0;

    return m_regDefs[iRegIndex].displayName;
}


int __fastcall TModbusDevice::GetRegisterEnum( int iRegIndex )
{
    if( ( m_regDefs == NULL ) || ( iRegIndex < 0 ) || ( iRegIndex >= m_nbrRegDefs ) )
        return 0;

    return m_regDefs[iRegIndex].userEnum;
}


MODBUS_REG_TYPE __fastcall TModbusDevice::GetRegisterType( int iRegIndex )
{
    if( ( m_regDefs == NULL ) || ( iRegIndex < 0 ) || ( iRegIndex >= m_nbrRegDefs ) )
        return MRT_UNKNOWN;

    return m_regDefs[iRegIndex].mrtType;
}


bool TModbusDevice::GetRegisterValueByIndex( int regIndex, REG_DATA_UNION& regData )
{
    // Handler to get register data. Returns true if a register
    // at that index exists and the data will be returned.
    // Otherwise returns false.
    if( ( m_regDefs == NULL ) || ( regIndex < 0 ) || ( regIndex >= m_nbrRegDefs ) )
        return false;

    regData = m_regDefs[regIndex].regData;

    return true;
}


bool TModbusDevice::SetRegisterValueByIndex( int regIndex, const REG_DATA_UNION& regData )
{
    // Handler to set register data. Returns true if a register
    // at that index exists and the data will be set. Otherwise
    // returns false.
    if( ( m_regDefs == NULL ) || ( regIndex < 0 ) || ( regIndex >= m_nbrRegDefs ) )
        return false;

    m_regDefs[regIndex].regData = regData;

    return true;
}


bool TModbusDevice::GetRegisterValueByEnum( int regEnum, REG_DATA_UNION& regData )
{
    // Handler to get register data. Returns true if a register
    // with that enum exists and the data will be returned in
    // that case. Returns the first instance of the enum in
    // the register map. Otherwise returns false.
    if( m_regDefs == NULL )
        return false;

    for( int iReg = 0; iReg < m_nbrRegDefs; iReg++ )
    {
        if( m_regDefs[iReg].userEnum == regEnum )
        {
            regData = m_regDefs[iReg].regData;
            return true;
        }
    }

    // Fall through means not found
    return false;
}


bool TModbusDevice::SetRegisterValueByEnum( int regEnum, const REG_DATA_UNION& regData )
{
    // Handler to set register data. Returns true if a register
    // with that enum exists and the data will be set in that case.
    // Otherwise returns false.
    if( m_regDefs == NULL )
        return false;

    bool bFound = false;

    for( int iReg = 0; iReg < m_nbrRegDefs; iReg++ )
    {
        if( m_regDefs[iReg].userEnum == regEnum )
        {
            // Set this register value, but keep searching in case
            // the enum exists in multiple registers
            m_regDefs[iReg].regData = regData;
            bFound  = true;
        }
    }

    // Fall through means not found
    return bFound;
}

