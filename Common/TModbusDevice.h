#ifndef TModbusDeviceH
#define TModbusDeviceH

#include "MODBUSPktHandler.h"
#include "MODBUSTypes.h"
#include "CommObj.h"


//
// Modbus Comms Settings Defines
//

typedef struct
{
    eModbusCommType eCommType;

    // Note: only on of the comm port / speed pair or the IP Addr / IP Port pair would be used
    int       iCommPort;       // Used for serial comms
    DWORD     dwSpeed;

    WORD      wIPPort;         // Used for UDP / TCP comms. Opens a listening socket on that port number.

    union {
        BYTE bySlaveAddr;      // Used for serial-type connections
        BYTE byUnitID;         // Used for UDP / TCP connections
    };

    bool      bOffsetRegs;     // True causes an offset of 1 to be applied to reg value sent in packet s
                               // to get physical reg number

} ModbusCommSettings;


//
// Class Declaration
//

class TModbusDevice : public TObject
{
  public:

    typedef enum {
        eMS_Initializing,     // MODBUS handling is intializing
        eMS_CommConnectErr,   // Init failed - comm connect error
        eMS_DevDescError,     // Init failed - bad dev desc file
        eMS_Listening,        // Init complete - waiting for a connection
        eMS_Ready,            // Init complete and connected to server
        eMS_NbrMODBUSStates
    } eMODBUSState;

  private:

    eMODBUSState m_eState;

    MODBUS_REG_DEFINITION* m_regDefs;
    int     m_nbrRegDefs;

    BYTE ReadRegData( BYTE* pData, DWORD logAddrStart, BYTE numRegs, BYTE& byException );
        // Transfers data from the register range requested to the pData array.
        // Returns the number of bytes stored in pData. If 0 is returned,
        // then byException has an error to be reported back to the host.

    BYTE ReadBoolData( BYTE* pData, DWORD logAddrStart, BYTE numRegs, BYTE& byException );
        // Transfers data from the passed DEVICE_REG_BLOCK data array to the
        // pData array starting at logical address logAddrStart for numPRegs.
        // Returns the number of bytes stored in pData. This function is
        // designed to read boolean register types. If 0 is returned, then
        // then byException has an error to be reported back to the host.

    bool WriteRegData( const BYTE* pSrc, WORD wDataCount, DWORD logAddrStart, BYTE numRegs, BYTE& byException );
        // Transfers data from the passed pSrc array to the registers starting
        // at logical address logAddrStart for numRegs. If false is returned,
        // then byException has an error to be reported back to the host.

    bool WriteBoolData( const BYTE* pSrc, WORD wDataCount, DWORD logAddrStart, BYTE numRegs, BYTE& byException );
        // Transfers bool data from the passed pSrc array to the registers starting
        // at logical address logAddrStart for numRegs. If false is returned,
        // then byException has an error to be reported back to the host.

    // Comms related variables
    struct
    {
        eModbusCommType eCommType;
        CCommObj*       commObj;

        BYTE            devAddr;       // Either the slave address or unit ID, depending on type of comms
        bool            bOffsetRegs;   // True causes logical reg number to be offset by one from phys number
        ENDIANESS       endByteOrder;
        ENDIANESS       endWordOrder;

        BYTE*           pBuff;         // Buffer holding current received data
        DWORD           dwBuffSize;    // Max size of buffer
        DWORD           dwBuffCount;   // Current number of bytes in buffer
        DWORD           dwRxTickCount; // Msec count when last data received

    } m_comms;

    bool LoadRegsFromFile( const String& regDescFile, String& errorInfo );
      // Loads register descriptions from the passed file. Returns true on
      // success, false on error with a message returned in errorInfo.

    void  FlushRecvData( void );
    DWORD GetRespPDULen( int functionCode, WORD wNbrRegs );
    void  ShiftRxBufferDown( DWORD dwBytes );

    typedef enum {
        DR_SUCCESS,
        DR_REG_DOESNT_EXIST,
        DR_CANT_ACCESS,
        DR_NOT_ENOUGH_DATA,
        DR_INVALID_REQUEST,
        NBR_DATA_RESPONSES
    } DATA_RESPONSE;

    DATA_RESPONSE GetDataForHost ( DWORD dwRegAddr, int regsRequested,       BYTE* pbyBuff,                    int& regsRead,    WORD& bytesSaved );
    DATA_RESPONSE SetDataFromHost( DWORD dwRegAddr, int regsAvail,     const BYTE* pbyBuff, WORD wBytesInBuff, int& regsWritten, WORD& bytesUsed  );
    DATA_RESPONSE GetBoolForHost ( DWORD dwRegAddr, bool& bVal );
    DATA_RESPONSE SetBoolFromHost( DWORD dwRegAddr, bool  bVal );
        // Methods to get data for the host or save data from the host.
        // The non-bool methods support reading from or writing to
        // multiple underlying physical registers. All the above
        // methods will perform endian swapping as required.

    typedef enum {
        PCR_NOT_THIS_DEVICE,
        PCR_SUCCESS_NO_RESP,
        PCR_SUCCESS_WITH_RESP,
        PCR_EXCEPT_NO_RESP,
        PCR_EXCEPT_WITH_RESP,
        NBR_PROC_CMD_RESULTS
    } PROCESS_CMD_RESULT;

    PROCESS_CMD_RESULT ProcessMODBUSCmd( TDateTime pktTime, const MODBUS_COMMAND_PKT& cmdPkt, MODBUS_RESPONSE_PKT& respPkt, eMODBUSExceptionCode& exceptCode );
      // Processes a command. Returns an enum value indicating the result
      // of the operation. If the function needs to send data, it will
      // respond with one of the "..._WITH_RESP" enums.

    DWORD           __fastcall GetRegisterNbr ( int iRegIndex );
    String          __fastcall GetRegisterName( int iRegIndex );
    int             __fastcall GetRegisterEnum( int iRegIndex );
    MODBUS_REG_TYPE __fastcall GetRegisterType( int iRegIndex );

  public:
    virtual __fastcall TModbusDevice();
    virtual __fastcall ~TModbusDevice();

    bool InitializeDevice( const ModbusCommSettings& commsSettings, const String& deviceDescFile );
      // Initializes are freshly constructed MODBUS instance. Note that no validation
      // is performed on the user enums associated with registers defined in the
      // device description file. For TCP and IP connections, assumes that the Winsock
      // interface has already been initialized.

    void Update( void );
        // Must be periodically called to process MODBUS commands and respond to them

    __property BYTE DeviceAddress = { read = m_comms.devAddr };
        // Will be either slave address or unit ID, depending on comms type

    __property eMODBUSState    State    = { read = m_eState };
    __property eModbusCommType CommType = { read = m_comms.eCommType };


    //
    // Methods for getting / setting data values in registers
    //
    // Note: these methods return the first register that has been assigned
    // the passed enum value. Registers are not sorted by number, but rather
    // and are saved in the order they were defined in the device description file.
    //

    __property int RegisterCount = { read = m_nbrRegDefs };

    __property DWORD           RegisterNumber[int] = { read = GetRegisterNbr };
    __property String          RegisterName[int]   = { read = GetRegisterName };
    __property int             RegisterEnum[int]   = { read = GetRegisterEnum };
    __property MODBUS_REG_TYPE RegisterType[int]   = { read = GetRegisterType };
        // Returns properties of a register as indexed by the register's
        // position in the register list

    bool GetRegisterValueByIndex( int regIndex,       REG_DATA_UNION& regData );
    bool SetRegisterValueByIndex( int regIndex, const REG_DATA_UNION& regData );
        // Handlers to get/set register data. Returns true if a register
        // at that index exists and the data will be set (or gotten)
        // in that case. Otherwise returns false.

    bool GetRegisterValueByEnum( int regEnum,       REG_DATA_UNION& regData );
    bool SetRegisterValueByEnum( int regEnum, const REG_DATA_UNION& regData );
        // Handlers to get/set register data. Returns true if a register
        // with that enum exists and the data will be set (or gotten)
        // in that case. Otherwise returns false.

};

#endif
