#include <vcl.h>
#pragma hdrstop

#include "TSerializerDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TSerializerDlg *SerializerDlg;


__fastcall TSerializerDlg::TSerializerDlg(TComponent* Owner) : TForm(Owner)
{
}


bool TSerializerDlg::ShowDlg( TForm* pClientForm, String& sHousingSN )
{
    TSerializerDlg *pDlg = NULL;

    bool bOK = false;

    try
    {
        pDlg = new TSerializerDlg( pClientForm );

        pDlg->CurrSNEdit->Text = sHousingSN;
        pDlg->NewSNEdit->Text  = "";
        pDlg->ActiveControl    = pDlg->NewSNEdit;

        if( pDlg->ShowModal() == mrOk )
        {
            sHousingSN = Trim( pDlg->NewSNEdit->Text );
            bOK = true;
        }
    }
    catch( ... )
    {
    }

    if( pDlg != NULL )
        delete pDlg;

    return bOK;
}


void __fastcall TSerializerDlg::OKBtnClick(TObject *Sender)
{
    if( Trim( NewSNEdit->Text ).Length() == 0 )
    {
        MessageDlg( "You cannot enter a blank serial number.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewSNEdit;

        return;
    }

    ModalResult = mrOk;
}

