object SerializerDlg: TSerializerDlg
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Update Housing Serial Number'
  ClientHeight = 140
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    457
    140)
  PixelsPerInch = 96
  TextHeight = 13
  object SNGB: TGroupBox
    Left = 8
    Top = 8
    Width = 441
    Height = 89
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Housing Serial Number '
    TabOrder = 0
    DesignSize = (
      441
      89)
    object Label8: TLabel
      Left = 16
      Top = 25
      Width = 70
      Height = 13
      Caption = 'Current Value:'
    end
    object Label4: TLabel
      Left = 16
      Top = 56
      Width = 54
      Height = 13
      Caption = 'New Value:'
    end
    object NewSNEdit: TEdit
      Left = 112
      Top = 53
      Width = 317
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 1
    end
    object CurrSNEdit: TEdit
      Left = 112
      Top = 22
      Width = 317
      Height = 21
      TabStop = False
      Anchors = [akLeft, akTop, akRight]
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
    end
  end
  object CancelBtn: TButton
    Left = 289
    Top = 107
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    ExplicitTop = 125
  end
  object OKBtn: TButton
    Left = 374
    Top = 107
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    TabOrder = 2
    OnClick = OKBtnClick
    ExplicitTop = 125
  end
end
