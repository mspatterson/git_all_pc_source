//---------------------------------------------------------------------------
#ifndef TSerializerDlgH
#define TSerializerDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
//---------------------------------------------------------------------------
class TSerializerDlg : public TForm
{
__published:	// IDE-managed Components
    TGroupBox *SNGB;
    TLabel *Label8;
    TEdit *NewSNEdit;
    TLabel *Label4;
    TEdit *CurrSNEdit;
    TButton *CancelBtn;
    TButton *OKBtn;
    void __fastcall OKBtnClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TSerializerDlg(TComponent* Owner);

    static bool ShowDlg( TForm* pClientForm, String& sHousingSN );
};
//---------------------------------------------------------------------------
extern PACKAGE TSerializerDlg *SerializerDlg;
//---------------------------------------------------------------------------
#endif
