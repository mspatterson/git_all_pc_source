#include <vcl.h>
#pragma hdrstop

#include "TSliderFrame.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TSliderFrame *SliderFrame;


//---------------------------------------------------------------------------
//    TSliderFrame(): public version of constructor. Sets basic control
//        properties
//---------------------------------------------------------------------------
__fastcall TSliderFrame::TSliderFrame( TComponent* pOwner, TWinControl* pParent, const String& sCompName, const String& sCaption ) :
                         TFrame( pOwner )
{
    // Immediately rename this component
    Name = sCompName;

    // Set the control's parent
    Parent = pParent;

    // Set the control caption. Assume the user has made it short enough to fit
    TitleLabel->Caption = sCaption;

    // Set defaults
    m_onValueChange = NULL;

    m_iMinValue  = 0;
    m_iMaxValue  = 100;
    m_iCurrValue = 0;

    // Set slider range. This never changes. The current value is mapped
    // into a slider value based on the min/max settings. Slider min/max
    // values are set for reasonable values to map user values into but
    // realistic for screen resolutions.
    LevelBar->Min       = -1000;
    LevelBar->Max       = 1000;
    LevelBar->Position  = 0;
    LevelBar->Hint      = "0";
    LevelBar->Frequency = 100;
    LevelBar->PageSize  = 100;
    LevelBar->LineSize  = 1;
}


//---------------------------------------------------------------------------
//    GetCaption(): returns the control's caption.
//---------------------------------------------------------------------------
String TSliderFrame::GetCaption( void )
{
    return TitleLabel->Caption;
}


//---------------------------------------------------------------------------
//    SetCaption(): sets the control's caption. No check is made to see
//        if the string will fit.
//---------------------------------------------------------------------------
void TSliderFrame::SetCaption( const String& newCaption )
{
    TitleLabel->Caption = newCaption;
}


//---------------------------------------------------------------------------
//    SetRange(): sets the range of values the slider bar is mapped to.
//        Slider values go from min (at bottom of screen) to max (at top).
//        If the current value is outside of the new min/max range, the
//        current value is set to min or max as the case may require.
//---------------------------------------------------------------------------
bool TSliderFrame::SetRange( int iMinValue, int iMaxValue )
{
    // Validate first
    if( iMinValue >= iMaxValue )
        return false;

    // Remember new range
    m_iMinValue = iMinValue;
    m_iMaxValue = iMaxValue;

    // Reset the slider position. This will also adjust the current
    // value to be in bounds if necessary.
    SetValue( m_iCurrValue );

    return true;
}


//---------------------------------------------------------------------------
//    SetValue(): maps the passed value to the slider range and positions
//        the slider there. If the value passed is outside of the current
//        min/max range, slider is positioned at min or max as the case
//        may require.
//---------------------------------------------------------------------------
void TSliderFrame::SetValue( int iNewValue )
{
    // If the new value is outside the min/max range, adjust it to be
    // in range
    if( iNewValue > m_iMaxValue )
        iNewValue = m_iMaxValue;
    else if( iNewValue < m_iMinValue )
        iNewValue = m_iMinValue;

    // Save new value
    m_iCurrValue = iNewValue;

    // Represent new value on the slider. Be careful on order of
    // operations to not cause rounding due to integer math. Note that
    // bar slides in opposite direction when its value increases
    // (Windows feature)
    LevelBar->Position = ( m_iCurrValue - m_iMaxValue ) * ( LevelBar->Max - LevelBar->Min ) / ( m_iMinValue - m_iMaxValue ) + LevelBar->Min;

    LevelBar->Hint = IntToStr( m_iCurrValue );

}


//---------------------------------------------------------------------------
//   LevelBarChange(): event handler called when a user moves the slider.
//      Maps the slider raw value to a user value and invokes the callback.
//---------------------------------------------------------------------------
void __fastcall TSliderFrame::LevelBarChange(TObject *Sender)
{
    // First, calculate new value represented on slider. Do math carefully
    // to avoid rounding errors. Note that Windows increases the bar in the
    // opposite direction that we desire.
    m_iCurrValue = ( LevelBar->Position - LevelBar->Min ) * ( m_iMinValue - m_iMaxValue ) / ( LevelBar->Max - LevelBar->Min ) + m_iMaxValue;

    // Update the hint value
    LevelBar->Hint = IntToStr( m_iCurrValue );

    // If we have an event handler, call it now
    if( m_onValueChange )
        m_onValueChange( this );
}


//---------------------------------------------------------------------------
//   ZeroBtnClick(): event handler called when the zero button is clicked.
//      Set the slider to zero (if in range), otherwise set to min or max.
//---------------------------------------------------------------------------
void __fastcall TSliderFrame::ZeroBtnClick(TObject *Sender)
{
    SetValue( 0 );
}

