object SliderFrame: TSliderFrame
  Left = 0
  Top = 0
  Width = 44
  Height = 201
  TabOrder = 0
  DesignSize = (
    44
    201)
  object SliderPanel: TPanel
    Left = 0
    Top = 0
    Width = 44
    Height = 201
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    DesignSize = (
      44
      201)
    object TitleLabel: TLabel
      Left = 4
      Top = 2
      Width = 36
      Height = 13
      Alignment = taCenter
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      Caption = 'good'
    end
    object ZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 30
      Height = 20
      Hint = 'Set value to 0'
      Anchors = [akLeft, akRight, akBottom]
      Caption = '0'
      OnClick = ZeroBtnClick
    end
    object LevelBar: TTrackBar
      Left = 7
      Top = 18
      Width = 34
      Height = 157
      Anchors = [akLeft, akTop, akRight, akBottom]
      Max = 100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      ShowSelRange = False
      TabOrder = 0
      OnChange = LevelBarChange
    end
  end
end
