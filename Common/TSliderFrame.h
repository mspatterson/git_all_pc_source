//---------------------------------------------------------------------------
#ifndef TSliderFrameH
#define TSliderFrameH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TSliderFrame : public TFrame
{
__published:	// IDE-managed Components
    TPanel *SliderPanel;
    TLabel *TitleLabel;
    TSpeedButton *ZeroBtn;
    TTrackBar *LevelBar;
    void __fastcall LevelBarChange(TObject *Sender);
    void __fastcall ZeroBtnClick(TObject *Sender);

private:

    TNotifyEvent m_onValueChange;

    int m_iMinValue;
    int m_iMaxValue;
    int m_iCurrValue;

    String GetCaption( void );
    void   SetCaption( const String& newCaption );

    void SetValue( int iNewValue );

public:
    __fastcall TSliderFrame( TComponent* pOwner, TWinControl* pParent, const String& sCompName, const String& sCaption );

    bool SetRange( int iMinValue, int iMaxValue );

    __property String       Caption       = { read = GetCaption,      write = SetCaption       };
    __property int          MinValue      = { read = m_iMinValue };
    __property int          MaxValue      = { read = m_iMaxValue };
    __property int          Value         = { read = m_iCurrValue,    write = SetValue         };
    __property TNotifyEvent OnValueChange = { read = m_onValueChange, write = m_onValueChange  };

};
//---------------------------------------------------------------------------
extern PACKAGE TSliderFrame *SliderFrame;
//---------------------------------------------------------------------------
#endif
