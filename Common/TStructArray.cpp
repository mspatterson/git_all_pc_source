#include <vcl.h>
#pragma hdrstop

#include "TStructArray.h"

#pragma package(smart_init)

//---------------------------------------------------------------------------
//  A template class for arrays of structures stored in an XML node
//
//  NOTE! NOTE! NOTE! You can't just add this module to a project,
//  because the compiler doesn't correctly instantiate the templated
//  classes when you do that. Instead, you have to include the class
//  definition below in either a using .h or .cpp file, and then copy
//  the template instantiation code in the .cpp file to the using
//  .cpp file.
//---------------------------------------------------------------------------


// We have to do a trick here to force the compiler to instantiate the
// template class code. If you get an 'unresolved external', try adding
// this function and that class to the module which uses TStructArray.

/*

static bool DoInit( void )
{
    TStructArray<MY_STRUCT> initRecs;

    MY_STRUCT temp;

    initRecs.Append( temp );
    initRecs.Modify( 0, temp );

    return true;
}

static bool initDone = DoInit();
*/


//
// TStructArray
//

//
// NOTE! NOTE! NOTE! Do not use memmove(), memcpy(), etc, where
// copying or moving members of the templated class. If the
// class contains Unicode strings, using mem...() will make bad
// things happen. You have to do an item by item copy instead.
//

template <class T> TStructArray<T>::TStructArray()
{
   m_allocElements = 16;
   m_allocIncrease = 16;
   m_nbrElements   = 0;

   m_pData = new T[m_allocElements];
}


template <class T> TStructArray<T>::~TStructArray()
{
  if( m_allocElements )
      delete [] m_pData;
}


template <class T> void TStructArray<T>::CheckAndAllocMem( int newNbrElements )
{
    // Checks to see if the object contains the number of records passed.
    // If not, allocates new memory and copies existing data over.
    if( newNbrElements >= m_allocElements )
    {
        // More memory required. Over-allocate it
        T *pTmp = new T[ newNbrElements + m_allocIncrease ];

        // Copy any existing elements over. Cannot use memcpy in case
        // element members are classes (eg, UnicodeString)
        if( m_nbrElements )
        {
            for( int iEl = 0; iEl < m_nbrElements; iEl++ )
                pTmp[iEl] = m_pData[iEl];

            delete [] m_pData;
        }

        m_pData = pTmp;
        m_allocElements = newNbrElements + m_allocIncrease;
    }
}


template <class T> TStructArray<T>& TStructArray<T>::operator =(const TStructArray &equ)
{
    try
    {
        if( this == &equ )
            return *this;

        CheckAndAllocMem( equ.m_nbrElements );

        // Copy data element by element. Cannot use memcpy() in case an
        // element member is a class
        for( int iEl = 0; iEl < equ.m_nbrElements; iEl++ )
            m_pData[iEl] = equ.m_pData[iEl];

        m_nbrElements = equ.m_nbrElements;

        return *this;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::operator =" );
        return *this;
    }
}


template <class T> TStructArray<T>& TStructArray<T>::operator += (const TStructArray &plus)
{
    try
    {
        CheckAndAllocMem( m_nbrElements + plus.m_nbrElements );

        // Copy data element by element. Cannot use memcpy() in case an
        // element member is a class
        for( int iEl = 0; iEl < plus.m_nbrElements; iEl++ )
            m_pData[m_nbrElements+iEl] = plus.m_pData[iEl];

        m_nbrElements += plus.m_nbrElements;

        return *this;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::operator +=" );
        return *this;
    }
}


template <class T> void TStructArray<T>::Append( const T& add )
{
    try
    {
        CheckAndAllocMem( m_nbrElements + 1 );

        m_pData[m_nbrElements++] = add;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::Append()" );
    }
}


template <class T> void TStructArray<T>::Insert( int index, const T& insert )
{
    // Inserts an item at the given index
    try
    {
        CheckAndAllocMem( m_nbrElements + 1 );

        for( int i = m_nbrElements; i > index; i-- )
            m_pData[i] = m_pData[i-1];

        m_pData[index] = insert;

        m_nbrElements++;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::Insert()" );
    }
}


template <class T> void TStructArray<T>::Modify( int index, const T& modify )
{
    if( ( index >= 0 ) && ( index < m_nbrElements ) )
    {
        m_pData[index] = modify;
    }
}


template <class T> void TStructArray<T>::Del( int index )
{
    if( ( index >= 0 ) && ( index < m_nbrElements ) )
    {
        for( int i = index; i < m_nbrElements - 1; i++ )
            m_pData[i] = m_pData[i+1];

        m_nbrElements--;
    }
}


template <class T> void TStructArray<T>::LoadFromNode( _di_IXMLNode aNode )
{
    // Assumes that each element T is stored under aNode under 0-based
    // key values (eg, "0" to "n-1" where n is the number of elements
    // in the array.

    // Clear all elements from array first
    Clear();

    if( aNode != NULL )
    {
        // The ChildNodes->Count property gives bogus values at times for some
        // reason. So don't use it. Instead, loop here until the next item
        // number is not found.
        int itemNbr = 0;

        while( itemNbr >= 0 )
        {
            T tempData;

            if( !GetEntryFromNode( aNode, ItemKeyName( itemNbr ), tempData ) )
                break;

            Append( tempData );

            itemNbr++;
        }
    }
}


template <class T> void TStructArray<T>::SaveToNode( _di_IXMLNode aNode )
{
    // Stores each element T under aNode using 0-based key values
    // (eg, "0" to "n-1" where n is the number of elements in the array).

    if( aNode != NULL )
    {
        // Delete any existing children (and presumably leaf nodes)
        aNode->ChildNodes->Clear();

        for( int iItem = 0; iItem < m_nbrElements; iItem++ )
            SaveEntryToNode( aNode, ItemKeyName( iItem ), m_pData[iItem] );
    }
}

