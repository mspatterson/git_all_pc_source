#ifndef TStructArrayH
#define TStructArrayH

    //---------------------------------------------------------------------------
    //  A template class for arrays of structures stored in an XML node
    //
    //  NOTE! NOTE! NOTE! You can't just add this module to a project,
    //  because the compiler doesn't correctly instantiate the templated
    //  classes when you do that. Instead, you have to include the class
    //  definition below in either a using .h or .cpp file, and then copy
    //  the template instantiation code in the .cpp file to the using
    //  .cpp file.
    //---------------------------------------------------------------------------

    #include <XMLIntf.hpp>

    template <class T> class TStructArray
    {
      private:
        int m_allocElements;      // number of elements memory currently allocated for
        int m_allocIncrease;      // incremental size of memory added when more memory needed
        T*  m_pData;              // pointer to the element array
        int m_nbrElements;        // number of elements in the array (<= m_allocElements)

        void CheckAndAllocMem( int nbrElements );

      public:
        TStructArray();
        ~TStructArray();

        const T& operator[] (int i) const { return m_pData[i]; }

        TStructArray& operator  = ( const TStructArray& equ );
        TStructArray& operator += ( const TStructArray& plus );

        void Clear( void ) { m_nbrElements = 0; }

        void Append( const T &add );
        void Insert( int index, const T& insert );
        void Del( int index );
        void Modify( int index, const T& modify );

        void LoadFromNode( _di_IXMLNode aNode );
        void SaveToNode( _di_IXMLNode aNode );

        __property int Count = { read = m_nbrElements };
    };


#endif
