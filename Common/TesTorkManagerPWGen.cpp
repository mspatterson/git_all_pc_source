#include <vcl.h>
#pragma hdrstop

#include "TesTorkManagerPWGen.h"

#pragma package(smart_init)


// Calculate the password on the current date
String GeneratePassword( void )
{
    String sResult = "RESET-";
    int iDay = Date() % 1000;

    sResult = sResult + GenerateChars( iDay );

    return sResult;
}


// Calculate the password on the given date
String GeneratePassword( TDate tDay )
{
    String sResult = "RESET-";
    int iDay = tDay % 1000;

    sResult = sResult + GenerateChars( iDay );

    return sResult;
}


// Calculate the characters used for the given day
String GenerateChars( int iDay )
{
    String sChars = "";

    for( int iChar = 0; iChar < CHARACTER_COUNT; iChar++ )
    {
        char cNextChar = '0';

        if( ( ( iChar % 3 ) % 2 ) == 1 )
        {
            cNextChar = ToChar( (int) sqrt( pow( ( iDay % 10 ) + 1, ( iChar + 3 ) ) ) );
        }
        else if( iChar % 2 == 0 )
        {
            cNextChar = ToChar( pow( iChar + 1, ( iDay % 10 ) ) );
        }
        else
        {
            cNextChar = ToChar( ( iChar + 3 ) * ( iDay + 3 ) );
        }

        sChars = sChars + cNextChar;
    }

    return sChars;
}


// Converts an integer to a character ( 0-9 = 0-9, 10-34 = A-Z, 36-61 = a-z )
char ToChar( int iValue )
{
    iValue = iValue % 62;

    char cReturn;

    if( iValue < 10 )
        cReturn = (char)( (BYTE)( iValue + 48 ) );
    else if( iValue <= 35 )
        cReturn = (char)( (BYTE)( iValue + 55 ) );
    else
        cReturn = (char)( (BYTE)( iValue + 61 ) );

    return cReturn;
}
