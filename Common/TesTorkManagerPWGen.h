#ifndef TesTorkManagerPWGenH
#define TesTorkManagerPWGenH

    #define CHARACTER_COUNT     10

    String  GeneratePassword( void );
    String  GeneratePassword( TDate tDay );

    String  GenerateChars( int iDay );
    char    ToChar( int iValue );

#endif
