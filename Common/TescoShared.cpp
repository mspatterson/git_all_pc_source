#include <vcl.h>
#pragma hdrstop

#include "TescoShared.h"

#pragma package(smart_init)


const TColor clTESCOBlue   = (TColor)0xD09333;
const TColor clTESCORed    = (TColor)0x3A40E7;
const TColor clBtnDisabled = (TColor)0x404040;


BoolStatus GetBoolStatus( BoolCmd bcCurrCmd, BoolState bsCurrState )
{
    switch( bsCurrState )
    {
        case eBS_Unknown:
            return eBSt_Unknown;

        case eBS_Off:
            if( bcCurrCmd == eBC_TurnOn )
                return eBSt_TurningOn;
            else
                return eBSt_Off;

        case eBS_On:
            if( bcCurrCmd == eBC_TurnOff )
                return eBSt_TurningOff;
            else
                return eBSt_On;
    }

    // Fall through should never happen
    return eBSt_Unknown;
}

