//---------------------------------------------------------------------------
//
//   Common TESCO-Specific Constants, Types, and Methods
//
//---------------------------------------------------------------------------

#ifndef TescoSharedH
#define TescoSharedH

    extern const TColor clTESCOBlue;
    extern const TColor clTESCORed;
    extern const TColor clBtnDisabled;

    //------------------------------------------------------------------------------
    // Boolean Data types

    // BoolStatus is used to report the status of elements that can be turned on or off.
    typedef enum {
        eBSt_Unknown,     // State of the element is unknown
        eBSt_Off,         // The element is off
        eBSt_On,          // The element is on
        eBSt_TurningOff,  // The element is on, but client has requested it turn off
        eBSt_TurningOn    // The element is off, but client has requested it turn on
    } BoolStatus;

    // BoolCmd is used by clients to request that elements be turned on or off.
    typedef enum {
        eBC_NoCmd,        // Client is not requesting any change to the element
        eBC_TurnOff,      // Client wants the element turned off
        eBC_TurnOn,       // Client wants the element turned on
    } BoolCmd;

    // BoolState is used to track the current state of an element
    typedef enum {
        eBS_Unknown,      // State of the element is unknown
        eBS_Off,          // The element is off
        eBS_On,           // The element is on
    } BoolState;

    BoolStatus GetBoolStatus( BoolCmd bcCurrCmd, BoolState bsCurrState );
        // Determines a BoolStatus based on the passed current command and state

#endif

