//
// Generic, project-independent type definitions
//

#ifndef TYPEDEFS_H
    #define TYPEDEFS_H

    typedef union
    {
        BYTE  byData[4];
        WORD  wData[2];
        DWORD dwData;
        float fData;
        int   iData;

        void SwapWords( void ) { WORD wTemp  = wData[0];  wData[0]  = wData[1];  wData[1]  = wTemp; }
        void SwapBytes( void ) { BYTE byTemp = byData[0]; byData[0] = byData[1]; byData[1] = byTemp; byTemp = byData[2]; byData[2] = byData[3]; byData[3] = byTemp; }

    } XFER_BUFFER;

#endif

