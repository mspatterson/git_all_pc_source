//************************************************************************
//
//  Comm32.H: Provides serial handling for Microlynx 32 bit applications.
//            It is a common file for many applications. This source must
//            not contain any application specific code.
//
//  Copyright (c) 2003, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************


#ifndef UDPCommH
    #define UDPCommH

    #include "CommObj.h"

    #include <winsock2.h>


    #define IP_ADDR_LEN   4
    #define MAX_ENET_PKT  1520


    bool InitWinsock( void );
        // Must be called before constructing any TCP/IP objects

    void ShutdownWinsock( void );
        // Must be called after destroying all TCP/IP objects

        
    class CUDPPort : public CCommObj {

      protected:

      public:

          __fastcall CUDPPort( void );
          virtual __fastcall ~CUDPPort();

        typedef struct
        {
            WORD portNbr;
            UnicodeString destAddr;
            bool acceptBroadcasts;
        } CONNECT_PARAMS;

        COMM_RESULT Connect( void* pConnectParams );

        void Disconnect( void );

        DWORD CommRecv( BYTE* byBuff, DWORD dwMaxBytes );
        DWORD CommSend( BYTE* byBuff, DWORD dwNbrBytes );

      private:

        SOCKET udpSrcSkt;
        int    maxDatagram;
        bool   dataLost;

        UnicodeString destAddr;
        WORD destPort;

        void __fastcall CloseSocket( SOCKET* aSocket );

    };

#endif
