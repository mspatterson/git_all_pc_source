//************************************************************************
//
//  USBPortDiscovery.h: implements helper functions for USB serial ports
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include <setupapi.h>
#include <devguid.h>
#include <regstr.h>

#include "USBPortDiscovery.h"

#pragma package(smart_init)


bool GetUSBPort( const AnsiString friendlyText, AnsiString& portStr )
{
    // Can't seem to get SetupDiEnumDeviceInfo() to cough up any info when
    // asking for just devices related to the ports GUID. So just get all
    // the friendly strings and look for ours.
    bool havePort = false;

    portStr = "";

    // Create a HDEVINFO with all present devices.
    HDEVINFO hDevInfo = SetupDiGetClassDevs( NULL, NULL, NULL, DIGCF_PRESENT | DIGCF_ALLCLASSES );

    if( hDevInfo == INVALID_HANDLE_VALUE )
        return false;

   // Enumerate through all devices returned by Windows. Look for our specific one.
   SP_DEVINFO_DATA DeviceInfoData;

   DeviceInfoData.cbSize = sizeof( SP_DEVINFO_DATA );

   for( DWORD i = 0; SetupDiEnumDeviceInfo( hDevInfo, i, &DeviceInfoData ); i++ )
   {
       char*  buffer    = NULL;
       DWORD  buffersize = 0;
       DWORD  dataType;

       // Call function with null to begin with, then use the returned
       // buffer size (doubled) to Alloc the buffer. Keep calling until
       // success or an unknown failure.
       //
       //  Double the returned buffersize to correct for underlying legacy
       //  CM functions that return an incorrect buffersize value on
       //  DBCS/MBCS systems.
       while( !SetupDiGetDeviceRegistryProperty( hDevInfo,
                                                 &DeviceInfoData,
                                                 SPDRP_FRIENDLYNAME, // SPDRP_FRIENDLYNAME, SPDRP_DEVICEDESC
                                                 &dataType,
                                                 (PBYTE)buffer,
                                                 buffersize,
                                                 &buffersize ) )
       {
           if( GetLastError() == ERROR_INSUFFICIENT_BUFFER )
           {
               // Change the buffer size.
               if( buffer != NULL )
                   LocalFree( buffer );

               // Double the size to avoid problems on
               // W2k MBCS systems per KB 888609.
               buffer = (char*)LocalAlloc( LPTR, buffersize * 2 );
           }
           else
           {
               break;
           }
       }

       if( buffer != NULL )
       {
           #ifdef UNICODE
               UnicodeString devText = (WideChar*)buffer;
               UnicodeString searchText( friendlyText );
           #else
               AnsiString devText = buffer;
               AnsiString searchText( friendlyText );
           #endif

           if( devText.Pos( searchText ) > 0 )
           {
               // Found our entry. Extract the com port string
               int comStartPos = devText.Pos( "(" );
               int comEndPos   = devText.Pos( ")" );

               if( ( comStartPos > 0 ) && ( comEndPos > 0 ) && ( comEndPos > comStartPos ) )
               {
                   portStr  = devText.SubString( comStartPos + 1, comEndPos - comStartPos - 1 );
                   havePort = true;
               }
           }

           LocalFree( buffer );
       }

       // Can quit if we've found our entry
       if( havePort )
           break;
   }

   //  Cleanup
   SetupDiDestroyDeviceInfoList( hDevInfo );

   return havePort;
}


bool GetPortNames( TStringList* pPortNames )
{
    // Create a HDEVINFO with all present devices.
    HDEVINFO hDevInfo = SetupDiGetClassDevs( NULL, NULL, NULL, DIGCF_PRESENT | DIGCF_ALLCLASSES );

    if( hDevInfo == INVALID_HANDLE_VALUE )
        return false;

   // Enumerate through all devices returned by Windows. Look for our specific one.
   SP_DEVINFO_DATA DeviceInfoData;

   DeviceInfoData.cbSize = sizeof( SP_DEVINFO_DATA );

   for( DWORD i = 0; SetupDiEnumDeviceInfo( hDevInfo, i, &DeviceInfoData ); i++ )
   {
       char*  buffer    = NULL;
       DWORD  buffersize = 0;
       DWORD  dataType;

       // Call function with null to begin with, then use the returned
       // buffer size (doubled) to Alloc the buffer. Keep calling until
       // success or an unknown failure.
       //
       //  Double the returned buffersize to correct for underlying legacy
       //  CM functions that return an incorrect buffersize value on
       //  DBCS/MBCS systems.
       while( !SetupDiGetDeviceRegistryProperty( hDevInfo,
                                                 &DeviceInfoData,
                                                 SPDRP_FRIENDLYNAME, // SPDRP_FRIENDLYNAME, SPDRP_DEVICEDESC
                                                 &dataType,
                                                 (PBYTE)buffer,
                                                 buffersize,
                                                 &buffersize ) )
       {
           if( GetLastError() == ERROR_INSUFFICIENT_BUFFER )
           {
               // Change the buffer size.
               if( buffer != NULL )
                   LocalFree( buffer );

               // Double the size to avoid problems on
               // W2k MBCS systems per KB 888609.
               buffer = (char*)LocalAlloc( LPTR, buffersize * 2 );
           }
           else
           {
               break;
           }
       }

       if( buffer != NULL )
       {
           #ifdef UNICODE
               UnicodeString devText = (WideChar*)buffer;
           #else
               AnsiString    devText = buffer;
           #endif

           pPortNames->Add( devText );

           LocalFree( buffer );
       }
   }

   //  Cleanup
   SetupDiDestroyDeviceInfoList( hDevInfo );

   return true;
}


bool GetAssociatedPorts( int iRadioPort, TList* pAssociatedPorts )
{
    // Get the list of all port strings
    TStringList* pCompPorts = new TStringList();
    String       sPortName  = "(COM" + IntToStr( iRadioPort ) + ")";

    // Clear the given list to be safe
    pAssociatedPorts->Clear();

    // Fill the string list
    if( !GetPortNames( pCompPorts ) )
        return false;

    String sUsbName = "";

    // First, iterate through the list of port strings, and
    // find the friendly string for our usb device
    for( int iPortIndex = 0; iPortIndex < pCompPorts->Count; iPortIndex++ )
    {
        int iCOMIndex = pCompPorts->Strings[iPortIndex].Pos( sPortName );

        // Check if this port is our base radio
        if( iCOMIndex != 0 )
        {
            // If so, get the usb device name from the start of the string
            sUsbName = pCompPorts->Strings[iPortIndex].SubString( 0, iCOMIndex - 1 ).Trim();

            break;
        }
    }

    // If we never found the Base Radio Port, return false
    if( sUsbName == "" )
        return false;

    // Next, iterate through the list of port strings and add the
    // port number to our list from each port using our Usb device
    for( int iPortIndex = 0; iPortIndex < pCompPorts->Count; iPortIndex++ )
    {
        String sCurrPort  = pCompPorts->Strings[iPortIndex];
        int iUsbNameIndex = sCurrPort.Pos( sUsbName );

        // Check if this port is from our usb device
        if( iUsbNameIndex != 0 )
        {
            // If so, cut out the port number and add to our list
            sPortName = sCurrPort.SubString( sUsbName.Length() + 1, sCurrPort.Length() + 1 - sUsbName.Length() );

            // Trim so we know the offsets
            sPortName = sPortName.Trim();

            // Constants come from the length of "(COM"
            String sTest = sPortName.SubString( 5, sPortName.Length() - 5 );
            int iNewPort = sPortName.SubString( 5, sPortName.Length() - 5 ).ToIntDef( -1 );

            // Check that we actually got a valid port number, if so, add it
            if( iNewPort >= 0 )
                pAssociatedPorts->Add( (TObject*)( iNewPort ) );

        }
    }

    // Cleanup
    delete pCompPorts;

    // If no associated ports found, return false
    if( pAssociatedPorts->Count == 0 )
        return false;

    return true;
}
