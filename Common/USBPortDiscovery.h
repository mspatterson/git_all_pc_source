//************************************************************************
//
//  USBPortDiscovery.h: implements helper functions for USB serial ports
//
//************************************************************************
#ifndef USBPortDiscoveryH
#define USBPortDiscoveryH

    bool GetUSBPort( const AnsiString friendlyText, AnsiString& portStr );
        // Looks for a USB serial port that matches the passed friendly
        // text string. Returns true and the port number in portStr if
        // the port is found.

    bool GetPortNames( TStringList* pPortNames );
    bool GetAssociatedPorts( int iRadioPort, TList* pAssociatedPorts );

#endif
