#include <vcl.h>
#pragma hdrstop

#include "UnitsOfMeasure.h"

#pragma package(smart_init)

//
// This module implements functions related to units of measure
//

static const UnicodeString m_MeasureTextStrings[NBR_UNITS_OF_MEASURE][NBR_MEASURE_TYPES] = {
                 // Measure Name   Dist1 Long   Dist1 Short  Dist2 Long      Dist2 Short  Weight Short  W/Len Short  Torque Short
  /* Metric   */ { L"Metric",      L"meters",   L"m",        L"centimeters", L"cm",       L"kg",        L"kg/cm",    L"N-m"    },
  /* Imperial */ { L"Imperial",    L"feet",     L"ft",       L"inches",      L"in",       L"lb",        L"lb/ft",    L"ft-lb"  },
};


//
// Public Functions
//

UnicodeString GetUnitsText( UNITS_OF_MEASURE uomType, MEASURE_TYPE mtType )
{
    return m_MeasureTextStrings[uomType][mtType];
}

