//---------------------------------------------------------------------------
#ifndef UnitsOfMeasureH
#define UnitsOfMeasureH

    //
    // This module implements functions related to units of measure
    //

    typedef enum {
        UOM_METRIC,
        UOM_IMPERIAL,
        NBR_UNITS_OF_MEASURE
    } UNITS_OF_MEASURE;

    typedef enum {
        MT_MEASURE_NAME,
        MT_DIST1_LONG,       // meters, feet
        MT_DIST1_SHORT,
        MT_DIST2_LONG,       // centimeters, inches
        MT_DIST2_SHORT,
        MT_WEIGHT_SHORT,
        MT_WEIGHT_BY_LENGTH_SHORT,
        MT_TORQUE_SHORT,
        NBR_MEASURE_TYPES
    } MEASURE_TYPE;

    UnicodeString GetUnitsText( UNITS_OF_MEASURE uomType, MEASURE_TYPE mtType );


#endif
