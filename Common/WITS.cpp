#include <vcl.h>
#pragma hdrstop

#include "WITS.h"

#pragma package(smart_init)


const char* m_szWITSHeader         = "&&\r\n";
const char* m_szWITSTrailer        = "!!\r\n";
const int   m_sizeofWITSHeader     = 4;
const int   m_sizeofWITSTrailer    = 4;
const char* m_szWITSFieldDelim     = "\r\n";
const int   m_sizeofWITSFieldDelim = 2;


int CreateWITSPacket( int buffLen, BYTE* pBuffer, WITS_FIELD witsFields[], int nbrFields )
{
    // Be safe, check incoming values
    if( ( buffLen == 0 ) || ( nbrFields == 0 ) || ( pBuffer == NULL ) || ( witsFields == NULL ) )
        return 0;

    // Validate the buffer size. We should never have a problem since this
    // is a static function only used in this module.
    if( buffLen < m_sizeofWITSHeader + m_sizeofWITSTrailer )
        return 0;

    memmove( pBuffer, m_szWITSHeader, m_sizeofWITSHeader );

    BYTE* pNextField  = &( pBuffer[m_sizeofWITSHeader] );
    int   bytesCopied = m_sizeofWITSHeader;

    if( ( witsFields != NULL ) && ( nbrFields > 0 ) )
    {
        for( int iField = 0; iField < nbrFields; iField++ )
        {
            // Calculate space required for this field
            int fieldValLen   = strlen( witsFields[iField].szValue );
            int bytesRequired = 4 /*field name*/ + fieldValLen + m_sizeofWITSFieldDelim;

            // Make sure we still have room for the trailer
            if( buffLen - bytesCopied - bytesRequired < m_sizeofWITSTrailer )
                break;

            // We have room for the entire field. Add the components
            memmove( pNextField, witsFields[iField].fieldName, WITS_FIELD_NAME_LEN );

            bytesCopied += WITS_FIELD_NAME_LEN;
            pNextField   = &( pBuffer[bytesCopied] );

            memmove( pNextField, witsFields[iField].szValue, fieldValLen );

            bytesCopied += fieldValLen;
            pNextField   = &( pBuffer[bytesCopied] );

            memmove( pNextField, m_szWITSFieldDelim, m_sizeofWITSFieldDelim );

            bytesCopied += m_sizeofWITSFieldDelim;
            pNextField   = &( pBuffer[bytesCopied] );
        }
    }

    // We will always have room for the trailer here.
    memmove( pNextField, m_szWITSTrailer, m_sizeofWITSTrailer );

    return bytesCopied + m_sizeofWITSTrailer;
}


bool SetWITSField( WITS_FIELD& witsField, const AnsiString& sCode, const AnsiString& sValue )
{
    // This method assumes that if the code string is 4 chars long that it is valid
    if( sCode.Length() == WITS_FIELD_NAME_LEN )
    {
        memcpy( witsField.fieldName, sCode.c_str(), WITS_FIELD_NAME_LEN );
        strcpy( witsField.szValue,   sValue.c_str() );

        return true;
    }

    // Fall through means bad length
    return false;
}


bool IsValidWITSCode( const UnicodeString& testCode )
{
    if( testCode.Length() != WITS_FIELD_NAME_LEN )
        return false;

    // All zeroes are not allowed
    if( testCode == "0000" )
        return false;

    for( int iChar = 1; iChar <= WITS_FIELD_NAME_LEN; iChar++ )
    {
        if( ( testCode[iChar] < '0' ) || ( testCode[iChar] > '9' ) )
            return false;

    }

    // Fall through means we have a valid WITS code
    return true;
}


AnsiString WITSFieldNameAsStr( const WITS_FIELD& witsField )
{
    AnsiString sResult;

    for( int iChar = 0; iChar < WITS_FIELD_NAME_LEN; iChar++ )
        sResult = sResult + witsField.fieldName[iChar];

    return sResult;
}

