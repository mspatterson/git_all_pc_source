#ifndef WITSH
#define WITSH

    #define WITS_FIELD_NAME_LEN        4
    #define WITS_FIELD_MAX_VALUE_LEN   20
    #define WITS_MAX_BUFF_LEN          2048

    typedef struct {
        char fieldName[WITS_FIELD_NAME_LEN];       // Must always be four numeric characters, not null terminated
        char szValue[WITS_FIELD_MAX_VALUE_LEN];    // May or may not be used, depending on the function
    } WITS_FIELD;

    int        CreateWITSPacket( int buffLen, BYTE* pBuffer, WITS_FIELD witsFields[], int nbrFields );
    bool       SetWITSField( WITS_FIELD& witsField, const AnsiString& sCode, const AnsiString& sValue );
    bool       IsValidWITSCode( const UnicodeString& testCode );
    AnsiString WITSFieldNameAsStr( const WITS_FIELD& witsField );

#endif
