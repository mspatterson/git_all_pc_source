
//
// Type defintions common to all modules in the project
//

#ifndef WTTTS_TYPES_H
    #define WTTTS_TYPES_H


    typedef struct {
        DWORD msecs;
        int   rotation;
        float torque;
        float tension;
    } WTTTS_READING;


    //
    // Serial Port Definitions
    //
    // The default order of serial port enumeration by Windows is as follows:
    #define BASESTN_CONTROL_PORT_OFFSET        0
    #define BASESTN_TESTORK_RADIO_PORT_OFFSET  1
    #define BASESTN_UNUSED_RADIO_PORT_OFFSET   2
    #define BASESTN_MGMT_PORT_OFFSET           3

#endif

