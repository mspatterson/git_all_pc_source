#include <vcl.h>
#pragma hdrstop

#include "WTTTSProtocolUtils.h"
#include "TypeDefs.h"

#pragma package(smart_init)


//
// Private Declarations
//

// Wireless network defaults

static const WIRELESS_SYSTEM_INFO WirelessSysInfo[eWS_NbrWirelessSystems] = {
    //   system            radioNbr    chan list
    { /* eWS_TesTork  */   eBRN_1,     { 11, 15, 20, 25 } },
    { /* eWS_MPL1     */   eBRN_1,     { 11, 15, 20, 25 } },
    { /* eWS_LinkTilt */   eBRN_1,     { 11, 15, 20, 25 } },
    { /* eWS_MPLS2    */   eBRN_2,     { 12, 16, 21, 24 } }
};


// The WTTTS packet parser is table driven. To add a new command
// or response, do the following:
//
//   1. In the .h file, add a new WTTTS_CMD_ or WTTTS_RESP_ define
//      (if required)
//
//   2. If a new command or response has been defined, and that
//      item has a data payload, define a structure for the
//      payload in the .h file, and add a member of that structure
//      type to the specific union definition.
//
//   3. Finally, add a row to (for new cmds or responses) or
//      modify an existing entry in the the following tables. If
//      adding a row, always insert it before the 'null' end of
//      table record.
//
//   4. For base station commands, follow the same procedure above
//      but modify the base station specific defines in the .h
//      file instead.

typedef struct {
    bool haveRecDef;    // true for all defined records, false to indicate end of array
    BYTE cmdRespByte;   // One of the ..._CMD_... or ..._RESP_ defines
    WORD expDataLen;    // Required value for header pktLen param
} WTTTS_PKT_INFO;

static const WTTTS_PKT_INFO m_wtttsTxPktInfo[] = {
    { true,  WTTTS_CMD_NO_CMD,           0 },
    { true,  WTTTS_CMD_START_STREAM,     0 },
    { true,  WTTTS_CMD_STOP_STREAM,      0 },
    { true,  WTTTS_CMD_SET_RATE,         sizeof( WTTTS_RATE_PKT ) },
    { true,  WTTTS_CMD_QUERY_VER,        0 },
    { true,  WTTTS_CMD_ENTER_DEEP_SLEEP, 0 },
    { true,  WTTTS_CMD_SET_RF_CHANNEL,   sizeof( WTTTS_SET_CHAN_PKT )   },
    { true,  WTTTS_CMD_SET_RF_POWER,     sizeof( WTTTS_SET_RF_PWR_PKT ) },
    { true,  WTTTS_CMD_GET_CFG_DATA,     sizeof( WTTTS_CFG_ITEM )       },
    { true,  WTTTS_CMD_SET_CFG_DATA,     sizeof( WTTTS_CFG_DATA )       },
    { false, 0,                          0 }    // Must always be the last item
};

static const WTTTS_PKT_INFO m_wtttsRxPktInfo[] = {
    { true,  WTTTS_RESP_STREAM_DATA,     sizeof( WTTTS_STREAM_DATA )  },
    { true,  WTTTS_RESP_REQ_FOR_CMD_V1,  sizeof( WTTTS_RFC_V1_PKT )   },
    { true,  WTTTS_RESP_REQ_FOR_CMD_V2,  sizeof( WTTTS_RFC_V2_PKT )   },
    { true,  WTTTS_RESP_CFG_DATA,        sizeof( WTTTS_CFG_DATA )     },
    { true,  WTTTS_RESP_VER_PKT,         sizeof( WTTTS_VER_PKT )      },
    { false, 0,                          0 }    // Must always be the last item
};

static const WTTTS_PKT_INFO m_mplTxPktInfo[] = {
    { true,  WTTTS_CMD_NO_CMD,            0                               },
    { true,  WTTTS_CMD_SET_RATE,          sizeof( WTTTS_RATE_PKT )        },
    { true,  WTTTS_CMD_QUERY_VER,         0                               },
    { true,  WTTTS_CMD_ENTER_DEEP_SLEEP,  0                               },
    { true,  WTTTS_CMD_SET_RF_CHANNEL,    sizeof( WTTTS_SET_CHAN_PKT )    },
    { true,  WTTTS_CMD_SET_RF_POWER,      sizeof( WTTTS_SET_RF_PWR_PKT )  },
    { true,  WTTTS_CMD_GET_CFG_DATA,      sizeof( WTTTS_CFG_ITEM )        },
    { true,  WTTTS_CMD_SET_CFG_DATA,      sizeof( WTTTS_CFG_DATA )        },
    { true,  WTTTS_CMD_GET_MIN_MAX_VALS,  sizeof( WTTTS_REQ_MIN_MAX_PKT ) },
    { true,  WTTTS_CMD_SET_CONTACTS,      sizeof( WTTTS_PIB_CONTACT_PKT ) },
    { true,  WTTTS_CMD_SET_SOLENOIDS,     sizeof( WTTTS_PIB_SOLENOID_PKT )},
    { true,  WTTTS_CMD_SET_PIB_PARAMS,    sizeof( WTTTS_PIB_PARAMS_PKT )  },
    { true,  WTTTS_CMD_CLR_PIB_SWITCH,    sizeof( WTTTS_PIB_CLR_INPUT_PKT)},
    { false, 0,                           0 }    // Must always be the last item
};

static const WTTTS_PKT_INFO m_mplRxPktInfo[] = {
    { true,  WTTTS_RESP_REQ_FOR_CMD_MPL,  sizeof( WTTTS_MPL_RFC_PKT )     },
    { true,  WTTTS_RESP_CFG_DATA,         sizeof( WTTTS_CFG_DATA )        },
    { true,  WTTTS_RESP_VER_PKT,          sizeof( WTTTS_VER_PKT )         },
    { true,  WTTTS_RESP_MPL_MIN_MAX_VALS, sizeof( WTTTS_MPL_MIN_MAX_PKT ) },
    { false, 0,                           0 }    // Must always be the last item
};

static const WTTTS_PKT_INFO m_mpls2TxPktInfo[] = {
    { true,  WTTTS_CMD_NO_CMD,            0                               },
    { true,  WTTTS_CMD_SET_RATE,          sizeof( MPLS2_RATES_PKT )       },
    { true,  WTTTS_CMD_MPLS2_QUERY_VER,   sizeof( MPLS2_QUERY_VER_PKT )   },
    { true,  WTTTS_CMD_SET_RF_CHANNEL,    sizeof( WTTTS_SET_CHAN_PKT )    },
    { true,  WTTTS_CMD_SET_RF_POWER,      sizeof( WTTTS_SET_RF_PWR_PKT )  },
    { true,  WTTTS_CMD_GET_CFG_DATA,      sizeof( WTTTS_CFG_ITEM )        },
    { true,  WTTTS_CMD_SET_CFG_DATA,      sizeof( WTTTS_CFG_DATA )        },
    { true,  WTTTS_CMD_MARK_CHAN_IN_USE,  0                               },
    { true,  WTTTS_CMD_MPLS2_SET_PIB_OUT, sizeof( MPLS2_PIB_OUTS_PKT )    },
    { true,  WTTTS_CMD_MPLS2_SET_TEC_PAR, sizeof( MPLS2_TEC_PARAMS_PKT )  },
    { true,  WTTTS_CMD_MPLS2_SET_DET_PAR, sizeof( MPLS2_DET_PARAMS_PKT )  },
    { false, 0,                           0 }    // Must always be the last item
};

static const WTTTS_PKT_INFO m_mpls2RxPktInfo[] = {
    { true,  WTTTS_RESP_MPLS2_RFC,        sizeof( MPLS2_RFC_PKT)          },
    { true,  WTTTS_RESP_MPLS2_CFG_DATA,   sizeof( WTTTS_CFG_DATA )        },
    { true,  WTTTS_RESP_MPLS2_VER_PKT,    sizeof( MPLS2_VERSION_PKT )     },
    { true,  WTTTS_RESP_MPLS2_ACK_PKT,    sizeof( MPLS2_ACK_PKT )         },
    { false, 0,                           0 }    // Must always be the last item
};

static const WTTTS_PKT_INFO m_baseStnTxPktInfo[] = {
    { true,  BASE_STN_CMD_GET_STATUS,      0 },
    { true,  BASE_STN_CMD_SET_CHAN_NBR,    sizeof( BASE_STN_SET_RADIO_CHAN )    },
    { true,  BASE_STN_CMD_SET_RF_PWR,      sizeof( BASE_STN_SET_RADIO_PWR )     },
    { true,  BASE_STN_CMD_SET_FCC_MODE,    sizeof( BASE_STN_SET_FCC_TEST_MODE ) },
    { true,  BASE_STN_CMD_SET_CHAN_PARAM,  0                                    },
    { true,  BASE_STN_CMD_SET_UPDATE_RATE, sizeof( BASE_STN_SET_RATE_PKT )      },
    { true,  BASE_STN_CMD_SET_PWR_STATE,   0                                    },
    { true,  BASE_STN_CMD_SET_OUTPUTS,     sizeof( BASE_STN_SET_OUTPUTS_PKT )   },
    { true,  BASE_STN_CMD_RESET_FIRMWARE,  0                                    },
    { false, 0,                            0 }    // Must always be the last item
};

static const WTTTS_PKT_INFO m_baseStnRxPktInfo[] = {
    { true,  BASE_STN_RESP_STATUS,         sizeof( BASE_STATUS_STATUS_PKT )     },
    { false, 0,                            0 }    // Must always be the last item
};

// The following control structures are passed the the generic HaveWTTTSPkt parser
typedef struct {
    const WTTTS_PKT_INFO* pktInfoArray;
    BYTE                  expectedHdrByte;
    WORD                  minPktLen;
    WORD                  maxPktLen;
} WTTTS_CONTROL_STRUCT;

typedef enum {
    WCS_TESTORK_RX,
    WCS_TESTORK_TX,
    WCS_BASE_RX,
    WCS_BASE_TX,
    WCS_MPL_RX,
    WCS_MPL_TX,
    WCS_MPLS2_RX,
    WCS_MPLS2_TX,
    NBR_WTTTS_CTRL_STRUCT_ITEMS
} WTTTS_CTRL_STRUCT_ITEM;

static const WTTTS_CONTROL_STRUCT m_ctrlStructs[NBR_WTTTS_CTRL_STRUCT_ITEMS] = {
    // Item               WTTTS_PKT_INFO        expectedHdrByte   minPktLen              maxPktLen
    /* WCS_TESTORK_RX */  { m_wtttsRxPktInfo,   TESTORK_HDR_RX,   MIN_TESTORK_PKT_LEN,   MAX_TESTORK_PKT_LEN  },
    /* WCS_TESTORK_TX */  { m_wtttsTxPktInfo,   TESTORK_HDR_TX,   MIN_TESTORK_PKT_LEN,   MAX_TESTORK_PKT_LEN  },
    /* WCS_BASE_RX  */    { m_baseStnRxPktInfo, BASE_STN_HDR_RX,  MIN_BASE_STN_PKT_LEN,  MAX_BASE_STN_PKT_LEN },
    /* WCS_BASE_TX  */    { m_baseStnTxPktInfo, BASE_STN_HDR_TX,  MIN_BASE_STN_PKT_LEN,  MAX_BASE_STN_PKT_LEN },
    /* WCS_MPL_RX */      { m_mplRxPktInfo,     MPL_HDR_RX,       MIN_MPL_PKT_LEN,       MAX_MPL_PKT_LEN      },
    /* WCS_MPL_TX */      { m_mplTxPktInfo,     MPL_HDR_TX,       MIN_MPL_PKT_LEN,       MAX_MPL_PKT_LEN      },
    /* WCS_MPLS2_RX */    { m_mpls2RxPktInfo,   MPLS2_HDR_RX,     MIN_MPLS2_PKT_LEN,     MAX_MPLS2_PKT_LEN    },
    /* WCS_MPLS2_TX */    { m_mpls2TxPktInfo,   MPLS2_HDR_TX,     MIN_MPLS2_PKT_LEN,     MAX_MPLS2_PKT_LEN    },
};


static bool HaveWTTTSPkt( const WTTTS_CONTROL_STRUCT& ctrlStruct, BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, void* pktData )
{
    // Scans the passed buffer for a WTTTS command or response. Returns true if
    // a packet is found, in which case pktHdr and pktData vars are populated.
    // Returns false if no packet is found. In either case, data in pBuff is
    // shifted out (if necessary) and the remaining count of bytes in the buffer
    // is returned in param buffLen.
    if( pBuff == NULL )
        return false;

    if( buffLen < (DWORD)ctrlStruct.minPktLen )
        return false;

    DWORD bytesSkipped = 0;
    bool  havePkt      = false;

    while( bytesSkipped + ctrlStruct.minPktLen <= buffLen )
    {
        // Have enough bytes for a packet. Header byte must be the first
        // byte we see
        if( pBuff[bytesSkipped] != ctrlStruct.expectedHdrByte )
        {
            bytesSkipped++;
            continue;
        }

        // Looks like the start of a header. See if the rest of the packet
        // makes sense.
        WTTTS_PKT_HDR* pHdr = (WTTTS_PKT_HDR*)&( pBuff[bytesSkipped] );

        bool foundInfoItem = false;
        WORD dataLen       = 0;
        WORD expPktLen     = 0;

        for( int iInfoItem = 0; ; iInfoItem++ )
        {
            // If we've hit the end of the table, break out of the loop
            if( !ctrlStruct.pktInfoArray[iInfoItem].haveRecDef )
                break;

            if( ctrlStruct.pktInfoArray[iInfoItem].cmdRespByte != pHdr->pktType )
                continue;

            // Found a hdr / type pair match. Calc the number of bytes required
            // for this packet. That would be a header, the data portion, and
            // a checksum byte.
            dataLen   = ctrlStruct.pktInfoArray[iInfoItem].expDataLen;
            expPktLen = ctrlStruct.minPktLen + dataLen;

            foundInfoItem = true;
            break;
        }

        if( !foundInfoItem )
        {
            bytesSkipped++;
            continue;
        }

        // If we have enough bytes, check the CRC. Otherwise, we have to
        // wait for more bytes to come in
        if( bytesSkipped + expPktLen > buffLen )
            break;

        // Have enough bytes - validate the CRC
        BYTE expCRC = CalcWTTTSChecksum( &( pBuff[bytesSkipped] ), expPktLen - 1 );

        if( pBuff[ bytesSkipped + expPktLen - 1 ] == expCRC )
        {
            // Success!
            memcpy( &pktHdr, &( pBuff[bytesSkipped] ), sizeof( WTTTS_PKT_HDR ) );

            if( dataLen > 0 )
                memcpy( pktData, &( pBuff[bytesSkipped + sizeof( WTTTS_PKT_HDR )] ), dataLen );

            // Remove this packet from the buffer
            bytesSkipped += expPktLen;

            havePkt = true;
            break;
        }

        // CRC error - continue scanning
        bytesSkipped++;
    }

    // If we've skipped over any bytes, remove them from the buffer
    if( bytesSkipped > 0 )
    {
        if( bytesSkipped >= buffLen )
        {
            buffLen = 0;
        }
        else
        {
            memmove( pBuff, &( pBuff[bytesSkipped] ), buffLen - bytesSkipped );
            buffLen -= bytesSkipped;
        }
    }

    return havePkt;
}


//
// Public Constants
//

const WORD   DEFAULT_RFC_RATE          = 250;    // msecs
const WORD   DEFAULT_RFC_TIMEOUT       = 100;    // msecs
const WORD   DEFAULT_STREAM_RATE       =  10;    // msecs
const WORD   DEFAULT_STREAM_TIMEOUT    = 300;    // secs
const WORD   DEFAULT_PAIRING_TIMEOUT   = 900;    // secs
const WORD   DEFAULT_SOLENOID_TIMEOUT  =  60;    // secs
const time_t DEFAULT_PACKET_TIMEOUT    =   5;    // secs

const UnicodeString sBatteryTypes[NBR_BATTERY_TYPES] =
{
    "Unknown",
    "Lithium",
    "NiMH (4000mA/h)",
    "NiMH (6050mA/h)",
};


//
// Public Functions
//

bool HaveTesTORKCmdPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, TESTORK_DATA_UNION& pktData )
{
    return HaveWTTTSPkt( m_ctrlStructs[WCS_TESTORK_TX], pBuff, buffLen, pktHdr, &pktData );
}


bool HaveTesTORKRespPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, TESTORK_DATA_UNION& pktData )
{
    return HaveWTTTSPkt( m_ctrlStructs[WCS_TESTORK_RX], pBuff, buffLen, pktHdr, &pktData );
}


bool HaveMPLCmdPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, MPL_DATA_UNION& pktData )
{
    return HaveWTTTSPkt( m_ctrlStructs[WCS_MPL_TX], pBuff, buffLen, pktHdr, &pktData );
}


bool HaveMPLRespPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, MPL_DATA_UNION& pktData )
{
    return HaveWTTTSPkt( m_ctrlStructs[WCS_MPL_RX], pBuff, buffLen, pktHdr, &pktData );
}


bool HaveMPLS2CmdPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, MPLS2_DATA_UNION& pktData )
{
    return HaveWTTTSPkt( m_ctrlStructs[WCS_MPLS2_TX], pBuff, buffLen, pktHdr, &pktData );
}


bool HaveMPLS2RespPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, MPLS2_DATA_UNION& pktData )
{
    return HaveWTTTSPkt( m_ctrlStructs[WCS_MPLS2_RX], pBuff, buffLen, pktHdr, &pktData );
}


bool HaveBaseStnCmdPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, BASE_STN_DATA_UNION& pktData )
{
    return HaveWTTTSPkt( m_ctrlStructs[WCS_BASE_TX], pBuff, buffLen, pktHdr, &pktData );
}


bool HaveBaseStnRespPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, BASE_STN_DATA_UNION& pktData )
{
    return HaveWTTTSPkt( m_ctrlStructs[WCS_BASE_RX], pBuff, buffLen, pktHdr, &pktData );
}


bool ConvertRFCPktToGeneric( GENERIC_RFC_PKT& genericPkt, const WTTTS_PKT_HDR& pktHdr, const TESTORK_DATA_UNION& pktData )
{
    // Converts a RFC V1 or V2 packet to generic format. Returns true on success
    // (will only fail if a non-RFC packet is passed to it)
    if( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_V1 )
    {
        // Create a sign-extended int value of the temperature
        int temp;

        if( pktData.rfcPktV1.temperature & 0x80 )
            temp = 0xFFFFFF00 | pktData.rfcPktV1.temperature;
        else
            temp = pktData.rfcPktV1.temperature;

        genericPkt.rfcV2DataPresent = false;
        genericPkt.fTemperature     = (float)temp / 2.0;
        genericPkt.battType         = pktData.rfcPktV1.battType;
        genericPkt.fBattVoltage     = (float)pktData.rfcPktV1.battVoltage / 1000.0;
        genericPkt.battUsed         = pktData.rfcPktV1.battUsed;
        genericPkt.pdsSwitch        = TriByteToInt( pktData.rfcPktV1.pdsSwitch );
        genericPkt.rpm              = pktData.rfcPktV1.rpm;
        genericPkt.lastReset        = pktData.rfcPktV1.lastReset;
        genericPkt.rfChannel        = pktData.rfcPktV1.rfChannel;
        genericPkt.currMode         = pktData.rfcPktV1.currMode;
        genericPkt.rfcRate          = pktData.rfcPktV1.rfcRate;
        genericPkt.rfcTimeout       = pktData.rfcPktV1.rfcTimeout;
        genericPkt.streamRate       = pktData.rfcPktV1.streamRate;
        genericPkt.streamTimeout    = pktData.rfcPktV1.streamTimeout;
        genericPkt.pairingTimeout   = pktData.rfcPktV1.pairingTimeout;
        genericPkt.torque045        = TriByteToInt( pktData.rfcPktV1.torque045 );
        genericPkt.torque225        = TriByteToInt( pktData.rfcPktV1.torque225 );
        genericPkt.tension000       = TriByteToInt( pktData.rfcPktV1.tension000 );
        genericPkt.tension090       = TriByteToInt( pktData.rfcPktV1.tension090 );
        genericPkt.tension180       = TriByteToInt( pktData.rfcPktV1.tension180 );
        genericPkt.tension270       = TriByteToInt( pktData.rfcPktV1.tension270 );
        genericPkt.torque045_R3     = 0;  // V2 packet only
        genericPkt.torque225_R3     = 0;  // V2 packet only
        genericPkt.compassX         = BiByteToShortInt( pktData.rfcPktV1.compassX );
        genericPkt.compassY         = BiByteToShortInt( pktData.rfcPktV1.compassY );
        genericPkt.compassZ         = BiByteToShortInt( pktData.rfcPktV1.compassZ );
        genericPkt.accelX           = BiByteToShortInt( pktData.rfcPktV1.accelX );
        genericPkt.accelY           = BiByteToShortInt( pktData.rfcPktV1.accelY );
        genericPkt.accelZ           = BiByteToShortInt( pktData.rfcPktV1.accelZ );
        genericPkt.rtdReading       = TriByteToInt( pktData.rfcPktV1.pdsSwitch );  // V2 packet only, but make same as PDS value
        genericPkt.byRFU1           = pktData.rfcPktV1.rfu;
        genericPkt.byRFU2[0]        = 0;  // V2 packet only
        genericPkt.byRFU2[1]        = 0;  // V2 packet only
        genericPkt.byRFU2[2]        = 0;  // V2 packet only
        genericPkt.byRFU2[3]        = 0;  // V2 packet only

        memset( genericPkt.deviceID, 0, DEV_ID_VAL_LEN );  // V2 packet only

        return true;
    }

    if( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_V2 )
    {
        // Create a sign-extended int value of the temperature
        int temp;

        if( pktData.rfcPktV2.temperature & 0x80 )
            temp = 0xFFFFFF00 | pktData.rfcPktV2.temperature;
        else
            temp = pktData.rfcPktV2.temperature;

        genericPkt.rfcV2DataPresent = true;
        genericPkt.fTemperature     = (float)temp / 2.0;
        genericPkt.battType         = pktData.rfcPktV2.battType;
        genericPkt.fBattVoltage     = (float)pktData.rfcPktV2.battVoltage / 1000.0;
        genericPkt.battUsed         = pktData.rfcPktV2.battUsed;
        genericPkt.pdsSwitch        = TriByteToInt( pktData.rfcPktV2.pdsSwitch );
        genericPkt.rpm              = pktData.rfcPktV2.rpm;
        genericPkt.lastReset        = pktData.rfcPktV2.lastReset;
        genericPkt.rfChannel        = pktData.rfcPktV2.rfChannel;
        genericPkt.currMode         = pktData.rfcPktV2.currMode;
        genericPkt.rfcRate          = pktData.rfcPktV2.rfcRate;
        genericPkt.rfcTimeout       = pktData.rfcPktV2.rfcTimeout;
        genericPkt.streamRate       = pktData.rfcPktV2.streamRate;
        genericPkt.streamTimeout    = pktData.rfcPktV2.streamTimeout;
        genericPkt.pairingTimeout   = pktData.rfcPktV2.pairingTimeout;
        genericPkt.torque045        = TriByteToInt( pktData.rfcPktV2.torque045 );
        genericPkt.torque225        = TriByteToInt( pktData.rfcPktV2.torque225 );
        genericPkt.tension000       = TriByteToInt( pktData.rfcPktV2.tension000 );
        genericPkt.tension090       = TriByteToInt( pktData.rfcPktV2.tension090 );
        genericPkt.tension180       = TriByteToInt( pktData.rfcPktV2.tension180 );
        genericPkt.tension270       = TriByteToInt( pktData.rfcPktV2.tension270 );
        genericPkt.torque045_R3     = TriByteToInt( pktData.rfcPktV2.torque045_R3 );
        genericPkt.torque225_R3     = TriByteToInt( pktData.rfcPktV2.torque225_R3 );
        genericPkt.compassX         = BiByteToShortInt( pktData.rfcPktV2.compassX );
        genericPkt.compassY         = BiByteToShortInt( pktData.rfcPktV2.compassY );
        genericPkt.compassZ         = BiByteToShortInt( pktData.rfcPktV2.compassZ );
        genericPkt.accelX           = BiByteToShortInt( pktData.rfcPktV2.accelX );
        genericPkt.accelY           = BiByteToShortInt( pktData.rfcPktV2.accelY );
        genericPkt.accelZ           = BiByteToShortInt( pktData.rfcPktV2.accelZ );
        genericPkt.rtdReading       = TriByteToInt( pktData.rfcPktV2.rtdReading );
        genericPkt.byRFU1           = pktData.rfcPktV2.rfu1;
        genericPkt.byRFU2[0]        = pktData.rfcPktV2.rfu2[0];
        genericPkt.byRFU2[1]        = pktData.rfcPktV2.rfu2[1];
        genericPkt.byRFU2[2]        = pktData.rfcPktV2.rfu2[2];
        genericPkt.byRFU2[3]        = pktData.rfcPktV2.rfu2[3];

        memcpy( genericPkt.deviceID, pktData.rfcPktV2.deviceID, DEV_ID_VAL_LEN );

        return true;
    }

    // Fall through means non-RFC packet passed to us
    return false;
}


BYTE CalcWTTTSChecksum( const BYTE pBuff[], int buffLen )
{
    // Calculates and returns the checksum for the passed buffer.
    // Returns zero if pBuff is NULL or buffLen == 0.
    if( ( pBuff == NULL ) || ( buffLen == 0 ) )
        return 0;

    const WORD const1 = 52845;
    const WORD const2 = 22719;

    WORD revolve = 55665;
    BYTE sum     = 0;

    for( int iByte = 0; iByte < buffLen; iByte++ )
    {
        WORD cipher = pBuff[iByte] ^ ( revolve >> 8 );

        revolve = ( cipher + revolve ) * const1 + const2;

        sum += cipher;
    }

    return sum;
}


void IncStat( DWORD& aStat, DWORD incrAmount )
{
    // Increments a statistic, watching for wrap
    const DWORD maxStat = 0xFFFFFFFF;

    if( aStat < maxStat )
    {
        DWORD roomLeft = maxStat - aStat;

        if( incrAmount < roomLeft )
            aStat += incrAmount;
        else
            aStat = maxStat;
    }
}


void ShiftBufferDown( BYTE pBuffer[], DWORD& buffLen, DWORD nbrBytes )
{
    // If we're shifting out as many or more bytes than are in the buffer,
    // the buffer will have nothing left.
    if( nbrBytes >= buffLen )
    {
        buffLen = 0;
        return;
    }

    // Shift the buffer contents down
    memmove( pBuffer, &(pBuffer[nbrBytes]), buffLen - nbrBytes );

    buffLen -= nbrBytes;
}


DWORD TimeSince( DWORD startTime )
{
    // Return number of msecs that have elapsed since the passed time.
    // Watch for the case where the current time is less than the
    // start time (and therefore has wrapped).
    DWORD currTime = GetTickCount();

    if( currTime >= startTime )
        return currTime - startTime;

    // Time has wrapped in this case
    return 0xFFFFFFFF - startTime + currTime;
}



bool HaveTimeout( DWORD startTime, DWORD timeoutValue )
{
    // Return true if a timeout has occurred. Because we have to support
    // Win XP, we can only use GetTickCount(), and not GetTickCount64().

    // Elminate simple cases first
    if( timeoutValue == 0 )
        return true;

    DWORD elapsedTime = TimeSince( startTime );

    return( elapsedTime > timeoutValue );
}


// Routines for converting byte arrays received in stream data
// packets to a 'natural' data type. Note that all readings
// are signed values.
INT16 BiByteToShortInt( const BYTE byteData[] )
{
    // byteData[] is expected to be an array of 2 bytes
    XFER_BUFFER xferBuff;

    xferBuff.byData[0] = byteData[0];
    xferBuff.byData[1] = byteData[1];

    return (INT16)xferBuff.wData[0];
}


int TriByteToInt( const BYTE byteData[] )
{
    // byteData[] is expected to be an array of 3 bytes. Note that
    // we have to sign-extend the value, if the high order bit is set.
    XFER_BUFFER xferBuff;

    xferBuff.byData[0] = byteData[0];
    xferBuff.byData[1] = byteData[1];
    xferBuff.byData[2] = byteData[2];

    if( ( byteData[2] & 0x80 ) == 0 )
        xferBuff.byData[3] = 0;
    else
        xferBuff.byData[3] = 0xFF;

    return (int)xferBuff.dwData;
}


int QuadByteToInt( const BYTE byteData[] )
{
    // byteData[] is expected to be an array of 4 bytes
    XFER_BUFFER xferBuff;

    xferBuff.byData[0] = byteData[0];
    xferBuff.byData[1] = byteData[1];
    xferBuff.byData[2] = byteData[2];
    xferBuff.byData[3] = byteData[3];

    return (int)xferBuff.dwData;
}


String DeviceIDToStr( const BYTE byDevChars[] )
{
    // Converts a device ID to a standard string
    String sDevID;
    sDevID.printf( L"%02x-%02x-%02x-%02x-%02x-%02x", byDevChars[0], byDevChars[1], byDevChars[2],
                                                     byDevChars[3], byDevChars[4], byDevChars[5] );

    return sDevID;
}


DWORD CalcElapsedTicks( WORD wLast, WORD wCurr )
{
    DWORD elapsedTicks = 0;

    // Timestamp needs some massaging, as it will wrap every
    // 65535 ticks. Each tick represents 10 msecs.
    if( wCurr >= wLast )
    {
        // Assume no wrap has occurred. Increment our
        // msecs counter by the amount of elapsed time.
        elapsedTicks = wCurr - wLast;
    }
    else
    {
        // Time has wrapped.
        elapsedTicks = wCurr + ( 0xFFFF - wLast );
    }

    return elapsedTicks;
}


String GetBattTypeText( BATTERY_TYPE aType )
{
    if( ( aType >=0 ) && ( aType < NBR_BATTERY_TYPES ) )
        return sBatteryTypes[aType];
    else
        return "Type " + IntToStr( aType );
}


BATTERY_TYPE GetBattTypeEnum( const String& battTypeText )
{
    for( int aType = 0; aType < NBR_BATTERY_TYPES; aType++ )
    {
        if( battTypeText.CompareIC( sBatteryTypes[aType] ) == 0 )
            return (BATTERY_TYPE)aType;
    }

    // Fall through means unknown battery type
    return BT_UNKNOWN;
}


bool GetWirelessSystemInfo( eWirelessSystem eWhichSystem, WIRELESS_SYSTEM_INFO& wirelessInfo )
{
    if( ( eWhichSystem < 0 ) || ( eWhichSystem >= eWS_NbrWirelessSystems ) )
        return false;

    wirelessInfo = WirelessSysInfo[eWhichSystem];

    return true;
}

