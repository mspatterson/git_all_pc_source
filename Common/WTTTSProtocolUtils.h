#ifndef WTTTSProtocolUtilsH
#define WTTTSProtocolUtilsH

#include <time.h>

//
// Packet Definitions for TESCO Wireless Communications
// Refer to Specification 153-0084 for further info
//
// Note: the capability exists to perform over-the-air updating of
// the device firmware. This capability is only implemented in a
// specialized software program. Therefore, all definitions related
// to that ability have been excluded from this file.
//


//
// Shared Definitions
//

// Packet formats (inbound and outbound). All structures are byte-aligned
#pragma pack(push, 1)

// Packet header. This header format is used for both WTTTS transactions
// and base station radio transactions.
typedef struct {
    BYTE pktHdr;       // xxx_HDR_... define
    BYTE pktType;      // xxx_CMD_... or xxx_RESP_... define
    BYTE seqNbr;       // Incremented on each tx packet. Rolls over to zero at 255
    BYTE dataLen;      // Number of bytes of packet data to follow; can be zero
    WORD timeStamp;    // Packet time, in 10's of msec, since device power-up
} WTTTS_PKT_HDR;

#define SIZEOF_WTTTS_CHECKSUM  1

#define TRI_BYTE_VAL_LEN   3
#define BI_BYTE_VAL_LEN    2
#define DWORD_VAL_LEN      4
#define DEV_ID_VAL_LEN     6

#define MAX_NBR_MPL_SENSOR_BOARDS   4


//
// Wireless Device Definitions
//

// Packet header defines
#define TESTORK_HDR_RX   0x29       // Inbound from TesTORK
#define TESTORK_HDR_TX   0x46       // Outbound to TesTORK

#define MPL_HDR_RX       0x15       // Inbound from MPL
#define MPL_HDR_TX       0x81       // Outbound to MPL

#define MPLS2_HDR_RX     0x15       // Inbound from MPLS
#define MPLS2_HDR_TX     0x81       // Outbound to MPLS


// Packet type defintions: outbound to Wireless Devices. Note that all
// Wireless Devices share these command values, but not all devices implement
// all commands.
#define WTTTS_CMD_NO_CMD            0x80
#define WTTTS_CMD_START_STREAM      0x82
#define WTTTS_CMD_STOP_STREAM       0x84
#define WTTTS_CMD_SET_RATE          0x86
#define WTTTS_CMD_QUERY_VER         0x8A
#define WTTTS_CMD_ENTER_DEEP_SLEEP  0x8C
#define WTTTS_CMD_SET_RF_CHANNEL    0x8E
#define WTTTS_CMD_SET_RF_POWER      0x8F
#define WTTTS_CMD_SET_CFG_DATA      0xA0
#define WTTTS_CMD_GET_CFG_DATA      0xA2
#define WTTTS_CMD_GET_MIN_MAX_VALS  0xA4
#define WTTTS_CMD_MARK_CHAN_IN_USE  0xA6
#define WTTTS_CMD_SET_CONTACTS      0xA8
#define WTTTS_CMD_SET_SOLENOIDS     0xAA
#define WTTTS_CMD_SET_PIB_PARAMS    0xAC
#define WTTTS_CMD_CLR_PIB_SWITCH    0xAE
#define WTTTS_CMD_MPLS2_SET_PIB_OUT 0xA8
#define WTTTS_CMD_MPLS2_SET_TEC_PAR 0xAC
#define WTTTS_CMD_MPLS2_SET_DET_PAR 0xAE
#define WTTTS_CMD_MPLS2_QUERY_VER   0x8A

// Packet type definitions: inbound from Wireless Devices.
#define WTTTS_RESP_STREAM_DATA      0x24
#define WTTTS_RESP_REQ_FOR_CMD_V1   0x26
#define WTTTS_RESP_VER_PKT          0x28
#define WTTTS_RESP_CFG_DATA         0x2A
#define WTTTS_RESP_REQ_FOR_CMD_V2   0x2C
#define WTTTS_RESP_REQ_FOR_CMD_MPL  0x2E
#define WTTTS_RESP_MPL_MIN_MAX_VALS 0x22
#define WTTTS_RESP_MPLS2_RFC        0x31
#define WTTTS_RESP_MPLS2_VER_PKT    0x33
#define WTTTS_RESP_MPLS2_CFG_DATA   0x35
#define WTTTS_RESP_MPLS2_ACK_PKT    0x37


// TesTORK stream data packet data
typedef struct {
    BYTE torque045[TRI_BYTE_VAL_LEN];        // Signed 24-bit value
    BYTE torque225[TRI_BYTE_VAL_LEN];        // Signed 24-bit value
    BYTE tension000[TRI_BYTE_VAL_LEN];       // Signed 24-bit value
    BYTE tension090[TRI_BYTE_VAL_LEN];       // Signed 24-bit value
    BYTE tension180[TRI_BYTE_VAL_LEN];       // Signed 24-bit value
    BYTE tension270[TRI_BYTE_VAL_LEN];       // Signed 24-bit value
    BYTE gyro[DWORD_VAL_LEN];                // Signed 32-bit value
    BYTE compassX[BI_BYTE_VAL_LEN];          // Signed 16-bit value
    BYTE compassY[BI_BYTE_VAL_LEN];          // Signed 16-bit value
    BYTE compassZ[BI_BYTE_VAL_LEN];          // Signed 16-bit value
    BYTE accelX[BI_BYTE_VAL_LEN];            // Signed 16-bit value
    BYTE accelY[BI_BYTE_VAL_LEN];            // Signed 16-bit value
    BYTE accelZ[BI_BYTE_VAL_LEN];            // Signed 16-bit value
} WTTTS_STREAM_DATA;

// TesTORK RFC packet V1
typedef struct {
    BYTE  temperature;                       // signed value, 0.5C per count with, -64C to +63.5C
    BYTE  battType;                          // 1 = lithium, 2 = NiMH
    WORD  battVoltage;                       // Voltage in mV (eg 3.742V would be 3742)
    WORD  battUsed;                          // Value returns number of mA/h used since last time the battery was inserted
    BYTE  pdsSwitch[TRI_BYTE_VAL_LEN];       // Signed 24-bit value
    BYTE  rpm;                               // current RPM
    BYTE  lastReset;                         // 1 = power-up, 2 = WDT, 3 = brownout reset
    BYTE  rfChannel;                         // 11 through 26
    BYTE  currMode;                          // 0 - entering deep sleep, 1 = normal, 2 = low-power
    BYTE  rfu;
    WORD  rfcRate;
    WORD  rfcTimeout;
    WORD  streamRate;
    WORD  streamTimeout;
    WORD  pairingTimeout;
    BYTE  torque045[TRI_BYTE_VAL_LEN];       // Signed 24-bit value
    BYTE  torque225[TRI_BYTE_VAL_LEN];       // Signed 24-bit value
    BYTE  tension000[TRI_BYTE_VAL_LEN];      // Signed 24-bit value
    BYTE  tension090[TRI_BYTE_VAL_LEN];      // Signed 24-bit value
    BYTE  tension180[TRI_BYTE_VAL_LEN];      // Signed 24-bit value
    BYTE  tension270[TRI_BYTE_VAL_LEN];      // Signed 24-bit value
    BYTE  compassX[BI_BYTE_VAL_LEN];         // Signed 16-bit value
    BYTE  compassY[BI_BYTE_VAL_LEN];         // Signed 16-bit value
    BYTE  compassZ[BI_BYTE_VAL_LEN];         // Signed 16-bit value
    BYTE  accelX[BI_BYTE_VAL_LEN];           // Signed 16-bit value
    BYTE  accelY[BI_BYTE_VAL_LEN];           // Signed 16-bit value
    BYTE  accelZ[BI_BYTE_VAL_LEN];           // Signed 16-bit value
} WTTTS_RFC_V1_PKT;

// TesTORK RFC packet V2
typedef struct {
    BYTE  temperature;                       // signed value, 0.5C per count with, -64C to +63.5C
    BYTE  battType;                          // 1 = lithium, 2 = NiMH
    WORD  battVoltage;                       // Voltage in mV (eg 3.742V would be 3742)
    WORD  battUsed;                          // Value returns number of mA/h used since last time the battery was inserted
    BYTE  pdsSwitch[TRI_BYTE_VAL_LEN];       // Signed 24-bit value
    BYTE  rpm;                               // current RPM
    BYTE  lastReset;                         // 1 = power-up, 2 = WDT, 3 = brownout reset
    BYTE  rfChannel;                         // 11 through 26
    BYTE  currMode;                          // 0 - entering deep sleep, 1 = normal, 2 = low-power
    BYTE  rfu1;                              // Available for debug / test purposes
    BYTE  rfu2[4];                           // Available for debug / test purposes
    WORD  rfcRate;
    WORD  rfcTimeout;
    WORD  streamRate;
    WORD  streamTimeout;
    WORD  pairingTimeout;
    BYTE  torque045[TRI_BYTE_VAL_LEN];       // Signed 24-bit value
    BYTE  torque225[TRI_BYTE_VAL_LEN];       // Signed 24-bit value
    BYTE  tension000[TRI_BYTE_VAL_LEN];      // Signed 24-bit value
    BYTE  tension090[TRI_BYTE_VAL_LEN];      // Signed 24-bit value
    BYTE  tension180[TRI_BYTE_VAL_LEN];      // Signed 24-bit value
    BYTE  tension270[TRI_BYTE_VAL_LEN];      // Signed 24-bit value
    BYTE  torque045_R3[TRI_BYTE_VAL_LEN];    // Signed 24-bit value
    BYTE  torque225_R3[TRI_BYTE_VAL_LEN];    // Signed 24-bit value
    BYTE  compassX[BI_BYTE_VAL_LEN];         // Signed 16-bit value
    BYTE  compassY[BI_BYTE_VAL_LEN];         // Signed 16-bit value
    BYTE  compassZ[BI_BYTE_VAL_LEN];         // Signed 16-bit value
    BYTE  accelX[BI_BYTE_VAL_LEN];           // Signed 16-bit value
    BYTE  accelY[BI_BYTE_VAL_LEN];           // Signed 16-bit value
    BYTE  accelZ[BI_BYTE_VAL_LEN];           // Signed 16-bit value
    BYTE  rtdReading[TRI_BYTE_VAL_LEN];      // Signed 24-bit value
    BYTE  deviceID[DEV_ID_VAL_LEN];          // Unique device ID from serial number chip
} WTTTS_RFC_V2_PKT;

// MPL RFC packet
typedef struct {
    BYTE  temperature;                       // signed value, 0.5C per count with, -64C to +63.5C
    BYTE  battType;                          // 1 = lithium, 2 = NiMH
    WORD  battVoltage;                       // Voltage in mV (eg 3.742V would be 3742)
    WORD  battUsed;                          // Value returns number of mA/h used since last time the battery was inserted
    BYTE  pressure[TRI_BYTE_VAL_LEN];        // Signed 24-bit value
    BYTE  rpm;                               // current RPM
    BYTE  lastReset;                         // 1 = power-up, 2 = WDT, 3 = brownout reset
    BYTE  rfChannel;                         // 11 through 26
    BYTE  currMode;                          // 0 - entering deep sleep, 1 = normal, 2 = low-power
    BYTE  contactState;                      // Bit flags D0 through D3 for contacts, D4 = SW1, D5 = SW2
    BYTE  inputState;                        // Bit flags, D0/D1 = Sensor 1, D2/D3 = Sensor 2, etc
    BYTE  activeSolenoid;                    // Values 1 through 6, 0 if no solenoid is active
    BYTE  rfu2[4];                           // Available for debug / test purposes
    WORD  rfcRate;
    WORD  rfcTimeout;
    WORD  solenoidTimeout;
    BYTE  sensorSamplingRate;                // Enum value: 0 through 8, refer to spec
    BYTE  sensorAvgFactor;                   // Allowed values: 1, 2, 4, 8, 16, 32, 64, or 128
    WORD  pairingTimeout;
    BYTE  deviceID[DEV_ID_VAL_LEN];          // Unique device ID from serial number chip
    BYTE  activeSensors[MAX_NBR_MPL_SENSOR_BOARDS];              // Sensors being read and reported in this packet
    BYTE  sensXAvg[MAX_NBR_MPL_SENSOR_BOARDS][BI_BYTE_VAL_LEN];  // Signed 16-bit value
    BYTE  sensYAvg[MAX_NBR_MPL_SENSOR_BOARDS][BI_BYTE_VAL_LEN];  // Signed 16-bit value
    BYTE  sensZAvg[MAX_NBR_MPL_SENSOR_BOARDS][BI_BYTE_VAL_LEN];  // Signed 16-bit value
} WTTTS_MPL_RFC_PKT;

// MPL min / max event packet
typedef struct {
    BYTE  activeSensors[MAX_NBR_MPL_SENSOR_BOARDS];              // Sensors being read and reported in this packet
    BYTE  XMin[MAX_NBR_MPL_SENSOR_BOARDS][BI_BYTE_VAL_LEN];      // Signed 16-bit value
    BYTE  XMax[MAX_NBR_MPL_SENSOR_BOARDS][BI_BYTE_VAL_LEN];      // Signed 16-bit value
    BYTE  YMin[MAX_NBR_MPL_SENSOR_BOARDS][BI_BYTE_VAL_LEN];      // Signed 16-bit value
    BYTE  YMax[MAX_NBR_MPL_SENSOR_BOARDS][BI_BYTE_VAL_LEN];      // Signed 16-bit value
    BYTE  ZMin[MAX_NBR_MPL_SENSOR_BOARDS][BI_BYTE_VAL_LEN];      // Signed 16-bit value
    BYTE  ZMax[MAX_NBR_MPL_SENSOR_BOARDS][BI_BYTE_VAL_LEN];      // Signed 16-bit value
} WTTTS_MPL_MIN_MAX_PKT;


// This structure represents the superset of RFC data that can be received
// from a TesTORK. A helper method converts a V1 or V2 packeet to this
// generic format.
typedef struct {
    bool  rfcV2DataPresent;   // True if the V2 fields are valid in this packet
    float fTemperature;
    BYTE  battType;      // 1 = lithium, 2 = NiMH
    float fBattVoltage;
    WORD  battUsed;      // Value returns number of mA/h used since last time the battery was inserted
    int   pdsSwitch;
    BYTE  byRFU1;
    BYTE  byRFU2[4];
    BYTE  rpm;           // current RPM
    BYTE  lastReset;     // 1 = power-up, 2 = WDT, 3 = brownout reset
    BYTE  rfChannel;     // 11 through 26
    BYTE  currMode;      // 0 - entering deep sleep, 1 = normal, 2 = low-power
    WORD  rfcRate;
    WORD  rfcTimeout;
    WORD  streamRate;
    WORD  streamTimeout;
    WORD  pairingTimeout;
    int   torque045;
    int   torque225;
    int   tension000;
    int   tension090;
    int   tension180;
    int   tension270;
    int   torque045_R3;
    int   torque225_R3;
    int   compassX;
    int   compassY;
    int   compassZ;
    int   accelX;
    int   accelY;
    int   accelZ;
    int   rtdReading;
    BYTE  deviceID[DEV_ID_VAL_LEN];
} GENERIC_RFC_PKT;


// Payload for set rate command.
typedef enum {
    SRT_RFC_RATE,                   // Sets RFC tx rate, in msecs
    SRT_RFC_TIMEOUT,                // Sets how long a device receiver remains active after sending a RFC
    SRT_STREAM_RATE,                // Sets the rate at which TesTORK stream packets are reported
    SRT_STREAM_TIMEOUT,             // Sets how long the TesTORK will send stream packets before automatically stopping
    SRT_PAIR_TIMEOUT,               // Sets how long before a device enters 'deep sleep' if it can't connect
    SRT_SOLENOID_TIMEOUT,           // Sets how long a MPL solenoid can stay closed for
    NBR_SET_RATE_TYPES
} WTTTS_SET_RATE_TYPE;

typedef struct {
    BYTE  rateType;                 // One of the SRT_ enums from above
    BYTE  rfu;                      // Must always be zero
    WORD  newValue;                 // Value in secs or msecs, see enum defs above
} WTTTS_RATE_PKT;

// The following payloads are used to get / set configuration information.
// Configuration data is stored in in pages. Each page consists of 16 bytes
// of data. All pages can stored in a device can be read. However, the host
// can only write to pages 8 and onward.
//
// The page number does not directly correlate with the area in memory
// where information is stored. The pages are mapped as follows:
//
// Info Area    Device Memory Addr Range  RO/RdWr   Protocol Page Nbr
//  Info A      0x1980 to 0x19FF            RO        0 -  7
//  Info B      0x1900 to 0x197F           RdWr       8 - 15
//  Info C      0x1880 to 0x18FF           RdWr      16 - 23
//  Info D      0x1800 to 0x187F           RdWr      24 - 31
//
// Allocation of the device memory by page number is as follows:
//
//   Pg  0     : WTTTS_MFG_DATA_STRUCT
//   Pg  1 -  7: reserved for future manufacturing data (read only)
//   Pg  8     : Firmware storage area for timing parameters
//   Pg  9 - 10: WTTTS_HOUSING_SN_STRUCT
//   Pg 11 - 15: reserved for future use
//   Pg 16 - 20: reserved for calibration information
//   Pg 31     : WTTTS_CAL_VER_STRUCT

#define NBR_BYTES_PER_WTTTS_PAGE   16
#define NBR_WTTTS_CFG_PAGES        32
#define FIRST_RDWR_CFG_PAGE        8

#define MFG_DATA_STRUCT_PAGE       0
#define MFG_DATA_STRUCT_PAGES      1
#define HOUSING_SN_STRUCT_PAGE     9
#define HOUSING_SN_STRUCT_PAGES    2

#define WTTTS_CFG_FIRST_HOST_PAGE  16
#define WTTTS_NBR_HOST_CFG_PAGES   16
#define CAL_DATA_STRUCT_PAGE       16
#define CAL_DATA_STRUCT_PAGES      15
#define CAL_VER_STRUCT_PAGE        31
#define CAL_VER_STRUCT_PAGES       1

#define WTTTS_PG_RESULT_SUCCESS    0
#define WTTTS_PG_RESULT_BAD_PG     1

typedef struct {
    BYTE pageNbr;
} WTTTS_CFG_ITEM;

typedef struct {
    BYTE pageNbr;
    BYTE result;    // Always zero host to WTTTS; from WTTTS: 0 = success, 1 = invalid page
    BYTE pageData[NBR_BYTES_PER_WTTTS_PAGE];
} WTTTS_CFG_DATA;

// Unit version information.
#define WTTTS_FW_VER_LEN   4

typedef struct {
    BYTE hardwareSettings;          // Physical jumper settings on sub
    BYTE fwVer[WTTTS_FW_VER_LEN];   // Firmware version info
} WTTTS_VER_PKT;

// Set RF Channel command
typedef struct {
    BYTE chanNbr;     // 11 through 26
} WTTTS_SET_CHAN_PKT;

// Set RF Power command
typedef struct {
    BYTE powerLevel;     // Binary value, legit values as per radio chip spec
} WTTTS_SET_RF_PWR_PKT;

// Request min/max readings
typedef struct {
    BYTE resetValues;    // 0 = don't reset min/max values, 1 = reset min/max values
} WTTTS_REQ_MIN_MAX_PKT;

// PIB contact closures command
typedef struct {
    BYTE contactSettings;   // Bit flags, D0 through D3 used
} WTTTS_PIB_CONTACT_PKT;

// PIB solenoid closure command
typedef struct {
    BYTE solenoidNbr;       // 1 through 6, or 0 to close all active solenoids
    BYTE state;             // 0 = deactivate, 1 = activate
} WTTTS_PIB_SOLENOID_PKT;

// PIB sensor parameters command
typedef struct {
    BYTE magnetSensRate;    // Enum value of 0 through 8, refer to spec
    BYTE sensorAvgFactor;   // Must be one of 1, 2, 4, 8, 16, 32, 64, or 128
} WTTTS_PIB_PARAMS_PKT;

// Clear PIB input indication packet
typedef struct {
    BYTE deviceAddr;        // Address of device to clear, 0 = PIB board, non-zero = sensor board
    BYTE switchFlags;       // D0 = clear SW1 indication, D1 = clear SW2 indication
} WTTTS_PIB_CLR_INPUT_PKT;


//
// MPLS V2 Packet definitions
//

// MPLS V2 RFC packet
typedef struct {
    WORD slaveBattVolts;                     // in mV
    BYTE slaveTankPress[TRI_BYTE_VAL_LEN];   // Signed 24-bit value
    BYTE slaveRegPress[TRI_BYTE_VAL_LEN];    // Signed 24-bit value
    WORD masterBattVolts;                    // in mV
    BYTE masterTankPress[TRI_BYTE_VAL_LEN];  // Signed 24-bit value
    BYTE masterRegPress[TRI_BYTE_VAL_LEN];   // Signed 24-bit value
    BYTE rpm;                                // Unsigned value, units of RPM
    BYTE temperature;                        // Signed value, 0.5C per count with, -64C to +63.5C
    WORD plugData[32];                       // Unused slots filled with 0xFFFF
    WORD sysStatusBits;                      // Bit-mapped field
    BYTE solStatus;                          // Lower nibble: master, upper nibble: slave
    BYTE isolOutStatus;                      // Lower nibble: master, upper nibble: slave
    BYTE slaveSolCurrent;                    // Unsigned, in mA
    BYTE masterSolCurrent;                   // Unsigned, in mA
    BYTE rfu[4];                             // Reserved for future use
} MPLS2_RFC_PKT;

typedef struct {
    WORD  rfcRate;
    WORD  rfcTimeout;
    WORD  pairTimeout;
} MPLS2_RATES_PKT;

typedef struct {
    BYTE  byUnitID;     // 1 = master, 2 = slave
} MPLS2_QUERY_VER_PKT;

typedef struct {
    BYTE   byUnitID;
    DWORD  tecHWRev;
    DWORD  tecFWRev;
    BYTE   tecSN[32];
    DWORD  pibHWRev;
    DWORD  pibFWRev;
    DWORD  plugDetHWRev;
    DWORD  plugDetFWRev;
    BYTE   plugDetSN[32];
} MPLS2_VERSION_PKT;

typedef struct {
    BYTE targetPIB;    // 1 = master, 2 = slave
    BYTE activeSol;    // 0 = sol off, 1 - 5 = sol on
    BYTE solTimeout;   // Value, in seconds, after which the TEC will disable currently active solenoid
    BYTE isolOuts;     // D3 - D0 map to ouputs 4 - 1
} MPLS2_PIB_OUTS_PKT;

typedef struct {
    BYTE devAddr;
    BYTE setting;
    BYTE readsPerRFC;
} MPLS2_TEC_PARAMS_PKT;

typedef struct {
    BYTE sampleRate;     // Enumerated value, see spec
    BYTE sampleAvging;   // Binary values: 1, 2, 4, 8, 16, 32, 64, or 128
} MPLS2_DET_PARAMS_PKT;

typedef struct {
    BYTE byCmdBeingAckd;
    BYTE byCmdResult;
} MPLS2_ACK_PKT;


// All packet payloads for a TesTORK device are defined in the following union
typedef union {
    WTTTS_STREAM_DATA    streamData;
    WTTTS_RFC_V1_PKT     rfcPktV1;
    WTTTS_RFC_V2_PKT     rfcPktV2;
    WTTTS_CFG_DATA       cfgData;
    WTTTS_CFG_ITEM       cfgRequest;
    WTTTS_VER_PKT        verPkt;
    WTTTS_RATE_PKT       ratePkt;
    WTTTS_SET_CHAN_PKT   chanPkt;
    WTTTS_SET_RF_PWR_PKT pwrPkt;
} TESTORK_DATA_UNION;

// All packet payloads for a MPL device are defined in the following union
typedef union {
    WTTTS_MPL_RFC_PKT       rfcPkt;
    WTTTS_CFG_DATA          cfgData;
    WTTTS_CFG_ITEM          cfgRequest;
    WTTTS_VER_PKT           verPkt;
    WTTTS_RATE_PKT          ratePkt;
    WTTTS_SET_CHAN_PKT      chanPkt;
    WTTTS_SET_RF_PWR_PKT    pwrPkt;
    WTTTS_REQ_MIN_MAX_PKT   reqMinMaxPkt;
    WTTTS_MPL_MIN_MAX_PKT   minMaxPkt;
    WTTTS_PIB_CONTACT_PKT   contactsPkt;
    WTTTS_PIB_SOLENOID_PKT  solenoidPkt;
    WTTTS_PIB_PARAMS_PKT    pibParamsPkt;
    WTTTS_PIB_CLR_INPUT_PKT clearPIBInput;
} MPL_DATA_UNION;

// All packet payloads for a V2 MPLS device are defined in the following union
typedef union {
    MPLS2_RFC_PKT           rfcPkt;
    WTTTS_CFG_DATA          cfgData;
    WTTTS_CFG_ITEM          cfgRequest;
    MPLS2_QUERY_VER_PKT     queryVerPkt;
    MPLS2_VERSION_PKT       verPkt;
    MPLS2_RATES_PKT         ratePkt;
    WTTTS_SET_CHAN_PKT      chanPkt;
    WTTTS_SET_RF_PWR_PKT    pwrPkt;
    MPLS2_PIB_OUTS_PKT      pibOutsPkt;
    MPLS2_TEC_PARAMS_PKT    tecParamsPkt;
    MPLS2_DET_PARAMS_PKT    detParamsPkt;
    MPLS2_ACK_PKT           ackPkt;
} MPLS2_DATA_UNION;

#pragma pack(pop)


#define MIN_TESTORK_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + SIZEOF_WTTTS_CHECKSUM )
#define MAX_TESTORK_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + sizeof( TESTORK_DATA_UNION ) + SIZEOF_WTTTS_CHECKSUM )

#define MIN_MPL_PKT_LEN          ( sizeof( WTTTS_PKT_HDR ) + SIZEOF_WTTTS_CHECKSUM )
#define MAX_MPL_PKT_LEN          ( sizeof( WTTTS_PKT_HDR ) + sizeof( MPL_DATA_UNION ) + SIZEOF_WTTTS_CHECKSUM )

#define MIN_MPLS2_PKT_LEN        ( sizeof( WTTTS_PKT_HDR ) + SIZEOF_WTTTS_CHECKSUM )
#define MAX_MPLS2_PKT_LEN        ( sizeof( WTTTS_PKT_HDR ) + sizeof( MPLS2_DATA_UNION ) + SIZEOF_WTTTS_CHECKSUM )


//
// Base station radio specific defines
//

// Base station payload definitions
#pragma pack(push, 1)

// Base station commands
typedef struct {
    BYTE radioNbr;
    BYTE newChanNbr;
} BASE_STN_SET_RADIO_CHAN;

typedef struct {
    BYTE radioNbr;
    BYTE powerLevel;     // Binary value, legit values as per radio chip spec
} BASE_STN_SET_RADIO_PWR;

typedef struct {
    WORD newUpdateRate;
} BASE_STN_SET_RATE_PKT;

typedef struct {
    BYTE outputFlags;
} BASE_STN_SET_OUTPUTS_PKT;

typedef struct {
    BYTE radioNbr;
    BYTE fccModeState;       // 0 = disabled, 1 = enabled
} BASE_STN_SET_FCC_TEST_MODE;

typedef struct {
    BYTE radioNbr;
    BYTE cwEnabled;          // 0 = disabled, 1 = enabled
} BASE_STN_SET_FCC_CW_MODE;

// Base station responses
typedef struct {
    BYTE  chanNbr;
    BYTE  rssi;
    BYTE  rfu[2];
    DWORD bitRate;
} RADIO_CHAN_INFO;


// eBaseRadioNbr defines the number of transceivers in a Base Station
typedef enum {
    eBRN_1,     // Typically used for the TesTORK and LinkTilt systems
    eBRN_2,     // Typically used by the MPLS system
    eBRN_3,     // No antenna present on base radio, so should never be used
    eBRN_NbrBaseRadios
} eBaseRadioNbr;

// Status packet format
typedef struct {
    RADIO_CHAN_INFO radioInfo[eBRN_NbrBaseRadios];
    BYTE  lastResetType;
    BYTE  outputStatus;
    WORD  statusUpdateRate;
    BYTE  fwVer[4];
    BYTE  usbCtrlSignals;
    BYTE  byRFU[3];
} BASE_STATUS_STATUS_PKT;

// All packet payloads are defined in the following union
typedef union {
    BASE_STN_SET_RADIO_CHAN    setChanPkt;
    BASE_STN_SET_RADIO_PWR     setRadioPwr;
    BASE_STN_SET_RATE_PKT      setRatePkt;
    BASE_STN_SET_OUTPUTS_PKT   setOutputsPkt;
    BASE_STN_SET_FCC_TEST_MODE setFCCModePkt;
    BASE_STN_SET_FCC_CW_MODE   setFCCCWPkt;
    BASE_STATUS_STATUS_PKT     statusPkt;
} BASE_STN_DATA_UNION;

#pragma pack(pop)


#define MIN_BASE_STN_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + SIZEOF_WTTTS_CHECKSUM )
#define MAX_BASE_STN_PKT_LEN      ( sizeof( WTTTS_PKT_HDR ) + sizeof( BASE_STN_DATA_UNION ) + SIZEOF_WTTTS_CHECKSUM )

// Packet header defines
#define BASE_STN_HDR_RX   0x99       // inbound from base station
#define BASE_STN_HDR_TX   0x04       // outbound to base station

// Packet type defintions: outbound to base station
#define BASE_STN_CMD_GET_STATUS      0x10
#define BASE_STN_CMD_SET_CHAN_NBR    0x12
#define BASE_STN_CMD_SET_RF_PWR      0x13
#define BASE_STN_CMD_SET_CHAN_PARAM  0x14
#define BASE_STN_CMD_SET_FCC_MODE    0x15
#define BASE_STN_CMD_SET_UPDATE_RATE 0x16
#define BASE_STN_CMD_SET_FCC_CW_MODE 0x17
#define BASE_STN_CMD_SET_PWR_STATE   0x1A
#define BASE_STN_CMD_SET_OUTPUTS     0x1B
#define BASE_STN_CMD_RESET_FIRMWARE  0x1C

// Packet type definitions: inbound from base station
#define BASE_STN_RESP_STATUS         0xA0


//
// Network Defines
//

typedef enum {
    eWS_TesTork,
    eWS_MPL1,
    eWS_LinkTilt,
    eWS_MPLS2,
    eWS_NbrWirelessSystems
} eWirelessSystem;

#define MAX_CHANS_PER_SYSTEM   4

typedef struct {
    eBaseRadioNbr radioNumber;
    int           chanList[MAX_CHANS_PER_SYSTEM];
} WIRELESS_SYSTEM_INFO;

bool GetWirelessSystemInfo( eWirelessSystem eWhichSystem, WIRELESS_SYSTEM_INFO& wirelessInfo );


//
//  Helper Functions
//

bool HaveTesTORKCmdPkt ( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, TESTORK_DATA_UNION& pktData );
bool HaveTesTORKRespPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, TESTORK_DATA_UNION& pktData );
    // Scans the passed buffer for a TesTORK command or response. Returns true if
    // a packet is found, in which case pktHdr and pktData vars are populated.
    // Returns false if no packet is found. In either case, data in pBuff is
    // shifted out (if necessary) and the remaining count of bytes in the buffer
    // is returned in param buffLen.

bool ConvertRFCPktToGeneric( GENERIC_RFC_PKT& genericPkt, const WTTTS_PKT_HDR& pktHdr, const TESTORK_DATA_UNION& pktData );
    // Converts a RFC V1 or V2 packet to generic format. Returns true on success
    // (will only fail if a non-RFC packet is passed to it)

bool HaveMPLCmdPkt ( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, MPL_DATA_UNION& pktData );
bool HaveMPLRespPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, MPL_DATA_UNION& pktData );
    // Scans the passed buffer for a MPL command or response. Returns true if
    // a packet is found, in which case pktHdr and pktData vars are populated.
    // Returns false if no packet is found. In either case, data in pBuff is
    // shifted out (if necessary) and the remaining count of bytes in the buffer
    // is returned in param buffLen.

bool HaveMPLS2CmdPkt ( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, MPLS2_DATA_UNION& pktData );
bool HaveMPLS2RespPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, MPLS2_DATA_UNION& pktData );
    // Scans the passed buffer for a MPL command or response. Returns true if
    // a packet is found, in which case pktHdr and pktData vars are populated.
    // Returns false if no packet is found. In either case, data in pBuff is
    // shifted out (if necessary) and the remaining count of bytes in the buffer
    // is returned in param buffLen.

bool HaveBaseStnCmdPkt ( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, BASE_STN_DATA_UNION& pktData );
bool HaveBaseStnRespPkt( BYTE pBuff[], DWORD& buffLen, WTTTS_PKT_HDR& pktHdr, BASE_STN_DATA_UNION& pktData );
    // Scans the passed buffer for a base station command or response. Returns true
    // if a packet is found, in which case pktHdr and pktData vars are populated.
    // Returns false if no packet is found. In either case, data in pBuff is
    // shifted out (if necessary) and the remaining count of bytes in the buffer
    // is returned in param buffLen.

BYTE CalcWTTTSChecksum( const BYTE pBuff[], int buffLen );
    // Calculates and returns the checksum for the passed buffer.
    // Returns zero if pBuff is NULL or buffLen == 0. The same checksum
    // is used for both WTTTS and base station radio comms.

void IncStat( DWORD& aStat, DWORD incrAmount = 1 );
    // Increments the passed stats var, not allowing it to wrap around 0xFFFFFFFF

void ShiftBufferDown( BYTE pBuffer[], DWORD& buffLen, DWORD nbrBytes );
    // Shifts down the contents of the passed buffer by nbrBytes.
    // Param buffLen is the number of bytes currently in teh buffer.

DWORD TimeSince( DWORD startTime );
bool  HaveTimeout( DWORD startTime, DWORD timeoutValue );
    // Timeout helper functions

INT16 BiByteToShortInt( const BYTE byteData[] );
int   TriByteToInt( const BYTE byteData[] );
int   QuadByteToInt( const BYTE byteData[] );
    // Routines used to convert WTTTS data arrays to more
    // usable data types

String DeviceIDToStr( const BYTE byDevChars[] );
    // Converts a device ID to a standard string

DWORD CalcElapsedTicks( WORD wLast, WORD wCurr );

//
// Battery Support
//

typedef enum {
    BT_UNKNOWN,
    BT_LITHIUM,
    BT_NiMH_4000,
    BT_NiMH_6050,
    NBR_BATTERY_TYPES
} BATTERY_TYPE;

typedef struct {
    BATTERY_TYPE battType;
    String       battTypeText;
    int          initialCapacity;
    float        currVolts;
    int          currUsage;
    float        avgVolts;
    int          avgUsage;
} BATTERY_INFO;

String       GetBattTypeText( BATTERY_TYPE battType );
BATTERY_TYPE GetBattTypeEnum( const String& battTypeText );
    // Battery helper routines and structs


//
// Device Hardware Info Struct
//

typedef struct {
    String housingSN;
    String mfgSN;
    String fwRev;
    String cfgStraps;
    String mfgInfo;
    String mfgDate;
} DEVICE_HW_INFO;


//
// Device Enums
//

typedef enum {
    WDRT_POWER_UP  = 1,
    WDRT_WATCH_DOG = 2,
    WDRT_BROWN_OUT = 3,
} WIRELESS_DEV_RESET_TYPE;

typedef enum {
    WDMT_DEEP_SLEEP  = 0,
    WDMT_NORMAL      = 1,
    WDMT_LOW_POWER   = 2,
} WIRELESS_DEV_MODE_TYPE;


//
// Common EEPROM Memory Structs
//

typedef struct {
    WORD devSN;            // Device serial number
    char szMfgInfo[8];     // Padded with nulls; null may be missing if all 8 chars used.
    BYTE monthOfMfg;       // 0 = Jan
    BYTE yearOfMfg;        // Years since 2000
    BYTE byRFU[4];         // Reserved for future use
} WTTTS_MFG_DATA_STRUCT;

typedef struct {
    BYTE byHousingSN[2*NBR_BYTES_PER_WTTTS_PAGE];  // Device SN, in text, null padded
} WTTTS_HOUSING_SN_STRUCT;

typedef struct {
    BYTE  byRFU[11];
    BYTE  calYear;         // The calibration date fields were introduced later in the development
    BYTE  calMonth;        //   of the system. Finding 0xFF in these three fields indicates no cal
    BYTE  calDay;          //   date was stored. Year is relative to 2000, month = 1-12, day = 1-31
    BYTE  byCalRevNbr;     // Minor version number of format of cal data
    BYTE  byCalVerNbr;     // Major version number of format of cal data; 0xFF means EEPROM is blank
} WTTTS_CAL_VER_STRUCT;


//
// Helper constants
//

extern const WORD   DEFAULT_RFC_RATE;          // msecs
extern const WORD   DEFAULT_RFC_TIMEOUT;       // msecs
extern const WORD   DEFAULT_STREAM_RATE;       // msecs
extern const WORD   DEFAULT_STREAM_TIMEOUT;    // secs
extern const WORD   DEFAULT_PAIRING_TIMEOUT;   // secs
extern const WORD   DEFAULT_SOLENOID_TIMEOUT;  // secs
extern const time_t DEFAULT_PACKET_TIMEOUT;    // secs

#endif
