#include <vcl.h>
#pragma hdrstop

#include <msxmldom.hpp>
#include <XMLDoc.hpp>
#include <xmldom.hpp>
#include <DateUtils.hpp>

#include "XMLHelpers.h"

#pragma package(smart_init)


//
// GetValue...() / SaveValue...() functions are overloaded implementations
// for getting and saving basic data types
//

int GetValueFromNode( _di_IXMLNode aNode, const String& keyName, int defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    String sText = node->Text;

    return sText.ToIntDef( defaultValue );
}


void SaveValueToNode( _di_IXMLNode aNode, const String& keyName, int value )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( node == NULL )
        node = aNode->AddChild( keyName );

    node->Text = IntToStr( value );
}


float GetValueFromNode( _di_IXMLNode aNode, const String& keyName, float defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    String sText = node->Text;

    return StrToFloatDef( sText, defaultValue );
}


void SaveValueToNode( _di_IXMLNode aNode, const String& keyName, float value )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( node == NULL )
        node = aNode->AddChild( keyName );

    node->Text = FloatToStr( value );
}


TDateTime GetValueFromNode( _di_IXMLNode aNode, const String& keyName, TDateTime defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    const String& sText = node->Text;

    TDateTime dtReturn;

    try
    {
        if( sText.Pos( "-" ) > 0 )
        {
            dtReturn = TDateTime( sText.SubString(  1, 4 ).ToIntDef( 0 ),
                                  sText.SubString(  6, 2 ).ToIntDef( 0 ),
                                  sText.SubString(  9, 2 ).ToIntDef( 0 ),
                                  sText.SubString( 12, 2 ).ToIntDef( 0 ),
                                  sText.SubString( 15, 2 ).ToIntDef( 0 ),
                                  sText.SubString( 18, 2 ).ToIntDef( 0 ),
                                  0 );
        }
        else
        {
            dtReturn = UnixToDateTime( sText.ToIntDef( 0 ), false );
        }
    }
    catch( ... )
    {
        return 0;
    }

    return dtReturn;
}


void SaveValueToNode( _di_IXMLNode aNode, const String& keyName, TDateTime value )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( node == NULL )
        node = aNode->AddChild( keyName );

    node->Text = value.FormatString( "yyyy-mm-dd hh:nn:ss" );
}


String GetValueFromNode( _di_IXMLNode aNode, const String& keyName, const String& defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    return node->Text;
}


void SaveValueToNode( _di_IXMLNode aNode, const String& keyName, const String& value )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    // If the node doesn't exist, and the value is an empty string, then
    // we don't have to do anything
    if( value.IsEmpty() )
    {
        if( node != NULL )
            aNode->ChildNodes->Delete( keyName );
    }
    else
    {
        if( node == NULL )
            node = aNode->AddChild( keyName );

        node->Text = value;
    }
}


bool GetValueFromNode( _di_IXMLNode aNode, const String& keyName, bool defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    String sText = node->Text;

    return sText.ToIntDef( defaultValue );
}


void SaveValueToNode( _di_IXMLNode aNode, const String& keyName, bool value )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( node == NULL )
        node = aNode->AddChild( keyName );

    node->Text = IntToStr( (int)value );
}


time_t GetValueFromNode( _di_IXMLNode aNode, const String& keyName, time_t defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    String sText = node->Text;

    return sText.ToIntDef( (int)defaultValue );
}


void SaveValueToNode( _di_IXMLNode aNode, const String& keyName, time_t tValue )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( node == NULL )
        node = aNode->AddChild( keyName );

    node->Text = IntToStr( (int)tValue );
}


void GetValueFromNode( _di_IXMLNode aNode, const String& keyName, TStringList* pStrings )
{
    if( pStrings == NULL )
        return;

    pStrings->Clear();

    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    // Find the node of entries
    _di_IXMLNode stringsNode = aNode->ChildNodes->FindNode( keyName );

    if( stringsNode == NULL )
        return;

    try
    {
        // Get our count first
        _di_IXMLNode countNode = stringsNode->ChildNodes->FindNode( "Count" );

        if( countNode == NULL )
            Abort();

        int iCount = countNode->Text.ToIntDef( 0 );

        // Look for entries in sequence now
        for( int iEntryNbr = 1; iEntryNbr <= iCount; iEntryNbr++ )
        {
            String sEntryKey = "Line" + IntToStr( iEntryNbr );

            _di_IXMLNode stringNode = stringsNode->ChildNodes->FindNode( sEntryKey );

            if( ( stringNode == NULL ) || ( stringNode->IsTextElement == false ) )
                pStrings->Add( "" );
            else
                pStrings->Add( stringNode->Text );
        }
    }
    catch( ... )
    {
    }
}


void SaveValueToNode( _di_IXMLNode aNode, const String& keyName, TStringList* pStrings )
{
    if( pStrings == NULL )
        return;

    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    try
    {
        // Delete any existing node
        aNode->ChildNodes->Delete( keyName );

        _di_IXMLNode stringsNode = aNode->ChildNodes->FindNode( keyName );

        if( stringsNode == NULL )
            stringsNode = aNode->AddChild( keyName );

        _di_IXMLNode countNode = stringsNode->AddChild( "Count" );

        if( countNode == NULL )
            Abort();

        countNode->Text = IntToStr( pStrings->Count );

        for( int iString = 0; iString < pStrings->Count; iString++ )
        {
            String sEntryKey = "Line" + IntToStr( iString + 1 );

            _di_IXMLNode stringNode = stringsNode->ChildNodes->FindNode( sEntryKey );

            if( stringNode == NULL )
                stringNode = stringsNode->AddChild( sEntryKey );

            // The XML node doesn't like to save empty strings. So if
            // our string is empty replace it with a blank
            if( pStrings->Strings[iString].IsEmpty() )
                stringNode->Text = " ";
            else
                stringNode->Text = pStrings->Strings[iString];
        }
    }
    catch( ... )
    {
    }
}


//
// Node Helper Functions
//

_di_IXMLNode FindOrCreateNode( _di_IXMLNode rootNode, const String& childName )
{
    _di_IXMLNode childNode = rootNode->ChildNodes->FindNode( childName );

    if( childNode == NULL )
        childNode = rootNode->AddChild( childName );

    return childNode;
}


_di_IXMLNode FindChildNode( _di_IXMLNode rootNode, String& childName )
{
    if( rootNode == NULL )
        return NULL;

    return rootNode->ChildNodes->FindNode( childName );
}

