#ifndef XMLHelpersH
#define XMLHelpersH

//
// Generic Helper Methods for Working with XML Files
//

#include <time.h>


//
// GetValue...() / SaveValue...() functions are overloaded implementations
// for getting and saving basic data types
//

int  GetValueFromNode( _di_IXMLNode aNode, const String& keyName, int defaultValue );
void SaveValueToNode ( _di_IXMLNode aNode, const String& keyName, int value );

float GetValueFromNode( _di_IXMLNode aNode, const String& keyName, float defaultValue );
void  SaveValueToNode ( _di_IXMLNode aNode, const String& keyName, float value );

TDateTime GetValueFromNode( _di_IXMLNode aNode, const String& keyName, TDateTime defaultValue );
void      SaveValueToNode ( _di_IXMLNode aNode, const String& keyName, TDateTime value );

String GetValueFromNode( _di_IXMLNode aNode, const String& keyName, const String& defaultValue );
void   SaveValueToNode ( _di_IXMLNode aNode, const String& keyName, const String& value );

bool GetValueFromNode( _di_IXMLNode aNode, const String& keyName, bool defaultValue );
void SaveValueToNode ( _di_IXMLNode aNode, const String& keyName, bool value );

time_t GetValueFromNode( _di_IXMLNode aNode, const String& keyName, time_t defaultValue );
void   SaveValueToNode ( _di_IXMLNode aNode, const String& keyName, time_t tValue );

void   GetValueFromNode( _di_IXMLNode aNode, const String& keyName, TStringList* pStrings );
void   SaveValueToNode ( _di_IXMLNode aNode, const String& keyName, TStringList* pStrings );


//
// Node Helper Functions
//

_di_IXMLNode FindOrCreateNode( _di_IXMLNode rootNode, const String& childName );
_di_IXMLNode FindChildNode( _di_IXMLNode rootNode, String& childName );


#endif
