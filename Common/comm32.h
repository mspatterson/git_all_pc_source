//************************************************************************
//
//  Comm32.H: Provides serial handling for Microlynx 32 bit applications.
//            It is a common file for many applications. This source must
//            not contain any application specific code.
//
//  Copyright (c) 2003, Microlynx Systems Ltd
//  ALL RIGHTS RESERVED
//
//  THIS SOURCE CODE MAY BE MODIFIED AND DISTRIBUTED ONLY IN ACCORDANCE
//  WITH THE TERMS OF THE MICROLYNX RTOS SOURCE CODE LICENSE.
//
//************************************************************************


#ifndef _Comm32CPP_H

    #define _Comm32CPP_H

    #include "CommObj.h"

    class CCommPort : public CCommObj
    {
      private:

        HANDLE        hCommHandle;
        COMSTAT       commStat;
        DWORD         dwErrorFlags;
        DCB           dcbInfo;
        DWORD         cbInQueue;
        DWORD         cbOutQueue;

      protected:

      public:
          __fastcall CCommPort( void );
          virtual __fastcall ~CCommPort();

        typedef struct
        {
            int   iPortNumber;
            DWORD dwLineBitRate;
        } CONNECT_PARAMS;

        COMM_RESULT Connect( void* pConnectParams );

        void Disconnect( void );

        DWORD CommRecv(       BYTE* byBuff, DWORD dwMaxBytes );
        DWORD CommSend( const BYTE* byBuff, DWORD dwNbrBytes );

        void SetRTS( bool bRTSOn );
        void SetDTR( bool bDTROn );

        DWORD GetCntlSignals( void );
            // Return value will consist of one or more of the following flags:
            #define COMM32_DTR_ON 0x0001
            #define COMM32_RTS_ON 0x0002
            #define COMM32_DSR_ON 0x0004
            #define COMM32_DCD_ON 0x0008
            #define COMM32_CTS_ON 0x0010
            #define COMM32_RI_ON  0x0020

    };

#endif
