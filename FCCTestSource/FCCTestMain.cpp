#include <vcl.h>
#pragma hdrstop

#include "FCCTestMain.h"
#include "ApplUtils.h"
#include "Comm32.h"
#include "Typedefs.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TMainForm *MainForm;


typedef struct {
    char* szDesc;
    BYTE  byRegValue;
} CC2530_TX_PWR_SETTING;

#define NBR_CC2530_PWR_SETTINGS 16
static const CC2530_TX_PWR_SETTING CC2530PowerSettings[NBR_CC2530_PWR_SETTINGS] = {
    { "4.5 dBm",     0xF5 },
    { "2.5 dBm",     0xE5 },
    { "1.0 dBm",     0xD5 },
    { "-0.5 dBm",    0xC5 },
    { "-1.5 dBm",    0xB5 },
    { "-3.0 dBm",    0xA5 },
    { "-4.0 dBm",    0x95 },
    { "-6.0 dBm",    0x85 },
    { "-8.0 dBm",    0x75 },
    { "-10.0 dBm",   0x65 },
    { "-12.0 dBm",   0x55 },
    { "-14.0 dBm",   0x45 },
    { "-16.0 dBm",   0x35 },
    { "-18.0 dBm",   0x25 },
    { "-20.0 dBm",   0x15 },
    { "-22.0 dBm",   0x05 },
};


// Common Status Grid Key Values
static const UnicodeString sPortState  ( "Port State" );
static const UnicodeString sLastCmdTime( "Last Cmd Sent" );
static const UnicodeString sLastRxTime ( "Last Pkt Rcvd At" );
static const UnicodeString sLastRxType ( "Last Pkt Type" );

// WTTTS Grid Specific Values
static const UnicodeString sRFCTemp      ( "Temperature" );
static const UnicodeString sRFCBattLife  ( "Battery Life" );
static const UnicodeString sRFCBattType  ( "Battery Type" );
static const UnicodeString sRFCPressure  ( "Presssure" );
static const UnicodeString sRFCRPM       ( "RPM" );
static const UnicodeString sRFCLastReset ( "Last Reset" );
static const UnicodeString sRFCRFChan    ( "RF Channel" );
static const UnicodeString sRFCCurrMode  ( "Current Mode" );
static const UnicodeString sRFCRate      ( "RFC Rate" );
static const UnicodeString sRFCTimeout   ( "RFC Timeout" );
static const UnicodeString sRFCStreamRate( "Stream Rate" );
static const UnicodeString sRFCStreamTO  ( "Stream Timeout" );
static const UnicodeString sRFCPairingTO ( "Pairing Timeout" );

// Base Radio Specific Values
static const UnicodeString sBR0ChanNbr    ( "Radio 0 Chan" );
static const UnicodeString sBR0RSSI       ( "Radio 0 RSSI" );
static const UnicodeString sBR0BitRate    ( "Radio 0 Bit Rate" );
static const UnicodeString sBR1ChanNbr    ( "Radio 1 Chan" );
static const UnicodeString sBR1RSSI       ( "Radio 1 RSSI " );
static const UnicodeString sBR1BitRate    ( "Radio 1 Bit Rate" );
static const UnicodeString sBROutputStatus( "Output Status" );
static const UnicodeString sBRUpdateRate  ( "Update Rate" );
static const UnicodeString sBRFWVer       ( "Firmware Ver" );
static const UnicodeString sBRCtrlSigs    ( "USB Ctrl Sigs" );
static const UnicodeString sBRRFU0        ( "RFU 0" );
static const UnicodeString sBRRFU1        ( "RFU 1" );
static const UnicodeString sBRLastReset   ( "Last Reset" );




static void EnableGBCtrls( TGroupBox* aGB, bool isEnabled )
{
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
        aGB->Controls[iCtrl]->Enabled = isEnabled;
}


static void ClearGBTags( TGroupBox* aGB )
{
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
        aGB->Controls[iCtrl]->Tag = 0;
}


static void SetEditState( TEdit* anEdit, bool isEnabled )
{
    if( isEnabled )
    {
        anEdit->Color   = clWindow;
        anEdit->Enabled = true;
    }
    else
    {
        anEdit->Color   = clBtnFace;
        anEdit->Enabled = false;
    }
}


__fastcall TMainForm::TMainForm(TComponent* Owner) : TForm(Owner)
{
    MainPgCtrl->ActivePageIndex = 0;

    memset( &m_BRPort,    0, sizeof( PORT_INFO ) );
    memset( &m_WTTTSPort, 0, sizeof( PORT_INFO ) );

    WTTTSTxPwrCombo->Items->Clear();
    BRTxPwrCombo->Items->Clear();

    for( int iItem = 0; iItem < NBR_CC2530_PWR_SETTINGS; iItem++ )
    {
        WTTTSTxPwrCombo->Items->Add( CC2530PowerSettings[iItem].szDesc );
        BRTxPwrCombo->Items->Add( CC2530PowerSettings[iItem].szDesc );
    }

    WTTTSTxPwrCombo->ItemIndex = 0;
    BRTxPwrCombo->ItemIndex = 0;
}


void __fastcall TMainForm::FormShow(TObject *Sender)
{
   // Set initial button states
    BRConnectBtn->Enabled    = true;
    BRDisconnectBtn->Enabled = false;

    SetEditState( BRPortEdit,  true );
    SetEditState( BRSpeedEdit, true );

    WTTTSConnectBtn->Enabled    = true;
    WTTTSDisconnectBtn->Enabled = false;

    SetEditState( WTTTSPortEdit,  true );
    SetEditState( WTTTSSpeedEdit, true );

    EnableGBCtrls( BRActionsGB,    false );
    EnableGBCtrls( WTTTSActionsGB, false );
}


void __fastcall TMainForm::FormDestroy(TObject *Sender)
{
    // Release resources
    BRDisconnectBtnClick( BRDisconnectBtn );
    WTTTSDisconnectBtnClick( WTTTSDisconnectBtn );
}


void __fastcall TMainForm::ExitBtnClick(TObject *Sender)
{
    Application->Terminate();
}


//
// Base Radio
//

void __fastcall TMainForm::BRConnectBtnClick(TObject *Sender)
{
    if( !ValidatePortSettings( BRPortEdit, BRSpeedEdit ) )
        return;

    // Try to create the comm obj
    if( !CreatePort( BRPortEdit->Text.ToInt(), BRSpeedEdit->Text.ToInt(), m_BRPort ) )
        return;

    UpdateStatusGrid( BRStatusGrid, sPortState, "Open on COM" + BRPortEdit->Text );

    // On success, set button states
    BRConnectBtn->Enabled    = false;
    BRDisconnectBtn->Enabled = true;

    SetEditState( BRPortEdit,  false );
    SetEditState( BRSpeedEdit, false );

    EnableGBCtrls( BRActionsGB, true );
    ClearGBTags( BRActionsGB );

    // Enable poll timer
    BRPollTimer->Enabled = true;
}


void __fastcall TMainForm::BRDisconnectBtnClick(TObject *Sender)
{
    // Stop poll timer and release comm obj
    BRPollTimer->Enabled = false;

    ReleasePort( m_BRPort );

    ClearStatusGrid( BRStatusGrid );
    UpdateStatusGrid( BRStatusGrid, sPortState, "Closed" );

    // Set button states
    BRConnectBtn->Enabled    = true;
    BRDisconnectBtn->Enabled = false;

    SetEditState( BRPortEdit,  true );
    SetEditState( BRSpeedEdit, true );

    EnableGBCtrls( BRActionsGB, false );
}


void __fastcall TMainForm::SetBRChanBtnClick(TObject *Sender)
{
    BASE_STN_DATA_UNION payloadData;
    payloadData.setChanPkt.radioNbr   = GetSelectedRadioNbr();
    payloadData.setChanPkt.newChanNbr = GetChanNbr( BRChanCombo );

    if( SendBRCmd( BASE_STN_CMD_SET_CHAN_NBR, payloadData, sizeof( BASE_STN_SET_RADIO_CHAN ) ) )
        UpdateStatusGrid( BRStatusGrid, sLastCmdTime, Time().TimeString() );
    else
        Beep();
}


void __fastcall TMainForm::SetBRPwrBtnClick(TObject *Sender)
{
    BASE_STN_DATA_UNION payloadData;
    payloadData.setRadioPwr.radioNbr   = GetSelectedRadioNbr();
    payloadData.setRadioPwr.powerLevel = GetPowerLevel( BRTxPwrCombo );

    if( SendBRCmd( BASE_STN_CMD_SET_RF_PWR, payloadData, sizeof( BASE_STN_SET_RADIO_PWR ) ) )
        UpdateStatusGrid( BRStatusGrid, sLastCmdTime, Time().TimeString() );
    else
        Beep();
}


void __fastcall TMainForm::EnableBRFCCModeBtnClick(TObject *Sender)
{
    BASE_STN_DATA_UNION payloadData;
    payloadData.setFCCModePkt.radioNbr     = GetSelectedRadioNbr();
    payloadData.setFCCModePkt.fccModeState = 1;

    if( SendBRCmd( BASE_STN_CMD_SET_FCC_MODE, payloadData, sizeof( BASE_STN_SET_FCC_TEST_MODE ) ) )
        UpdateStatusGrid( BRStatusGrid, sLastCmdTime, Time().TimeString() );
    else
        Beep();
}


void __fastcall TMainForm::DisableBRFCCModeBtnClick(TObject *Sender)
{
    BASE_STN_DATA_UNION payloadData;
    payloadData.setFCCModePkt.radioNbr     = GetSelectedRadioNbr();
    payloadData.setFCCModePkt.fccModeState = 0;

    if( SendBRCmd( BASE_STN_CMD_SET_FCC_MODE, payloadData, sizeof( BASE_STN_SET_FCC_TEST_MODE ) ) )
        UpdateStatusGrid( BRStatusGrid, sLastCmdTime, Time().TimeString() );
    else
        Beep();
}


void __fastcall TMainForm::EnableBRCWModeBtnClick(TObject *Sender)
{
    BASE_STN_DATA_UNION payloadData;
    payloadData.setFCCCWPkt.radioNbr  = GetSelectedRadioNbr();
    payloadData.setFCCCWPkt.cwEnabled = 1;

    if( SendBRCmd( BASE_STN_CMD_SET_FCC_CW_MODE, payloadData, sizeof( BASE_STN_SET_FCC_CW_MODE ) ) )
        UpdateStatusGrid( BRStatusGrid, sLastCmdTime, Time().TimeString() );
    else
        Beep();
}


void __fastcall TMainForm::DisableBRCWModeBtnClick(TObject *Sender)
{
    BASE_STN_DATA_UNION payloadData;
    payloadData.setFCCCWPkt.radioNbr  = GetSelectedRadioNbr();
    payloadData.setFCCCWPkt.cwEnabled = 0;

    if( SendBRCmd( BASE_STN_CMD_SET_FCC_CW_MODE, payloadData, sizeof( BASE_STN_SET_FCC_CW_MODE ) ) )
        UpdateStatusGrid( BRStatusGrid, sLastCmdTime, Time().TimeString() );
    else
        Beep();
}


void __fastcall TMainForm::BRPollTimerTimer(TObject *Sender)
{
    PollPortForData( m_BRPort );

    // Have the parser check for any responses from the unit.
    WTTTS_PKT_HDR       pktHdr;
    BASE_STN_DATA_UNION pktData;

    if( HaveBaseStnRespPkt( m_BRPort.rxBuffer, m_BRPort.rxBuffCount, pktHdr, pktData ) )
    {
        m_BRPort.lastPktTime = GetTickCount();

        UpdateStatusGrid( BRStatusGrid, sLastRxTime, Time().TimeString() );
        UpdateStatusGrid( BRStatusGrid, sLastRxType, IntToStr( pktHdr.pktType ) );

        // We are only interested in RFC commands from the WTTTS device.
        if( pktHdr.pktType == BASE_STN_RESP_STATUS )
            UpdateBRStatusValues( pktData.statusPkt );
    }

}


bool TMainForm::SendBRCmd( BYTE cmdType, BASE_STN_DATA_UNION& payloadData, BYTE payloadLen )
{
    if( m_BRPort.commPort == NULL )
        return false;

    // Allocate and clear command area
    BYTE txBuffer[MAX_BASE_STN_PKT_LEN];
    memset( txBuffer, 0, MAX_BASE_STN_PKT_LEN );

    WTTTS_PKT_HDR* pHdr  = (WTTTS_PKT_HDR*)txBuffer;

    // Setup header defaults
    pHdr->pktHdr    = BASE_STN_HDR_TX;
    pHdr->pktType   = cmdType;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = payloadLen;

    if( payloadLen > 0 )
    {
        BYTE* pPayload = &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pPayload, &payloadData, payloadLen );
    }

    int hdrAndPayLoadLen = sizeof(WTTTS_PKT_HDR) + payloadLen;

    txBuffer[hdrAndPayLoadLen] = CalcWTTTSChecksum( txBuffer, hdrAndPayLoadLen );

    DWORD pktLen = hdrAndPayLoadLen + 1;

    DWORD bytesSent = m_BRPort.commPort->CommSend( txBuffer, pktLen );

    return( bytesSent == pktLen );
}


void TMainForm::UpdateBRStatusValues( const BASE_STATUS_STATUS_PKT& statusPkt )
{
    UpdateStatusGrid( BRStatusGrid, sBR0ChanNbr, IntToStr(      statusPkt.radioInfo[0].chanNbr ) );
    UpdateStatusGrid( BRStatusGrid, sBR0RSSI,    IntToStr(      statusPkt.radioInfo[0].rssi ) );
    UpdateStatusGrid( BRStatusGrid, sBR0BitRate, IntToStr( (int)statusPkt.radioInfo[0].bitRate ) );

    UpdateStatusGrid( BRStatusGrid, sBR1ChanNbr, IntToStr(      statusPkt.radioInfo[1].chanNbr ) );
    UpdateStatusGrid( BRStatusGrid, sBR1RSSI,    IntToStr(      statusPkt.radioInfo[1].rssi ) );
    UpdateStatusGrid( BRStatusGrid, sBR1BitRate, IntToStr( (int)statusPkt.radioInfo[1].bitRate ) );

    UpdateStatusGrid( BRStatusGrid, sBROutputStatus, IntToHex( statusPkt.outputStatus, 2 ) );
    UpdateStatusGrid( BRStatusGrid, sBRUpdateRate,   IntToStr( statusPkt.statusUpdateRate ) );
    UpdateStatusGrid( BRStatusGrid, sBRFWVer,        IntToStr( statusPkt.fwVer[0] ) + IntToStr( statusPkt.fwVer[1] ) + IntToStr( statusPkt.fwVer[2] ) + IntToStr( statusPkt.fwVer[3] ) );
    UpdateStatusGrid( BRStatusGrid, sBRCtrlSigs,     IntToHex( statusPkt.usbCtrlSignals, 2 ) );
    UpdateStatusGrid( BRStatusGrid, sBRRFU0,         IntToHex( statusPkt.byRFU[0], 2 ) );
    UpdateStatusGrid( BRStatusGrid, sBRRFU1,         IntToHex( statusPkt.byRFU[1], 2 ) );

    switch( statusPkt.lastResetType )
    {
        case 1:   UpdateStatusGrid( WTTTSStatusGrid, sBRLastReset, "Power-up"  );  break;
        case 2:   UpdateStatusGrid( WTTTSStatusGrid, sBRLastReset, "WDT"       );  break;
        case 3:   UpdateStatusGrid( WTTTSStatusGrid, sBRLastReset, "Brown out" );  break;
        default:  UpdateStatusGrid( WTTTSStatusGrid, sBRLastReset, "Type " + IntToStr( statusPkt.lastResetType ) );  break;
    }

}


BYTE TMainForm::GetSelectedRadioNbr( void )
{
    if( BRRadioNbrCombo->ItemIndex < 0 )
        BRRadioNbrCombo->ItemIndex = 0;

    return BRRadioNbrCombo->ItemIndex;
}


//
// WTTTS
//

void __fastcall TMainForm::WTTTSConnectBtnClick(TObject *Sender)
{
    if( !ValidatePortSettings( WTTTSPortEdit, WTTTSSpeedEdit ) )
        return;

    // Try to create the comm obj
    if( !CreatePort( WTTTSPortEdit->Text.ToInt(), WTTTSSpeedEdit->Text.ToInt(), m_WTTTSPort ) )
        return;

    UpdateStatusGrid( WTTTSStatusGrid, sPortState, "Open on COM" + WTTTSPortEdit->Text );

    // On success, set button states
    WTTTSConnectBtn->Enabled    = false;
    WTTTSDisconnectBtn->Enabled = true;

    SetEditState( WTTTSPortEdit,  false );
    SetEditState( WTTTSSpeedEdit, false );

    EnableGBCtrls( WTTTSActionsGB, true );
    ClearGBTags( WTTTSActionsGB );

    // Enable poll timer
    WTTTSPollTimer->Enabled = true;
}


void __fastcall TMainForm::WTTTSDisconnectBtnClick(TObject *Sender)
{
    // Stop poll timer and release comm obj
    WTTTSPollTimer->Enabled = false;

    ReleasePort( m_WTTTSPort );

    ClearStatusGrid( WTTTSStatusGrid );
    UpdateStatusGrid( WTTTSStatusGrid, sPortState, "Closed" );

    // Set button states
    WTTTSConnectBtn->Enabled    = true;
    WTTTSDisconnectBtn->Enabled = false;

    SetEditState( WTTTSPortEdit,  true );
    SetEditState( WTTTSSpeedEdit, true );

    EnableGBCtrls( WTTTSActionsGB, false );
}


void __fastcall TMainForm::WTTTSBtnClick(TObject *Sender)
{
    // Just set the tag to 1 indicating a message is to be sent
    TButton* currBtn = dynamic_cast<TButton*>(Sender);

    if( currBtn != NULL )
        currBtn->Tag = 1;
}


void __fastcall TMainForm::WTTTSPollTimerTimer(TObject *Sender)
{
    PollPortForData( m_WTTTSPort );

    // Have the parser check for any responses from the unit.
    bool bRFCPending = false;

    WTTTS_PKT_HDR    pktHdr;
    WTTTS_DATA_UNION pktData;

    if( HaveTesTORKRespPkt( m_WTTTSPort.rxBuffer, m_WTTTSPort.rxBuffCount, pktHdr, pktData ) )
    {
        m_WTTTSPort.lastPktTime = GetTickCount();

        UpdateStatusGrid( WTTTSStatusGrid, sLastRxTime, Time().TimeString() );
        UpdateStatusGrid( WTTTSStatusGrid, sLastRxType, IntToStr( pktHdr.pktType ) );

        // We are only interested in RFC commands from the WTTTS device.
        if( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD )
        {
            UpdateWTTTSRFCValues( pktData.rfcPkt );
            bRFCPending = true;
        }
    }

    if( bRFCPending )
    {
        TButton* currBtn = NULL;

        // If no button has been pressed, by default respond with a 'no cmd' pkt
        WTTTS_DATA_UNION payloadData;
        BYTE cmdType    = WTTTS_CMD_NO_CMD;
        BYTE payloadLen = 0;

        if( SetWTTTSTxPwrBtn->Tag == 1 )
        {
            currBtn = SetWTTTSTxPwrBtn;

            cmdType    = WTTTS_CMD_SET_RF_POWER;
            payloadLen = sizeof( WTTTS_SET_RF_PWR_PKT );

            payloadData.pwrPkt.powerLevel = GetPowerLevel( WTTTSTxPwrCombo );
        }
        else if( SetWTTTSChanBtn->Tag == 1 )
        {
            currBtn = SetWTTTSChanBtn;

            cmdType    = WTTTS_CMD_SET_RF_CHANNEL;
            payloadLen = sizeof( WTTTS_SET_CHAN_PKT );

            payloadData.chanPkt.chanNbr = GetChanNbr( WTTTSChanCombo );
        }

        if( SendWTTTSCmd( cmdType, payloadData, payloadLen ) )
        {
            if( currBtn != NULL )
            {
                currBtn->Tag = 0;

                UpdateStatusGrid( WTTTSStatusGrid, sLastCmdTime, Time().TimeString() );
            }
        }
    }
}


bool TMainForm::SendWTTTSCmd( BYTE cmdType, WTTTS_DATA_UNION& payloadData, BYTE payloadLen )
{
    if( m_WTTTSPort.commPort == NULL )
        return false;

    // Allocate and clear command area
    BYTE txBuffer[MAX_WTTTS_PKT_LEN];
    memset( txBuffer, 0, MAX_WTTTS_PKT_LEN );

    WTTTS_PKT_HDR* pHdr  = (WTTTS_PKT_HDR*)txBuffer;

    // Setup header defaults
    pHdr->pktHdr    = WTTTS_HDR_TX;
    pHdr->pktType   = cmdType;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = payloadLen;

    if( payloadLen > 0 )
    {
        BYTE* pPayload = &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pPayload, &payloadData, payloadLen );
    }

    int hdrAndPayLoadLen = sizeof(WTTTS_PKT_HDR) + payloadLen;

    txBuffer[hdrAndPayLoadLen] = CalcWTTTSChecksum( txBuffer, hdrAndPayLoadLen );

    DWORD pktLen = hdrAndPayLoadLen + 1;

    DWORD bytesSent = m_WTTTSPort.commPort->CommSend( txBuffer, pktLen );

    return( bytesSent == pktLen );
}


void TMainForm::UpdateWTTTSRFCValues( const WTTTS_RFC_PKT& rfcPkt )
{
    int temp;

    if( rfcPkt.temperature & 0x80 )
        temp = 0xFFFFFF00 | rfcPkt.temperature;
    else
        temp = rfcPkt.temperature;

    UpdateStatusGrid( WTTTSStatusGrid, sRFCTemp,       FloatToStrF( (float)temp / 2.0, ffFixed, 7, 1 ) );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCBattLife,   "---" );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCRate,       IntToStr( rfcPkt.rfcRate )        + " msecs" );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCTimeout,    IntToStr( rfcPkt.rfcTimeout )     + " msecs" );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCStreamRate, IntToStr( rfcPkt.streamRate )     + " msecs" );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCStreamTO,   IntToStr( rfcPkt.streamTimeout )  + " secs" );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCPairingTO,  IntToStr( rfcPkt.pairingTimeout ) + " secs" );

    switch( rfcPkt.battType )
    {
        case 1:   UpdateStatusGrid( WTTTSStatusGrid, sRFCBattType, "Lithium" );  break;
        case 2:   UpdateStatusGrid( WTTTSStatusGrid, sRFCBattType, "NiMH"    );  break;
        default:  UpdateStatusGrid( WTTTSStatusGrid, sRFCBattType, "Type " + IntToStr( rfcPkt.battType ) ); break;
    }

    switch( rfcPkt.lastReset )
    {
        case 1:   UpdateStatusGrid( WTTTSStatusGrid, sRFCLastReset, "Power-up"  );  break;
        case 2:   UpdateStatusGrid( WTTTSStatusGrid, sRFCLastReset, "WDT"       );  break;
        case 3:   UpdateStatusGrid( WTTTSStatusGrid, sRFCLastReset, "Brown out" );  break;
        default:  UpdateStatusGrid( WTTTSStatusGrid, sRFCLastReset, "Type " + IntToStr( rfcPkt.lastReset ) );  break;
    }

    switch( rfcPkt.currMode )
    {
        case 0:   UpdateStatusGrid( WTTTSStatusGrid, sRFCCurrMode, "Entering deep sleep" );  break;
        case 1:   UpdateStatusGrid( WTTTSStatusGrid, sRFCCurrMode, "Normal"              );  break;
        case 2:   UpdateStatusGrid( WTTTSStatusGrid, sRFCCurrMode, "Low power"           );  break;
        default:  UpdateStatusGrid( WTTTSStatusGrid, sRFCCurrMode, "Type " + IntToStr( rfcPkt.lastReset ) );  break;
    }

    XFER_BUFFER xferBuff;
    xferBuff.byData[0] = rfcPkt.pressure[0];
    xferBuff.byData[1] = rfcPkt.pressure[1];
    xferBuff.byData[2] = rfcPkt.pressure[2];
    xferBuff.byData[3] = 0;

    UpdateStatusGrid( WTTTSStatusGrid, sRFCPressure, IntToStr( (int)xferBuff.dwData ) );

    UpdateStatusGrid( WTTTSStatusGrid, sRFCRPM,    IntToStr( rfcPkt.rpm ) );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCRFChan, IntToStr( rfcPkt.rfChannel ) );
}



//
// Common Handlers
//

bool TMainForm::CreatePort( int portNbr, DWORD portSpeed, PORT_INFO& portInfo )
{
    // Clear return struct params first
    portInfo.commPort    = NULL;
    portInfo.rxBuffCount = 0;
    portInfo.lastRxTime  = 0;
    portInfo.lastPktTime = 0;

    // Create a comm port object. Serial ports are the only type supported
    CCommObj* portObj = new CCommPort();

    if( portObj == NULL )
    {
        MessageDlg( L"Could create comm object.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    CCommPort::CONNECT_PARAMS connParams;

    connParams.iPortNumber   = portNbr;
    connParams.dwLineBitRate = portSpeed;

    CCommObj::COMM_RESULT connResult = portObj->Connect( &connParams );

    if( connResult != CCommObj::ERR_NONE )
    {
        delete portObj;
        portObj = NULL;

        UnicodeString connResultText;

        switch( connResult )
        {
            case CCommObj::ERR_NONE:                 connResultText = "success";           break;
            case CCommObj::ERR_COMM_SET_COMM_STATE:  connResultText = "set state error";   break;
            case CCommObj::ERR_COMM_INV_BUFFER_LEN:  connResultText = "set buffer error";  break;
            case CCommObj::ERR_COMM_OPEN_FAIL:       connResultText = "open port error";   break;
            case CCommObj::ERR_COMM_GET_COMM_STATE:  connResultText = "get state error";   break;
            default:                                 connResultText = "error " + IntToStr( connResult );  break;
        }

        MessageDlg( L"Could not open port: " + connResultText, mtError, TMsgDlgButtons() << mbOK, 0 );

        return false;
    }

    // Fall through means success
    portInfo.commPort = portObj;

    return true;
}


void TMainForm::ReleasePort( PORT_INFO& portInfo )
{
    if( portInfo.commPort != NULL )
    {
        portInfo.commPort->Disconnect();

        delete portInfo.commPort;

        portInfo.commPort = NULL;
    }

    portInfo.rxBuffCount = 0;
    portInfo.lastRxTime  = 0;
    portInfo.lastPktTime = 0;
}


bool TMainForm::ValidatePortSettings( TEdit* portEdit, TEdit* speedEdit )
{
    try
    {
        int testResult = portEdit->Text.ToInt();

        if( testResult <= 0 )
            Abort();
    }
    catch( ... )
    {
        MessageDlg( "You have entered an invalid port number.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    try
    {
        int testResult = speedEdit->Text.ToInt();

        if( testResult <= 0 )
            Abort();
    }
    catch( ... )
    {
        MessageDlg( "You have entered an invalid speed.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    // Fall through means success
    return true;
}


BYTE TMainForm::GetPowerLevel( TComboBox* aCombo )
{
    if( aCombo->ItemIndex < 0 )
        aCombo->ItemIndex = 0;
    else if( aCombo->ItemIndex >= NBR_CC2530_PWR_SETTINGS )
        aCombo->ItemIndex = NBR_CC2530_PWR_SETTINGS - 1;

    return CC2530PowerSettings[aCombo->ItemIndex].byRegValue;
}


BYTE TMainForm::GetChanNbr( TComboBox* aCombo )
{
    if( aCombo->ItemIndex < 0 )
        aCombo->ItemIndex = 0;

    return aCombo->Items->Strings[ aCombo->ItemIndex ].ToInt();
}


void TMainForm::PollPortForData( PORT_INFO& portInfo )
{
    if( portInfo.commPort != NULL )
    {
        DWORD bytesRxd = portInfo.commPort->CommRecv( &(portInfo.rxBuffer[portInfo.rxBuffCount]), RX_BUFF_SIZE - portInfo.rxBuffCount );

        if( bytesRxd > 0 )
        {
            portInfo.lastRxTime  = GetTickCount();
            portInfo.rxBuffCount += bytesRxd;
        }
        else
        {
            // No data received - check if any bytes in the buffer have gone stale
            if( HaveTimeout( portInfo.lastRxTime, 250 ) )
                portInfo.rxBuffCount = 0;
        }
    }
}


void TMainForm::UpdateStatusGrid( TValueListEditor* aGrid, const UnicodeString& key, const UnicodeString& text )
{
    // Look for the key first. If not found, add it
    bool bKeyExists = false;

    for( int iItem = 1; iItem < aGrid->RowCount; iItem++ )
    {
        if( aGrid->Keys[iItem] == key )
        {
            bKeyExists = true;
            break;
        }
    }

    if( bKeyExists )
        aGrid->Values[ key ] = text;
    else
        aGrid->InsertRow( key, text, true );
}


void TMainForm::ClearStatusGrid( TValueListEditor* aGrid )
{
    // Clear all but the title row
    for( int iRow = 1; iRow < aGrid->RowCount; iRow++ )
        aGrid->Values[ aGrid->Keys[iRow] ] = "";
}


