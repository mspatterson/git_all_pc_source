object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'FCC Test Configuration Applet'
  ClientHeight = 472
  ClientWidth = 679
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    679
    472)
  PixelsPerInch = 96
  TextHeight = 13
  object MainPgCtrl: TPageControl
    Left = 8
    Top = 8
    Width = 663
    Height = 425
    ActivePage = BaseRadioSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object BaseRadioSheet: TTabSheet
      Caption = 'Base Radio'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        655
        397)
      object BRStatusGrid: TValueListEditor
        Left = 312
        Top = 16
        Width = 327
        Height = 365
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        TitleCaptions.Strings = (
          'Parameter'
          'Value')
        ColWidths = (
          150
          171)
      end
      object BaseRadioCommsGB: TGroupBox
        Left = 16
        Top = 16
        Width = 281
        Height = 91
        Caption = ' Comms '
        TabOrder = 1
        object Label1: TLabel
          Left = 12
          Top = 27
          Width = 20
          Height = 13
          Caption = 'Port'
        end
        object Label4: TLabel
          Left = 12
          Top = 58
          Width = 30
          Height = 13
          Caption = 'Speed'
        end
        object BRPortEdit: TEdit
          Left = 63
          Top = 24
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 0
          Text = '1'
        end
        object BRConnectBtn: TButton
          Left = 160
          Top = 22
          Width = 105
          Height = 25
          Caption = 'Connect'
          TabOrder = 2
          OnClick = BRConnectBtnClick
        end
        object BRDisconnectBtn: TButton
          Left = 160
          Top = 53
          Width = 105
          Height = 25
          Caption = 'Disconnect'
          Enabled = False
          TabOrder = 3
          OnClick = BRDisconnectBtnClick
        end
        object BRSpeedEdit: TEdit
          Left = 63
          Top = 55
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 1
          Text = '115200'
        end
      end
      object BRActionsGB: TGroupBox
        Left = 16
        Top = 115
        Width = 281
        Height = 266
        Caption = ' Actions '
        TabOrder = 2
        object Label5: TLabel
          Left = 20
          Top = 30
          Width = 27
          Height = 13
          Caption = 'Radio'
        end
        object SetBRChanBtn: TButton
          Left = 160
          Top = 60
          Width = 105
          Height = 25
          Caption = 'Set Chan'
          Enabled = False
          TabOrder = 0
          OnClick = SetBRChanBtnClick
        end
        object SetBRPwrBtn: TButton
          Left = 160
          Top = 91
          Width = 105
          Height = 25
          Caption = 'Set Tx Pwr'
          Enabled = False
          TabOrder = 1
          OnClick = SetBRPwrBtnClick
        end
        object EnableBRFCCModeBtn: TButton
          Left = 160
          Top = 122
          Width = 105
          Height = 25
          Caption = 'Enable FCC Mode'
          Enabled = False
          TabOrder = 2
          OnClick = EnableBRFCCModeBtnClick
        end
        object DisableBRFCCModeBtn: TButton
          Left = 160
          Top = 153
          Width = 105
          Height = 25
          Caption = 'Disable FCC Mode'
          Enabled = False
          TabOrder = 3
          OnClick = DisableBRFCCModeBtnClick
        end
        object BRChanCombo: TComboBox
          Left = 12
          Top = 62
          Width = 128
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 4
          Text = '11'
          Items.Strings = (
            '11'
            '12'
            '13'
            '14'
            '15'
            '16'
            '17'
            '18'
            '19'
            '20'
            '21'
            '22'
            '23'
            '24'
            '25'
            '26')
        end
        object BRTxPwrCombo: TComboBox
          Left = 12
          Top = 93
          Width = 128
          Height = 21
          Style = csDropDownList
          TabOrder = 5
        end
        object BRRadioNbrCombo: TComboBox
          Left = 63
          Top = 27
          Width = 77
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 6
          Text = '0'
          Items.Strings = (
            '0'
            '1')
        end
        object EnableBRCWModeBtn: TButton
          Left = 160
          Top = 186
          Width = 105
          Height = 25
          Caption = 'Enable CW Mode'
          Enabled = False
          TabOrder = 7
          OnClick = EnableBRCWModeBtnClick
        end
        object DisableBRCWModeBtn: TButton
          Left = 160
          Top = 217
          Width = 105
          Height = 25
          Caption = 'Disable CW Mode'
          Enabled = False
          TabOrder = 8
          OnClick = DisableBRCWModeBtnClick
        end
      end
    end
    object WTTTSSheet: TTabSheet
      Caption = 'WTTTS'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        655
        397)
      object WTTTSStatusGrid: TValueListEditor
        Left = 312
        Top = 16
        Width = 327
        Height = 365
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        TitleCaptions.Strings = (
          'Parameter'
          'Value')
        ColWidths = (
          150
          171)
      end
      object WTTTSCommsGB: TGroupBox
        Left = 16
        Top = 16
        Width = 281
        Height = 91
        Caption = ' Comms '
        TabOrder = 1
        object Label2: TLabel
          Left = 12
          Top = 27
          Width = 20
          Height = 13
          Caption = 'Port'
        end
        object Label3: TLabel
          Left = 12
          Top = 58
          Width = 30
          Height = 13
          Caption = 'Speed'
        end
        object WTTTSPortEdit: TEdit
          Left = 63
          Top = 24
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 0
          Text = '1'
        end
        object WTTTSConnectBtn: TButton
          Left = 160
          Top = 22
          Width = 105
          Height = 25
          Caption = 'Connect'
          TabOrder = 2
          OnClick = WTTTSConnectBtnClick
        end
        object WTTTSDisconnectBtn: TButton
          Left = 160
          Top = 53
          Width = 105
          Height = 25
          Caption = 'Disconnect'
          Enabled = False
          TabOrder = 3
          OnClick = WTTTSDisconnectBtnClick
        end
        object WTTTSSpeedEdit: TEdit
          Left = 63
          Top = 55
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 1
          Text = '115200'
        end
      end
      object WTTTSActionsGB: TGroupBox
        Left = 16
        Top = 115
        Width = 281
        Height = 266
        Caption = ' Actions '
        TabOrder = 2
        object SetWTTTSChanBtn: TButton
          Left = 160
          Top = 26
          Width = 105
          Height = 25
          Caption = 'Set Chan'
          Enabled = False
          TabOrder = 0
          OnClick = WTTTSBtnClick
        end
        object SetWTTTSTxPwrBtn: TButton
          Left = 160
          Top = 57
          Width = 105
          Height = 25
          Caption = 'Set Tx Pwr'
          Enabled = False
          TabOrder = 1
          OnClick = WTTTSBtnClick
        end
        object WTTTSChanCombo: TComboBox
          Left = 12
          Top = 28
          Width = 128
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 2
          Text = '11'
          Items.Strings = (
            '11'
            '12'
            '13'
            '14'
            '15'
            '16'
            '17'
            '18'
            '19'
            '20'
            '21'
            '22'
            '23'
            '24'
            '25'
            '26')
        end
        object WTTTSTxPwrCombo: TComboBox
          Left = 12
          Top = 59
          Width = 128
          Height = 21
          Style = csDropDownList
          TabOrder = 3
        end
      end
    end
  end
  object ExitBtn: TButton
    Left = 595
    Top = 439
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Exit'
    TabOrder = 1
    OnClick = ExitBtnClick
  end
  object WTTTSPollTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = WTTTSPollTimerTimer
    Left = 152
    Top = 8
  end
  object BRPollTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = BRPollTimerTimer
    Left = 208
    Top = 8
  end
end
