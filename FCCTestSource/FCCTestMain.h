//---------------------------------------------------------------------------
#ifndef FCCTestMainH
#define FCCTestMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "CommObj.h"
#include "WTTTSProtocolUtils.h"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
    TPageControl *MainPgCtrl;
    TButton *ExitBtn;
    TTabSheet *BaseRadioSheet;
    TTabSheet *WTTTSSheet;
    TValueListEditor *BRStatusGrid;
    TGroupBox *BaseRadioCommsGB;
    TEdit *BRPortEdit;
    TLabel *Label1;
    TButton *BRConnectBtn;
    TButton *BRDisconnectBtn;
    TGroupBox *BRActionsGB;
    TButton *SetBRChanBtn;
    TButton *SetBRPwrBtn;
    TButton *EnableBRFCCModeBtn;
    TButton *DisableBRFCCModeBtn;
    TComboBox *BRChanCombo;
    TComboBox *BRTxPwrCombo;
    TValueListEditor *WTTTSStatusGrid;
    TGroupBox *WTTTSCommsGB;
    TLabel *Label2;
    TEdit *WTTTSPortEdit;
    TButton *WTTTSConnectBtn;
    TButton *WTTTSDisconnectBtn;
    TGroupBox *WTTTSActionsGB;
    TButton *SetWTTTSChanBtn;
    TButton *SetWTTTSTxPwrBtn;
    TComboBox *WTTTSChanCombo;
    TComboBox *WTTTSTxPwrCombo;
    TLabel *Label3;
    TEdit *WTTTSSpeedEdit;
    TLabel *Label4;
    TEdit *BRSpeedEdit;
    TTimer *WTTTSPollTimer;
    TTimer *BRPollTimer;
    TComboBox *BRRadioNbrCombo;
    TLabel *Label5;
    TButton *EnableBRCWModeBtn;
    TButton *DisableBRCWModeBtn;
    void __fastcall ExitBtnClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall BRDisconnectBtnClick(TObject *Sender);
    void __fastcall BRConnectBtnClick(TObject *Sender);
    void __fastcall SetBRChanBtnClick(TObject *Sender);
    void __fastcall SetBRPwrBtnClick(TObject *Sender);
    void __fastcall EnableBRFCCModeBtnClick(TObject *Sender);
    void __fastcall DisableBRFCCModeBtnClick(TObject *Sender);
    void __fastcall WTTTSConnectBtnClick(TObject *Sender);
    void __fastcall WTTTSDisconnectBtnClick(TObject *Sender);
    void __fastcall WTTTSBtnClick(TObject *Sender);
    void __fastcall WTTTSPollTimerTimer(TObject *Sender);
    void __fastcall BRPollTimerTimer(TObject *Sender);
    void __fastcall EnableBRCWModeBtnClick(TObject *Sender);
    void __fastcall DisableBRCWModeBtnClick(TObject *Sender);

private:
    // WTTTS specific functions
    bool SendWTTTSCmd( BYTE cmdType, WTTTS_DATA_UNION& payloadData, BYTE payloadLen );

    void UpdateWTTTSRFCValues( const WTTTS_RFC_PKT& rfcPkt );

    // Base Radio specific functions
    bool SendBRCmd( BYTE cmdType, BASE_STN_DATA_UNION& payloadData, BYTE payloadLen );

    void UpdateBRStatusValues( const BASE_STATUS_STATUS_PKT& statusPkt );

    BYTE GetSelectedRadioNbr( void );

    // Comm objects
    #define RX_BUFF_SIZE 2048

    typedef struct {
        CCommObj* commPort;
        BYTE      rxBuffer[RX_BUFF_SIZE];
        DWORD     rxBuffCount;
        DWORD     lastRxTime;
        DWORD     lastPktTime;
    } PORT_INFO;

    PORT_INFO m_BRPort;
    PORT_INFO m_WTTTSPort;

    // Common handlers
    bool      CreatePort( int portNbr, DWORD portSpeed, PORT_INFO& portInfo );
    void      ReleasePort( PORT_INFO& portInfo );

    bool      ValidatePortSettings( TEdit* portEdit, TEdit* speedEdit );

    BYTE      GetPowerLevel( TComboBox* pwrCombo );
    BYTE      GetChanNbr( TComboBox* pwrCombo );

    void      PollPortForData( PORT_INFO& portInfo );

    void      UpdateStatusGrid( TValueListEditor* aGrid, const UnicodeString& key, const UnicodeString& text );
    void      ClearStatusGrid( TValueListEditor* aGrid );

public:		// User declarations
    __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
