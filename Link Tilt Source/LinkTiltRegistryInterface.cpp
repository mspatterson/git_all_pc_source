#include <vcl.h>
#pragma hdrstop

#include <registry.hpp>

#include "LinkTiltRegistryInterface.h"
#include "ApplUtils.h"

#pragma package(smart_init)


//
// Private Registry Defines
//

static const UnicodeString m_BootSection( "Boot" );
    static const UnicodeString m_LastShutdown     ( "LastShutdown" );
    static const UnicodeString m_ProcPriority     ( "ProcessPriority" );

static const UnicodeString m_SettingsSection( "Settings" );
    static const UnicodeString m_DefaultUOM         ( "Default UOM" );
    static const UnicodeString m_Password           ( "WindowAttributes" );
    static const UnicodeString m_AvgSettingMethod   ( "Avg %d Method" );
    static const UnicodeString m_AvgSettingParam    ( "Avg %d Param" );
    static const UnicodeString m_AvgSettingVariance ( "Avg %d Variance" );
    static const UnicodeString m_AvgSettingLevel    ( "Avg %d Level" );
    static const UnicodeString m_BattValidThresh    ( "Battery Valid Threshold" );
    static const UnicodeString m_BattMinOperV       ( "Battery Min Oper Volts" );
    static const UnicodeString m_BattGoodV          ( "Battery Good Volts" );
    static const UnicodeString m_XAccelComp         ( "X Accel Component" );
    static const UnicodeString m_YAccelComp         ( "Y Accel Component" );
    static const UnicodeString m_RotationOffset     ( "Rotation Offset" );
    static const UnicodeString m_MinAccelAmpl       ( "Min Accel Ampl" );

static const UnicodeString m_CommsSection( "Comms Settings" );
    static const UnicodeString m_portType         ( "Port %d Type" );
    static const UnicodeString m_portName         ( "Port %d Name" );
    static const UnicodeString m_portParam        ( "Port %d Param" );
    static const UnicodeString m_autoAssignPorts  ( "AutoAssignPorts" );
    static const UnicodeString m_radioChanTimeout ( "Radio Chan Timeout" );
    static const UnicodeString m_defaultRadioChan ( "Default Radio Chan" );

static const UnicodeString m_MODBUSSection( "MODBUS Settings" );
    static const UnicodeString m_MODBUSType       ( "Port Type" );
    static const UnicodeString m_MODBUSCommPort   ( "Serial Port" );
    static const UnicodeString m_MODBUSSpeed      ( "Serial Speed" );
    static const UnicodeString m_MODBUSIPPort     ( "IP Port" );
    static const UnicodeString m_MODBUSDevAddr    ( "Device Address" );
    static const UnicodeString m_MODBUSOffsetRegs ( "Offset Registers" );


// Some properties only persist through the execution of the application.
// Those are stored here.

static bool          m_loggingEnabled[NBR_DATA_LOG_TYPES] = { false, };
static UnicodeString m_logFileName[NBR_DATA_LOG_TYPES]    = { "",    };


//
//  Private Functions
//

static TRegistryIniFile* CreateRegIni( void )
{
    // Caller must free the created object
    return new TRegistryIniFile( String( "Software\\Tesco\\LinkTiltManager\\V" ) + IntToStr( GetApplMajorVer() ) );
}


static TRegistryIniFile* CreateSharedRegIni( void )
{
    // Caller must free the created object
    return new TRegistryIniFile( "Software\\Tesco\\Shared" );
}


//
// Public Functions
//

SHUT_DOWN_TYPE GetLastShutdown( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    SHUT_DOWN_TYPE sdtReturn = (SHUT_DOWN_TYPE)(regIni->ReadInteger( m_BootSection, m_LastShutdown, SDT_NEW ) );

    delete regIni;

    return sdtReturn;
}


void SetProgramInUse( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_LastShutdown, SDT_CRASH );

    delete regIni;
}


void SetProgramShutdownGood( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_LastShutdown, SDT_GOOD );

    delete regIni;
}


DWORD GetProcessPriority( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    DWORD procPriority = regIni->ReadInteger( m_BootSection, m_ProcPriority, NORMAL_PRIORITY_CLASS );

    delete regIni;

    return procPriority;
}


void SetProcessPriority( DWORD newPriority )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_ProcPriority, newPriority );

    delete regIni;
}


int GetDefaultUnitsOfMeasure( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    int iResult = regIni->ReadInteger( m_SettingsSection, m_DefaultUOM, 1 /*imperial*/ );

    delete regIni;

    return iResult;
}


void SetDefaultUnitsOfMeasure( int newDefaultUOM )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_SettingsSection, m_DefaultUOM, newDefaultUOM );

    delete regIni;
}


void GetAveragingSettings( AVG_TYPE avgType, AVERAGING_PARAMS& avgParams )
{
    // Set default return values first
    avgParams.avgMethod   = AM_NONE;
    avgParams.avgLevel    = AL_OFF;
    avgParams.param       = 1.0;
    avgParams.absVariance = 0.0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_AvgSettingMethod.w_str(), avgType );
        avgParams.avgMethod = (AVG_METHOD)( regIni->ReadInteger( m_SettingsSection, sKey, avgParams.avgMethod ) );

        sKey.printf( m_AvgSettingParam.w_str(), avgType );
        avgParams.param = regIni->ReadFloat( m_SettingsSection, sKey, avgParams.param );

        sKey.printf( m_AvgSettingLevel.w_str(), avgType );
        avgParams.avgLevel = (AVG_LEVEL)( regIni->ReadInteger( m_SettingsSection, sKey, avgParams.avgLevel ) );

        sKey.printf( m_AvgSettingVariance.w_str(), avgType );
        avgParams.absVariance = regIni->ReadFloat( m_SettingsSection, sKey, avgParams.absVariance );

        delete regIni;
    }
}


void SaveAveragingSettings( AVG_TYPE avgType, const AVERAGING_PARAMS& avgParams )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_AvgSettingMethod.w_str(), avgType );
        regIni->WriteInteger( m_SettingsSection, sKey, avgParams.avgMethod );

        sKey.printf( m_AvgSettingParam.w_str(), avgType );
        regIni->WriteFloat( m_SettingsSection, sKey, avgParams.param );

        sKey.printf( m_AvgSettingLevel.w_str(), avgType );
        regIni->WriteInteger( m_SettingsSection, sKey, avgParams.avgLevel );

        sKey.printf( m_AvgSettingVariance.w_str(), avgType );
        regIni->WriteFloat( m_SettingsSection, sKey, avgParams.absVariance );

        delete regIni;
    }
}


UnicodeString GetSysAdminPassword( void )
{
    // Initialize return result with default password
    UnicodeString sPassword( "tesco" );

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TMemoryStream* pStream = new TMemoryStream();

        if( pStream != NULL )
        {
            int bytesRead = regIni->ReadBinaryStream( m_SettingsSection, m_Password, pStream );

            if( bytesRead > 0 )
            {
                // Decode the password. Byte values are xor'd with 0x85
                AnsiString sTemp;
                sTemp.SetLength( bytesRead );

                BYTE* pData = (BYTE*)( pStream->Memory );

                for( int iByte = 0; iByte < bytesRead; iByte++ )
                    sTemp[iByte+1] = (char)( pData[iByte] ^ 0x85 );

                sPassword = sTemp;
            }

            delete pStream;

        }

        delete regIni;
    }

    return sPassword;
}


void SaveSysAdminPassword( UnicodeString newPW )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TMemoryStream* pStream = new TMemoryStream();

        if( pStream != NULL )
        {
            AnsiString sTemp( newPW );

            pStream->SetSize( sTemp.Length() );

            BYTE* pData = (BYTE*)( pStream->Memory );

            for( int iByte = 0; iByte < sTemp.Length(); iByte++ )
                pData[iByte] = 0x85 ^ sTemp[iByte+1];

            pStream->Position = 0;

            regIni->WriteBinaryStream( m_SettingsSection, m_Password, pStream );

            delete pStream;
        }

        delete regIni;
    }
}


void GetCommSettings( DEV_COMM_TYPE devType, COMMS_CFG& commCfg )
{
    // Init defaults
    commCfg.portType  = CT_UNUSED;
    commCfg.portName  = "";
    commCfg.portParam = 0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sPortTypeKey;
        UnicodeString sPortNameKey;
        UnicodeString sPortParamKey;

        sPortTypeKey.printf ( m_portType.w_str(),  devType );
        sPortNameKey.printf ( m_portName.w_str(),  devType );
        sPortParamKey.printf( m_portParam.w_str(), devType );

        commCfg.portType  = (COMM_TYPE) regIni->ReadInteger( m_CommsSection, sPortTypeKey,  commCfg.portType );
        commCfg.portName  =             regIni->ReadString ( m_CommsSection, sPortNameKey,  commCfg.portName );
        commCfg.portParam =             regIni->ReadInteger( m_CommsSection, sPortParamKey, commCfg.portParam );

        delete regIni;
    }
}


void SaveCommSettings( DEV_COMM_TYPE devType, const COMMS_CFG& commCfg )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sPortTypeKey;
        UnicodeString sPortNameKey;
        UnicodeString sPortParamKey;

        sPortTypeKey.printf ( m_portType.w_str(),  devType );
        sPortNameKey.printf ( m_portName.w_str(),  devType );
        sPortParamKey.printf( m_portParam.w_str(), devType );

        regIni->WriteInteger( m_CommsSection, sPortTypeKey,  commCfg.portType );
        regIni->WriteString ( m_CommsSection, sPortNameKey,  commCfg.portName );
        regIni->WriteInteger( m_CommsSection, sPortParamKey, commCfg.portParam );

        delete regIni;
    }
}


bool GetAutoAssignCommPorts( void )
{
    // Init defaults
    bool bAutoAssignPorts = true;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bAutoAssignPorts = regIni->ReadBool( m_CommsSection, m_autoAssignPorts, bAutoAssignPorts );

        delete regIni;
    }

    return bAutoAssignPorts;
}


void SaveAutoAssignCommPorts( bool bAutoAssignPorts )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool( m_CommsSection, m_autoAssignPorts, bAutoAssignPorts );

        delete regIni;
    }
}


DWORD GetRadioChanWaitTimeout( void )
{
    // Init defaults
    DWORD dwTimeout = 30000;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        dwTimeout = regIni->ReadInteger( m_CommsSection, m_radioChanTimeout, dwTimeout );

        delete regIni;
    }

    return dwTimeout;
}


void SaveRadioChanWaitTimeout( DWORD dwNewTimeout )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_CommsSection, m_radioChanTimeout, dwNewTimeout );

        delete regIni;
    }
}


int GetDefaultRadioChan( void )
{
    // Init defaults
    int iDefaultChan = 20;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        iDefaultChan = regIni->ReadInteger( m_CommsSection, m_defaultRadioChan, iDefaultChan );

        delete regIni;
    }

    return iDefaultChan;
}


bool GetDataLogging( DATA_LOG_TYPE logType, String& logFileName )
{
    // Non-persistent property
    logFileName = m_logFileName[logType];

    return m_loggingEnabled[logType];
}


void SaveDataLogging( DATA_LOG_TYPE logType, bool loggingOn, const String& logFileName )
{
    // Non-persistent property
    m_logFileName[logType]    = logFileName;
    m_loggingEnabled[logType] = loggingOn;
}


void GetBatteryThresholds( BATT_THRESH_VALS& battThresh )
{
    // Set defaults
    battThresh.fReadingValidThresh = 2.5;   // Battery readings below this value are ignored, in V
    battThresh.fMinOperValue       = 3.8;   // Operation not allowed below this threshold, in V
    battThresh.fGoodThresh         = 3.9;   // Battery is in a good state at or above this, in V

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        battThresh.fReadingValidThresh = regIni->ReadFloat( m_SettingsSection, m_BattValidThresh, battThresh.fReadingValidThresh );
        battThresh.fMinOperValue       = regIni->ReadFloat( m_SettingsSection, m_BattMinOperV,    battThresh.fMinOperValue       );
        battThresh.fGoodThresh         = regIni->ReadFloat( m_SettingsSection, m_BattGoodV,       battThresh.fGoodThresh         );

        delete regIni;
    }
}


void SaveBatteryThresholds( const BATT_THRESH_VALS& battThresh )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteFloat( m_SettingsSection, m_BattValidThresh, battThresh.fReadingValidThresh );
        regIni->WriteFloat( m_SettingsSection, m_BattMinOperV,    battThresh.fMinOperValue       );
        regIni->WriteFloat( m_SettingsSection, m_BattGoodV,       battThresh.fGoodThresh         );

        delete regIni;
    }
}


void GetMODBUSSettings( MODBUS_SETTINGS& modbusSettings )
{
    // Set defaults
    modbusSettings.eCommType   = eMCT_EnetTCP;
    modbusSettings.iCommPort   = 0;
    modbusSettings.dwSpeed     = 9600;
    modbusSettings.wIPPort     = 502;
    modbusSettings.byDevAddr   = 1;
    modbusSettings.bOffsetRegs = true;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        modbusSettings.eCommType   = (eModbusCommType)regIni->ReadInteger( m_MODBUSSection, m_MODBUSType, modbusSettings.eCommType );
        modbusSettings.iCommPort   = regIni->ReadInteger( m_MODBUSSection, m_MODBUSCommPort,   modbusSettings.iCommPort );
        modbusSettings.dwSpeed     = regIni->ReadInteger( m_MODBUSSection, m_MODBUSSpeed,      modbusSettings.dwSpeed );
        modbusSettings.wIPPort     = regIni->ReadInteger( m_MODBUSSection, m_MODBUSIPPort,     modbusSettings.wIPPort );
        modbusSettings.byDevAddr   = regIni->ReadInteger( m_MODBUSSection, m_MODBUSDevAddr,    modbusSettings.byDevAddr );
        modbusSettings.bOffsetRegs = regIni->ReadBool   ( m_MODBUSSection, m_MODBUSOffsetRegs, modbusSettings.bOffsetRegs );


        delete regIni;
    }
}


void SaveMODBUSSettings( const MODBUS_SETTINGS& modbusSettings )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_MODBUSSection, m_MODBUSType,       modbusSettings.eCommType );
        regIni->WriteInteger( m_MODBUSSection, m_MODBUSCommPort,   modbusSettings.iCommPort );
        regIni->WriteInteger( m_MODBUSSection, m_MODBUSSpeed,      modbusSettings.dwSpeed );
        regIni->WriteInteger( m_MODBUSSection, m_MODBUSIPPort,     modbusSettings.wIPPort );
        regIni->WriteInteger( m_MODBUSSection, m_MODBUSDevAddr,    modbusSettings.byDevAddr );
        regIni->WriteBool   ( m_MODBUSSection, m_MODBUSOffsetRegs, modbusSettings.bOffsetRegs );

        delete regIni;
    }
}


String GetMODBUSDevDescFile( void )
{
    // For now we hard code this as existing in the same dir as the exe
    return GetExeDir() + "MODBUS Description File.mdf";
}


void GetAngleFactors( ANGLE_FACTORS& angleFactors )
{
    // Set defaults
    angleFactors.iXAccelComponent = 0;
    angleFactors.iYAccelComponent = -1;
    angleFactors.iRotationOffset  = -30;
    angleFactors.fMinAccelAmpl    = 0.0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        angleFactors.iXAccelComponent = regIni->ReadInteger( m_SettingsSection, m_XAccelComp,     angleFactors.iXAccelComponent );
        angleFactors.iYAccelComponent = regIni->ReadInteger( m_SettingsSection, m_YAccelComp,     angleFactors.iYAccelComponent );
        angleFactors.iRotationOffset  = regIni->ReadInteger( m_SettingsSection, m_RotationOffset, angleFactors.iRotationOffset  );
        angleFactors.fMinAccelAmpl    = regIni->ReadFloat  ( m_SettingsSection, m_MinAccelAmpl,   angleFactors.fMinAccelAmpl    );

        delete regIni;
    }
}


void SaveAngleFactors( const ANGLE_FACTORS& angleFactors )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_SettingsSection, m_XAccelComp,     angleFactors.iXAccelComponent );
        regIni->WriteInteger( m_SettingsSection, m_YAccelComp,     angleFactors.iYAccelComponent );
        regIni->WriteInteger( m_SettingsSection, m_RotationOffset, angleFactors.iRotationOffset  );
        regIni->WriteFloat  ( m_SettingsSection, m_MinAccelAmpl,   angleFactors.fMinAccelAmpl    );

        delete regIni;
    }
}


