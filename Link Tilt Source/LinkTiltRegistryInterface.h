#ifndef LinkTiltRegistryInterfaceH
#define LinkTiltRegistryInterfaceH

    #include "SerialPortUtils.h"
    #include "MODBUSTypes.h"
    #include "Averaging.h"

    typedef enum {
        SDT_NEW,       // First time this program has been run
        SDT_GOOD,      // Program has been run before, and closed properly
        SDT_CRASH,     // Program has been run before, but not closed properly
        NBR_SHUT_DOWN_TYPES
    } SHUT_DOWN_TYPE;

    SHUT_DOWN_TYPE GetLastShutdown( void );
    void           SetProgramInUse( void );
    void           SetProgramShutdownGood( void );

    DWORD          GetProcessPriority( void );
    void           SetProcessPriority( DWORD newPriority );

    int            GetDefaultUnitsOfMeasure( void );
    void           SetDefaultUnitsOfMeasure( int newDefaultUOM );

    // Data Averaging setting
    typedef enum{
        AT_ACCEL,
        AT_COMPASS,
        AT_RSSI,
        NBR_AVG_TYPES
    } AVG_TYPE;

    void GetAveragingSettings ( AVG_TYPE avgType,       AVERAGING_PARAMS& avgParams );
    void SaveAveragingSettings( AVG_TYPE avgType, const AVERAGING_PARAMS& avgParams );

    // Misc functions
    UnicodeString  GetSysAdminPassword( void );
    void           SaveSysAdminPassword( UnicodeString newPW );

    // Comms settings
    typedef enum {
        DCT_TM,
        DCT_BASE,
        DCT_MGMT,
        NBR_DEV_COMM_TYPES
    } DEV_COMM_TYPE;

    void GetCommSettings ( DEV_COMM_TYPE devType, COMMS_CFG& commCfg );
    void SaveCommSettings( DEV_COMM_TYPE devType, const COMMS_CFG& commCfg );

    bool GetAutoAssignCommPorts( void );
    void SaveAutoAssignCommPorts( bool bAutoAssignPorts );

    DWORD GetRadioChanWaitTimeout( void );
    void  SaveRadioChanWaitTimeout( DWORD dwNewTimeout );

    int   GetDefaultRadioChan( void );

    DWORD GetSolenoidOnTime( void ) { return 10000; }
        // Solenoids are not used in this app, but required for the MPL device
        // class handler. So just return a fixed default value.

    typedef enum {
        DLT_RFC_DATA,
        NBR_DATA_LOG_TYPES
    } DATA_LOG_TYPE;

    bool  GetDataLogging ( DATA_LOG_TYPE logType, String& logFileName );
    void  SaveDataLogging( DATA_LOG_TYPE logType, bool loggingOn, const String& logFileName );

    typedef struct {
        float fReadingValidThresh;   // Battery readings below this value are ignored, in V
        float fMinOperValue;         // Operation not allowed below this threshold, in V
        float fGoodThresh;           // Battery is in a good state at or above this, in V
    } BATT_THRESH_VALS;

    void GetBatteryThresholds( BATT_THRESH_VALS& battThresh );
    void SaveBatteryThresholds( const BATT_THRESH_VALS& battThresh );

    typedef struct {
        eModbusCommType eCommType;
        int    iCommPort;       // Used for serial comms
        DWORD  dwSpeed;         // Used for serial comms
        WORD   wIPPort;         // Used for UDP / TCP comms. Opens a listening socket on that port number.
        BYTE   byDevAddr;       // Slave or Unit ID
        bool   bOffsetRegs;     // True will cause the MODBUS device to offset register values in packets
    } MODBUS_SETTINGS;

    void GetMODBUSSettings( MODBUS_SETTINGS& modbusSettings );
    void SaveMODBUSSettings( const MODBUS_SETTINGS& modbusSettings );

    String GetMODBUSDevDescFile( void );

    typedef struct {
        int   iXAccelComponent;   // X and Y component determines which of X or Y to use for
        int   iYAccelComponent;   //   tilt angle calculation. These values must only ever
                                  //   be +1, 0, or -1, and one of X or Y must be zero.
        int   iRotationOffset;    // Offset applied after asin() calc to get final arm position
        float fMinAccelAmpl;      // Min value of X**2 + y**2 required to perform angle calc, in g
    } ANGLE_FACTORS;

    void GetAngleFactors( ANGLE_FACTORS& angleFactors );
    void SaveAngleFactors( const ANGLE_FACTORS& angleFactors );

#endif
