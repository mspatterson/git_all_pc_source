//
// This is a generic interface to logging capabilities.
// In the Windows platform, we just redirect log messages
// to one event handler.
//

#include <vcl.h>
#pragma hdrstop

#include "Logging.h"

#pragma package(smart_init)


static TLogMsgEvent m_onAddToLog = NULL;


//
// Public Methods
//

void SetLogEventHandler( TLogMsgEvent eventProc )
{
    m_onAddToLog = eventProc;
}


void OutputDebugMsg( TObject* Sender, const String& sMsg )
{
    if( m_onAddToLog != NULL )
        m_onAddToLog( Sender, sMsg );
}


