#ifndef LoggingH
#define LoggingH

    //
    // This is a generic interface to logging capabilities.
    // In the Windows platform, we just redirect log messages
    // to one event handler.
    //

    typedef void __fastcall (__closure *TLogMsgEvent)(TObject* Sender, const String& sLogMsg);

    // Memo that will get the logs
    void SetLogEventHandler( TLogMsgEvent eventProc );

    // Debug message support
    void OutputDebugMsg( TObject* Sender, const String& sMsg );

#endif
