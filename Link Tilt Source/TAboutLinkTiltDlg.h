//---------------------------------------------------------------------------

#ifndef TAboutLinkTiltDlgH
#define TAboutLinkTiltDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
//---------------------------------------------------------------------------
class TAboutMPLForm : public TForm
{
__published:	// IDE-managed Components
    TImage *TescoAboutImage;
    TLabel *Label21;
    TLabel *MPLVerLabel;
    TLabel *Label27;
    TGroupBox *GroupBox1;
    TLabel *Label22;
    TLabel *Label23;
    TLabel *Label24;
    TLabel *Label25;
    TLabel *Label26;
    TLabel *Label1;
private:	// User declarations
public:		// User declarations
    __fastcall TAboutMPLForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TAboutMPLForm *AboutMPLForm;
//---------------------------------------------------------------------------
#endif
