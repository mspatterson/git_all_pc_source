//******************************************************************************
//
//  CommsMgr.cpp: This module provides a wrapper around the device
//        classes, and implements a thread to keep the comms layers
//        constantly updated.
//
//******************************************************************************
#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>

#include "TCommPoller.h"
#include "CUDPPort.h"
#include "USBPortDiscovery.h"
#include "LinkTiltRegistryInterface.h"

#pragma package(smart_init)


const String TCommPoller::ConnResultStrings[TCommPoller::eCR_NumConnectResult] =
{
    "Initializing",
    "Bad Base Radio port",
    "Bad Tilt Manager port",
    "Bad Management port",
    "Connected",
    "Scanning for Radio",
    "Scanning for Tilt Manager",
    "Winsock() initialization error",
    "Unhandled comms thread exception",
};


static bool GetMutex( HANDLE aMutex, DWORD maxWait )
{
    // Return true if the mutex is acquired
    if( aMutex == NULL )
        return false;

    switch( WaitForSingleObject( aMutex, maxWait ) )
    {
        case WAIT_ABANDONED:
        case WAIT_OBJECT_0:
            // Have access to the mutex now
            return true;

        case WAIT_FAILED:
            // Not a good thing! Call failed.
            break;

        case WAIT_TIMEOUT:
            // Timeout expired!
            break;
    }

    // Fall through means mutex not acquired
    return false;
}


//
// Poll Thread Class Implementation
//

__fastcall TCommPoller::TCommPoller( HWND hwndEvent, DWORD dwEventMsg ) : TThread( false )
{
    // Save the event handle for later use
    m_hwndEvent  = hwndEvent;
    m_dwEventMsg = dwEventMsg;

    // Init state vars. Always set m_connectResult first before
    // setting the poller state
    SetPollerState( PS_INITIALIZING, eCR_Initializing );
    PostEventToClient( eCPE_Initializing );

    // Initialize device variables
    m_device    = NULL;
    m_radio     = NULL;
    m_mgmtPort  = NULL;

    m_dwRadioChanWaitTime = GetRadioChanWaitTimeout();

    // Caller must delete this object on terminate
    FreeOnTerminate = false;

    // Init the thread-safe packet transfer. A critical section
    // is used for this.
    InitializeCriticalSection( &m_criticalSection );

    FlushPendingPktQueue();
}


__fastcall TCommPoller::~TCommPoller()
{
    Terminate();

    switch( WaitForSingleObject( (HANDLE)Handle, 1000 /*msecs*/ ) )
    {
        case WAIT_FAILED:
            // Not a good thing - don't try to delete the object.
            break;

        case WAIT_ABANDONED:
        case WAIT_OBJECT_0:
            // Safe to delete the object in both these cases
            break;

        case WAIT_TIMEOUT:
            // Timeout expired! Don't delete the object in this case
            break;
    }

    // Release the critical section
    DeleteCriticalSection( &m_criticalSection );
}


void __fastcall TCommPoller::Execute()
{
    // Initialize and create objects
    // Winsock is required for UDP ports
    NameThreadForDebugging( AnsiString( "TiltMgrCommThread" ) );

    if( !InitWinsock() )
    {
        SetPollerState( PS_COMMS_FAILED, eCR_WinsockError );
        return;
    }

    // Set the Poller to scanning
    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );

    // Loop here now executing the thread
    try
    {
        while( !Terminated )
        {
            if( GetTMPortLost() || ( PollerState == PS_BR_PORT_SCAN ) || ( PollerState == PS_TM_PORT_SCAN ) )
            {
                // Check which Port Opening method to use. Read this from the
                // registry each time in case it changes
                if( GetAutoAssignCommPorts() )
                {
                    while( !OpenAutomaticPorts() )
                    {
                        if( Terminated )
                            break;

                        Sleep( 100 );
                    }
                }
                else
                {
                    while( !OpenAssignedPorts() )
                    {
                        if( Terminated )
                            break;

                        Sleep( 100 );
                    }
                }
            }

            if( Terminated )
                break;

            // If we are hunting for the TM, loop here until it is found
            DWORD dwRFCWaitStart = GetTickCount();

            while( PollerState == PS_TM_CHAN_SCAN )
            {
                m_device->Update();
                m_radio->Update();

                if( Terminated )
                    break;

                if( !m_radio->IsReceiving )
                {
                    // If the base radio is not receiving, we are in big trouble.
                    // Need to rescan for all devices
                    DeleteDevices();
                    PostEventToClient( eCPE_CommsLost );
                    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
                }
                else if( m_device->DeviceIsRecving )
                {
                    SetPollerState( PS_RUNNING, eCR_Connected );
                    PostEventToClient( eCPE_InitComplete );

                    break;
                }
                else if( HaveTimeout( dwRFCWaitStart, m_dwRadioChanWaitTime ) )
                {
                    // Time to switch channels - the base radio can figure out
                    // the next channel
                    m_radio->GoToNextRadioChannel( TBaseRadioDevice::BRN_TESTORK );

                    dwRFCWaitStart = GetTickCount();
                }
            }

            if( Terminated )
                break;

            // Do are work if we are in the running state
            if( PollerState == PS_RUNNING )
            {
                m_device->Update();
                m_radio->Update();

                if( !m_radio->IsReceiving )
                {
                    // If the base radio is not receiving, we are in big trouble.
                    // Need to rescan for all devices
                    DeleteDevices();
                    PostEventToClient( eCPE_CommsLost );
                    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
                }
                else if( !m_device->DeviceIsRecving )
                {
                    // Note we don't delete devices in this case - first try different channels
                    PostEventToClient( eCPE_CommsLost );
                    SetPollerState( PS_TM_CHAN_SCAN, eCR_HuntingForTM );
                }
                else
                {
                    // Check if a refresh of settings has been requested
                    if( m_refreshSettings )
                    {
                        // Refresh requested - get new values
                        m_currUnits = m_newUnits;

                        // Refresh our RSSI average
                        AVERAGING_PARAMS avgParams;
                        GetAveragingSettings( AT_RSSI, avgParams );
                        // TODO

                        m_dwRadioChanWaitTime = GetRadioChanWaitTimeout();

                        m_device->RefreshSettings();

                        m_refreshSettings = false;
                    }
                }
            }

            // Work done for this iteration
            Sleep( 5 );
        }
    }
    catch( ... )
    {
        SetPollerState( PS_EXCEPTION, eCR_ThreadException );
        PostEventToClient( eCPE_ThreadFailed );
    }

    // Release all comm mgr resources
    DeleteDevices();

    ShutdownWinsock();
}


bool TCommPoller::OpenAssignedPorts( void )
{
    while( true )
    {
        // Delete all devices safely
        DeleteDevices();

        // Create the devices
        CreateDevices();

        // Set the result to Scanning
        SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
        PostEventToClient( eCPE_Initializing );

        COMMS_CFG mgmtCommCfg;
        COMMS_CFG baseCommCfg;
        COMMS_CFG deviceCommCfg;

        GetCommSettings( DCT_MGMT, mgmtCommCfg );
        GetCommSettings( DCT_BASE, baseCommCfg );
        GetCommSettings( DCT_TM,   deviceCommCfg );

        // Attempt to connect our devices
        if( ( m_device->Connect( deviceCommCfg ) ) && ( m_radio->Connect( baseCommCfg ) ) && ( m_mgmtPort->Connect( mgmtCommCfg ) ) )
        {
            // Check if we have a Null-Radio
            if( m_radio->ClassNameIs( "TNullBaseRadio" ) )
            {
                SetPollerState( PS_TM_CHAN_SCAN, eCR_HuntingForTM );
                PostEventToClient( eCPE_Initializing );

                return true;
            }

            // The port could be opened, wait here for 2 seconds for a
            // a message to appear from the base radio
            DWORD dwStartPktCount = m_radio->StatusPktCount;

            DWORD dwEndWait = GetTickCount() + 2500;

            while( GetTickCount() < dwEndWait )
            {
                m_radio->Update();

                if( m_radio->StatusPktCount > dwStartPktCount + 5 )
                {
                    // Found the base radio. Set the TM radio port onto its
                    // default channel and then move on to the TM scan.
                    m_radio->SetRadioChannel( GetDefaultRadioChan(), TBaseRadioDevice::BRN_TESTORK );

                    AfterCommConnect();

                    SetPollerState( PS_TM_CHAN_SCAN, eCR_HuntingForTM );
                    PostEventToClient( eCPE_Initializing );

                    return true;
                }

                if( Terminated )
                    return false;

                Sleep( 50 );
            }

            m_radio->Disconnect();
        }

        // Could not connect to devices, sleep for one second, then try
        // again on the same ports. Break the sleep into 100ms intervals
        // so we can respond to a terminate request quickly.
        for( int iSleepIndex = 0; iSleepIndex < 10; iSleepIndex++ )
        {
            Sleep( 100 );

            if( Terminated )
                return false;
        }
    }
}


bool TCommPoller::OpenAutomaticPorts( void )
{
    while( true )
    {
        // Delete all devices safely
        DeleteDevices();

        // Create the devices
        CreateDevices();

        // Set the result to Scanning
        SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
        PostEventToClient( eCPE_Initializing );

        COMMS_CFG baseCommCfg;
        COMMS_CFG deviceCommCfg;
        COMMS_CFG mgmtCommCfg;

        GetCommSettings( DCT_BASE, baseCommCfg );
        GetCommSettings( DCT_TM,   deviceCommCfg );
        GetCommSettings( DCT_MGMT, mgmtCommCfg );

        // For automatic ports, all port types must be serial
        baseCommCfg.portType   = CT_SERIAL;
        deviceCommCfg.portType = CT_SERIAL;
        mgmtCommCfg.portType   = CT_SERIAL;

        baseCommCfg.portParam   = 115200;
        deviceCommCfg.portParam = 115200;
        mgmtCommCfg.portParam   = 115200;

        // Init search loop
        int iPortNbr   = baseCommCfg.portName.ToIntDef( 1 );
        int iPortStart = iPortNbr;

        while( true )
        {
            // Set the Port Number for the radio
            baseCommCfg.portName = IntToStr( iPortNbr );

            // Attempt to connect our Base Radio
            if( m_radio->Connect( baseCommCfg ) )
            {
                // The port could be opened, wait here for 2 seconds for a
                // a message to appear from the base radio. The base radio
                // sends periodic status messages even if no command is
                // sent to it
                DWORD dwStartPktCount = m_radio->StatusPktCount;

                DWORD dwEndWait = GetTickCount() + 2500;

                while( GetTickCount() < dwEndWait )
                {
                    m_radio->Update();

                    if( m_radio->StatusPktCount > dwStartPktCount + 5 )
                    {
                        // Found the base radio. Set the TM radio port onto its
                        // default channel now
                        m_radio->SetRadioChannel( GetDefaultRadioChan(), TBaseRadioDevice::BRN_TESTORK );

                        SetPollerState( PS_TM_PORT_SCAN, eCR_HuntingForTM );
                        PostEventToClient( eCPE_Initializing );

                        // If we can't find the ports, return false
                        if( !FindDevPorts( baseCommCfg, deviceCommCfg, mgmtCommCfg ) )
                            return false;

                        // If the ports fail to connect, return false
                        if( ( !m_device->Connect( deviceCommCfg ) ) || ( !m_mgmtPort->Connect( mgmtCommCfg ) ) )
                            return false;

                        SaveCommSettings( DCT_BASE, baseCommCfg );
                        SaveCommSettings( DCT_TM,   deviceCommCfg );
                        SaveCommSettings( DCT_MGMT, mgmtCommCfg );

                        AfterCommConnect();

                        SetPollerState( PS_TM_CHAN_SCAN, eCR_HuntingForTM );
                        PostEventToClient( eCPE_Initializing );

                        return true;
                    }

                    if( Terminated )
                        return false;

                    Sleep( 50 );
                }

                m_radio->Disconnect();
            }

            if( Terminated )
                return false;

            // Autoscan enabled, try next port
            iPortNbr++;

            if( iPortStart == iPortNbr )
                break;

            if( iPortNbr > 999 )
                iPortNbr = 1;

            Sleep( 10 );
        }

        Sleep( 100 );
    }
}


bool TCommPoller::FindDevPorts( const COMMS_CFG& baseCommCfg, COMMS_CFG& deviceCommCfg, COMMS_CFG& mgmtCommCfg )
{
    // Set up the CommPort and Connection Params
    CCommPort* pCommPort = new CCommPort();

    CCommPort::CONNECT_PARAMS CommPortParams;

    CommPortParams.dwLineBitRate = 9600;

    int iPortNum = 0;
    int iR0Port  = -1;
    int iR1Port  = -1;

    // Iterate through 1000 port numbers, attempt to connect to each
    while( iPortNum <= 999 )
    {
        CommPortParams.iPortNumber = iPortNum;

        // Check if we can connect to the port, if so, wiggle it's signals
        if( pCommPort->Connect( &CommPortParams ) == CCommObj::ERR_NONE )
        {
            // Get the Port Type of the current CommPort
            PORT_TYPE PortType = GetPortType( pCommPort );

            // Check if the Port Type returned was one we care about
            if( PortType == eRadio0 )
                iR0Port = iPortNum;
            else if( PortType == eRadio1 )
                iR1Port = iPortNum;
        }

        // Always disconnect to be safe
        pCommPort->Disconnect();

        // If we've found both ports in this iteration, try to find Mgmt
        if( ( iR0Port >= 0 ) && ( iR1Port >= 0 ) )
        {
            TList* pPorts = new TList();

            int iRadioPort = baseCommCfg.portName.ToIntDef( -1 );

            if( !GetAssociatedPorts( iRadioPort, pPorts ) )
            {
                delete pPorts;
                break;
            }

            pPorts->Remove( (TObject*)( iRadioPort ) );
            pPorts->Remove( (TObject*)( iR0Port    ) );
            pPorts->Remove( (TObject*)( iR1Port    ) );

            if( pPorts->Count != 1 )
            {
                delete pPorts;
                break;
            }

            // The TM device uses radio port 1
            deviceCommCfg.portName = IntToStr( iR0Port );
            mgmtCommCfg.portName   = IntToStr( (int)( pPorts->List[0] ) );

            delete pPorts;
            delete pCommPort;

            return true;
        }

        // Be safe and check for termination
        if( Terminated )
            break;

        Sleep( 10 );

        iPortNum++;
    }

    // Clean-up and return
    delete pCommPort;

    return false;
}


TCommPoller::PORT_TYPE TCommPoller::GetPortType( CCommPort* pPort )
{
    // Init our Ctrl Status variables
    BYTE bInitCtrlStatus = 0;
    BYTE bHighCtrlStatus = 0;
    BYTE bLastCtrlStatus = 0;

    if( !WigglePort( pPort, bInitCtrlStatus, bHighCtrlStatus, bLastCtrlStatus ) )
        return eUnknown;

    // WigglePort() will raise RTS, drop it, and then raise it again. The base
    // radio reports the inverse state of the signal, so look in the control
    // signal status byte for a low-high-low sequence.
    if( !( bInitCtrlStatus & 0x01 ) && ( bHighCtrlStatus & 0x01 ) && !( bLastCtrlStatus & 0x01 ) )
        return eRadio0;
    else if( !( bInitCtrlStatus & 0x02 ) && ( bHighCtrlStatus & 0x02 ) && !( bLastCtrlStatus & 0x02 ) )
        return eRadio1;

    return eUnknown;
}


bool TCommPoller::WigglePort( CCommPort* pPort, BYTE& bInitCtrlStatus, BYTE& bHighCtrlStatus, BYTE& bLastCtrlStatus )
{
    // Init our Base Radio Packet variables
    BASE_STATUS_STATUS_PKT RadioStatusPkt;
    time_t tLastPkt;

    // Set RTS bit to low
    pPort->SetRTS( true );

    DWORD dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the Initial CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bInitCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    // Set RTS bit to high
    pPort->SetRTS( false );

    dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the High CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bHighCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    // Set RTS bit to low
    pPort->SetRTS( true );

    dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the Last CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bLastCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    return true;
}


void TCommPoller::AfterCommConnect( void )
{
    m_currUnits = GetDefaultUnitsOfMeasure();
    m_newUnits  = GetDefaultUnitsOfMeasure();

    m_refreshSettings  = true;
}


void TCommPoller::CreateDevices( void )
{
    // If we are auto-assigning ports, then real objects are required.
    // Otherwise, construct the object type based on the registry setting.
    if( GetAutoAssignCommPorts() )
    {
        m_mgmtPort = new TRealMgmtPort();
        m_radio    = new TRealBaseRadio();
        m_device   = new TTiltMgrDevice();
    }
    else
    {
        // Create devices based on the registry setting
        COMMS_CFG mgmtCommCfg;
        GetCommSettings( DCT_MGMT, mgmtCommCfg );

        COMMS_CFG baseCommCfg;
        GetCommSettings( DCT_BASE, baseCommCfg );

        COMMS_CFG tmCommCfg;
        GetCommSettings( DCT_TM, tmCommCfg );

        if( mgmtCommCfg.portType == CT_UNUSED )
            m_mgmtPort = new TNullMgmtPort();
        else
            m_mgmtPort = new TRealMgmtPort();

        if( baseCommCfg.portType == CT_UNUSED )
            m_radio = new TNullBaseRadio();
        else
            m_radio = new TRealBaseRadio();

        m_device = new TTiltMgrDevice();
    }

    // For the TM, we use the 'auto switch control' feature
    m_radio->AutoSwitchControl = true;

    // Hook the TM device event handlers
    m_device->OnNewDevice = OnTMChanged;
    m_device->OnConnected = OnTMConnected;
    m_device->OnRFCRecvd  = OnTMRFCRecvd;
}


void TCommPoller::DeleteDevices( void )
{
    // Delete the radio if it exists
    if( m_radio != NULL )
    {
        delete m_radio;
        m_radio = NULL;
    }

    // Delete the device if it exists
    if( m_device != NULL )
    {
        delete m_device;
        m_device = NULL;
    }

    // Delete the Mgmt Port if it exists
    if( m_mgmtPort != NULL )
    {
        delete m_mgmtPort;
        m_mgmtPort = NULL;
    }
}


bool TCommPoller::CommsGetStats( PORT_STATS& portStats )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_device != NULL )
    {
        m_device->GetStats( portStats );
        return true;
    }

    return false;
}


void TCommPoller::CommsClearStats( void )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_device != NULL )
        m_device->ClearStats();
}


bool TCommPoller::GetRadioStats( PORT_STATS& radioStats )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_radio != NULL )
        return m_radio->GetCommStats( radioStats );

    return false;
}


void TCommPoller::ClearRadioStats( void )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_radio != NULL )
        m_radio->ClearCommStats();
}


bool TCommPoller::MgmtGetStats( PORT_STATS& mgmtStats )
{
    // We only report the connection type for the management port
    if( m_mgmtPort == NULL )
        return false;

    if( !m_mgmtPort->GetCommStats( mgmtStats ) )
        return false;

    return true;
}


void TCommPoller::ClearMgmtStats( void )
{
    // Currently this does nothing, as we don't track any data traffic
    // on the management port
}


String TCommPoller::GetTMPortDesc( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return "";

    // If we aren't connected, overwrite the dev status with comm status
    if( PollerState != PS_RUNNING )
        return m_connectResult;

    return m_device->DeviceStatus;
}


String TCommPoller::GetTMStatusText( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return "";

    // If we aren't connected, overwrite the dev status with comm status
    if( PollerState != PS_RUNNING )
        return m_connectResult;

    return m_device->DeviceStatus;
}


bool TCommPoller::GetTMIsIdle( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    // Rely on the 'can start data collection' property for this
    if( !m_device->IsConnected )
        return false;

    return m_device->DeviceIsIdle;
}


bool TCommPoller::GetTMIsRecving( void )
{
    if( m_device == NULL )
        return false;

    return m_device->DeviceIsRecving;
}


bool TCommPoller::GetTMIsConn( void )
{
    if( m_device == NULL )
        return false;

    return m_device->IsConnected;
}


bool TCommPoller::GetTMPortLost( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->PortLost;
}


float TCommPoller::GetTMTemp( void )
{
    if( m_device == NULL )
        return 0.0;

    return m_device->Temperature;
}



TTiltMgrDevice::BatteryLevelType TCommPoller::GetTMBattLevel( void )
{
    if( m_device == NULL )
        return TTiltMgrDevice::eBL_Unknown;

    return m_device->BatteryLevel;
}


String TCommPoller::GetBaseRadioStatus( void )
{
    if( m_radio == NULL )
        return "";

    return m_radio->DeviceStatus;
}


bool TCommPoller::GetBaseRadioPortLost( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->PortLost;
}


bool TCommPoller::GetBaseRadioIsConn( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->IsConnected;
}


bool TCommPoller::GetBaseRadioIsRecving( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->IsReceiving;
}


DWORD TCommPoller::GetBaseRadioStatusPktCount( void )
{
    if( m_radio == NULL )
        return 0;

    return m_radio->StatusPktCount;
}


void TCommPoller::SetPollerState( POLLER_STATE epsNew, CONNECT_RESULT eCRMsgEnum )
{
    m_pollerState   = epsNew;
    m_connectResult = ConnResultStrings[eCRMsgEnum];
}


void TCommPoller::PostEventToClient( CommPollerEvent eEvent, DWORD dwLParam )
{
    ::PostMessage( m_hwndEvent, m_dwEventMsg, eEvent, dwLParam );
}


bool TCommPoller::GetLastStatusPktInfo( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->ShowLastStatusPkt( pCaptions, pValues );
}


bool TCommPoller::GetLastStatusPkt( time_t& pktTime, GENERIC_RFC_PKT& lastPkt )
{
    if( m_device == NULL )
       return false;

    return m_device->GetLastStatusPkt( pktTime, lastPkt );
}


bool TCommPoller::GetLastAvgReading( TTiltMgrDevice::DEVICE_READING& lastPkt )
{
    if( m_device == NULL )
       return false;

    return m_device->GetLastDevReading( lastPkt );
}


bool TCommPoller::GetLastRadioStatusPktInfo( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_radio == NULL )
       return false;

    return m_radio->ShowLastStatusPkt( pCaptions, pValues );
}


bool TCommPoller::GetLastRadioStatusPkt( time_t& tLastPkt, BASE_STATUS_STATUS_PKT& lastStatus )
{
    if( m_radio == NULL )
       return false;

    return m_radio->GetLastStatusPkt( tLastPkt, lastStatus );
}


int TCommPoller::GetRadioChannel( TBaseRadioDevice::BASE_RADIO_NBR radioNbr )
{
    if( ( m_radio == NULL ) || ( !m_radio->IsReceiving ) )
        return 0;

    time_t tLastPkt;
    BASE_STATUS_STATUS_PKT statusPkt;

    if( !m_radio->GetLastStatusPkt( tLastPkt, statusPkt ) )
        return 0;

    if( ( radioNbr >= 0 ) && ( radioNbr < NBR_BASE_RADIO_CHANS ) )
        return statusPkt.radioInfo[radioNbr].chanNbr;

    // Fall through means invalid radio number passed
    return 0;
}


TBaseRadioDevice::SET_RADIO_CHAN_RESULT TCommPoller::SetRadioChannel( int newChanNbr, TBaseRadioDevice::BASE_RADIO_NBR radioNbr )
{
    if( m_radio == NULL )
       return TBaseRadioDevice::SRCR_NO_RADIO;

     return m_radio->SetRadioChannel( newChanNbr, radioNbr );
}


void TCommPoller::RefreshSettings( int unitsOfMeasure )
{
    // Record that a refresh has been requested. The actual refreshing
    // of data will happen in the worker thread.
    m_newUnits        = unitsOfMeasure;
    m_refreshSettings = true;
}


bool TCommPoller::GetCalDataRaw( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->GetCalDataRaw( pCaptions, pValues );
}


bool TCommPoller::GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->GetCalDataFormatted( pCaptions, pValues );
}


bool TCommPoller::SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    if( m_device == NULL )
       return false;

    return m_device->SetCalDataFormatted( pCaptions, pValues );
}


bool TCommPoller::ReloadCalDataFromDevice( void )
{
    if( m_device == NULL )
       return false;

    return m_device->ReloadCalDataFromDevice();
}


bool TCommPoller::WriteCalDataToDevice( void )
{
    if( m_device == NULL )
       return false;

    return m_device->WriteCalDataToDevice();
}


bool TCommPoller::GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo )
{
    if( m_device == NULL )
       return false;

    return m_device->GetDeviceHWInfo( devHWInfo );
}


bool TCommPoller::GetDeviceFWRev( BYTE& byMajor, BYTE& byMinor, BYTE& byRelease, BYTE& byBuild )
{
    if( m_device == NULL )
       return false;

    return m_device->GetDeviceFWRev( byMajor, byMinor, byRelease, byBuild );
}


bool TCommPoller::GetDeviceMfgInfo( WTTTS_MFG_DATA_STRUCT& mfgInfo )
{
    if( m_device == NULL )
       return false;

    return m_device->GetDeviceMfgInfo( mfgInfo );
}


bool TCommPoller::GetDeviceCfgStraps( BYTE& byStraps )
{
    if( m_device == NULL )
       return false;

    return m_device->GetDeviceCfgStraps( byStraps );
}


bool TCommPoller::GetDeviceBatteryInfo( BATTERY_INFO& battInfo )
{
    if( m_device == NULL )
       return false;

    return m_device->GetDeviceBatteryInfo( battInfo );
}


//
// Queue Handling Functions
//

static int AdvanceIndex( int currIndex )
{
    currIndex++;

    if( currIndex >= MAX_DEV_READINGS )
        currIndex = 0;

    return currIndex;
}


void TCommPoller::FlushPendingPktQueue( void )
{
    // Flushes all pending packets from the queue. This is a thread-safe function.
    EnterCriticalSection( &m_criticalSection );

    try
    {
        m_pktReadIndex  = 0;
        m_pktWriteIndex = 0;
    }
    __finally
    {
        LeaveCriticalSection ( &m_criticalSection );
    }
}


void TCommPoller::AddPktToPendingQueue( const TTiltMgrDevice::DEVICE_READING& nextReading )
{
    // Adds a packet received by the serial port into the pending queue. Will
    // overwrite an old packet's data if the queue is full.
    EnterCriticalSection ( &m_criticalSection );

    // Make sure we have room for this packet. If not, toss the
    // oldest packet.
    int newWriteIndex = AdvanceIndex( m_pktWriteIndex );

    if( newWriteIndex == m_pktReadIndex )
        m_pktReadIndex = AdvanceIndex( m_pktReadIndex );

    // Store the sample
    m_pktQueue[m_pktWriteIndex] = nextReading;

    // Okay to advance the write index now
    m_pktWriteIndex = newWriteIndex;

    LeaveCriticalSection ( &m_criticalSection );
}


bool TCommPoller::GetPktFromQueue( TTiltMgrDevice::DEVICE_READING& nextReading )
{
    // Gets the next packet from the queue. Returns true if a packet was
    // pending (and copied), false otherwise. This function is thread-safe.
    bool havePkt = false;

    EnterCriticalSection ( &m_criticalSection );

    // Check if we have packets to report
    if( m_pktReadIndex != m_pktWriteIndex )
    {
        nextReading = m_pktQueue[m_pktReadIndex];
        havePkt     = true;

        // Advance our read index now
        m_pktReadIndex = AdvanceIndex( m_pktReadIndex );
    }

    LeaveCriticalSection ( &m_criticalSection );

    return havePkt;
}


void __fastcall TCommPoller::OnTMConnected( TObject* pTTDev )
{
    // TODO
}


void __fastcall TCommPoller::OnTMChanged( TObject* pTTDev )
{
    // TODO
}


void __fastcall TCommPoller::OnTMRFCRecvd( TObject* pTTDev )
{
    // If an RFC has been received, add it to our queue and let the client know
    TTiltMgrDevice::DEVICE_READING lastReading;

    if( m_device->GetLastDevReading( lastReading ) )
    {
        AddPktToPendingQueue( lastReading );

        PostEventToClient( eCPE_RFCRecvd );
    }
}

