#ifndef TCommPollerH
#define TCommPollerH

#include "WTTTSDefs.h"
#include "TTiltMgrDevice.h"
#include "BaseRadioDevice.h"
#include "MgmtPortDevice.h"


// The TCommPoller class does most of its work in a separate thread. It provides
// events notifications through a PostMessage() call back to a client window.
// The following enums are passed as the WParam member in the message. Depending
// on the WParam, an LParam value may also be provided as noted below.
typedef enum {
    eCPE_Initializing,          // Event posted while comm ports are being opened; gets posted multiple times
    eCPE_InitComplete,          // Ports are open and ready to start mode operating
    eCPE_PortLost,              // Event posted when a port has been lost
    eCPE_CommsLost,             // Event posted when we've lost comms with either the TM or base radio
    eCPE_ThreadFailed,          // Unhandled exception in poll thread
    eCPE_RFCRecvd,              // RFC received, new status values will be available
    eCPE_NbrCommPollerEvents
} CommPollerEvent;


class TCommPoller : public TThread
{
  public:

    typedef enum {
        PS_INITIALIZING,       // Thread is starting
        PS_BR_PORT_SCAN,       // Looking for base radio comm port
        PS_TM_PORT_SCAN,       // Looking for Tilt Mgr comm port
        PS_TM_CHAN_SCAN,       // Looking for Tilt Mgr radio channel
        PS_RUNNING,            // Ports open and have Tilt Mgr
        PS_COMMS_FAILED,       // Comm error, see ConnectResult for more info
        PS_EXCEPTION,          // Thread loop failed due to exception being thrown
        NBR_POLLER_ERRORS
    } POLLER_STATE;

    typedef enum {
        eAT_None,              // No alarm
        eAT_LowBattery,        // Low battery alarm
        eAT_NbrAlarmTypes
    } AlarmType;

  private:

    typedef enum {
        eCR_Initializing,
        eCR_BadBaseRadioPort,
        eCR_BadTMPort,
        eCR_BadMgmtPort,
        eCR_Connected,
        eCR_HuntingForBase,
        eCR_HuntingForTM,
        eCR_WinsockError,
        eCR_ThreadException,
        eCR_NumConnectResult
    } CONNECT_RESULT;

    static const String ConnResultStrings[eCR_NumConnectResult];

    typedef enum {
        eUnknown,
        eRadio0,
        eRadio1,
        eBase,
        eMgmt
    } PORT_TYPE;

    TTiltMgrDevice*   m_device;
    TBaseRadioDevice* m_radio;
    TMgmtPortDevice*  m_mgmtPort;

    POLLER_STATE      m_pollerState;
    String            m_connectResult;

    DWORD             m_dwRadioChanWaitTime;

    // Event notification window handle
    HWND              m_hwndEvent;
    DWORD             m_dwEventMsg;

    bool IsThreadRunning( void ) { return( m_device != NULL ); }

    void SetPollerState( POLLER_STATE epsNew, CONNECT_RESULT eCRMsgEnum );
    void PostEventToClient( CommPollerEvent eEvent, DWORD dwLParam = 0 );

    bool OpenAssignedPorts( void );
    bool OpenAutomaticPorts( void );

    bool FindDevPorts( const COMMS_CFG& radioCommCfg, COMMS_CFG& deviceCommCfg, COMMS_CFG& mgmtCommCfg );
    PORT_TYPE GetPortType( CCommPort* pPort );
    bool WigglePort( CCommPort* pPort, BYTE& bInitCtrlStatus, BYTE& bHighCtrlStatus, BYTE& bLastCtrlStatus );

    void AfterCommConnect( void );

    void CreateDevices( void );
    void DeleteDevices( void );

    void __fastcall OnTMConnected( TObject* pTMDev );
    void __fastcall OnTMChanged  ( TObject* pTMDev );
    void __fastcall OnTMRFCRecvd ( TObject* pTMDev );

  protected:

    void __fastcall Execute();
        // Worker thread function

    void FlushPendingPktQueue( void );
        // Flushes all pending packets from the queue.

    void AddPktToPendingQueue( const TTiltMgrDevice::DEVICE_READING& nextReading );
        // Adds a packet received by the port into the pending queue. Will
        // overwrite an old packet's data if the queue is full.

    bool GetPktFromQueue( TTiltMgrDevice::DEVICE_READING& nextReading );
        // Gets the next reading from the queue. Returns true if a reading
        // was returned.

    // The max number of readings should be such that data won't be lost
    // during the longest possible delay in the calling process
    #define MAX_DEV_READINGS   100
    TTiltMgrDevice::DEVICE_READING m_pktQueue[MAX_DEV_READINGS];

    int m_pktReadIndex;
    int m_pktWriteIndex;

    // Units of measure
    int      m_currUnits;
    int      m_newUnits;

    // The following flag is set by the main thread if this thread should
    // reload settings from the registry. Note that comms settings are
    // not reloaded by this process
    bool     m_refreshSettings;

    // A critical section controls the access of shared objects between threads
    CRITICAL_SECTION m_criticalSection;

    // Property getter methods
    String   GetTMStatusText( void );
    String   GetTMPortDesc( void );
    bool     GetTMIsIdle( void );
    bool     GetTMIsRecving( void );
    bool     GetTMIsConn( void );
    bool     GetTMPortLost( void );

    float    GetTMTemp( void );

    TTiltMgrDevice::BatteryLevelType GetTMBattLevel( void );

    String   GetBaseRadioStatus( void );
    bool     GetBaseRadioPortLost( void );
    bool     GetBaseRadioIsConn( void );
    bool     GetBaseRadioIsRecving( void );
    DWORD    GetBaseRadioStatusPktCount( void );

  public:

    __fastcall TCommPoller( HWND hwndEvent, DWORD dwEventMsg );
    virtual __fastcall ~TCommPoller();
        // Constructor and destructor. Because the comm poller does most of
        // its work in a thread, direct event callbacks cannot be used. So
        // instead messages are posted to hwndEvent window when events
        // occur

    bool GetNextReading( TTiltMgrDevice::DEVICE_READING& nextReading );

    __property bool          ThreadRunning  = { read = IsThreadRunning };
    __property POLLER_STATE  PollerState    = { read = m_pollerState };
    __property UnicodeString ConnectResult  = { read = m_connectResult };

    void RefreshSettings( int unitsOfMeasure );
        // Informs the poller is must reload registry-based settings

    //
    // TiltMgr Specific Methods
    //

    __property String        TMPortDesc     = { read = GetTMPortDesc };
    __property bool          TMIsConnected  = { read = GetTMIsConn };

    __property String        TMStatus       = { read = GetTMStatusText };
    __property bool          TMIsIdle       = { read = GetTMIsIdle };
    __property bool          TMIsRecving    = { read = GetTMIsRecving };
    __property bool          TMPortLost     = { read = GetTMPortLost };

    __property float         TMTemperature  = { read = GetTMTemp };

    __property TTiltMgrDevice::BatteryLevelType TMBatteryLevel = { read = GetTMBattLevel };

    bool CommsGetStats( PORT_STATS& portStats );
    void CommsClearStats( void );

    bool GetLastStatusPktInfo( TStringList* pCaptions, TStringList* pValues );
    bool GetLastStatusPkt( time_t& pktTime, GENERIC_RFC_PKT& lastPkt );
        // Pass through function that pulls last status packet information
        // out of the underlying device object.

    bool GetLastAvgReading( TTiltMgrDevice::DEVICE_READING& lastPkt );
        // Pass through function that pulls the last average reading
        // directly out of the device

    bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues );
    bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
    bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
        // Pass through functions that get / set calibration data. Not
        // all devices support on-board calibration memory.

    bool ReloadCalDataFromDevice( void );
        // Causes the device to clear it internal cached cal data and
        // reload the data from the device.

    bool WriteCalDataToDevice( void );
        // Writes the calibration data currently cached in the software
        // to the WTTTS device.

    bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo );
    bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo );
    bool GetDeviceFWRev( BYTE& byMajor, BYTE& byMinor, BYTE& byRelease, BYTE& byBuild );
    bool GetDeviceMfgInfo( WTTTS_MFG_DATA_STRUCT& mfgInfo );
    bool GetDeviceCfgStraps( BYTE& byStraps );
        // Direct access to device information

    //
    // Base Radio Specific Methods
    //

    __property String        BaseRadioStatus         = { read = GetBaseRadioStatus };
    __property bool          BaseRadioIsConnected    = { read = GetBaseRadioIsConn };
    __property bool          BaseRadioIsRecving      = { read = GetBaseRadioIsRecving };
    __property bool          BaseRadioPortLost       = { read = GetBaseRadioPortLost };
    __property DWORD         BaseRadioStatusPktCount = { read = GetBaseRadioStatusPktCount };

    bool GetRadioStats( PORT_STATS& radioStats );
    void ClearRadioStats( void );

    bool GetLastRadioStatusPktInfo( TStringList* pCaptions, TStringList* pValues );
        // Pass through function that pulls last status packet information
        // out of the underlying device object.

    bool GetLastRadioStatusPkt( time_t& tLastPkt, BASE_STATUS_STATUS_PKT& lastStatus );
        // Returns the underlying status packet data

    int GetRadioChannel( TBaseRadioDevice::BASE_RADIO_NBR radioNbr );
    TBaseRadioDevice::SET_RADIO_CHAN_RESULT SetRadioChannel( int newChanNbr, TBaseRadioDevice::BASE_RADIO_NBR radioNbr );
        // Gets / sets a radio to operate on a given channel

    //
    // Management Port Specific Methods
    //

    bool MgmtGetStats( PORT_STATS& mgmtStats );
    void ClearMgmtStats( void );
};

#endif
