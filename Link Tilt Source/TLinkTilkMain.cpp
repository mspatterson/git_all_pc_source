#include <vcl.h>
#pragma hdrstop

#include "TLinkTilkMain.h"
#include "TAboutMPLDlg.h"
#include "TAckDlg.h"
#include "TPlugJobsDlg.h"
#include "TPlugJobDetailsDlg.h"
#include "TProjectLogDlg.h"
#include "MPLSettingsDlg.h"
#include "PasswordInputDlg.h"
#include "CommsMgr.h"
#include "SerialPortUtils.h"
#include "ApplUtils.h"
#include "TescoShared.h"

#pragma package(smart_init)
#pragma link "AdvSmoothExpanderButtonPanel"
#pragma link "AdvSmoothExpanderPanel"
#pragma link "AdvSmoothPanel"
#pragma link "AdvSmoothToggleButton"
#pragma link "AdvSmoothProgressBar"
#pragma link "GDIPPictureContainer"
#pragma resource "*.dfm"


TMPLMainForm *MPLMainForm;


static const String PasswordDlgCaption( "Administrator Password Required" );
static const String BadPWEnteredMsg( "You have entered the incorrect password." );
static const String EmptyCaption( "---" );

static const TColor clPlugGreen = (TColor)0x0FAF03;


// Battery level resource names.
const String BattLevelResNames[TMPLDevice::eNbrBattLevels] = {
    "BattLevelImg_Unknown",
    "BattLevelImg_Low",
    "BattLevelImg_Medium",
    "BattLevelImg_Full",
};


__fastcall TMPLMainForm::TMPLMainForm(TComponent* Owner) : TForm(Owner)
{
    // Set initial button states
    EnableLaunchBtn->Enabled = false;

    m_LaunchBtns[0] = Launch1Btn;
    m_LaunchBtns[1] = Launch2Btn;

    for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
    {
        m_LaunchBtns[iPlug]->Enabled = false;
        m_LaunchBtns[iPlug]->Tag     = iPlug;
        m_LaunchBtns[iPlug]->Color            = clPlugGreen;
        m_LaunchBtns[iPlug]->ColorDisabled    = (TColor)0x404040;
        m_LaunchBtns[iPlug]->BorderColor      = clTESCOBlue;
        m_LaunchBtns[iPlug]->BorderInnerColor = clTESCOBlue;
    }

    // Create the plug controls - set defaults for size and position for now.
    // That will get updated with the form is shown
    for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
    {
        m_pPlugControls[iPlug] = new TPlugControl( this, ControlPanel );

        m_pPlugControls[iPlug]->Top     = m_LaunchBtns[iPlug]->Top;
        m_pPlugControls[iPlug]->Left    = m_LaunchBtns[iPlug]->Left + m_LaunchBtns[iPlug]->Width + 16;
        m_pPlugControls[iPlug]->Width   = 200;
        m_pPlugControls[iPlug]->Height  = m_LaunchBtns[iPlug]->Height;
        m_pPlugControls[iPlug]->PlugNbr = iPlug + 1;
        m_pPlugControls[iPlug]->OnClick = OnPlugLaunchedClick;

        m_pPlugControls[iPlug]->Initialize( ePCS_NotLoaded );
    }

    // Set the launch state to our loading state
    SetLaunchState( eLS_Loading );

    // Set border color of buttons (can't do this special colour at design time)
    EnableLaunchBtn->BorderColor      = clTESCOBlue;
    EnableLaunchBtn->BorderInnerColor = clTESCOBlue;
    EnableLaunchBtn->ColorDisabled    = (TColor)0x404040;
    EnableLaunchBtn->BorderColor      = clTESCOBlue;
    EnableLaunchBtn->BorderInnerColor = clTESCOBlue;

    ProjSettingsBtn->BorderColor      = clTESCOBlue;
    ProjSettingsBtn->BorderInnerColor = clTESCOBlue;

    ProjLogBtn->BorderColor      = clTESCOBlue;
    ProjLogBtn->BorderInnerColor = clTESCOBlue;

    SysSettingsBtn->BorderColor      = clTESCOBlue;
    SysSettingsBtn->BorderInnerColor = clTESCOBlue;

    AboutBtn->BorderColor      = clTESCOBlue;
    AboutBtn->BorderInnerColor = clTESCOBlue;

    // Set marque colors for progress bars
    BaseRadioProgBar->MarqueeColor = clPlugGreen;
    MPLLinkProgBar->MarqueeColor   = clPlugGreen;

    // In this version, the project settings button is disabled
    ProjSettingsBtn->Enabled = false;

    // Reduce flickering on the plug controls panel
    ControlPanel->DoubleBuffered = true;

    // Settings are good at this point. Load default units of measure
    m_defaultUOM = (UNITS_OF_MEASURE) GetDefaultUnitsOfMeasure();

    // Init operating params
    m_dwPlugDetectTimeout  = GetPlugDetectTimeout();
    m_dwLaunchClickTimeout = GetLaunchClickTimeout();

    // Next, set the program in use flag so that we can
    // detect if the program crashed on the next launch.
    m_lastShutdownType = GetLastShutdown();
    SetProgramInUse();
}


void __fastcall TMPLMainForm::FormShow(TObject *Sender)
{
    // Resize the window according to the screen area. The max size
    // we'll show is 1024 x 768
    const int MaxFormWidth  = 1024;
    const int MaxFormHeight =  468;

    RECT rcWorkArea;

    if( SystemParametersInfo( SPI_GETWORKAREA, 0, &rcWorkArea, 0 ) )
    {
        if( rcWorkArea.right - rcWorkArea.left > MaxFormWidth )
            rcWorkArea.right = rcWorkArea.left + MaxFormWidth;

        if( rcWorkArea.bottom - rcWorkArea.top > MaxFormHeight )
            rcWorkArea.bottom = rcWorkArea.top + MaxFormHeight;
    }
    else
    {
        rcWorkArea.left   = 0;
        rcWorkArea.top    = 0;
        rcWorkArea.right  = MaxFormWidth;
        rcWorkArea.bottom = MaxFormHeight;
    }

    // Now resize the form.
    Top    = rcWorkArea.top;
    Left   = rcWorkArea.left;
    Width  = rcWorkArea.right  - rcWorkArea.left;
    Height = rcWorkArea.bottom - rcWorkArea.top;

    // Now re-size the plug control area. Buttons and plug controls
    // are centered under the enable button
    const int CtrlMargin = 40;
    const int CtrlGap    = 20;

    int controlWidth = ( EnableLaunchBtn->Width - 2 * CtrlMargin - CtrlGap ) / 2;
    int btnLeft      = EnableLaunchBtn->Left + CtrlMargin;
    int plugCtrlLeft = btnLeft + controlWidth + CtrlGap;

    // For vertical spacing, controls go from upwards 1 to 5
    const int VertMargin  = 20;
    const int VertGap     = 10;
    const int PanelFactor =  8;  // Panel client area is less than panel height

    int vertSpace     = ControlPanel->Height - PanelFactor - 2 * VertMargin - ( EnableLaunchBtn->Top + EnableLaunchBtn->Height );
    int controlHeight = ( vertSpace - VertGap * ( NBR_MPL_PLUGS - 1 ) ) / NBR_MPL_PLUGS;
    int controlTop    = EnableLaunchBtn->Top + EnableLaunchBtn->Height + VertMargin + ( NBR_MPL_PLUGS - 1 ) * ( controlHeight + VertGap );

    for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
    {
        m_LaunchBtns[iPlug]->Top    = controlTop;
        m_LaunchBtns[iPlug]->Height = controlHeight;
        m_LaunchBtns[iPlug]->Left   = btnLeft;
        m_LaunchBtns[iPlug]->Width  = controlWidth;

        // Bring the plug control top and bottom in by 1 pixel to make the
        // alignment look better with the (fuzzy) launch button edges
        m_pPlugControls[iPlug]->Top     = controlTop + 1;
        m_pPlugControls[iPlug]->Left    = plugCtrlLeft;
        m_pPlugControls[iPlug]->Width   = controlWidth;
        m_pPlugControls[iPlug]->Height  = controlHeight - 2;

        controlTop -= controlHeight + VertGap;
    }

    // Tell the AckForm to advise us of acknowledgements
    AckForm->SetAckEventMessage( Handle, WM_ACK_EVENT );

/* bypass for initial prototype
    // On form show, show the jobs dialog
    String jobFileName;
    String preferredJobFileName;

    if( PlugJobsForm->OpenJob( jobFileName, preferredJobFileName ) )
    {
    }
*/
    // Setup a demo job
    PLUG_JOB_INFO jobInfo;

    jobInfo.clientName     = "TESCO Corp";
    jobInfo.location       = "";
    jobInfo.customerRep    = "";
    jobInfo.tescoTech      = "";
    jobInfo.unitsOfMeasure = UOM_METRIC;
    jobInfo.jobStartTime   = Now();

    for( int iPlug = 0; iPlug < NBR_OF_PLUGS; iPlug++ )
        jobInfo.launchOrder[iPlug] = iPlug;

    m_currJob = new TPlugJob( jobInfo );

    RefreshCaption();

    ProjectLogForm->SetJob( m_currJob );

    // Also after loading a job we'd set (or restore) the plug states
    for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
    {
        m_pPlugControls[iPlug]->Initialize( m_currJob->PlugState[iPlug+1] );

        m_LaunchBtns[iPlug]->Caption = "Launch Plug " + IntToStr( iPlug + 1 );
    }

    // Okay to create the comm obj now
    m_devPoller = new TCommPoller( Handle, WM_POLLER_EVENT );
}


void __fastcall TMPLMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Be sure to shut down helper forms
    AckForm->Flush();
    ProjectLogForm->CloseJob();

    // Now delete objects
    if( m_devPoller != NULL )
        delete m_devPoller;

    if( m_currJob != NULL )
        delete m_currJob;
}


void __fastcall TMPLMainForm::AboutBtnClick(TObject *Sender)
{
    AboutMPLForm->ShowModal();
}


void __fastcall TMPLMainForm::ProjSettingsBtnClick(TObject *Sender)
{
    PLUG_JOB_INFO jobInfo;

    PlugJobDetailsForm->ShowJobInfo( jobInfo );
}


void __fastcall TMPLMainForm::ProjLogBtnClick(TObject *Sender)
{
    ProjectLogForm->ViewLog();
}


void __fastcall TMPLMainForm::SystemTimerTimer(TObject *Sender)
{
    // Check the launch state. If we are enabled and the enable timer
    // has timed out, revert to the ready state
    if( m_eLaunchState == eLS_Enabled )
    {
        if( HaveTimeout( m_dwEnableBtnTimeout, m_dwLaunchClickTimeout ) )
            SetLaunchState( eLS_Ready );
    }
    else if( ( m_eLaunchState == eLS_Launching ) || ( m_eLaunchState == eLS_HWWait ) )
    {
        if( m_devPoller->CanLaunch )
            SetLaunchState( eLS_Ready );
    }

    // For any launched plugs, see if we have a detect timeout. Remember that
    // launch duration is in seconds, but that the timeout is in msecs.
    for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
    {
        if( m_pPlugControls[iPlug]->State == ePCS_Launched )
        {
            if( m_pPlugControls[iPlug]->LaunchDuration * 1000 > m_dwPlugDetectTimeout )
            {
                m_pPlugControls[iPlug]->Lost();

                m_currJob->PlugState[ m_pPlugControls[iPlug]->PlugNbr ] = ePCS_Lost;

                switch( m_pPlugControls[iPlug]->PlugNbr )
                {
                    case 1:   AckForm->DisplayEvent( eAET_Plug1Lost   );   break;
                    case 2:   AckForm->DisplayEvent( eAET_Plug2Lost   );   break;
                    case 3:   AckForm->DisplayEvent( eAET_Plug3Lost   );   break;
                    case 4:   AckForm->DisplayEvent( eAET_Plug4Lost   );   break;
                    case 5:   AckForm->DisplayEvent( eAET_Plug5Lost   );   break;
                    default:  AckForm->DisplayEvent( (AckEventType)98 );   break;  // Should never happen!
                }

                // If a plug gets lost, we must manually assert the alert
                // output for the base radio
                m_devPoller->AssertAlertOutput();
            }
        }
    }

    // If we have no pending events, clear the output alarm indications
    if( !AckForm->HaveEvents() )
        m_devPoller->ClearAlertOutput();

    // Lastly, update each plug control
    for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
        m_pPlugControls[iPlug]->UpdateControl();
}


void __fastcall TMPLMainForm::EnableLaunchBtnClick(TObject *Sender)
{
    // Double-check here that the battery is good. If it isn't tell the
    // user they can't launch.
    switch( m_devPoller->MPLBatteryLevel )
    {
        case TMPLDevice::eBL_Med:
        case TMPLDevice::eBL_OK:
            break;

        default:
            MessageDlg( "The battery voltage is too low to launch a plug. You must change the battery immediately.",
                        mtError, TMsgDlgButtons() << mbOK, 0 );
            return;
    }

    SetLaunchState( eLS_Enabled );
}


void TMPLMainForm::SetLaunchState( eLaunchState eNewState )
{
    // Try to set the system to the passed state. First, if we are loading
    // a job then all buttons get disabled
    if( eNewState == eLS_Loading )
    {
        EnableLaunchBtn->Enabled = false;

        for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
        {
            m_pPlugControls[iPlug]->Initialize( ePCS_NotLoaded );

            m_LaunchBtns[iPlug]->Enabled = false;
        }

        m_eLaunchState = eLS_Loading;

        return;
    }

    // If the launch is complete, set the overall state and buttons but
    // leave the plug controls as they are. Also do this if we're waiting
    // for the hardware to complete an operation
    if( ( eNewState == eLS_Complete ) || ( eNewState == eLS_Disabled ) || ( eNewState == eLS_HWWait ) )
    {
        EnableLaunchBtn->Enabled = false;

        for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
            m_LaunchBtns[iPlug]->Enabled = false;

        m_eLaunchState = eNewState;

        return;
    }

    // If we are launching a plug, must wait for the launch to complete
    if( eNewState == eLS_Launching )
    {
        EnableLaunchBtn->Enabled = false;

        for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
            m_LaunchBtns[iPlug]->Enabled = false;

        m_eLaunchState = eLS_Launching;

        return;
    }

    // Check to see if we are currently in an actionable state
    switch( m_eLaunchState )
    {
        case eLS_Loading:
        case eLS_Ready:
        case eLS_Enabled:
        case eLS_Launching:
        case eLS_HWWait:
            // These are actionable states
            break;

        case eLS_Disabled:
        case eLS_Complete:
        default:
            // Not actionable states
            return;
    }

    // We are in an actionable state.
    if( eNewState == eLS_Ready )
    {
        // Because the user can launch / re-launch at will regardless of
        // the detected state of a plug, just re-enable launch here.
        EnableLaunchBtn->Enabled = true;

        m_eLaunchState = eLS_Ready;

        // Disable the launch buttons until the enable launch is clicked
        for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
            m_LaunchBtns[iPlug]->Enabled = false;
    }
    else if( eNewState == eLS_Enabled )
    {
        // When switching to the enabled state, enable the launch
        // buttons of any loaded plugs and start the timeout timer.
        // Note that we can only launch plugs in sequence, and that
        // sequence stops at the first loaded plug.
        int iPlug = 0;

        for( ; iPlug < NBR_MPL_PLUGS; iPlug++ )
        {
            switch( m_pPlugControls[iPlug]->State )
            {
                case ePCS_Loaded:
                case ePCS_Detected:
                case ePCS_Launched:
                case ePCS_Lost:
                    // Can launch / relaunch in these states
                    m_LaunchBtns[iPlug]->Enabled = true;
                    break;

                default:
                    // Cannot launch in these states
                    m_LaunchBtns[iPlug]->Enabled = false;
                    break;
            }

            // If this is the first loaded plug, increment our
            // index to point to the next button and then break
            // out of this loop
            if( m_pPlugControls[iPlug]->State == ePCS_Loaded )
            {
                iPlug++;
                break;
            }
        }

        // Any remaining plugs are not enabled
        for( ; iPlug < NBR_MPL_PLUGS; iPlug++ )
            m_LaunchBtns[iPlug]->Enabled = false;

        m_dwEnableBtnTimeout = GetTickCount();

        m_eLaunchState = eLS_Enabled;
    }
}


void __fastcall TMPLMainForm::LaunchPlugBtnClick(TObject *Sender)
{
    // Can only occur if the button was enabled.
    TAdvSmoothToggleButton* pBtn = dynamic_cast<TAdvSmoothToggleButton*>( Sender );

    if( pBtn != NULL )
    {
        // The button's tag is the plug index number
        if( m_devPoller->LaunchPlug( pBtn->Tag + 1 ) )
        {
            m_pPlugControls[pBtn->Tag]->Launch();

            m_currJob->PlugState[pBtn->Tag+1] = ePCS_Launched;

            // After the click, set the system state to launching
            SetLaunchState( eLS_Launching );

            // After a plug has been launched, change the caption to
            // account for re-launch ability.
            pBtn->Caption = "Re-launch Plug " + IntToStr( pBtn->Tag + 1 );
        }
    }
}


void __fastcall TMPLMainForm::OnPlugLaunchedClick( TObject* Sender )
{
    // Placeholder for code to execute when a plug animation is clicked
}


void TMPLMainForm::RefreshCaption( void )
{
    String mainCaption = "TESCO TesTORK Multi-Plug Launcher";

    if( m_devPoller != NULL )
    {
        if( m_devPoller->UsingMPLSim )
            mainCaption += " (Simulation Mode)";
    }

    if( m_currJob != NULL )
    {
        mainCaption = mainCaption + " - " + m_currJob->Client + ", " + m_currJob->Location;

        if( m_currJob->Completed )
            mainCaption = mainCaption + " (completed)";
    }

    Caption = mainCaption;
}


void TMPLMainForm::UpdateStatusScanning( void )
{
    // This method is called when the comms manager is scanning for either or
    // both the Base Radio and MPL Device.
    String BROpenModeStr;

    if( GetAutoAssignCommPorts() )
        BROpenModeStr = "Scanning for Radio";
    else
        BROpenModeStr = "Opening Radio Port";

    switch( m_devPoller->PollerState )
    {
        case TCommPoller::PS_INITIALIZING:
            SetSmoothPanelCaption( BaseRadioStatusPanel, "Initializing" );
            SetSmoothPanelCaption( MPLLinkStatusPanel,   EmptyCaption );
            BaseRadioProgBar->Visible = false;
            MPLLinkProgBar->Visible   = false;
            break;

        case TCommPoller::PS_BR_PORT_SCAN:
            SetSmoothPanelCaption( BaseRadioStatusPanel, BROpenModeStr );
            SetSmoothPanelCaption( MPLLinkStatusPanel,   EmptyCaption );
            BaseRadioProgBar->Visible = true;
            MPLLinkProgBar->Visible   = false;
            break;

        case TCommPoller::PS_MPL_PORT_SCAN:
            SetSmoothPanelCaption( BaseRadioStatusPanel, m_devPoller->BaseRadioStatus );
            SetSmoothPanelCaption( MPLLinkStatusPanel,   EmptyCaption );
            BaseRadioProgBar->Visible = false;
            MPLLinkProgBar->Visible   = true;
            break;

        case TCommPoller::PS_MPL_CHAN_SCAN:
            SetSmoothPanelCaption( BaseRadioStatusPanel, m_devPoller->BaseRadioStatus );
            SetSmoothPanelCaption( MPLLinkStatusPanel,   m_devPoller->MPLPortDesc );
            BaseRadioProgBar->Visible = false;
            MPLLinkProgBar->Visible   = true;
            break;
    }

    SetSmoothPanelCaption( BattLevelStatusPanel, EmptyCaption );
    SetSmoothPanelCaption( RPMStatusPanel,       EmptyCaption );
    SetSmoothPanelCaption( TempStatusPanel,      EmptyCaption );
    SetSmoothPanelCaption( PressStatusPanel,     EmptyCaption );
}


void TMPLMainForm::SetMainState( MainStates eNewState )
{
    switch( eNewState )
    {
        case eMS_StartUp:
        case eMS_ViewOnly:
        case eMS_DiagnosticChk:
        case eMS_Running:
        case eMS_NbrMainStates:
            break;
    }

    m_msState = eNewState;
}


void TMPLMainForm::UpdateStatusValues( void )
{
    GENERIC_MPL_RFC_PKT lastPkt;
    TDateTime           pktTime;
    DWORD               rfcCount;

    SetSmoothPanelCaption( BaseRadioStatusPanel, m_devPoller->BaseRadioStatus );
    SetSmoothPanelCaption( MPLLinkStatusPanel,   m_devPoller->MPLPortDesc );

    // If we are getting status messages, then certainly hide the base radio progress bar.
    // For the MPL, its progress bar is not visible only if the MPL is idle (which really
    // means its running but not doing a config upload/download).
    BaseRadioProgBar->Visible = false;
    MPLLinkProgBar->Visible   = m_devPoller->MPLIsIdle ? false : true;

    if( m_devPoller->GetLastStatusPkt( pktTime, rfcCount, lastPkt ) )
    {
        SetSmoothPanelCaption( RPMStatusPanel,       FloatToStrF( lastPkt.fRPM, ffFixed, 7, 0 ) );
        SetSmoothPanelCaption( TempStatusPanel,      FloatToStrF( lastPkt.fTemperature, ffFixed, 7, 1 ) );
// 1v004 - do not display pressure at yet
//        SetSmoothPanelCaption( PressStatusPanel,     FloatToStrF( lastPkt.fPressure, ffFixed, 7, 1 ) );
        SetSmoothPanelCaption( PressStatusPanel,     EmptyCaption );

        // Switch 1 is the MPL detect switch and gets cleared elsewhere. Switch 2
        // is not currently used, so if it is on command it off here
        if( lastPkt.pibSwitches[1] == eBS_On )
            m_devPoller->ClearSW2Indication();
    }
    else
    {
        SetSmoothPanelCaption( RPMStatusPanel,   "" );
        SetSmoothPanelCaption( TempStatusPanel,  "" );
        SetSmoothPanelCaption( PressStatusPanel, "" );
    }

    // Show battery info separately, as it will give averaged values (lastPkt
    // will show instantaneous value
    BATTERY_INFO battInfo;

    if( m_devPoller->GetDeviceBatteryInfo( battInfo ) )
    {
        SetSmoothPanelCaption( BattLevelStatusPanel, FloatToStrF( battInfo.avgVolts, ffFixed, 7, 2 ) );

        // Update the image - make sure the battery level is a known enum
        switch( m_devPoller->MPLBatteryLevel )
        {
            case TMPLDevice::eBL_Med:
            case TMPLDevice::eBL_OK:
            case TMPLDevice::eBL_Low:
                BattLevelStatusPanel->Fill->Picture->LoadFromResourceName( (int)HInstance, BattLevelResNames[m_devPoller->MPLBatteryLevel] );
                break;

            default:
                BattLevelStatusPanel->Fill->Picture->LoadFromResourceName( (int)HInstance, BattLevelResNames[TMPLDevice::eBL_Unknown] );
                break;
        }
    }
    else
    {
        SetSmoothPanelCaption( BattLevelStatusPanel, "" );

        BattLevelStatusPanel->Fill->Picture->LoadFromResourceName( (int)HInstance, BattLevelResNames[TMPLDevice::eBL_Unknown] );
    }
}


void TMPLMainForm::SetSmoothPanelCaption( TAdvSmoothPanel* pPanel, const String& sText )
{
    pPanel->Caption->HTMLText = "<FONT size=\"11\" face=\"Tahoma\">" + sText + "</FONT>";
}


void __fastcall TMPLMainForm::SysSettingsBtnClick(TObject *Sender)
{
    String newPassword;

    if( !TPasswordInputForm::CheckPassword( PasswordDlgCaption, GetSysAdminPassword(), newPassword ) )
        return;

    if( newPassword != "" )
        SaveSysAdminPassword( newPassword );

    // Show the dlg, record if important settings changed
    DWORD dwSettingsChanged = TSystemSettingsForm::ShowDlg( m_devPoller, m_currJob );

    // Get current job unit type and then update the comms manager
    UNITS_OF_MEASURE currUOM;

    if( m_currJob == NULL )
        currUOM = m_defaultUOM;
    else
        currUOM = m_currJob->Units;

    m_devPoller->RefreshSettings( currUOM );

    // Refresh operating params
    m_dwPlugDetectTimeout  = GetPlugDetectTimeout();
    m_dwLaunchClickTimeout = GetLaunchClickTimeout();

    // Reload the poller if the comms or misc settings were changed
    if( ( dwSettingsChanged & TSystemSettingsForm::COMMS_SETTINGS_CHANGED ) || ( dwSettingsChanged & TSystemSettingsForm::MISC_SETTINGS_CHANGED ) )
    {
        delete m_devPoller;
        m_devPoller = new TCommPoller( Handle, WM_POLLER_EVENT );

        // Check for a change in simulation mode
        if( m_devPoller->UsingMPLSim )
        {
            // In sim mode. If the job isn't, then warn the user about changes being lost
            if( m_currJob != NULL )
            {
                if( m_currJob->InSimMode == false )
                {
                    // We are now in sim mode and the currently open job is not in sim mode. Warn the user
                    MessageDlg( "You have entered Simulation Mode. Any changes made to the currently open job "
                                "will not be saved.", mtWarning, TMsgDlgButtons() << mbOK, 0 );
                }
            }
        }
        else
        {
            // Not in sim mode. If the current job is, warn the user and close the job
            if( m_currJob != NULL )
            {
                if( m_currJob->InSimMode )
                {
                    MessageDlg( "You have left Simulation Mode. The current job must be closed and any changes made to it will be lost. "
                                "Press OK to continue.", mtInformation, TMsgDlgButtons() << mbOK, 0 );

//                    OpenJob( "" );
                }
            }
        }

        EnterSimMode( m_devPoller->UsingMPLSim );
    }
}


void TMPLMainForm::EnterSimMode( bool bIsSimulating )
{
    // If we want to do any extra handling/captions, do it here
    if( m_currJob != NULL )
    {
        // Can only put a job into sim mode; can't clear that flag
        if( bIsSimulating )
            m_currJob->InSimMode = true;
    }
}


void TMPLMainForm::ProcessPlugDetected( void )
{
    // A plug has been detected. Set the state of the plug that has been
    // launched for the longest time. First, do some validation
    if( ( m_currJob == NULL ) || ( m_currJob->Completed ) )
    {
        m_devPoller->ClearPlugDetected();
        return;
    }

    // We are processing an open job. Look for the plug that has been
    // launched for the longest time.
    TPlugControl* pLaunchedPlug = NULL;

    for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
    {
        if( m_pPlugControls[iPlug]->State == ePCS_Launched )
        {
            if( pLaunchedPlug == NULL )
            {
                pLaunchedPlug = m_pPlugControls[iPlug];
            }
            else if( m_pPlugControls[iPlug]->LaunchDuration > pLaunchedPlug->LaunchDuration )
            {
                pLaunchedPlug = m_pPlugControls[iPlug];
            }
        }
    }

    if( pLaunchedPlug != NULL )
    {
        pLaunchedPlug->Detected();
        m_currJob->PlugState[pLaunchedPlug->PlugNbr] = ePCS_Detected;

        switch( pLaunchedPlug->PlugNbr )
        {
            case 1:   AckForm->DisplayEvent( eAET_Plug1Detected );   break;
            case 2:   AckForm->DisplayEvent( eAET_Plug2Detected );   break;
            case 3:   AckForm->DisplayEvent( eAET_Plug3Detected );   break;
            case 4:   AckForm->DisplayEvent( eAET_Plug4Detected );   break;
            case 5:   AckForm->DisplayEvent( eAET_Plug5Detected );   break;
            default:  AckForm->DisplayEvent( (AckEventType)99   );   break;  // Should never happen!
        }
    }
    else
    {
        // This should never happen!
        m_currJob->AddJobComment( "Plug detected, but no plug in the launched state." );
        AckForm->DisplayEvent( eAET_OrphanDetected );
    }

    m_devPoller->ClearPlugDetected();
}


void TMPLMainForm::ProcessAlarm( TCommPoller::AlarmType eAlarmType )
{
    // First log the event, then tell the user.
    if( m_currJob != NULL )
    {
        switch( eAlarmType )
        {
            case TCommPoller::eAT_LowBattery:
                m_currJob->AddJobComment( "Low battery alarm reported." );
                break;

            default:
                m_currJob->AddJobComment( "Alarm type " + IntToStr( eAlarmType ) + " reported." );
                break;
        }
    }

    // Map the alarm type to an event type, and display the event
    switch( eAlarmType )
    {
        case TCommPoller::eAT_LowBattery:
            AckForm->DisplayEvent( eAET_LowBattery, true );
            break;

        default:
            AckForm->DisplayEvent( (AckEventType)97, true );
            break;
    }

    // Assert the audible alarm indication
    m_devPoller->AssertAlertOutput();
}


//
// Windows Message Handlers
//

void __fastcall TMPLMainForm::WMCompleteJob( TMessage &Message )
{
    // Sets the current job's status to completed
    if( m_currJob == NULL )
        return;

    if( m_currJob->Completed )
        return;

    int mrResult = MessageDlg( L"You are about to mark this job as completed. You will not be able to make any more changes to the job file.\r Click Yes to proceed. \r Click Cancel to reconsider.",
                               mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    if( mrResult != mrYes )
        return;

    // The sys admin password is required to perform this task
    String newPassword;

    if( !TPasswordInputForm::CheckPassword( PasswordDlgCaption, GetSysAdminPassword(), newPassword ) )
        return;

    if( newPassword != "" )
        SaveSysAdminPassword( newPassword );

    m_currJob->Completed = true;

    // Force a change to the job status to be sure all controls are properly updated
//    MainState = MS_LOADING;
//    MainState = MS_IDLE;
    SetLaunchState( eLS_Disabled );

    RefreshCaption();
}


void __fastcall TMPLMainForm::WMReopenJob( TMessage &Message )
{
    // Sets the current job's status to in-progress (or open)
    if( m_currJob == NULL )
        return;

    if( m_currJob->Completed == false )
        return;

    int mrResult = MessageDlg( L"This job has been completed. If you re-open it, changes will be allowed to the data.\r Click Yes to proceed.\r Click Cancel to reconsider.",
                               mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    if( mrResult != mrYes )
        return;

    String newPassword;

    if( !TPasswordInputForm::CheckPassword( PasswordDlgCaption, GetSysAdminPassword(), newPassword ) )
        return;

    if( newPassword != "" )
        SaveSysAdminPassword( newPassword );

    m_currJob->Completed = false;

    // Force a change to the job status to be sure all controls are properly updated
//    MainState = MS_LOADING;
//    MainState = MS_IDLE;
    SetLaunchState( eLS_Ready );

    RefreshCaption();
}


void __fastcall TMPLMainForm::WMShowSysSettingsDlg( TMessage &Message )
{
    // This event is only called from the constructor when there is an error
    // with the comm settings.
    MessageDlg( "The device port could not be opened (" + m_devPoller->ConnectResult + "). You will have to modify the port settings before continuing.",
                mtError, TMsgDlgButtons() << mbOK, 0 );

    SysSettingsBtnClick( SysSettingsBtn );
}


void __fastcall TMPLMainForm::WMCommsEvent( TMessage &Message )
{
    // Event posted the by comms mgr when something changes
    switch( Message.WParam )
    {
        case eCPE_Initializing:
            // Event posted when comm port is opened and init process started
            UpdateStatusScanning();
            break;

        case eCPE_InitComplete:
            // Ports are open and ready to start mode operating
            if( m_currJob != NULL )
            {
                // Can launch if the job is not complete, otherwise can't launch
                if( m_currJob->Completed )
                    SetLaunchState( eLS_Disabled );
                else
                    SetLaunchState( eLS_HWWait );
            }

            // TODO - need to detect if switches are in their 'on' state on boot.
            // If so, that is a problem the user needs to know and deal with

          break;

        case eCPE_PortLost:
            // Event posted when a port which was open has been lost
            UpdateStatusScanning();
            SetLaunchState( eLS_Disabled );
            break;

        case eCPE_CommsLost:
            // Event posted when we've lost comms with either the MPL or base radio
            UpdateStatusScanning();
            SetLaunchState( eLS_Disabled );
            break;

        case eCPE_RFCRecvd:
            // RFC received, new status values will be available
            UpdateStatusValues();
            break;

        case eCPE_PlugDetected:
            // A plug-detection event has occurred
            ProcessPlugDetected();
            break;

        case eCEP_Alarm:
            ProcessAlarm( (TCommPoller::AlarmType)Message.LParam );
            break;

        default:
            break;
    }
}


void __fastcall TMPLMainForm::WMAckEvent( TMessage &Message )
{
    // Event posted by ACK form when user has acknowledged an event
    AckEventType eAckEvent = (AckEventType)( Message.WParam );

    String  msgFormat;
    String  detectedMsg = "Plug %d detection acknowledged";
    String  lostMsg     = "Plug %d lost acknowledged";
    int     iPlugNbr = 0;

    switch( eAckEvent )
    {
        case eAET_Plug1Detected:  msgFormat = detectedMsg;   iPlugNbr = 1; break;
        case eAET_Plug1Lost:      msgFormat = lostMsg;       iPlugNbr = 1; break;
        case eAET_Plug2Detected:  msgFormat = detectedMsg;   iPlugNbr = 2; break;
        case eAET_Plug2Lost:      msgFormat = lostMsg;       iPlugNbr = 2; break;
        case eAET_Plug3Detected:  msgFormat = detectedMsg;   iPlugNbr = 3; break;
        case eAET_Plug3Lost:      msgFormat = lostMsg;       iPlugNbr = 3; break;
        case eAET_Plug4Detected:  msgFormat = detectedMsg;   iPlugNbr = 4; break;
        case eAET_Plug4Lost:      msgFormat = lostMsg;       iPlugNbr = 4; break;
        case eAET_Plug5Detected:  msgFormat = detectedMsg;   iPlugNbr = 5; break;
        case eAET_Plug5Lost:      msgFormat = lostMsg;       iPlugNbr = 5; break;
        case eAET_LowBattery:     msgFormat = "Low battery alarm acknowledged";         break;
        case eAET_OrphanDetected: msgFormat = "No plug detection event acknowledged.";  break;
        default:                  msgFormat = "Unknown event " + IntToStr( eAckEvent ) + " acknowledged."; break;
    }

    // Format the message now. We always pass the plug number as a param but
    // ack or lost messages are the only ones that will actually use it.
    String logMessage;
    logMessage.printf( msgFormat.w_str(), iPlugNbr );

    m_currJob->AddJobComment( logMessage );

    // If we have a plug number, ack the event
    if( iPlugNbr > 0 )
    {
        for( int iPlug = 0; iPlug < NBR_MPL_PLUGS; iPlug++ )
        {
            if( m_pPlugControls[iPlug]->PlugNbr == iPlugNbr )
            {
                m_pPlugControls[iPlug]->Acknowledge();
                break;
            }
        }
    }

    // If we have an alarm event, ack it as well
    if( eAckEvent == eAET_LowBattery )
        m_devPoller->AckAlarm( TCommPoller::eAT_LowBattery );
}


/*
    PostMessage( Handle, WM_SHOW_SYS_SETTINGS_DLG, 0, 0 );
    // Check for lost comms
    if( MainState == MS_LOST_BASERADIO )
    {
        // No base radio, check if this has changed
        if( m_devPoller->GetLastError() == TCommPoller::PE_SUCCESS )
            MainState = MS_IDLE;
    }
    else if( MainState == MS_LOST_TESTORK )
    {
        // No TesTORK, check if we have a base radio, or if the TesTORK is back
        if( m_devPoller->GetLastError() == TCommPoller::PE_SCANNING )
        {
            // Radio was also lost
            m_devPoller->ResetBaseRadioUp();

            MainState = MS_LOST_BASERADIO;
        }
        else if( m_devPoller->DeviceIsRecving )
        {
            // TesTORK is communicating again
            MainState = MS_IDLE;
        }
    }
    else if( MainState != MS_LOADING )
    {
        if( ( m_devPoller->BaseRadioInitUp ) && ( m_devPoller->GetLastError() == TCommPoller::PE_SCANNING ) )
        {
            // If the radio was up and we're now scanning, we lost the radio
            SysTimer->Enabled  = false;
            PollTimer->Enabled = false;

            // Warn the user based on the state of AutoAssign ports
            if( GetCommOptions() )
                MessageDlg( "Base Radio was lost. The system is auto-scanning for a new device.", mtError, TMsgDlgButtons() << mbOK, 0 );
            else
                MessageDlg( "Base Radio was lost. The system will continue to scan on the assigned ports. To modify, open the System Settings.", mtError, TMsgDlgButtons() << mbOK, 0 );

            MainState = MS_LOST_BASERADIO;

            m_devPoller->ResetBaseRadioUp();
            m_devPoller->ResetTesTORKUp();

            SysTimer->Enabled  = true;
            PollTimer->Enabled = true;
        }
        else if( ( m_devPoller->TesTORKInitUp ) && ( !m_devPoller->DeviceIsRecving ) )
        {
            // If the TesTORk was up, and we're no longer receiving packets, we lost the TesTORK
            SysTimer->Enabled  = false;
            PollTimer->Enabled = false;

            MessageDlg( L"Communications with the TesTORK have been lost. ", mtError, TMsgDlgButtons() << mbOK, 0 );

            MainState = MS_LOST_TESTORK;

            m_devPoller->ResetTesTORKUp();

            SysTimer->Enabled  = true;
            PollTimer->Enabled = true;
        }
    }
*/

