#include <vcl.h>
#pragma hdrstop

#include "TLinkTiltMain.h"
#include "TAboutLinkTiltDlg.h"
#include "TProjectLogDlg.h"
#include "TLinkTiltSettingsDlg.h"
#include "PasswordInputDlg.h"
#include "SerialPortUtils.h"
#include "IPUtils.h"
#include "ApplUtils.h"
#include "TescoShared.h"

#pragma package(smart_init)
#pragma link "AdvSmoothExpanderButtonPanel"
#pragma link "AdvSmoothExpanderPanel"
#pragma link "AdvSmoothPanel"
#pragma link "AdvSmoothToggleButton"
#pragma link "AdvSmoothProgressBar"
#pragma link "GDIPPictureContainer"
#pragma resource "*.dfm"


TLinkTiltMainForm *LinkTiltMainForm;


static const TColor clPlugGreen = (TColor)0x0FAF03;

static const String PasswordDlgCaption( "Administrator Password Required" );
static const String BadPWEnteredMsg( "You have entered the incorrect password." );
static const String EmptyCaption( "---" );
static const String InitingString( "Initializing" );

// Battery level resource names.
const String BattLevelResNames[TTiltMgrDevice::eNbrBattLevels] = {
    "BattLevelImg_Unknown",
    "BattLevelImg_Low",
    "BattLevelImg_Medium",
    "BattLevelImg_Full",
};


__fastcall TLinkTiltMainForm::TLinkTiltMainForm(TComponent* Owner) : TForm(Owner)
{
    // Set border color of buttons (can't do this special colour at design time)
    SysLogBtn->BorderColor      = clTESCOBlue;
    SysLogBtn->BorderInnerColor = clTESCOBlue;

    SysSettingsBtn->BorderColor      = clTESCOBlue;
    SysSettingsBtn->BorderInnerColor = clTESCOBlue;

    AboutBtn->BorderColor      = clTESCOBlue;
    AboutBtn->BorderInnerColor = clTESCOBlue;

    // Set marque colors for progress bars
    BaseRadioProgBar->MarqueeColor = clPlugGreen;
    TiltLinkProgBar->MarqueeColor  = clPlugGreen;
    PLCLinkProgBar->MarqueeColor   = clPlugGreen;

    // Init arm angle. Angle is reported as tenths of a degree from plumb.
    // That is, a value of zero means the arm is handing straight down.
    // Values should never exceed 1800 (straight up). Values of 4000 and
    // greater are deemed to be alarm codes.
    #define ANGLE_ALARM_NOT_CONNECTED   4000
    #define ANGLE_ALARM_NOT_RECEIVING   4001
    #define ANGLE_ALARM_ON_Z_AXIS       4002
    m_wArmAngle = ANGLE_ALARM_NOT_CONNECTED;

    // Set the process priority
    DWORD currPriority = GetPriorityClass( GetCurrentProcess() );
    DWORD reqdPriority = GetProcessPriority();

    if( currPriority != reqdPriority )
    {
        if( !SetPriorityClass( GetCurrentProcess(), reqdPriority ) )
        {
            DWORD dwError = GetLastError();

            MessageDlg( "Could not set process priority, error 0x" + IntToHex( (int)dwError, 8 ), mtError, TMsgDlgButtons() << mbOK, 0 );
        }
    }

    // Init Winsock now
    InitWinsock();

    // We can construct the MODBUS device right away
    m_modbusDev = new TModbusDevice();

    MODBUS_SETTINGS modbusSettings;
    GetMODBUSSettings( modbusSettings );

    ModbusCommSettings commsSettings;
    commsSettings.eCommType   = modbusSettings.eCommType;
    commsSettings.iCommPort   = modbusSettings.iCommPort;
    commsSettings.dwSpeed     = modbusSettings.dwSpeed;
    commsSettings.wIPPort     = modbusSettings.wIPPort;
    commsSettings.bySlaveAddr = modbusSettings.byDevAddr;
    commsSettings.bOffsetRegs = modbusSettings.bOffsetRegs;

    String deviceDescFile = GetMODBUSDevDescFile();

    m_modbusDev->InitializeDevice( commsSettings, deviceDescFile );

    m_modbusInitComplete = false;

    SetSmoothPanelCaption( PLCLinkStatusPanel, InitingString );
    PLCLinkProgBar->Visible = true;

    // Read any registry settings required here
    GetAngleFactors( m_angleFactors );
}


void __fastcall TLinkTiltMainForm::FormShow(TObject *Sender)
{
    // Okay to create the comm obj now
    m_devPoller = new TCommPoller( Handle, WM_POLLER_EVENT );

    // Okay to start the system timer now
    SystemTimer->Enabled = true;
}


void __fastcall TLinkTiltMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Delete objects
    if( m_devPoller != NULL )
        delete m_devPoller;

    if( m_modbusDev != NULL )
        delete m_modbusDev;

    // Shut down Winsock
    ShutdownWinsock();
}


void __fastcall TLinkTiltMainForm::AboutBtnClick(TObject *Sender)
{
    AboutMPLForm->ShowModal();
}


void __fastcall TLinkTiltMainForm::SysLogBtnClick(TObject *Sender)
{
    ProjectLogForm->ViewLog();
}


void __fastcall TLinkTiltMainForm::SystemTimerTimer(TObject *Sender)
{
    UpdatePLCInterface();
}


void TLinkTiltMainForm::UpdateStatusScanning( void )
{
    // This method is called when the comms manager is scanning for either or
    // both the Base Radio and MPL Device.
    String BROpenModeStr;

    if( GetAutoAssignCommPorts() )
        BROpenModeStr = "Scanning for Radio";
    else
        BROpenModeStr = "Opening Radio Port";

    switch( m_devPoller->PollerState )
    {
        case TCommPoller::PS_INITIALIZING:
            SetSmoothPanelCaption( BaseRadioStatusPanel, InitingString );
            SetSmoothPanelCaption( TiltLinkStatusPanel,  InitingString );
            BaseRadioProgBar->Visible = false;
            TiltLinkProgBar->Visible  = false;
            break;

        case TCommPoller::PS_BR_PORT_SCAN:
            SetSmoothPanelCaption( BaseRadioStatusPanel, BROpenModeStr );
            SetSmoothPanelCaption( TiltLinkStatusPanel,  EmptyCaption );
            BaseRadioProgBar->Visible = true;
            TiltLinkProgBar->Visible  = false;
            break;

        case TCommPoller::PS_TM_PORT_SCAN:
            SetSmoothPanelCaption( BaseRadioStatusPanel, m_devPoller->BaseRadioStatus );
            SetSmoothPanelCaption( TiltLinkStatusPanel,  EmptyCaption );
            BaseRadioProgBar->Visible = false;
            TiltLinkProgBar->Visible  = true;
            break;

        case TCommPoller::PS_TM_CHAN_SCAN:
            SetSmoothPanelCaption( BaseRadioStatusPanel, m_devPoller->BaseRadioStatus );
            SetSmoothPanelCaption( TiltLinkStatusPanel,  m_devPoller->TMPortDesc );
            BaseRadioProgBar->Visible = false;
            TiltLinkProgBar->Visible  = true;
            break;

        case TCommPoller::PS_COMMS_FAILED:
            SetSmoothPanelCaption( BaseRadioStatusPanel, "Configuration Error" );
            SetSmoothPanelCaption( TiltLinkStatusPanel,  "Configuration Error" );
            BaseRadioProgBar->Visible = false;
            TiltLinkProgBar->Visible  = false;
    }

    SetSmoothPanelCaption( BattLevelStatusPanel, EmptyCaption );
    SetSmoothPanelCaption( TempStatusPanel,      EmptyCaption );
    SetSmoothPanelCaption( TiltAnglePanel,       EmptyCaption );
}


void TLinkTiltMainForm::UpdateStatusValues( void )
{
    SetSmoothPanelCaption( BaseRadioStatusPanel, m_devPoller->BaseRadioStatus );
    SetSmoothPanelCaption( TiltLinkStatusPanel,  m_devPoller->TMPortDesc );

    // If we are getting status messages, then certainly hide the base radio progress bar.
    // For the MPL, its progress bar is not visible only if the MPL is idle (which really
    // means its running but not doing a config upload/download).
    BaseRadioProgBar->Visible = false;
    TiltLinkProgBar->Visible  = m_devPoller->TMIsIdle ? false : true;

    if( m_devPoller->TMIsRecving )
    {
        SetSmoothPanelCaption( TempStatusPanel, FloatToStrF( m_devPoller->TMTemperature, ffFixed, 7, 1 ) );
    }
    else
    {
        SetSmoothPanelCaption( TempStatusPanel,  "" );
    }

    // Show battery info separately, as it will give averaged values (lastPkt
    // will show instantaneous value
    BATTERY_INFO battInfo;

    if( m_devPoller->GetDeviceBatteryInfo( battInfo ) )
    {
        SetSmoothPanelCaption( BattLevelStatusPanel, FloatToStrF( battInfo.avgVolts, ffFixed, 7, 2 ) );

        // Update the image - make sure the battery level is a known enum
        switch( m_devPoller->TMBatteryLevel )
        {
            case TTiltMgrDevice::eBL_Med:
            case TTiltMgrDevice::eBL_OK:
            case TTiltMgrDevice::eBL_Low:
                BattLevelStatusPanel->Fill->Picture->LoadFromResourceName( (int)HInstance, BattLevelResNames[m_devPoller->TMBatteryLevel] );
                break;

            default:
                BattLevelStatusPanel->Fill->Picture->LoadFromResourceName( (int)HInstance, BattLevelResNames[TTiltMgrDevice::eBL_Unknown] );
                break;
        }
    }
    else
    {
        SetSmoothPanelCaption( BattLevelStatusPanel, "" );

        BattLevelStatusPanel->Fill->Picture->LoadFromResourceName( (int)HInstance, BattLevelResNames[TTiltMgrDevice::eBL_Unknown] );
    }

    // Calculate the arm reading. For accuracy, subract any z accel from the expected
    // total acceleration due to gravity.
    TTiltMgrDevice::DEVICE_READING devReading;

    if( m_devPoller->GetLastAvgReading( devReading ) )
    {
        float fDegrees = NaN;

        // Get the total vertical plane acceleration first. Note that on first
        // power up the accelerometer values might read as zero.
        float fVerticalAccel = sqrt( devReading.accelX * devReading.accelX + devReading.accelY * devReading.accelY );

        if( fVerticalAccel == 0.0 )
        {
            m_wArmAngle = ANGLE_ALARM_NOT_RECEIVING;
        }
        else if( fVerticalAccel >= m_angleFactors.fMinAccelAmpl )
        {
            // Rotate the X/Y readings to get asin() == 0 at 90 degrees
            float fRotation = (float)m_angleFactors.iRotationOffset / 360.0 * 2.0 * M_PI;

            float fXRot = devReading.accelX * cos( fRotation ) - devReading.accelY * sin( fRotation );
            float fYRot = devReading.accelY * cos( fRotation ) + devReading.accelX * sin( fRotation );

            // Apply the 'calibration' factors so that we get an opposite
            // arm length that is positive when above the horizon and
            // negative when below it. Note that the X and Y factors
            // must only be 0, +1, or -1, and one factor must be zero.
            float fOppositeArm = 0.0;

            if( m_angleFactors.iXAccelComponent > 0 )
                fOppositeArm = fXRot;
            else if( m_angleFactors.iXAccelComponent < 0 )
                fOppositeArm = -fXRot;
            else if( m_angleFactors.iYAccelComponent > 0 )
                fOppositeArm = fYRot;
            else if( m_angleFactors.iYAccelComponent < 0 )
                fOppositeArm = -fYRot;
            else
                fOppositeArm = fXRot;

            float fOppOverAdj = fOppositeArm / fVerticalAccel;

            float fRadians;

            if( fOppOverAdj >= 1.0 )
                fRadians = M_PI_2;
            else if( fOppOverAdj <= -1.0 )
                fRadians = -M_PI_2;
            else
                fRadians = asin( fOppOverAdj );

            // Convert to degrees. A negative value will 'loop around'
            // to the positive side.
            fDegrees = fRadians / ( 2.0 * M_PI ) * 360.0;

            while( fDegrees < 0.0 )
                fDegrees += 360.0;

            m_wArmAngle = ( fDegrees * 10.0 + 0.5 );
        }
        else
        {
            m_wArmAngle = ANGLE_ALARM_ON_Z_AXIS;
        }

        String sData =     "X " + FloatToStrF( devReading.accelX, ffFixed, 7, 2 ) +
                       "<BR>Y " + FloatToStrF( devReading.accelY, ffFixed, 7, 2 ) +
                       "<BR>Z " + FloatToStrF( devReading.accelZ, ffFixed, 7, 2 ) +
                       "<BR>Angle " + FloatToStrF( fDegrees, ffFixed, 7, 2 );

        // Debug: also show RSSI for now
        time_t tLastBRPkt;
        BASE_STATUS_STATUS_PKT lastBRStatus;

        if( m_devPoller->GetLastRadioStatusPkt( tLastBRPkt, lastBRStatus ) )
        {
            sData = sData + "<BR>RSSI " + UIntToStr( (UINT)lastBRStatus.radioInfo[0].rssi ) + ", " + UIntToStr( (UINT)lastBRStatus.radioInfo[1].rssi );
        }
        // End Debug

        SetSmoothPanelCaption( TiltAnglePanel, sData );
    }
    else
    {
        m_wArmAngle = ANGLE_ALARM_NOT_RECEIVING;

        SetSmoothPanelCaption( TiltAnglePanel, "" );
    }
}


void TLinkTiltMainForm::SetSmoothPanelCaption( TAdvSmoothPanel* pPanel, const String& sText )
{
    // The Tilt angle panel has a different format
    if( pPanel == TiltAnglePanel )
        pPanel->Caption->HTMLText = "<FONT size=\"32\" face=\"Tahoma\">" + sText + "</FONT>";
    else
        pPanel->Caption->HTMLText = "<FONT size=\"11\" face=\"Tahoma\">" + sText + "</FONT>";
}


void __fastcall TLinkTiltMainForm::SysSettingsBtnClick(TObject *Sender)
{
    String newPassword;

    if( !TPasswordInputForm::CheckPassword( PasswordDlgCaption, GetSysAdminPassword(), newPassword ) )
        return;

    if( newPassword != "" )
        SaveSysAdminPassword( newPassword );

    // Show the dlg, record if important settings changed
    DWORD dwSettingsChanged = TLinkTiltSettingsDlg::ShowDlg( m_devPoller, m_modbusDev );

    // Need UOM to refresh settings, even though UOM is not used in this application.
    UNITS_OF_MEASURE currUOM = UOM_IMPERIAL;

    m_devPoller->RefreshSettings( currUOM );

    // Reload the poller if the comms or misc settings were changed
    if( ( dwSettingsChanged & TLinkTiltSettingsDlg::COMMS_SETTINGS_CHANGED ) || ( dwSettingsChanged & TLinkTiltSettingsDlg::MISC_SETTINGS_CHANGED ) )
    {
        delete m_devPoller;
        m_devPoller = new TCommPoller( Handle, WM_POLLER_EVENT );
    }

    if( dwSettingsChanged & TLinkTiltSettingsDlg::CAL_SETTINGS_CHANGED )
        GetAngleFactors( m_angleFactors );

    if( dwSettingsChanged & TLinkTiltSettingsDlg::PLC_SETTINGS_CHANGED )
        MessageDlg( "You have made changes to the PLC interface. These settings will take effect the next time you start this program.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
}


void TLinkTiltMainForm::UpdatePLCInterface( void )
{
    // Update the PLC interface. In this release, we only support
    // a MODBUS interface
    if( m_modbusDev == NULL )
        return;

    m_modbusDev->Update();

    // Typedef of all possible values we can report over MODBUS. Note that these
    // are all read-only values.
    typedef enum {
    //  Enum                   Value     Description                     Register Type   Notes
        eLTDT_Fixed,        /* 0         Fixed value                     User defined    Fixed value, uses default value in reg def file, never changes  */
        eLTDT_SWVersion,    /* 1         LT Manager software version     DWORD (7)       4 bytes, each byte being one digit of the version Major.Minor.Release.Build */
        eLTDT_Radio1Port,   /* 2         Base Radio radio 1 serial port  WORD (4)        TBD */
        eLTDT_Radio2Port,   /* 3         Base Radio radio 2 serial port  WORD (4)        TBD */
        eLTDT_BRPort,       /* 4         Base Radio control serial port  WORD (4)        TBD */
        eLTDT_MgmtPort,     /* 5         Base Radio mgmt serial port     WORD (4)        TBD */
        eLTDT_Radio1Status, /* 6         Radio channel 1 status          WORD (4)        Low byte: channel number, high byte = RSSI */
        eLTDT_Radio2Status, /* 7         Radio channel 2 status          WORD (4)        Low byte: channel number, high byte = RSSI */
        eLTDT_BRState,      /* 8         Base radio state                WORD (4)        Low byte: D0: connected, D1: receiving, D2-D7: RFU, high byte = output switch status */
        eLTDT_BRUpdateRate, /* 9         Base radio status update rate   WORD (4) */
        eLTDT_BRFWVer,      /* 10        Base radio firmware version     DWORD (7)       4 bytes, each byte being one digit of the version Major.Minor.Release.Build */
        eLTDT_BRtLastPkt,   /* 11        Time of last base radio status  DWORD (7)       "time_t" time value as returned by the C library time() function */
        eLTDT_BRPktCount,   /* 12        Number of status pkts recv'd    DWORD (7)       Count of status packets received since the software powered up, 0xFFFFFFFF if not connected */
        eLTDT_BRResetCode,  /* 13        Base radio state reset code     WORD (4)        Low byte: last reset type, high byte = RFU */
        eLTDT_LTtLastPkt,   /* 14        Link Tilt last status received  DWORD (7)       "time_t" time value as returned by the C library time() function */
        eLTDT_LTBattLevel,  /* 15        Link Tilt battery level         WORD (4)        Battery voltage, in mV. 0xFFFF if not connected */
        eLTDT_LTTemp,       /* 16        Link Tilt temperature           Int16 (3)       Temperature, in 10th of a degree C. 0xFFFF if not connected */
        eLTDT_LTState,      /* 17        Link Tilt state                 WORD (4)        Low byte: D0: connected, D1: receiving, D2-D15: RFU */
        eLTDT_LTDevID1,     /* 18        Link Tilt device ID 1           WORD (4)        First 2 bytes of device ID */
        eLTDT_LTDevID2,     /* 19        Link Tilt device ID 2           WORD (4)        Mid 2 bytes of device ID */
        eLTDT_LTDevID3,     /* 20        Link Tilt device ID 3           WORD (4)        Last 2 bytes of device ID */
        eLTDT_LTCompassX,   /* 21        Link Tilt compass X             Float32 (9)     Averaged value of compass X in last RFC */
        eLTDT_LTCompassY,   /* 22        Link Tilt compass Y             Float32 (9)     Averaged value of compass Y in last RFC */
        eLTDT_LTCompassZ,   /* 23        Link Tilt compass Z             Float32 (9)     Averaged value of compass Z in last RFC */
        eLTDT_LTAccelX,     /* 24        Link Tilt accel X               Float32 (9)     Averaged value of accel X in last RFC */
        eLTDT_LTAccelY,     /* 25        Link Tilt accel Y               Float32 (9)     Averaged value of accel Y in last RFC */
        eLTDT_LTAccelZ,     /* 26        Link Tilt accel Z               Float32 (9)     Averaged value of accel Z in last RFC */
        eLTDT_LTFWVer,      /* 27        Link Tilt firmware version      DWORD (7)       4 bytes, each byte being one digit of the version Major.Minor.Release.Build, 0xFFFF if not connected */
        eLTDT_LTSerNbr,     /* 28        Link Tilt serial number         WORD (4)        Value as stored in EEPROM manufacturing area, 0xFFFF if not connected */
        eLTDT_LTMfgInfo1,   /* 29        Link Tilt manufacture info 1    DWORD (7)       First 4 bytes of value stored in EEPROM manufacturing area, 0xFFFF if not connected */
        eLTDT_LTMfgInfo2,   /* 30        Link Tilt manufacture info 2    DWORD (7)       Last 4 bytes of value stored in EEPROM manufacturing area, 0xFFFF if not connected */
        eLTDT_LTMfgDate,    /* 31        Link Tilt Year / Mth of mfg     WORD (4)        Value as stored in EEPROM manufacturing area, low byte = month, high byte = year, 0xFFFF if not connected */
        eLTDT_LTCfgStraps,  /* 32        Link Tilt config straps         WORD (4)        Low byte = value as returned by TEC, high byte = 0; Will be 0xFFFF if not connected */
        eLTDT_LTArmAngle,   /* 33        Link Tilt arm angle             WORD (4)        Calculate link arm angle, in 10th of degrees. Valid range = 0 to 1799. Values 2000 and greater are error codes */
        eLTDT_SensorType,   /* 34        Link Tilt sensor type           WORD (4)        Always reports a value of 2 */
    } eLinkTiltDataType;

    // If we've not finished initialization, check the device status
    if( !m_modbusInitComplete )
    {
        // If the device is no longer in the init state, init has completed
        if( m_modbusDev->State != TModbusDevice::eMS_Initializing )
        {
            PLCLinkProgBar->Visible = false;

            m_modbusInitComplete = true;

            switch( m_modbusDev->State )
            {
                case TModbusDevice::eMS_Initializing:
                    // Could not be in this state here
                    break;

                case TModbusDevice::eMS_CommConnectErr:
                    SetSmoothPanelCaption( PLCLinkStatusPanel, "Comm Error" );
                    break;

                case TModbusDevice::eMS_DevDescError:
                    SetSmoothPanelCaption( PLCLinkStatusPanel, "Dev Desc Error" );
                    break;

                default:
                    // Assume we're connected and listening
                    SetSmoothPanelCaption( PLCLinkStatusPanel, "Listening" );
                    break;
            }

            // At this point, we can set register values that never change
            REG_DATA_UNION regData;

            regData.asDWORD = GetApplVer();
            m_modbusDev->SetRegisterValueByEnum( eLTDT_SWVersion, regData );

            regData.asWORD = 2;  // Fixed value as per spec
            m_modbusDev->SetRegisterValueByEnum( eLTDT_SensorType, regData );
        }
    }
    else
    {
        // Init has completed. Update connected state here
        if( m_modbusDev->State == TModbusDevice::eMS_Listening )
            SetSmoothPanelCaption( PLCLinkStatusPanel, "Listening" );
        else if( m_modbusDev->State == TModbusDevice::eMS_Ready )
            SetSmoothPanelCaption( PLCLinkStatusPanel, "Connected" );
    }

    // No more work to do if the device is not ready
    if( ( m_modbusDev->State != TModbusDevice::eMS_Listening ) && ( m_modbusDev->State != TModbusDevice::eMS_Ready ) )
        return;

    // Refresh MODBUS registers now. Most of the data values come out of
    // the RFC or status packets, so grab those now
    bool bBRIsConnected = m_devPoller->BaseRadioIsConnected;
    bool bBRIsRecving   = m_devPoller->BaseRadioIsRecving;

    BASE_STATUS_STATUS_PKT lastBRStatus;
    time_t                 tlastBRTime;

    bool bHaveBRStatus = m_devPoller->GetLastRadioStatusPkt( tlastBRTime, lastBRStatus );

    bool bLTIsConnected = m_devPoller->TMIsConnected;
    bool bLTIsRecving   = m_devPoller->TMIsRecving;

    GENERIC_RFC_PKT lastLTRFC;
    time_t          tLastRFCTime;

    bool bHaveLTRFC = m_devPoller->GetLastStatusPkt( tLastRFCTime, lastLTRFC );

    // Update each defined register now
    for( int iReg = 0; iReg < m_modbusDev->RegisterCount; iReg++ )
    {
        REG_DATA_UNION regData;

        // Always clear the data union first
        regData.asDWORD = 0;

        switch( m_modbusDev->RegisterEnum[iReg] )
        {
            case eLTDT_Fixed:
                 // Fixed value - never changes, always stays at default value given in reg def file
                 break;

            case eLTDT_SWVersion:
                // Handled once the MODBUS interface is initialized
                break;

            case eLTDT_Radio1Port:
            case eLTDT_Radio2Port:
            case eLTDT_BRPort:
            case eLTDT_MgmtPort:
                // These are all TBD values
                regData.asDWORD = 0;
                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_Radio1Status:

                // Low byte: channel number, high byte = RSSI
                if( bBRIsRecving && bHaveBRStatus )
                {
                    regData.byData[0] = lastBRStatus.radioInfo[0].chanNbr;
                    regData.byData[1] = lastBRStatus.radioInfo[0].rssi;
                }
                else
                {
                    regData.asWORD = 0xFFFF;
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_Radio2Status:

                // Low byte: channel number, high byte = RSSI
                if( bBRIsRecving && bHaveBRStatus )
                {
                    regData.byData[0] = lastBRStatus.radioInfo[1].chanNbr;
                    regData.byData[1] = lastBRStatus.radioInfo[1].rssi;
                }
                else
                {
                    regData.asWORD = 0xFFFF;
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_BRState:

                // Low byte: D0: connected, D1: receiving, D2-D7: RFU, high byte = output switch status
                regData.byData[0] |= bBRIsConnected ? 0x01 : 0x00;
                regData.byData[0] |= bBRIsRecving   ? 0x02 : 0x00;

                regData.byData[1] = lastBRStatus.outputStatus;

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_BRUpdateRate:

                if( bBRIsRecving && bHaveBRStatus )
                    regData.asWORD = lastBRStatus.statusUpdateRate;
                else
                    regData.asWORD = 0xFFFF;

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_BRFWVer:

                if( bBRIsRecving && bHaveBRStatus )
                {
                    regData.byData[0] = lastBRStatus.fwVer[0];
                    regData.byData[1] = lastBRStatus.fwVer[1];
                    regData.byData[2] = lastBRStatus.fwVer[2];
                    regData.byData[3] = lastBRStatus.fwVer[3];
                }
                else
                {
                    regData.asDWORD = 0xFFFFFFFF;
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_BRtLastPkt:

                if( bBRIsRecving && bHaveBRStatus )
                    regData.asDWORD = tlastBRTime;
                else
                    regData.asDWORD = 0xFFFFFFFF;

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_BRPktCount:

                if( bBRIsRecving && bHaveBRStatus )
                    regData.asDWORD = m_devPoller->BaseRadioStatusPktCount;
                else
                    regData.asDWORD = 0xFFFFFFFF;

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_BRResetCode:
                // Low byte: last reset type, high byte = RFU */
                if( bBRIsRecving && bHaveBRStatus )
                {
                    regData.byData[0] = lastBRStatus.lastResetType;
                    regData.byData[1] = 0;
                }
                else
                {
                    regData.asWORD = 0xFFFF;
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTtLastPkt:

                if( bLTIsConnected && bHaveLTRFC )
                    regData.asDWORD = tLastRFCTime;
                else
                    regData.asDWORD = 0xFFFFFFFF;

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTBattLevel:

                // Battery level is sent as a percentage where 100% represents 4 volts
                // and 0% represents 3v or less.
                if( bLTIsConnected && bHaveLTRFC )
                {
                    if( lastLTRFC.fBattVoltage >= 4.0 )
                        regData.asWORD = 100;
                    else if( lastLTRFC.fBattVoltage <= 3.0 )
                        regData.asWORD = 0;
                    else
                        regData.asWORD = (WORD)( ( lastLTRFC.fBattVoltage - 3.0 ) * 100.0 );
                }
                else
                {
                    // Report zero on error, as per requirement from Bruno Gregur
                    regData.asWORD = 0;
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTTemp:

                // Temperature is sent in 10ths of a degree C
                if( bLTIsConnected && bHaveLTRFC )
                    regData.asWORD = (WORD)( lastLTRFC.fTemperature * 10.0 );
                else
                    regData.asWORD = 0x7FFF;

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTState:

                // Low byte: D0: connected, D1: receiving, D2-D15: RFU
                regData.byData[0] |= bLTIsConnected ? 0x01 : 0x00;
                regData.byData[0] |= bLTIsRecving   ? 0x02 : 0x00;

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTDevID1:
            case eLTDT_LTDevID2:
            case eLTDT_LTDevID3:

                if( bLTIsConnected && bHaveLTRFC )
                {
                    if( m_modbusDev->RegisterEnum[iReg] == eLTDT_LTDevID1 )
                    {
                        regData.byData[0] = lastLTRFC.deviceID[0];
                        regData.byData[1] = lastLTRFC.deviceID[1];
                    }
                    else if( m_modbusDev->RegisterEnum[iReg] == eLTDT_LTDevID2 )
                    {
                        regData.byData[0] = lastLTRFC.deviceID[2];
                        regData.byData[1] = lastLTRFC.deviceID[3];
                    }
                    else
                    {
                        regData.byData[0] = lastLTRFC.deviceID[4];
                        regData.byData[1] = lastLTRFC.deviceID[5];
                    }
                }
                else
                {
                    regData.asWORD = 0xFFFF;
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTCompassX:
            case eLTDT_LTCompassY:
            case eLTDT_LTCompassZ:

                // Init result now so that we don't have a bunch of else conditions to deal with
                regData.asFloat = 0.0;

                if( bLTIsConnected && bHaveLTRFC )
                {
                    TTiltMgrDevice::DEVICE_READING lastReading;

                    if( m_devPoller->GetLastAvgReading( lastReading ) )
                    {
                        if( m_modbusDev->RegisterEnum[iReg] == eLTDT_LTCompassX )
                            regData.asFloat = lastReading.compassX;
                        else if( m_modbusDev->RegisterEnum[iReg] == eLTDT_LTCompassY )
                            regData.asFloat = lastReading.compassY;
                        else
                            regData.asFloat = lastReading.compassZ;
                    }
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTAccelX:
            case eLTDT_LTAccelY:
            case eLTDT_LTAccelZ:

                // Init result now so that we don't have a bunch of else conditions to deal with
                regData.asFloat = 0.0;

                if( bLTIsConnected && bHaveLTRFC )
                {
                    TTiltMgrDevice::DEVICE_READING lastReading;

                    if( m_devPoller->GetLastAvgReading( lastReading ) )
                    {
                        if( m_modbusDev->RegisterEnum[iReg] == eLTDT_LTAccelX )
                            regData.asFloat = lastReading.accelX;
                        else if( m_modbusDev->RegisterEnum[iReg] == eLTDT_LTAccelY )
                            regData.asFloat = lastReading.accelY;
                        else
                            regData.asFloat = lastReading.accelZ;
                    }
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTFWVer:

                // Init result now so that we don't have a bunch of else conditions to deal with
                regData.asDWORD = 0xFFFFFFFF;

                if( bLTIsConnected && bHaveLTRFC )
                    m_devPoller->GetDeviceFWRev( regData.byData[0], regData.byData[1], regData.byData[2], regData.byData[3] );

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTSerNbr:

                // Init result now so that we don't have a bunch of else conditions to deal with
                regData.asWORD = 0xFFFF;

                if( bLTIsConnected && bHaveLTRFC )
                {
                    WTTTS_MFG_DATA_STRUCT mfgInfo;
                    m_devPoller->GetDeviceMfgInfo( mfgInfo );

                    regData.asWORD = mfgInfo.devSN;
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTMfgInfo1:
            case eLTDT_LTMfgInfo2:

                // Init result now so that we don't have a bunch of else conditions to deal with
                regData.asDWORD = 0xFFFFFFFF;

                if( bLTIsConnected && bHaveLTRFC )
                {
                    WTTTS_MFG_DATA_STRUCT mfgInfo;

                    if( m_devPoller->GetDeviceMfgInfo( mfgInfo ) )
                    {
                        if( m_modbusDev->RegisterEnum[iReg] == eLTDT_LTMfgInfo1 )
                        {
                            regData.byData[0] = mfgInfo.szMfgInfo[0];
                            regData.byData[1] = mfgInfo.szMfgInfo[1];
                            regData.byData[2] = mfgInfo.szMfgInfo[2];
                            regData.byData[3] = mfgInfo.szMfgInfo[3];
                        }
                        else
                        {
                            regData.byData[0] = mfgInfo.szMfgInfo[4];
                            regData.byData[1] = mfgInfo.szMfgInfo[5];
                            regData.byData[2] = mfgInfo.szMfgInfo[6];
                            regData.byData[3] = mfgInfo.szMfgInfo[7];
                        }
                    }
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTMfgDate:

                // Init result now so that we don't have a bunch of else conditions to deal with
                regData.asWORD = 0xFFFF;

                // Technically, this data does not come from an RFC. Should improve the
                // TTMgrDevice so that it reports true only when having received this info.
                if( bLTIsConnected && bHaveLTRFC )
                {
                    WTTTS_MFG_DATA_STRUCT mfgInfo;

                    if( m_devPoller->GetDeviceMfgInfo( mfgInfo ) )
                    {
                        if( m_modbusDev->RegisterEnum[iReg] == eLTDT_LTMfgInfo1 )
                        {
                            regData.byData[0] = mfgInfo.szMfgInfo[0];
                            regData.byData[1] = mfgInfo.szMfgInfo[1];
                            regData.byData[2] = mfgInfo.szMfgInfo[2];
                            regData.byData[3] = mfgInfo.szMfgInfo[3];
                        }
                        else
                        {
                            regData.byData[0] = mfgInfo.szMfgInfo[4];
                            regData.byData[1] = mfgInfo.szMfgInfo[5];
                            regData.byData[2] = mfgInfo.szMfgInfo[6];
                            regData.byData[3] = mfgInfo.szMfgInfo[7];
                        }
                    }
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTCfgStraps:

                // Init result now so that we don't have a bunch of else conditions to deal with
                regData.asWORD = 0xFFFF;

                // Technically, this data does not come from an RFC. Should improve the
                // TTMgrDevice so that it reports true only when having received this info.
                if( bLTIsConnected && bHaveLTRFC )
                {
                    m_devPoller->GetDeviceCfgStraps( regData.byData[0] );

                    // If we have the cfg straps, the byte will be non-zero. In that
                    // case clear the upper byte too
                    if( regData.byData[0] != 0xFF )
                        regData.byData[1] = 0;
                }

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;

            case eLTDT_LTArmAngle:

                // Override arm angle based on current device status
                if( !bLTIsConnected )
                    regData.asWORD = ANGLE_ALARM_NOT_CONNECTED;
                else if( !bLTIsRecving )
                    regData.asWORD = ANGLE_ALARM_NOT_RECEIVING;
                else
                    regData.asWORD = m_wArmAngle;

                m_modbusDev->SetRegisterValueByIndex( iReg, regData );
                break;
        }
    }

}


//
// Windows Message Handlers
//

void __fastcall TLinkTiltMainForm::WMShowSysSettingsDlg( TMessage &Message )
{
    // This event is only called from the constructor when there is an error
    // with the comm settings.
    MessageDlg( "The device port could not be opened (" + m_devPoller->ConnectResult + "). You will have to modify the port settings before continuing.",
                mtError, TMsgDlgButtons() << mbOK, 0 );

    SysSettingsBtnClick( SysSettingsBtn );
}


void __fastcall TLinkTiltMainForm::WMCommsEvent( TMessage &Message )
{
    // Event posted the by comms mgr when something changes
    switch( Message.WParam )
    {
        case eCPE_Initializing:
            // Event posted when comm port is opened and init process started
            UpdateStatusScanning();
            break;

        case eCPE_InitComplete:
            // Ports are open and ready to start mode operating
          break;

        case eCPE_PortLost:
            // Event posted when a port which was open has been lost
            UpdateStatusScanning();
            break;

        case eCPE_CommsLost:
            // Event posted when we've lost comms with either the MPL or base radio
            UpdateStatusScanning();
            break;

        case eCPE_RFCRecvd:
            // RFC received, new status values will be available
            UpdateStatusValues();
            break;

        default:
            break;
    }
}

