//---------------------------------------------------------------------------
#ifndef TLinkTiltMainH
#define TLinkTiltMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
#include "AdvSmoothButton.hpp"
#include "AdvSmoothLabel.hpp"
#include "AdvSmoothStatusIndicator.hpp"
#include "AdvSmoothPanel.hpp"
#include "AdvSmoothToggleButton.hpp"
#include "AdvSmoothProgressBar.hpp"
#include "GDIPPictureContainer.hpp"
#include <Vcl.ImgList.hpp>
#include "TCommPoller.h"
#include "TModbusDevice.h"
#include "Messages.h"
#include "LinkTiltRegistryInterface.h"
//---------------------------------------------------------------------------
class TLinkTiltMainForm : public TForm
{
__published:	// IDE-managed Components
    TAdvSmoothPanel *MenuPanel;
    TImage *TESCOLogoImage;
    TTimer *SystemTimer;
    TAdvSmoothToggleButton *SysLogBtn;
    TAdvSmoothToggleButton *SysSettingsBtn;
    TAdvSmoothToggleButton *AboutBtn;
    TAdvSmoothPanel *BaseRadioStatusPanel;
    TAdvSmoothPanel *BattLevelStatusPanel;
    TAdvSmoothPanel *TiltLinkStatusPanel;
    TAdvSmoothPanel *TempStatusPanel;
    TAdvSmoothProgressBar *BaseRadioProgBar;
    TAdvSmoothProgressBar *TiltLinkProgBar;
    TAdvSmoothPanel *PLCLinkStatusPanel;
    TAdvSmoothProgressBar *PLCLinkProgBar;
    TAdvSmoothPanel *TiltAnglePanel;
    void __fastcall AboutBtnClick(TObject *Sender);
    void __fastcall SystemTimerTimer(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall SysLogBtnClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall SysSettingsBtnClick(TObject *Sender);

private:
    TCommPoller*   m_devPoller;
    TModbusDevice* m_modbusDev;

    ANGLE_FACTORS  m_angleFactors;
    WORD           m_wArmAngle;

    bool m_modbusInitComplete;

    void UpdatePLCInterface( void );

    void UpdateStatusValues( void );
    void UpdateStatusScanning( void );
    void SetSmoothPanelCaption( TAdvSmoothPanel* pPanel, const String& sText );

protected:
    void __fastcall WMShowSysSettingsDlg( TMessage &Message );
    void __fastcall WMCommsEvent        ( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_SHOW_SYS_SETTINGS_DLG, TMessage, WMShowSysSettingsDlg )
      MESSAGE_HANDLER( WM_POLLER_EVENT,          TMessage, WMCommsEvent )
    END_MESSAGE_MAP(TForm)

public:		// User declarations
    __fastcall TLinkTiltMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TLinkTiltMainForm *LinkTiltMainForm;
//---------------------------------------------------------------------------
#endif
