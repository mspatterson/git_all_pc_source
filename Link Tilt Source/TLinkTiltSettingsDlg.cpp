#include <vcl.h>
#pragma hdrstop

#include <FileCtrl.hpp>
#include <IniFiles.hpp>
#include <DateUtils.hpp>
#include <Math.hpp>

#include "TLinkTiltSettingsDlg.h"
#include "LinkTiltRegistryInterface.h"
#include "ApplUtils.h"
#include "TesTorkManagerPWGen.h"
#include "NewBattDlg.h"
#include "Messages.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TLinkTiltSettingsDlg *LinkTiltSettingsDlg;


//
// Private Functions
//

static bool IsValidInt( TEdit* anEdit )
{
    try
    {
        int iVal = anEdit->Text.ToInt();
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


static bool IsValidFloat( TEdit* anEdit )
{
    float fVal = 0.0;

    try
    {
        fVal = anEdit->Text.ToDouble();
    }
    catch( ... )
    {
    }

    return( fVal > 0.0 );
}


// The calibration load from file / save to file functions use the ini file format.
// The following constants define the section names in those files.
static const String calIniDataSection( "Calibration Information" );


// Process Priority Combo settings
typedef struct {
    char*  szDesc;
    DWORD  procEnum;
} PROC_PRIORITY_ENTRY;

#define NBR_PROC_PRIORITIES 4

static const PROC_PRIORITY_ENTRY ProcPriorities[NBR_PROC_PRIORITIES] = {
    { "Normal",        NORMAL_PRIORITY_CLASS       },
    { "Above Normal",  ABOVE_NORMAL_PRIORITY_CLASS },
    { "High Priority", HIGH_PRIORITY_CLASS         },
    { "Real Time",     REALTIME_PRIORITY_CLASS     }
};


//
// Class Implementation
//

__fastcall TLinkTiltSettingsDlg::TLinkTiltSettingsDlg(TComponent* Owner) : TForm(Owner)
{
    // Init the comms sheet controls structs
    m_TMCommsCtrls.pTypeCombo     = TMPortTypeCombo;
    m_TMCommsCtrls.pPortNbrEdit   = TMPortEdit;
    m_TMCommsCtrls.pParamEdit     = TMParamEdit;

    m_BaseCommsCtrls.pTypeCombo   = BaseRadioPortTypeCombo;
    m_BaseCommsCtrls.pPortNbrEdit = BaseRadioPortEdit;
    m_BaseCommsCtrls.pParamEdit   = BaseRadioParamEdit;

    m_MgmtCommsCtrls.pTypeCombo   = MgmtDevPortTypeCombo;
    m_MgmtCommsCtrls.pPortNbrEdit = MgmtDevPortEdit;
    m_MgmtCommsCtrls.pParamEdit   = MgmtDevParamEdit;

    // Init comm type combos
    TMPortTypeCombo->Items->Clear();
    BaseRadioPortTypeCombo->Items->Clear();
    MgmtDevPortTypeCombo->Items->Clear();

    for( int iType = 0; iType < NBR_COMM_TYPES; iType++ )
    {
        TMPortTypeCombo->Items->Add       ( CommTypeText[iType] );
        BaseRadioPortTypeCombo->Items->Add( CommTypeText[iType] );
        MgmtDevPortTypeCombo->Items->Add  ( CommTypeText[iType] );
    }

    TMPortTypeCombo->ItemIndex        = CT_SERIAL;
    BaseRadioPortTypeCombo->ItemIndex = CT_SERIAL;
    MgmtDevPortTypeCombo->ItemIndex   = CT_SERIAL;

    // Init base radio chan combo
    BRChanCombo->Items->Clear();

    for( int iChan = 0; iChan < NbrBaseRadioChans; iChan++ )
        BRChanCombo->Items->Add( IntToStr( BaseRadioChans[iChan] ) );

    BRChanCombo->ItemIndex = 0;

    // Init process priority combo
    ProcPriorityCombo->Items->Clear();

    for( int iItem = 0; iItem < NBR_PROC_PRIORITIES; iItem++ )
        ProcPriorityCombo->Items->Add( ProcPriorities[iItem].szDesc );

    ProcPriorityCombo->ItemIndex = 0;

    // Init averaging combos
    AccelAvgCombo->Items->Clear();
    CompassAvgCombo->Items->Clear();
    RSSIAvgCombo->Items->Clear();

    for( int iItem = 0; iItem < NBR_AVG_METHODS; iItem++ )
    {
        AccelAvgCombo->Items->Add  ( AvgMethodDesc[iItem] );
        CompassAvgCombo->Items->Add( AvgMethodDesc[iItem] );
        RSSIAvgCombo->Items->Add   ( AvgMethodDesc[iItem] );
    }

    AccelAvgCombo->ItemIndex   = 0;
    CompassAvgCombo->ItemIndex = 0;
    RSSIAvgCombo->ItemIndex    = 0;
}


DWORD TLinkTiltSettingsDlg::ShowDlg( TCommPoller* pPoller, TModbusDevice* pModbusObj )
{
    DWORD dwSettingsChanged = 0x0000;

    TLinkTiltSettingsDlg* currForm = new TLinkTiltSettingsDlg( NULL );

    currForm->PollObject   = pPoller;
    currForm->MODBUSObject = pModbusObj;

    try
    {
        if( currForm->ShowModal() == mrOk )
            dwSettingsChanged = currForm->SettingsChanged;
    }
    __finally
    {
        delete currForm;
    }

    return dwSettingsChanged;
}


void __fastcall TLinkTiltSettingsDlg::FormShow(TObject *Sender)
{
    // Determine units of measure in effect at time of form show.
    // Get from the current job (if we have one) or the default
    // from the registry otherwise
    m_uomType = (UNITS_OF_MEASURE) GetDefaultUnitsOfMeasure();

    // On show, populate current settings
    LoadCommsPage();
    LoadMiscPage();
    LoadCalPage();

    // Default settings changed to none
    m_dwSettingsChanged = 0x0000;

    PgCtrl->ActivePage        = CommsSheet;
    CalDataPgCtrl->ActivePage = DevMemorySheet;
    DevConnPgCtrl->ActivePage = TMSheet;

    RFCPollTimer->Enabled = true;

    // Force a timer tick right away for immediate update
    RFCPollTimerTimer( RFCPollTimer );
}


void __fastcall TLinkTiltSettingsDlg::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Kill the comms object poll timer first
    RFCPollTimer->Enabled = false;

    // On close, if ModalResult == mrOK, then save current settings.
    // In this case, all settings are guaranteed to be valid.
    if( ModalResult == mrOk )
    {
        m_dwSettingsChanged = 0x0000;

        m_dwSettingsChanged |= SaveCommsPage();
        m_dwSettingsChanged |= SaveCalPage();
        m_dwSettingsChanged |= SaveMiscPage();
    }
}


void __fastcall TLinkTiltSettingsDlg::OKBtnClick(TObject *Sender)
{
    // Validate settings
    TWinControl* invalidControl = CheckCommsPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = CommsSheet;

        if( invalidControl == RadioChanTimeoutEdit )
            DevConnPgCtrl->ActivePage = TMSheet;
        else if( ( invalidControl == MODBUSPortEdit ) || ( invalidControl == MODBUSDevIDEdit ) )
            DevConnPgCtrl->ActivePage = PLCSheet;
        else
            DevConnPgCtrl->ActivePage = PortsSheet;

        ActiveControl = invalidControl;

        return;
    }

    invalidControl = CheckCalPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = CalSheet;
        ActiveControl = invalidControl;

        return;
    }

    invalidControl = CheckMiscPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = MiscSheet;
        ActiveControl = invalidControl;

        return;
    }

    // Values look good. Can close dialog
    ModalResult = mrOk;
}


void __fastcall TLinkTiltSettingsDlg::CommTypeClick(TObject *Sender)
{
    // There are two identical tabs of comm settings tab sheets.
    // Find the parent of the sheet of the Sender, then enable
    // all edits on that sheet whose Tag matches that of the
    // Sender. All other edits are disabled.
    TRadioButton* pRB = dynamic_cast<TRadioButton*>(Sender);

    if( pRB == NULL )
        return;

    TTabSheet* parentSheet = (TTabSheet*)( pRB->Parent );

    for( int iCtrl = 0; iCtrl < parentSheet->ControlCount; iCtrl++ )
    {
        TEdit* pEdit = dynamic_cast<TEdit*>(parentSheet->Controls[iCtrl]);

        if( pEdit == NULL )
            continue;

        if( pEdit->Tag == pRB->Tag )
        {
            pEdit->Enabled = true;
            pEdit->Color   = clWindow;
        }
        else
        {
            pEdit->Enabled = false;
            pEdit->Color   = clBtnFace;
        }
    }
}


void __fastcall TLinkTiltSettingsDlg::BrowseRFCLogFileBtnClick(TObject *Sender)
{
    if( SaveLogDlg->Execute() )
        RFCLogFileNameEdit->Text = SaveLogDlg->FileName;
}


void __fastcall TLinkTiltSettingsDlg::SavePWBtnClick(TObject *Sender)
{
    // Password is saved with explicit button click, rather than
    // when the dialog is closed.
    UnicodeString currPW     = GetSysAdminPassword();
    UnicodeString backdoorPW = GeneratePassword();

    if( ( currPW != CurrPWEdit->Text ) && ( backdoorPW != CurrPWEdit->Text ) )
    {
        MessageDlg( "The current password you have entered is incorrect.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = CurrPWEdit;
        return;
    }

    if( NewPWEdit1->Text.Trim().Length() == 0 )
    {
        MessageDlg( "Your new password cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewPWEdit1;
        return;
    }

    if( NewPWEdit1->Text != NewPWEdit2->Text )
    {
        MessageDlg( "Your new passwords do not match.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewPWEdit1;
        return;
    }

    // Password looks good. Save it and clear the boxes
    SaveSysAdminPassword( NewPWEdit1->Text );

    // Clear the edits of any text
    CurrPWEdit->Text = "";
    NewPWEdit1->Text = "";
    NewPWEdit2->Text = "";

    MessageDlg( "Password successfully changed.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
}


void __fastcall TLinkTiltSettingsDlg::BrowseJobDirBtnClick(TObject *Sender)
{
    UnicodeString jobDir = JobDirEdit->Text;

      if( SelectDirectory( "Select Job Directory", "Desktop", jobDir, TSelectDirExtOpts() << sdNewUI << sdNewFolder, NULL ) )
        JobDirEdit->Text = jobDir;
}


void __fastcall TLinkTiltSettingsDlg::ListEditorSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect)
{
    // Called by read-only grids
    CanSelect = false;
}


void TLinkTiltSettingsDlg::SetCommsCtrls( const COMMS_CFG& commCfg, const COMMS_SHEET_CTRLS& sheetCtrls )
{
    if( ( commCfg.portType >= 0 ) && ( commCfg.portType < sheetCtrls.pTypeCombo->Items->Count ) )
        sheetCtrls.pTypeCombo->ItemIndex = commCfg.portType;
    else
        sheetCtrls.pTypeCombo->ItemIndex = CT_SERIAL;

    switch( commCfg.portType )
    {
        case CT_SERIAL:
        case CT_UDP:
        case CT_USB:
            sheetCtrls.pPortNbrEdit->Text = commCfg.portName;

            // Force a default value if the param value is 0
            if( commCfg.portParam == 0 )
            {
                if( commCfg.portType == CT_UDP )
                    sheetCtrls.pParamEdit->Text = "10000";
                else
                    sheetCtrls.pParamEdit->Text = "115200";
            }
            else
            {
                sheetCtrls.pParamEdit->Text = IntToStr( (int)commCfg.portParam );
            }
            break;

        default:
            sheetCtrls.pPortNbrEdit->Text    = "1";
            sheetCtrls.pPortNbrEdit->Enabled = false;
            sheetCtrls.pParamEdit->Text      = "115200";
            sheetCtrls.pParamEdit->Enabled   = false;
            break;
    }
}


TWinControl* TLinkTiltSettingsDlg::GetCommsCtrls( COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls )
{
    // Assume default for now
    commsCfg.portType  = CT_UNUSED;
    commsCfg.portName  = "";
    commsCfg.portParam = 0;

    int iTestPort;
    int iTestParam;

    switch( sheetCtrls.pTypeCombo->ItemIndex )
    {
        case CT_SERIAL:

            iTestPort = sheetCtrls.pPortNbrEdit->Text.ToIntDef( 0 );

            if( iTestPort <= 0 )
            {
                MessageDlg( "Invalid Comm Port number.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pPortNbrEdit;
            }

            iTestParam = sheetCtrls.pParamEdit->Text.ToIntDef( 0 );

            if( iTestParam <= 0 )
            {
                MessageDlg( "Invalid serial Bit Rate value.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pParamEdit;
            }

            commsCfg.portType  = CT_SERIAL;
            commsCfg.portName  = IntToStr( iTestPort );
            commsCfg.portParam = iTestParam;

            break;

        case CT_UDP:

            // Port number may or may not be blank, depending on the
            // device we're connecting to (or being connected from)
            // However, a port must always be given
            iTestParam = sheetCtrls.pParamEdit->Text.ToIntDef( 0 );

            if( ( iTestParam <= 0 ) || ( iTestParam > 65535 ) )
            {
                MessageDlg( "Invalid UDP port number.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pParamEdit;
            }

            commsCfg.portType  = CT_UDP;
            commsCfg.portName  = sheetCtrls.pPortNbrEdit->Text;
            commsCfg.portParam = iTestParam;

            break;

        case CT_USB:

            if( sheetCtrls.pPortNbrEdit->Text.Trim().Length() == 0 )
            {
                MessageDlg( "USB Port Description cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pPortNbrEdit;
            }

            iTestParam = sheetCtrls.pParamEdit->Text.ToIntDef( 0 );

            if( iTestParam <= 0 )
            {
                MessageDlg( "Invalid USB Bit Rate value.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pParamEdit;
            }

            commsCfg.portType  = CT_USB;
            commsCfg.portName  = sheetCtrls.pPortNbrEdit->Text;
            commsCfg.portParam = iTestParam;

            break;

        case CT_UNUSED:
            break;

        default:
            // Should never occur, but if it does return something to indicate an error
            MessageDlg( "TSystemSettingsForm::GetCommsCtrls() : invalid port type.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return sheetCtrls.pTypeCombo;
    }

    // Fall through means all values are valid
    return NULL;
}


void TLinkTiltSettingsDlg::SetAverageControls( const AVERAGING_PARAMS& avgParams, TComboBox* pTypeCombo, TLabel* pParamLabel, TEdit* pParamEdit )
{
    // Private method - no validation performed on passed pointers
    pTypeCombo->ItemIndex = avgParams.avgMethod;

    SetAverageParams( avgParams.avgMethod, pParamLabel, pParamEdit );

    switch( avgParams.avgMethod )
    {
        case AM_BOXCAR:
            pParamEdit->Text = IntToStr( (int)( avgParams.param + 0.5 ) );
            break;

        case AM_EXPONENTIAL:
            pParamEdit->Text = FloatToStrF( avgParams.param, ffFixed, 7, 3 );
            break;

        default:
            pParamEdit->Text = "";
            break;
    }

    // Force refreshes of the averaging param labels
    pTypeCombo->OnClick( pTypeCombo );
}


void TLinkTiltSettingsDlg::SetAverageParams( AVG_METHOD aMethod, TLabel* pParamLabel, TEdit* pParamEdit )
{
    pParamLabel->Caption = AvgMethodParamDesc[aMethod];

    switch( aMethod )
    {
        case AM_BOXCAR:
            pParamEdit->Visible  = true;
            pParamLabel->Visible = true;
            break;

        case AM_EXPONENTIAL:
            pParamEdit->Visible  = true;
            pParamLabel->Visible = true;
            break;

        default:
            pParamEdit->Visible  = false;
            pParamLabel->Visible = false;
            break;
    }
}


bool TLinkTiltSettingsDlg::GetAverageControls( AVERAGING_PARAMS& avgParams, TComboBox* pTypeCombo, TLabel* pParamLabel, TEdit* pParamEdit )
{
    if( pTypeCombo->ItemIndex < 0 )
        return false;

    avgParams.avgMethod = (AVG_METHOD)( pTypeCombo->ItemIndex );

    // Interpret the params
    int   iTest;
    float fTest;

    switch( avgParams.avgMethod )
    {
        case AM_NONE:
            // Param not used in this case
            avgParams.param = 0.0;
            break;

        case AM_BOXCAR:
            // Param must be a non-negative integer
            iTest = pParamEdit->Text.ToIntDef( -1 );

            if( iTest < 0 )
                return false;

            avgParams.param = iTest;

            break;

        case AM_EXPONENTIAL:
            // Param must be a non-negative float
            fTest = StrToFloatDef( pParamEdit->Text, -1 );

            if( fTest < 0.0 )
                return false;

            avgParams.param = fTest;

            break;

        default:
            // Don't know this method!
            return false;
    }

    // Averaging level and variance is not used in this release. Set to default values.
    avgParams.avgLevel    = AL_OFF;
    avgParams.absVariance = 0.0;

    return true;
}


void TLinkTiltSettingsDlg::LoadCommsPage( void )
{
    COMMS_CFG tmCommCfg;
    COMMS_CFG baseCommCfg;
    COMMS_CFG mgmtCommCfg;

    GetCommSettings( DCT_TM,   tmCommCfg );
    GetCommSettings( DCT_BASE, baseCommCfg );
    GetCommSettings( DCT_MGMT, mgmtCommCfg );

    m_OrigCommsSettings.tmCommCfg   = tmCommCfg;
    m_OrigCommsSettings.baseCommCfg = baseCommCfg;
    m_OrigCommsSettings.mgmtCommCfg = mgmtCommCfg;

    SetCommsCtrls( tmCommCfg,    m_TMCommsCtrls );
    SetCommsCtrls( baseCommCfg,  m_BaseCommsCtrls );
    SetCommsCtrls( mgmtCommCfg,  m_MgmtCommsCtrls );

    bool bAutoAssignPorts = GetAutoAssignCommPorts();

    m_OrigCommsSettings.bAutoAssignPorts = bAutoAssignPorts;

    AutoAssignPortsCB->Checked = bAutoAssignPorts;

    // Data Logging
    UnicodeString logFileName;

    EnableRFCLogCB->Checked  = GetDataLogging( DLT_RFC_DATA, logFileName );
    RFCLogFileNameEdit->Text = logFileName;

    m_OrigCommsSettings.bEnableRFCLog   = EnableRFCLogCB->Checked;
    m_OrigCommsSettings.sRFCLogFileName = logFileName;

    // Parameters
    RadioChanTimeoutEdit->Text = UIntToStr( (UINT)( GetRadioChanWaitTimeout() / 1000 ) );
    m_OrigCommsSettings.dwRadioChanTimeout = GetRadioChanWaitTimeout();

    // Show current MPL radio channel number
    if( m_poller != NULL )
    {
        int iCurrChan = m_poller->GetRadioChannel( TBaseRadioDevice::BRN_MPL );

        BRChanCombo->ItemIndex = BRChanCombo->Items->IndexOf( IntToStr( iCurrChan ) );
    }
    else
    {
        BRChanCombo->ItemIndex = -1;
    }

    // PLC Page. Note: only a subset of MODBUS settings are displayed
    MODBUS_SETTINGS modbusSettings;
    GetMODBUSSettings( modbusSettings );

    m_OrigCommsSettings.wMODBUSIPPort   = modbusSettings.wIPPort;
    m_OrigCommsSettings.byMODBUSDevAddr = modbusSettings.byDevAddr;

    MODBUSPortEdit->Text  = UIntToStr( (UINT)modbusSettings.wIPPort );
    MODBUSDevIDEdit->Text = UIntToStr( (UINT)modbusSettings.byDevAddr );

    // Load the register map, if we have a MODBUS device
    RegistersLV->Items->Clear();

    if( m_modbusObj != NULL )
    {
        for( int iReg = 0; iReg < m_modbusObj->RegisterCount; iReg++ )
        {
            // We keep the listview items in the same order as the regs in the MODBUS device
            TListItem* pNewItem = RegistersLV->Items->Add();

            pNewItem->Caption = UIntToStr( (UINT)m_modbusObj->RegisterNumber[iReg] );
            pNewItem->SubItems->Add( m_modbusObj->RegisterName[iReg] );
            pNewItem->SubItems->Add( "" );  // Value gets updated later
        }
    }

    RefreshMODBUSRegMap();
}


void TLinkTiltSettingsDlg::LoadCalPage( void )
{
    FormattedCalDataEditor->Strings->Clear();
    RawCalDataEditor->Strings->Clear();

    // Load raw data view first
    if( m_poller != NULL )
    {
       TStringList* pCaptions = new TStringList();
       TStringList* pValues   = new TStringList();

       if( m_poller->GetCalDataRaw( pCaptions, pValues ) )
       {
           for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
               RawCalDataEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
       }

       if( m_poller->GetCalDataFormatted( pCaptions, pValues ) )
       {
           for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
               FormattedCalDataEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
       }

       delete pCaptions;
       delete pValues;
    }

    // Can only change battery if we have a poller
    NewBattBtn->Enabled = ( m_poller != NULL );

    // The WriteToDevBtn's tag is used to indicate if the user has
    // uploaded new data. Clear it on load.
    WriteToDevBtn->Tag = 0;

    // Load angle factors
    GetAngleFactors( m_OrigCalSettings.angleFactors );

    // Hardcoded hack here for determining AngleAccelCombo index. Only certain
    // value combinations are allowed for the X and Y accel components
    if( m_OrigCalSettings.angleFactors.iXAccelComponent == 1 )
        AngleAccelCombo->ItemIndex = 0;
    else if( m_OrigCalSettings.angleFactors.iXAccelComponent == -1 )
        AngleAccelCombo->ItemIndex = 1;
    else if( m_OrigCalSettings.angleFactors.iYAccelComponent == 1 )
        AngleAccelCombo->ItemIndex = 2;
    else if( m_OrigCalSettings.angleFactors.iYAccelComponent == -1 )
        AngleAccelCombo->ItemIndex = 3;
    else
        AngleAccelCombo->ItemIndex = 0;

    AngleRotnOffsetEdit->Text = IntToStr   ( m_OrigCalSettings.angleFactors.iRotationOffset );
    MinAccelValEdit->Text     = FloatToStrF( m_OrigCalSettings.angleFactors.fMinAccelAmpl, ffFixed, 7, 2 );
}


void TLinkTiltSettingsDlg::LoadMiscPage( void )
{
    // Clear password field edits
    CurrPWEdit->Text = "";
    NewPWEdit1->Text = "";
    NewPWEdit2->Text = "";

    // Get process priority. Force default setting to start, but
    // override that when we find the priority in the list.
    ProcPriorityCombo->ItemIndex = 0;

    DWORD procPriority = GetProcessPriority();

    m_OrigMiscSettings.dwProcPriority = procPriority;

    for( int iItem = 0; iItem < NBR_PROC_PRIORITIES; iItem++ )
    {
        if( ProcPriorities[iItem].procEnum == procPriority )
        {
            ProcPriorityCombo->ItemIndex = iItem;
            break;
        }
    }

    // Load data averaging from registry. Format of the factor value
    // depends on the averaging type.
    GetAveragingSettings( AT_ACCEL,   m_OrigMiscSettings.accelAvgParams );
    GetAveragingSettings( AT_COMPASS, m_OrigMiscSettings.compassAvgParams );
    GetAveragingSettings( AT_RSSI,    m_OrigMiscSettings.RSSIAvgParams );

    SetAverageControls( m_OrigMiscSettings.accelAvgParams,   AccelAvgCombo,   AccelParamLabel,   AccelAvgParamEdit );
    SetAverageControls( m_OrigMiscSettings.compassAvgParams, CompassAvgCombo, CompassParamLabel, CompassAvgParamEdit );
    SetAverageControls( m_OrigMiscSettings.RSSIAvgParams,    RSSIAvgCombo,    RSSIParamLabel,    RSSIAvgParamEdit );
}


TWinControl* TLinkTiltSettingsDlg::CheckCommsPage( void )
{
    COMMS_CFG temp;

    TWinControl* commsErrCtrl = GetCommsCtrls( temp, m_TMCommsCtrls );

    if( commsErrCtrl != NULL )
        return commsErrCtrl;

    commsErrCtrl = GetCommsCtrls( temp, m_BaseCommsCtrls );

    if( commsErrCtrl != NULL )
        return commsErrCtrl;

    if( EnableRFCLogCB->Checked )
    {
        if( RFCLogFileNameEdit->Text.Trim().Length() == 0 )
        {
            MessageDlg( "You must specify a file name to enable logging.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return RFCLogFileNameEdit;
        }
    }

    int iTestValue = RadioChanTimeoutEdit->Text.ToIntDef( 0 );

    if( iTestValue <= 0 )
    {
        MessageDlg( "You have entered an invalid Radio Channel Timeout value.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return RadioChanTimeoutEdit;
    }

    iTestValue = MODBUSPortEdit->Text.ToIntDef( 0 );

    if( ( iTestValue <= 0 ) || ( iTestValue > 65535 ) )
    {
        MessageDlg( "You have entered an invalid value for the MODBUS Listening Port.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return MODBUSPortEdit;
    }

    iTestValue = MODBUSDevIDEdit->Text.ToIntDef( -1 );

    if( ( iTestValue < 0 ) || ( iTestValue > 255 ) )
    {
        MessageDlg( "You have entered an invalid value for the MODBUS Device Address.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return MODBUSDevIDEdit;
    }

    // Fall through means all page controls valid
    return NULL;
}


TWinControl* TLinkTiltSettingsDlg::CheckCalPage( void )
{
    // If a calibration has been loaded, but not written to the device,
    // warn the user.
    if( WriteToDevBtn->Tag == 1 )
    {
        int mrResult = MessageDlg( "You have loaded calibration data but not written the data to the device. "
                                   "If you exit now, the software will continue to use this calibration data, "
                                   "but the data stored on the device will be different."
                                   "\r  Press OK to continue with different calibrations."
                                   "\r  Press Cancel to reconsider.",
                                   mtWarning, TMsgDlgButtons() << mbOK << mbCancel, 0 );

        if( mrResult != mrOk )
            return FormattedCalDataEditor;
    }

    // Rotation offset must be an integer value from 180 to -180
    int iTest = AngleRotnOffsetEdit->Text.ToIntDef( 9999 );

    if( ( iTest < -180 ) || ( iTest > 180 ) )
        return AngleRotnOffsetEdit;

    // Min accel must be >= 0 and < 1.0
    float fTest = StrToFloatDef( MinAccelValEdit->Text, 0.0 );

    if( ( fTest < 0.0 ) || ( fTest > 1.0 ) )
        return MinAccelValEdit;

    return NULL;
}


TWinControl* TLinkTiltSettingsDlg::CheckMiscPage( void )
{
    AVERAGING_PARAMS avgParams;

    if( !GetAverageControls( avgParams, AccelAvgCombo, AccelParamLabel, AccelAvgParamEdit ) )
        return AccelAvgParamEdit;

    if( !GetAverageControls( avgParams, CompassAvgCombo, CompassParamLabel, CompassAvgParamEdit ) )
        return CompassAvgParamEdit;

    if( !GetAverageControls( avgParams, RSSIAvgCombo, RSSIParamLabel, RSSIAvgParamEdit ) )
        return RSSIAvgParamEdit;

    return NULL;
}


DWORD TLinkTiltSettingsDlg::SaveCommsPage( void )
{
    // All controls guaranteed to be valid with this function is called.
    bool bSettingsChanged = false;

    // Save comms settings
    COMMS_CFG tmCommCfg;
    COMMS_CFG baseCommCfg;
    COMMS_CFG mgmtCommCfg;

    GetCommsCtrls( tmCommCfg,    m_TMCommsCtrls  );
    GetCommsCtrls( baseCommCfg,  m_BaseCommsCtrls );
    GetCommsCtrls( mgmtCommCfg,  m_MgmtCommsCtrls );

    if( ( tmCommCfg.portType != m_OrigCommsSettings.tmCommCfg.portType ) || ( tmCommCfg.portName != m_OrigCommsSettings.tmCommCfg.portName ) || ( tmCommCfg.portParam != m_OrigCommsSettings.tmCommCfg.portParam ) )
        bSettingsChanged = true;

    if( ( baseCommCfg.portType != m_OrigCommsSettings.baseCommCfg.portType ) || ( baseCommCfg.portName != m_OrigCommsSettings.baseCommCfg.portName ) || ( baseCommCfg.portParam != m_OrigCommsSettings.baseCommCfg.portParam ) )
        bSettingsChanged = true;

    if( ( mgmtCommCfg.portType != m_OrigCommsSettings.mgmtCommCfg.portType ) || ( mgmtCommCfg.portName != m_OrigCommsSettings.mgmtCommCfg.portName ) || ( mgmtCommCfg.portParam != m_OrigCommsSettings.mgmtCommCfg.portParam ) )
        bSettingsChanged = true;

    SaveCommSettings( DCT_TM,   tmCommCfg );
    SaveCommSettings( DCT_BASE, baseCommCfg );
    SaveCommSettings( DCT_MGMT, mgmtCommCfg );

    if( AutoAssignPortsCB->Checked != m_OrigCommsSettings.bAutoAssignPorts )
        bSettingsChanged = true;

    SaveAutoAssignCommPorts( AutoAssignPortsCB->Checked );

    if( ( EnableRFCLogCB->Checked != m_OrigCommsSettings.bEnableRFCLog ) || ( RFCLogFileNameEdit->Text != m_OrigCommsSettings.sRFCLogFileName ) )
        bSettingsChanged = true;

    // Save raw logging settings
    SaveDataLogging( DLT_RFC_DATA, EnableRFCLogCB->Checked, RFCLogFileNameEdit->Text );

    // Parameters
    DWORD dwTimeout = RadioChanTimeoutEdit->Text.ToInt() * 1000;

    SaveRadioChanWaitTimeout( dwTimeout );

    if( m_OrigCommsSettings.dwRadioChanTimeout != dwTimeout )
        bSettingsChanged = true;

    // PLC - only a subset are configurable in this release. We also force
    // the use of TCP/IP
    MODBUS_SETTINGS modbusSettings;
    GetMODBUSSettings( modbusSettings );

    modbusSettings.eCommType = eMCT_EnetTCP;
    modbusSettings.wIPPort   = MODBUSPortEdit->Text.ToInt();
    modbusSettings.byDevAddr = MODBUSDevIDEdit->Text.ToInt();

    SaveMODBUSSettings( modbusSettings );

    bool bPLCSettingsChanged = false;

    bPLCSettingsChanged |= ( m_OrigCommsSettings.wMODBUSIPPort   != modbusSettings.wIPPort );
    bPLCSettingsChanged |= ( m_OrigCommsSettings.byMODBUSDevAddr != modbusSettings.byDevAddr );

    // Return changed flags
    DWORD changeFlags = 0;

    if( bSettingsChanged )
        changeFlags |= COMMS_SETTINGS_CHANGED;

    if( bPLCSettingsChanged )
        changeFlags |= PLC_SETTINGS_CHANGED;

    return changeFlags;
}


DWORD TLinkTiltSettingsDlg::SaveCalPage( void )
{
    // Settings will have been validated already
    bool bSettingsChanged = false;

    ANGLE_FACTORS newFactors = { 0 };

    // Hardcoded hack here for determining AngleAccelCombo index. Only certain
    // value combinations are allowed for the X and Y accel components
    if( AngleAccelCombo->ItemIndex == 0 )
    {
        newFactors.iXAccelComponent = 1;
        newFactors.iYAccelComponent = 0;
    }
    else if( AngleAccelCombo->ItemIndex == 1 )
    {
        newFactors.iXAccelComponent = -1;
        newFactors.iYAccelComponent = 0;
    }
    else if( AngleAccelCombo->ItemIndex == 2 )
    {
        newFactors.iXAccelComponent = 0;
        newFactors.iYAccelComponent = 1;
    }
    else if( AngleAccelCombo->ItemIndex == 3 )
    {
        newFactors.iXAccelComponent = 0;
        newFactors.iYAccelComponent = -1;
    }
    else
    {
        newFactors.iXAccelComponent = 1;
        newFactors.iYAccelComponent = 0;
    }

    newFactors.iRotationOffset = AngleRotnOffsetEdit->Text.ToInt();
    newFactors.fMinAccelAmpl   = MinAccelValEdit->Text.ToDouble();

    if( memcmp( &newFactors, &m_OrigCalSettings.angleFactors, sizeof( ANGLE_FACTORS ) ) != 0 )
        bSettingsChanged = true;

    SaveAngleFactors( newFactors );

    // Now return flags indicating what has changed
    if( bSettingsChanged )
        return CAL_SETTINGS_CHANGED;

    return 0;
}


DWORD TLinkTiltSettingsDlg::SaveMiscPage( void )
{
    // All controls guaranteed to be valid with this function is called.
    bool bSettingsChanged = false;

    if( ProcPriorities[ ProcPriorityCombo->ItemIndex ].procEnum != m_OrigMiscSettings.dwProcPriority )
        bSettingsChanged = true;

    // Save process setting
    SetProcessPriority( ProcPriorities[ ProcPriorityCombo->ItemIndex ].procEnum );

    // Save averaging. Because of previous validation, GetAverageControls() will
    // always succeed
    AVERAGING_PARAMS accelAvg;
    AVERAGING_PARAMS compassAvg;
    AVERAGING_PARAMS rssiAvg;

    GetAverageControls( accelAvg,   AccelAvgCombo,   AccelParamLabel,   AccelAvgParamEdit   );
    GetAverageControls( compassAvg, CompassAvgCombo, CompassParamLabel, CompassAvgParamEdit );
    GetAverageControls( rssiAvg,    RSSIAvgCombo,    RSSIParamLabel,    RSSIAvgParamEdit    );

    bSettingsChanged |= !TAverage::IsSameAverage( accelAvg,   m_OrigMiscSettings.accelAvgParams   );
    bSettingsChanged |= !TAverage::IsSameAverage( compassAvg, m_OrigMiscSettings.compassAvgParams );
    bSettingsChanged |= !TAverage::IsSameAverage( rssiAvg,    m_OrigMiscSettings.RSSIAvgParams    );

    SaveAveragingSettings( AT_ACCEL,   accelAvg   );
    SaveAveragingSettings( AT_COMPASS, compassAvg );
    SaveAveragingSettings( AT_RSSI,    rssiAvg    );

    // Return bit flag if any settings changed
    if( bSettingsChanged )
        return MISC_SETTINGS_CHANGED;
    else
        return 0;
}


//
// Local Event Handlers
//

void __fastcall TLinkTiltSettingsDlg::RFCPollTimerTimer(TObject *Sender)
{
    // If we have a device, poll and display its last status
    if( m_poller != NULL )
    {
        TStringList* pCaptions = new TStringList();
        TStringList* pValues   = new TStringList();

        if( m_poller->GetLastStatusPktInfo( pCaptions, pValues ) )
        {
            for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
                LastRFCEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
        }

        // If either the Read From Dev or the Write To Dev buttons are
        // disabled and we the poller is idle, assume that a read/write
        // operation has completed. Reload the cal data in that case.
        if( m_poller->TMIsIdle )
        {
            if( !WriteToDevBtn->Enabled || !ReadFromDevBtn->Enabled )
                LoadCalPage();
        }

        // Update the cal page read/write to device buttons. These are
        // only enabled if the device is idle
        WriteToDevBtn->Enabled  = m_poller->TMIsIdle;
        ReadFromDevBtn->Enabled = m_poller->TMIsIdle;

        // Check for base radio status packet.
        if( m_poller->GetLastRadioStatusPktInfo( pCaptions, pValues ) )
        {
            for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
                LastStatusEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
        }

        delete pCaptions;
        delete pValues;
    }
    else
    {
        WriteToDevBtn->Enabled  = false;
        ReadFromDevBtn->Enabled = false;
    }

    RefreshMODBUSRegMap();
}


void TLinkTiltSettingsDlg::RefreshMODBUSRegMap( void )
{
    for( int iItem = 0; iItem < RegistersLV->Items->Count; iItem++ )
    {
        TListItem* pItem = RegistersLV->Items->Item[iItem];

        REG_DATA_UNION regData;

        if( ( m_modbusObj != NULL ) && m_modbusObj->GetRegisterValueByIndex( iItem, regData ) )
        {
            String sValueText;

            switch( m_modbusObj->RegisterType[iItem] )
            {
                case MRT_BOOL:  sValueText = regData.asBool ? String( "True" ) : String( "False" );   break;
                case MRT_BYTE:  sValueText = UIntToStr( (UINT)regData.byData[0] );                    break;
                case MRT_CHAR:  sValueText.printf( L"%c", regData.asByte );                           break;
                case MRT_INT16: sValueText = IntToStr( regData.asShortInt );                          break;
                case MRT_WORD:  sValueText = UIntToStr( (UINT)regData.asWORD );                       break;
                case MRT_HEX16: sValueText = IntToHex( regData.asWORD, 4 );                           break;
                case MRT_INT32: sValueText = IntToStr( regData.asInt );                               break;
                case MRT_DWORD: sValueText = UIntToStr( (UINT)regData.asDWORD );                      break;
                case MRT_HEX32: sValueText = IntToHex( (int)regData.asDWORD, 8 );                     break;
                case MRT_FLOAT: sValueText = FloatToStr( regData.asFloat );                           break;
                default:        sValueText = "Unknown MODBUS_REG_TYPE";                                        break;
            }

            pItem->SubItems->Strings[1] = sValueText;
        }
        else
        {
            pItem->SubItems->Strings[1] = "";
        }
    }
}


void __fastcall TLinkTiltSettingsDlg::SaveCalDatBtnClick(TObject *Sender)
{
    // Save the data as is in the current view.
    if( SaveCalDataDlg->Execute() )
    {
        TIniFile* pIniFile = new TIniFile( SaveCalDataDlg->FileName );

        pIniFile->EraseSection( calIniDataSection );

        // For each key in the editor, look for a corresponding section in the
        // ini file. If the section is not found, skip it.
        for( int iKey = 1; iKey <= FormattedCalDataEditor->Strings->Count; iKey++ )
        {
            UnicodeString sKey   = FormattedCalDataEditor->Keys[iKey];
            UnicodeString sValue = FormattedCalDataEditor->Values[sKey];

            pIniFile->WriteString( calIniDataSection, sKey, sValue );
        }

        pIniFile->UpdateFile();

        delete pIniFile;
    }
}


void __fastcall TLinkTiltSettingsDlg::LoadCalDatBtnClick(TObject *Sender)
{
    // Load the calibration data. Can only do this from the formatted view.
    if( CalDataPgCtrl->ActivePage != DevMemorySheet )
    {
        MessageDlg( "You can only load data in the Device Memory view.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    if( OpenCalDataDlg->Execute() )
    {
        TStringList* pCaptions = new TStringList();
        TStringList* pValues   = new TStringList();

        try
        {
            TIniFile* pIniFile = new TIniFile( OpenCalDataDlg->FileName );

            // For each key in the editor, look for a corresponding section in the
            // ini file. If the section is not found, skip it.
            for( int iKey = 1; iKey <= FormattedCalDataEditor->Strings->Count; iKey++ )
            {
                UnicodeString sKey     = FormattedCalDataEditor->Keys[iKey];
                UnicodeString newValue = pIniFile->ReadString( calIniDataSection, sKey, "" );

                if( newValue.Length() > 0 )
                {
                    FormattedCalDataEditor->Values[ sKey ] = newValue;
                }
            }

            delete pIniFile;

            // Now write the values to the software cache (not to the device yet though)
            for( int iKey = 1; iKey <= FormattedCalDataEditor->Strings->Count; iKey++ )
            {
                UnicodeString sKey = FormattedCalDataEditor->Keys[iKey];

                pCaptions->Add( sKey );
                pValues->Add( FormattedCalDataEditor->Values[sKey] );
            }

            if( !m_poller->SetCalDataFormatted( pCaptions, pValues ) )
                Abort();
        }
        catch( ... )
        {
            MessageDlg( "Load calibration data failed.", mtError, TMsgDlgButtons() << mbOK, 0 );
        }

        delete pCaptions;
        delete pValues;

        // Regardless of the outcome, always refresh the cal data grids with
        // what is in the cache
        LoadCalPage();
    }
}


void __fastcall TLinkTiltSettingsDlg::WriteToDevBtnClick(TObject *Sender)
{
    // Write the data to the device. On success, set the 'write pending' indication
    if( m_poller->WriteCalDataToDevice() )
        WriteToDevBtn->Tag = 1;
    else
        MessageDlg( "Could not write calibration data to device.", mtError, TMsgDlgButtons() << mbOK, 0 );

    // Call the timer tick function to update the current status
    RFCPollTimerTimer( RFCPollTimer );
}


void __fastcall TLinkTiltSettingsDlg::ReadFromDevBtnClick(TObject *Sender)
{
    // On success, clear any 'write pending' indication
    if( m_poller->ReloadCalDataFromDevice() )
        WriteToDevBtn->Tag = 0;
    else
        MessageDlg( "Could not read calibration data from device.", mtError, TMsgDlgButtons() << mbOK, 0 );

    // Call the timer tick function to update the current status
    RFCPollTimerTimer( RFCPollTimer );
}


void __fastcall TLinkTiltSettingsDlg::NewBattBtnClick(TObject *Sender)
{
    BYTE battType;
    WORD capacity;

    if( TNewBattForm::ShowDlg( battType, capacity ) )
    {
        // We need only pass a list of the cal items that need to be updated to the comms mgr
        TStringList* pCaptions = new TStringList();
        TStringList* pValues   = new TStringList();

        pCaptions->Add( TTiltMgrDevice::CalFactorCaptions[ TTiltMgrDevice::CI_BATTERY_TYPE ] );
        pValues->Add  ( IntToStr( battType ) );

        pCaptions->Add( TTiltMgrDevice::CalFactorCaptions[ TTiltMgrDevice::CI_BATTERY_CAPACITY ] );
        pValues->Add  ( IntToStr( capacity ) );

        if( !m_poller->SetCalDataFormatted( pCaptions, pValues ) )
        {
            MessageDlg( "Could not set new battery information.", mtError, TMsgDlgButtons() << mbOK, 0 );

            delete pCaptions;
            delete pValues;

            return;
        }

        delete pCaptions;
        delete pValues;

        if( !m_poller->WriteCalDataToDevice() )
        {
            MessageDlg( "Could not write calibration data to device.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return;
        }

        // Now wait up to 30 seconds for the cal to upload. Device will return to idle
        // when upload is done.
        NewBattBtn->Enabled = false;
        Screen->Cursor = crHourGlass;

        TDateTime timeoutTime = IncSecond( Now(), 30 );

        while( !m_poller->TMIsIdle )
        {
            // Let the appl process its messages while in this loop
            Application->ProcessMessages();

            if( Now() > timeoutTime )
                break;
        }

        if( m_poller->TMIsIdle )
             MessageDlg( "New battery information successfully uploaded.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
        else
             MessageDlg( "Upload of new battery information failed.", mtError, TMsgDlgButtons() << mbOK, 0 );

        Screen->Cursor = crDefault;
        NewBattBtn->Enabled = true;
    }
}


void __fastcall TLinkTiltSettingsDlg::SendChangeChanBtnClick(TObject *Sender)
{
    if( m_poller != NULL )
    {
        if( BRChanCombo->ItemIndex >= 0 )
        {
            switch( m_poller->SetRadioChannel( BRChanCombo->Items->Strings[ BRChanCombo->ItemIndex ].ToInt(), (TBaseRadioDevice::BASE_RADIO_NBR)BRRadNbrCombo->ItemIndex ) )
            {
                case TBaseRadioDevice::SRCR_SUCCESS:
                    break;

                case TBaseRadioDevice::SRCR_NO_RADIO:
                    MessageDlg( "You cannot issue this command if you are not connected to a base radio.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    break;

                case TBaseRadioDevice::SRCR_IN_USE:
                    MessageDlg( "The selected channel is in use by another radio. Please try again with a different channel.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    break;

                default:
                    MessageDlg( "The command failed: there is a problem with the serial port.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    break;
            }
        }
    }
}


void __fastcall TLinkTiltSettingsDlg::AutoAssignPortsCBClick(TObject *Sender)
{
    // If we're auto-assigning ports, then disable everything
    if( AutoAssignPortsCB->Checked )
    {
        for( int iCtrl = 0; iCtrl < PortsGB->ControlCount; iCtrl++ )
        {
            TComboBox* pCombo = dynamic_cast<TComboBox*>( PortsGB->Controls[iCtrl] );

            if( pCombo != NULL )
            {
                // Disable the 'onclick' handler here so that it doesn't fire
                // and undo our work if the item index changes
                TNotifyEvent pOnComboClick = pCombo->OnClick;
                pCombo->OnClick = NULL;

                pCombo->ItemIndex = CT_SERIAL;  // serial
                pCombo->Enabled   = false;

                // Okay to restore click handler now
                pCombo->OnClick = pOnComboClick;
                continue;
            }

            TEdit* pEdit = dynamic_cast<TEdit*>( PortsGB->Controls[iCtrl] );

            if( pEdit != NULL )
                pEdit->Enabled   = false;

        }

        // Get the last saved settings
        COMMS_CFG radioCommCfg;
        COMMS_CFG deviceCommCfg;
        COMMS_CFG mgmtCommCfg;

        GetCommSettings( DCT_BASE, radioCommCfg );
        GetCommSettings( DCT_TM,   deviceCommCfg );
        GetCommSettings( DCT_MGMT, mgmtCommCfg );

        // Set the Port Numbers to that from the registry
        BaseRadioPortEdit->Text = radioCommCfg.portName;
        TMPortEdit->Text        = deviceCommCfg.portName;
        MgmtDevPortEdit->Text   = mgmtCommCfg.portName;

        // If any of the texts are empty, give them a default value
        if( ( radioCommCfg.portName.IsEmpty() ) || ( deviceCommCfg.portName.IsEmpty() ) || ( mgmtCommCfg.portName.IsEmpty() ) )
        {
            BaseRadioPortEdit->Text = IntToStr( BASESTN_CONTROL_PORT_OFFSET + 1 );
            TMPortEdit->Text        = IntToStr( BASESTN_TESTORK_RADIO_PORT_OFFSET + 1 );
            MgmtDevPortEdit->Text   = IntToStr( BASESTN_MGMT_PORT_OFFSET );
        }

        // When auto assigning, all ports are at 115200
        BaseRadioParamEdit->Text = "115200";
        TMParamEdit->Text        = "115200";
        MgmtDevParamEdit->Text   = "115200";
    }
    else
    {
        // Not auto-assigning. Enable all edits
        for( int iCtrl = 0; iCtrl < PortsGB->ControlCount; iCtrl++ )
        {
            TComboBox* pCombo = dynamic_cast<TComboBox*>( PortsGB->Controls[iCtrl] );

            if( pCombo != NULL )
            {
                pCombo->Enabled = true;
                continue;
            }

            TEdit* pEdit = dynamic_cast<TEdit*>( PortsGB->Controls[iCtrl] );

            if( pEdit != NULL )
            {
                pEdit->Enabled = true;
                pEdit->Color   = clWindow;
                continue;
            }
        }
    }
}


void __fastcall TLinkTiltSettingsDlg::BaseRadioPortTypeComboClick(TObject *Sender)
{
    if( BaseRadioPortTypeCombo->ItemIndex == CT_UNUSED )
    {
        BaseRadioPortEdit->Text     = "";
        BaseRadioParamEdit->Text    = "";
        BaseRadioPortEdit->Enabled  = false;
        BaseRadioParamEdit->Enabled = false;
    }
    else if( BaseRadioPortTypeCombo->ItemIndex == CT_UDP )
    {
        BaseRadioPortEdit->Text     = "";
        BaseRadioParamEdit->Text    = "62220";
        BaseRadioPortEdit->Enabled  = false;
        BaseRadioParamEdit->Enabled = true;
    }
    else
    {
        BaseRadioPortEdit->Enabled  = true;
        BaseRadioParamEdit->Enabled = true;
    }
}


void __fastcall TLinkTiltSettingsDlg::TMPortTypeComboClick(TObject *Sender)
{
    // Tilt Mgr port can never be unused
    if( TMPortTypeCombo->ItemIndex == CT_UNUSED )
    {
        TMPortTypeCombo->ItemIndex = CT_SERIAL;
    }
    else if( TMPortTypeCombo->ItemIndex == CT_UDP )
    {
        TMPortEdit->Text     = "";
        TMParamEdit->Text    = "62222";
        TMPortEdit->Enabled  = false;
        TMParamEdit->Enabled = true;
    }
    else
    {
        TMPortEdit->Enabled  = true;
        TMParamEdit->Enabled = true;
    }
}


void __fastcall TLinkTiltSettingsDlg::MgmtDevPortTypeComboClick(TObject *Sender)
{
    if( MgmtDevPortTypeCombo->ItemIndex == CT_UNUSED )
    {
        MgmtDevPortEdit->Text     = "";
        MgmtDevParamEdit->Text    = "";
        MgmtDevPortEdit->Enabled  = false;
        MgmtDevParamEdit->Enabled = false;
    }
    else if( MgmtDevPortTypeCombo->ItemIndex == CT_UDP )
    {
        MgmtDevPortEdit->Text     = "127.0.0.1";
        MgmtDevParamEdit->Text    = "62224";
        MgmtDevPortEdit->Enabled  = true;
        MgmtDevParamEdit->Enabled = true;
    }
    else
    {
        MgmtDevPortEdit->Enabled  = true;
        MgmtDevParamEdit->Enabled = true;
    }
}


void __fastcall TLinkTiltSettingsDlg::AccelAvgComboClick(TObject *Sender)
{
    SetAverageParams( (AVG_METHOD)AccelAvgCombo->ItemIndex, AccelParamLabel, AccelAvgParamEdit );
}


void __fastcall TLinkTiltSettingsDlg::CompassAvgComboClick(TObject *Sender)
{
    SetAverageParams( (AVG_METHOD)CompassAvgCombo->ItemIndex, CompassParamLabel, CompassAvgParamEdit );
}


void __fastcall TLinkTiltSettingsDlg::RSSIAvgComboClick(TObject *Sender)
{
    SetAverageParams( (AVG_METHOD)RSSIAvgCombo->ItemIndex, RSSIParamLabel, RSSIAvgParamEdit );
}

