object LinkTiltSettingsDlg: TLinkTiltSettingsDlg
  Left = 0
  Top = 0
  Caption = 'Tilt Manager System Settings'
  ClientHeight = 525
  ClientWidth = 698
  Color = clBtnFace
  Constraints.MinHeight = 564
  Constraints.MinWidth = 710
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    698
    525)
  PixelsPerInch = 96
  TextHeight = 13
  object PgCtrl: TPageControl
    Left = 8
    Top = 8
    Width = 684
    Height = 478
    ActivePage = MiscSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object CommsSheet: TTabSheet
      Caption = 'Communications'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        676
        450)
      object DevConnPgCtrl: TPageControl
        Left = 3
        Top = 3
        Width = 670
        Height = 444
        ActivePage = TMSheet
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object TMSheet: TTabSheet
          Caption = 'Tilt Manager'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            662
            416)
          object DataLoggingGB: TGroupBox
            Left = 312
            Top = 8
            Width = 339
            Height = 81
            Anchors = [akTop, akRight]
            Caption = ' Data Logging '
            TabOrder = 1
            object EnableRFCLogCB: TCheckBox
              Left = 14
              Top = 24
              Width = 187
              Height = 17
              Caption = 'Enable RFC Data Logging'
              TabOrder = 0
            end
            object RFCLogFileNameEdit: TEdit
              Left = 13
              Top = 45
              Width = 273
              Height = 21
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 1
            end
            object BrowseRFCLogFileBtn: TButton
              Left = 295
              Top = 42
              Width = 33
              Height = 25
              Caption = '...'
              TabOrder = 2
              OnClick = BrowseRFCLogFileBtnClick
            end
          end
          object DiagnosticsGB: TGroupBox
            Left = 8
            Top = 8
            Width = 295
            Height = 400
            Anchors = [akLeft, akTop, akRight, akBottom]
            Caption = ' Diagnostics '
            TabOrder = 0
            DesignSize = (
              295
              400)
            object Label7: TLabel
              Left = 12
              Top = 16
              Width = 43
              Height = 13
              Caption = 'Last RFC'
            end
            object LastRFCEditor: TValueListEditor
              Left = 12
              Top = 32
              Width = 271
              Height = 357
              Anchors = [akLeft, akTop, akRight, akBottom]
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
              TabOrder = 0
              TitleCaptions.Strings = (
                'Property'
                'Value')
              OnSelectCell = ListEditorSelectCell
              ColWidths = (
                155
                110)
            end
          end
          object GroupBox1: TGroupBox
            Left = 312
            Top = 97
            Width = 339
            Height = 54
            Anchors = [akTop, akRight]
            Caption = ' Settings '
            TabOrder = 2
            object Label2: TLabel
              Left = 14
              Top = 23
              Width = 110
              Height = 13
              Caption = 'Radio Channel Timeout'
            end
            object Label3: TLabel
              Left = 295
              Top = 23
              Width = 29
              Height = 13
              Caption = '(secs)'
            end
            object RadioChanTimeoutEdit: TEdit
              Left = 165
              Top = 20
              Width = 121
              Height = 21
              TabOrder = 0
              Text = '30'
            end
          end
        end
        object BaseRadioSheet: TTabSheet
          Caption = 'Base Radio'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            662
            416)
          object BRDiagnosticsGB: TGroupBox
            Left = 8
            Top = 8
            Width = 295
            Height = 400
            Anchors = [akLeft, akTop, akRight, akBottom]
            Caption = ' Diagnostics '
            TabOrder = 0
            DesignSize = (
              295
              400)
            object Label37: TLabel
              Left = 12
              Top = 16
              Width = 54
              Height = 13
              Caption = 'Last Status'
            end
            object LastStatusEditor: TValueListEditor
              Left = 12
              Top = 32
              Width = 271
              Height = 357
              Anchors = [akLeft, akTop, akRight, akBottom]
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
              TabOrder = 0
              TitleCaptions.Strings = (
                'Property'
                'Value')
              OnSelectCell = ListEditorSelectCell
              ColWidths = (
                155
                110)
            end
          end
          object BRCmdGB: TGroupBox
            Left = 312
            Top = 8
            Width = 339
            Height = 66
            Anchors = [akTop, akRight]
            Caption = ' Commands '
            TabOrder = 1
            object Label39: TLabel
              Left = 14
              Top = 31
              Width = 74
              Height = 13
              Caption = 'Set Radio Chan'
            end
            object SendChangeChanBtn: TButton
              Left = 253
              Top = 26
              Width = 75
              Height = 25
              Caption = 'Send'
              TabOrder = 2
              OnClick = SendChangeChanBtnClick
            end
            object BRChanCombo: TComboBox
              Left = 206
              Top = 28
              Width = 41
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 1
              Text = '11'
              Items.Strings = (
                '11'
                '15'
                '20'
                '25')
            end
            object BRRadNbrCombo: TComboBox
              Left = 98
              Top = 28
              Width = 102
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 0
              Text = 'Tilt Mgr Chan 1'
              Items.Strings = (
                'Tilt Mgr Chan 1'
                'Tilt Mgr Chan 2')
            end
          end
        end
        object PortsSheet: TTabSheet
          Caption = 'Ports'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PortsGB: TGroupBox
            Left = 8
            Top = 8
            Width = 445
            Height = 157
            Caption = ' Ports '
            TabOrder = 0
            object Label19: TLabel
              Left = 16
              Top = 67
              Width = 59
              Height = 13
              Caption = 'Tilt Manager'
            end
            object Label21: TLabel
              Left = 16
              Top = 95
              Width = 85
              Height = 13
              Caption = 'Management Port'
            end
            object Label22: TLabel
              Left = 232
              Top = 17
              Width = 69
              Height = 13
              Caption = 'Port / Address'
            end
            object Label23: TLabel
              Left = 338
              Top = 17
              Width = 50
              Height = 13
              Caption = 'Parameter'
            end
            object Label24: TLabel
              Left = 115
              Top = 17
              Width = 47
              Height = 13
              Caption = 'Port Type'
            end
            object Label20: TLabel
              Left = 16
              Top = 39
              Width = 53
              Height = 13
              Caption = 'Base Radio'
            end
            object AutoAssignPortsCB: TCheckBox
              Left = 115
              Top = 127
              Width = 169
              Height = 17
              Caption = 'Automatically Assign Ports'
              TabOrder = 9
              OnClick = AutoAssignPortsCBClick
            end
            object TMPortTypeCombo: TComboBox
              Left = 115
              Top = 64
              Width = 95
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 3
              Text = 'Serial'
              OnClick = TMPortTypeComboClick
              Items.Strings = (
                'Serial'
                'UDP'
                'No Device')
            end
            object MgmtDevPortTypeCombo: TComboBox
              Left = 115
              Top = 92
              Width = 95
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 6
              Text = 'Serial'
              OnClick = MgmtDevPortTypeComboClick
              Items.Strings = (
                'Serial'
                'UDP'
                'No Device')
            end
            object TMPortEdit: TEdit
              Left = 232
              Top = 64
              Width = 85
              Height = 21
              TabOrder = 4
              Text = '1'
            end
            object TMParamEdit: TEdit
              Left = 338
              Top = 64
              Width = 85
              Height = 21
              TabOrder = 5
              Text = '115200'
            end
            object MgmtDevPortEdit: TEdit
              Left = 232
              Top = 92
              Width = 85
              Height = 21
              TabOrder = 7
              Text = '1'
            end
            object MgmtDevParamEdit: TEdit
              Left = 338
              Top = 91
              Width = 85
              Height = 21
              TabOrder = 8
              Text = '115200'
            end
            object BaseRadioPortTypeCombo: TComboBox
              Left = 115
              Top = 36
              Width = 95
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 0
              Text = 'Serial'
              OnClick = BaseRadioPortTypeComboClick
              Items.Strings = (
                'Serial'
                'UDP'
                'No Device')
            end
            object BaseRadioPortEdit: TEdit
              Left = 232
              Top = 36
              Width = 85
              Height = 21
              TabOrder = 1
              Text = '1'
            end
            object BaseRadioParamEdit: TEdit
              Left = 338
              Top = 36
              Width = 85
              Height = 21
              TabOrder = 2
              Text = '115200'
            end
          end
        end
        object PLCSheet: TTabSheet
          Caption = 'PLC'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            662
            416)
          object MODBUSSettingsGB: TGroupBox
            Left = 8
            Top = 11
            Width = 641
            Height = 85
            Anchors = [akLeft, akTop, akRight]
            Caption = ' MODBUS Settings '
            TabOrder = 0
            object Label13: TLabel
              Left = 16
              Top = 24
              Width = 87
              Height = 13
              Caption = 'Listening TCP Port'
            end
            object Label14: TLabel
              Left = 16
              Top = 53
              Width = 74
              Height = 13
              Caption = 'Device Address'
            end
            object MODBUSPortEdit: TEdit
              Left = 128
              Top = 21
              Width = 121
              Height = 21
              NumbersOnly = True
              TabOrder = 0
              Text = '502'
            end
            object MODBUSDevIDEdit: TEdit
              Left = 128
              Top = 50
              Width = 121
              Height = 21
              NumbersOnly = True
              TabOrder = 1
              Text = '1'
            end
          end
          object MODBUSMapGB: TGroupBox
            Left = 8
            Top = 102
            Width = 641
            Height = 305
            Caption = ' MODBUS Register Map '
            TabOrder = 1
            DesignSize = (
              641
              305)
            object RegistersLV: TListView
              Left = 16
              Top = 21
              Width = 612
              Height = 270
              Anchors = [akLeft, akTop, akRight, akBottom]
              Columns = <
                item
                  Caption = 'Register #'
                  Width = 75
                end
                item
                  Caption = 'Description'
                  Width = 200
                end
                item
                  Caption = 'Value'
                  Width = 150
                end>
              ColumnClick = False
              GridLines = True
              TabOrder = 0
              ViewStyle = vsReport
              ExplicitHeight = 251
            end
          end
        end
      end
    end
    object CalSheet: TTabSheet
      Caption = 'Calibration'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        676
        450)
      object CalDataPgCtrl: TPageControl
        Left = 8
        Top = 8
        Width = 398
        Height = 394
        ActivePage = DevMemorySheet
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object DevMemorySheet: TTabSheet
          Caption = 'Device Memory'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            390
            366)
          object FormattedCalDataEditor: TValueListEditor
            Left = 8
            Top = 8
            Width = 374
            Height = 352
            Anchors = [akLeft, akTop, akRight, akBottom]
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
            TabOrder = 0
            TitleCaptions.Strings = (
              'Property'
              'Value')
            OnSelectCell = ListEditorSelectCell
            ColWidths = (
              211
              157)
          end
        end
        object RawDataSheet: TTabSheet
          Caption = 'Raw Data View'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            390
            366)
          object RawCalDataEditor: TValueListEditor
            Left = 8
            Top = 8
            Width = 374
            Height = 352
            Anchors = [akLeft, akTop, akRight, akBottom]
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
            TabOrder = 0
            TitleCaptions.Strings = (
              'Page'
              'Data')
            OnSelectCell = ListEditorSelectCell
            ColWidths = (
              76
              292)
          end
        end
      end
      object LoadCalDatBtn: TButton
        Left = 113
        Top = 413
        Width = 99
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Load From File'
        TabOrder = 1
        OnClick = LoadCalDatBtnClick
      end
      object SaveCalDatBtn: TButton
        Left = 7
        Top = 413
        Width = 99
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Save To File'
        TabOrder = 2
        OnClick = SaveCalDatBtnClick
      end
      object WriteToDevBtn: TButton
        Left = 200
        Top = 413
        Width = 99
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Write To Device'
        TabOrder = 3
        OnClick = WriteToDevBtnClick
      end
      object ReadFromDevBtn: TButton
        Left = 305
        Top = 413
        Width = 99
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Read From Device'
        TabOrder = 4
        OnClick = ReadFromDevBtnClick
      end
      object NewBattBtn: TButton
        Left = 419
        Top = 32
        Width = 99
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'New Battery'
        TabOrder = 5
        OnClick = NewBattBtnClick
      end
      object AngleFactorsGB: TGroupBox
        Left = 419
        Top = 72
        Width = 246
        Height = 121
        Caption = ' Angle Factors '
        TabOrder = 6
        object Label4: TLabel
          Left = 16
          Top = 24
          Width = 73
          Height = 13
          Caption = 'Reading to Use'
        end
        object Label5: TLabel
          Left = 16
          Top = 56
          Width = 75
          Height = 13
          Caption = 'Rotation Offset'
        end
        object Label12: TLabel
          Left = 16
          Top = 88
          Width = 86
          Height = 13
          Caption = 'Min Accel Reading'
        end
        object AngleAccelCombo: TComboBox
          Left = 122
          Top = 21
          Width = 111
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = 'Positive X'
          Items.Strings = (
            'Positive X'
            'Negative X'
            'Positive Y'
            'Negative Y')
        end
        object AngleRotnOffsetEdit: TEdit
          Left = 122
          Top = 53
          Width = 111
          Height = 21
          TabOrder = 1
          Text = '0'
        end
        object MinAccelValEdit: TEdit
          Left = 122
          Top = 85
          Width = 111
          Height = 21
          TabOrder = 2
          Text = '0.5'
        end
      end
    end
    object MiscSheet: TTabSheet
      Caption = 'Misc'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        676
        450)
      object JobDirGB: TGroupBox
        Left = 8
        Top = 8
        Width = 657
        Height = 54
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Job Directory '
        TabOrder = 0
        DesignSize = (
          657
          54)
        object JobDirEdit: TEdit
          Left = 14
          Top = 21
          Width = 591
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 0
        end
        object BrowseJobDirBtn: TButton
          Left = 613
          Top = 19
          Width = 33
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '...'
          TabOrder = 1
          OnClick = BrowseJobDirBtnClick
        end
      end
      object SysAdminPWGB: TGroupBox
        Left = 8
        Top = 68
        Width = 335
        Height = 139
        Caption = ' Sys Admin Password '
        TabOrder = 1
        object Label8: TLabel
          Left = 14
          Top = 24
          Width = 117
          Height = 13
          Caption = 'Enter current password:'
        end
        object Label9: TLabel
          Left = 14
          Top = 51
          Width = 102
          Height = 13
          Caption = 'Enter new password:'
        end
        object Label10: TLabel
          Left = 14
          Top = 78
          Width = 113
          Height = 13
          Caption = 'Confirm new password:'
        end
        object CurrPWEdit: TEdit
          Left = 148
          Top = 21
          Width = 171
          Height = 21
          PasswordChar = '*'
          TabOrder = 0
        end
        object NewPWEdit1: TEdit
          Left = 148
          Top = 48
          Width = 171
          Height = 21
          PasswordChar = '*'
          TabOrder = 1
        end
        object NewPWEdit2: TEdit
          Left = 148
          Top = 75
          Width = 171
          Height = 21
          PasswordChar = '*'
          TabOrder = 2
        end
        object SavePWBtn: TButton
          Left = 244
          Top = 103
          Width = 75
          Height = 25
          Caption = 'Save'
          TabOrder = 3
          OnClick = SavePWBtnClick
        end
      end
      object PriorityGB: TGroupBox
        Left = 8
        Top = 215
        Width = 335
        Height = 53
        Caption = ' Process Priority '
        TabOrder = 2
        object ProcPriorityCombo: TComboBox
          Left = 11
          Top = 21
          Width = 199
          Height = 21
          Style = csDropDownList
          TabOrder = 0
        end
      end
      object AvgingGB: TGroupBox
        Left = 8
        Top = 274
        Width = 335
        Height = 110
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Averaging '
        TabOrder = 3
        object Label1: TLabel
          Left = 14
          Top = 24
          Width = 59
          Height = 13
          Caption = 'Acceleration'
        end
        object AccelParamLabel: TLabel
          Left = 200
          Top = 24
          Width = 50
          Height = 13
          Caption = 'Parameter'
        end
        object Label6: TLabel
          Left = 14
          Top = 51
          Width = 43
          Height = 13
          Caption = 'Compass'
        end
        object Label11: TLabel
          Left = 14
          Top = 78
          Width = 23
          Height = 13
          Caption = 'RSSI'
        end
        object CompassParamLabel: TLabel
          Left = 200
          Top = 51
          Width = 50
          Height = 13
          Caption = 'Parameter'
        end
        object RSSIParamLabel: TLabel
          Left = 200
          Top = 78
          Width = 50
          Height = 13
          Caption = 'Parameter'
        end
        object AccelAvgCombo: TComboBox
          Left = 84
          Top = 21
          Width = 101
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = '1'
          OnClick = AccelAvgComboClick
          Items.Strings = (
            '1'
            '2'
            '4'
            '8'
            '16'
            '32'
            '64'
            '128')
        end
        object AccelAvgParamEdit: TEdit
          Left = 270
          Top = 21
          Width = 49
          Height = 21
          TabOrder = 1
          Text = '1'
        end
        object CompassAvgCombo: TComboBox
          Left = 84
          Top = 48
          Width = 101
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 2
          Text = '1'
          OnClick = CompassAvgComboClick
          Items.Strings = (
            '1'
            '2'
            '4'
            '8'
            '16'
            '32'
            '64'
            '128')
        end
        object CompassAvgParamEdit: TEdit
          Left = 270
          Top = 48
          Width = 49
          Height = 21
          TabOrder = 3
          Text = '1'
        end
        object RSSIAvgCombo: TComboBox
          Left = 84
          Top = 75
          Width = 101
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 4
          Text = '1'
          OnClick = RSSIAvgComboClick
          Items.Strings = (
            '1'
            '2'
            '4'
            '8'
            '16'
            '32'
            '64'
            '128')
        end
        object RSSIAvgParamEdit: TEdit
          Left = 270
          Top = 75
          Width = 49
          Height = 21
          TabOrder = 5
          Text = '1'
        end
      end
    end
  end
  object OKBtn: TButton
    Left = 525
    Top = 494
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'O&K'
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 615
    Top = 494
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object SaveLogDlg: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All File (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Log File As...'
    Left = 608
    Top = 8
  end
  object RFCPollTimer: TTimer
    Enabled = False
    OnTimer = RFCPollTimerTimer
    Left = 552
    Top = 8
  end
  object SaveCalDataDlg: TSaveDialog
    DefaultExt = 'ini'
    Filter = 'Calibrated Data Files (*.ini)|*.ini|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Calibration Data to File'
    Left = 448
    Top = 8
  end
  object OpenCalDataDlg: TOpenDialog
    DefaultExt = 'ini'
    Filter = 'Calibrated Data Files (*.ini)|*.ini|All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Load Calibration Data from File'
    Left = 504
    Top = 8
  end
end
