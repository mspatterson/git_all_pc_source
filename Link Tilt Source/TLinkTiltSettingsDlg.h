#ifndef TLinkTiltSettingsDlgH
#define TLinkTiltSettingsDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.Menus.hpp>

#include "TCommPoller.h"
#include "TModbusDevice.h"
#include "UnitsOfMeasure.h"


class TLinkTiltSettingsDlg : public TForm
{
__published:    // IDE-managed Components
    TPageControl *PgCtrl;
    TButton *OKBtn;
    TButton *CancelBtn;
    TTabSheet *CommsSheet;
    TTabSheet *MiscSheet;
    TSaveDialog *SaveLogDlg;
    TGroupBox *JobDirGB;
    TEdit *JobDirEdit;
    TButton *BrowseJobDirBtn;
    TGroupBox *SysAdminPWGB;
    TLabel *Label8;
    TLabel *Label9;
    TLabel *Label10;
    TEdit *CurrPWEdit;
    TEdit *NewPWEdit1;
    TEdit *NewPWEdit2;
    TButton *SavePWBtn;
    TTimer *RFCPollTimer;
    TPageControl *DevConnPgCtrl;
    TTabSheet *TMSheet;
    TTabSheet *BaseRadioSheet;
    TGroupBox *DataLoggingGB;
    TCheckBox *EnableRFCLogCB;
    TEdit *RFCLogFileNameEdit;
    TButton *BrowseRFCLogFileBtn;
    TGroupBox *DiagnosticsGB;
    TLabel *Label7;
    TValueListEditor *LastRFCEditor;
    TGroupBox *BRDiagnosticsGB;
    TLabel *Label37;
    TGroupBox *BRCmdGB;
    TButton *SendChangeChanBtn;
    TComboBox *BRChanCombo;
    TLabel *Label39;
    TValueListEditor *LastStatusEditor;
    TComboBox *BRRadNbrCombo;
    TGroupBox *PriorityGB;
    TComboBox *ProcPriorityCombo;
    TGroupBox *AvgingGB;
    TTabSheet *PortsSheet;
    TGroupBox *PortsGB;
    TLabel *Label19;
    TCheckBox *AutoAssignPortsCB;
    TLabel *Label21;
    TLabel *Label22;
    TLabel *Label23;
    TComboBox *TMPortTypeCombo;
    TComboBox *MgmtDevPortTypeCombo;
    TEdit *TMPortEdit;
    TEdit *TMParamEdit;
    TEdit *MgmtDevPortEdit;
    TEdit *MgmtDevParamEdit;
    TLabel *Label24;
    TLabel *Label20;
    TComboBox *BaseRadioPortTypeCombo;
    TEdit *BaseRadioPortEdit;
    TEdit *BaseRadioParamEdit;
    TLabel *Label1;
    TComboBox *AccelAvgCombo;
    TEdit *AccelAvgParamEdit;
    TTabSheet *CalSheet;
    TPageControl *CalDataPgCtrl;
    TTabSheet *DevMemorySheet;
    TValueListEditor *FormattedCalDataEditor;
    TTabSheet *RawDataSheet;
    TValueListEditor *RawCalDataEditor;
    TButton *LoadCalDatBtn;
    TButton *SaveCalDatBtn;
    TButton *WriteToDevBtn;
    TButton *ReadFromDevBtn;
    TButton *NewBattBtn;
    TSaveDialog *SaveCalDataDlg;
    TOpenDialog *OpenCalDataDlg;
    TGroupBox *GroupBox1;
    TLabel *Label2;
    TEdit *RadioChanTimeoutEdit;
    TLabel *Label3;
    TLabel *AccelParamLabel;
    TLabel *Label6;
    TComboBox *CompassAvgCombo;
    TEdit *CompassAvgParamEdit;
    TLabel *Label11;
    TComboBox *RSSIAvgCombo;
    TEdit *RSSIAvgParamEdit;
    TLabel *CompassParamLabel;
    TLabel *RSSIParamLabel;
    TGroupBox *AngleFactorsGB;
    TLabel *Label4;
    TLabel *Label5;
    TLabel *Label12;
    TComboBox *AngleAccelCombo;
    TEdit *AngleRotnOffsetEdit;
    TEdit *MinAccelValEdit;
    TTabSheet *PLCSheet;
    TGroupBox *MODBUSSettingsGB;
    TGroupBox *MODBUSMapGB;
    TListView *RegistersLV;
    TLabel *Label13;
    TLabel *Label14;
    TEdit *MODBUSPortEdit;
    TEdit *MODBUSDevIDEdit;
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall CommTypeClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall BrowseRFCLogFileBtnClick(TObject *Sender);
    void __fastcall SavePWBtnClick(TObject *Sender);
    void __fastcall BrowseJobDirBtnClick(TObject *Sender);
    void __fastcall RFCPollTimerTimer(TObject *Sender);
    void __fastcall ListEditorSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
    void __fastcall SaveCalDatBtnClick(TObject *Sender);
    void __fastcall LoadCalDatBtnClick(TObject *Sender);
    void __fastcall WriteToDevBtnClick(TObject *Sender);
    void __fastcall ReadFromDevBtnClick(TObject *Sender);
    void __fastcall SendChangeChanBtnClick(TObject *Sender);
    void __fastcall NewBattBtnClick(TObject *Sender);
    void __fastcall AutoAssignPortsCBClick(TObject *Sender);
    void __fastcall BaseRadioPortTypeComboClick(TObject *Sender);
    void __fastcall TMPortTypeComboClick(TObject *Sender);
    void __fastcall MgmtDevPortTypeComboClick(TObject *Sender);
    void __fastcall AccelAvgComboClick(TObject *Sender);
    void __fastcall CompassAvgComboClick(TObject *Sender);
    void __fastcall RSSIAvgComboClick(TObject *Sender);

private:
    TCommPoller*   m_poller;
    TModbusDevice* m_modbusObj;

    DWORD        m_dwSettingsChanged;

    UNITS_OF_MEASURE m_uomType;

    typedef struct {
        TComboBox* pTypeCombo;
        TEdit*     pPortNbrEdit;
        TEdit*     pParamEdit;
    } COMMS_SHEET_CTRLS;

    COMMS_SHEET_CTRLS m_TMCommsCtrls;
    COMMS_SHEET_CTRLS m_BaseCommsCtrls;
    COMMS_SHEET_CTRLS m_MgmtCommsCtrls;

    struct {
        COMMS_CFG   tmCommCfg;
        COMMS_CFG   baseCommCfg;
        COMMS_CFG   mgmtCommCfg;
        bool        bAutoAssignPorts;
        bool        bEnableRFCLog;
        String      sRFCLogFileName;
        DWORD       dwRadioChanTimeout;
        WORD        wMODBUSIPPort;         // Used for UDP / TCP comms. Opens a listening socket on that port number.
        BYTE        byMODBUSDevAddr;       // Slave or Unit ID
    } m_OrigCommsSettings;

    struct {
        DWORD            dwProcPriority;
        int              iSensorAlpha;
        int              iSamplingRate;
        AVERAGING_PARAMS compassAvgParams;
        AVERAGING_PARAMS accelAvgParams;
        AVERAGING_PARAMS RSSIAvgParams;
    } m_OrigMiscSettings;

    struct {
        ANGLE_FACTORS   angleFactors;
    } m_OrigCalSettings;

    void         SetCommsCtrls( const COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls );
    TWinControl* GetCommsCtrls(       COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls );

    void SetAverageControls( const AVERAGING_PARAMS& avgParams, TComboBox* pTypeCombo, TLabel* pParamLabel, TEdit* pParamEdit );
    bool GetAverageControls(       AVERAGING_PARAMS& avgParams, TComboBox* pTypeCombo, TLabel* pParamLabel, TEdit* pParamEdit );

    void SetAverageParams( AVG_METHOD aMethod, TLabel* pParamLabel, TEdit* pParamEdit );

    void LoadCommsPage( void );
    void LoadCalPage( void );
    void LoadMiscPage( void );

    TWinControl* CheckCommsPage( void );
    TWinControl* CheckCalPage( void );
    TWinControl* CheckMiscPage( void );

    // All controls must be valid when calling the following 'Save' functions
    DWORD SaveCommsPage( void );
    DWORD SaveCalPage( void );
    DWORD SaveMiscPage( void );

    void RefreshMODBUSRegMap( void );

public:
    __fastcall TLinkTiltSettingsDlg(TComponent* Owner);

    __property TCommPoller*   PollObject      = { read = m_poller,    write = m_poller    };
    __property TModbusDevice* MODBUSObject    = { read = m_modbusObj, write = m_modbusObj };
    __property DWORD          SettingsChanged = { read = m_dwSettingsChanged };

    typedef enum {
        COMMS_SETTINGS_CHANGED = 0x0001,
        MISC_SETTINGS_CHANGED  = 0x0002,
        CAL_SETTINGS_CHANGED   = 0x0004,
        PLC_SETTINGS_CHANGED   = 0x0008,
    } SETTINGS_CHANGED;

    // This is not an auto-created form. Must call the static ShowDlg() method
    // to construct, show, and then destroy the dialog.
    static DWORD ShowDlg( TCommPoller* pPoller, TModbusDevice* pModbusObj );

};

extern PACKAGE TLinkTiltSettingsDlg *LinkTiltSettingsDlg;

#endif
