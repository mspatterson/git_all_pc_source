#include <vcl.h>
#pragma hdrstop

#include "TProjectLogDlg.h"
#include "Logging.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TProjectLogForm *ProjectLogForm;


__fastcall TProjectLogForm::TProjectLogForm(TComponent* Owner) : TForm(Owner)
{
    // Hook the logging event handler
    SetLogEventHandler( OnLogMsgEvent );
}


void TProjectLogForm::ViewLog( void )
{
    Show();
}


void __fastcall TProjectLogForm::CloseBtnClick(TObject *Sender)
{
    Close();
}


void __fastcall TProjectLogForm::PrintBtnClick(TObject *Sender)
{
    TRichEdit* pRichEdit = new TRichEdit( Handle );

    try
    {
        pRichEdit->Lines->Assign( LogMemo->Lines );

        if( PrinterSetupDlg->Execute() )
        {
            String sJobName = "Link Tilt Manager System Log";

            pRichEdit->Print( sJobName );
        }
    }
    catch( ... )
    {
        MessageDlg( "An error occurred while printing the log.", mtError, TMsgDlgButtons() << mbOK, 0 );
    }

    delete pRichEdit;
}


void __fastcall TProjectLogForm::OnLogMsgEvent( TObject* Sender, const String& sLogMsg )
{
    while( LogMemo->Lines->Count > 1000 )
        LogMemo->Lines->Delete( 0 );

    LogMemo->Lines->Add( Now().TimeString() + ": " + sLogMsg );
}

