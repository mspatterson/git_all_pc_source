object ProjectLogForm: TProjectLogForm
  Left = 0
  Top = 0
  Caption = 'Project Log'
  ClientHeight = 401
  ClientWidth = 589
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    589
    401)
  PixelsPerInch = 96
  TextHeight = 13
  object PrintBtn: TButton
    Left = 8
    Top = 368
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Print'
    TabOrder = 0
    OnClick = PrintBtnClick
  end
  object CloseBtn: TButton
    Left = 506
    Top = 368
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 1
    OnClick = CloseBtnClick
  end
  object LogMemo: TMemo
    Left = 8
    Top = 8
    Width = 573
    Height = 354
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object PrinterSetupDlg: TPrinterSetupDialog
    Left = 504
    Top = 24
  end
end
