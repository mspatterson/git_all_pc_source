//---------------------------------------------------------------------------
#ifndef TProjectLogDlgH
#define TProjectLogDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
class TProjectLogForm : public TForm
{
__published:	// IDE-managed Components
    TButton *PrintBtn;
    TButton *CloseBtn;
    TPrinterSetupDialog *PrinterSetupDlg;
    TMemo *LogMemo;
    void __fastcall PrintBtnClick(TObject *Sender);
    void __fastcall CloseBtnClick(TObject *Sender);

private:

    // Logging support
    void __fastcall OnLogMsgEvent( TObject* Sender, const String& sLogMsg );

public:
    __fastcall TProjectLogForm(TComponent* Owner);

    void ViewLog( void );
};
//---------------------------------------------------------------------------
extern PACKAGE TProjectLogForm *ProjectLogForm;
//---------------------------------------------------------------------------
#endif
