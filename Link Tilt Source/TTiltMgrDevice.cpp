//************************************************************************
//
//  TTiltMgrDevice.h: this class interfaces with the TEC board using
//       tilt management firmware.
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "TTiltMgrDevice.h"
#include "CUDPPort.h"
#include "ApplUtils.h"

#pragma package(smart_init)


//
// Private Functions
//

static const DWORD  RxBuffLen  = 2048;

static const WORD   StartupRFCInterval    =  100;   // When initializing link, start with fast RFC
static const WORD   StartupRFCTimeout     =  250;   // rate with a long response timeout.
static const WORD   StandbyRFCInterval    = 1000;
static const WORD   StandbyRFCTimeout     =  100;
static const WORD   StreamTimeoutInterval =  120;   // Device will stop streaming this many seconds after last start command

static const DWORD  MSecsPerPktTick       =   10;   // Each count in the pkt header equals this many msecs

static const int    PktSanityTolerance    =    2;   // Ensures packets are sequential

static const String EmptyValue( "---" );


//
// TEC EEPROM Data Definitions
//

// Each area of memory is represented by its own structure. The first 8 pages
// are for manufacturing and other 'public' information. The second 8 pages
// are reserved for internal use by the WTTTS microcontroller. The next 15
// pages are available for host-controlled calibration data. The last page
// contains host-controlled calibration data version control.

#define CURR_CAL_VERSION  1
#define CURR_CAL_REV      0

typedef struct {
    WORD  battCapacity;
    BYTE  battType;
} TILT_CAL_DATA_STRUCT_V1_0;

static const TILT_CAL_DATA_STRUCT_V1_0 defaultTiltCalData = {
    /* battery capacity  */    0,     // in mAh
    /* battery type      */    0,     // 0 = unknown battery type
};



static float AccelRawToG( INT16 iReading )
{
    // The reading is a 14-bit signed value shifted to the upper bits
    // representing 0.488 mG per count.
    if( iReading & 0x8000 )
        iReading = ( iReading >> 2 ) | 0xC000;
    else
        iReading = ( iReading >> 2 ) & 0x3FFF;

    float fReading = iReading;

    return fReading * 0.488 / 1000;
}


static float CompassRawToDeg( INT16 iReading )
{
    // For now...
    // TODO
    return (float)iReading;
}


//
// Class Implementation
//

const String TTiltMgrDevice::CalFactorCaptions[NBR_CALIBRATION_ITEMS] = {
    "Cal Version",
    "Cal Revision",
    "Serial Number",
    "Manufacture Info",
    "Manufacture Date",
    "Calibration Date",
    "Battery Type",
    "Battery Capacity",
};


__fastcall TTiltMgrDevice::TTiltMgrDevice( void )
{
    // Init state vars
    m_linkState = LS_CLOSED;

    // Init rx control vars
    m_rxBuffer    = new BYTE[RxBuffLen];
    m_rxBuffCount = 0;

    memset( &m_lastRFC, 0, sizeof( m_lastRFC ) );

    ClearStats();

    m_port.portType       = CT_UNUSED;
    m_port.connResult     = CCommObj::ERR_NONE;
    m_port.connResultText = "(closed)";

    m_portLost = false;

    // UDP control vars
    m_bUsingUDP   = false;
    m_sUDPDevAddr = "";
    m_wUDPDevPort = 0;

    // Set time param defaults
    m_timeParams.rfcRate        = DEFAULT_RFC_RATE;
    m_timeParams.rfcTimeout     = DEFAULT_RFC_TIMEOUT;
    m_timeParams.streamRate     = DEFAULT_STREAM_RATE;
    m_timeParams.streamTimeout  = DEFAULT_STREAM_TIMEOUT;
    m_timeParams.pairingTimeout = DEFAULT_PAIRING_TIMEOUT;

    // Set the default packet timeout
    m_tPacketTimeout = DEFAULT_PACKET_TIMEOUT;

    // Only newer units have a device ID
    m_devID = "";

    // Create default averaging for now
    for( int iAvg = 0; iAvg < eNbr_Avg_Vars; iAvg++ )
        m_avgValue[iAvg] = new TNoAverage();

    // Unknown battery level default
    m_battLevel = eBL_Unknown;

    // Init command cache
    for( int iCachedItem = 0; iCachedItem < NBR_TEC_CMD_TYPES; iCachedItem++ )
        InitTECCmdPkt( (TEC_CMD_TYPE)iCachedItem );

    // Init battery averaging - averaging is hard-coded for now
    m_batteryLevel.fLastBattVolts = 0.0;
    m_batteryLevel.iLastBattUsed  = 0;
    m_batteryLevel.avgBattVolts   = new TExpAverage( 100, 0 );
    m_batteryLevel.avgBattUsed    = new TExpAverage( 100, 0 );
    m_batteryLevel.fValidThresh   = 0.0;   // Will be updated by RefreshSettings() call below
    m_batteryLevel.fMinOperThresh = 0.0;   // Will be updated by RefreshSettings() call below
    m_batteryLevel.fGoodThresh    = 0.0;   // Will be updated by RefreshSettings() call below

    // Clear other device info
    ClearConfigurationData();
    ClearHardwareInfo();

    // Refresh Settings
    RefreshSettings();
}


__fastcall TTiltMgrDevice::~TTiltMgrDevice()
{
    Disconnect();

    if( m_rxBuffer != NULL )
        delete [] m_rxBuffer;

    // Free battery averages
    delete m_batteryLevel.avgBattVolts;
    delete m_batteryLevel.avgBattUsed;
}


bool TTiltMgrDevice::Connect( const COMMS_CFG& portCfg )
{
    Disconnect();

    // Try to connect now
    if( portCfg.portType == CT_UNUSED )
    {
        m_port.connResultText = "port settings not specified";
        return false;
    }

    if( !CreateCommPort( portCfg, m_port ) )
        return false;

    // Note if we're using UDP
    if( portCfg.portType == CT_UDP )
        m_bUsingUDP = true;

    // Once the physical connection is established, we go into a hunt
    // mode. In this mode, we are looking for any type of response
    // from the sub. This tells us we have the correct physical link.
    m_linkState = LS_HUNTING;

    m_pktDiscardCount = 2;

    ClearConfigurationData();
    ClearHardwareInfo();

    // Set the initial timing parameters so that the configuration download
    // happens quickly.
    m_timeParams.rfcRate       = StartupRFCInterval;
    m_timeParams.rfcTimeout    = StartupRFCTimeout;
    m_timeParams.streamTimeout = StreamTimeoutInterval;

    return true;
}


void TTiltMgrDevice::Disconnect( void )
{
    // Reset our state first
    m_linkState = LS_CLOSED;

    ReleaseCommPort( m_port );
}


bool TTiltMgrDevice::GetDevIsRecving( void )
{
    if( time( NULL ) > m_port.stats.tLastPkt + m_tPacketTimeout )
        return false;

    return true;
}


bool TTiltMgrDevice::GetIsConnected( void )
{
    // Legacy sub only uses the data port
    if( m_port.portObj == NULL )
        return false;

    return m_port.portObj->isConnected;
}


void TTiltMgrDevice::RefreshSettings( void )
{
    // Called to refresh any registry-based settings. Causes those settings
    // to be re-read from the registry.
    BATT_THRESH_VALS battThresh;
    GetBatteryThresholds( battThresh );

    m_batteryLevel.fValidThresh   = battThresh.fReadingValidThresh;
    m_batteryLevel.fMinOperThresh = battThresh.fMinOperValue;
    m_batteryLevel.fGoodThresh    = battThresh.fGoodThresh;

    // Refresh averaging now. This will cause the values to reset
    AVERAGING_PARAMS avgParams;
    GetAveragingSettings( AT_ACCEL, avgParams );

    for( int iAvg = eAV_AccelX; iAvg <= eAV_AccelZ; iAvg++ )
    {
         if( m_avgValue[iAvg] != NULL )
         {
             delete m_avgValue[iAvg];

             m_avgValue[iAvg] = NULL;
         }

         m_avgValue[iAvg] = CreateAverage( avgParams );
    }

    GetAveragingSettings( AT_COMPASS, avgParams );

    for( int iAvg = eAV_CompassX; iAvg <= eAV_CompassZ; iAvg++ )
    {
         if( m_avgValue[iAvg] != NULL )
         {
             delete m_avgValue[iAvg];

             m_avgValue[iAvg] = NULL;
         }

         m_avgValue[iAvg] = CreateAverage( avgParams );
    }
}


bool TTiltMgrDevice::Update( void )
{
    // Update our state machine here. First, if we have no port, there is
    // nothing to do
    if( m_linkState == LS_CLOSED )
        return false;

    // The nature of the protocol is such that we need to process any requests
    // or events from the device first.
    DWORD bytesRxd;

    if( m_bUsingUDP )
        bytesRxd = ((CUDPPort*)m_port.portObj)->RecvFrom( &(m_rxBuffer[m_rxBuffCount]), RxBuffLen - m_rxBuffCount, m_sUDPDevAddr, m_wUDPDevPort );
    else
        bytesRxd = m_port.portObj->CommRecv( &(m_rxBuffer[m_rxBuffCount]), RxBuffLen - m_rxBuffCount );

    if( bytesRxd > 0 )
    {
        m_lastRxByteTime = GetTickCount();

        IncStat( m_port.stats.bytesRecv, bytesRxd );

        m_rxBuffCount += bytesRxd;
    }
    else
    {
        // No data received - check if any bytes in the buffer have gone stale
        if( HaveTimeout( m_lastRxByteTime, 250 ) )
            m_rxBuffCount = 0;
    }

    // After checking for received data, confirm the port is still open
    if( m_port.portObj->portLost )
    {
        // This is not good!
        m_portLost = true;
        Disconnect();

        m_linkState = LS_CLOSED;

        // Reset averaging data
        for( int iAvg = 0; iAvg < eNbr_Avg_Vars; iAvg++ )
            m_avgValue[iAvg]->Reset();

        return false;
    }

    // Have the parser check for any responses from the unit.
    bool bRFCPending = false;

    WTTTS_PKT_HDR      pktHdr;
    TESTORK_DATA_UNION pktData;

    if( HaveTesTORKRespPkt( m_rxBuffer, m_rxBuffCount, pktHdr, pktData ) )
    {
        IncStat( m_port.stats.respRecv );

        m_port.stats.tLastPkt = time( NULL );
        m_lastRxPktTime       = GetTickCount();

        // Process the received packet. Firstly, if we are in the hunting
        // state, receiving a packet means we are on the right channel.
        // The next thing we have to do is download the cal table.
        if( m_linkState == LS_HUNTING )
        {
            m_linkState = LS_CFG_DOWNLOAD;

            m_currCfgReqCycle = 1;
        }

        if( pktHdr.pktType == WTTTS_RESP_STREAM_DATA )
        {
            // We are receiving stream data. This can never happen in this application
            // Immediately send a stop command.
            SendTECCommand( TCT_STOP_DATA_COLLECTION );
        }
        else if( pktHdr.pktType == WTTTS_RESP_VER_PKT )
        {
            // Save the version info
            m_cfgStraps =  pktData.verPkt.hardwareSettings;

            memcpy( m_firmwareVer, pktData.verPkt.fwVer, WTTTS_FW_VER_LEN );
        }
        else if( pktHdr.pktType == WTTTS_RESP_CFG_DATA )
        {
            // This is valid only in the upload or download cfg states.
            // If downloading cfg data, save the data and mark the page
            // as such. If uploading, this is an ack to a previous
            // 'set cfg' command - verify the response.
            if( m_linkState == LS_CFG_DOWNLOAD )
            {
                if( pktData.cfgData.pageNbr < NBR_WTTTS_CFG_PAGES )
                {
                    m_cfgPgStatus[pktData.cfgData.pageNbr].havePage = true;

                    int pageOffset = pktData.cfgData.pageNbr * NBR_BYTES_PER_WTTTS_PAGE;

                    memcpy( &( m_rawCfgData[pageOffset] ), pktData.cfgData.pageData, NBR_BYTES_PER_WTTTS_PAGE );
                }
            }
            else if( m_linkState == LS_CFG_UPLOAD )
            {
                // The WTTTS will have echoed back in response to a previous
                // 'set' packet command. If the values echoed back match
                // the values in the table, then that item has been
                // successfully uploaded. First, check the response code.
                if( pktData.cfgData.result != WTTTS_PG_RESULT_SUCCESS )
                {
                    // WTTTS is indicating we cannot download this page.
                    // Clear the download pending bit and move on.
                    m_cfgPgStatus[pktData.cfgData.pageNbr].setPending = false;
                }
                else if( pktData.cfgData.pageNbr < NBR_WTTTS_CFG_PAGES )
                {
                    // WTTTS liked the page number. If our cached data matches
                    // the data echoed back, this page has been updated.
                    int   pageOffset = pktData.cfgData.pageNbr * NBR_BYTES_PER_WTTTS_PAGE;
                    BYTE* pgPointer  = &( m_rawCfgData[pageOffset] );

                    if( memcmp( pgPointer, pktData.cfgData.pageData, NBR_BYTES_PER_WTTTS_PAGE ) == 0 )
                        m_cfgPgStatus[pktData.cfgData.pageNbr].setPending = false;
                }
            }
        }
        else if( ( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_V1 ) || ( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_V2 ) )
        {
            // The WTTTS has requested a command from us. If we have
            // none pending, send a 'no command' response back.
            m_lastRFC.dwRFCTick = GetTickCount();
            m_lastRFC.tRFCTime  = time( NULL );
            m_lastRFC.dtRFC     = Now();

            // Before saving the RFC packet, check if we have a new device attached.
            // If so, we need to download its cal data (and will need to advise
            // clients that the device changed)
            if( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_V2 )
            {
                String sIDInRFC = DeviceIDToStr( pktData.rfcPktV2.deviceID );

                if( sIDInRFC.CompareIC( m_devID ) != 0 )
                {
                    // Advise client of the change
                    if( m_onDevChanged )
                        m_onDevChanged( this );

                    // Reload config data. This will only succeed if we are in an idle
                    // state. But that should be the case if we're receiving RFC's
                    ReloadCalDataFromDevice();

                    // Reset averaging data
                    for( int iAvg = 0; iAvg < eNbr_Avg_Vars; iAvg++ )
                        m_avgValue[iAvg]->Reset();

                    ClearBatteryVolts();

                    m_devID = sIDInRFC;
                }
            }

            // Packet looks good - convert generic form and log it
            ConvertRFCPktToGeneric( m_lastRFC.rfcPkt, pktHdr, pktData );
            LogRawRFCPacket( pktHdr, m_lastRFC.rfcPkt );

            bRFCPending = true;

            // We throw away a certain number of packets after a state
            // change - do not average those values
            if( m_pktDiscardCount > 0 )
            {
                m_pktDiscardCount--;
            }
            else
            {
                // Okay to average
                UpdateAveraging();
                UpdateBatteryVolts();

                if( m_onRFCRecvd )
                    m_onRFCRecvd( this );
            }
        }

        // After all the above processing is done, save this packet's header
        m_lastPktHdr = pktHdr;
    }

    // If a RFC is pending, check that the time params are up to date. If they
    // are not, a command will be sent to update those first.
    if( bRFCPending )
        bRFCPending = TimeParamsGood();

    // Now update the state machine
    switch( m_linkState )
    {
        case LS_IDLE:
            // If we have a RFC timeout, could have lost the link. Could also
            // scale back RFC interval based on the last time we got a command
            // from client modules.
            /* to do */
            break;

        case LS_HUNTING:
            // We are currently waiting to receive a command from the sub.
            // Packet reception is captured in receive processing above,
            // so there is nothing to do here.
            break;

        case LS_CFG_DOWNLOAD:
            // We are downloading the calibration table. Check if we are
            // done - switch to idle if so.
            if( bRFCPending )
            {
                bool bNeedPages = false;

                // Loop until we send a config request or until we know we have
                // all config pages
                while( true )
                {
                    // Scan the table for any pending entries. If there is
                    // one, request it. Otherwise, switch to the idle state.
                    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
                    {
                        if( m_cfgPgStatus[iPg].havePage == false )
                        {
                            // We still need a page
                            bNeedPages = true;

                            // Send a request for this page if we haven't already
                            // done so in this page request cycle
                            if ( m_cfgPgStatus[iPg].lastCfgReqCycle < m_currCfgReqCycle )
                            {
                                TESTORK_DATA_UNION pktPayload;
                                pktPayload.cfgRequest.pageNbr = iPg;

                                SendTECCommand( TCT_GET_CFG_PAGE, &pktPayload );

                                m_cfgPgStatus[iPg].lastCfgReqCycle = m_currCfgReqCycle;
                                bRFCPending = false;
                                break;
                            }
                        }
                    }

                    // Check if we've sent a request
                    if( !bRFCPending )
                        break;

                    // Check if we have all pages
                    if( !bNeedPages )
                        break;

                    // Step the page request cycle and start through the loop again.
                    // This will allow us to re-sent requests for pages that have
                    // not yet been received for some reason.
                    m_currCfgReqCycle += 1;
                }

                // If the RFC is pending, it means all pages have been downloaded.
                // Send the 'get version' command, and also put the link into the
                // idle state
                if( bRFCPending )
                {
                    // Calibration data all downloaded. Check the version of the
                    // cal data, and update if necessary.
                    CheckCalDataVersion();

                    // Send a request to get the verison info from the WTTTS.
                    // We don't implement a state for this, as the response
                    // consists of a single packet only.
                    SendTECCommand( TCT_REQUEST_VER_INFO );
                    m_linkState = LS_IDLE;

#ifdef DISCARD_RFU_ON_POWER_UP
                    // On first power up, the TEC will have had its sensors
                    // turned off. Skip some extra RFC packets to allow the
                    // sensors to power up
                    m_pktDiscardCount = 6;
#endif

                    // Also slow down the RFC rate
                    m_timeParams.rfcRate    = StandbyRFCInterval;
                    m_timeParams.rfcTimeout = StandbyRFCTimeout;
                }
            }
            break;

        case LS_CFG_UPLOAD:
            // We are uploading the calibration table. Check if we are
            // done - switch to idle if so.
            if( bRFCPending )
            {
                // Scan the table for any pending entries. If there is
                // one, request it. Otherwise, switch to the idle state.
                for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
                {
                    if( m_cfgPgStatus[iPg].setPending )
                    {
                        TESTORK_DATA_UNION pktPayload;

                        pktPayload.cfgData.pageNbr = iPg;
                        pktPayload.cfgData.result  = 0;

                        int   pageOffset = iPg * NBR_BYTES_PER_WTTTS_PAGE;
                        BYTE* pgPointer  = &( m_rawCfgData[pageOffset] );

                        memcpy( pktPayload.cfgData.pageData, pgPointer, NBR_BYTES_PER_WTTTS_PAGE );

                        SendTECCommand( TCT_SET_CFG_PAGE, &pktPayload );

                        bRFCPending = false;

                        break;
                    }
                }

                // If the RFC is pending, it means all pages have been set. Switch
                // to the idle state.
                if( bRFCPending )
                {
                    m_linkState = LS_IDLE;

                    // Also slow down the RFC rate
                    m_timeParams.rfcRate    = StandbyRFCInterval;
                    m_timeParams.rfcTimeout = StandbyRFCTimeout;
                }
            }
            break;

        case LS_STOPPING:
            // If we've received a RFC, then the streaming data has stopped
            if( bRFCPending )
                m_linkState = LS_IDLE;

            break;

        case LS_STREAMING:
            // Nothing extra needs to be done in this state
            break;
    }

    // If at this point the RFC is still pending, send a 'no command' response
    if( bRFCPending )
        SendTECCommand( TCT_NO_COMMAND );

    return true;
}


void TTiltMgrDevice::GetStats( PORT_STATS& portStats )
{
}


void TTiltMgrDevice::ClearStats( void )
{
    ClearPortStats( m_port.stats );
}


typedef enum {
    SI_DEV_STATE,
    SI_LAST_RFC_TIME,
    SI_RFC_TYPE,
    SI_TEMPERATURE,
    SI_BATT_VOLTS,
    SI_BATT_USED,
    SI_BATT_TYPE,
    SI_LAST_RESET,
    SI_RF_CHAN,
    SI_CURR_MODE,
    SI_RFC_RATE,
    SI_RFC_TIMEOUT,
    SI_STREAM_RATE,
    SI_STREAM_TIMEOUT,
    SI_PAIR_TIMEOUT,
    SI_TORQUE_045,
    SI_TORQUE_225,
    SI_TORQUE_045_R3,
    SI_TORQUE_225_R3,
    SI_TENSION_000,
    SI_TENSION_090,
    SI_TENSION_180,
    SI_TENSION_270,
    SI_COMPASS_X,
    SI_COMPASS_Y,
    SI_COMPASS_Z,
    SI_ACCEL_X,
    SI_ACCEL_Y,
    SI_ACCEL_Z,
    SI_DEV_ID,
    NBR_STATUS_ITEMS
} STATUS_ITEM;

static const String statusCaption[NBR_STATUS_ITEMS] = {
    "Link State",
    "Last RFC",
    "RFC Type",
    "Temperature",
    "Battery Volts",
    "Battery Used",
    "Battery Type",
    "Last Reset",
    "RF Channel",
    "Current Mode",
    "RFC Rate",
    "RFC Timeout",
    "Stream Rate",
    "Stream Timeout",
    "Pairing Timeout",
    "Compass X",
    "Compass Y",
    "Compass Z",
    "Accel X",
    "Accel Y",
    "Accel Z",
    "Device ID",
};


bool TTiltMgrDevice::ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, statusCaption, NBR_STATUS_ITEMS ) )
        return false;

    // Always show the device state. Show simulation differently though
    if( ( m_port.portType == CT_UDP ) && ( m_linkState == LS_IDLE ) )
        pValues->Strings[SI_DEV_STATE] = "Simulation (Save Disabled)";
    else
        pValues->Strings[SI_DEV_STATE] = GetDevStatus();

    // Make sure we have a RFC packet
    if( m_lastRFC.tRFCTime == 0 )
        return true;

    // Have a RFC - populate the value string list
    pValues->Strings[SI_LAST_RFC_TIME]  = LocalTimeString( m_lastRFC.tRFCTime );
    pValues->Strings[SI_TEMPERATURE]    = FloatToStrF( m_lastRFC.rfcPkt.fTemperature, ffFixed, 7, 1 );
    pValues->Strings[SI_BATT_VOLTS]     = FloatToStrF( m_batteryLevel.avgBattVolts->AverageValue, ffFixed, 7, 2 );
    pValues->Strings[SI_BATT_USED]      = IntToStr( (int)( m_batteryLevel.avgBattUsed->AverageValue ) );
    pValues->Strings[SI_RFC_RATE]       = IntToStr( m_lastRFC.rfcPkt.rfcRate )        + " msecs";
    pValues->Strings[SI_RFC_TIMEOUT]    = IntToStr( m_lastRFC.rfcPkt.rfcTimeout )     + " msecs";
    pValues->Strings[SI_STREAM_RATE]    = IntToStr( m_lastRFC.rfcPkt.streamRate )     + " msecs";
    pValues->Strings[SI_STREAM_TIMEOUT] = IntToStr( m_lastRFC.rfcPkt.streamTimeout )  + " secs";
    pValues->Strings[SI_PAIR_TIMEOUT]   = IntToStr( m_lastRFC.rfcPkt.pairingTimeout ) + " secs";

    switch( m_lastRFC.rfcPkt.battType )
    {
        case 1:   pValues->Strings[SI_BATT_TYPE] = "Lithium";   break;
        case 2:   pValues->Strings[SI_BATT_TYPE] = "NiMH";      break;
        default:  pValues->Strings[SI_BATT_TYPE] = "Type " + IntToStr( m_lastRFC.rfcPkt.battType );  break;
    }

    switch( m_lastRFC.rfcPkt.lastReset )
    {
        case 1:   pValues->Strings[SI_LAST_RESET] = "Power-up";   break;
        case 2:   pValues->Strings[SI_LAST_RESET] = "WDT";        break;
        case 3:   pValues->Strings[SI_LAST_RESET] = "Brown out";  break;
        default:  pValues->Strings[SI_LAST_RESET] = "Type " + IntToStr( m_lastRFC.rfcPkt.lastReset );  break;
    }

    switch( m_lastRFC.rfcPkt.currMode )
    {
        case 0:   pValues->Strings[SI_CURR_MODE] = "Entering deep sleep";  break;
        case 1:   pValues->Strings[SI_CURR_MODE] = "Normal";               break;
        case 2:   pValues->Strings[SI_CURR_MODE] = "Low power";            break;
        default:  pValues->Strings[SI_CURR_MODE] = "Type " + IntToStr( m_lastRFC.rfcPkt.lastReset );  break;
    }

    pValues->Strings[SI_RF_CHAN]   = IntToStr( m_lastRFC.rfcPkt.rfChannel );
    pValues->Strings[SI_COMPASS_X] = IntToStr( m_lastRFC.rfcPkt.compassX );
    pValues->Strings[SI_COMPASS_Y] = IntToStr( m_lastRFC.rfcPkt.compassY );
    pValues->Strings[SI_COMPASS_Z] = IntToStr( m_lastRFC.rfcPkt.compassZ );
    pValues->Strings[SI_ACCEL_X]   = IntToStr( m_lastRFC.rfcPkt.accelX );
    pValues->Strings[SI_ACCEL_Y]   = IntToStr( m_lastRFC.rfcPkt.accelY );
    pValues->Strings[SI_ACCEL_Z]   = IntToStr( m_lastRFC.rfcPkt.accelZ );

    // We are always using V2 packets with this firmware
    pValues->Strings[SI_RFC_TYPE]  = "V2";
    pValues->Strings[SI_DEV_ID]    = m_devID;

    return true;
}


bool TTiltMgrDevice::GetLastStatusPkt( time_t& tLastPkt, GENERIC_RFC_PKT& lastPkt )
{
    tLastPkt = m_lastRFC.tRFCTime;
    lastPkt  = m_lastRFC.rfcPkt;

    return( tLastPkt != 0 );
}


bool TTiltMgrDevice::GetLastDevReading( DEVICE_READING& lastReading )
{
    memset( &lastReading, 0, sizeof( DEVICE_READING ) );

    lastReading.compassX = m_avgValue[eAV_CompassX]->AverageValue;
    lastReading.compassY = m_avgValue[eAV_CompassY]->AverageValue;
    lastReading.compassZ = m_avgValue[eAV_CompassZ]->AverageValue;
    lastReading.accelX   = m_avgValue[eAV_AccelX  ]->AverageValue;
    lastReading.accelY   = m_avgValue[eAV_AccelY  ]->AverageValue;
    lastReading.accelZ   = m_avgValue[eAV_AccelZ  ]->AverageValue;

    return( m_lastRFC.tRFCTime != 0 );
}


void TTiltMgrDevice::InitTECCmdPkt( TEC_CMD_TYPE cmdType )
{
    // Clear the memory area first
    memset( &( m_cachedCmds[cmdType] ), 0, sizeof( CACHED_CMD_PKT ) );

    // Determine the packet type byte, and payload length (if any)
    BYTE* byBuff = m_cachedCmds[cmdType].byTxData;

    WTTTS_PKT_HDR* pHdr  = (WTTTS_PKT_HDR*)byBuff;

    // Setup header defaults
    pHdr->pktHdr    = TESTORK_HDR_TX;
    pHdr->pktType   = 0;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = 0;

    // Now initialize specific commands
    switch( cmdType )
    {
        case TCT_NO_COMMAND:
            pHdr->pktType = WTTTS_CMD_NO_CMD;
            pHdr->dataLen = 0;
            break;

        case TCT_START_DATA_COLLECTION:
            pHdr->pktType = WTTTS_CMD_START_STREAM;
            pHdr->dataLen = 0;
            break;

        case TCT_STOP_DATA_COLLECTION:
            pHdr->pktType = WTTTS_CMD_STOP_STREAM;
            pHdr->dataLen = 0;
            break;

        case TCT_REQUEST_VER_INFO:
            pHdr->pktType = WTTTS_CMD_QUERY_VER;
            pHdr->dataLen = 0;
            break;

        case TCT_GET_CFG_PAGE:
            pHdr->pktType = WTTTS_CMD_GET_CFG_DATA;
            pHdr->dataLen = sizeof( WTTTS_CFG_ITEM );
            break;

        case TCT_SET_CFG_PAGE:
            pHdr->pktType = WTTTS_CMD_SET_CFG_DATA;
            pHdr->dataLen = sizeof( WTTTS_CFG_DATA );
            break;

        case TCT_SET_RATE_CMD:
            pHdr->pktType = WTTTS_CMD_SET_RATE;
            pHdr->dataLen = sizeof( WTTTS_RATE_PKT );
            break;

        case TCT_ENTER_DEEP_SLEEP:
            pHdr->pktType = WTTTS_CMD_ENTER_DEEP_SLEEP;
            pHdr->dataLen = 0;
            break;

        case TCT_SET_RF_CHANNEL:
            pHdr->pktType = WTTTS_CMD_SET_RF_CHANNEL;
            pHdr->dataLen = sizeof( WTTTS_SET_CHAN_PKT );
            break;

        default:
            // Don't know this command!
            return;
    }

    // If we don't need a payload, can finish initializing the command.
    // Otherwise, we have to finish the command each time it is sent.
    if( pHdr->dataLen == 0 )
    {
        m_cachedCmds[cmdType].needsPayload = false;
        m_cachedCmds[cmdType].cmdLen       = MIN_TESTORK_PKT_LEN;

        BYTE byCRC = CalcWTTTSChecksum( byBuff, sizeof( WTTTS_PKT_HDR ) );

        byBuff[ sizeof( WTTTS_PKT_HDR ) ] = byCRC;
    }
    else
    {
        m_cachedCmds[cmdType].needsPayload = true;
        m_cachedCmds[cmdType].cmdLen       = MIN_TESTORK_PKT_LEN + pHdr->dataLen;
    }
}


bool TTiltMgrDevice::SendTECCommand( TEC_CMD_TYPE aCmd, const TESTORK_DATA_UNION* pPayload )
{
    if( !IsConnected )
        return false;

    // If the command needs a payload, add it on
    if( m_cachedCmds[aCmd].needsPayload )
    {
        if( pPayload == NULL )
            return false;

        BYTE* txBuff   = m_cachedCmds[aCmd].byTxData;
        int   crcCount = m_cachedCmds[aCmd].cmdLen - 1;

        TESTORK_DATA_UNION* pData = (TESTORK_DATA_UNION*)&( txBuff[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pData, pPayload, sizeof( TESTORK_DATA_UNION ) );

        txBuff[crcCount] = CalcWTTTSChecksum( txBuff, crcCount );
    }

    BYTE* pBuff       = m_cachedCmds[aCmd].byTxData;
    DWORD bytesToSend = m_cachedCmds[aCmd].cmdLen;

    DWORD bytesSent;

    if( m_bUsingUDP )
        bytesSent = ((CUDPPort*)m_port.portObj)->SendTo( pBuff, bytesToSend, m_sUDPDevAddr, m_wUDPDevPort );
    else
        bytesSent = m_port.portObj->CommSend( pBuff, bytesToSend );

    IncStat( m_port.stats.cmdsSent );
    IncStat( m_port.stats.bytesSent, bytesSent );

    return( bytesSent == bytesToSend );
}


UnicodeString TTiltMgrDevice::GetDevStatus( void )
{
    switch( m_linkState )
    {
        case LS_CLOSED:        return "Port closed";
        case LS_HUNTING:       return "Hunting";
        case LS_CFG_DOWNLOAD:  return "Downloading cfg data";
        case LS_CFG_UPLOAD:    return "Uploading cfg data";
        case LS_IDLE:          return "Active";
        case LS_STREAMING:     return "Streaming";
        case LS_STOPPING:      return "Stopping streaming";
    }

    // Fall through means unknown state!
    return "State " + IntToStr( m_linkState );
}


bool TTiltMgrDevice::GetDevIsIdle( void )
{
    return( m_linkState == LS_IDLE );
}


int TTiltMgrDevice::GetConfigUploadProgress( void )
{
    // Config upload progress is based on the host-writable pages (not all pages)
    int iTotalHostPgs = NBR_WTTTS_CFG_PAGES - WTTTS_CFG_FIRST_HOST_PAGE;

    int iPgsPending = 0;

    for( int iPg = WTTTS_CFG_FIRST_HOST_PAGE; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        if( m_cfgPgStatus[iPg].setPending )
            iPgsPending++;
    }

    return 100 - 100 * iPgsPending / iTotalHostPgs;
}


bool TTiltMgrDevice::InitPropertyList( TStringList* pCaptions, TStringList* pValues, const UnicodeString captionList[], int nbrCaptions )
{
    // Helper function that initializes the passed string lists used
    // to populate property editors.
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( !IsConnected )
        return false;

    // Add all of the captions first, with blank
    for( int iItem = 0; iItem < nbrCaptions; iItem++ )
    {
        pCaptions->Add( captionList[iItem] );
        pValues->Add( "" );
    }

    return true;
}


TAverage* TTiltMgrDevice::CreateAverage( const AVERAGING_PARAMS& avgParams )
{
    // Create a new averaging object based on the passed params.
    TAverage* newAvgObj;

    switch( avgParams.avgMethod )
    {
        case AM_BOXCAR:      newAvgObj = new TMovingAverage( (int)( avgParams.param + 0.5 ), avgParams.absVariance );  break;
        case AM_EXPONENTIAL: newAvgObj = new TExpAverage( avgParams.param, avgParams.absVariance );                    break;
        default:             newAvgObj = new TNoAverage();  break;
    }

    return newAvgObj;
}


void TTiltMgrDevice::UpdateAveraging( void )
{
    // Update averages with readings from last RFC. Need to convert the
    // native value in the RFC packet to a value in g's for accel and
    // degrees for compass.
    m_avgValue[eAV_AccelX  ]->AddValue( AccelRawToG( m_lastRFC.rfcPkt.accelX ) );
    m_avgValue[eAV_AccelY  ]->AddValue( AccelRawToG( m_lastRFC.rfcPkt.accelY ) );
    m_avgValue[eAV_AccelZ  ]->AddValue( AccelRawToG( m_lastRFC.rfcPkt.accelZ ) );
    m_avgValue[eAV_CompassX]->AddValue( CompassRawToDeg( m_lastRFC.rfcPkt.compassX ) );
    m_avgValue[eAV_CompassY]->AddValue( CompassRawToDeg( m_lastRFC.rfcPkt.compassY ) );
    m_avgValue[eAV_CompassZ]->AddValue( CompassRawToDeg( m_lastRFC.rfcPkt.compassZ ) );
}


void TTiltMgrDevice::ClearBatteryVolts( void )
{
    m_batteryLevel.avgBattVolts->Reset();
    m_batteryLevel.avgBattUsed->Reset();
}


void TTiltMgrDevice::UpdateBatteryVolts( void )
{
    // Assumes that an RFC packet was just received
    if( m_lastRFC.rfcPkt.fBattVoltage >= m_batteryLevel.fValidThresh )
    {
        m_batteryLevel.fLastBattVolts = m_lastRFC.rfcPkt.fBattVoltage;
        m_batteryLevel.iLastBattUsed  = m_lastRFC.rfcPkt.battUsed;

        m_batteryLevel.avgBattVolts->AddValue( m_lastRFC.rfcPkt.fBattVoltage );
        m_batteryLevel.avgBattUsed->AddValue ( m_lastRFC.rfcPkt.battUsed );

        if( m_batteryLevel.avgBattVolts->AverageValue < m_batteryLevel.fMinOperThresh )
            m_battLevel = eBL_Low;
        else if( m_batteryLevel.avgBattVolts->AverageValue < m_batteryLevel.fGoodThresh )
            m_battLevel = eBL_Med;
        else
            m_battLevel = eBL_OK;
    }
}


void TTiltMgrDevice::ClearHardwareInfo( void )
{
    m_cfgStraps = 0;
    memset( m_firmwareVer, 0, WTTTS_FW_VER_LEN );
}


bool TTiltMgrDevice::TimeParamsGood( void )
{
    // Check that the current time params match those in the last RFC packet.
    // Only call this function if a RFC has been received and no other command
    // has been sent. Return true if all timing params match, otherwise
    // return false.
    TESTORK_DATA_UNION pktUnion;
    memset( &pktUnion, 0, sizeof( TESTORK_DATA_UNION ) );

    bool paramsGood = true;

    if( m_lastRFC.rfcPkt.rfcRate != m_timeParams.rfcRate )
    {
        pktUnion.ratePkt.rateType = SRT_RFC_RATE;
        pktUnion.ratePkt.newValue = m_timeParams.rfcRate;

        paramsGood = false;
    }
    else if( m_lastRFC.rfcPkt.rfcTimeout != m_timeParams.rfcTimeout )
    {
        pktUnion.ratePkt.rateType = SRT_RFC_TIMEOUT;
        pktUnion.ratePkt.newValue = m_timeParams.rfcTimeout;

        paramsGood = false;
    }
    else if( m_lastRFC.rfcPkt.streamRate != m_timeParams.streamRate )
    {
        pktUnion.ratePkt.rateType = SRT_STREAM_RATE;
        pktUnion.ratePkt.newValue = m_timeParams.streamRate;

        paramsGood = false;
    }
    else if( m_lastRFC.rfcPkt.streamTimeout != m_timeParams.streamTimeout )
    {
        pktUnion.ratePkt.rateType = SRT_STREAM_TIMEOUT;
        pktUnion.ratePkt.newValue = m_timeParams.streamTimeout;

        paramsGood = false;
    }
    else if( m_lastRFC.rfcPkt.pairingTimeout != m_timeParams.pairingTimeout )
    {
        pktUnion.ratePkt.rateType = SRT_PAIR_TIMEOUT;
        pktUnion.ratePkt.newValue = m_timeParams.pairingTimeout;

        paramsGood = false;
    }

    if( paramsGood )
        return true;

    // Fall through means we have to change a rate
    SendTECCommand( TCT_SET_RATE_CMD, &pktUnion );

    return false;
}


//
// TEC Onboard Configuration Data Support Functions
//

bool TTiltMgrDevice::GetCalDataRaw( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( !IsConnected )
        return false;

    // Report the raw data as array of pages
    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        UnicodeString pageDataStr;

        BYTE* pData = &( m_rawCfgData[ iPg * NBR_BYTES_PER_WTTTS_PAGE ] );

        for( int iByte = 0; iByte < NBR_BYTES_PER_WTTTS_PAGE; iByte++ )
            pageDataStr = pageDataStr + IntToHex( pData[iByte], 2 ) + " ";

        pCaptions->Add( "Page " + IntToStr( iPg ) );
        pValues->Add( pageDataStr );
    }

    return true;
}


static UnicodeString CharsToStr( const char* szString, int maxLength )
{
    char* szTemp = new char[maxLength];

    memset( szTemp, 0, maxLength );

    for( int iChar = 0; iChar < maxLength - 1; iChar++ )
    {
        if( szString[iChar] == '\0' )
            break;

        szTemp[iChar] = szString[iChar];
    }

    UnicodeString sResult = szTemp;

    delete [] szTemp;

    return sResult;
}


static UnicodeString MfgDateToStr( BYTE relYear, BYTE month )
{
    int year = 2000 + (int)relYear;

    AnsiString sMonth;

    switch( month )
    {
        case 0:   sMonth = "Jan";  break;
        case 1:   sMonth = "Feb";  break;
        case 2:   sMonth = "Mar";  break;
        case 3:   sMonth = "Apr";  break;
        case 4:   sMonth = "May";  break;
        case 5:   sMonth = "Jun";  break;
        case 6:   sMonth = "Jul";  break;
        case 7:   sMonth = "Aug";  break;
        case 8:   sMonth = "Sep";  break;
        case 9:   sMonth = "Oct";  break;
        case 10:  sMonth = "Nov";  break;
        case 11:  sMonth = "Dec";  break;
        default:  sMonth = "Month " + IntToStr( month );
    }

    AnsiString sResult;
    sResult.printf( "%d %s", year, sMonth.c_str() );

    return UnicodeString( sResult );
}


bool TTiltMgrDevice::GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo )
{
    WTTTS_MFG_DATA_STRUCT* pMfgData = (WTTTS_MFG_DATA_STRUCT*)m_rawCfgData;

    const String notSet( "(not set)" );

    if( pMfgData->devSN != 0xFFFF )
        devHWInfo.serialNbr = IntToStr( pMfgData->devSN );
    else
        devHWInfo.serialNbr = notSet;

    if( pMfgData->szMfgInfo[0] != (char)0xFF )
        devHWInfo.mfgInfo = CharsToStr( pMfgData->szMfgInfo, 8 );
    else
        devHWInfo.mfgInfo = notSet;

    if( pMfgData->yearOfMfg != 0xFF )
        devHWInfo.mfgDate = MfgDateToStr( pMfgData->yearOfMfg, pMfgData->monthOfMfg );
    else
        devHWInfo.mfgDate = notSet;

    if( m_firmwareVer[0] != 0xFF )
        devHWInfo.fwRev = IntToStr( m_firmwareVer[0] ) + "." + IntToStr( m_firmwareVer[1] ) + "." + IntToStr( m_firmwareVer[2] ) + "." + IntToStr( m_firmwareVer[3] );
    else
        devHWInfo.fwRev = notSet;

    devHWInfo.cfgStraps = IntToHex( m_cfgStraps, 2 );

    return true;
}


bool TTiltMgrDevice::GetDeviceBatteryInfo( BATTERY_INFO& battInfo )
{
    if( !m_batteryLevel.avgBattVolts->IsValid )
        return false;

    TILT_CAL_DATA_STRUCT_V1_0* pCalData = (TILT_CAL_DATA_STRUCT_V1_0*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    BATTERY_TYPE battType = (BATTERY_TYPE)m_lastRFC.rfcPkt.battType;

    battInfo.battType        = battType;
    battInfo.battTypeText    = GetBattTypeText( battType );
    battInfo.initialCapacity = pCalData->battCapacity;
    battInfo.currVolts       = m_batteryLevel.fLastBattVolts;
    battInfo.currUsage       = m_batteryLevel.iLastBattUsed;
    battInfo.avgVolts        = m_batteryLevel.avgBattVolts->AverageValue;
    battInfo.avgUsage        = (int)( m_batteryLevel.avgBattUsed->AverageValue );

    return true;
}


bool TTiltMgrDevice::GetDeviceFWRev( BYTE& byMajor, BYTE& byMinor, BYTE& byRelease, BYTE& byBuild )
{
    byMajor   = m_firmwareVer[0];
    byMinor   = m_firmwareVer[1];
    byRelease = m_firmwareVer[2];
    byBuild   = m_firmwareVer[3];

    return true;
}


bool TTiltMgrDevice::GetDeviceMfgInfo( WTTTS_MFG_DATA_STRUCT& mfgInfo )
{
    WTTTS_MFG_DATA_STRUCT* pMfgData = (WTTTS_MFG_DATA_STRUCT*)m_rawCfgData;

    mfgInfo = *pMfgData;

    return true;
}


bool TTiltMgrDevice::GetDeviceCfgStraps( BYTE& byStraps )
{
    byStraps = m_cfgStraps;
    return true;
}


bool TTiltMgrDevice::GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, CalFactorCaptions, NBR_CALIBRATION_ITEMS ) )
        return false;

    WTTTS_MFG_DATA_STRUCT*      pMfgData = (WTTTS_MFG_DATA_STRUCT*)m_rawCfgData;
    TILT_CAL_DATA_STRUCT_V1_0*  pCalData = (TILT_CAL_DATA_STRUCT_V1_0*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*       pVerData = (WTTTS_CAL_VER_STRUCT*)     &( m_rawCfgData[ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    for( int iItem = 0; iItem < NBR_CALIBRATION_ITEMS; iItem++ )
    {
        UnicodeString sValue;

        switch( iItem )
        {
            case CI_VERSION:           sValue = IntToStr    ( pVerData->byCalVerNbr );     break;
            case CI_REVISION:          sValue = IntToStr    ( pVerData->byCalRevNbr );     break;
            case CI_SERIAL_NBR:        sValue = IntToStr    ( pMfgData->devSN );           break;
            case CI_MFG_INFO:          sValue = CharsToStr  ( pMfgData->szMfgInfo, 8 );    break;
            case CI_MFG_DATE:          sValue = MfgDateToStr( pMfgData->yearOfMfg, pMfgData->monthOfMfg );      break;
            case CI_BATTERY_CAPACITY:  sValue = IntToStr    ( pCalData->battCapacity );                          break;
            case CI_BATTERY_TYPE:      sValue = GetBattTypeText( (BATTERY_TYPE)pCalData->battType );            break;

            case CI_CALIBRATION_DATE:
                if( pVerData->calYear != 0xFF )
                {
                    sValue.printf( L"%d/%02d/%02d", (int)pVerData->calYear + 2000,
                                                    (int)pVerData->calMonth,
                                                    (int)pVerData->calDay );
                }
                else
                {
                    sValue = "----/--/--";
                }
                break;
        }

        pValues->Strings[iItem] = sValue;
    }

    return true;
}


bool TTiltMgrDevice::SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    // Although the user can try to pass values for all items in configuration
    // memory, we only change the values of items in the host config area.
    // Note that this function only updates the data cached in the host memory
    // area - it does not commit the data to the device.
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    TILT_CAL_DATA_STRUCT_V1_0* pCalData = (TILT_CAL_DATA_STRUCT_V1_0*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*       pVerData = (WTTTS_CAL_VER_STRUCT*)    &( m_rawCfgData[ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    try
    {
        for( int iParam = 0; iParam < pCaptions->Count; iParam++ )
        {
            UnicodeString sKey   = pCaptions->Strings[iParam];
            UnicodeString sValue = pValues->Strings[iParam];

            for( int paramIndex = 0; paramIndex < NBR_CALIBRATION_ITEMS; paramIndex++ )
            {
                if( sKey == CalFactorCaptions[paramIndex] )
                {
                    switch( paramIndex )
                    {
                        case CI_REVISION:           pVerData->byCalRevNbr  = sValue.ToInt();     break;
                        case CI_BATTERY_CAPACITY:   pCalData->battCapacity = sValue.ToInt();     break;
                        case CI_BATTERY_TYPE:       pCalData->battType     = GetBattTypeEnum( sValue ); break;
                    }

                    // Can break out of search loop when an item is found
                    break;
                }
            }
        }
    }
    catch( ... )
    {
        // A catch means either the pValues array was insufficiently populated
        // or one of the entries was an invalid value.
        return false;
    }

    // Fall through means success - update the current cal version
    pVerData->byCalVerNbr = CURR_CAL_VERSION;
    pVerData->byCalRevNbr = CURR_CAL_REV;

    return true;
}


bool TTiltMgrDevice::ReloadCalDataFromDevice( void )
{
    if( !IsConnected )
        return false;

    // Can only start the upload to device if we are currently idle
    if( m_linkState != LS_IDLE )
        return false;

    // Clear the configuration data. When doing this, we also reset averages
    ClearConfigurationData();

    for( int iAvg = 0; iAvg < eNbr_Avg_Vars; iAvg++ )
        m_avgValue[iAvg]->Reset();

    // Set the link state, and bump up the RFC timers for the data exchange
    m_timeParams.rfcRate    = StartupRFCInterval;
    m_timeParams.rfcTimeout = StartupRFCTimeout;

    m_linkState = LS_CFG_DOWNLOAD;

    m_currCfgReqCycle = 1;

    return true;
}


bool TTiltMgrDevice::WriteCalDataToDevice( void )
{
    if( !IsConnected )
        return false;

    // Can only start the upload to device if we are currently idle
    if( m_linkState != LS_IDLE )
        return false;

    for( int iPg = WTTTS_CFG_FIRST_HOST_PAGE; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
        m_cfgPgStatus[iPg].setPending = true;

    // Reset averages when uploading config data
    for( int iAvg = 0; iAvg < eNbr_Avg_Vars; iAvg++ )
        m_avgValue[iAvg]->Reset();

    // Set the link state, and bump up the RFC timers for the data exchange
    m_timeParams.rfcRate    = StartupRFCInterval;
    m_timeParams.rfcTimeout = StartupRFCTimeout;

    m_linkState = LS_CFG_UPLOAD;

    return true;
}


void TTiltMgrDevice::ClearConfigurationData( void )
{
    // Set all pages to the EEPROM 'empty' value first (0xFF)
    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        m_cfgPgStatus[iPg].havePage        = false;
        m_cfgPgStatus[iPg].lastCfgReqCycle = 0;
        m_cfgPgStatus[iPg].setPending      = false;

        memset( m_rawCfgData, 0xFF, NBR_WTTTS_CFG_PAGES * NBR_BYTES_PER_WTTTS_PAGE );
    }

    m_currCfgReqCycle = 0;

    // Call CheckCalDataVersion next - this will initialize our local
    // memory to default values while we wait for the config data
    // to be downloaded.
    CheckCalDataVersion();
}


void TTiltMgrDevice::CheckCalDataVersion( void )
{
    // There is only one cal version currently implemented. For a template on how
    // to implement version upgradig, refer to TWTTTSSub::CheckCalDataVersion()
}


//
// Data Logging
//

void TTiltMgrDevice::LogRawRFCPacket( const WTTTS_PKT_HDR& pktHdr, const GENERIC_RFC_PKT& rfcPkt )
{
    // Write the record to file as text, if logging is enabled
    String logFileName;

    if( !GetDataLogging( DLT_RFC_DATA, logFileName ) )
        return;

    TFileStream* logStream = NULL;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            AnsiString sHeader( "RxTime,SeqNbr,msecs,CompassX,CompassY,CompassZ,AccelX,AccelY,AccelZ\r" );

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        AnsiString sTime = Time().TimeString();

        AnsiString sLine;
        sLine.printf( "%s,%hu,%u,%i,%i,%i,%i,%i,%i\r",
                      sTime.c_str(), pktHdr.seqNbr, pktHdr.timeStamp * MSecsPerPktTick,
                      rfcPkt.compassX, rfcPkt.compassY, rfcPkt.compassZ,
                      rfcPkt.accelX, rfcPkt.accelY, rfcPkt.accelZ );

        logStream->Write( sLine.c_str(), sLine.Length() );
    }
    catch( ... )
    {
    }

    if( logStream != NULL )
        delete logStream;
}

