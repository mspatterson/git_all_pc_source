//************************************************************************
//
//  TTiltMgrDevice.h: this class interfaces with the TEC board using
//       tilt management firmware.
//
//************************************************************************
#ifndef TTiltMgrDeviceH
#define TTiltMgrDeviceH

    #include "CommObj.h"
    #include "WTTTSDefs.h"
    #include "WTTTSProtocolUtils.h"
    #include "SerialPortUtils.h"
    #include "Averaging.h"
    #include "TescoShared.h"
    #include "LinkTiltRegistryInterface.h"

    class TTiltMgrDevice : public TObject {

      public:

        typedef enum {
            eBL_Unknown,
            eBL_Low,
            eBL_Med,
            eBL_OK,
            eNbrBattLevels
        } BatteryLevelType;

        typedef enum {
            CI_VERSION,
            CI_REVISION,
            CI_SERIAL_NBR,
            CI_MFG_INFO,
            CI_MFG_DATE,
            CI_CALIBRATION_DATE,
            CI_BATTERY_TYPE,
            CI_BATTERY_CAPACITY,
            NBR_CALIBRATION_ITEMS
        } CALIBRATION_ITEM;

        static const String CalFactorCaptions[NBR_CALIBRATION_ITEMS];

      protected:

        DEVICE_PORT    m_port;
        bool           m_portLost;

        TNotifyEvent   m_onRFCRecvd;
        TNotifyEvent   m_onDevConnected;
        TNotifyEvent   m_onDevChanged;

        // Receive data vars
        BYTE*          m_rxBuffer;         // received data holding buffer
        DWORD          m_rxBuffCount;      // number of bytes currently in the buffer
        DWORD          m_lastRxByteTime;   // time (tick count) of last byte received in buffer
        DWORD          m_lastRxPktTime;    // time last valid packet of any type received from sub

        WTTTS_PKT_HDR  m_lastPktHdr;    // Header from the last packet

        // We use different comm object routines for UDP comms - these
        // flags track that info
        bool           m_bUsingUDP;     // True if we're using UDP comms
        AnsiString     m_sUDPDevAddr;   // If using UDP, this is the IP addr from which the last event was received
        WORD           m_wUDPDevPort;   // If using UDP, this is the port from which the last event was received

        time_t         m_tPacketTimeout;

        String         m_devID;

        virtual String GetDevStatus( void );
        virtual bool   GetDevIsIdle( void );
        virtual bool   GetDevIsRecving( void );
        virtual bool   GetIsConnected( void );

        virtual String GetPortDesc( void )  { return m_port.stats.portDesc; }

        virtual int    GetConfigUploadProgress( void );

                bool   InitPropertyList( TStringList* pCaptions, TStringList* pValues, const UnicodeString captionList[], int nbrCaptions );

        // RFC info
        struct {
            DWORD            dwRFCTick;   // time (tick count) of last RFC from device
            time_t           tRFCTime;    // time_t of last RFC from device; 0 = don't have an RFC
            TDateTime        dtRFC;       // TDateTime of last RFC
            GENERIC_RFC_PKT  rfcPkt;      // Converted form of RFC packet
        } m_lastRFC;

        // Battery averaging
        struct {
            float        fLastBattVolts;
            int          iLastBattUsed;
            float        fValidThresh;
            float        fMinOperThresh;
            float        fGoodThresh;
            TExpAverage* avgBattVolts;
            TExpAverage* avgBattUsed;
        } m_batteryLevel;

        // The following structure stores the timing values for the device.
        // Note that this structure is designed with the WTTTS device in mind.
        // The legacy device class can interpret these values best it can.
        typedef struct {
            WORD rfcRate;                // Sets RFC tx rate, in msecs
            WORD rfcTimeout;             // Sets how long the WTTTS receiver remains active after sending a RFC
            WORD streamRate;             // Sets the rate at which stream packets are reported
            WORD streamTimeout;          // Sets how long the WTTTS will send stream packets before automatically stopping
            WORD pairingTimeout;         // Sets how long before the WTTTS enters 'deep sleep' if it can't connect
        } TIME_PARAMS;

        TIME_PARAMS m_timeParams;

        WORD GetTimeParam( WTTTS_SET_RATE_TYPE aType );
        void SetTimeParam( WTTTS_SET_RATE_TYPE aType, WORD newValue );

        bool TimeParamsGood( void );

        // Battery management
        BatteryLevelType m_battLevel;

        virtual BatteryLevelType GetBattLevel( void ) { return m_battLevel; }

        float GetTemperature( void ) { return m_lastRFC.rfcPkt.fTemperature; }

        // Link state
        typedef enum {
            LS_CLOSED,         // Comm port closed
            LS_HUNTING,        // Waiting for first RFC command from sub
            LS_CFG_DOWNLOAD,   // Downloading config data from device
            LS_CFG_UPLOAD,     // Uploading config data to device
            LS_IDLE,           // Idle, responding to sub RFC requests with 'no command'
            LS_STREAMING,      // Streaming data samples
            LS_STOPPING,       // Stopping data stream
            NBR_LINK_STATES
        } LINK_STATE;

        LINK_STATE m_linkState;

        // WTTTS hardware info
        BYTE m_cfgStraps;
        BYTE m_firmwareVer[WTTTS_FW_VER_LEN];

        void ClearHardwareInfo( void );

        // TEC command management
        typedef enum {
            TCT_NO_COMMAND,
            TCT_START_DATA_COLLECTION,
            TCT_STOP_DATA_COLLECTION,
            TCT_REQUEST_VER_INFO,
            TCT_GET_CFG_PAGE,
            TCT_SET_CFG_PAGE,
            TCT_SET_RATE_CMD,
            TCT_ENTER_DEEP_SLEEP,
            TCT_SET_RF_CHANNEL,
            NBR_TEC_CMD_TYPES
        } TEC_CMD_TYPE;

        // Create an array of the sub commands. For no payload commands, the
        // cached data can be sent as is.
        typedef struct {
            bool  needsPayload;
            DWORD cmdLen;
            BYTE  byTxData[MAX_TESTORK_PKT_LEN];
        } CACHED_CMD_PKT;

        CACHED_CMD_PKT m_cachedCmds[NBR_TEC_CMD_TYPES];

        void InitTECCmdPkt( TEC_CMD_TYPE cmdType );
        bool SendTECCommand( TEC_CMD_TYPE aCmd, const TESTORK_DATA_UNION* pPayload = NULL );

        // We sometimes want to discard messages from the device when it is
        // switching modes
        int   m_pktDiscardCount;

        // Averaging support
        typedef enum {
           eAV_AccelX,
           eAV_AccelY,
           eAV_AccelZ,
           eAV_CompassX,
           eAV_CompassY,
           eAV_CompassZ,
           eNbr_Avg_Vars
        } eAvgVar;

        TAverage* m_avgValue[eNbr_Avg_Vars];

        TAverage* CreateAverage( const AVERAGING_PARAMS& avgParams );

        void UpdateAveraging( void );

        // Configuration data support
        BYTE m_rawCfgData[NBR_WTTTS_CFG_PAGES * NBR_BYTES_PER_WTTTS_PAGE];

        typedef struct {
            bool  havePage;
            int   lastCfgReqCycle;
            bool  setPending;
        } CFG_PAGE_STATUS;

        // Used to prevent multiple requests for each config page
        int m_currCfgReqCycle;

        CFG_PAGE_STATUS m_cfgPgStatus[NBR_WTTTS_CFG_PAGES];

        void ClearConfigurationData( void );
        void CheckCalDataVersion( void );

        void ClearBatteryVolts( void );
        void UpdateBatteryVolts( void );

        // Logging
        void LogRawRFCPacket( const WTTTS_PKT_HDR& pktHdr, const GENERIC_RFC_PKT& rfcPkt );

      public:

        virtual __fastcall  TTiltMgrDevice();
        virtual __fastcall ~TTiltMgrDevice();

        __property String        DeviceStatus       = { read = GetDevStatus };
        __property bool          DeviceIsIdle       = { read = GetDevIsIdle };
        __property bool          DeviceIsRecving    = { read = GetDevIsRecving };
        __property String        DeviceID           = { read = m_devID };

        virtual bool Connect( const COMMS_CFG& portCfg );
        virtual void Disconnect( void );

        __property bool          IsConnected   = { read = GetIsConnected };
        __property String        ConnectResult = { read = m_port.connResultText };
        __property bool          PortLost      = { read = m_portLost };
        __property String        PortDesc      = { read = GetPortDesc };

        __property TNotifyEvent  OnRFCRecvd    = { read = m_onRFCRecvd,      write = m_onRFCRecvd    };
        __property TNotifyEvent  OnConnected   = { read = m_onDevConnected,  write = m_onDevConnected };
        __property TNotifyEvent  OnNewDevice   = { read = m_onDevChanged,    write = m_onDevChanged} ;

        __property WORD          ParamRate[WTTTS_SET_RATE_TYPE] = { read = GetTimeParam, write = SetTimeParam };

        __property int           ConfigUploadProgress   = { read = GetConfigUploadProgress };

        __property float            Temperature  = { read = GetTemperature };
        __property BatteryLevelType BatteryLevel = { read = GetBattLevel };

        typedef struct {
            float compassX;
            float compassY;
            float compassZ;
            float accelX;
            float accelY;
            float accelZ;
        } DEVICE_READING;

        bool GetLastDevReading( DEVICE_READING& lastReading );
            // Returns averaged values for the last reading

        void RefreshSettings( void );
            // Called to refresh any registry-based settings. Causes those settings
            // to be re-read from the registry.

        virtual bool Update( void );
            // Descendant classes must implement this function. It will get called
            // regularly to allow the class to receive and process information.

        virtual void GetStats( PORT_STATS& portStats );
        virtual void ClearStats( void );

        virtual bool ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues );
        virtual bool GetLastStatusPkt( time_t& tLastPkt, GENERIC_RFC_PKT& lastPkt );
            // Returned values are raw (unaveraged) readings

        virtual bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo );
        virtual bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo );
        virtual bool GetDeviceFWRev( BYTE& byMajor, BYTE& byMinor, BYTE& byRelease, BYTE& byBuild );
        virtual bool GetDeviceMfgInfo( WTTTS_MFG_DATA_STRUCT& mfgInfo );
        virtual bool GetDeviceCfgStraps( BYTE& byStraps );

        bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues );
        bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
        bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
            // Note: users should not rely on the items in the above string lists
            // being in the same order as the calibration enum list above.

        virtual bool ReloadCalDataFromDevice( void );
            // Causes the device to clear it internal cached cal data and
            // reload the data from the device.

        virtual bool WriteCalDataToDevice( void );
            // Writes the calibration data currently cached in the software
            // to the device.

      private:

    };

#endif
