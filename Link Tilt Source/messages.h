//
// Custom Messages passed by classes to the main form
//

#ifndef Messages_h
#define Messages_h

    #define WM_SHOW_SYS_SETTINGS_DLG ( WM_APP + 1 )
         // WParam: RFU
         // LParam: RFU

    #define WM_POLLER_EVENT          ( WM_APP + 2 )
         // WParam: See CommsMgr.h
         // LParam: See CommsMgr.h

#endif

 