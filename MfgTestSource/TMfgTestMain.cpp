#include <vcl.h>
#pragma hdrstop

#include "TMfgTestMain.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TMfgTestMain *MfgTestMain;

static const UnicodeString sPortState  ( "Port State" );
static const UnicodeString sLastCmdTime( "Last Cmd Sent" );
static const UnicodeString sLastRxTime ( "Last Pkt Rcvd At" );
static const UnicodeString sLastRxType ( "Last Pkt Type" );

static const UnicodeString sBR0ChanNbr    ( "Radio 0 Chan" );
static const UnicodeString sBR0RSSI       ( "Radio 0 RSSI" );
static const UnicodeString sBR0BitRate    ( "Radio 0 Bit Rate" );
static const UnicodeString sBR1ChanNbr    ( "Radio 1 Chan" );
static const UnicodeString sBR1RSSI       ( "Radio 1 RSSI " );
static const UnicodeString sBR1BitRate    ( "Radio 1 Bit Rate" );
static const UnicodeString sBROutputStatus( "Output Status" );
static const UnicodeString sBRUpdateRate  ( "Update Rate" );
static const UnicodeString sBRFWVer       ( "Firmware Ver" );
static const UnicodeString sBRCtrlSigs    ( "USB Ctrl Sigs" );
static const UnicodeString sBRRFU0        ( "RFU 0" );
static const UnicodeString sBRRFU1        ( "RFU 1" );
static const UnicodeString sBRLastReset   ( "Last Reset" );


__fastcall TMfgTestMain::TMfgTestMain(TComponent* Owner) : TForm(Owner)
{
    pBaseRadio = new TRealBaseRadio();
}


void __fastcall TMfgTestMain::FormDestroy(TObject *Sender)
{
    delete pBaseRadio;
}


void __fastcall TMfgTestMain::StartScanBtnClick(TObject *Sender)
{
    // Loop here trying to find a base radio
    StartScanBtn->Enabled  = false;
    CancelScanBtn->Enabled = true;
    CancelScanBtn->Tag     = 0;

    ScanStatusPanel->Caption = "Starting scan for Base Radio...";
    Application->ProcessMessages();

    bool bHaveBaseRadio = false;

    int  iPortNbr = 0;

    COMMS_CFG commCfg;

    commCfg.portType  = CT_SERIAL;
    commCfg.portName  = IntToStr( iPortNbr );
    commCfg.portParam = 115200;

    while( !bHaveBaseRadio )
    {
        iPortNbr++;

        if( iPortNbr > 999 )
            iPortNbr = 1;

        commCfg.portName = IntToStr( iPortNbr );

        if( pBaseRadio->Connect( commCfg ) )
        {
            // The port could be opened, wait here for 2 seconds for a
            // a message to appear from the base radio
            ScanStatusPanel->Caption = "Checking for Base Radio on COM" + commCfg.portName;

            DWORD dwEndWait = GetTickCount() + 5000;

            DWORD dwStartPktCount = pBaseRadio->StatusPktCount;

            while( GetTickCount() < dwEndWait )
            {
                Application->ProcessMessages();

                pBaseRadio->Update();

                if( pBaseRadio->StatusPktCount > dwStartPktCount + 10 )
                {
                    bHaveBaseRadio = true;
                    break;
                }
            }

            if( !bHaveBaseRadio )
            {
                pBaseRadio->Disconnect();
                ScanStatusPanel->Caption = "Continuing scan for Base Radio...";
            }
        }

        // Let the application breath on each pass through the loop
        Application->ProcessMessages();

        // Check for cancel
        if( CancelScanBtn->Tag != 0 )
            break;
    }

    if( bHaveBaseRadio )
        ScanStatusPanel->Caption = "Base Radio found on COM" + commCfg.portName;
    else
        ScanStatusPanel->Caption = "No Base Radio found";

    StartScanBtn->Enabled  = true;
    CancelScanBtn->Enabled = false;
    CancelScanBtn->Tag     = 0;
}


void __fastcall TMfgTestMain::CancelScanBtnClick(TObject *Sender)
{
    CancelScanBtn->Tag = 1;
}


void TMfgTestMain::UpdateStatusGrid( TValueListEditor* aGrid, const UnicodeString& key, const UnicodeString& text )
{
    // Look for the key first. If not found, add it
    bool bKeyExists = false;

    for( int iItem = 1; iItem < aGrid->RowCount; iItem++ )
    {
        if( aGrid->Keys[iItem] == key )
        {
            bKeyExists = true;
            break;
        }
    }

    if( bKeyExists )
        aGrid->Values[ key ] = text;
    else
        aGrid->InsertRow( key, text, true );
}


void TMfgTestMain::ClearStatusGrid( TValueListEditor* aGrid )
{
    // Clear all but the title row
    for( int iRow = 1; iRow < aGrid->RowCount; iRow++ )
        aGrid->Values[ aGrid->Keys[iRow] ] = "";
}


void __fastcall TMfgTestMain::PollTimerTimer(TObject *Sender)
{
    time_t tLastPkt;
    BASE_STATUS_STATUS_PKT statusPkt;

    if( !pBaseRadio->IsConnected )
    {
        ClearStatusGrid( BRStatusGrid );

        UpdateStatusGrid( BRStatusGrid, sPortState, "Port closed" );
    }
    else if( pBaseRadio->GetLastStatusPkt( tLastPkt, statusPkt ) )
    {
        UpdateStatusGrid( BRStatusGrid, sPortState,  pBaseRadio->PortDesc );
        UpdateStatusGrid( BRStatusGrid, sLastRxTime, Time().TimeString() );

        UpdateStatusGrid( BRStatusGrid, sBR0ChanNbr, IntToStr(      statusPkt.radioInfo[0].chanNbr ) );
        UpdateStatusGrid( BRStatusGrid, sBR0RSSI,    IntToStr(      statusPkt.radioInfo[0].rssi ) );
        UpdateStatusGrid( BRStatusGrid, sBR0BitRate, IntToStr( (int)statusPkt.radioInfo[0].bitRate ) );

        UpdateStatusGrid( BRStatusGrid, sBR1ChanNbr, IntToStr(      statusPkt.radioInfo[1].chanNbr ) );
        UpdateStatusGrid( BRStatusGrid, sBR1RSSI,    IntToStr(      statusPkt.radioInfo[1].rssi ) );
        UpdateStatusGrid( BRStatusGrid, sBR1BitRate, IntToStr( (int)statusPkt.radioInfo[1].bitRate ) );

        UpdateStatusGrid( BRStatusGrid, sBROutputStatus, IntToHex( statusPkt.outputStatus, 2 ) );
        UpdateStatusGrid( BRStatusGrid, sBRUpdateRate,   IntToStr( statusPkt.statusUpdateRate ) );
        UpdateStatusGrid( BRStatusGrid, sBRFWVer,        IntToStr( statusPkt.fwVer[0] ) + IntToStr( statusPkt.fwVer[1] ) + IntToStr( statusPkt.fwVer[2] ) + IntToStr( statusPkt.fwVer[3] ) );
        UpdateStatusGrid( BRStatusGrid, sBRCtrlSigs,     IntToHex( statusPkt.usbCtrlSignals, 2 ) );
        UpdateStatusGrid( BRStatusGrid, sBRRFU0,         IntToHex( statusPkt.byRFU[0], 2 ) );
        UpdateStatusGrid( BRStatusGrid, sBRRFU1,         IntToHex( statusPkt.byRFU[1], 2 ) );

        switch( statusPkt.lastResetType )
        {
            case 1:   UpdateStatusGrid( BRStatusGrid, sBRLastReset, "Power-up"  );  break;
            case 2:   UpdateStatusGrid( BRStatusGrid, sBRLastReset, "WDT"       );  break;
            case 3:   UpdateStatusGrid( BRStatusGrid, sBRLastReset, "Brown out" );  break;
            default:  UpdateStatusGrid( BRStatusGrid, sBRLastReset, "Type " + IntToStr( statusPkt.lastResetType ) );  break;
        }
    }
}

