object MfgTestMain: TMfgTestMain
  Left = 0
  Top = 0
  Caption = 'TesTORK Manufacturing Test Utility'
  ClientHeight = 467
  ClientWidth = 536
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  DesignSize = (
    536
    467)
  PixelsPerInch = 96
  TextHeight = 13
  object StartScanBtn: TButton
    Left = 32
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Scan for BR'
    TabOrder = 0
    OnClick = StartScanBtnClick
  end
  object ScanStatusPanel: TPanel
    Left = 113
    Top = 24
    Width = 336
    Height = 56
    Caption = 'Port Closed'
    TabOrder = 1
  end
  object CancelScanBtn: TButton
    Left = 32
    Top = 55
    Width = 75
    Height = 25
    Caption = 'Cancel'
    Enabled = False
    TabOrder = 2
    OnClick = CancelScanBtnClick
  end
  object BRStatusGrid: TValueListEditor
    Left = 113
    Top = 86
    Width = 336
    Height = 363
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 3
    TitleCaptions.Strings = (
      'Parameter'
      'Value')
    ColWidths = (
      150
      180)
  end
  object PollTimer: TTimer
    Interval = 100
    OnTimer = PollTimerTimer
    Left = 16
    Top = 416
  end
end
