//---------------------------------------------------------------------------
#ifndef TMfgTestMainH
#define TMfgTestMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "BaseRadioDevice.h"
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
//---------------------------------------------------------------------------
class TMfgTestMain : public TForm
{
__published:	// IDE-managed Components
    TButton *StartScanBtn;
    TPanel *ScanStatusPanel;
    TButton *CancelScanBtn;
    TValueListEditor *BRStatusGrid;
    TTimer *PollTimer;
    void __fastcall StartScanBtnClick(TObject *Sender);
    void __fastcall CancelScanBtnClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall PollTimerTimer(TObject *Sender);
private:
    TBaseRadioDevice* pBaseRadio;

    void UpdateStatusGrid( TValueListEditor* aGrid, const UnicodeString& key, const UnicodeString& text );
    void ClearStatusGrid( TValueListEditor* aGrid );

public:
    __fastcall TMfgTestMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMfgTestMain *MfgTestMain;
//---------------------------------------------------------------------------
#endif
