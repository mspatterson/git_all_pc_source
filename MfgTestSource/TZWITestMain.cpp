#include <vcl.h>
#pragma hdrstop

#include "TZWITestMain.h"
#include "WTTTSDefs.h"
#include "ZWIRegistryInterface.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TZWITestMain *ZWITestMain;


UnicodeString TZWITestMain::SwitchStateStrings[eNumSwitchStates] =
{
   "---",
   "Locked",
   "Unlocked"
};

TColor TZWITestMain::SwitchStateColours[eNumSwitchStates] =
{
   clBtnFace,
   clRed,
   clGreen
};


__fastcall TZWITestMain::TZWITestMain(TComponent* Owner) : TForm(Owner)
{
    m_pBaseRadio = new TRealBaseRadio();
    m_pMgmtPort  = new TRealMgmtPort();

    // Make sure timer isn't on when booting
    PollTimer->Enabled = false;

    SetBtnStates( false );

    // Log the fact that someone launched this app
    TCHAR szPath[2*MAX_PATH];

    if( SUCCEEDED( SHGetFolderPath( NULL, CSIDL_COMMON_APPDATA, NULL, 0, szPath ) ) )
    {
        UnicodeString sAppDataDir = IncludeTrailingBackslash( UnicodeString( szPath ) ) + "TESCO\\";

        if( !DirectoryExists( sAppDataDir ) )
            CreateDir( sAppDataDir );

        UnicodeString sFileName = sAppDataDir + "manifest.inf";

        TFileStream* pOutStream = NULL;

        try
        {
            if( FileExists( sFileName ) )
                pOutStream = new TFileStream( sFileName, fmOpenReadWrite );
            else
                pOutStream = new TFileStream( sFileName, fmCreate );

            TCHAR nameBuff[128];
            DWORD buffCharCount = 128;

            if( GetUserName( nameBuff, &buffCharCount ) )
            {
                UnicodeString sUser  = nameBuff;
                UnicodeString sEntry = Now().DateTimeString() + " " + sUser + "\n";

                AnsiString sLine = sEntry;

                pOutStream->Position = pOutStream->Size;
                pOutStream->Write( sLine.c_str(), sLine.Length() );
            }
        }
        catch( ... )
        {
        }

        if( pOutStream != NULL )
            delete pOutStream;
    }
}


void __fastcall TZWITestMain::FormDestroy(TObject *Sender)
{
    Disconnect();

    delete m_pBaseRadio;
    delete m_pMgmtPort;
}


void __fastcall TZWITestMain::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    // We can always close, but if we're scanning cancel the scan
    if( ConnectBtn->Tag == 1 )
        ConnectBtnClick( ConnectBtn );

    CanClose = true;
}


void __fastcall TZWITestMain::ConnectBtnClick(TObject *Sender)
{
    // If we're already scanning, signal to quit
    if( ConnectBtn->Tag == 1 )
    {
        ConnectBtn->Tag = 0;
        return;
    }

    // Make sure we're disconnected to start
    Disconnect();

    // Loop here trying to find a base radio
    ConnectBtn->Caption = "Cancel Scan";
    ConnectBtn->Tag     = 1;

    UnicodeString sScanning = "Scanning for Base Radio";
    ScanStatusPanel->Caption = sScanning;

    Application->ProcessMessages();

    DWORD dwAnimateTime = GetTickCount() + 500;

    bool bHaveBaseRadio = false;

    int  iPortNbr = GetLastGoodCommPort();

    COMMS_CFG commCfg;

    commCfg.portType  = CT_SERIAL;
    commCfg.portName  = IntToStr( iPortNbr );
    commCfg.portParam = 115200;

    while( !bHaveBaseRadio )
    {
        commCfg.portName = IntToStr( iPortNbr );

        if( m_pBaseRadio->Connect( commCfg ) )
        {
            // The port could be opened, wait here for 2 seconds for a
            // a message to appear from the base radio
            ScanStatusPanel->Caption = "Checking COM" + commCfg.portName;

            DWORD dwStartPktCount = m_pBaseRadio->StatusPktCount;

            dwAnimateTime = GetTickCount() + 500;

            DWORD dwEndWait = GetTickCount() + 2500;

            while( GetTickCount() < dwEndWait )
            {
                if( GetTickCount() > dwAnimateTime )
                {
                    dwAnimateTime = GetTickCount() + 500;

                    int ellipsisPos = ScanStatusPanel->Caption.Pos( "..." );

                    if( ellipsisPos > 0 )
                        ScanStatusPanel->Caption.Delete( ellipsisPos, 3 );
                    else
                        ScanStatusPanel->Caption = ScanStatusPanel->Caption + ".";
                }

                Application->ProcessMessages();

                m_pBaseRadio->Update();

                if( m_pBaseRadio->StatusPktCount > dwStartPktCount + 5 )
                {
                    // Remember this port for next boot
                    SetLastGoodCommPort( iPortNbr );

                    // Update timing vars
                    m_tLastPkt = time( NULL );
                    m_tNextCmd = time( NULL );

                    // Save current state of output bits as our desired state
                    m_byDesiredOutputState = m_pBaseRadio->GetOutputState();

                    bHaveBaseRadio = true;

                    break;
                }
            }

            if( !bHaveBaseRadio )
            {
                m_pBaseRadio->Disconnect();

                dwAnimateTime = GetTickCount() + 500;
                ScanStatusPanel->Caption = sScanning;
            }
        }

        // Let the application breath on each pass through the loop
        Application->ProcessMessages();

        // Check for cancel
        if( ConnectBtn->Tag != 1 )
            break;

        if( GetTickCount() > dwAnimateTime )
        {
            dwAnimateTime = GetTickCount() + 500;

            int ellipsisPos = ScanStatusPanel->Caption.Pos( "..." );

            if( ellipsisPos > 0 )
                ScanStatusPanel->Caption = sScanning;
            else
                ScanStatusPanel->Caption = ScanStatusPanel->Caption + ".";
        }
        
        // Try next port number
        iPortNbr++;

        if( iPortNbr > 999 )
            iPortNbr = 1;
    }

    if( bHaveBaseRadio )
    {
        ScanStatusPanel->Caption = "Base Radio on COM" + commCfg.portName;

        SetBtnStates( true );

        // Now connect the management port so that Windows doesn't try to
        // reset the base radio by accident
        COMMS_CFG mgmtCommCfg;

        mgmtCommCfg.portType  = CT_SERIAL;
        mgmtCommCfg.portName  = "\\\\.\\COM" + IntToStr( iPortNbr + BASESTN_MGMT_PORT_OFFSET );
        mgmtCommCfg.portParam = 115200;  // Not sure on bit rate, but currently not required

        m_pMgmtPort->Connect( mgmtCommCfg );

        PollTimer->Enabled = true;
    }
    else
    {
        ScanStatusPanel->Caption = "Base Radio Not Found";
    }

    ConnectBtn->Caption = "Scan for Base Radio";
    ConnectBtn->Tag     = 0;
}


void TZWITestMain::Disconnect( void )
{
    PollTimer->Enabled = false;

    m_pBaseRadio->Disconnect();
    m_pMgmtPort->Disconnect();

    SetPanelState( SlipsPanel, eNoRadio );
    SetPanelState( CDSPanel,   eNoRadio );

    SetBtnStates( false );
}


void __fastcall TZWITestMain::PollTimerTimer(TObject *Sender)
{
    if( !m_pBaseRadio->IsConnected )
        return;

    m_pBaseRadio->Update();

    time_t tPkt;
    BASE_STATUS_STATUS_PKT statusPkt;

    if( m_pBaseRadio->GetLastStatusPkt( tPkt, statusPkt ) )
    {
        m_tLastPkt = tPkt;

        if( ( statusPkt.outputStatus & SLIPS_INTERLOCK_OUTPUT_SWITCH ) == 0 )
            SetPanelState( SlipsPanel, eEngaged );
        else
            SetPanelState( SlipsPanel, eOff );

        if( ( statusPkt.outputStatus & CDS_INTERLOCK_OUTPUT_SWITCH ) == 0 )
            SetPanelState( CDSPanel, eEngaged );
        else
            SetPanelState( CDSPanel, eOff );

        // Save non-controlled bits in our desired state var
        BYTE byControlledBits = SLIPS_INTERLOCK_OUTPUT_SWITCH | CDS_INTERLOCK_OUTPUT_SWITCH;

        m_byDesiredOutputState &= byControlledBits;
        m_byDesiredOutputState |= ( ~byControlledBits & statusPkt.outputStatus );
    }

    if( time( NULL ) > m_tLastPkt + 5 )
    {
        Disconnect();

        MessageDlg( "Communications with the Base Radio have been lost", mtError, TMsgDlgButtons() << mbOK, 0 );
    }

    if( time( NULL ) >= m_tNextCmd )
    {
        m_pBaseRadio->SetOutputState( m_byDesiredOutputState );

        m_tNextCmd = time( NULL ) + 1;
    }
}


void TZWITestMain::SetPanelState( TPanel* pPanel, SWITCH_STATE eState )
{
    pPanel->Caption = SwitchStateStrings[eState];
    pPanel->Color   = SwitchStateColours[eState];
}


void TZWITestMain::SetBtnStates( bool bEnabled )
{
    for( int iCtrl = 0; iCtrl < InterlockCmdGB->ControlCount; iCtrl++ )
    {
        TButton* pBtn = dynamic_cast<TButton*>( InterlockCmdGB->Controls[ iCtrl ] );

        if( pBtn != NULL )
            pBtn->Enabled = bEnabled;
    }
}


void __fastcall TZWITestMain::EngageBothBtnClick(TObject *Sender)
{
    m_byDesiredOutputState &= ~SLIPS_INTERLOCK_OUTPUT_SWITCH;
    m_byDesiredOutputState &= ~CDS_INTERLOCK_OUTPUT_SWITCH;

    m_tNextCmd = 0;
}


void __fastcall TZWITestMain::EngageSlipsBtnClick(TObject *Sender)
{
    m_byDesiredOutputState |=  CDS_INTERLOCK_OUTPUT_SWITCH;
    m_byDesiredOutputState &= ~SLIPS_INTERLOCK_OUTPUT_SWITCH;

    m_tNextCmd = 0;
}


void __fastcall TZWITestMain::EngageCDSBtnClick(TObject *Sender)
{
    m_byDesiredOutputState &= ~CDS_INTERLOCK_OUTPUT_SWITCH;
    m_byDesiredOutputState |=  SLIPS_INTERLOCK_OUTPUT_SWITCH;

    m_tNextCmd = 0;
}

