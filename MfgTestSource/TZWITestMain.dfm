object ZWITestMain: TZWITestMain
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'TesTORK ZWI Test Utility'
  ClientHeight = 371
  ClientWidth = 567
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnDestroy = FormDestroy
  DesignSize = (
    567
    371)
  PixelsPerInch = 96
  TextHeight = 16
  object ConnectionGB: TGroupBox
    Left = 8
    Top = 8
    Width = 551
    Height = 97
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Connection '
    TabOrder = 0
    DesignSize = (
      551
      97)
    object ConnectBtn: TButton
      Left = 18
      Top = 25
      Width = 225
      Height = 56
      Caption = 'Scan for Base Radio'
      TabOrder = 0
      OnClick = ConnectBtnClick
    end
    object ScanStatusPanel: TPanel
      Left = 289
      Top = 24
      Width = 241
      Height = 56
      Anchors = [akLeft, akRight]
      BevelKind = bkSoft
      BevelOuter = bvNone
      Caption = 'Not Connected'
      TabOrder = 1
    end
  end
  object InterlockCmdGB: TGroupBox
    Left = 8
    Top = 120
    Width = 261
    Height = 241
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Interlock Command '
    TabOrder = 1
    object EngageBothBtn: TButton
      Left = 18
      Top = 25
      Width = 225
      Height = 56
      Caption = 'Both Locked'
      TabOrder = 0
      OnClick = EngageBothBtnClick
    end
    object EngageSlipsBtn: TButton
      Left = 18
      Top = 169
      Width = 225
      Height = 56
      Caption = 'Unlock CDS'
      TabOrder = 2
      OnClick = EngageSlipsBtnClick
    end
    object EngageCDSBtn: TButton
      Left = 18
      Top = 97
      Width = 225
      Height = 56
      Caption = 'Unlock Slips'
      TabOrder = 1
      OnClick = EngageCDSBtnClick
    end
  end
  object StatusGB: TGroupBox
    Left = 279
    Top = 120
    Width = 280
    Height = 241
    Caption = ' Interlock Status '
    TabOrder = 2
    object Label1: TLabel
      Left = 18
      Top = 25
      Width = 27
      Height = 16
      Caption = 'Slips'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 18
      Top = 145
      Width = 24
      Height = 16
      Caption = 'CDS'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object SlipsPanel: TPanel
      Left = 18
      Top = 48
      Width = 241
      Height = 57
      BevelKind = bkSoft
      BevelOuter = bvNone
      Caption = '---'
      ParentBackground = False
      TabOrder = 0
    end
    object CDSPanel: TPanel
      Left = 18
      Top = 167
      Width = 241
      Height = 57
      BevelKind = bkSoft
      BevelOuter = bvNone
      Caption = '---'
      ParentBackground = False
      TabOrder = 1
    end
  end
  object PollTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = PollTimerTimer
    Left = 20
    Top = 328
  end
end
