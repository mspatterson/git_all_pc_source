//---------------------------------------------------------------------------
#ifndef TZWITestMainH
#define TZWITestMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include <time.h>
#include "BaseRadioDevice.h"
#include "MgmtPortDevice.h"
//---------------------------------------------------------------------------
class TZWITestMain : public TForm
{
__published:	// IDE-managed Components
    TTimer *PollTimer;
    TGroupBox *ConnectionGB;
    TButton *ConnectBtn;
    TPanel *ScanStatusPanel;
    TGroupBox *InterlockCmdGB;
    TButton *EngageBothBtn;
    TButton *EngageSlipsBtn;
    TButton *EngageCDSBtn;
    TGroupBox *StatusGB;
    TLabel *Label1;
    TLabel *Label2;
    TPanel *SlipsPanel;
    TPanel *CDSPanel;
    void __fastcall ConnectBtnClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall PollTimerTimer(TObject *Sender);
    void __fastcall EngageBothBtnClick(TObject *Sender);
    void __fastcall EngageSlipsBtnClick(TObject *Sender);
    void __fastcall EngageCDSBtnClick(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
private:
    TBaseRadioDevice* m_pBaseRadio;
    TMgmtPortDevice*  m_pMgmtPort;

    time_t m_tLastPkt;
    time_t m_tNextCmd;

    BYTE   m_byDesiredOutputState;

    typedef enum {
       eNoRadio,
       eEngaged,
       eOff,
       eNumSwitchStates
    } SWITCH_STATE;

    static UnicodeString SwitchStateStrings[eNumSwitchStates];
    static TColor        SwitchStateColours[eNumSwitchStates];

    void Disconnect( void );

    void SetPanelState( TPanel* pPanel, SWITCH_STATE eState );
    void SetBtnStates( bool bEnabled );

public:
    __fastcall TZWITestMain(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TZWITestMain *ZWITestMain;
//---------------------------------------------------------------------------
#endif
