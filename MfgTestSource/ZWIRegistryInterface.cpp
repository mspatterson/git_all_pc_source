#include <vcl.h>
#pragma hdrstop

#include <registry.hpp>

#include "RegistryInterface.h"
#include "ApplUtils.h"

#pragma package(smart_init)


//
// Private Registry Defines
//

static const UnicodeString m_SettingsSection( "Settings" );
    static const UnicodeString m_LastPort       ( "LastPort" );


static TRegistryIniFile* CreateRegIni( void );
    // Creates a reg ini file object. Caller is responsible for releasing it


//
// Public Functions
//

int GetLastGoodCommPort( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    int iLastPort = regIni->ReadInteger( m_SettingsSection, m_LastPort, 1 );

    delete regIni;

    return iLastPort;
}


void SetLastGoodCommPort( int iPort )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_SettingsSection, m_LastPort, iPort );

    delete regIni;
}


//
//  Private Functions
//

TRegistryIniFile* CreateRegIni( void )
{
    // Caller must free the created object
    return new TRegistryIniFile( String( "Software\\Tesco\\ZWITestApplet\\V" ) + IntToStr( GetApplMajorVer() ) );
}

