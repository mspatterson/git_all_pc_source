#include <vcl.h>
#pragma hdrstop

#include "AutoConnDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TAutoConnForm *AutoConnForm;


__fastcall TAutoConnForm::TAutoConnForm(TComponent* Owner) : TForm(Owner)
{
}


void TAutoConnForm::ShowDlg( HWND hwndEvent, DWORD dwEventMsg, const String& sMsg )
{
    // Save the event handle for later use
    m_hwndEvent  = hwndEvent;
    m_dwEventMsg = dwEventMsg;

    // Initialize the Animation caption
    m_sDots    = "";
    m_sInfoMsg = sMsg;

    // Don't let the user "X" out of the dialog
    m_bCanClose = false;

    // Start the animation timer
    Animation->Enabled = true;

    // Show the dialog
    Show();
}


void __fastcall TAutoConnForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    // We can only close if the main form has said we can
    CanClose = m_bCanClose;

    if( !CanClose )
        Beep();
}


void __fastcall TAutoConnForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Cleanup the Animation
    Animation->Enabled = false;
}


void TAutoConnForm::CloseDlg( void )
{
    m_bCanClose = true;
    Close();
}


void TAutoConnForm::SetInfoMsg( const String& sNewMsg )
{
    m_sInfoMsg = sNewMsg;
    m_sDots    = "";
}


void __fastcall TAutoConnForm::CancelBtnClick(TObject *Sender)
{
    ::PostMessage( m_hwndEvent, m_dwEventMsg, 0, 0 );
}


void __fastcall TAutoConnForm::AnimationTimer(TObject *Sender)
{
    // Animate another dot onto our text
    if( m_sDots.Length() == 3 )
        m_sDots = "";
    else
        m_sDots += ".";

    // Update the text
    ConnectingLB->Caption = m_sInfoMsg + m_sDots;
}


