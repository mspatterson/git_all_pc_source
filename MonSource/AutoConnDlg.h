#ifndef AutoConnDlgH
#define AutoConnDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>


class TAutoConnForm : public TForm
{
    __published:    // IDE-managed Components
    TButton *CancelBtn;
    TLabel *ConnectingLB;
    TTimer *Animation;
    void __fastcall CancelBtnClick(TObject *Sender);
    void __fastcall AnimationTimer(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);

    private:        // User declarations
        String m_sInfoMsg;
        String m_sDots;

        HWND   m_hwndEvent;
        DWORD  m_dwEventMsg;

        bool   m_bCanClose;

        void SetInfoMsg( const String& sNewMsg );

    public:         // User declarations
        __fastcall TAutoConnForm(TComponent* Owner);

        void ShowDlg( HWND hwndEvent, DWORD dwEventMsg, const String& sFirstMsg );
        void CloseDlg( void );

        __property String InfoMsg = { read = m_sInfoMsg, write = SetInfoMsg };
};

extern PACKAGE TAutoConnForm *AutoConnForm;

#endif
