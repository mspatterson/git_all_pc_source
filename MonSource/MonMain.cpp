#include <vcl.h>
#pragma hdrstop

#include "MonMain.h"
#include "ApplUtils.h"
#include "Comm32.h"
#include "CUDPPort.h"
#include "TypeDefs.h"
#include "TTempCompForm.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TMainForm *MainForm;


// Common Status Grid Key Values
static const UnicodeString sPortState  ( "Port State" );
static const UnicodeString sLastCmdTime( "Last Cmd Sent" );
static const UnicodeString sLastRxTime ( "Last Pkt Rcvd At" );
static const UnicodeString sLastRxType ( "Last Pkt Type" );

// WTTTS Grid Specific Values
static const UnicodeString sRFCTemp      ( "Temperature" );
static const UnicodeString sRFCBattVolts ( "Battery Volts" );
static const UnicodeString sRFCBattUsed  ( "Battery Used" );
static const UnicodeString sRFCBattType  ( "Battery Type" );
static const UnicodeString sRFCAuxInput  ( "Aux Input" );
static const UnicodeString sRFCRPM       ( "RPM" );
static const UnicodeString sRFCLastReset ( "Last Reset" );
static const UnicodeString sRFCRFChan    ( "RF Channel" );
static const UnicodeString sRFCCurrMode  ( "Current Mode" );
static const UnicodeString sRFCRate      ( "RFC Rate" );
static const UnicodeString sRFCTimeout   ( "RFC Timeout" );
static const UnicodeString sRFCStreamRate( "Stream Rate" );
static const UnicodeString sRFCStreamTO  ( "Stream Timeout" );
static const UnicodeString sRFCPairingTO ( "Pairing Timeout" );
static const UnicodeString sTorque045_R3 ( "Torque 045 (R3)" );
static const UnicodeString sTorque225_R3 ( "Torque 225 (R3)" );
static const UnicodeString sTorque045    ( "Torque 045" );       // From here down shared with
static const UnicodeString sTorque225    ( "Torque 225" );       // streaming data grid
static const UnicodeString sTension000   ( "Tension 000" );
static const UnicodeString sTension090   ( "Tension 090" );
static const UnicodeString sTension180   ( "Tension 180" );
static const UnicodeString sTension270   ( "Tension 270" );
static const UnicodeString sGyro         ( "Gyro" );             // Data grid only
static const UnicodeString sCompassX     ( "Compass X" );
static const UnicodeString sCompassY     ( "Compass Y" );
static const UnicodeString sCompassZ     ( "Compass Z" );
static const UnicodeString sAccelX       ( "Accel X" );
static const UnicodeString sAccelY       ( "Accel Y" );
static const UnicodeString sAccelZ       ( "Accel Z" );
static const UnicodeString sRTDValue     ( "RTD" );
static const UnicodeString sDeviceID     ( "Dev ID" );
static const UnicodeString sLastStreamPkt( "Last Pkt Time" );
static const UnicodeString sHdrSeqNbr    ( "Pkt Seq Nbr" );
static const UnicodeString sHdrMsecs     ( "Pkt MSecs" );

// Base Radio Specific Values
static const UnicodeString sBR0ChanNbr    ( "Radio 0 Chan" );
static const UnicodeString sBR0RSSI       ( "Radio 0 RSSI" );
static const UnicodeString sBR0BitRate    ( "Radio 0 Bit Rate" );
static const UnicodeString sBR1ChanNbr    ( "Radio 1 Chan" );
static const UnicodeString sBR1RSSI       ( "Radio 1 RSSI " );
static const UnicodeString sBR1BitRate    ( "Radio 1 Bit Rate" );
static const UnicodeString sBROutputStatus( "Output Status" );
static const UnicodeString sBRUpdateRate  ( "Update Rate" );
static const UnicodeString sBRFWVer       ( "Firmware Ver" );
static const UnicodeString sBRCtrlSigs    ( "USB Ctrl Sigs" );
static const UnicodeString sBRRFU0        ( "RFU 0" );
static const UnicodeString sBRRFU1        ( "RFU 1" );
static const UnicodeString sBRLastReset   ( "Last Reset" );




static void EnableGBCtrls( TGroupBox* aGB, bool isEnabled )
{
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
        aGB->Controls[iCtrl]->Enabled = isEnabled;
}


static void ClearGBTags( TGroupBox* aGB )
{
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
        aGB->Controls[iCtrl]->Tag = 0;
}


static void SetEditState( TEdit* anEdit, bool isEnabled )
{
    if( isEnabled )
    {
        anEdit->Color   = clWindow;
        anEdit->Enabled = true;
    }
    else
    {
        anEdit->Color   = clBtnFace;
        anEdit->Enabled = false;
    }
}


__fastcall TMainForm::TMainForm(TComponent* Owner) : TForm(Owner)
{
    MainPgCtrl->ActivePageIndex = 0;

    memset( &m_BRPort,    0, sizeof( PORT_INFO ) );
    memset( &m_WTTTSPort, 0, sizeof( PORT_INFO ) );

    // Set automode defaults
    asMode = AS_OFF;

    // Reset automode stats
    ClearStatsBtnClick( ClearStatsBtn );
}


void __fastcall TMainForm::FormShow(TObject *Sender)
{
   // Set initial button states
    BRConnectBtn->Enabled    = true;
    BRDisconnectBtn->Enabled = false;

    SetEditState( BRPortEdit,  true );
    SetEditState( BRSpeedEdit, true );

    WTTTSConnectBtn->Enabled    = true;
    WTTTSDisconnectBtn->Enabled = false;

    SetEditState( WTTTSPortEdit,  true );
    SetEditState( WTTTSSpeedEdit, true );

    EnableGBCtrls( BRActionsGB,    false );
    EnableGBCtrls( WTTTSActionsGB, false );

    EnableGBCtrls( AutoParamsGB,  false );

    // Auto page is our front page
    MainPgCtrl->ActivePage = AutoSheet;

    // Start the Auto-Connection
    ::PostMessage( Handle, WM_DO_AUTO_CONN, 0, 0 );
}


void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Release resources
    BRDisconnectBtnClick( BRDisconnectBtn );
    WTTTSDisconnectBtnClick( WTTTSDisconnectBtn );
}


void __fastcall TMainForm::ExitBtnClick(TObject *Sender)
{
    Application->Terminate();
}


//
// Base Radio
//

void __fastcall TMainForm::BRConnectBtnClick(TObject *Sender)
{
    if( !ValidatePortSettings( BRPortEdit, BRSpeedEdit ) )
        return;

    String sErrMsg;

    if( !DoBRConnect( BRPortEdit->Text.ToInt(), BRSpeedEdit->Text.ToInt(), sErrMsg ) )
        MessageDlg( sErrMsg, mtError, TMsgDlgButtons() << mbOK, 0 );
}


bool TMainForm::DoBRConnect( int iBRPort, DWORD dwBitRate, String& sErrMsg )
{
    // Try to create the comm obj
    if( !CreatePort( iBRPort, dwBitRate, m_BRPort, sErrMsg ) )
        return false;

    if( m_BRPort.bUsingUDP )
        UpdateStatusGrid( BRStatusGrid, sPortState, "Open on UDP " + BRPortEdit->Text );
    else
        UpdateStatusGrid( BRStatusGrid, sPortState, "Open on COM" + BRPortEdit->Text );

    // On success, set button states
    BRConnectBtn->Enabled    = false;
    BRDisconnectBtn->Enabled = true;

    SetEditState( BRPortEdit,  false );
    SetEditState( BRSpeedEdit, false );

    EnableGBCtrls( BRActionsGB, true );
    ClearGBTags( BRActionsGB );

    // Enable poll timer
    BRPollTimer->Enabled = true;

    return true;
}


void __fastcall TMainForm::BRDisconnectBtnClick(TObject *Sender)
{
    // Stop poll timer and release comm obj
    BRPollTimer->Enabled = false;

    ReleasePort( m_BRPort );

    ClearStatusGrid( BRStatusGrid );
    UpdateStatusGrid( BRStatusGrid, sPortState, "Closed" );

    // Set button states
    BRConnectBtn->Enabled    = true;
    BRDisconnectBtn->Enabled = false;

    SetEditState( BRPortEdit,  true );
    SetEditState( BRSpeedEdit, true );

    EnableGBCtrls( BRActionsGB, false );
}


void __fastcall TMainForm::SetBRChanBtnClick(TObject *Sender)
{
    BASE_STN_DATA_UNION payloadData;
    payloadData.setChanPkt.radioNbr   = GetSelectedRadioNbr();
    payloadData.setChanPkt.newChanNbr = GetChanNbr( BRChanCombo );

    if( SendBRCmd( BASE_STN_CMD_SET_CHAN_NBR, payloadData, sizeof( BASE_STN_SET_RADIO_CHAN ) ) )
        UpdateStatusGrid( BRStatusGrid, sLastCmdTime, Time().TimeString() );
    else
        Beep();
}


void __fastcall TMainForm::BRSetOutputBtnClick(TObject *Sender)
{
    BASE_STN_DATA_UNION payloadData;
    payloadData.setOutputsPkt.outputFlags = 0;

    if( BROutput1CB->Checked )
        payloadData. setOutputsPkt.outputFlags |= 0x01;

    if( BROutput2CB->Checked )
        payloadData. setOutputsPkt.outputFlags |= 0x02;

    if( BROutput3CB->Checked )
        payloadData. setOutputsPkt.outputFlags |= 0x04;

    if( BROutput4CB->Checked )
        payloadData. setOutputsPkt.outputFlags |= 0x08;

    if( SendBRCmd(BASE_STN_CMD_SET_OUTPUTS, payloadData, sizeof(BASE_STN_SET_OUTPUTS_PKT ) ) )
        UpdateStatusGrid( BRStatusGrid, sLastCmdTime, Time().TimeString() );
    else
        Beep();
}


void __fastcall TMainForm::BRPollTimerTimer(TObject *Sender)
{
    PollPortForData( m_BRPort );

    // Have the parser check for any responses from the unit.
    WTTTS_PKT_HDR       pktHdr;
    BASE_STN_DATA_UNION pktData;

    while( HaveBaseStnRespPkt( m_BRPort.rxBuffer, m_BRPort.rxBuffCount, pktHdr, pktData ) )
    {
        m_BRPort.lastPktTime = GetTickCount();

        UpdateStatusGrid( BRStatusGrid, sLastRxTime, Time().TimeString() );
        UpdateStatusGrid( BRStatusGrid, sLastRxType, IntToStr( pktHdr.pktType ) );

        if( pktHdr.pktType == BASE_STN_RESP_STATUS )
            UpdateBRStatusValues( pktData.statusPkt );
    }
}


bool TMainForm::SendBRCmd( BYTE cmdType, BASE_STN_DATA_UNION& payloadData, BYTE payloadLen )
{
    if( m_BRPort.commPort == NULL )
        return false;

    // Allocate and clear command area
    BYTE txBuffer[MAX_BASE_STN_PKT_LEN];
    memset( txBuffer, 0, MAX_BASE_STN_PKT_LEN );

    WTTTS_PKT_HDR* pHdr  = (WTTTS_PKT_HDR*)txBuffer;

    // Setup header defaults
    pHdr->pktHdr    = BASE_STN_HDR_TX;
    pHdr->pktType   = cmdType;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = payloadLen;

    if( payloadLen > 0 )
    {
        BYTE* pPayload = &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pPayload, &payloadData, payloadLen );
    }

    int hdrAndPayLoadLen = sizeof(WTTTS_PKT_HDR) + payloadLen;

    txBuffer[hdrAndPayLoadLen] = CalcWTTTSChecksum( txBuffer, hdrAndPayLoadLen );

    DWORD pktLen = hdrAndPayLoadLen + 1;

    DWORD bytesSent = m_BRPort.commPort->CommSend( txBuffer, pktLen );

    return( bytesSent == pktLen );
}


void TMainForm::UpdateBRStatusValues( const BASE_STATUS_STATUS_PKT& statusPkt )
{
    UpdateStatusGrid( BRStatusGrid, sBR0ChanNbr, IntToStr(      statusPkt.radioInfo[0].chanNbr ) );
    UpdateStatusGrid( BRStatusGrid, sBR0RSSI,    IntToStr(      statusPkt.radioInfo[0].rssi ) );
    UpdateStatusGrid( BRStatusGrid, sBR0BitRate, IntToStr( (int)statusPkt.radioInfo[0].bitRate ) );

    UpdateStatusGrid( BRStatusGrid, sBR1ChanNbr, IntToStr(      statusPkt.radioInfo[1].chanNbr ) );
    UpdateStatusGrid( BRStatusGrid, sBR1RSSI,    IntToStr(      statusPkt.radioInfo[1].rssi ) );
    UpdateStatusGrid( BRStatusGrid, sBR1BitRate, IntToStr( (int)statusPkt.radioInfo[1].bitRate ) );

    UpdateStatusGrid( BRStatusGrid, sBROutputStatus, IntToHex( statusPkt.outputStatus, 2 ) );
    UpdateStatusGrid( BRStatusGrid, sBRUpdateRate,   IntToStr( statusPkt.statusUpdateRate ) );
    UpdateStatusGrid( BRStatusGrid, sBRFWVer,        IntToStr( statusPkt.fwVer[0] ) + IntToStr( statusPkt.fwVer[1] ) + IntToStr( statusPkt.fwVer[2] ) + IntToStr( statusPkt.fwVer[3] ) );
    UpdateStatusGrid( BRStatusGrid, sBRCtrlSigs,     IntToHex( statusPkt.usbCtrlSignals, 2 ) );
    UpdateStatusGrid( BRStatusGrid, sBRRFU0,         IntToHex( statusPkt.byRFU[0], 2 ) );
    UpdateStatusGrid( BRStatusGrid, sBRRFU1,         IntToHex( statusPkt.byRFU[1], 2 ) );

    switch( statusPkt.lastResetType )
    {
        case 1:   UpdateStatusGrid( BRStatusGrid, sBRLastReset, "Power-up"  );  break;
        case 2:   UpdateStatusGrid( BRStatusGrid, sBRLastReset, "WDT"       );  break;
        case 3:   UpdateStatusGrid( BRStatusGrid, sBRLastReset, "Brown out" );  break;
        default:  UpdateStatusGrid( BRStatusGrid, sBRLastReset, "Type " + IntToStr( statusPkt.lastResetType ) );  break;
    }
}


BYTE TMainForm::GetSelectedRadioNbr( void )
{
    if( BRRadioNbrCombo->ItemIndex < 0 )
        BRRadioNbrCombo->ItemIndex = 0;

    return BRRadioNbrCombo->ItemIndex;
}


//
// WTTTS
//

void __fastcall TMainForm::WTTTSConnectBtnClick(TObject *Sender)
{
    if( !ValidatePortSettings( WTTTSPortEdit, WTTTSSpeedEdit ) )
        return;

    String sErrMsg;

    if( !DoWTTTSConnect( WTTTSPortEdit->Text.ToInt(), WTTTSSpeedEdit->Text.ToInt(), sErrMsg ) )
        MessageDlg( sErrMsg, mtError, TMsgDlgButtons() << mbOK, 0 );
}


bool TMainForm::DoWTTTSConnect( int iWTTTSPort, DWORD dwBitRate, String& sErrMsg )
{
    // Try to create the comm obj.
    if( !CreatePort( iWTTTSPort, dwBitRate, m_WTTTSPort, sErrMsg ) )
        return false;

    if( m_WTTTSPort.bUsingUDP )
        UpdateStatusGrid( WTTTSStatusGrid, sPortState, "Open on UDP " + WTTTSPortEdit->Text );
    else
        UpdateStatusGrid( WTTTSStatusGrid, sPortState, "Open on COM" + WTTTSPortEdit->Text );

    // On success, set button states
    WTTTSConnectBtn->Enabled    = false;
    WTTTSDisconnectBtn->Enabled = true;

    SetEditState( WTTTSPortEdit,  false );
    SetEditState( WTTTSSpeedEdit, false );

    EnableGBCtrls( WTTTSActionsGB, true );
    ClearGBTags( WTTTSActionsGB );

    EnableGBCtrls( TempCompGB, true );

    // Enable the autoparams group, but make the stop button disabled
    EnableGBCtrls( AutoParamsGB, true );

    AutoStopBtn->Enabled = false;

    // Enable poll timer
    WTTTSPollTimer->Enabled = true;

    return true;
}


void __fastcall TMainForm::WTTTSDisconnectBtnClick(TObject *Sender)
{
    // Stop poll timer and release comm obj
    WTTTSPollTimer->Enabled = false;

    ReleasePort( m_WTTTSPort );

    ClearStatusGrid( WTTTSStatusGrid );
    UpdateStatusGrid( WTTTSStatusGrid, sPortState, "Closed" );

    // Set button states
    WTTTSConnectBtn->Enabled    = true;
    WTTTSDisconnectBtn->Enabled = false;

    SetEditState( WTTTSPortEdit,  true );
    SetEditState( WTTTSSpeedEdit, true );

    EnableGBCtrls( WTTTSActionsGB, false );
    EnableGBCtrls( AutoParamsGB,   false );
    EnableGBCtrls( TempCompGB,     false );

    // Clear the automode state
    asMode = AS_OFF;
}


void __fastcall TMainForm::WTTTSBtnClick(TObject *Sender)
{
    // Just set the tag to 1 indicating a message is to be sent
    TButton* currBtn = dynamic_cast<TButton*>(Sender);

    if( currBtn != NULL )
        currBtn->Tag = 1;
}


void __fastcall TMainForm::SetStreamRateBtnClick(TObject *Sender)
{
    int streamRate = StreamRateEdit->Text.ToIntDef( 0 );

    if( streamRate <= 0 )
    {
        Beep();
        return;
    }

    WTTTSBtnClick( SetStreamRateBtn );
}




void __fastcall TMainForm::SetRFCRateBtnClick(TObject *Sender)
{
    int RFCRate = RFCRateEdit->Text.ToIntDef( 0 );

    if( RFCRate <= 0 )
    {
        Beep();
        return;
    }

    WTTTSBtnClick( SetRFCRateBtn );
}


void __fastcall TMainForm::ShowTempCompBtnClick(TObject *Sender)
{
    TempCompForm->ShowTempCompForm();
}


void __fastcall TMainForm::WTTTSPollTimerTimer(TObject *Sender)
{
    // First look for any data from the device
    PollPortForData( m_WTTTSPort );

    // Have the parser check for any responses from the unit.
    bool bRFCPending    = false;
    bool bHaveStreamPkt = false;

    WTTTS_PKT_HDR      pktHdr;
    TESTORK_DATA_UNION pktData;

    while( HaveTesTORKRespPkt( m_WTTTSPort.rxBuffer, m_WTTTSPort.rxBuffCount, pktHdr, pktData ) )
    {
        m_WTTTSPort.lastPktTime = GetTickCount();

        UpdateStatusGrid( WTTTSStatusGrid, sLastRxTime, Time().TimeString() );
        UpdateStatusGrid( WTTTSStatusGrid, sLastRxType, IntToStr( pktHdr.pktType ) );

        // We are only interested in RFC commands from the WTTTS device.
        if( ( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_V1 ) || ( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_V2 ) )
        {
            GENERIC_RFC_PKT rfcPkt;
            ConvertRFCPktToGeneric( rfcPkt, pktHdr, pktData );

            UpdateWTTTSRFCValues( rfcPkt );

            TempCompForm->ProcessRFC( rfcPkt );

            bRFCPending = true;

            if( RFCDataCB->Checked )
                RFCDataCB->Checked = LogRFCPacket( RFCDataFileEdit->Text, pktHdr, rfcPkt );
        }
        else if( pktHdr.pktType == WTTTS_RESP_STREAM_DATA )
        {
            UpdateWTTTSStreamValue( pktHdr.seqNbr, pktHdr.timeStamp, pktData.streamData );
            bHaveStreamPkt = true;

            if( StreamDataCB->Checked )
                StreamDataCB->Checked = LogStreamPacket( StreamDataFileEdit->Text, pktHdr, pktData.streamData );
        }
    }

    // Now process the auto-mode state machine
    switch( asMode )
    {
        case AS_RFC_MODE:
            // We are receiving RFCs. Check if our timer has expired
            if( time( NULL ) >= asNextChangeTime )
            {
                if( bRFCPending )
                {
                    // Cause the streaming mode command to be sent
                    WTTTSBtnClick( StartStreamBtn );
                    amsStartStreamStats.requestsSent++;
                    amsStartStreamStats.currRetries = 0;

                    asMode = AS_STREAM_STARTING;
                }
            }

            // Check for RFC timeout if we didn't receive a packet. This
            // work is done in msecs instead of secs for better accuracy.
            // Note we'll have a problem if GetTickCount() wraps, but that
            // is okay here as this is just a test utility that shouldn't
            // be running for 49 days straight.
            if( bRFCPending )
            {
                m_rfcStats.tLastRFC = GetTickCount();
            }
            else
            {
                // Note: the RFC rate should never be zero - if it is,
                // set it to a default value
                if( m_rfcStats.rfcRate == 0 )
                    m_rfcStats.rfcRate = 1000;

                // Use the last RFC rate to determine timeout, but add a
                // little buffer to account for system lag
                DWORD currTickCount = GetTickCount();
                DWORD rfcTimeout    = m_rfcStats.tLastRFC + m_rfcStats.rfcRate + m_rfcStats.rfcRate / 4;

                if( currTickCount > rfcTimeout )
                {
                    // We have a timeout. We'll hit this event on every timer tick
                    // after a timeout starts, so we have to determine how many
                    // RFCs have actually been missed.
                    DWORD rfcWaitTime = currTickCount - m_rfcStats.tLastRFC;
                    DWORD missedRFCs  = rfcWaitTime / m_rfcStats.rfcRate;

                    if( missedRFCs > m_rfcStats.currMissedRFCs )
                    {
                        m_rfcStats.currMissedRFCs = missedRFCs;

                        if( missedRFCs > m_rfcStats.maxMissedRFCs )
                            m_rfcStats.maxMissedRFCs = missedRFCs;
                    }
                }
            }

            break;

        case AS_STREAM_STARTING:
            // We are waiting for streaming data to start. If an RFC packet
            // has been received, resend our start command
            if( bRFCPending )
            {
                WTTTSBtnClick( StartStreamBtn );
                amsStartStreamStats.retriesSent++;
                amsStartStreamStats.currRetries++;

                if( amsStartStreamStats.currRetries > amsStartStreamStats.maxRetries )
                    amsStartStreamStats.maxRetries = amsStartStreamStats.currRetries;
            }
            else if( bHaveStreamPkt )
            {
                // Update the state change timer. In case someone has entered bad
                // text in the edit, update the edit with the actual value we
                // are using.
                int stateTime = StreamTimeEdit->Text.ToIntDef( 30 );

                StreamTimeEdit->Text = IntToStr( stateTime );

                asNextChangeTime = time( NULL ) + stateTime;

                asMode = AS_STREAM_MODE;
            }
            break;

        case AS_STREAM_MODE:
            // We are receiving stream data. Check if our timer has expired
            if( time( NULL ) >= asNextChangeTime )
            {
                if( bRFCPending || bHaveStreamPkt )
                {
                    // Time to switch states
                    WTTTSBtnClick( StopStreamBtn );
                    amsStopStreamStats.requestsSent++;
                    amsStopStreamStats.currRetries = 0;

                    asMode = AS_STREAM_STOPPING;
                }
            }
            break;

        case AS_STREAM_STOPPING:
            // We are waiting for streaming to stop. Check if an RFC has been received.
            if( bRFCPending )
            {
                // Update the state change timer. In case someone has entered bad
                // text in the edit, update the edit with the actual value we
                // are using.
                int stateTime = RFCTimeEdit->Text.ToIntDef( 90 );

                RFCTimeEdit->Text = IntToStr( stateTime );

                asNextChangeTime = time( NULL ) + stateTime;

                asMode = AS_RFC_MODE;

                // Reset RFC timer vars now
                m_rfcStats.tLastRFC       = GetTickCount();
                m_rfcStats.currMissedRFCs = 0;
            }
            else if( bHaveStreamPkt )
            {
                WTTTSBtnClick( StopStreamBtn );
                amsStopStreamStats.retriesSent++;
                amsStopStreamStats.currRetries++;

                if( amsStopStreamStats.currRetries > amsStopStreamStats.maxRetries )
                    amsStopStreamStats.maxRetries = amsStopStreamStats.currRetries;
            }

            break;
    }

    // Update auto-mode stats
    RefreshAutoStats();

    // During streaming mode, we must be able to still send start stream
    // and stop stream commands. If either of those buttons have been
    // pressed, fake out that a RFC has been received.
    if( bHaveStreamPkt )
    {
        if( ( StartStreamBtn->Tag == 1 ) || ( StopStreamBtn->Tag == 1 ) )
            bRFCPending = true;
    }

    if( bRFCPending )
    {
        // If no button has been pressed, by default respond with a 'no cmd' pkt
        TButton* currBtn = NULL;

        TESTORK_DATA_UNION payloadData;
        BYTE cmdType    = WTTTS_CMD_NO_CMD;
        BYTE payloadLen = 0;

        if( SetWTTTSChanBtn->Tag == 1 )
        {
            currBtn = SetWTTTSChanBtn;

            cmdType    = WTTTS_CMD_SET_RF_CHANNEL;
            payloadLen = sizeof( WTTTS_SET_CHAN_PKT );

            payloadData.chanPkt.chanNbr = GetChanNbr( WTTTSChanCombo );
        }
        else if( SetStreamRateBtn->Tag == 1 )
        {
            currBtn = SetStreamRateBtn;

            cmdType    = WTTTS_CMD_SET_RATE;
            payloadLen = sizeof( WTTTS_RATE_PKT );

            payloadData.ratePkt.rateType = SRT_STREAM_RATE;
            payloadData.ratePkt.newValue = StreamRateEdit->Text.ToInt();
        }
        else if( SetRFCRateBtn->Tag == 1 )
        {
            currBtn = SetRFCRateBtn;

            cmdType    = WTTTS_CMD_SET_RATE;
            payloadLen = sizeof( WTTTS_RATE_PKT );

            payloadData.ratePkt.rateType = SRT_RFC_RATE;
            payloadData.ratePkt.newValue = RFCRateEdit->Text.ToInt();
        }
        else if( StopStreamBtn->Tag == 1 )
        {
            currBtn = StopStreamBtn;

            cmdType    = WTTTS_CMD_STOP_STREAM;
            payloadLen = 0;
        }
        else if( StartStreamBtn->Tag == 1 )
        {
            currBtn = StartStreamBtn;

            cmdType    = WTTTS_CMD_START_STREAM;
            payloadLen = 0;
        }

        if( SendWTTTSCmd( cmdType, payloadData, payloadLen ) )
        {
            if( currBtn != NULL )
            {
                currBtn->Tag = 0;

                UpdateStatusGrid( WTTTSStatusGrid, sLastCmdTime, Time().TimeString() );
            }
        }
    }
}


bool TMainForm::SendWTTTSCmd( BYTE cmdType, TESTORK_DATA_UNION& payloadData, BYTE payloadLen )
{
    if( m_WTTTSPort.commPort == NULL )
        return false;

    // Allocate and clear command area
    BYTE txBuffer[MAX_TESTORK_PKT_LEN];
    memset( txBuffer, 0, MAX_TESTORK_PKT_LEN);

    WTTTS_PKT_HDR* pHdr  = (WTTTS_PKT_HDR*)txBuffer;

    // Setup header defaults
    pHdr->pktHdr    = TESTORK_HDR_TX;
    pHdr->pktType   = cmdType;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = payloadLen;

    if( payloadLen > 0 )
    {
        BYTE* pPayload = &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pPayload, &payloadData, payloadLen );
    }

    int hdrAndPayLoadLen = sizeof(WTTTS_PKT_HDR) + payloadLen;

    txBuffer[hdrAndPayLoadLen] = CalcWTTTSChecksum( txBuffer, hdrAndPayLoadLen );

    DWORD pktLen = hdrAndPayLoadLen + 1;

    DWORD bytesSent;

    if( m_WTTTSPort.bUsingUDP )
        bytesSent = ((CUDPPort*)m_WTTTSPort.commPort)->SendTo( txBuffer, pktLen, m_WTTTSPort.sIPAddr, m_WTTTSPort.wUDPPort );
    else
        bytesSent = m_WTTTSPort.commPort->CommSend( txBuffer, pktLen );

    return( bytesSent == pktLen );
}


void TMainForm::UpdateWTTTSRFCValues( const GENERIC_RFC_PKT& rfcPkt )
{
    // First, record the current RFC rate
    m_rfcStats.rfcRate = rfcPkt.rfcRate;

    // Now display packet values
    UpdateStatusGrid( WTTTSStatusGrid, sRFCTemp,       FloatToStrF( rfcPkt.fTemperature, ffFixed, 7, 1 ) );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCBattVolts,  FloatToStrF( rfcPkt.fBattVoltage, ffFixed, 7, 3 ) );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCBattUsed,   IntToStr( rfcPkt.battUsed ) );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCRate,       IntToStr( rfcPkt.rfcRate )        + " msecs" );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCTimeout,    IntToStr( rfcPkt.rfcTimeout )     + " msecs" );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCStreamRate, IntToStr( rfcPkt.streamRate )     + " msecs" );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCStreamTO,   IntToStr( rfcPkt.streamTimeout )  + " secs" );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCPairingTO,  IntToStr( rfcPkt.pairingTimeout ) + " secs" );

    switch( rfcPkt.battType )
    {
        case 1:   UpdateStatusGrid( WTTTSStatusGrid, sRFCBattType, "Lithium" );  break;
        case 2:   UpdateStatusGrid( WTTTSStatusGrid, sRFCBattType, "NiMH"    );  break;
        default:  UpdateStatusGrid( WTTTSStatusGrid, sRFCBattType, "Type " + IntToStr( rfcPkt.battType ) ); break;
    }

    switch( rfcPkt.lastReset )
    {
        case 1:   UpdateStatusGrid( WTTTSStatusGrid, sRFCLastReset, "Power-up"  );  break;
        case 2:   UpdateStatusGrid( WTTTSStatusGrid, sRFCLastReset, "WDT"       );  break;
        case 3:   UpdateStatusGrid( WTTTSStatusGrid, sRFCLastReset, "Brown out" );  break;
        default:  UpdateStatusGrid( WTTTSStatusGrid, sRFCLastReset, "Type " + IntToStr( rfcPkt.lastReset ) );  break;
    }

    switch( rfcPkt.currMode )
    {
        case 0:   UpdateStatusGrid( WTTTSStatusGrid, sRFCCurrMode, "Entering deep sleep" );  break;
        case 1:   UpdateStatusGrid( WTTTSStatusGrid, sRFCCurrMode, "Normal"              );  break;
        case 2:   UpdateStatusGrid( WTTTSStatusGrid, sRFCCurrMode, "Low power"           );  break;
        default:  UpdateStatusGrid( WTTTSStatusGrid, sRFCCurrMode, "Type " + IntToStr( rfcPkt.lastReset ) );  break;
    }

    UpdateStatusGrid( WTTTSStatusGrid, sRFCRPM,    IntToStr( rfcPkt.rpm ) );
    UpdateStatusGrid( WTTTSStatusGrid, sRFCRFChan, IntToStr( rfcPkt.rfChannel ) );

    UpdateStatusGrid( WTTTSStatusGrid, sRFCAuxInput, IntToStr( rfcPkt.pdsSwitch ) );

    UpdateStatusGrid( WTTTSStatusGrid, sTorque045, IntToStr( rfcPkt.torque045 ) );
    UpdateStatusGrid( WTTTSStatusGrid, sTorque225, IntToStr( rfcPkt.torque225 ) );

    UpdateStatusGrid( WTTTSStatusGrid, sTension000, IntToStr( rfcPkt.tension000 ) );
    UpdateStatusGrid( WTTTSStatusGrid, sTension090, IntToStr( rfcPkt.tension090 ) );
    UpdateStatusGrid( WTTTSStatusGrid, sTension180, IntToStr( rfcPkt.tension180 ) );
    UpdateStatusGrid( WTTTSStatusGrid, sTension270, IntToStr( rfcPkt.tension270 ) );

    UpdateStatusGrid( WTTTSStatusGrid, sCompassX, IntToStr( rfcPkt.compassX ) );
    UpdateStatusGrid( WTTTSStatusGrid, sCompassY, IntToStr( rfcPkt.compassY ) );
    UpdateStatusGrid( WTTTSStatusGrid, sCompassZ, IntToStr( rfcPkt.compassZ ) );

    UpdateStatusGrid( WTTTSStatusGrid, sAccelX, IntToStr( rfcPkt.accelX ) );
    UpdateStatusGrid( WTTTSStatusGrid, sAccelY, IntToStr( rfcPkt.accelY ) );
    UpdateStatusGrid( WTTTSStatusGrid, sAccelZ, IntToStr( rfcPkt.accelZ ) );

    if( rfcPkt.rfcV2DataPresent )
    {
        UpdateStatusGrid( WTTTSStatusGrid, sTorque045_R3, IntToStr( rfcPkt.torque045_R3 ) );
        UpdateStatusGrid( WTTTSStatusGrid, sTorque225_R3, IntToStr( rfcPkt.torque225_R3 ) );
        UpdateStatusGrid( WTTTSStatusGrid, sRTDValue, IntToStr( rfcPkt.rtdReading ) );
        UpdateStatusGrid( WTTTSStatusGrid, sDeviceID, DeviceIDToStr( rfcPkt.deviceID ) );
    }
    else
    {
        UpdateStatusGrid( WTTTSStatusGrid, sTorque045_R3, "" );
        UpdateStatusGrid( WTTTSStatusGrid, sTorque225_R3, "" );
        UpdateStatusGrid( WTTTSStatusGrid, sRTDValue,     "" );
        UpdateStatusGrid( WTTTSStatusGrid, sDeviceID,     "" );
    }
}


void TMainForm::UpdateWTTTSStreamValue( BYTE seqNbr, WORD msecs, const WTTTS_STREAM_DATA& streamPkt )
{
    UpdateStatusGrid( StreamValueEditor, sLastStreamPkt, Now().FormatString( "nn:ss" ) );

    UpdateStatusGrid( StreamValueEditor, sHdrSeqNbr, IntToStr( seqNbr ) );
    UpdateStatusGrid( StreamValueEditor, sHdrMsecs,  IntToStr( msecs ) );

    UpdateStatusGrid( StreamValueEditor, sTorque045, IntToStr( TriByteToInt( streamPkt.torque045 ) ) );
    UpdateStatusGrid( StreamValueEditor, sTorque225, IntToStr( TriByteToInt( streamPkt.torque225 ) ) );

    UpdateStatusGrid( StreamValueEditor, sTension000, IntToStr( TriByteToInt( streamPkt.tension000 ) ) );
    UpdateStatusGrid( StreamValueEditor, sTension090, IntToStr( TriByteToInt( streamPkt.tension090 ) ) );
    UpdateStatusGrid( StreamValueEditor, sTension180, IntToStr( TriByteToInt( streamPkt.tension180 ) ) );
    UpdateStatusGrid( StreamValueEditor, sTension270, IntToStr( TriByteToInt( streamPkt.tension270 ) ) );

    UpdateStatusGrid( StreamValueEditor, sGyro, IntToStr( QuadByteToInt( streamPkt.gyro ) ) );
}


//
// Common Handlers
//

bool TMainForm::CreatePort( int portNbr, DWORD portSpeed, PORT_INFO& portInfo, String& sErrMsg )
{
    // Clear return struct params first
    portInfo.commPort    = NULL;
    portInfo.rxBuffCount = 0;
    portInfo.lastRxTime  = 0;
    portInfo.lastPktTime = 0;
    portInfo.bUsingUDP   = false;
    portInfo.sIPAddr     = "";
    portInfo.wUDPPort    = 0;

    sErrMsg = "";

    // Create a comm port object. We do a cheat here: if the port number is < 100
    // then assume we are creating a serial port
    CCommObj* portObj;

    if( portNbr < 100 )
        portObj = new CCommPort();
    else
        portObj = new CUDPPort();

    if( portObj == NULL )
        throw Exception( "Could create comm object!" );

    CCommObj::COMM_RESULT connResult;

    if( portNbr < 100 )
    {
        CCommPort::CONNECT_PARAMS connParams;

        connParams.iPortNumber   = portNbr;
        connParams.dwLineBitRate = portSpeed;

        connResult = portObj->Connect( &connParams );
    }
    else
    {
        CUDPPort::CONNECT_PARAMS connParams;

        connParams.destAddr = "";
        connParams.destPort = 0;
        connParams.portNbr  = portNbr;
        connParams.acceptBroadcasts = true;

        InitWinsock();

        connResult = portObj->Connect( &connParams );

        if( connResult == CCommObj::ERR_NONE )
            portInfo.bUsingUDP = true;
        else
            ShutdownWinsock();
    }

    if( connResult != CCommObj::ERR_NONE )
    {
        delete portObj;
        portObj = NULL;

        UnicodeString connResultText;

        switch( connResult )
        {
            case CCommObj::ERR_NONE:                 connResultText = "success";           break;
            case CCommObj::ERR_COMM_SET_COMM_STATE:  connResultText = "set state error";   break;
            case CCommObj::ERR_COMM_INV_BUFFER_LEN:  connResultText = "set buffer error";  break;
            case CCommObj::ERR_COMM_OPEN_FAIL:       connResultText = "open port error";   break;
            case CCommObj::ERR_COMM_GET_COMM_STATE:  connResultText = "get state error";   break;
            default:                                 connResultText = "error " + IntToStr( connResult );  break;
        }

        sErrMsg = "Could not open port: " + connResultText;

        return false;
    }

    // Fall through means success
    portInfo.commPort = portObj;

    return true;
}


void TMainForm::ReleasePort( PORT_INFO& portInfo )
{
    if( portInfo.commPort != NULL )
    {
        portInfo.commPort->Disconnect();

        delete portInfo.commPort;

        portInfo.commPort = NULL;

        if( portInfo.bUsingUDP )
            ShutdownWinsock();
    }

    portInfo.rxBuffCount = 0;
    portInfo.lastRxTime  = 0;
    portInfo.lastPktTime = 0;
    portInfo.bUsingUDP   = false;
    portInfo.sIPAddr     = "";
    portInfo.wUDPPort    = 0;
}


bool TMainForm::ValidatePortSettings( TEdit* portEdit, TEdit* speedEdit )
{
    try
    {
        int testResult = portEdit->Text.ToInt();

        if( testResult <= 0 )
            Abort();
    }
    catch( ... )
    {
        MessageDlg( "You have entered an invalid port number.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    try
    {
        int testResult = speedEdit->Text.ToInt();

        if( testResult <= 0 )
            Abort();
    }
    catch( ... )
    {
        MessageDlg( "You have entered an invalid speed.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    // Fall through means success
    return true;
}


BYTE TMainForm::GetChanNbr( TComboBox* aCombo )
{
    if( aCombo->ItemIndex < 0 )
        aCombo->ItemIndex = 0;

    return aCombo->Items->Strings[ aCombo->ItemIndex ].ToInt();
}


void TMainForm::PollPortForData( PORT_INFO& portInfo )
{
    if( portInfo.commPort != NULL )
    {
        DWORD bytesRxd;

        if( portInfo.bUsingUDP )
            bytesRxd = ((CUDPPort*)portInfo.commPort)->RecvFrom( &(portInfo.rxBuffer[portInfo.rxBuffCount]), RX_BUFF_SIZE - portInfo.rxBuffCount, portInfo.sIPAddr, portInfo.wUDPPort );
        else
            bytesRxd = portInfo.commPort->CommRecv( &(portInfo.rxBuffer[portInfo.rxBuffCount]), RX_BUFF_SIZE - portInfo.rxBuffCount );

        if( bytesRxd > 0 )
        {
            portInfo.lastRxTime  = GetTickCount();
            portInfo.rxBuffCount += bytesRxd;
        }
        else
        {
            // No data received - check if any bytes in the buffer have gone stale
            if( HaveTimeout( portInfo.lastRxTime, 250 ) )
                portInfo.rxBuffCount = 0;
        }
    }
}


void TMainForm::UpdateStatusGrid( TValueListEditor* aGrid, const UnicodeString& key, const UnicodeString& text )
{
    // Look for the key first. If not found, add it
    bool bKeyExists = false;

    for( int iItem = 1; iItem < aGrid->RowCount; iItem++ )
    {
        if( aGrid->Keys[iItem] == key )
        {
            bKeyExists = true;
            break;
        }
    }

    if( bKeyExists )
        aGrid->Values[ key ] = text;
    else
        aGrid->InsertRow( key, text, true );
}


void TMainForm::ClearStatusGrid( TValueListEditor* aGrid )
{
    // Clear all but the title row
    for( int iRow = 1; iRow < aGrid->RowCount; iRow++ )
        aGrid->Values[ aGrid->Keys[iRow] ] = "";
}


void __fastcall TMainForm::RFCOpenFileBtnClick(TObject *Sender)
{
    if( SaveLogDlg->Execute() )
        RFCDataFileEdit->Text = SaveLogDlg->FileName;
}


void __fastcall TMainForm::StreamOpenFileBtnClick(TObject *Sender)
{
    if( SaveLogDlg->Execute() )
        StreamDataFileEdit->Text = SaveLogDlg->FileName;
}


bool TMainForm::LogStreamPacket( UnicodeString logFileName, const WTTTS_PKT_HDR& pktHdr, const WTTTS_STREAM_DATA& streamData )
{
    // Writes the passed stream packet to file
    TFileStream* logStream = NULL;

    bool logGood = false;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            AnsiString sHeader( "RxTime,SeqNbr,msecs,Torque045,Torque225,Tension000,Tension090,Tension180,Tension270,Gyro,CompassX,CompassY,CompassZ,AccelX,AccelY,AccelZ\r" );

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        // Convert all structure members first
        int   iTorque045  = TriByteToInt    ( streamData.torque045 );
        int   iTorque225  = TriByteToInt    ( streamData.torque225 );
        int   iTension000 = TriByteToInt    ( streamData.tension000 );
        int   iTension090 = TriByteToInt    ( streamData.tension090 );
        int   iTension180 = TriByteToInt    ( streamData.tension180 );
        int   iTension270 = TriByteToInt    ( streamData.tension270 );
        int   iRotation   = QuadByteToInt   ( streamData.gyro );
        INT16 siCompassX  = BiByteToShortInt( streamData.compassX );
        INT16 siCompassY  = BiByteToShortInt( streamData.compassY );
        INT16 siCompassZ  = BiByteToShortInt( streamData.compassZ );
        INT16 siAccelX    = BiByteToShortInt( streamData.accelX );
        INT16 siAccelY    = BiByteToShortInt( streamData.accelY );
        INT16 siAccelZ    = BiByteToShortInt( streamData.accelZ );

        AnsiString sTime = Time().TimeString();

        AnsiString sLine;
        sLine.printf( "%s,%hu,%hu,%i,%i,%i,%i,%i,%i,%i,%hi,%hi,%hi,%hi,%hi,%hi\r",
                      sTime.c_str(), pktHdr.seqNbr, pktHdr.timeStamp,
                      iTorque045, iTorque225, iTension000, iTension090, iTension180, iTension270,
                      iRotation, siCompassX, siCompassY, siCompassZ, siAccelX, siAccelY, siAccelZ );

        logStream->Write( sLine.c_str(), sLine.Length() );

        logGood = true;
    }
    catch( ... )
    {
        logGood = false;
    }

    if( logStream != NULL )
        delete logStream;

    return logGood;
}


bool TMainForm::LogRFCPacket( UnicodeString logFileName, const WTTTS_PKT_HDR& pktHdr, const GENERIC_RFC_PKT& rfcData )
{
    // Writes the passed stream packet to file
    TFileStream* logStream = NULL;

    bool logGood = false;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            AnsiString sHeader( "RxTime,SeqNbr,msecs,temperature,battVolts,battUsed,battType,auxIn,rpm,lastReset,rfChannel,currMode,rfcRate,rfcTimeout,streamRate,streamTimeout,pairingTimeout,Torque045,Torque225,Torque045_R3,Torque225_F3,Tension000,Tension090,Tension180,Tension270,CompassX,CompassY,CompassZ,AccelX,AccelY,AccelZ,RTD,DevID\r" );

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        // Convert all structure members first
        float fTemperature    = rfcData.fTemperature;
        float fBattVolts      = rfcData.fBattVoltage;
        int   iBattUsed       = (int)rfcData.battUsed;
        int   iBattType       = (int)rfcData.battType;
        int   iRpm            = (int)rfcData.rpm;
        int   iLastReset      = (int)rfcData.lastReset;
        int   iRfChannel      = (int)rfcData.rfChannel;
        int   iCurrMode       = (int)rfcData.currMode;
        int   iRfcRate        = (int)rfcData.rfcRate;
        int   iRfcTimeout     = (int)rfcData.rfcTimeout;
        int   iStreamRate     = (int)rfcData.streamRate;
        int   iStreamTimeout  = (int)rfcData.streamTimeout;
        int   iPairingTimeout = (int)rfcData.pairingTimeout;
        int   iAuxInput       = rfcData.pdsSwitch;
        int   iTorque045      = rfcData.torque045;
        int   iTorque225      = rfcData.torque225;
        int   iTorque045_R3   = rfcData.torque045_R3;
        int   iTorque225_R3   = rfcData.torque225_R3;
        int   iTension000     = rfcData.tension000;
        int   iTension090     = rfcData.tension090;
        int   iTension180     = rfcData.tension180;
        int   iTension270     = rfcData.tension270;
        INT16 siCompassX      = rfcData.compassX;
        INT16 siCompassY      = rfcData.compassY;
        INT16 siCompassZ      = rfcData.compassZ;
        INT16 siAccelX        = rfcData.accelX;
        INT16 siAccelY        = rfcData.accelY;
        INT16 siAccelZ        = rfcData.accelZ;
        int   iRTD            = rfcData.rtdReading;

        AnsiString sTime  = Time().TimeString();
        AnsiString sDevID = DeviceIDToStr( rfcData.deviceID );

        AnsiString sLine;
        sLine.printf( "%s,%hu,%hu,%f,%f,%hu,%hu,%i,%u,%hu,%hu,%hu,%hu,%hu,%hu,%hu,%hu,%i,%i,%i,%i,%i,%i,%i,%i,%hi,%hi,%hi,%hi,%hi,%hi,%i,%s\r",
                      sTime.c_str(), pktHdr.seqNbr, pktHdr.timeStamp,
                      fTemperature, fBattVolts, iBattUsed, iBattType,
                      iAuxInput, iRpm, iLastReset, iRfChannel, iCurrMode,
                      iRfcRate, iRfcTimeout, iStreamRate, iStreamTimeout, iPairingTimeout,
                      iTorque045, iTorque225, iTorque045_R3, iTorque225_R3,
                      iTension000, iTension090, iTension180, iTension270,
                      siCompassX, siCompassY, siCompassZ, siAccelX, siAccelY, siAccelZ,
                      iRTD, sDevID.c_str() );

        logStream->Write( sLine.c_str(), sLine.Length() );

        logGood = true;
    }
    catch( ... )
    {
        logGood = false;
    }

    if( logStream != NULL )
        delete logStream;

    return logGood;
}


void __fastcall TMainForm::AutoStartBtnClick(TObject *Sender)
{
    // When starting, assume we are currently sending RFCs.
    // Set our auto-state to RFC mode and force an immediate state
    // change.
    asMode = AS_RFC_MODE;
    asNextChangeTime = 0;

    // Assume we've just received a RFC packet
    m_rfcStats.tLastRFC = GetTickCount();

    // Also disable the time input edits so the user can't fiddle
    // with those while we are in auto mode
    RFCTimeEdit->Enabled    = false;
    StreamTimeEdit->Enabled = false;

    AutoStartBtn->Enabled = false;
    AutoStopBtn->Enabled  = true;
}


void __fastcall TMainForm::AutoStopBtnClick(TObject *Sender)
{
    // Just switch modes and re-enable controls
    asMode = AS_OFF;

    // Also disable the time input edits so the user can't fiddle
    // with those while we are in auto mode
    RFCTimeEdit->Enabled    = true;
    StreamTimeEdit->Enabled = true;

    AutoStartBtn->Enabled = true;
    AutoStopBtn->Enabled  = false;
}


void __fastcall TMainForm::ClearStatsBtnClick(TObject *Sender)
{
    memset( &amsStartStreamStats, 0, sizeof( AUTO_MODE_STAT ) );
    memset( &amsStopStreamStats,  0, sizeof( AUTO_MODE_STAT ) );

    m_rfcStats.missedRFCs    = 0;
    m_rfcStats.maxMissedRFCs = 0;

    RefreshAutoStats();
}


void TMainForm::RefreshAutoStats( void )
{
    switch( asMode )
    {
        case AS_OFF:               AutoModeStatePanel->Caption = "Auto-mode Off";       break;
        case AS_RFC_MODE:          AutoModeStatePanel->Caption = "RFC Mode";            break;
        case AS_STREAM_STARTING:   AutoModeStatePanel->Caption = "Starting Streaming";  break;
        case AS_STREAM_MODE:       AutoModeStatePanel->Caption = "Streaming";           break;
        case AS_STREAM_STOPPING:   AutoModeStatePanel->Caption = "Stopping Streaming";  break;
        default:                   AutoModeStatePanel->Caption = "Bad state!!!";        break;
    }

    StartStreamReqSentPanel->Caption = IntToStr( (int)amsStartStreamStats.requestsSent );
    StartStreamRetSentPanel->Caption = IntToStr( (int)amsStartStreamStats.retriesSent );
    StartStreamMaxRetPanel->Caption  = IntToStr( (int)amsStartStreamStats.maxRetries );
    StopStreamReqSentPanel->Caption  = IntToStr( (int)amsStopStreamStats.requestsSent );
    StopStreamRetSentPanel->Caption  = IntToStr( (int)amsStopStreamStats.retriesSent );
    StopStreamMaxRetPanel->Caption   = IntToStr( (int)amsStopStreamStats.maxRetries );

    MissedRFCsPanel->Caption         = IntToStr( (int)m_rfcStats.missedRFCs );
    MaxMissedRFCsPanel->Caption      = IntToStr( (int)m_rfcStats.maxMissedRFCs );
}


void __fastcall TMainForm::WMDoAutoConn( TMessage &Message )
{
    // This message gets posted on form show. Start the auto-connect process
    AutoConnBtnClick( AutoConnBtn );
}


void __fastcall TMainForm::AutoConnBtnClick( TObject *Sender )
{
    // Ensure nothing else can be done during this time, disable main page
    MainPgCtrl->Enabled = false;

    // Be safe, attempt to disconnect both first
    BRDisconnectBtnClick( BRDisconnectBtn );
    WTTTSDisconnectBtnClick( WTTTSDisconnectBtn );

    // Ensure that we run until finished or cancelled
    m_bAutoScanCancelled = false;

    bool  bBRFound    = false;
    bool  bWTTTSFound = false;
    DWORD dwDefSpeed  = 115200;

    String sLastErrMsg;

    AutoConnForm->ShowDlg( Handle, WM_AUTO_CONN_CANCELLED, "Scanning for Base Radio" );

    // Scan for BR
    for( int iPortNum = 1; iPortNum < 100; iPortNum++ )
    {
        // Check if we can connect to this port
        BRPortEdit->Text = IntToStr( iPortNum );

        if( DoBRConnect( iPortNum, dwDefSpeed, sLastErrMsg ) )
        {
            // The port could be opened, wait here for 2 seconds for a
            // a message to appear from the base radio
            DWORD dwStartPktTime = m_BRPort.lastPktTime;
            DWORD dwEndWait      = GetTickCount() + 2500;

            while( GetTickCount() < dwEndWait )
            {
                // Ensure that we check our other threads in the meantime
                Application->ProcessMessages();

                if( dwStartPktTime != m_BRPort.lastPktTime )
                {
                    bBRFound = true;
                    break;
                }

                if( m_bAutoScanCancelled )
                    break;
            }

            // If we fall through to here and we haven't found the BR,
            // be sure to do a disconnect to release the port
            if( !bBRFound )
                BRDisconnectBtnClick( BRDisconnectBtn );
        }

        // Check if we're done
        if( m_bAutoScanCancelled || bBRFound )
            break;
    }

    // Ensure we can continue
    if( !m_bAutoScanCancelled && bBRFound )
    {
        // Scan for TesTORK
        AutoConnForm->InfoMsg = "Scanning for TesTORK";

        for( int iPortNum = 1; iPortNum < 100; iPortNum++ )
        {
            // Check if we can connect to this port
            WTTTSPortEdit->Text = IntToStr( iPortNum );

            if( DoWTTTSConnect( iPortNum, dwDefSpeed, sLastErrMsg ) )
            {
                // The port could be opened, wait here for 2 seconds for a
                // a message to appear from the base radio
                DWORD dwStartPktTime = m_WTTTSPort.lastPktTime;
                DWORD dwEndWait      = GetTickCount() + 5000;

                while( GetTickCount() < dwEndWait )
                {
                    // Ensure that we check our other threads in the meantime
                    Application->ProcessMessages();

                    if( dwStartPktTime != m_WTTTSPort.lastPktTime )
                    {
                        bWTTTSFound = true;
                        break;
                    }

                    if( m_bAutoScanCancelled )
                        break;
                }

                // If we fall through to here and we haven't found the TT,
                // be sure to do a disconnect to release the port
                if( !bWTTTSFound )
                    WTTTSDisconnectBtnClick( WTTTSDisconnectBtn );
            }

            // Check if we're done
            if( m_bAutoScanCancelled || bWTTTSFound )
                break;
        }
    }

    // If cancelled, clean-up
    if( m_bAutoScanCancelled )
    {
        // If connection attempts were made but failed, disconnect
        if( !bBRFound )
            BRDisconnectBtnClick( BRDisconnectBtn );
        else if( !bWTTTSFound )
            WTTTSDisconnectBtnClick( WTTTSDisconnectBtn );

    }
    else if( !bBRFound )
    {
        MessageDlg( "A Base Radio was not found. Please check the connections to the Base Radio.", mtError, TMsgDlgButtons() << mbOK, 0 );
        BRDisconnectBtnClick( BRDisconnectBtn );
    }
    else if( !bWTTTSFound )
    {
        MessageDlg( "A TesTORK was not found. Please ensure it is powered up. You may have to change the TesTORK radio channel on the Base Radio tab.", mtError, TMsgDlgButtons() << mbOK, 0 );
        WTTTSDisconnectBtnClick( WTTTSDisconnectBtn );
    }

    // Fall through means we're done, close the Auto-Connect Progress form
    AutoConnForm->CloseDlg();

    // Re-Enable main page
    MainPgCtrl->Enabled = true;
}


void __fastcall TMainForm::WMDoAutoConnCancelled( TMessage &Message )
{
    m_bAutoScanCancelled = true;
}


