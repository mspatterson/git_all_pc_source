object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'TesTORK Monitor'
  ClientHeight = 566
  ClientWidth = 679
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    679
    566)
  PixelsPerInch = 96
  TextHeight = 13
  object MainPgCtrl: TPageControl
    Left = 8
    Top = 8
    Width = 663
    Height = 519
    ActivePage = WTTTSSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object BaseRadioSheet: TTabSheet
      Caption = 'Base Radio'
      DesignSize = (
        655
        491)
      object BRStatusGrid: TValueListEditor
        Left = 312
        Top = 16
        Width = 327
        Height = 459
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        TitleCaptions.Strings = (
          'Parameter'
          'Value')
        ColWidths = (
          150
          171)
      end
      object BaseRadioCommsGB: TGroupBox
        Left = 16
        Top = 16
        Width = 281
        Height = 91
        Caption = ' Comms '
        TabOrder = 1
        object Label1: TLabel
          Left = 12
          Top = 27
          Width = 20
          Height = 13
          Caption = 'Port'
        end
        object Label4: TLabel
          Left = 12
          Top = 58
          Width = 30
          Height = 13
          Caption = 'Speed'
        end
        object BRPortEdit: TEdit
          Left = 63
          Top = 24
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 0
          Text = '1'
        end
        object BRConnectBtn: TButton
          Left = 160
          Top = 22
          Width = 105
          Height = 25
          Caption = 'Connect'
          TabOrder = 2
          OnClick = BRConnectBtnClick
        end
        object BRDisconnectBtn: TButton
          Left = 160
          Top = 53
          Width = 105
          Height = 25
          Caption = 'Disconnect'
          Enabled = False
          TabOrder = 3
          OnClick = BRDisconnectBtnClick
        end
        object BRSpeedEdit: TEdit
          Left = 63
          Top = 55
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 1
          Text = '115200'
        end
      end
      object BRActionsGB: TGroupBox
        Left = 16
        Top = 114
        Width = 281
        Height = 266
        Caption = ' Actions '
        TabOrder = 2
        object Label5: TLabel
          Left = 12
          Top = 30
          Width = 27
          Height = 13
          Caption = 'Radio'
        end
        object SetBRChanBtn: TButton
          Left = 160
          Top = 60
          Width = 105
          Height = 25
          Caption = 'Set Chan'
          Enabled = False
          TabOrder = 2
          OnClick = SetBRChanBtnClick
        end
        object BRChanCombo: TComboBox
          Left = 12
          Top = 62
          Width = 128
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = '11'
          Items.Strings = (
            '11'
            '12'
            '13'
            '14'
            '15'
            '16'
            '17'
            '18'
            '19'
            '20'
            '21'
            '22'
            '23'
            '24'
            '25'
            '26')
        end
        object BRRadioNbrCombo: TComboBox
          Left = 63
          Top = 27
          Width = 77
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = '0'
          Items.Strings = (
            '0'
            '1')
        end
        object BROutput1CB: TCheckBox
          Left = 12
          Top = 99
          Width = 28
          Height = 17
          Caption = '1'
          TabOrder = 3
        end
        object BROutput2CB: TCheckBox
          Left = 46
          Top = 99
          Width = 28
          Height = 17
          Caption = '2'
          TabOrder = 4
        end
        object BROutput3CB: TCheckBox
          Left = 79
          Top = 99
          Width = 28
          Height = 17
          Caption = '3'
          TabOrder = 5
        end
        object BROutput4CB: TCheckBox
          Left = 112
          Top = 99
          Width = 28
          Height = 17
          Caption = '4'
          TabOrder = 6
        end
        object BRSetOutputBtn: TButton
          Left = 160
          Top = 95
          Width = 105
          Height = 25
          Caption = 'Set Output'
          TabOrder = 7
          OnClick = BRSetOutputBtnClick
        end
      end
    end
    object WTTTSSheet: TTabSheet
      Caption = 'TesTORK'
      ImageIndex = 1
      DesignSize = (
        655
        491)
      object WTTTSCommsGB: TGroupBox
        Left = 16
        Top = 16
        Width = 281
        Height = 91
        Caption = ' Comms '
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 27
          Width = 20
          Height = 13
          Caption = 'Port'
        end
        object Label3: TLabel
          Left = 12
          Top = 58
          Width = 30
          Height = 13
          Caption = 'Speed'
        end
        object WTTTSPortEdit: TEdit
          Left = 63
          Top = 24
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 0
          Text = '1'
        end
        object WTTTSConnectBtn: TButton
          Left = 160
          Top = 22
          Width = 105
          Height = 25
          Caption = 'Connect'
          TabOrder = 2
          OnClick = WTTTSConnectBtnClick
        end
        object WTTTSDisconnectBtn: TButton
          Left = 160
          Top = 53
          Width = 105
          Height = 25
          Caption = 'Disconnect'
          Enabled = False
          TabOrder = 3
          OnClick = WTTTSDisconnectBtnClick
        end
        object WTTTSSpeedEdit: TEdit
          Left = 63
          Top = 55
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 1
          Text = '115200'
        end
      end
      object WTTTSActionsGB: TGroupBox
        Left = 16
        Top = 113
        Width = 281
        Height = 205
        Caption = ' Actions '
        TabOrder = 1
        object Label6: TLabel
          Left = 12
          Top = 105
          Width = 48
          Height = 13
          Caption = 'Strm Rate'
        end
        object Label14: TLabel
          Left = 12
          Top = 67
          Width = 46
          Height = 13
          Caption = 'RFC Rate'
        end
        object SetWTTTSChanBtn: TButton
          Left = 160
          Top = 26
          Width = 105
          Height = 25
          Caption = 'Set Chan'
          Enabled = False
          TabOrder = 1
          OnClick = WTTTSBtnClick
        end
        object WTTTSChanCombo: TComboBox
          Left = 12
          Top = 28
          Width = 128
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = '11'
          Items.Strings = (
            '11'
            '12'
            '13'
            '14'
            '15'
            '16'
            '17'
            '18'
            '19'
            '20'
            '21'
            '22'
            '23'
            '24'
            '25'
            '26')
        end
        object StreamRateEdit: TEdit
          Left = 63
          Top = 102
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 2
          Text = '10'
        end
        object StartStreamBtn: TButton
          Left = 160
          Top = 133
          Width = 105
          Height = 25
          Caption = 'Start Stream'
          Enabled = False
          TabOrder = 4
          OnClick = WTTTSBtnClick
        end
        object StopStreamBtn: TButton
          Left = 160
          Top = 165
          Width = 105
          Height = 25
          Caption = 'Stop Stream'
          Enabled = False
          TabOrder = 5
          OnClick = WTTTSBtnClick
        end
        object SetStreamRateBtn: TButton
          Left = 160
          Top = 100
          Width = 105
          Height = 25
          Caption = 'Set Stream Rate'
          Enabled = False
          TabOrder = 3
          OnClick = SetStreamRateBtnClick
        end
        object SetRFCRateBtn: TButton
          Left = 160
          Top = 62
          Width = 105
          Height = 25
          Caption = 'Set RFC Rate'
          Enabled = False
          TabOrder = 6
          OnClick = SetRFCRateBtnClick
        end
        object RFCRateEdit: TEdit
          Left = 63
          Top = 64
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 7
          Text = '1000'
        end
      end
      object TesTORKPgCtrl: TPageControl
        Left = 308
        Top = 22
        Width = 338
        Height = 459
        ActivePage = DataSheet
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 3
        object RFCSheet: TTabSheet
          Caption = 'RFC / Status'
          DesignSize = (
            330
            431)
          object WTTTSStatusGrid: TValueListEditor
            Left = 8
            Top = 8
            Width = 312
            Height = 414
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 0
            TitleCaptions.Strings = (
              'Parameter'
              'Value')
            ColWidths = (
              150
              156)
          end
        end
        object DataSheet: TTabSheet
          Caption = 'Data'
          ImageIndex = 1
          DesignSize = (
            330
            431)
          object StreamValueEditor: TValueListEditor
            Left = 8
            Top = 8
            Width = 312
            Height = 414
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 0
            TitleCaptions.Strings = (
              'Parameter'
              'Value')
            ColWidths = (
              150
              156)
          end
        end
      end
      object TempCompGB: TGroupBox
        Left = 16
        Top = 323
        Width = 281
        Height = 60
        Caption = ' Temp Comp '
        TabOrder = 2
        object ShowTempCompBtn: TButton
          Left = 160
          Top = 22
          Width = 105
          Height = 25
          Caption = 'Show Temp Comp'
          Enabled = False
          TabOrder = 0
          OnClick = ShowTempCompBtnClick
        end
      end
    end
    object AutoSheet: TTabSheet
      Caption = 'Auto'
      ImageIndex = 2
      object AutoParamsGB: TGroupBox
        Left = 16
        Top = 16
        Width = 620
        Height = 91
        Caption = ' Parameters '
        TabOrder = 0
        object RFCTimeLB: TLabel
          Left = 12
          Top = 27
          Width = 43
          Height = 13
          BiDiMode = bdLeftToRight
          Caption = 'RFC Iime'
          ParentBiDiMode = False
        end
        object StreamTimeLB: TLabel
          Left = 12
          Top = 58
          Width = 59
          Height = 13
          Caption = 'Stream Time'
        end
        object RFCSecsLB: TLabel
          Left = 202
          Top = 27
          Width = 29
          Height = 13
          Caption = '(secs)'
        end
        object StreamSecLB: TLabel
          Left = 202
          Top = 58
          Width = 29
          Height = 13
          Caption = '(secs)'
        end
        object RFCTimeEdit: TEdit
          Left = 107
          Top = 24
          Width = 86
          Height = 21
          NumbersOnly = True
          TabOrder = 0
          Text = '90'
        end
        object StreamTimeEdit: TEdit
          Left = 107
          Top = 55
          Width = 86
          Height = 21
          NumbersOnly = True
          TabOrder = 1
          Text = '30'
        end
        object AutoStartBtn: TButton
          Left = 258
          Top = 22
          Width = 106
          Height = 25
          Caption = 'Start'
          TabOrder = 2
          OnClick = AutoStartBtnClick
        end
        object AutoStopBtn: TButton
          Left = 258
          Top = 53
          Width = 106
          Height = 25
          Caption = 'Stop'
          Enabled = False
          TabOrder = 3
          OnClick = AutoStopBtnClick
        end
      end
      object LoggingGB: TGroupBox
        Left = 16
        Top = 113
        Width = 620
        Height = 92
        Caption = ' Logging '
        TabOrder = 1
        object RFCDataCB: TCheckBox
          Left = 12
          Top = 28
          Width = 97
          Height = 17
          Caption = 'RFC Data'
          TabOrder = 0
        end
        object StreamDataCB: TCheckBox
          Left = 12
          Top = 60
          Width = 97
          Height = 17
          Caption = 'Stream Data'
          TabOrder = 2
        end
        object RFCDataFileEdit: TEdit
          Left = 107
          Top = 26
          Width = 446
          Height = 21
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 1
        end
        object StreamDataFileEdit: TEdit
          Left = 107
          Top = 58
          Width = 446
          Height = 21
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 3
        end
        object RFCOpenFileBtn: TButton
          Left = 567
          Top = 24
          Width = 36
          Height = 25
          Caption = '...'
          TabOrder = 4
          OnClick = RFCOpenFileBtnClick
        end
        object StreamOpenFileBtn: TButton
          Left = 567
          Top = 55
          Width = 36
          Height = 25
          Caption = '...'
          TabOrder = 5
          OnClick = StreamOpenFileBtnClick
        end
      end
      object AutoModeStatsGB: TGroupBox
        Left = 16
        Top = 211
        Width = 620
        Height = 162
        Caption = ' Stats '
        TabOrder = 2
        object Label7: TLabel
          Left = 12
          Top = 75
          Width = 75
          Height = 13
          Caption = 'Start Streaming'
        end
        object Label8: TLabel
          Left = 12
          Top = 103
          Width = 73
          Height = 13
          Caption = 'Stop Streaming'
        end
        object Label9: TLabel
          Left = 107
          Top = 52
          Width = 86
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = 'Requests Sent'
        end
        object Label10: TLabel
          Left = 211
          Top = 51
          Width = 86
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = 'Retries Sent'
        end
        object Label11: TLabel
          Left = 12
          Top = 23
          Width = 66
          Height = 13
          Caption = 'Current State'
        end
        object Label12: TLabel
          Left = 315
          Top = 51
          Width = 86
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = 'Max Retries'
        end
        object Label13: TLabel
          Left = 12
          Top = 131
          Width = 60
          Height = 13
          Caption = 'Missed RFCs'
        end
        object ClearStatsBtn: TButton
          Left = 528
          Top = 126
          Width = 75
          Height = 25
          Caption = 'Clear'
          TabOrder = 0
          OnClick = ClearStatsBtnClick
        end
        object StartStreamReqSentPanel: TPanel
          Left = 107
          Top = 71
          Width = 86
          Height = 22
          BevelKind = bkSoft
          BevelOuter = bvNone
          Caption = '0'
          TabOrder = 1
        end
        object StopStreamReqSentPanel: TPanel
          Left = 107
          Top = 99
          Width = 86
          Height = 22
          BevelKind = bkSoft
          BevelOuter = bvNone
          Caption = '0'
          TabOrder = 2
        end
        object StartStreamRetSentPanel: TPanel
          Left = 211
          Top = 71
          Width = 86
          Height = 22
          BevelKind = bkSoft
          BevelOuter = bvNone
          Caption = '0'
          TabOrder = 3
        end
        object StopStreamRetSentPanel: TPanel
          Left = 211
          Top = 99
          Width = 86
          Height = 22
          BevelKind = bkSoft
          BevelOuter = bvNone
          Caption = '0'
          TabOrder = 4
        end
        object AutoModeStatePanel: TPanel
          Left = 107
          Top = 19
          Width = 190
          Height = 22
          BevelKind = bkSoft
          BevelOuter = bvNone
          Caption = 'Auto Mode Off'
          TabOrder = 5
        end
        object StartStreamMaxRetPanel: TPanel
          Left = 315
          Top = 71
          Width = 86
          Height = 22
          BevelKind = bkSoft
          BevelOuter = bvNone
          Caption = '0'
          TabOrder = 6
        end
        object StopStreamMaxRetPanel: TPanel
          Left = 315
          Top = 99
          Width = 86
          Height = 22
          BevelKind = bkSoft
          BevelOuter = bvNone
          Caption = '0'
          TabOrder = 7
        end
        object MissedRFCsPanel: TPanel
          Left = 107
          Top = 127
          Width = 86
          Height = 22
          BevelKind = bkSoft
          BevelOuter = bvNone
          Caption = '0'
          TabOrder = 8
        end
        object MaxMissedRFCsPanel: TPanel
          Left = 315
          Top = 127
          Width = 86
          Height = 22
          BevelKind = bkSoft
          BevelOuter = bvNone
          Caption = '0'
          TabOrder = 9
        end
      end
      object PortsGB: TGroupBox
        Left = 16
        Top = 379
        Width = 620
        Height = 60
        Caption = ' Ports '
        TabOrder = 3
        object AutoConnBtn: TButton
          Left = 12
          Top = 20
          Width = 85
          Height = 25
          Caption = 'Auto Connect'
          TabOrder = 0
          OnClick = AutoConnBtnClick
        end
      end
    end
  end
  object ExitBtn: TButton
    Left = 595
    Top = 533
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Exit'
    TabOrder = 1
    OnClick = ExitBtnClick
  end
  object WTTTSPollTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = WTTTSPollTimerTimer
    Left = 8
    Top = 518
  end
  object BRPollTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = BRPollTimerTimer
    Left = 80
    Top = 518
  end
  object SaveLogDlg: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All File (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Log File As...'
    Left = 144
    Top = 518
  end
end
