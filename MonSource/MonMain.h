#ifndef MonMainH
#define MonMainH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <time.h>
#include "CommObj.h"
#include "WTTTSProtocolUtils.h"
#include <Vcl.Menus.hpp>
#include "AutoConnDlg.h"


class TMainForm : public TForm
{
__published:	// IDE-managed Components
    TPageControl *MainPgCtrl;
    TButton *ExitBtn;
    TTabSheet *BaseRadioSheet;
    TTabSheet *WTTTSSheet;
    TValueListEditor *BRStatusGrid;
    TGroupBox *BaseRadioCommsGB;
    TEdit *BRPortEdit;
    TLabel *Label1;
    TButton *BRConnectBtn;
    TButton *BRDisconnectBtn;
    TGroupBox *BRActionsGB;
    TButton *SetBRChanBtn;
    TComboBox *BRChanCombo;
    TGroupBox *WTTTSCommsGB;
    TLabel *Label2;
    TEdit *WTTTSPortEdit;
    TButton *WTTTSConnectBtn;
    TButton *WTTTSDisconnectBtn;
    TGroupBox *WTTTSActionsGB;
    TButton *SetWTTTSChanBtn;
    TComboBox *WTTTSChanCombo;
    TLabel *Label3;
    TEdit *WTTTSSpeedEdit;
    TLabel *Label4;
    TEdit *BRSpeedEdit;
    TTimer *WTTTSPollTimer;
    TTimer *BRPollTimer;
    TComboBox *BRRadioNbrCombo;
    TLabel *Label5;
    TEdit *StreamRateEdit;
    TButton *StartStreamBtn;
    TLabel *Label6;
    TButton *StopStreamBtn;
    TPageControl *TesTORKPgCtrl;
    TTabSheet *RFCSheet;
    TTabSheet *DataSheet;
    TValueListEditor *WTTTSStatusGrid;
    TValueListEditor *StreamValueEditor;
    TButton *SetStreamRateBtn;
    TTabSheet *AutoSheet;
    TGroupBox *AutoParamsGB;
    TLabel *RFCTimeLB;
    TLabel *StreamTimeLB;
    TEdit *RFCTimeEdit;
    TEdit *StreamTimeEdit;
    TLabel *RFCSecsLB;
    TLabel *StreamSecLB;
    TButton *AutoStartBtn;
    TButton *AutoStopBtn;
    TGroupBox *LoggingGB;
    TCheckBox *RFCDataCB;
    TCheckBox *StreamDataCB;
    TEdit *RFCDataFileEdit;
    TEdit *StreamDataFileEdit;
    TButton *RFCOpenFileBtn;
    TButton *StreamOpenFileBtn;
    TSaveDialog *SaveLogDlg;
    TGroupBox *AutoModeStatsGB;
    TButton *ClearStatsBtn;
    TLabel *Label7;
    TLabel *Label8;
    TLabel *Label9;
    TLabel *Label10;
    TPanel *StartStreamReqSentPanel;
    TPanel *StopStreamReqSentPanel;
    TPanel *StartStreamRetSentPanel;
    TPanel *StopStreamRetSentPanel;
    TLabel *Label11;
    TPanel *AutoModeStatePanel;
    TLabel *Label12;
    TPanel *StartStreamMaxRetPanel;
    TPanel *StopStreamMaxRetPanel;
    TLabel *Label13;
    TPanel *MissedRFCsPanel;
    TPanel *MaxMissedRFCsPanel;
    TCheckBox *BROutput1CB;
    TCheckBox *BROutput2CB;
    TCheckBox *BROutput3CB;
    TCheckBox *BROutput4CB;
    TButton *BRSetOutputBtn;
    TGroupBox *TempCompGB;
    TButton *ShowTempCompBtn;
    TButton *SetRFCRateBtn;
    TEdit *RFCRateEdit;
    TLabel *Label14;
    TGroupBox *PortsGB;
    TButton *AutoConnBtn;
    void __fastcall ExitBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall BRDisconnectBtnClick(TObject *Sender);
    void __fastcall BRConnectBtnClick(TObject *Sender);
    void __fastcall SetBRChanBtnClick(TObject *Sender);
    void __fastcall WTTTSConnectBtnClick(TObject *Sender);
    void __fastcall WTTTSDisconnectBtnClick(TObject *Sender);
    void __fastcall WTTTSBtnClick(TObject *Sender);
    void __fastcall WTTTSPollTimerTimer(TObject *Sender);
    void __fastcall BRPollTimerTimer(TObject *Sender);
    void __fastcall SetStreamRateBtnClick(TObject *Sender);
    void __fastcall RFCOpenFileBtnClick(TObject *Sender);
    void __fastcall StreamOpenFileBtnClick(TObject *Sender);
    void __fastcall AutoStartBtnClick(TObject *Sender);
    void __fastcall AutoStopBtnClick(TObject *Sender);
    void __fastcall ClearStatsBtnClick(TObject *Sender);
    void __fastcall BRSetOutputBtnClick(TObject *Sender);
    void __fastcall ShowTempCompBtnClick(TObject *Sender);
    void __fastcall SetRFCRateBtnClick(TObject *Sender);
    void __fastcall AutoConnBtnClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);

private:
    // WTTTS specific functions
    bool DoWTTTSConnect( int iWTTTSPort, DWORD dwBitRate, String& sErrMsg );

    bool SendWTTTSCmd( BYTE cmdType, TESTORK_DATA_UNION& payloadData, BYTE payloadLen );

    void UpdateWTTTSRFCValues( const GENERIC_RFC_PKT& rfcPkt );
    void UpdateWTTTSStreamValue( BYTE seqNbr, WORD msecs, const WTTTS_STREAM_DATA& streamPkt );

    bool LogStreamPacket( UnicodeString logFileName, const WTTTS_PKT_HDR& pktHdr, const WTTTS_STREAM_DATA& streamData );
    bool LogRFCPacket( UnicodeString logFileName, const WTTTS_PKT_HDR& pktHdr, const GENERIC_RFC_PKT& rfcData );

    // Base Radio specific functions
    bool DoBRConnect( int iBRPort, DWORD dwBitRate, String& sErrMsg );

    bool SendBRCmd( BYTE cmdType, BASE_STN_DATA_UNION& payloadData, BYTE payloadLen );

    void UpdateBRStatusValues( const BASE_STATUS_STATUS_PKT& statusPkt );

    BYTE GetSelectedRadioNbr( void );

    // Comm objects
    #define RX_BUFF_SIZE 2048

    typedef struct {
        CCommObj*  commPort;
        BYTE       rxBuffer[RX_BUFF_SIZE];
        DWORD      rxBuffCount;
        DWORD      lastRxTime;
        DWORD      lastPktTime;
        bool       bUsingUDP;   // True if using UDP comms
        AnsiString sIPAddr;     // If using UDP, IP addr that last sent a packet
        WORD       wUDPPort;    // If using UDP, port from which last packet came from
    } PORT_INFO;

    PORT_INFO m_BRPort;
    PORT_INFO m_WTTTSPort;

    // Common handlers
    bool      CreatePort( int portNbr, DWORD portSpeed, PORT_INFO& portInfo, String& sErrMsg );
    void      ReleasePort( PORT_INFO& portInfo );

    bool      ValidatePortSettings( TEdit* portEdit, TEdit* speedEdit );

    BYTE      GetChanNbr( TComboBox* pwrCombo );

    void      PollPortForData( PORT_INFO& portInfo );

    void      UpdateStatusGrid( TValueListEditor* aGrid, const UnicodeString& key, const UnicodeString& text );
    void      ClearStatusGrid( TValueListEditor* aGrid );

    // Auto-state allows a user to force the monitor to automatically cycle
    // between RFC and streaming at set intervals.
    typedef enum {
        AS_OFF,
        AS_RFC_MODE,
        AS_STREAM_STARTING,
        AS_STREAM_MODE,
        AS_STREAM_STOPPING,
        NBR_AUTO_STATE_MODES
    } AUTO_STATE_MODE;

    AUTO_STATE_MODE asMode;
    time_t          asNextChangeTime;

    // In auto mode, we track how long it takes for a command to be
    // acted on by the TesTORK. The following structs are used to
    // count these stats
    typedef struct {
        DWORD requestsSent;
        DWORD retriesSent;
        DWORD currRetries;
        DWORD maxRetries;
    } AUTO_MODE_STAT;

    AUTO_MODE_STAT amsStartStreamStats;
    AUTO_MODE_STAT amsStopStreamStats;

    typedef struct {
        DWORD rfcRate;    // msecs
        DWORD tLastRFC;   // msecs
        DWORD missedRFCs;
        DWORD currMissedRFCs;
        DWORD maxMissedRFCs;
    } RFC_STATS;

    RFC_STATS m_rfcStats;

    void RefreshAutoStats( void );

    bool m_bAutoScanCancelled;

    void __fastcall WMDoAutoConn         ( TMessage &Message );
    void __fastcall WMDoAutoConnCancelled( TMessage &Message );

    #define WM_DO_AUTO_CONN         ( WM_APP + 1 )
    #define WM_AUTO_CONN_CANCELLED  ( WM_APP + 2 )

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_DO_AUTO_CONN,        TMessage, WMDoAutoConn          )
      MESSAGE_HANDLER( WM_AUTO_CONN_CANCELLED, TMessage, WMDoAutoConnCancelled )
    END_MESSAGE_MAP(TForm)

public:		// User declarations
    __fastcall TMainForm(TComponent* Owner);
};

extern PACKAGE TMainForm *MainForm;

#endif
