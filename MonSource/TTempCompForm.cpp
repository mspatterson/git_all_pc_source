#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>

#include "TTempCompForm.h"

#pragma package(smart_init)
#pragma resource "*.dfm"

TTempCompForm *TempCompForm;

// Assume the packet rate is every 1000 millisecs.
//   Improvement: Each packet contains the RFC Rate
//      therefore this code could be restructured to use the actual rate.
const int RFCRateInMsecs = 1000;

const int RFCListSize = ( ( 60000 /*msecs/min*/ / RFCRateInMsecs ) * 60 /*min*/ );


__fastcall TTempCompForm::TTempCompForm(TComponent* Owner) : TForm(Owner)
{
    // This form is auto created and the user could show and close it multiple
    //   times therefore this code will only be executed once.
    m_iRFCListSize = 0;
    m_iRFCCount    = 0;
    m_pRFCList     = NULL;

    // Set Line widths.
    RawTensionSeries->LinePen->Width  = 2;
    CompTensionSeries->LinePen->Width = 2;
    RTDSeries->LinePen->Width         = 2;
}


void TTempCompForm::ShowTempCompForm( void )
{
    // Setup the environment and show the form modally
    //  Note: Initialization here because user can close and re-show this form multiple times.

    // Intialize the display to not be paused (set to true then call handler which sets it to false).
    m_bPause = true;
    PauseBtnClick( PauseBtn );

    // Initialize Reset Start to pending.
    ResetStartBtn->Tag     = 1;
    ResetStartBtn->Enabled = false;

    // Initialize the user specified values (TEdits).
    InitUserSpecified();

    // Initialize the RFC history queue.
    if( m_pRFCList != NULL )
    {
        delete [] m_pRFCList;
        m_pRFCList = NULL;
    }

    m_iRFCListSize = RFCListSize;
    m_iRFCCount    = 0;

    m_pRFCList = new RFC_ENTRY[RFCListSize];

    // Start with the graph all empty data
    ClearGraphBtnClick( ClearGraphBtn );

    ShowModal();

    // When the view is closed, clear our history
    m_iRFCListSize = 0;
    m_iRFCCount    = 0;

    delete [] m_pRFCList;
    m_pRFCList = NULL;
}


void TTempCompForm::ProcessRFC( const GENERIC_RFC_PKT& currRFC )
{
    if( m_bPause )
        return;

    // Process the passed RFC packet. If we have no list allocated,
    // we aren't visible so ignore the packet
    if( m_pRFCList == NULL )
        return;

    // Calculate the offsets using the first m_iOffsetNum values; ignore those values.
    if( m_iRFCCount < m_iOffsetNum )
    {
        m_fOffsetTen000 += currRFC.tension000;
        m_fOffsetTen090 += currRFC.tension090;
        m_fOffsetTen180 += currRFC.tension180;
        m_fOffsetTen270 += currRFC.tension270;
        m_fOffsetRTD    += currRFC.rfcV2DataPresent ? currRFC.rtdReading : currRFC.pdsSwitch;

        m_iRFCCount++;

        return;
    }

    // Divide the offset values by the number of summed values.
    if( m_iRFCCount == m_iOffsetNum )
    {
        m_fOffsetTen000 /= m_iOffsetNum;
        m_fOffsetTen090 /= m_iOffsetNum;
        m_fOffsetTen180 /= m_iOffsetNum;
        m_fOffsetTen270 /= m_iOffsetNum;
        m_fOffsetRTD    /= m_iOffsetNum;
    }

    m_iRFCCount++;

    if( ResetStartBtn->Tag == 1 )
    {
        // There was a pending reset start.
        // Reset the reset start button.
        ResetStartBtn->Tag     = 0;
        ResetStartBtn->Enabled = true;

        // Determine a start time in the past.
        TDateTime tPt = Now();
        int iInc = - ( RFCRateInMsecs * m_iRFCListSize );
        tPt = IncMilliSecond( tPt, iInc );

        // Fill the history queue with this packet.
        for( int iItem = 0; iItem < m_iRFCListSize - 1; iItem++ )
        {
            // Reset all values in the queue to the current packet.
            m_pRFCList[iItem].rfcPkt  = currRFC;

            // Set the past time.
            m_pRFCList[iItem].pktTime = tPt;

            // Inc to the next past time.
            tPt = IncMilliSecond( tPt, RFCRateInMsecs );
        }
    }
    else
    {
        // Shift to make room for the new packet.
        for( int iItem = 0; iItem < m_iRFCListSize - 1; iItem++ )
            m_pRFCList[iItem] = m_pRFCList[iItem+1];
    }

    // Add the item to the end of the queue.
    m_pRFCList[m_iRFCListSize - 1].pktTime = Now();
    m_pRFCList[m_iRFCListSize - 1].rfcPkt  = currRFC;

    // Now refresh the graph
    RefreshGraph();
}


void __fastcall TTempCompForm::ClearGraphBtnClick(TObject *Sender)
{
    m_iRFCCount = 0;

    // Initialize the offsets.
//    m_iOffsetNum    = 30;
    m_iOffsetNum    = 1;
    m_fOffsetTen000 = 0.0;
    m_fOffsetTen090 = 0.0;
    m_fOffsetTen180 = 0.0;
    m_fOffsetTen270 = 0.0;
    m_fOffsetRTD    = 0.0;

    // The following clear and initialize must be done once
    //   but the pending reset will over write the values.
    RawTensionSeries->Clear();
    CompTensionSeries->Clear();
    RTDSeries->Clear();

    // Now init the list with empty values for times in the past
    TDateTime tPt = Now();
    int iInc = - ( RFCRateInMsecs * m_iRFCListSize );
    tPt = IncMilliSecond( tPt, iInc );

    for( int iItem = 0; iItem < m_iRFCListSize; iItem++ )
    {
        RawTensionSeries->AddXY ( tPt.Val, 0.0 );
        CompTensionSeries->AddXY( tPt.Val, 0.0 );
        RTDSeries->AddXY        ( tPt.Val, 0.0 );

        m_pRFCList[iItem].pktTime = tPt;

        // Inc the X axis value by an arbitrary value for now
        tPt = IncMilliSecond( tPt, RFCRateInMsecs );
    }

    // Initialize Reset Start to pending.
    ResetStartBtn->Tag     = 1;
    ResetStartBtn->Enabled = false;

    // Initialize the most recent raw and calculated values to unknown.
    RecentTimeEdit->Text     = "?";
    RecentRTDRawEdit->Text   = "?";
    RecentRTDAvgEdit->Text   = "?";
    RecentRTDSlpEdit->Text   = "?";
    RecentTenRawEdit->Text   = "?";
    RecentTenFirstEdit->Text = "?";
    RecentTenCompEdit->Text  = "?";
    RecentTenAvgEdit->Text   = "?";

    RefreshGraph();
}


void TTempCompForm::RefreshGraph( void )
{
    COMP_VALUES cv;

    // Get user specified values for the Averagers.
    GetUserSpecified( cv );

    // Construct Averagers.
    cv.pRTDAvg = new TExpAverage( cv.fRTDAlpha, cv.fRTDMaxVar );
    cv.pTenAvg = new TExpAverage( cv.fTenAlpha, cv.fTenMaxVar );

    // Initialize the calculations with the first packet.
    InitializeValues( m_pRFCList[0].rfcPkt, cv );

    RawTensionSeries->BeginUpdate();
    CompTensionSeries->BeginUpdate();
    RTDSeries->BeginUpdate();

    for( int iItem = 0; iItem < m_iRFCListSize; iItem++ )
    {
        CalcValues( m_pRFCList[iItem].rfcPkt, cv );

        // Specify what to display in the graph.
        RawTensionSeries->YValue[iItem]  = cv.fTen_Raw;
        CompTensionSeries->YValue[iItem] = cv.fTen_Avg;
        RTDSeries->YValue[iItem]         = cv.fRTD_Slp;

        // All items are already in the series - just update them
        RawTensionSeries->XValue[iItem]  = m_pRFCList[iItem].pktTime.Val;
        CompTensionSeries->XValue[iItem] = m_pRFCList[iItem].pktTime.Val;
        RTDSeries->XValue[iItem]         = m_pRFCList[iItem].pktTime.Val;
    }

    RawTensionSeries->EndUpdate();
    CompTensionSeries->EndUpdate();
    RTDSeries->EndUpdate();

    CompChart->BottomAxis->AdjustMaxMin();
    CompChart->Refresh();

    // Deleted Averagers
    delete cv.pRTDAvg;
    delete cv.pTenAvg;

    // Display the most recent raw and calculated values.
    RecentTimeEdit->Text = m_pRFCList[m_iRFCListSize - 1].pktTime.TimeString();

    UnicodeString sVal;
    sVal.printf( L"%lf", cv.fRTD_Off );
    RecentRTDRawEdit->Text   = sVal;

    sVal.printf( L"%lf", cv.fRTD_Avg );
    RecentRTDAvgEdit->Text   = sVal;

    sVal.printf( L"%lf", cv.fRTD_Slp );
    RecentRTDSlpEdit->Text   = sVal;

    sVal.printf( L"%lf", cv.fTen_Raw );
    RecentTenRawEdit->Text   = sVal;

    sVal.printf( L"%lf", cv.fTen_First );
    RecentTenFirstEdit->Text = sVal;

    sVal.printf( L"%lf", cv.fTen_Comp );
    RecentTenCompEdit->Text  = sVal;

    sVal.printf( L"%lf", cv.fTen_Avg );
    RecentTenAvgEdit->Text   = sVal;
}


void __fastcall TTempCompForm::RedrawBtnClick(TObject *Sender)
{
    RefreshGraph();
}

void __fastcall TTempCompForm::PauseBtnClick(TObject *Sender)
{
    m_bPause = !m_bPause;

    if( m_bPause )
    {
        PauseBtn->Caption = "Resume";
        PauseBtn->Hint    = "Resume: process incoming packets; each packet shifts display.";
    }
    else
    {
        PauseBtn->Caption = "Pause";
        PauseBtn->Hint    = "Pause: ignore incoming packets; display does not shift.";
    }
}


void TTempCompForm::InitUserSpecified( void )
{
    RTDAlphaEdit->Text  = "0.0016666";
    TenAlphaEdit->Text  = "0.001";
    RTDMaxVarEdit->Text = "100000";
    TenMaxVarEdit->Text = "100000";

    RTDSpanEdit->Text   = "1.0";
    TenSpanEdit->Text   = "1.0";
    RTDOffsetEdit->Text = "0";
    TenOffsetEdit->Text = "0";

    RisingSlopeEdit->Text   =  "2.0";
    FallingSlopeEdit->Text  = "-2.0";
    RisingFactorEdit->Text  =  "-450.0";
    FallingFactorEdit->Text =  "-450.0";
}


void TTempCompForm::GetUserSpecified( COMP_VALUES& cv )
{
    // Get the current Alpha values.
    cv.fRTDAlpha  = StrToFloatDef( RTDAlphaEdit->Text, 0.0016666 );
    cv.fTenAlpha  = StrToFloatDef( TenAlphaEdit->Text, 0.001 );
    cv.fRTDMaxVar = StrToFloatDef( RTDMaxVarEdit->Text, 100000 );
    cv.fTenMaxVar = StrToFloatDef( TenMaxVarEdit->Text, 100000 );

    // Get the RTD and Tension Span and Offset values.
    cv.fRTDSpan   = StrToFloatDef( RTDSpanEdit->Text,   1.0 );
    cv.fTenSpan   = StrToFloatDef( TenSpanEdit->Text,   1.0 );
    cv.iRTDOffset = StrToIntDef  ( RTDOffsetEdit->Text, 0 );
    cv.iTenOffset = StrToIntDef  ( TenOffsetEdit->Text, 0 );

    // Get rising and falling slope and factor values.
    cv.fRisingSlope   = StrToFloatDef( RisingSlopeEdit->Text,   2.0 );
    cv.fFallingSlope  = StrToFloatDef( FallingSlopeEdit->Text, -2.0 );
    cv.fRisingFactor  = StrToFloatDef( RisingFactorEdit->Text,  -450.0 );
    cv.fFallingFactor = StrToFloatDef( FallingFactorEdit->Text, -450.0 );
}


void TTempCompForm::GetValues( const GENERIC_RFC_PKT& rfcPkt, COMP_VALUES& cv )
{
    // Get the raw values.
    cv.fRTD_Raw = rfcPkt.rfcV2DataPresent ? rfcPkt.rtdReading : rfcPkt.pdsSwitch;
    cv.fTen000  = rfcPkt.tension000;
    cv.fTen090  = rfcPkt.tension090;
    cv.fTen180  = rfcPkt.tension180;
    cv.fTen270  = rfcPkt.tension270;

    // Remove the previously determined offsets.
    cv.fRTD_Off    = cv.fRTD_Raw - m_fOffsetRTD;
    cv.fTen000_Off = cv.fTen000  - m_fOffsetTen000;
    cv.fTen090_Off = cv.fTen090  - m_fOffsetTen090;
    cv.fTen180_Off = cv.fTen180  - m_fOffsetTen180;
    cv.fTen270_Off = cv.fTen270  - m_fOffsetTen270;

    // Calculate the average tension.
    cv.fTen_Raw  = cv.fTen000_Off;
    cv.fTen_Raw += cv.fTen090_Off;
    cv.fTen_Raw += cv.fTen180_Off;
    cv.fTen_Raw += cv.fTen270_Off;
    cv.fTen_Raw /= 4.0;

    // Apply standard compensation.
    cv.fTen_Raw = cv.fTenSpan * ( cv.fTen_Raw - cv.iTenOffset );
    cv.fRTD_Off = cv.fRTDSpan * ( cv.fRTD_Off - cv.iRTDOffset );
}


void TTempCompForm::InitializeValues( const GENERIC_RFC_PKT& rfcPkt, COMP_VALUES& cv )
{
    // Get the user specified parameters.
    GetUserSpecified( cv );

    // Get the RTD and Tension values from the packet.
    GetValues( rfcPkt, cv );

    // Initialize Averagers
    cv.pRTDAvg->Reset( cv.fRTD_Off );
    cv.pTenAvg->Reset( cv.fTen_Raw );
}


void TTempCompForm::CalcValues( const GENERIC_RFC_PKT& rfcPkt, COMP_VALUES& cv )
{
    // Get the RTD and Tension values from the packet.
    GetValues( rfcPkt, cv );

    // Save the old average RTD.
    float fOldRTDAvg = cv.pRTDAvg->AverageValue;

    // Update the average with the new RTD value.
    cv.pRTDAvg->AddValue( cv.fRTD_Off );

    // Save the new average RTD.
    cv.fRTD_Avg = cv.pRTDAvg->AverageValue;

    // Calculate the slope of the averaged RTD values: new - old.
    cv.fRTD_Slp = cv.fRTD_Avg - fOldRTDAvg;

    // Initialize the compensated tension to the raw tension.
    cv.fTen_Comp = cv.fTen_Raw;

    // Apply slope compensation to tension if RTD slope limits exceeded.
    if     ( cv.fRTD_Slp > cv.fRisingSlope )
        cv.fTen_Comp += cv.fRTD_Slp * cv.fRisingFactor;
    else if( cv.fRTD_Slp < cv.fFallingSlope )
        cv.fTen_Comp += cv.fRTD_Slp * cv.fFallingFactor;

    // Update the average with the new Tension value.
    cv.pTenAvg->AddValue( cv.fTen_Comp );

    // Save the new average Tension.
    cv.fTen_Avg = cv.pTenAvg->AverageValue;
}


void __fastcall TTempCompForm::ResetStartBtnClick(TObject *Sender)
{
    // Reset Start is pending.
    ResetStartBtn->Tag     = 1;
    ResetStartBtn->Enabled = false;
}


void __fastcall TTempCompForm::PrintScreenBtnClick(TObject *Sender)
{
    // Will fit the form to the printer page size.
    PrintScale = poPrintToFit;

    // Print the form to the default printer.
    Print();
}


void __fastcall TTempCompForm::ExportBtnClick(TObject *Sender)
{
    // Get the name of the file which will receive the exported data.
    if( SaveLogDlg->Execute() == false )
        return;

    UnicodeString logFileName = SaveLogDlg->FileName;

    // Get the user specified values for the Averagers
    COMP_VALUES cv;
    GetUserSpecified( cv );

    // Construct Averagers.
    cv.pRTDAvg = new TExpAverage( cv.fRTDAlpha, cv.fRTDMaxVar );
    cv.pTenAvg = new TExpAverage( cv.fTenAlpha, cv.fTenMaxVar );

    // Initialize the calculations to start with the first packet values.
    InitializeValues( m_pRFCList[0].rfcPkt, cv );

    // Remember the time of the first packet.
    TDateTime tFirstDateTime = m_pRFCList[0].pktTime;

    for( int iItem = 0; iItem < m_iRFCListSize; iItem++ )
    {
        // Calculate the values for the next packet.
        CalcValues( m_pRFCList[iItem].rfcPkt, cv );

        // Determine the number of milliseconds since the first packet.
        TDateTime tItemDateTime = m_pRFCList[iItem].pktTime;

        int iMilliSecs = MilliSecondsBetween( tItemDateTime, tFirstDateTime );

        // Write values to csv file.
        LogRFCPacket( logFileName, iItem, iMilliSecs, m_pRFCList[iItem].rfcPkt, cv );
    }

    // Deleted Averagers.
    delete cv.pRTDAvg;
    delete cv.pTenAvg;
}


bool TTempCompForm::LogRFCPacket( UnicodeString logFileName, int iItem, int iMilliSecs, GENERIC_RFC_PKT& rfcData, COMP_VALUES& cv )
{
    // Writes the passed RFC packet to file (based on TMainForm::LogRFCPacket)
    TFileStream* logStream = NULL;

    bool logGood = false;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            AnsiString sHeader( "RxTime,SeqNbr,msecs,temperature,battVolts,battUsed,battType,auxIn,rpm,lastReset,rfChannel,currMode,rfcRate,rfcTimeout,streamRate,streamTimeout,pairingTimeout,Torque045,Torque225,Torque045_R3,Torque225_F3,Tension000,Tension090,Tension180,Tension270,CompassX,CompassY,CompassZ,AccelX,AccelY,AccelZ,RTD,DevID" );

            // Append title for all values involved in calculations.
            sHeader += ",RTDAlpha,RTDMaxVar,TenAlpha,TenMaxVar";
            sHeader += ",RTDSpan,RTDOffset,TenSpan,TenOffset";
            sHeader += ",RisingSlope,RisingFactor,FallingSlope,FallingFactor";
            sHeader += ",RTD_Raw,RTD_Avg,RTD_Slope";
            sHeader += ",Ten_Raw,Ten_Comp,Ten_Avg";

            sHeader += "\r";

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        // Convert all structure members first
        float fTemperature    = rfcData.fTemperature;
        float fBattVolts      = rfcData.fBattVoltage;
        int   iBattUsed       = (int)rfcData.battUsed;
        int   iBattType       = (int)rfcData.battType;
        int   iRpm            = (int)rfcData.rpm;
        int   iLastReset      = (int)rfcData.lastReset;
        int   iRfChannel      = (int)rfcData.rfChannel;
        int   iCurrMode       = (int)rfcData.currMode;
        int   iRfcRate        = (int)rfcData.rfcRate;
        int   iRfcTimeout     = (int)rfcData.rfcTimeout;
        int   iStreamRate     = (int)rfcData.streamRate;
        int   iStreamTimeout  = (int)rfcData.streamTimeout;
        int   iPairingTimeout = (int)rfcData.pairingTimeout;
        int   iAuxInput       = rfcData.pdsSwitch;
        int   iTorque045      = rfcData.torque045;
        int   iTorque225      = rfcData.torque225;
        int   iTorque045_R3   = rfcData.torque045_R3;
        int   iTorque225_R3   = rfcData.torque225_R3;
        int   iTension000     = rfcData.tension000;
        int   iTension090     = rfcData.tension090;
        int   iTension180     = rfcData.tension180;
        int   iTension270     = rfcData.tension270;
        INT16 siCompassX      = rfcData.compassX;
        INT16 siCompassY      = rfcData.compassY;
        INT16 siCompassZ      = rfcData.compassZ;
        INT16 siAccelX        = rfcData.accelX;
        INT16 siAccelY        = rfcData.accelY;
        INT16 siAccelZ        = rfcData.accelZ;
        int   iRTD            = rfcData.rtdReading;

        AnsiString sTime  = Time().TimeString();
        AnsiString sDevID = DeviceIDToStr( rfcData.deviceID );

        AnsiString sLine;
        sLine.printf( "%s,%hu,%hu,%f,%f,%hu,%hu,%i,%u,%hu,%hu,%hu,%hu,%hu,%hu,%hu,%hu,%i,%i,%i,%i,%i,%i,%i,%i,%hi,%hi,%hi,%hi,%hi,%hi,%i,%s",
                      sTime.c_str(), iItem, iMilliSecs,
                      fTemperature, fBattVolts, iBattUsed, iBattType,
                      iAuxInput, iRpm, iLastReset, iRfChannel, iCurrMode,
                      iRfcRate, iRfcTimeout, iStreamRate, iStreamTimeout, iPairingTimeout,
                      iTorque045, iTorque225, iTorque045_R3, iTorque225_R3,
                      iTension000, iTension090, iTension180, iTension270,
                      siCompassX, siCompassY, siCompassZ, siAccelX, siAccelY, siAccelZ,
                      iRTD, sDevID.c_str() );

        // Append all values involved in calculations.
        sLine.cat_printf( ",%f,%f,%f,%f", cv.fRTDAlpha,    cv.fRTDMaxVar,    cv.fTenAlpha,     cv.fTenMaxVar );
        sLine.cat_printf( ",%f,%i,%f,%i", cv.fRTDSpan,     cv.iRTDOffset,    cv.fTenSpan,      cv.iTenOffset );
        sLine.cat_printf( ",%f,%f,%f,%f", cv.fRisingSlope, cv.fRisingFactor, cv.fFallingSlope, cv.fFallingFactor );

        sLine.cat_printf( ",%f,%f,%f", cv.fRTD_Off, cv.fRTD_Avg,  cv.fRTD_Slp );
        sLine.cat_printf( ",%f,%f,%f", cv.fTen_Raw, cv.fTen_Comp, cv.fTen_Avg );

        sLine += "\r";

        logStream->Write( sLine.c_str(), sLine.Length() );

        logGood = true;
    }
    catch( ... )
    {
        logGood = false;
    }

    if( logStream != NULL )
        delete logStream;

    return logGood;
}

