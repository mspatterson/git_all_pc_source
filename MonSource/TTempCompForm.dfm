object TempCompForm: TTempCompForm
  Left = 0
  Top = 0
  Caption = 'Temperature Compensation'
  ClientHeight = 582
  ClientWidth = 957
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    957
    582)
  PixelsPerInch = 96
  TextHeight = 13
  object ControlsGB: TGroupBox
    Left = 8
    Top = 423
    Width = 941
    Height = 151
    Anchors = [akLeft, akRight, akBottom]
    Caption = ' Parameters '
    TabOrder = 0
    DesignSize = (
      941
      151)
    object CloseBtn: TButton
      Left = 854
      Top = 119
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Close'
      ModalResult = 1
      TabOrder = 0
    end
    object ClearGraphBtn: TButton
      Left = 8
      Top = 119
      Width = 75
      Height = 25
      Hint = 'Reset everythin including offsets.'
      Anchors = [akLeft, akBottom]
      Caption = 'Clear Graph'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = ClearGraphBtnClick
    end
    object PauseBtn: TButton
      Left = 170
      Top = 120
      Width = 75
      Height = 24
      Anchors = [akLeft, akBottom]
      Caption = 'Pause'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = PauseBtnClick
    end
    object RedrawBtn: TButton
      Left = 251
      Top = 119
      Width = 87
      Height = 25
      Hint = 'Redraw graph with current parameters.'
      Anchors = [akLeft, akBottom]
      Caption = 'Redraw Graph'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 7
      OnClick = RedrawBtnClick
    end
    object AvgGB: TGroupBox
      Left = 8
      Top = 16
      Width = 244
      Height = 100
      Caption = 'Averaging'
      TabOrder = 1
      object Label1: TLabel
        Left = 11
        Top = 39
        Width = 63
        Height = 13
        Caption = 'RTD Exp Avg'
      end
      object Label2: TLabel
        Left = 11
        Top = 64
        Width = 80
        Height = 13
        Caption = 'Tension Exp Avg'
      end
      object Label3: TLabel
        Left = 112
        Top = 16
        Width = 27
        Height = 13
        Caption = 'Alpha'
      end
      object Label4: TLabel
        Left = 168
        Top = 16
        Width = 64
        Height = 13
        Caption = 'Max Variance'
      end
      object RTDAlphaEdit: TEdit
        Left = 97
        Top = 36
        Width = 65
        Height = 21
        TabOrder = 0
        Text = '0.98'
      end
      object RTDMaxVarEdit: TEdit
        Left = 168
        Top = 36
        Width = 65
        Height = 21
        TabOrder = 2
        Text = '100000'
      end
      object TenAlphaEdit: TEdit
        Left = 96
        Top = 61
        Width = 65
        Height = 21
        TabOrder = 1
        Text = '0.98'
      end
      object TenMaxVarEdit: TEdit
        Left = 168
        Top = 61
        Width = 65
        Height = 21
        TabOrder = 3
        Text = '100000'
      end
    end
    object StdCompGB: TGroupBox
      Left = 259
      Top = 16
      Width = 203
      Height = 100
      Caption = 'Standard Compensation'
      TabOrder = 2
      object Label5: TLabel
        Left = 12
        Top = 39
        Width = 37
        Height = 13
        Caption = 'Tension'
      end
      object Label6: TLabel
        Left = 12
        Top = 64
        Width = 20
        Height = 13
        Caption = 'RTD'
      end
      object Label7: TLabel
        Left = 69
        Top = 16
        Width = 24
        Height = 13
        Caption = 'Span'
      end
      object Label8: TLabel
        Left = 141
        Top = 16
        Width = 31
        Height = 13
        Caption = 'Offset'
      end
      object TenSpanEdit: TEdit
        Left = 54
        Top = 36
        Width = 65
        Height = 21
        TabOrder = 0
        Text = '1.0'
      end
      object RTDSpanEdit: TEdit
        Left = 54
        Top = 61
        Width = 65
        Height = 21
        TabOrder = 1
        Text = '1.0'
      end
      object TenOffsetEdit: TEdit
        Left = 127
        Top = 36
        Width = 65
        Height = 21
        TabOrder = 2
        Text = '0'
      end
      object RTDOffsetEdit: TEdit
        Left = 127
        Top = 61
        Width = 65
        Height = 21
        TabOrder = 3
        Text = '0'
      end
    end
    object SlopeCompGB: TGroupBox
      Left = 470
      Top = 16
      Width = 188
      Height = 100
      Caption = 'Slope Compensation'
      TabOrder = 3
      object Label9: TLabel
        Left = 6
        Top = 39
        Width = 28
        Height = 13
        Caption = 'Rising'
      end
      object Label10: TLabel
        Left = 6
        Top = 64
        Width = 30
        Height = 13
        Caption = 'Falling'
      end
      object Label11: TLabel
        Left = 52
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Slope'
      end
      object Label12: TLabel
        Left = 125
        Top = 16
        Width = 31
        Height = 13
        Caption = 'Factor'
      end
      object RisingSlopeEdit: TEdit
        Left = 42
        Top = 36
        Width = 65
        Height = 21
        TabOrder = 0
        Text = '2.0'
      end
      object FallingSlopeEdit: TEdit
        Left = 42
        Top = 61
        Width = 65
        Height = 21
        TabOrder = 1
        Text = '-2.0'
      end
      object RisingFactorEdit: TEdit
        Left = 113
        Top = 36
        Width = 65
        Height = 21
        TabOrder = 2
        Text = '1.0'
      end
      object FallingFactorEdit: TEdit
        Left = 113
        Top = 61
        Width = 65
        Height = 21
        TabOrder = 3
        Text = '1.0'
      end
    end
    object LastValuesGB: TGroupBox
      Left = 666
      Top = 16
      Width = 266
      Height = 100
      Caption = 'Most Recent Values'
      TabOrder = 10
      object Label13: TLabel
        Left = 10
        Top = 19
        Width = 22
        Height = 13
        Caption = 'Time'
      end
      object Label14: TLabel
        Left = 10
        Top = 39
        Width = 44
        Height = 13
        Caption = 'RTD Raw'
      end
      object Label15: TLabel
        Left = 10
        Top = 58
        Width = 42
        Height = 13
        Caption = 'RTD Avg'
      end
      object Label16: TLabel
        Left = 10
        Top = 79
        Width = 37
        Height = 13
        Caption = 'RTD Slp'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 136
        Top = 19
        Width = 42
        Height = 13
        Caption = 'Ten Raw'
      end
      object Label18: TLabel
        Left = 136
        Top = 39
        Width = 36
        Height = 13
        Caption = 'Ten 1st'
      end
      object Label19: TLabel
        Left = 136
        Top = 58
        Width = 48
        Height = 13
        Caption = 'Ten Comp'
      end
      object Label20: TLabel
        Left = 136
        Top = 77
        Width = 40
        Height = 13
        Caption = 'Ten Avg'
      end
      object RecentTimeEdit: TEdit
        Left = 58
        Top = 12
        Width = 75
        Height = 21
        Color = clBtnFace
        Enabled = False
        TabOrder = 0
        Text = '0'
      end
      object RecentRTDRawEdit: TEdit
        Left = 58
        Top = 33
        Width = 75
        Height = 21
        Color = clBtnFace
        Enabled = False
        TabOrder = 1
        Text = '0'
      end
      object RecentRTDAvgEdit: TEdit
        Left = 58
        Top = 54
        Width = 75
        Height = 21
        Color = clBtnFace
        Enabled = False
        TabOrder = 2
        Text = '0'
      end
      object RecentRTDSlpEdit: TEdit
        Left = 58
        Top = 76
        Width = 75
        Height = 21
        Color = clRed
        Enabled = False
        TabOrder = 3
        Text = '0'
      end
      object RecentTenRawEdit: TEdit
        Left = 186
        Top = 12
        Width = 75
        Height = 21
        Color = clAqua
        Enabled = False
        TabOrder = 4
        Text = '0'
      end
      object RecentTenFirstEdit: TEdit
        Left = 186
        Top = 33
        Width = 75
        Height = 21
        Color = clBtnFace
        Enabled = False
        TabOrder = 5
        Text = '0'
      end
      object RecentTenCompEdit: TEdit
        Left = 186
        Top = 54
        Width = 75
        Height = 21
        Color = clBtnFace
        Enabled = False
        TabOrder = 6
        Text = '0'
      end
      object RecentTenAvgEdit: TEdit
        Left = 186
        Top = 74
        Width = 75
        Height = 21
        Color = clLime
        Enabled = False
        TabOrder = 7
        Text = '0'
      end
    end
    object ResetStartBtn: TButton
      Left = 89
      Top = 119
      Width = 75
      Height = 25
      Hint = 'Reset (except offset) to next incoming packet.'
      Anchors = [akLeft, akBottom]
      Caption = 'Reset Start'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = ResetStartBtnClick
    end
    object PrintScreenBtn: TButton
      Left = 342
      Top = 119
      Width = 75
      Height = 25
      Hint = 'Print this form to the default printer.'
      Anchors = [akLeft, akBottom]
      Caption = 'Print Form'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnClick = PrintScreenBtnClick
    end
    object ExportBtn: TButton
      Left = 423
      Top = 119
      Width = 81
      Height = 25
      Hint = 'Export currently displayed packets to CSV file.'
      Anchors = [akLeft, akBottom]
      Caption = 'Export to CSV'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      OnClick = ExportBtnClick
    end
  end
  object CompChart: TChart
    Left = 8
    Top = 8
    Width = 941
    Height = 409
    BackWall.Brush.Gradient.Direction = gdBottomTop
    BackWall.Brush.Gradient.EndColor = clWhite
    BackWall.Brush.Gradient.StartColor = 15395562
    BackWall.Brush.Gradient.Visible = True
    BackWall.Transparent = False
    Foot.Font.Color = clBlue
    Foot.Font.Name = 'Verdana'
    Gradient.Direction = gdBottomTop
    Gradient.EndColor = clWhite
    Gradient.MidColor = 15395562
    Gradient.StartColor = 15395562
    Gradient.Visible = True
    LeftWall.Color = 14745599
    Legend.Font.Name = 'Verdana'
    Legend.Shadow.Transparency = 0
    RightWall.Color = 14745599
    SubTitle.Visible = False
    Title.Font.Name = 'Verdana'
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    BottomAxis.Axis.Color = 4210752
    BottomAxis.Grid.Color = 11119017
    BottomAxis.LabelsFormat.Font.Name = 'Verdana'
    BottomAxis.TicksInner.Color = 11119017
    BottomAxis.Title.Font.Name = 'Verdana'
    Chart3DPercent = 5
    DepthAxis.Axis.Color = 4210752
    DepthAxis.Grid.Color = 11119017
    DepthAxis.LabelsFormat.Font.Name = 'Verdana'
    DepthAxis.TicksInner.Color = 11119017
    DepthAxis.Title.Font.Name = 'Verdana'
    DepthTopAxis.Axis.Color = 4210752
    DepthTopAxis.Grid.Color = 11119017
    DepthTopAxis.LabelsFormat.Font.Name = 'Verdana'
    DepthTopAxis.TicksInner.Color = 11119017
    DepthTopAxis.Title.Font.Name = 'Verdana'
    LeftAxis.Axis.Color = 4210752
    LeftAxis.Grid.Color = 11119017
    LeftAxis.LabelsFormat.Font.Name = 'Verdana'
    LeftAxis.TicksInner.Color = 11119017
    LeftAxis.Title.Caption = 'Tension (blue raw, green avg)'
    LeftAxis.Title.Font.Name = 'Verdana'
    RightAxis.Axis.Color = 4210752
    RightAxis.Grid.Color = 11119017
    RightAxis.LabelsFormat.Font.Name = 'Verdana'
    RightAxis.TicksInner.Color = 11119017
    RightAxis.Title.Caption = 'RTD Slope (red)'
    RightAxis.Title.Font.Name = 'Verdana'
    TopAxis.Axis.Color = 4210752
    TopAxis.Grid.Color = 11119017
    TopAxis.LabelsFormat.Font.Name = 'Verdana'
    TopAxis.TicksInner.Color = 11119017
    TopAxis.Title.Font.Name = 'Verdana'
    View3D = False
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    Anchors = [akLeft, akTop, akRight, akBottom]
    DefaultCanvas = 'TGDIPlusCanvas'
    PrintMargins = (
      15
      34
      15
      34)
    ColorPaletteIndex = 13
    object RawTensionSeries: TLineSeries
      Legend.Visible = False
      Marks.Symbol.Frame.Width = 4
      Marks.Symbol.Pen.Width = 4
      ShowInLegend = False
      Title = 'Raw Avg Tension'
      Brush.BackColor = clDefault
      OutLine.Width = 4
      Pointer.HorizSize = 8
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.VertSize = 8
      XValues.DateTime = True
      XValues.Name = 'RFC Time'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Data = {00010000000000000000000000}
    end
    object CompTensionSeries: TLineSeries
      Legend.Visible = False
      SeriesColor = clGreen
      ShowInLegend = False
      Title = 'Comp'#39'd Avg Tension'
      Brush.BackColor = clDefault
      OutLine.Width = 4
      Pointer.HorizSize = 8
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.VertSize = 8
      XValues.DateTime = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Data = {00010000000000000000000000}
    end
    object RTDSeries: TLineSeries
      Legend.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      Title = 'RTD Reading'
      VertAxis = aRightAxis
      Brush.BackColor = clDefault
      OutLine.Width = 4
      Pointer.HorizSize = 8
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.VertSize = 8
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      Data = {00010000000000000000000000}
    end
  end
  object SaveLogDlg: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All File (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Log File As...'
    Left = 704
    Top = 542
  end
end
