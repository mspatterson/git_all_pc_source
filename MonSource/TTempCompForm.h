//---------------------------------------------------------------------------
#ifndef TTempCompFormH
#define TTempCompFormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.Series.hpp>
#include "WTTTSProtocolUtils.h"
#include "Averaging.h"
#include <Vcl.Dialogs.hpp>

typedef struct
{
    TExpAverage* pRTDAvg;
    TExpAverage* pTenAvg;

    float fRTDAlpha;
    float fRTDMaxVar;
    float fTenAlpha;
    float fTenMaxVar;

    float fRTDSpan;
    float fTenSpan;
    int   iRTDOffset;
    int   iTenOffset;

    float fRisingSlope;
    float fFallingSlope;
    float fRisingFactor;
    float fFallingFactor;

    float fRTD_Raw;
    float fRTD_Off;
    float fRTD_Avg;
    float fRTD_Slp;

    float fTen000;
    float fTen090;
    float fTen180;
    float fTen270;
    float fTen000_Off;
    float fTen090_Off;
    float fTen180_Off;
    float fTen270_Off;

    float fTen_Raw;
    float fTen_First;
    float fTen_Comp;
    float fTen_Avg;
} COMP_VALUES;

class TTempCompForm : public TForm
{
__published:	// IDE-managed Components
    TGroupBox *ControlsGB;
    TButton *CloseBtn;
    TChart *CompChart;
    TLineSeries *RawTensionSeries;
    TLineSeries *CompTensionSeries;
    TLineSeries *RTDSeries;
    TButton *ClearGraphBtn;
    TLabel *Label1;
    TLabel *Label2;
    TEdit *RTDAlphaEdit;
    TEdit *TenAlphaEdit;
    TLabel *Label3;
    TEdit *RTDMaxVarEdit;
    TEdit *TenMaxVarEdit;
    TLabel *Label4;
    TButton *PauseBtn;
    TButton *RedrawBtn;
    TGroupBox *AvgGB;
    TGroupBox *StdCompGB;
    TLabel *Label5;
    TLabel *Label6;
    TLabel *Label7;
    TLabel *Label8;
    TEdit *TenSpanEdit;
    TEdit *RTDSpanEdit;
    TEdit *TenOffsetEdit;
    TEdit *RTDOffsetEdit;
    TGroupBox *SlopeCompGB;
    TLabel *Label9;
    TLabel *Label10;
    TLabel *Label11;
    TLabel *Label12;
    TEdit *RisingSlopeEdit;
    TEdit *FallingSlopeEdit;
    TEdit *RisingFactorEdit;
    TEdit *FallingFactorEdit;
    TGroupBox *LastValuesGB;
    TLabel *Label13;
    TLabel *Label14;
    TLabel *Label15;
    TLabel *Label16;
    TLabel *Label17;
    TLabel *Label18;
    TLabel *Label19;
    TLabel *Label20;
    TEdit *RecentTimeEdit;
    TEdit *RecentRTDRawEdit;
    TEdit *RecentRTDAvgEdit;
    TEdit *RecentRTDSlpEdit;
    TEdit *RecentTenRawEdit;
    TEdit *RecentTenFirstEdit;
    TEdit *RecentTenCompEdit;
    TEdit *RecentTenAvgEdit;
    TButton *ResetStartBtn;
    TButton *PrintScreenBtn;
    TButton *ExportBtn;
    TSaveDialog *SaveLogDlg;
    void __fastcall ClearGraphBtnClick(TObject *Sender);
    void __fastcall RedrawBtnClick(TObject *Sender);
    void __fastcall PauseBtnClick(TObject *Sender);
    void __fastcall ResetStartBtnClick(TObject *Sender);
    void __fastcall PrintScreenBtnClick(TObject *Sender);
    void __fastcall ExportBtnClick(TObject *Sender);

private:
    int m_iRFCListSize;   // Max number of RFCs we can store
    int m_iRFCCount;      // Current number of 'real' RFCs in the list

    typedef struct {
        TDateTime       pktTime;
        GENERIC_RFC_PKT rfcPkt;
    } RFC_ENTRY;

    RFC_ENTRY* m_pRFCList;

    bool m_bPause;
        // When true RFC packets are received but ignored.

    int   m_iOffsetNum;
    float m_fOffsetTen000;
    float m_fOffsetTen090;
    float m_fOffsetTen180;
    float m_fOffsetTen270;
    float m_fOffsetRTD;
        // Offsets removed from raw packet values.

    void RefreshGraph( void );

    void InitUserSpecified( void );
        // Initialize the user specified TEdits to default values.

    void GetUserSpecified( COMP_VALUES& cv );
        // Get the user specified TEdit values into the structure.

    void GetValues       ( const GENERIC_RFC_PKT& rfcPkt, COMP_VALUES& cv );
        // Get the values from the packet, remove offset, apply span/offset calculation.
        // GetValues is called by InitializeValues() and CalcValues()
    void InitializeValues( const GENERIC_RFC_PKT& rfcPkt, COMP_VALUES& cv );
        //
    void CalcValues      ( const GENERIC_RFC_PKT& rfcPkt, COMP_VALUES& cv );

    bool LogRFCPacket( UnicodeString logFileName, int iItem, int iMilliSecs, GENERIC_RFC_PKT& rfcData, COMP_VALUES& cv );

public:		// User declarations
    __fastcall TTempCompForm(TComponent* Owner);

    void ShowTempCompForm( void );
    void ProcessRFC( const GENERIC_RFC_PKT& currRFC );
};
//---------------------------------------------------------------------------
extern PACKAGE TTempCompForm *TempCompForm;
//---------------------------------------------------------------------------
#endif

