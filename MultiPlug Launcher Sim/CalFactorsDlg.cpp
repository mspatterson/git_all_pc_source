#include <vcl.h>
#pragma hdrstop

#include "CalFactorsDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TCalFactorsForm *CalFactorsForm;


__fastcall TCalFactorsForm::TCalFactorsForm(TComponent* Owner) : TForm(Owner)
{
    // Initialize the grid on boot. Clear out all factors
    CalGrid->ColCount = NBR_BYTES_PER_WTTTS_PAGE + 1;
    CalGrid->RowCount = NBR_WTTTS_CFG_PAGES      + 1;

    CalGrid->Cells[0][0] = "Page";

    for( int iHdrCol = 1; iHdrCol < CalGrid->ColCount; iHdrCol++ )
        CalGrid->Cells[iHdrCol][0] = "Byte " + IntToStr( iHdrCol - 1 );

    for( int iRow = 1; iRow < CalGrid->RowCount; iRow++ )
    {
        CalGrid->Cells[0][iRow] = IntToStr( iRow - 1 );

        for( int iHdrCol = 1; iHdrCol < CalGrid->ColCount; iHdrCol++ )
            CalGrid->Cells[iHdrCol][iRow] = "255";
    }

    // Initialize Page 0 with manufacturing information
    CalGrid->Cells[ 1][1] = "21";   // WORD devSN, 43d 21d = 0x2B15 = 11029
    CalGrid->Cells[ 2][1] = "43";
    CalGrid->Cells[ 3][1] = "77";   // char szMfgInfo[8], 77 = 'M'
    CalGrid->Cells[ 4][1] = "76";   // 76 = 'L'
    CalGrid->Cells[ 5][1] = "89";   // 89 = 'Y'
    CalGrid->Cells[ 6][1] = "78";   // 78 = 'N'
    CalGrid->Cells[ 7][1] = "88";   // 88 = 'X'
    CalGrid->Cells[ 8][1] = "0";
    CalGrid->Cells[ 9][1] = "0";
    CalGrid->Cells[10][1] = "0";
    CalGrid->Cells[11][1] = "9";    // 0 = Jan
    CalGrid->Cells[12][1] = "16";   // Years since 2000
    CalGrid->Cells[13][1] = "0";    // RFU
    CalGrid->Cells[14][1] = "0";    // RFU
    CalGrid->Cells[15][1] = "0";    // RFU
    CalGrid->Cells[16][1] = "0";    // RFU
}


void __fastcall TCalFactorsForm::LoadCalFactorsBtnClick(TObject *Sender)
{
    if( OpenDlg->Execute() )
    {
        TStringList* cfgData  = new TStringList();
        TStringList* csvItems = new TStringList();

        int rowsLoaded = 0;

        try
        {
            cfgData->LoadFromFile( OpenDlg->FileName );

            // Load each line - assume there is a header row in the file
            for( int iLine = 1; iLine < cfgData->Count; iLine++ )
            {
                if( iLine >= CalGrid->RowCount )
                    break;

                csvItems->CommaText = cfgData->Strings[ iLine ];

                if( csvItems->Count >= CalGrid->ColCount )
                {
                    // Assume that the first is the cal factor number - skip it
                    for( int iItem = 1; iItem < CalGrid->ColCount; iItem++ )
                        CalGrid->Cells[iItem][iLine] = csvItems->Strings[iItem];
                }

                rowsLoaded++;
            }

            MessageDlg( "Config data file read, " + IntToStr( rowsLoaded ) + " rows loaded.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
        }
        catch( ... )
        {
            MessageDlg( "An error occurred loading the config data file! " + IntToStr( rowsLoaded ) + " rows loaded.", mtError, TMsgDlgButtons() << mbOK, 0 );
        }

        delete cfgData;
        delete csvItems;
    }
}


void __fastcall TCalFactorsForm::SaveCalFactorsBtnClick(TObject *Sender)
{
    if( SaveDlg->Execute() )
    {
        TStringList* cfgData = new TStringList();
        TStringList* csvItems   = new TStringList();

        try
        {
            // Save each line - save the header line too
            for( int iLine = 0; iLine < CalGrid->RowCount; iLine++ )
            {
                csvItems->Clear();

                for( int iItem = 0; iItem < CalGrid->ColCount; iItem++ )
                    csvItems->Add( CalGrid->Cells[iItem][iLine] );

                cfgData->Add( csvItems->CommaText );
            }

            cfgData->SaveToFile( SaveDlg->FileName );

            MessageDlg( "Calbration file saved.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
        }
        catch( ... )
        {
            MessageDlg( "An error occurred save the cal factors!", mtError, TMsgDlgButtons() << mbOK, 0 );
        }

        delete cfgData;
        delete csvItems;
    }
}


bool TCalFactorsForm::GetConfigData( BYTE whichPage, WTTTS_CFG_DATA& pgData )
{
    // Init response, assuming the worst
    pgData.pageNbr = whichPage;
    pgData.result  = WTTTS_PG_RESULT_BAD_PG;

    memset( pgData.pageData, 0xFF, NBR_BYTES_PER_WTTTS_PAGE );

    try
    {
        int tableRow = (int)whichPage + 1;

        if( tableRow >= CalGrid->RowCount )
            Abort();

        // All cal factors are displayed as integer values. Convert
        // directly to array slots.
        for( int iByte = 0; iByte < NBR_BYTES_PER_WTTTS_PAGE; iByte++ )
            pgData.pageData[iByte] = CalGrid->Cells[iByte+1][tableRow].ToIntDef( 255 );

        pgData.result = WTTTS_PG_RESULT_SUCCESS;
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool TCalFactorsForm::SetConfigData( const WTTTS_CFG_DATA& pgData )
{
    // The firmware will disallow the writing of data to read-only pages.
    // Do the same here
    if( pgData.pageNbr < FIRST_RDWR_CFG_PAGE )
        return false;

    try
    {
        int tableRow = (int)pgData.pageNbr + 1;

        if( tableRow >= CalGrid->RowCount )
            Abort();

        for( int iByte = 0; iByte < NBR_BYTES_PER_WTTTS_PAGE; iByte++ )
            CalGrid->Cells[iByte+1][tableRow] = IntToStr( pgData.pageData[iByte] );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


