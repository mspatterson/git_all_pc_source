object CalFactorsForm: TCalFactorsForm
  Left = 0
  Top = 0
  Caption = 'Configuration Data'
  ClientHeight = 372
  ClientWidth = 454
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    454
    372)
  PixelsPerInch = 96
  TextHeight = 13
  object CalGrid: TStringGrid
    Left = 8
    Top = 8
    Width = 438
    Height = 321
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 4
    DefaultColWidth = 44
    RowCount = 257
    TabOrder = 0
  end
  object CloseBtn: TButton
    Left = 370
    Top = 339
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 1
  end
  object LoadCalFactorsBtn: TButton
    Left = 8
    Top = 339
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Load From File'
    TabOrder = 2
    OnClick = LoadCalFactorsBtnClick
  end
  object SaveCalFactorsBtn: TButton
    Left = 109
    Top = 339
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Save to File'
    TabOrder = 3
    OnClick = SaveCalFactorsBtnClick
  end
  object OpenDlg: TOpenDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Load Calibration Factors'
    Left = 264
    Top = 48
  end
  object SaveDlg: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Cal Factors to File'
    Left = 304
    Top = 48
  end
end
