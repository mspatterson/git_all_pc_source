//---------------------------------------------------------------------------
#ifndef CalFactorsDlgH
#define CalFactorsDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.Grids.hpp>
#include "WTTTSProtocolUtils.h"
//---------------------------------------------------------------------------
class TCalFactorsForm : public TForm
{
__published:	// IDE-managed Components
    TStringGrid *CalGrid;
    TButton *CloseBtn;
    TButton *LoadCalFactorsBtn;
    TButton *SaveCalFactorsBtn;
    TOpenDialog *OpenDlg;
    TSaveDialog *SaveDlg;
    void __fastcall LoadCalFactorsBtnClick(TObject *Sender);
    void __fastcall SaveCalFactorsBtnClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TCalFactorsForm(TComponent* Owner);

    bool GetConfigData( BYTE whichPage, WTTTS_CFG_DATA& pgData );
    bool SetConfigData( const WTTTS_CFG_DATA& pgData );
};
//---------------------------------------------------------------------------
extern PACKAGE TCalFactorsForm *CalFactorsForm;
//---------------------------------------------------------------------------
#endif
