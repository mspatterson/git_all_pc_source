#include <vcl.h>
#pragma hdrstop

#include "MPLSimMain.h"
#include "RFCDataDlg.h"
#include "CalFactorsDlg.h"
#include "TLoggingForm.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TMPLSimMainForm *MPLSimMainForm;


typedef union {
    BYTE byData[4];
    WORD wData[2];
    int  iData;
} XFER_BUFFER;


static void IntToBiByte( int iValue, BYTE* pBytes )
{
    XFER_BUFFER xferBuff;

    xferBuff.iData = iValue;

    pBytes[0] = xferBuff.byData[0];
    pBytes[1] = xferBuff.byData[1];
}


static void IntToTriByte( int iValue, BYTE* pBytes )
{
    XFER_BUFFER xferBuff;

    xferBuff.iData = iValue;

    pBytes[0] = xferBuff.byData[0];
    pBytes[1] = xferBuff.byData[1];
    pBytes[2] = xferBuff.byData[2];
}


__fastcall TMPLSimMainForm::TMPLSimMainForm(TComponent* Owner) : TForm(Owner)
{
    // Winsock is required for UDP comms. Init it now
    InitWinsock();
}


void __fastcall TMPLSimMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Fake disconnect click
    DisconnectBtnClick( DisconnectBtn );

    ShutdownWinsock();
}


void __fastcall TMPLSimMainForm::CfgDataBtnClick(TObject *Sender)
{
    CalFactorsForm->ShowModal();
}


void __fastcall TMPLSimMainForm::ViewLogBtnClick(TObject *Sender)
{
    LoggingForm->ShowModal();
}


void __fastcall TMPLSimMainForm::RFCDataBtnClick(TObject *Sender)
{
    RFCDataForm->ShowModal();
}


void __fastcall TMPLSimMainForm::ConnectBtnClick(TObject *Sender)
{
    // Create the comm object
    if( SerialModeRB->Checked )
    {
        CCommPort::CONNECT_PARAMS portParams;

        portParams.iPortNumber   = CommPortEdit->Text.ToInt();
        portParams.dwLineBitRate = BitRateEdit->Text.ToInt();

        m_commObj = new CCommPort();

        if( m_commObj->Connect( &portParams ) == CCommObj::ERR_NONE )
        {
            LoggingForm->AddLogMessage( "COM" + CommPortEdit->Text + " opened" );
        }
    }
    else
    {
        CUDPPort::CONNECT_PARAMS portParams;

        // The information entered here is the dest info for the MPL app
        // We open an unbound port to transmit on
        portParams.destAddr = HostIPEdit->Text;
        portParams.destPort = UDPPortEdit->Text.ToInt();
        portParams.portNbr  = 0;
        portParams.acceptBroadcasts = true;

        m_commObj = new CUDPPort();

        if( m_commObj->Connect( &portParams ) == CCommObj::ERR_NONE )
        {
            LoggingForm->AddLogMessage( "UDP" + HostIPEdit->Text + ":" + UDPPortEdit->Text + " opened" );
        }
    }

    // On success, set default values
    m_lastRFCTime = 0;
    m_seqNbr      = 0;

    // Rates, as received from Host
    m_rfcRate         = 1000;   // msecs
    m_rfcTimeout      =   10;   // msecs
    m_pairingTimeout  =  900;   // secs
    m_solenoidTimeout =   10;   // secs

    m_powerMode = 1;

    m_solOnTime = 0;
    m_activeSolenoid = 0;

    for( int iContact = 0; iContact < NBR_CONTACTS; iContact++ )
        m_activeContacts[iContact] = false;

    m_avgingFactor = 1;
    m_sampleRate   = 1;

    UpdateSensorParamsDisplay();
    UpdateRatesDisplay();
    UpdateContactsDisplay();
    UpdateSolenoidsDisplay();

    EnableCommControls( false /*connect disabled*/ );

    SimTimer->Enabled = true;
}


void __fastcall TMPLSimMainForm::DisconnectBtnClick(TObject *Sender)
{
    // Kill the timer first
    SimTimer->Enabled = false;

    if( m_commObj != NULL )
    {
        delete m_commObj;
        m_commObj = NULL;
    }

    EnableCommControls( true /*can connect*/ );

    // Clear status panel dispaly
    StatusBar->Panels->Items[0]->Text = "";
}


void __fastcall TMPLSimMainForm::WMCloseConnection( TMessage &Message )
{
    DisconnectBtnClick( DisconnectBtn );
}


void TMPLSimMainForm::UpdateSensorParamsDisplay( void )
{
    if( ( m_sampleRate >= 0 ) && ( m_sampleRate < MagRateCombo->Items->Count ) )
        MagRateCombo->ItemIndex = m_sampleRate;

    int iAvgIndex = AvgFactorCombo->Items->IndexOf( IntToStr( m_avgingFactor ) );

    if( iAvgIndex >= 0 )
        AvgFactorCombo->ItemIndex = iAvgIndex;
}


void TMPLSimMainForm::UpdateRatesDisplay( void )
{
    RFCRateEdit->Text     = IntToStr( m_rfcRate );
    RFCTimeoutEdit->Text  = IntToStr( m_rfcTimeout );
    PairTimeoutEdit->Text = IntToStr( m_pairingTimeout );
    SolTimeoutEdit->Text  = IntToStr( m_solenoidTimeout );
}


String TMPLSimMainForm::GetContactString( const String& sPrefix, int itemNbr, bool bIsClosed )
{
    String sResult = sPrefix + IntToStr( itemNbr ) + ": ";

    if( bIsClosed )
        return sResult + "On";
    else
        return sResult + "Off";
}


void TMPLSimMainForm::UpdateContactsDisplay( void )
{
    const String sPrefix( "Contact " );

    Contact1Label->Caption = GetContactString( sPrefix, 1, m_activeContacts[0] );
    Contact2Label->Caption = GetContactString( sPrefix, 2, m_activeContacts[1] );
    Contact3Label->Caption = GetContactString( sPrefix, 3, m_activeContacts[2] );
    Contact4Label->Caption = GetContactString( sPrefix, 4, m_activeContacts[3] );
}


void TMPLSimMainForm::UpdateSolenoidsDisplay( void )
{
    const String sPrefix( "Solenoid " );

    Solenoid1Label->Caption = GetContactString( sPrefix, 1, m_activeSolenoid == 1 );
    Solenoid2Label->Caption = GetContactString( sPrefix, 2, m_activeSolenoid == 2 );
    Solenoid3Label->Caption = GetContactString( sPrefix, 3, m_activeSolenoid == 3 );
    Solenoid4Label->Caption = GetContactString( sPrefix, 4, m_activeSolenoid == 4 );
    Solenoid5Label->Caption = GetContactString( sPrefix, 5, m_activeSolenoid == 5 );
    Solenoid6Label->Caption = GetContactString( sPrefix, 6, m_activeSolenoid == 6 );
}


void TMPLSimMainForm::EnableCommControls( bool bCanConnect )
{
    for( int iCtrl = 0; iCtrl < ConnectionGB->ControlCount; iCtrl++ )
    {
        TRadioButton* pRB = dynamic_cast<TRadioButton*>( ConnectionGB->Controls[iCtrl] );

        if( pRB != NULL )
            pRB->Enabled = bCanConnect;

        TEdit* pEdit = dynamic_cast<TEdit*>( ConnectionGB->Controls[iCtrl] );

        if( pEdit != NULL )
        {
            pEdit->Enabled = bCanConnect;
            pEdit->Color   = bCanConnect ? clWindow : clBtnFace;
        }
    }

    ConnectBtn->Enabled    = bCanConnect;
    DisconnectBtn->Enabled = !bCanConnect;
}


void __fastcall TMPLSimMainForm::SimTimerTimer(TObject *Sender)
{
    // Check for inbound messages
    m_buffCount += m_commObj->CommRecv( &(m_commBuff[m_buffCount]), COMM_BUFF_LEN - m_buffCount );

    // Simulating WTTTS sub. In this case, we need to process inbound messages.
    WTTTS_PKT_HDR  pktHdr;
    MPL_DATA_UNION pktData;

    if( HaveMPLCmdPkt( m_commBuff, m_buffCount, pktHdr, pktData ) )
    {
        // Process the command.
        bool replyWithRFC = true;

        switch( pktHdr.pktType )
        {
            case WTTTS_CMD_GET_CFG_DATA:
                GetCfgData( pktData.cfgRequest );
                break;

            case WTTTS_CMD_SET_CFG_DATA:
                SetCfgData( pktData.cfgData );
                break;

            case WTTTS_CMD_SET_RATE:
                SetRates( pktData.ratePkt );
                break;

            case WTTTS_CMD_QUERY_VER:
                SendVersionPkt();
                break;

            case WTTTS_CMD_ENTER_DEEP_SLEEP:
                SetDeepSleepMode();
                break;

            case WTTTS_CMD_SET_RF_CHANNEL:
                SetRFChan( pktData.chanPkt );
                break;

            case WTTTS_CMD_SET_RF_POWER:
                SetRFPower( pktData.pwrPkt );
                break;

            case WTTTS_CMD_GET_MIN_MAX_VALS:
                GetMinMaxVals( pktData.reqMinMaxPkt );
                break;

            case WTTTS_CMD_MARK_CHAN_IN_USE:
                MarkChanInUse();
                break;

            case WTTTS_CMD_SET_CONTACTS:
                SetContacts( pktData.contactsPkt );
                break;

            case WTTTS_CMD_SET_SOLENOIDS:
                SetSolenoid( pktData.solenoidPkt );
                break;

            case WTTTS_CMD_SET_PIB_PARAMS:
                SetPIBParams( pktData.pibParamsPkt );
                break;

            case WTTTS_CMD_CLR_PIB_SWITCH:
                ClearInputIndication( pktData.clearPIBInput );
                break;

            case WTTTS_CMD_NO_CMD:
                replyWithRFC = false;
                LoggingForm->AddLogMessage( "No Cmd received" );
                break;

            default:
                replyWithRFC = false;
                LoggingForm->AddLogMessage( "Unknown cmd received: " + IntToHex( pktHdr.pktType, 2 ) );
                break;
        }

        if( replyWithRFC )
            SendRequestForCmd( true /*send now*/ );
    }
    else
    {
        // Send a RFC, if its time to
        SendRequestForCmd( false /*send if time to*/ );
    }

    // Now check if we have a solenoid timeout. Note that the timeout value is
    // given in seconds
    if( m_activeSolenoid != 0 )
    {
        if( HaveTimeout( m_solOnTime, m_solenoidTimeout * 1000 ) )
        {
            LoggingForm->AddLogMessage( "WARNING - solenoid on timeout occurred!" );

            m_activeSolenoid = 0;
            UpdateSolenoidsDisplay();
        }
    }

    // Update the status panel time.
    StatusBar->Panels->Items[1]->Text = Now().TimeString() + "   ";
}


void TMPLSimMainForm::SendRequestForCmd( bool sendNow )
{
    // Send a RFC command now (if param is true) or if the RFC timer has expired
    if( sendNow || ( GetTickCount() >= m_lastRFCTime + m_rfcRate ) )
    {
        WTTTS_MPL_RFC_PKT pRFCPkt;

        pRFCPkt.temperature     = (BYTE)RFCDataForm->Temp;
        pRFCPkt.battType        = 1;
        pRFCPkt.battVoltage     = (WORD)RFCDataForm->BattVoltage;
        pRFCPkt.battUsed        = (WORD)RFCDataForm->BattmAhUsed;
        pRFCPkt.currMode        = m_powerMode;
        pRFCPkt.rpm             = (BYTE)RFCDataForm->RPM;
        pRFCPkt.lastReset       = 1;
        pRFCPkt.rfChannel       = 11;
        pRFCPkt.rfcRate         = m_rfcRate;
        pRFCPkt.rfcTimeout      = m_rfcTimeout;
        pRFCPkt.pairingTimeout  = m_pairingTimeout;
        pRFCPkt.solenoidTimeout = m_solenoidTimeout;
        pRFCPkt.activeSolenoid  = m_activeSolenoid;
        pRFCPkt.sensorSamplingRate = m_sampleRate;
        pRFCPkt.sensorAvgFactor = m_avgingFactor;

        // Bit flags D0 through D3 for contacts, D4 = SW1, D5 = SW2
        BYTE byData = 0;

        if( m_activeContacts[0] )
            byData |= 0x01;

        if( m_activeContacts[1] )
            byData |= 0x02;

        if( m_activeContacts[2] )
            byData |= 0x04;

        if( m_activeContacts[3] )
            byData |= 0x08;

        if( RFCDataForm->SW1 )
            byData |= 0x10;

        if( RFCDataForm->SW2 )
            byData |= 0x20;

        pRFCPkt.contactState = byData;

        // Bit flags, D0/D1 = Sensor 1, D2/D3 = Sensor 2, etc
        byData = 0;

        BYTE bySW1Flag = 0x01;
        BYTE bySW2Flag = 0x02;

        for( int iBd = 0; iBd < MAX_SENSOR_BOARDS; iBd++ )
        {
            if( RFCDataForm->SensorBdSw1[iBd] )
                byData |= bySW1Flag;

            if( RFCDataForm->SensorBdSw2[iBd] )
                byData |= bySW2Flag;

            bySW1Flag <<= 2;
            bySW2Flag <<= 2;
        }

        pRFCPkt.inputState = byData;

        // Pressure
        IntToTriByte( RFCDataForm->Pressure, pRFCPkt.pressure );

        // Now do the readings for each sensor board
        for( int iBd = 0; iBd < MAX_SENSOR_BOARDS; iBd++ )
        {
            if( RFCDataForm->SensorBdActive[iBd] )
            {
                pRFCPkt.activeSensors[iBd] = RFCDataForm->SensorBdAddr[iBd];

                IntToBiByte( RFCDataForm->SensorBdX[iBd], pRFCPkt.sensXAvg[iBd] );
                IntToBiByte( RFCDataForm->SensorBdY[iBd], pRFCPkt.sensYAvg[iBd] );
                IntToBiByte( RFCDataForm->SensorBdZ[iBd], pRFCPkt.sensZAvg[iBd] );
            }
            else
            {
                pRFCPkt.activeSensors[iBd] = 0;

                IntToBiByte( 0, pRFCPkt.sensXAvg[iBd] );
                IntToBiByte( 0, pRFCPkt.sensYAvg[iBd] );
                IntToBiByte( 0, pRFCPkt.sensZAvg[iBd] );
            }
        }

        // Lastly, do device ID
        String sDevID = DevIDEdit->Text;   // "aa bb cc dd ee ff"

        pRFCPkt.deviceID[0] = ( sDevID.Length() >=  2 ) ? sDevID.SubString(  1, 2 ).ToIntDef( 0 ) : 0;
        pRFCPkt.deviceID[1] = ( sDevID.Length() >=  5 ) ? sDevID.SubString(  4, 2 ).ToIntDef( 0 ) : 0;
        pRFCPkt.deviceID[2] = ( sDevID.Length() >=  8 ) ? sDevID.SubString(  7, 2 ).ToIntDef( 0 ) : 0;
        pRFCPkt.deviceID[3] = ( sDevID.Length() >= 11 ) ? sDevID.SubString( 10, 2 ).ToIntDef( 0 ) : 0;
        pRFCPkt.deviceID[4] = ( sDevID.Length() >= 14 ) ? sDevID.SubString( 13, 2 ).ToIntDef( 0 ) : 0;
        pRFCPkt.deviceID[5] = ( sDevID.Length() >= 17 ) ? sDevID.SubString( 16, 2 ).ToIntDef( 0 ) : 0;

        // Send the packet now
        bool bSentOK = SendMPLEvent( WTTTS_RESP_REQ_FOR_CMD_MPL, sizeof( WTTTS_MPL_RFC_PKT ), &pRFCPkt );

        if( bSentOK )
            LoggingForm->AddLogMessage( "Send RFC successful" );
        else
            LoggingForm->AddLogMessage( "Send RFC failed" );

        m_lastRFCTime = GetTickCount();

        // Update the last RFC time display
        if( bSentOK )
            StatusBar->Panels->Items[0]->Text = "  Last RFC sent at " + Now().TimeString();
        else
            StatusBar->Panels->Items[0]->Text = "  Last RFC send failed at " + Now().TimeString();
    }
}


void TMPLSimMainForm::GetCfgData( const WTTTS_CFG_ITEM& cfgReq )
{
    WTTTS_CFG_DATA cfgData;

    bool bGetGood = CalFactorsForm->GetConfigData( cfgReq.pageNbr, cfgData );

    if( bGetGood )
    {
        // Update log memo
        UnicodeString sResult;

        if( SendMPLEvent( WTTTS_RESP_CFG_DATA, sizeof( WTTTS_CFG_DATA ), &cfgData ) )
            sResult = " good";
        else
            sResult = " failed";

        LoggingForm->AddLogMessage( "Get cfg page " + IntToStr( cfgReq.pageNbr ) + sResult );
    }
    else
    {
        // Set data failed!
        WTTTS_CFG_DATA failedData;

        failedData.pageNbr = cfgReq.pageNbr;
        failedData.result  = WTTTS_PG_RESULT_BAD_PG;

        memset( failedData.pageData, 0xFF, NBR_BYTES_PER_WTTTS_PAGE );

        SendMPLEvent( WTTTS_RESP_CFG_DATA, sizeof( WTTTS_CFG_DATA ), &failedData );

        LoggingForm->AddLogMessage( "Get cfg page " + IntToStr( cfgData.pageNbr ) + " failed" );
    }
}


void TMPLSimMainForm::SetCfgData( const WTTTS_CFG_DATA& cfgData )
{
    if( CalFactorsForm->SetConfigData( cfgData ) )
    {
        LoggingForm->AddLogMessage( "Set cfg page " + IntToStr( cfgData.pageNbr ) + " good" );

        // A successful 'set' requires that the WTTTS follows-up with a get of the same page
        // GetCfgData() will also trigger an RFC being sent afterwards.
        WTTTS_CFG_ITEM cfgReq = { 0 };

        cfgReq.pageNbr = cfgData.pageNbr;

        GetCfgData( cfgReq );
    }
    else
    {
        // Set data failed!
        WTTTS_CFG_DATA failedData;

        failedData.pageNbr = cfgData.pageNbr;
        failedData.result  = WTTTS_PG_RESULT_BAD_PG;

        memset( failedData.pageData, 0xFF, NBR_BYTES_PER_WTTTS_PAGE );

        SendMPLEvent( WTTTS_RESP_CFG_DATA, sizeof( WTTTS_CFG_DATA ), &failedData );

        LoggingForm->AddLogMessage( "Set cfg page " + IntToStr( cfgData.pageNbr ) + " failed" );
    }
}


void TMPLSimMainForm::SetRates( const WTTTS_RATE_PKT& ratePkt )
{
    switch( ratePkt.rateType )
    {
        case SRT_RFC_RATE:          m_rfcRate         = ratePkt.newValue;   break;
        case SRT_RFC_TIMEOUT:       m_rfcTimeout      = ratePkt.newValue;   break;
        case SRT_PAIR_TIMEOUT:      m_pairingTimeout  = ratePkt.newValue;   break;
        case SRT_SOLENOID_TIMEOUT:  m_solenoidTimeout = ratePkt.newValue;   break;
    }

    // Refresh status display
    UpdateRatesDisplay();
}


void TMPLSimMainForm::SendVersionPkt( void )
{
    // Build the payload
    WTTTS_VER_PKT verPkt = { 0 };

    verPkt.hardwareSettings = HWRevEdit->Text.ToIntDef( 0 );

    AnsiString sFWVer = FWRevEdit->Text;

    while( sFWVer.Length() < WTTTS_FW_VER_LEN )
        sFWVer = sFWVer + " ";

    for( int iFWChar = 0; iFWChar < WTTTS_FW_VER_LEN; iFWChar++ )
        verPkt.fwVer[iFWChar] = sFWVer[iFWChar+1];

    // Send it
    if( SendMPLEvent( WTTTS_RESP_VER_PKT, sizeof( WTTTS_VER_PKT ), &verPkt ) )
        LoggingForm->AddLogMessage( "Version packet requested and sent" );
    else
        LoggingForm->AddLogMessage( "Version packet requested, send failed" );
}


void TMPLSimMainForm::SetDeepSleepMode( void )
{
    LoggingForm->AddLogMessage( "Enter Deep Sleep Mode command received - closing connection" );

    m_powerMode = 0;

    ::PostMessage( Handle, WM_CLOSE_CONNECTION, 0, 0 );
}


void TMPLSimMainForm::SetRFChan( const WTTTS_SET_CHAN_PKT& chanPkt )
{
    LoggingForm->AddLogMessage( "Set RF Chan cmd received, new chan = " + IntToStr( chanPkt.chanNbr ) );
}


void TMPLSimMainForm::SetRFPower( const WTTTS_SET_RF_PWR_PKT& pwrPkt )
{
    LoggingForm->AddLogMessage( "Set RF Pwr cmd received, new power = " + IntToStr( pwrPkt.powerLevel ) );
}


void TMPLSimMainForm::GetMinMaxVals( const WTTTS_REQ_MIN_MAX_PKT& minMaxReq )
{
    // Build the payload
    WTTTS_MPL_MIN_MAX_PKT minMaxPkt = { 0 };

    for( int iBd = 0; iBd < MAX_SENSOR_BOARDS; iBd++ )
    {
        TRFCDataForm::RFC_MIN_MAX_VALS minMaxVals;

        if( RFCDataForm->SensorBdActive[iBd] )
        {
            minMaxPkt.activeSensors[iBd] = RFCDataForm->SensorBdAddr[iBd];

            TRFCDataForm::RFC_MIN_MAX_VALS minMaxVals;
            RFCDataForm->GetMinMaxValues( minMaxVals, iBd );

            IntToBiByte( minMaxVals.minX, minMaxPkt.XMin[iBd] );
            IntToBiByte( minMaxVals.minY, minMaxPkt.YMin[iBd] );
            IntToBiByte( minMaxVals.minZ, minMaxPkt.ZMin[iBd] );

            IntToBiByte( minMaxVals.maxX, minMaxPkt.XMax[iBd] );
            IntToBiByte( minMaxVals.maxY, minMaxPkt.YMax[iBd] );
            IntToBiByte( minMaxVals.maxZ, minMaxPkt.ZMax[iBd] );
        }
        else
        {
            minMaxPkt.activeSensors[iBd] = 0;

            IntToBiByte( 0, minMaxPkt.XMin[iBd] );
            IntToBiByte( 0, minMaxPkt.YMin[iBd] );
            IntToBiByte( 0, minMaxPkt.ZMin[iBd] );

            IntToBiByte( 0, minMaxPkt.XMax[iBd] );
            IntToBiByte( 0, minMaxPkt.YMax[iBd] );
            IntToBiByte( 0, minMaxPkt.ZMax[iBd] );
        }
    }

    // Send it
    bool bSentOK = SendMPLEvent( WTTTS_RESP_MPL_MIN_MAX_VALS, sizeof( WTTTS_MPL_MIN_MAX_PKT ), &minMaxPkt );

    if( bSentOK )
        LoggingForm->AddLogMessage( "Req Min/Max Vals cmd received, reset param = " + IntToStr( minMaxReq.resetValues ) );
    else
        LoggingForm->AddLogMessage( "Req Min/Max Vals cmd received, send failed" );

    // Only reset min max on success
    if( bSentOK )
    {
        if( minMaxReq.resetValues )
            RFCDataForm->ResetMinMaxValues();
    }
}


void TMPLSimMainForm::MarkChanInUse( void )
{
    LoggingForm->AddLogMessage( "Mark Channel in Use cmd received - ignoring" );
}


void TMPLSimMainForm::SetContacts( const WTTTS_PIB_CONTACT_PKT& contactPkt )
{
    m_activeContacts[0] = ( contactPkt.contactSettings & 0x01 ) ? true : false;
    m_activeContacts[1] = ( contactPkt.contactSettings & 0x02 ) ? true : false;
    m_activeContacts[2] = ( contactPkt.contactSettings & 0x04 ) ? true : false;
    m_activeContacts[3] = ( contactPkt.contactSettings & 0x08 ) ? true : false;

    UpdateContactsDisplay();

    LoggingForm->AddLogMessage( "Set Contacts cmd received, value = " + IntToStr( contactPkt.contactSettings ) );
}


void TMPLSimMainForm::SetSolenoid( const WTTTS_PIB_SOLENOID_PKT& solenoidPkt )
{
    if( ( solenoidPkt.solenoidNbr == 0 ) || ( solenoidPkt.state == 0 ) )
    {
        m_activeSolenoid = 0;

        LoggingForm->AddLogMessage( "Set Solenoids cmd received, clearing active solenoid" );
    }
    else if( ( solenoidPkt.solenoidNbr >= 1 ) && ( solenoidPkt.solenoidNbr <= 6 ) )
    {
        m_activeSolenoid = solenoidPkt.solenoidNbr;
        m_solOnTime = GetTickCount();

        LoggingForm->AddLogMessage( "Set Solenoids cmd received, activating solenoid " + IntToStr( solenoidPkt.solenoidNbr ) );
    }
    else
    {
        LoggingForm->AddLogMessage( "Set Solenoids cmd received, invalid solenoid number " + IntToStr( solenoidPkt.solenoidNbr ) );
    }

    UpdateSolenoidsDisplay();
}


void TMPLSimMainForm::SetPIBParams( const WTTTS_PIB_PARAMS_PKT& pibParamsPkt )
{
    m_avgingFactor = pibParamsPkt.sensorAvgFactor;
    m_sampleRate   = pibParamsPkt.magnetSensRate;

    UpdateSensorParamsDisplay();

    LoggingForm->AddLogMessage( "Set PIB Params cmd received" );
}


void TMPLSimMainForm::ClearInputIndication( const WTTTS_PIB_CLR_INPUT_PKT& clearInputPkt )
{
    RFCDataForm->ClearSWIndications( clearInputPkt.deviceAddr, clearInputPkt.switchFlags );

    LoggingForm->AddLogMessage( "Clear Input Indication cmd received" );
}


bool TMPLSimMainForm::SendMPLEvent( BYTE pktType, WORD payloadLen, const void* pPayload )
{
    BYTE txBuffer[MAX_MPL_PKT_LEN];

    WTTTS_PKT_HDR* pPktHdr = (WTTTS_PKT_HDR*)txBuffer;

    pPktHdr->pktHdr    = MPL_HDR_RX;
    pPktHdr->pktType   = pktType;
    pPktHdr->seqNbr    = 0;
    pPktHdr->timeStamp = 0;
    pPktHdr->dataLen   = payloadLen;

    BYTE* pData = (BYTE*) &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

    if( payloadLen > 0 )
        memcpy( pData, pPayload, payloadLen );

    int packetHdrAndDataLen = sizeof( WTTTS_PKT_HDR ) + payloadLen;

    txBuffer[ packetHdrAndDataLen ] = CalcWTTTSChecksum( txBuffer, packetHdrAndDataLen );

    DWORD buffLen   = packetHdrAndDataLen + 1;
    DWORD bytesSent = m_commObj->CommSend( txBuffer, buffLen );

    return( bytesSent == buffLen );
}

