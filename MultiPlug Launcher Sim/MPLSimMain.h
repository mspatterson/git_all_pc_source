//---------------------------------------------------------------------------
#ifndef MPLSimMainH
#define MPLSimMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include "WTTTSProtocolUtils.h"
#include "CUDPPort.h"
#include "Comm32.h"
#include <Vcl.ComCtrls.hpp>
//---------------------------------------------------------------------------
class TMPLSimMainForm : public TForm
{
__published:	// IDE-managed Components
    TImage *TescoLogo;
    TGroupBox *ConnectionGB;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *Label4;
    TEdit *CommPortEdit;
    TEdit *BitRateEdit;
    TEdit *HostIPEdit;
    TEdit *UDPPortEdit;
    TRadioButton *SerialModeRB;
    TRadioButton *UDPModeRB;
    TGroupBox *DevSettingsGB;
    TComboBox *MagRateCombo;
    TLabel *Label5;
    TLabel *Label6;
    TComboBox *AvgFactorCombo;
    TGroupBox *DevInfoGB;
    TLabel *Label7;
    TEdit *RFCRateEdit;
    TLabel *Label8;
    TEdit *RFCTimeoutEdit;
    TLabel *Label9;
    TEdit *PairTimeoutEdit;
    TLabel *Label10;
    TEdit *SolTimeoutEdit;
    TLabel *Label11;
    TLabel *Label12;
    TLabel *Label13;
    TEdit *HWRevEdit;
    TEdit *FWRevEdit;
    TEdit *DevIDEdit;
    TGroupBox *DialogsGB;
    TButton *CfgDataBtn;
    TButton *RFCDataBtn;
    TButton *ViewLogBtn;
    TButton *ConnectBtn;
    TButton *DisconnectBtn;
    TGroupBox *ContactsGB;
    TLabel *Contact1Label;
    TLabel *Contact2Label;
    TLabel *Contact3Label;
    TLabel *Contact4Label;
    TGroupBox *SolenoidsGB;
    TLabel *Solenoid1Label;
    TLabel *Solenoid2Label;
    TLabel *Solenoid3Label;
    TLabel *Solenoid4Label;
    TLabel *Solenoid5Label;
    TLabel *Solenoid6Label;
    TTimer *SimTimer;
    TStatusBar *StatusBar;
    void __fastcall CfgDataBtnClick(TObject *Sender);
    void __fastcall ViewLogBtnClick(TObject *Sender);
    void __fastcall RFCDataBtnClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall ConnectBtnClick(TObject *Sender);
    void __fastcall DisconnectBtnClick(TObject *Sender);
    void __fastcall SimTimerTimer(TObject *Sender);

private:

    // Simulation control vars
    DWORD     m_lastRFCTime;
    BYTE      m_seqNbr;

    #define NBR_CONTACTS  4

    DWORD     m_solOnTime;
    int       m_activeSolenoid;
    bool      m_activeContacts[NBR_CONTACTS];
    int       m_avgingFactor;
    int       m_sampleRate;

    BYTE      m_powerMode; // 0 = Switching to Deep Sleep, 1 = Ready, 2 = Sleep mode

    // Rates, as received from WTTTS Host
    WORD      m_rfcRate;
    WORD      m_rfcTimeout;
    WORD      m_pairingTimeout;
    WORD      m_solenoidTimeout;

    // Comms vars
    CCommObj* m_commObj;

    #define COMM_BUFF_LEN  2048
    BYTE      m_commBuff[COMM_BUFF_LEN];
    DWORD     m_buffCount;

    // Command handlers
    void GetCfgData   ( const WTTTS_CFG_ITEM& cfgReq );
    void SetCfgData   ( const WTTTS_CFG_DATA& cfgData );
    void SetRates     ( const WTTTS_RATE_PKT& ratePtk );
    void SendVersionPkt( void );
    void SetDeepSleepMode( void );
    void SetRFChan    ( const WTTTS_SET_CHAN_PKT& chanPkt );
    void SetRFPower   ( const WTTTS_SET_RF_PWR_PKT& pwrPkt );
    void GetMinMaxVals( const WTTTS_REQ_MIN_MAX_PKT& minMaxReq );
    void MarkChanInUse( void );
    void SetContacts  ( const WTTTS_PIB_CONTACT_PKT& contactPkt );
    void SetSolenoid  ( const WTTTS_PIB_SOLENOID_PKT& solenoidPkt );
    void SetPIBParams ( const WTTTS_PIB_PARAMS_PKT& pibParamsPkt );
    void ClearInputIndication( const WTTTS_PIB_CLR_INPUT_PKT& clearInputPkt );

    void SendRequestForCmd( bool sendNow );

    bool SendMPLEvent( BYTE pktType, WORD payloadLen, const void* pPayload );

    // Display update methods
    void UpdateSensorParamsDisplay( void );
    void UpdateRatesDisplay( void );
    void UpdateContactsDisplay( void );
    void UpdateSolenoidsDisplay( void );

    String GetContactString( const String& sPrefix, int itemNbr, bool bIsClosed );

    void EnableCommControls( bool bCanConnect );

    #define WM_CLOSE_CONNECTION  ( WM_APP + 1 )
         // WParam: RFU
         // LParam: RFU

    void __fastcall WMCloseConnection( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_CLOSE_CONNECTION, TMessage, WMCloseConnection )
    END_MESSAGE_MAP(TForm)

public:
    __fastcall TMPLSimMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMPLSimMainForm *MPLSimMainForm;
//---------------------------------------------------------------------------
#endif
