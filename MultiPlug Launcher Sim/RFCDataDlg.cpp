#include <vcl.h>
#pragma hdrstop

#include "RFCDataDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TRFCDataForm *RFCDataForm;


// All track bars range from -100 to 100. To report reasonably realistic
// values, the track bar position gets scaled when reported to the caller.
// Scale factors are listed below.
const int PressScale  = 1000;   // 24-bit value
const int RPMScale    =    1;   //  8-bit value
const int TempScale   =    1;   //  8-bit value
const int MagScale    =  320;   // 16-bit value

// Battery voltage and mAh range from 0-100. To report reasonably realistic
// values, the track bar position gets scaled when reported to the caller.
// Scale factors are listed below.
const int BatVoltScale    = 50;
const int MilliAmpHrScale = 25;


__fastcall TRFCDataForm::TRFCDataForm(TComponent* Owner) : TForm(Owner)
{
    m_sbControls[0].enCB     = SB1EnCB;
    m_sbControls[0].sw1CB    = SB1SW1CB;
    m_sbControls[0].sw2CB    = SB1SW2CB;
    m_sbControls[0].xTBar    = S1XBar;
    m_sbControls[0].yTBar    = S1YBar;
    m_sbControls[0].zTBar    = S1ZBar;
    m_sbControls[0].zeroXBtn = SB1XZeroBtn;
    m_sbControls[0].zeroYBtn = SB1YZeroBtn;
    m_sbControls[0].zeroZBtn = SB1ZZeroBtn;

    m_sbControls[1].enCB     = SB2EnCB;
    m_sbControls[1].sw1CB    = SB2SW1CB;
    m_sbControls[1].sw2CB    = SB2SW2CB;
    m_sbControls[1].xTBar    = S2XBar;
    m_sbControls[1].yTBar    = S2YBar;
    m_sbControls[1].zTBar    = S2ZBar;
    m_sbControls[1].zeroXBtn = SB2XZeroBtn;
    m_sbControls[1].zeroYBtn = SB2YZeroBtn;
    m_sbControls[1].zeroZBtn = SB2ZZeroBtn;

    m_sbControls[2].enCB     = SB3EnCB;
    m_sbControls[2].sw1CB    = SB3SW1CB;
    m_sbControls[2].sw2CB    = SB3SW2CB;
    m_sbControls[2].xTBar    = S3XBar;
    m_sbControls[2].yTBar    = S3YBar;
    m_sbControls[2].zTBar    = S3ZBar;
    m_sbControls[2].zeroXBtn = SB3XZeroBtn;
    m_sbControls[2].zeroYBtn = SB3YZeroBtn;
    m_sbControls[2].zeroZBtn = SB3ZZeroBtn;

    m_sbControls[3].enCB     = SB4EnCB;
    m_sbControls[3].sw1CB    = SB4SW1CB;
    m_sbControls[3].sw2CB    = SB4SW2CB;
    m_sbControls[3].xTBar    = S4XBar;
    m_sbControls[3].yTBar    = S4YBar;
    m_sbControls[3].zTBar    = S4ZBar;
    m_sbControls[3].zeroXBtn = SB4XZeroBtn;
    m_sbControls[3].zeroYBtn = SB4YZeroBtn;
    m_sbControls[3].zeroZBtn = SB4ZZeroBtn;

    for( int iBd = 0; iBd < MAX_SENSOR_BOARDS; iBd++ )
    {
        m_sbControls[iBd].byBoardAddr       = ( iBd + 1 ) * 2;
        m_sbControls[iBd].enCB->OnClick     = EnableCheckBoxClick;
        m_sbControls[iBd].sw1CB->OnClick    = SwitchCheckBoxClick;
        m_sbControls[iBd].sw2CB->OnClick    = SwitchCheckBoxClick;
        m_sbControls[iBd].xTBar->OnChange   = MagValueChange;
        m_sbControls[iBd].yTBar->OnChange   = MagValueChange;
        m_sbControls[iBd].zTBar->OnChange   = MagValueChange;
        m_sbControls[iBd].zeroXBtn->OnClick = MagValueZeroClick;
        m_sbControls[iBd].zeroYBtn->OnClick = MagValueZeroClick;
        m_sbControls[iBd].zeroZBtn->OnClick = MagValueZeroClick;

        // Tag property for the zero buttons points to the trackbar that needs to be zeroed
        m_sbControls[iBd].zeroXBtn->Tag     = (int)m_sbControls[iBd].xTBar;
        m_sbControls[iBd].zeroYBtn->Tag     = (int)m_sbControls[iBd].yTBar;
        m_sbControls[iBd].zeroZBtn->Tag     = (int)m_sbControls[iBd].zTBar;
    }

    PIBSw1CB->OnClick = SwitchCheckBoxClick;
    PIBSw2CB->OnClick = SwitchCheckBoxClick;

    ResetMinMaxValues();
    ClearLatchedSwitches();
}


void __fastcall TRFCDataForm::FormShow(TObject *Sender)
{
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::TempZeroBtnClick(TObject *Sender)
{
    TempBar->Position = 0;
}


void __fastcall TRFCDataForm::BatVoltZeroBtnClick(TObject *Sender)
{
    BatVoltBar->Position = 0;
}


void __fastcall TRFCDataForm::MilliAmpHrZeroBtnClick(TObject *Sender)
{
    MilliAmpHrBar->Position = 0;
}


void __fastcall TRFCDataForm::PressZeroBtnClick(TObject *Sender)
{
    PressBar->Position = 0;
}


void __fastcall TRFCDataForm::RPMZeroBtnClick(TObject *Sender)
{
    RPMBar->Position = 0;
}


void __fastcall TRFCDataForm::PressBarChange(TObject *Sender)
{
    PressBar->Hint =  IntToStr( Pressure );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::BatVoltBarChange(TObject *Sender)
{
    BatVoltBar->Hint = IntToStr( BattVoltage );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::MilliAmpHrBarChange(TObject *Sender)
{
    MilliAmpHrBar->Hint = IntToStr( BattmAhUsed );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::TempBarChange(TObject *Sender)
{
    TempBar->Hint = IntToStr( Temp );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::RPMBarChange(TObject *Sender)
{
    RPMBar->Hint = IntToStr( RPM );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::EnableCheckBoxClick(TObject *Sender)
{
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::SwitchCheckBoxClick(TObject *Sender)
{
    // When a switch checkbox is clicked on, latch that indication
    // in the Tag property
    TCheckBox* pCB = dynamic_cast<TCheckBox*>(Sender);

    if( pCB != NULL )
    {
        if( pCB->Checked )
            pCB->Tag = 1;
    }
}


void __fastcall TRFCDataForm::MagValueChange(TObject *Sender)
{
    TTrackBar* pBar = dynamic_cast<TTrackBar*>( Sender );

    if( pBar != NULL )
    {
        int iNewValue = pBar->Position * MagScale;

        pBar->Hint = iNewValue;

        // Update the corresponding min/max val
        for( int iBd = 0; iBd < MAX_SENSOR_BOARDS; iBd++ )
        {
            int* piMin = NULL;
            int* piMax = NULL;

            if( pBar == m_sbControls[iBd].xTBar )
            {
                piMin = &(m_sbControls[iBd].minXValue);
                piMax = &(m_sbControls[iBd].maxXValue);
            }
            else if( pBar == m_sbControls[iBd].yTBar )
            {
                piMin = &(m_sbControls[iBd].minYValue);
                piMax = &(m_sbControls[iBd].maxYValue);
            }
            else if( pBar == m_sbControls[iBd].zTBar )
            {
                piMin = &(m_sbControls[iBd].minZValue);
                piMax = &(m_sbControls[iBd].maxZValue);
            }

            if( piMin != NULL )
            {
                if( iNewValue > *piMax )
                    *piMax = iNewValue;
                else if( iNewValue < *piMin )
                    *piMin = iNewValue;

                break;
            }
        }
    }

    UpdateValuesListview();
}


void __fastcall TRFCDataForm::MagValueZeroClick(TObject *Sender)
{
    TSpeedButton* pBtn = (TSpeedButton*)Sender;
    TTrackBar*    pBar = (TTrackBar*)( pBtn->Tag );

    pBar->Position = 0;

    UpdateValuesListview();
}


int TRFCDataForm::GetTemp( void )
{
    return TempBar->Position * TempScale;
}


int TRFCDataForm::GetBattVoltage( void )
{
    return BatVoltBar->Position * BatVoltScale;
}


int TRFCDataForm::GetBattmAhUsed( void )
{
    return MilliAmpHrBar->Position * MilliAmpHrScale;
}


int TRFCDataForm::GetRPM( void )
{
    return RPMBar->Position * RPMScale;
}


int TRFCDataForm::GetPress( void )
{
    return PressBar->Position * PressScale;
}


bool TRFCDataForm::GetSensorBdActive( int iSensor )
{
    if( ( iSensor < 0 ) || ( iSensor >= MAX_SENSOR_BOARDS ) )
        return false;

    return m_sbControls[iSensor].enCB->Checked;
}


int TRFCDataForm::GetSensorBdAddr( int iSensor )
{
    if( ( iSensor < 0 ) || ( iSensor >= MAX_SENSOR_BOARDS ) )
        return 0;

    return m_sbControls[iSensor].byBoardAddr;
}


bool TRFCDataForm::GetSensorBdSw1( int iSensor )
{
    if( ( iSensor < 0 ) || ( iSensor >= MAX_SENSOR_BOARDS ) )
        return false;

    return m_sbControls[iSensor].sw1CB->Checked;
}


bool TRFCDataForm::GetSensorBdSw2( int iSensor )
{
    if( ( iSensor < 0 ) || ( iSensor >= MAX_SENSOR_BOARDS ) )
        return false;

    return m_sbControls[iSensor].sw2CB->Checked;
}


int TRFCDataForm::GetSensorX( int iSensor )
{
    if( ( iSensor < 0 ) || ( iSensor >= MAX_SENSOR_BOARDS ) )
        return false;

    return m_sbControls[iSensor].xTBar->Position * MagScale;
}


int TRFCDataForm::GetSensorY( int iSensor )
{
    if( ( iSensor < 0 ) || ( iSensor >= MAX_SENSOR_BOARDS ) )
        return false;

    return m_sbControls[iSensor].yTBar->Position * MagScale;
}


int TRFCDataForm::GetSensorZ( int iSensor )
{
    if( ( iSensor < 0 ) || ( iSensor >= MAX_SENSOR_BOARDS ) )
        return false;

    return m_sbControls[iSensor].zTBar->Position * MagScale;
}


//
// Status Population
//
// All status information is displayed in a key editor grid. The grid
// operates in read-only mode though. The following list of strings
// identifies each key item.
//

typedef enum {
    SDE_BATTERY_VOLTAGE,
    SDE_MILLIAMP_HOUR,
    SDE_TEMP,
    SDE_RPM,
    SDE_PRESS,
    SDE_PIB_SW1,
    SDE_PIB_SW2,
    SDE_SB1_EN,
    SDE_SB1_ADDR,
    SDE_SB1_SW1,
    SDE_SB1_SW2,
    SDE_SB1_X,
    SDE_SB1_Y,
    SDE_SB1_Z,
    SDE_SB2_EN,
    SDE_SB2_ADDR,
    SDE_SB2_SW1,
    SDE_SB2_SW2,
    SDE_SB2_X,
    SDE_SB2_Y,
    SDE_SB2_Z,
    SDE_SB3_EN,
    SDE_SB3_ADDR,
    SDE_SB3_SW1,
    SDE_SB3_SW2,
    SDE_SB3_X,
    SDE_SB3_Y,
    SDE_SB3_Z,
    SDE_SB4_EN,
    SDE_SB4_ADDR,
    SDE_SB4_SW1,
    SDE_SB4_SW2,
    SDE_SB4_X,
    SDE_SB4_Y,
    SDE_SB4_Z,
    NBR_RFC_ENTRIES
} RFC_ENTRY;


static const AnsiString rfcKeys[NBR_RFC_ENTRIES] = {
    "Battery Voltage",
    "MilliAmp Per Hour",
    "Temp",
    "RPM",
    "Pressure",
    "PIB SW1",
    "PIB SW2",
    "SB1 Enabled",
    "SB1 Address",
    "SB1 SW1",
    "SB1 SW2",
    "SB1 X",
    "SB1 Y",
    "SB1 Z",
    "SB2 Enabled",
    "SB2 Address",
    "SB2 SW1",
    "SB2 SW2",
    "SB2 X",
    "SB2 Y",
    "SB2 Z",
    "SB3 Enabled",
    "SB3 Address",
    "SB3 SW1",
    "SB3 SW2",
    "SB3 X",
    "SB3 Y",
    "SB3 Z",
    "SB4 Enabled",
    "SB4 Address",
    "SB4 SW1",
    "SB4 SW2",
    "SB4 X",
    "SB4 Y",
    "SB4 Z",
};


void TRFCDataForm::UpdateValuesListview( void )
{
    RawDataVLE->Values[ rfcKeys[SDE_BATTERY_VOLTAGE] ] = IntToStr( BattVoltage );
    RawDataVLE->Values[ rfcKeys[SDE_MILLIAMP_HOUR] ]   = IntToStr( BattmAhUsed );
    RawDataVLE->Values[ rfcKeys[SDE_TEMP] ]            = IntToStr( Temp );
    RawDataVLE->Values[ rfcKeys[SDE_RPM] ]             = IntToStr( RPM );
    RawDataVLE->Values[ rfcKeys[SDE_PRESS] ]           = IntToStr( Pressure );
    RawDataVLE->Values[ rfcKeys[SDE_PIB_SW1] ]         = SW1 ? String("On") : String( "Off" );
    RawDataVLE->Values[ rfcKeys[SDE_PIB_SW2] ]         = SW2 ? String("On") : String( "Off" );

    for( int iBd = 0; iBd < MAX_SENSOR_BOARDS; iBd++ )
    {
        int iPropOffset;

        switch( iBd )
        {
            case 0:   iPropOffset = SDE_SB1_EN;   break;
            case 1:   iPropOffset = SDE_SB2_EN;   break;
            case 2:   iPropOffset = SDE_SB3_EN;   break;
            default:  iPropOffset = SDE_SB4_EN;   break;
        }

        RawDataVLE->Values[ rfcKeys[iPropOffset]   ] = SensorBdActive[iBd] ? String("Yes") : String( "No" );
        RawDataVLE->Values[ rfcKeys[iPropOffset+1] ] = IntToStr( SensorBdAddr[iBd] );
        RawDataVLE->Values[ rfcKeys[iPropOffset+2] ] = SensorBdSw1[iBd] ? String("On") : String( "Off" );
        RawDataVLE->Values[ rfcKeys[iPropOffset+3] ] = SensorBdSw2[iBd] ? String("On") : String( "Off" );
        RawDataVLE->Values[ rfcKeys[iPropOffset+4] ] = IntToStr( SensorBdX[iBd] );
        RawDataVLE->Values[ rfcKeys[iPropOffset+5] ] = IntToStr( SensorBdY[iBd] );
        RawDataVLE->Values[ rfcKeys[iPropOffset+6] ] = IntToStr( SensorBdZ[iBd] );
    }
}


bool TRFCDataForm::GetMinMaxValues( RFC_MIN_MAX_VALS& rfcMinMaxVals, int iBoard )
{
    if( ( iBoard < 0 ) || ( iBoard >= MAX_SENSOR_BOARDS ) )
        return false;

    rfcMinMaxVals.minX = m_sbControls[iBoard].minXValue;
    rfcMinMaxVals.maxX = m_sbControls[iBoard].maxXValue;

    rfcMinMaxVals.minY = m_sbControls[iBoard].minYValue;
    rfcMinMaxVals.maxY = m_sbControls[iBoard].maxYValue;

    rfcMinMaxVals.minZ = m_sbControls[iBoard].minZValue;
    rfcMinMaxVals.maxZ = m_sbControls[iBoard].maxZValue;

    return true;
}


void TRFCDataForm::ResetMinMaxValues( void )
{
    for( int iBd = 0; iBd < MAX_SENSOR_BOARDS; iBd++ )
    {
        m_sbControls[iBd].minXValue = MaxInt;
        m_sbControls[iBd].maxXValue = -MaxInt;

        m_sbControls[iBd].minYValue = MaxInt;
        m_sbControls[iBd].maxYValue = -MaxInt;

        m_sbControls[iBd].minZValue = MaxInt;
        m_sbControls[iBd].maxZValue = -MaxInt;
    }
}


void TRFCDataForm::ClearSWIndications( BYTE byDevAddr, BYTE byFlags )
{
    // The tag property of the switch checkboxes records the latched
    // state of the switch. Clear the tag for the passed device.
    if( byDevAddr == 0 )
    {
        if( byFlags & 0x01 )
            PIBSw1CB->Tag = 0;

        if( byFlags & 0x02 )
            PIBSw2CB->Tag = 0;
    }
    else
    {
        for( int iBd = 0; iBd < MAX_SENSOR_BOARDS; iBd++ )
        {
            if( m_sbControls[iBd].byBoardAddr == byDevAddr )
            {
                if( byFlags & 0x01 )
                    m_sbControls[iBd].sw1CB->Tag = 0;

                if( byFlags & 0x02 )
                    m_sbControls[iBd].sw2CB->Tag = 0;

                break;
            }
        }
    }
}


void TRFCDataForm::ClearLatchedSwitches( void )
{
    for( int iBd = 0; iBd < MAX_SENSOR_BOARDS; iBd++ )
    {
        // Tag property for switch checkbox latches the 'on' state of a switch
        m_sbControls[iBd].sw1CB->Tag = 0;
        m_sbControls[iBd].sw2CB->Tag = 0;
    }

    PIBSw1CB->Tag = 0;
    PIBSw1CB->Tag = 0;
}


