object RFCDataForm: TRFCDataForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'RFC Values'
  ClientHeight = 469
  ClientWidth = 745
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  DesignSize = (
    745
    469)
  PixelsPerInch = 96
  TextHeight = 13
  object SensBd1Panel: TPanel
    Left = 8
    Top = 215
    Width = 126
    Height = 249
    TabOrder = 6
    object Label3: TLabel
      Left = 25
      Top = 2
      Width = 73
      Height = 13
      Caption = 'Sensor Board 1'
    end
    object SB1XZeroBtn: TSpeedButton
      Left = 7
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object SB1YZeroBtn: TSpeedButton
      Left = 47
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object SB1ZZeroBtn: TSpeedButton
      Left = 87
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object S1XBar: TTrackBar
      Left = 7
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
    end
    object S1YBar: TTrackBar
      Left = 47
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 1
    end
    object S1ZBar: TTrackBar
      Left = 87
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 2
    end
    object SB1EnCB: TCheckBox
      Left = 11
      Top = 21
      Width = 97
      Height = 17
      Caption = 'Enabled'
      TabOrder = 3
    end
    object SB1SW1CB: TCheckBox
      Left = 11
      Top = 42
      Width = 50
      Height = 17
      Caption = 'SW 1'
      TabOrder = 4
    end
    object SB1SW2CB: TCheckBox
      Left = 71
      Top = 42
      Width = 50
      Height = 17
      Caption = 'SW 2'
      TabOrder = 5
    end
  end
  object SensBd2Panel: TPanel
    Left = 143
    Top = 215
    Width = 126
    Height = 249
    TabOrder = 7
    object Label4: TLabel
      Left = 25
      Top = 2
      Width = 73
      Height = 13
      Caption = 'Sensor Board 2'
    end
    object SB2XZeroBtn: TSpeedButton
      Left = 7
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object SB2YZeroBtn: TSpeedButton
      Left = 47
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object SB2ZZeroBtn: TSpeedButton
      Left = 87
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object S2XBar: TTrackBar
      Left = 7
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
    end
    object S2YBar: TTrackBar
      Left = 47
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 1
    end
    object S2ZBar: TTrackBar
      Left = 87
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 2
    end
    object SB2EnCB: TCheckBox
      Left = 11
      Top = 21
      Width = 97
      Height = 17
      Caption = 'Enabled'
      TabOrder = 3
    end
    object SB2SW1CB: TCheckBox
      Left = 11
      Top = 42
      Width = 50
      Height = 17
      Caption = 'SW 1'
      TabOrder = 4
    end
    object SB2SW2CB: TCheckBox
      Left = 71
      Top = 42
      Width = 50
      Height = 17
      Caption = 'SW 2'
      TabOrder = 5
    end
  end
  object PressPanel: TPanel
    Left = 160
    Top = 8
    Width = 44
    Height = 201
    TabOrder = 3
    DesignSize = (
      44
      201)
    object Label5: TLabel
      Left = 7
      Top = 2
      Width = 26
      Height = 13
      Caption = 'Press'
    end
    object PressZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = PressZeroBtnClick
    end
    object PressBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = PressBarChange
    end
  end
  object BatVoltPanel: TPanel
    Left = 8
    Top = 8
    Width = 44
    Height = 201
    TabOrder = 0
    DesignSize = (
      44
      201)
    object BatVoltLB: TLabel
      Left = 10
      Top = 2
      Width = 22
      Height = 13
      Caption = 'VBat'
    end
    object BatVoltZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = BatVoltZeroBtnClick
    end
    object BatVoltBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = BatVoltBarChange
    end
  end
  object MilliAmpHrPanel: TPanel
    Left = 60
    Top = 8
    Width = 44
    Height = 201
    TabOrder = 1
    DesignSize = (
      44
      201)
    object MillAmpHrLB: TLabel
      Left = 10
      Top = 2
      Width = 21
      Height = 13
      Caption = 'mAh'
    end
    object MilliAmpHrZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = MilliAmpHrZeroBtnClick
    end
    object MilliAmpHrBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = MilliAmpHrBarChange
    end
  end
  object RawDataVLE: TValueListEditor
    Left = 538
    Top = 10
    Width = 201
    Height = 455
    Anchors = [akTop, akRight, akBottom]
    TabOrder = 10
    TitleCaptions.Strings = (
      'Property'
      'Value')
    ColWidths = (
      97
      98)
  end
  object TempPanel: TPanel
    Left = 110
    Top = 8
    Width = 44
    Height = 201
    TabOrder = 2
    DesignSize = (
      44
      201)
    object Label6: TLabel
      Left = 8
      Top = 2
      Width = 26
      Height = 13
      Caption = 'Temp'
    end
    object TempZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = TempZeroBtnClick
    end
    object TempBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = TempBarChange
    end
  end
  object RPMPanel: TPanel
    Left = 210
    Top = 8
    Width = 44
    Height = 201
    TabOrder = 4
    DesignSize = (
      44
      201)
    object Label7: TLabel
      Left = 10
      Top = 2
      Width = 21
      Height = 13
      Caption = 'RPM'
    end
    object RPMZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = RPMZeroBtnClick
    end
    object RPMBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = RPMBarChange
    end
  end
  object SensBd3Panel: TPanel
    Left = 275
    Top = 215
    Width = 126
    Height = 249
    TabOrder = 8
    object Label1: TLabel
      Left = 25
      Top = 2
      Width = 73
      Height = 13
      Caption = 'Sensor Board 3'
    end
    object SB3XZeroBtn: TSpeedButton
      Left = 6
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object SB3YZeroBtn: TSpeedButton
      Left = 46
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object SB3ZZeroBtn: TSpeedButton
      Left = 86
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object S3XBar: TTrackBar
      Left = 6
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
    end
    object S3YBar: TTrackBar
      Left = 46
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 1
    end
    object S3ZBar: TTrackBar
      Left = 86
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 2
    end
    object SB3EnCB: TCheckBox
      Left = 11
      Top = 21
      Width = 97
      Height = 17
      Caption = 'Enabled'
      TabOrder = 3
    end
    object SB3SW1CB: TCheckBox
      Left = 11
      Top = 42
      Width = 50
      Height = 17
      Caption = 'SW 1'
      TabOrder = 4
    end
    object SB3SW2CB: TCheckBox
      Left = 71
      Top = 42
      Width = 50
      Height = 17
      Caption = 'SW 2'
      TabOrder = 5
    end
  end
  object SensBd4Panel: TPanel
    Left = 407
    Top = 215
    Width = 126
    Height = 249
    TabOrder = 9
    object Label2: TLabel
      Left = 25
      Top = 2
      Width = 73
      Height = 13
      Caption = 'Sensor Board 4'
    end
    object SB4XZeroBtn: TSpeedButton
      Left = 7
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object SB4YZeroBtn: TSpeedButton
      Left = 47
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object SB4ZZeroBtn: TSpeedButton
      Left = 87
      Top = 222
      Width = 21
      Height = 20
      Caption = '0'
    end
    object S4XBar: TTrackBar
      Left = 7
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
    end
    object S4YBar: TTrackBar
      Left = 47
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 1
    end
    object S4ZBar: TTrackBar
      Left = 87
      Top = 60
      Width = 34
      Height = 161
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 2
    end
    object SB4EnCB: TCheckBox
      Left = 11
      Top = 21
      Width = 97
      Height = 17
      Caption = 'Enabled'
      TabOrder = 3
    end
    object SB4SW1CB: TCheckBox
      Left = 11
      Top = 42
      Width = 50
      Height = 17
      Caption = 'SW 1'
      TabOrder = 4
    end
    object SB4SW2CB: TCheckBox
      Left = 71
      Top = 42
      Width = 50
      Height = 17
      Caption = 'SW 2'
      TabOrder = 5
    end
  end
  object PIBSwPanel: TPanel
    Left = 260
    Top = 8
    Width = 61
    Height = 201
    TabOrder = 5
    object Label8: TLabel
      Left = 6
      Top = 2
      Width = 42
      Height = 13
      Caption = 'Switches'
    end
    object PIBSw1CB: TCheckBox
      Left = 7
      Top = 22
      Width = 50
      Height = 17
      Caption = 'SW 1'
      TabOrder = 0
    end
    object PIBSw2CB: TCheckBox
      Left = 7
      Top = 45
      Width = 50
      Height = 17
      Caption = 'SW 2'
      TabOrder = 1
    end
  end
end
