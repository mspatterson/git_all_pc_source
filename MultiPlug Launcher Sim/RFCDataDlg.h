//---------------------------------------------------------------------------
#ifndef RFCDataDlgH
#define RFCDataDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
//---------------------------------------------------------------------------

class TRFCDataForm : public TForm
{
__published:    // IDE-managed Components
    TPanel *SensBd1Panel;
    TLabel *Label3;
    TSpeedButton *SB1XZeroBtn;
    TSpeedButton *SB1YZeroBtn;
    TTrackBar *S1XBar;
    TTrackBar *S1YBar;
    TTrackBar *S1ZBar;
    TSpeedButton *SB1ZZeroBtn;
    TPanel *SensBd2Panel;
    TLabel *Label4;
    TSpeedButton *SB2XZeroBtn;
    TSpeedButton *SB2YZeroBtn;
    TSpeedButton *SB2ZZeroBtn;
    TTrackBar *S2XBar;
    TTrackBar *S2YBar;
    TTrackBar *S2ZBar;
    TPanel *PressPanel;
    TLabel *Label5;
    TSpeedButton *PressZeroBtn;
    TTrackBar *PressBar;
    TPanel *BatVoltPanel;
    TLabel *BatVoltLB;
    TSpeedButton *BatVoltZeroBtn;
    TTrackBar *BatVoltBar;
    TPanel *MilliAmpHrPanel;
    TLabel *MillAmpHrLB;
    TSpeedButton *MilliAmpHrZeroBtn;
    TTrackBar *MilliAmpHrBar;
    TValueListEditor *RawDataVLE;
    TPanel *TempPanel;
    TLabel *Label6;
    TSpeedButton *TempZeroBtn;
    TTrackBar *TempBar;
    TPanel *RPMPanel;
    TLabel *Label7;
    TSpeedButton *RPMZeroBtn;
    TTrackBar *RPMBar;
    TPanel *SensBd3Panel;
    TLabel *Label1;
    TSpeedButton *SB3XZeroBtn;
    TSpeedButton *SB3YZeroBtn;
    TSpeedButton *SB3ZZeroBtn;
    TTrackBar *S3XBar;
    TTrackBar *S3YBar;
    TTrackBar *S3ZBar;
    TPanel *SensBd4Panel;
    TLabel *Label2;
    TSpeedButton *SB4XZeroBtn;
    TSpeedButton *SB4YZeroBtn;
    TSpeedButton *SB4ZZeroBtn;
    TTrackBar *S4XBar;
    TTrackBar *S4YBar;
    TTrackBar *S4ZBar;
    TCheckBox *SB1EnCB;
    TCheckBox *SB1SW1CB;
    TCheckBox *SB1SW2CB;
    TCheckBox *SB2EnCB;
    TCheckBox *SB2SW1CB;
    TCheckBox *SB2SW2CB;
    TCheckBox *SB3EnCB;
    TCheckBox *SB3SW1CB;
    TCheckBox *SB3SW2CB;
    TCheckBox *SB4EnCB;
    TCheckBox *SB4SW1CB;
    TCheckBox *SB4SW2CB;
    TPanel *PIBSwPanel;
    TLabel *Label8;
    TCheckBox *PIBSw1CB;
    TCheckBox *PIBSw2CB;
    void __fastcall PressBarChange(TObject *Sender);
    void __fastcall BatVoltZeroBtnClick(TObject *Sender);
    void __fastcall MilliAmpHrZeroBtnClick(TObject *Sender);
    void __fastcall BatVoltBarChange(TObject *Sender);
    void __fastcall MilliAmpHrBarChange(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall TempZeroBtnClick(TObject *Sender);
    void __fastcall PressZeroBtnClick(TObject *Sender);
    void __fastcall RPMZeroBtnClick(TObject *Sender);
    void __fastcall TempBarChange(TObject *Sender);
    void __fastcall RPMBarChange(TObject *Sender);

private:
    typedef struct {
        BYTE          byBoardAddr;
        TCheckBox*    enCB;
        TCheckBox*    sw1CB;
        TCheckBox*    sw2CB;
        TTrackBar*    xTBar;
        TTrackBar*    yTBar;
        TTrackBar*    zTBar;
        TSpeedButton* zeroXBtn;
        TSpeedButton* zeroYBtn;
        TSpeedButton* zeroZBtn;
        int           minXValue;
        int           maxXValue;
        int           minYValue;
        int           maxYValue;
        int           minZValue;
        int           maxZValue;
    } SENSOR_BOARD_CONTROLS;

    #define MAX_SENSOR_BOARDS  4    // Keep this in sync with MAX_NBR_MPL_SENSOR_BOARDS in WTTTSProtocolUtils.h

    SENSOR_BOARD_CONTROLS m_sbControls[MAX_SENSOR_BOARDS];

    void __fastcall EnableCheckBoxClick(TObject *Sender);
    void __fastcall SwitchCheckBoxClick(TObject *Sender);
    void __fastcall MagValueChange(TObject *Sender);
    void __fastcall MagValueZeroClick(TObject *Sender);

    int  GetBattVoltage( void );
    int  GetBattmAhUsed( void );
    int  GetTemp( void );
    int  GetPress( void );
    int  GetRPM( void );
    bool GetSW1( void ) { return PIBSw1CB->Checked || ( PIBSw1CB->Tag != 0 ); }
    bool GetSW2( void ) { return PIBSw2CB->Checked || ( PIBSw2CB->Tag != 0 ); }

    bool GetSensorBdActive( int iSensor );
    int  GetSensorBdAddr( int iSensor );
    bool GetSensorBdSw1( int iSensor );
    bool GetSensorBdSw2( int iSensor );
    int  GetSensorX( int iSensor );
    int  GetSensorY( int iSensor );
    int  GetSensorZ( int iSensor );

    void UpdateValuesListview( void );

    void ClearLatchedSwitches( void );

public:     // User declarations
    __fastcall TRFCDataForm(TComponent* Owner);

    __property int     BattVoltage = { read = GetBattVoltage };
    __property int     BattmAhUsed = { read = GetBattmAhUsed };
    __property int     Temp        = { read = GetTemp };
    __property int     RPM         = { read = GetRPM };
    __property int     Pressure    = { read = GetPress };
    __property bool    SW1         = { read = GetSW1 };
    __property bool    SW2         = { read = GetSW2 };

    __property bool    SensorBdActive[int] = { read = GetSensorBdActive };
    __property int     SensorBdAddr[int]   = { read = GetSensorBdAddr };
    __property bool    SensorBdSw1[int]    = { read = GetSensorBdSw1 };
    __property bool    SensorBdSw2[int]    = { read = GetSensorBdSw2 };
    __property int     SensorBdX[int]      = { read = GetSensorX };
    __property int     SensorBdY[int]      = { read = GetSensorY };
    __property int     SensorBdZ[int]      = { read = GetSensorZ };

    typedef struct {
         int minX;
         int maxX;
         int minY;
         int maxY;
         int minZ;
         int maxZ;
    } RFC_MIN_MAX_VALS;

    bool GetMinMaxValues( RFC_MIN_MAX_VALS& rfcMinMaxVals, int iBoard );
    void ResetMinMaxValues( void );

    void ClearSWIndications( BYTE byDevAddr, BYTE byFlags );
};
//---------------------------------------------------------------------------
extern PACKAGE TRFCDataForm *RFCDataForm;
//---------------------------------------------------------------------------
#endif
