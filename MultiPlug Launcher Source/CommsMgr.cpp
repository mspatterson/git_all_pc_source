//******************************************************************************
//
//  CommsMgr.cpp: This module provides a wrapper around the Sub Device
//        classes, and implements a thread to keep the comms layers
//        constantly updated.
//
//******************************************************************************
#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>

#include "CommsMgr.h"
#include "MPLDeviceNone.h"
#include "MPLDeviceWireless.h"
#include "CUDPPort.h"
#include "USBPortDiscovery.h"
#include "MPLRegistryInterface.h"

#pragma package(smart_init)


#define PLUG_DETECTED_ALERT_SW   MPL_ALERT_OUTPUT_SWITCH_NBR

const String TCommPoller::ConnResultStrings[TCommPoller::eCR_NumConnectResult] =
{
    "Initializing",
    "Bad Base Radio port",
    "Bad Plug Launcher port",
    "Bad Management port",
    "Connected",
    "Scanning for Radio",
    "Scanning for Plug Launcher",
    "Winsock() initialization error",
    "Unhandled comms thread exception",
};


static bool GetMutex( HANDLE aMutex, DWORD maxWait )
{
    // Return true if the mutex is acquired
    if( aMutex == NULL )
        return false;

    switch( WaitForSingleObject( aMutex, maxWait ) )
    {
        case WAIT_ABANDONED:
        case WAIT_OBJECT_0:
            // Have access to the mutex now
            return true;

        case WAIT_FAILED:
            // Not a good thing! Call failed.
            break;

        case WAIT_TIMEOUT:
            // Timeout expired!
            break;
    }

    // Fall through means mutex not acquired
    return false;
}


//
// Poll Thread Class Implementation
//

__fastcall TCommPoller::TCommPoller( HWND hwndEvent, DWORD dwEventMsg ) : TThread( false )
{
    // Save the event handle for later use
    m_hwndEvent  = hwndEvent;
    m_dwEventMsg = dwEventMsg;

    // Init state vars. Always set m_connectResult first before
    // setting the poller state
    SetPollerState( PS_INITIALIZING, eCR_Initializing );
    PostEventToClient( eCPE_Initializing );

    // Initialize device variables
    m_device = NULL;
    m_radio  = NULL;

    m_mplType = MT_NULL;

    m_dwRadioChanWaitTime = GetRadioChanWaitTimeout();

    // Caller must delete this object on terminate
    FreeOnTerminate = false;

    // Init the thread-safe packet transfer. A critical section
    // is used for this.
    InitializeCriticalSection( &m_criticalSection );

    FlushPendingPktQueue();
}


__fastcall TCommPoller::~TCommPoller()
{
    Terminate();

    switch( WaitForSingleObject( (HANDLE)Handle, 1000 /*msecs*/ ) )
    {
        case WAIT_FAILED:
            // Not a good thing - don't try to delete the object.
            break;

        case WAIT_ABANDONED:
        case WAIT_OBJECT_0:
            // Safe to delete the object in both these cases
            break;

        case WAIT_TIMEOUT:
            // Timeout expired! Don't delete the object in this case
            break;
    }

    // Release the critical section
    DeleteCriticalSection( &m_criticalSection );
}


void __fastcall TCommPoller::Execute()
{
    // Initialize and create objects
    // Winsock is required for UDP ports
    NameThreadForDebugging( AnsiString( "MPLThread" ) );

    if( !InitWinsock() )
    {
        SetPollerState( PS_COMMS_FAILED, eCR_WinsockError );
        return;
    }

    // Initialize alarm structures
    InitAlarms();

    // Set the Poller to scanning
    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );

    // Loop here now executing the thread
    try
    {
        while( !Terminated )
        {
            if( GetMPLPortLost() || ( PollerState == PS_BR_PORT_SCAN ) || ( PollerState == PS_MPL_PORT_SCAN ) )
            {
                // Check which Port Opening method to use. Read this from the
                // registry each time in case it changes
                if( GetAutoAssignCommPorts() )
                {
                    while( !OpenAutomaticPorts() )
                    {
                        if( Terminated )
                            break;

                        Sleep( 100 );
                    }
                }
                else
                {
                    while( !OpenAssignedPorts() )
                    {
                        if( Terminated )
                            break;

                        Sleep( 100 );
                    }
                }
            }

            if( Terminated )
                break;

            // If we are hunting for the MPL, loop here until it is found
            DWORD dwRFCWaitStart = GetTickCount();

            while( PollerState == PS_MPL_CHAN_SCAN )
            {
                m_device->Update();
                m_radio->Update();

                if( Terminated )
                    break;

                if( !m_radio->IsReceiving )
                {
                    // If the base radio is not receiving, we are in big trouble.
                    // Need to rescan for all devices
                    DeleteDevices();
                    PostEventToClient( eCPE_CommsLost );
                    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
                }
                else if( m_device->DeviceIsRecving )
                {
                    SetPollerState( PS_RUNNING, eCR_Connected );
                    PostEventToClient( eCPE_InitComplete );

                    break;
                }
                else if( HaveTimeout( dwRFCWaitStart, m_dwRadioChanWaitTime ) )
                {
                    // Time to switch channels - the base radio can figure out
                    // the next channel
                    m_radio->GoToNextRadioChannel( TBaseRadioDevice::BRN_MPL );

                    dwRFCWaitStart = GetTickCount();
                }
            }

            if( Terminated )
                break;

            // Do are work if we are in the running state
            if( PollerState == PS_RUNNING )
            {
                m_device->Update();
                m_radio->Update();

                if( !m_radio->IsReceiving )
                {
                    // If the base radio is not receiving, we are in big trouble.
                    // Need to rescan for all devices
                    DeleteDevices();
                    PostEventToClient( eCPE_CommsLost );
                    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
                }
                else if( !m_device->DeviceIsRecving )
                {
                    // Note we don't delete devices in this case - first try different channels
                    PostEventToClient( eCPE_CommsLost );
                    SetPollerState( PS_MPL_CHAN_SCAN, eCR_HuntingForMPL );
                }
                else
                {
                    // Get any sample data
                    TMPLDevice::MPL_READING nextReading;

                    while( m_device->GetLastAvgReading( nextReading ) )
                    {
                        AddPktToPendingQueue( nextReading );
                        m_device->Update();

                        if( Terminated )
                            break;
                    }

                    // Check if a refresh of settings has been requested
                    if( m_refreshSettings )
                    {
                        // Refresh requested - get new values
                        m_currUnits = m_newUnits;

                        AVERAGING_PARAMS avgParams;
                        GetAveragingSettings( AT_RPM, avgParams );

    // TODO                    m_device->SetAveraging( AT_TORQUE, avgParams );

                        m_dwRadioChanWaitTime = GetRadioChanWaitTimeout();

                        m_device->RefreshSettings();

                        m_refreshSettings = false;
                    }
                }
            }

            // Work done for this iteration
            Sleep( 5 );
        }
    }
    catch( ... )
    {
        SetPollerState( PS_EXCEPTION, eCR_ThreadException );
        PostEventToClient( eCPE_ThreadFailed );
    }

    // Release all comm mgr resources
    DeleteDevices();

    ShutdownWinsock();
}


bool TCommPoller::OpenAssignedPorts( void )
{
    while( true )
    {
        // Delete all devices safely
        DeleteDevices();

        // Create the devices
        CreateDevices();

        // Set the result to Scanning
        SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
        PostEventToClient( eCPE_Initializing );

        COMMS_CFG mgmtCommCfg;
        COMMS_CFG baseCommCfg;
        COMMS_CFG deviceCommCfg;

        GetCommSettings( DCT_MGMT, mgmtCommCfg );
        GetCommSettings( DCT_BASE, baseCommCfg );
        GetCommSettings( DCT_MPL,  deviceCommCfg );

        // Attempt to connect our devices
        if( ( m_device->Connect( deviceCommCfg ) ) && ( m_radio->Connect( baseCommCfg ) ) && ( m_mgmtPort->Connect( mgmtCommCfg ) ) )
        {
            // Check if we have a Null-Radio
            if( m_radio->ClassNameIs( "TNullBaseRadio" ) )
            {
                SetPollerState( PS_MPL_CHAN_SCAN, eCR_HuntingForMPL );
                PostEventToClient( eCPE_Initializing );

                return true;
            }

            // The port could be opened, wait here for 2 seconds for a
            // a message to appear from the base radio
            DWORD dwStartPktCount = m_radio->StatusPktCount;

            DWORD dwEndWait = GetTickCount() + 2500;

            while( GetTickCount() < dwEndWait )
            {
                m_radio->Update();

                if( m_radio->StatusPktCount > dwStartPktCount + 5 )
                {
                    // Found the base radio. Set the MPL radio port onto its
                    // default channel and then move on to the MPL scan.
                    m_radio->SetRadioChannel( GetDefaultRadioChan(), TBaseRadioDevice::BRN_MPL );

                    AfterCommConnect();

                    SetPollerState( PS_MPL_CHAN_SCAN, eCR_HuntingForMPL );
                    PostEventToClient( eCPE_Initializing );

                    return true;
                }

                if( Terminated )
                    return false;

                Sleep( 50 );
            }

            m_radio->Disconnect();
        }

        // Could not connect to devices, sleep for one second, then try
        // again on the same ports. Break the sleep into 100ms intervals
        // so we can respond to a terminate request quickly.
        for( int iSleepIndex = 0; iSleepIndex < 10; iSleepIndex++ )
        {
            Sleep( 100 );

            if( Terminated )
                return false;
        }
    }
}


bool TCommPoller::OpenAutomaticPorts( void )
{
    while( true )
    {
        // Delete all devices safely
        DeleteDevices();

        // Create the devices
        CreateDevices();

        // Set the result to Scanning
        SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
        PostEventToClient( eCPE_Initializing );

        COMMS_CFG baseCommCfg;
        COMMS_CFG deviceCommCfg;
        COMMS_CFG mgmtCommCfg;

        GetCommSettings( DCT_BASE, baseCommCfg );
        GetCommSettings( DCT_MPL,  deviceCommCfg );
        GetCommSettings( DCT_MGMT, mgmtCommCfg );

        // For automatic ports, all port types must be serial
        baseCommCfg.portType   = CT_SERIAL;
        deviceCommCfg.portType = CT_SERIAL;
        mgmtCommCfg.portType   = CT_SERIAL;

        baseCommCfg.portParam   = 115200;
        deviceCommCfg.portParam = 115200;
        mgmtCommCfg.portParam   = 115200;

        // Init search loop
        int iPortNbr   = baseCommCfg.portName.ToIntDef( 1 );
        int iPortStart = iPortNbr;

        while( true )
        {
            // Set the Port Number for the radio
            baseCommCfg.portName = IntToStr( iPortNbr );

            // Attempt to connect our Base Radio
            if( m_radio->Connect( baseCommCfg ) )
            {
                // The port could be opened, wait here for 2 seconds for a
                // a message to appear from the base radio. The base radio
                // sends periodic status messages even if no command is
                // sent to it
                DWORD dwStartPktCount = m_radio->StatusPktCount;

                DWORD dwEndWait = GetTickCount() + 2500;

                while( GetTickCount() < dwEndWait )
                {
                    m_radio->Update();

                    if( m_radio->StatusPktCount > dwStartPktCount + 5 )
                    {
                        // Found the base radio. Set the MPL radio port onto its
                        // default channel now
                        m_radio->SetRadioChannel( GetDefaultRadioChan(), TBaseRadioDevice::BRN_MPL );

                        SetPollerState( PS_MPL_PORT_SCAN, eCR_HuntingForMPL );
                        PostEventToClient( eCPE_Initializing );

                        // If we can't find the ports, return false
                        if( !FindDevPorts( baseCommCfg, deviceCommCfg, mgmtCommCfg ) )
                            return false;

                        // If the ports fail to connect, return false
                        if( ( !m_device->Connect( deviceCommCfg ) ) || ( !m_mgmtPort->Connect( mgmtCommCfg ) ) )
                            return false;

                        SaveCommSettings( DCT_BASE, baseCommCfg );
                        SaveCommSettings( DCT_MPL,  deviceCommCfg );
                        SaveCommSettings( DCT_MGMT, mgmtCommCfg );

                        AfterCommConnect();

                        SetPollerState( PS_MPL_CHAN_SCAN, eCR_HuntingForMPL );
                        PostEventToClient( eCPE_Initializing );

                        return true;
                    }

                    if( Terminated )
                        return false;

                    Sleep( 50 );
                }

                m_radio->Disconnect();
            }

            if( Terminated )
                return false;

            // Autoscan enabled, try next port
            iPortNbr++;

            if( iPortStart == iPortNbr )
                break;

            if( iPortNbr > 999 )
                iPortNbr = 1;

            Sleep( 10 );
        }

        Sleep( 100 );
    }
}


bool TCommPoller::FindDevPorts( const COMMS_CFG& baseCommCfg, COMMS_CFG& deviceCommCfg, COMMS_CFG& mgmtCommCfg )
{
    // Set up the CommPort and Connection Params
    CCommPort* pCommPort = new CCommPort();

    CCommPort::CONNECT_PARAMS CommPortParams;

    CommPortParams.dwLineBitRate = 9600;

    int iPortNum = 0;
    int iR0Port  = -1;
    int iR1Port  = -1;

    // Iterate through 1000 port numbers, attempt to connect to each
    while( iPortNum <= 999 )
    {
        CommPortParams.iPortNumber = iPortNum;

        // Check if we can connect to the port, if so, wiggle it's signals
        if( pCommPort->Connect( &CommPortParams ) == CCommObj::ERR_NONE )
        {
            // Get the Port Type of the current CommPort
            PORT_TYPE PortType = GetPortType( pCommPort );

            // Check if the Port Type returned was one we care about
            if( PortType == eRadio0 )
                iR0Port = iPortNum;
            else if( PortType == eRadio1 )
                iR1Port = iPortNum;
        }

        // Always disconnect to be safe
        pCommPort->Disconnect();

        // If we've found both ports in this iteration, try to find Mgmt
        if( ( iR0Port >= 0 ) && ( iR1Port >= 0 ) )
        {
            TList* pPorts = new TList();

            int iRadioPort = baseCommCfg.portName.ToIntDef( -1 );

            if( !GetAssociatedPorts( iRadioPort, pPorts ) )
            {
                delete pPorts;
                break;
            }

            pPorts->Remove( (TObject*)( iRadioPort ) );
            pPorts->Remove( (TObject*)( iR0Port    ) );
            pPorts->Remove( (TObject*)( iR1Port    ) );

            if( pPorts->Count != 1 )
            {
                delete pPorts;
                break;
            }

            // The MPL device uses radio port 1 (TT Mgr uses port 0)
            deviceCommCfg.portName = IntToStr( iR1Port );
            mgmtCommCfg.portName   = IntToStr( (int)( pPorts->List[0] ) );

            delete pPorts;
            delete pCommPort;

            return true;
        }

        // Be safe and check for termination
        if( Terminated )
            break;

        Sleep( 10 );

        iPortNum++;
    }

    // Clean-up and return
    delete pCommPort;

    return false;
}


TCommPoller::PORT_TYPE TCommPoller::GetPortType( CCommPort* pPort )
{
    // Init our Ctrl Status variables
    BYTE bInitCtrlStatus = 0;
    BYTE bHighCtrlStatus = 0;
    BYTE bLastCtrlStatus = 0;

    if( !WigglePort( pPort, bInitCtrlStatus, bHighCtrlStatus, bLastCtrlStatus ) )
        return eUnknown;

    // WigglePort() will raise RTS, drop it, and then raise it again. The base
    // radio reports the inverse state of the signal, so look in the control
    // signal status byte for a low-high-low sequence.
    if( !( bInitCtrlStatus & 0x01 ) && ( bHighCtrlStatus & 0x01 ) && !( bLastCtrlStatus & 0x01 ) )
        return eRadio0;
    else if( !( bInitCtrlStatus & 0x02 ) && ( bHighCtrlStatus & 0x02 ) && !( bLastCtrlStatus & 0x02 ) )
        return eRadio1;

    return eUnknown;
}


bool TCommPoller::WigglePort( CCommPort* pPort, BYTE& bInitCtrlStatus, BYTE& bHighCtrlStatus, BYTE& bLastCtrlStatus )
{
    // Init our Base Radio Packet variables
    BASE_STATUS_STATUS_PKT RadioStatusPkt;
    time_t tLastPkt;

    // Set RTS bit to low
    pPort->SetRTS( true );

    DWORD dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the Initial CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bInitCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    // Set RTS bit to high
    pPort->SetRTS( false );

    dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the High CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bHighCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    // Set RTS bit to low
    pPort->SetRTS( true );

    dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the Last CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bLastCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    return true;
}


void TCommPoller::AfterCommConnect( void )
{
    m_currUnits = GetDefaultUnitsOfMeasure();
    m_newUnits  = GetDefaultUnitsOfMeasure();

    m_refreshSettings  = true;
}


void TCommPoller::CreateDevices( void )
{
    // If we are auto-assigning ports, then real objects are required.
    // Otherwise, construct the object type based on the registry setting.
    if( GetAutoAssignCommPorts() )
    {
        m_mgmtPort = new TRealMgmtPort();
        m_radio    = new TRealBaseRadio();
        m_device   = new TWirelessMPLDevice();
    }
    else
    {
        // Create devices based on the registry setting
        COMMS_CFG mgmtCommCfg;
        GetCommSettings( DCT_MGMT, mgmtCommCfg );

        COMMS_CFG baseCommCfg;
        GetCommSettings( DCT_BASE, baseCommCfg );

        COMMS_CFG mplCommCfg;
        GetCommSettings( DCT_MPL, mplCommCfg );

        if( mgmtCommCfg.portType == CT_UNUSED )
            m_mgmtPort = new TNullMgmtPort();
        else
            m_mgmtPort = new TRealMgmtPort();

        if( baseCommCfg.portType == CT_UNUSED )
            m_radio = new TNullBaseRadio();
        else
            m_radio = new TRealBaseRadio();

        if( mplCommCfg.portType == CT_UNUSED )
        {
            m_mplType = MT_NULL;
            m_device  = new TNullMPLDevice();
        }
        else
        {
            m_mplType = MT_MPL;
            m_device  = new TWirelessMPLDevice();
        }
    }

    // For the MPL, we use the 'auto switch control' feature
    m_radio->AutoSwitchControl = true;

    // Hook the MPL device event handlers
    m_device->OnDeviceChanged   = OnMPLChanged;
    m_device->OnDeviceConnected = OnMPLConnected;
    m_device->OnRFCRecvd        = OnMPLRFCRecvd;
    m_device->OnPlugDetected    = OnPlugDetected;
    m_device->OnCleaningDone    = OnCleaningDone;
    m_device->OnResetFlagDone   = OnResetFlagDone;
}


void TCommPoller::DeleteDevices( void )
{
    // Delete the radio if it exists
    if( m_radio != NULL )
    {
        delete m_radio;
        m_radio = NULL;
    }

    // Delete the device if it exists
    if( m_device != NULL )
    {
        delete m_device;
        m_device = NULL;
    }

    // Delete the Mgmt Port if it exists
    if( m_mgmtPort != NULL )
    {
        delete m_mgmtPort;
        m_mgmtPort = NULL;
    }
}


bool TCommPoller::LaunchPlug( int iPlug )
{
    // Instructs the MPL to activate the launch mechanism
    if( m_device == NULL )
        return false;

    return m_device->LaunchPlug( iPlug );
}


bool TCommPoller::CleanPlug( int iPlug )
{
    // Instructs the MPL to activate the cleaning mechanism
    if( m_device == NULL )
        return false;

    return m_device->CleanPlug( iPlug );
}


bool TCommPoller::DoResetFlag( void )
{
    // Instructs the MPL to activate its 'reset flag' solenoid. Returns true
    // on success.
    if( m_device == NULL )
        return false;

    return m_device->ActivateResetFlag();
}


void TCommPoller::ClearPlugDetected( void )
{
    if( m_device != NULL )
        m_device->AckPlugDetected();
}


void TCommPoller::ClearSW2Indication( void )
{
    if( m_device != NULL )
        m_device->ClearPIBSwitchOn( 1 );
}


bool TCommPoller::AssertAlertOutput( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->SetOutputSwitch( MPL_ALERT_OUTPUT_SWITCH_NBR, eBC_TurnOn );
}


void TCommPoller::ClearAlertOutput( void )
{
    if( m_radio != NULL )
        m_radio->SetOutputSwitch( MPL_ALERT_OUTPUT_SWITCH_NBR, eBC_TurnOff );
}


bool TCommPoller::CommsGetStats( PORT_STATS& portStats )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_device != NULL )
    {
        m_device->GetCommStats( portStats );
        return true;
    }

    return false;
}


void TCommPoller::CommsClearStats( void )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_device != NULL )
        m_device->ClearCommStats();
}


bool TCommPoller::GetRadioStats( PORT_STATS& radioStats )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_radio != NULL )
        return m_radio->GetCommStats( radioStats );

    return false;
}


void TCommPoller::ClearRadioStats( void )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_radio != NULL )
        m_radio->ClearCommStats();
}


bool TCommPoller::MgmtGetStats( PORT_STATS& mgmtStats )
{
    // We only report the connection type for the management port
    if( m_mgmtPort == NULL )
        return false;

    if( !m_mgmtPort->GetCommStats( mgmtStats ) )
        return false;

    return true;
}


void TCommPoller::ClearMgmtStats( void )
{
    // Currently this does nothing, as we don't track any data traffic
    // on the management port
}


UnicodeString TCommPoller::GetMPLTypeText( void )
{
    // No need to access thread lock for this function
    return ::MPLTypeText[m_mplType];
}


String TCommPoller::GetMPLPortDesc( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return "";

    // If we aren't connected, overwrite the dev status with comm status
    if( PollerState != PS_RUNNING )
        return m_connectResult;

    return m_device->DeviceStatus;
}


String TCommPoller::GetMPLStatusText( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return "";

    // If we aren't connected, overwrite the dev status with comm status
    if( PollerState != PS_RUNNING )
        return m_connectResult;

    return m_device->DeviceStatus;
}


bool TCommPoller::GetMPLIsIdle( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    // Rely on the 'can start data collection' property for this
    if( !m_device->IsConnected )
        return false;

    return m_device->DeviceIsIdle;
}


bool TCommPoller::GetMPLIsCalibrated( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->DeviceIsCalibrated;
}


bool TCommPoller::GetMPLIsRecving( void )
{
    if( m_device == NULL )
        return false;

    return m_device->DeviceIsRecving;
}


bool TCommPoller::GetMPLIsConn( void )
{
    if( m_radio == NULL )
        return false;

    return m_device->IsConnected;
}


bool TCommPoller::GetUsingMPLSim( void )
{
    COMMS_CFG wtttsCfg;
    GetCommSettings( DCT_MPL, wtttsCfg );

    if( wtttsCfg.portType == CT_UDP )
        return true;
    else
        return false;
}


bool TCommPoller::GetCanStartCycle( void )
{
    if( m_device == NULL )
        return false;

    return m_device->CanStartCycle;
}


bool TCommPoller::GetPlugDetected( void )
{
    if( m_device == NULL )
        return false;

    return m_device->PlugDetected;
}


bool TCommPoller::GetCleanDone( void )
{
    if( m_device == NULL )
        return true;

    return m_device->CleaningDone;
}


bool TCommPoller::GetMPLPortLost( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->PortLost;
}


TMPLDevice::BatteryLevelType TCommPoller::GetMPLBattLevel( void )
{
    if( m_device == NULL )
        return TMPLDevice::eBL_Unknown;

    return m_device->BatteryLevel;
}


String TCommPoller::GetBaseRadioStatus( void )
{
    if( m_radio == NULL )
        return "";

    return m_radio->DeviceStatus;
}


bool TCommPoller::GetBaseRadioPortLost( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->PortLost;
}


bool TCommPoller::GetBaseRadioIsConn( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->IsConnected;
}


bool TCommPoller::GetBaseRadioIsRecving( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->IsReceiving;
}


void TCommPoller::SetPollerState( POLLER_STATE epsNew, CONNECT_RESULT eCRMsgEnum )
{
    m_pollerState   = epsNew;
    m_connectResult = ConnResultStrings[eCRMsgEnum];
}


void TCommPoller::PostEventToClient( CommPollerEvent eEvent, DWORD dwLParam )
{
    ::PostMessage( m_hwndEvent, m_dwEventMsg, eEvent, dwLParam );
}


bool TCommPoller::GetLastStatusPktInfo( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->ShowLastRFCPkt( pCaptions, pValues );
}


bool TCommPoller::GetLastStatusPkt( TDateTime& pktTime, DWORD& rfcCount, GENERIC_MPL_RFC_PKT& lastPkt )
{
    if( m_device == NULL )
       return false;

    return m_device->GetLastRFCPkt( pktTime, rfcCount, lastPkt );
}


bool TCommPoller::GetLastAvgReading( TMPLDevice::MPL_READING& lastPkt )
{
    // TODO
    return false;
}


bool TCommPoller::GetLastMinMaxReading( TMPLDevice::MIN_MAX_DATA& lastPkt )
{
    // TODO
    return false;
}


bool TCommPoller::GetLastRadioStatusPktInfo( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_radio == NULL )
       return false;

    return m_radio->ShowLastStatusPkt( pCaptions, pValues );
}


int TCommPoller::GetRadioChannel( TBaseRadioDevice::BASE_RADIO_NBR radioNbr )
{
    if( ( m_radio == NULL ) || ( !m_radio->IsReceiving ) )
        return 0;

    time_t tLastPkt;
    BASE_STATUS_STATUS_PKT statusPkt;

    if( !m_radio->GetLastStatusPkt( tLastPkt, statusPkt ) )
        return 0;

    if( ( radioNbr >= 0 ) && ( radioNbr < NBR_BASE_RADIO_CHANS ) )
        return statusPkt.radioInfo[radioNbr].chanNbr;

    // Fall through means invalid radio number passed
    return 0;
}


TBaseRadioDevice::SET_RADIO_CHAN_RESULT TCommPoller::SetRadioChannel( int newChanNbr, TBaseRadioDevice::BASE_RADIO_NBR radioNbr )
{
    if( m_radio == NULL )
       return TBaseRadioDevice::SRCR_NO_RADIO;

     return m_radio->SetRadioChannel( newChanNbr, radioNbr );
}


void TCommPoller::RefreshSettings( int unitsOfMeasure )
{
    // Record that a refresh has been requested. The actual refreshing
    // of data will happen in the worker thread.
    m_newUnits        = unitsOfMeasure;
    m_refreshSettings = true;
}


bool TCommPoller::GetCalDataRaw( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->GetCalDataRaw( pCaptions, pValues );
}


bool TCommPoller::GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->GetCalDataFormatted( pCaptions, pValues );
}


bool TCommPoller::SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    if( m_device == NULL )
       return false;

    return m_device->SetCalDataFormatted( pCaptions, pValues );
}


bool TCommPoller::ReloadCalDataFromDevice( void )
{
    if( m_device == NULL )
       return false;

    return m_device->ReloadCalDataFromDevice();
}


bool TCommPoller::WriteCalDataToDevice( void )
{
    if( m_device == NULL )
       return false;

    return m_device->WriteCalDataToDevice();
}


bool TCommPoller::GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo )
{
    if( m_device == NULL )
       return false;

    return m_device->GetDeviceHWInfo( devHWInfo );
}


bool TCommPoller::GetDeviceBatteryInfo( BATTERY_INFO& battInfo )
{
    if( m_device == NULL )
       return false;

    return m_device->GetDeviceBatteryInfo( battInfo );
}


//
// Queue Handling Functions
//

static int AdvanceIndex( int currIndex )
{
    currIndex++;

    if( currIndex >= MAX_MPL_READINGS )
        currIndex = 0;

    return currIndex;
}


void TCommPoller::FlushPendingPktQueue( void )
{
    // Flushes all pending packets from the queue. This is a thread-safe function.
    EnterCriticalSection( &m_criticalSection );

    try
    {
        m_pktReadIndex  = 0;
        m_pktWriteIndex = 0;
    }
    __finally
    {
        LeaveCriticalSection ( &m_criticalSection );
    }
}


void TCommPoller::AddPktToPendingQueue( const TMPLDevice::MPL_READING& nextReading )
{
    // Adds a packet received by the serial port into the pending queue. Will
    // overwrite an old packet's data if the queue is full.
    EnterCriticalSection ( &m_criticalSection );

    // Make sure we have room for this packet. If not, toss the
    // oldest packet.
    int newWriteIndex = AdvanceIndex( m_pktWriteIndex );

    if( newWriteIndex == m_pktReadIndex )
        m_pktReadIndex = AdvanceIndex( m_pktReadIndex );

    // Store the sample
    m_pktQueue[m_pktWriteIndex] = nextReading;

    // Okay to advance the write index now
    m_pktWriteIndex = newWriteIndex;

    LeaveCriticalSection ( &m_criticalSection );
}


bool TCommPoller::GetPktFromQueue( TMPLDevice::MPL_READING& nextReading )
{
    // Gets the next packet from the queue. Returns true if a packet was
    // pending (and copied), false otherwise. This function is thread-safe.
    bool havePkt = false;

    EnterCriticalSection ( &m_criticalSection );

    // Check if we have packets to report
    if( m_pktReadIndex != m_pktWriteIndex )
    {
        nextReading = m_pktQueue[m_pktReadIndex];
        havePkt     = true;

        // Advance our read index now
        m_pktReadIndex = AdvanceIndex( m_pktReadIndex );
    }

    LeaveCriticalSection ( &m_criticalSection );

    return havePkt;
}


void __fastcall TCommPoller::OnMPLConnected( TObject* pMPLDev )
{
    // TODO
}


void __fastcall TCommPoller::OnMPLChanged( TObject* pMPLDev )
{
    // TODO
}


void __fastcall TCommPoller::OnMPLRFCRecvd( TObject* pMPLDev )
{
    // If an RFC has been received, process alarms and let the client know
    ProcessAlarms();
    PostEventToClient( eCPE_RFCRecvd );
}


void __fastcall TCommPoller::OnPlugDetected( TObject* pMPLDev )
{
    // On detection of a plug, first turn on the 'plug alert' output switch
    m_radio->SetOutputSwitch( PLUG_DETECTED_ALERT_SW, eBC_TurnOn );

    // Now let the client know
    PostEventToClient( eCPE_PlugDetected );
}


void __fastcall TCommPoller::OnCleaningDone( TObject* pMPLDev )
{
    // Only need to post this to the client
    PostEventToClient( eCPE_CleaningDone );
}


void __fastcall TCommPoller::OnResetFlagDone( TObject* pMPLDev )
{
    // Only need to post this to the client
    PostEventToClient( eCPE_ResetFlagDone );
}


//
// Alarm Management
//

void TCommPoller::InitAlarms( void )
{
    for( int iAlarm = 0; iAlarm < eAT_NbrAlarmTypes; iAlarm++ )
    {
        m_alarmStatus[iAlarm].eState       = eAS_NoAlarm;
        m_alarmStatus[iAlarm].tLastAck.Val = 0.0;
    }
}


void TCommPoller::ProcessAlarms( void )
{
    // Look for alarm conditions and report them to the notification window.
    // Alarms are processed as follows:
    //
    //   Device State       eState          Action
    //   -------------------------------------------------------------
    //   No alarm present   eAS_NoAlarm     None
    //   No alarm present   eAS_NewAlarm    Alarm condition has gone away, stay in this state until user ack's it
    //   No alarm present   eAS_AckdAlarm   Alarm condition has gone away, can reset state to eAS_NoAlarm
    //   Alarm present      eAS_NoAlarm     Switch to either new alarm or ack'd alarm state, depending on tLastAck
    //                                      If new alarm, then post event to notification window
    //   Alarm present      eAS_NewAlarm    Alarm condition is still present and not yet ack'd by user
    //                                      Repost notification if ack timeout has elapsed
    //   Alarm present      eAS_AckdAlarm   Alarm condition is still present and has been ack'd by user
    //                                      Repost notification if ack timeout has elapsed

    const __int64 AckMinutesTimeout = 2;

    // Check for pending alarms
    if( m_device->IsConnected )
    {
        switch( m_device->BatteryLevel )
        {
            case TMPLDevice::eBL_Med:
            case TMPLDevice::eBL_OK:
                // Can clear an ack'd alarms in this condition
                if( m_alarmStatus[eAT_LowBattery].eState == eAS_AckdAlarm )
                    m_alarmStatus[eAT_LowBattery].eState = eAS_NoAlarm;
                break;

            case TMPLDevice::eBL_Low:
                // This is an alarm condition
                switch( m_alarmStatus[eAT_LowBattery].eState )
                {
                    case eAS_NoAlarm:
                        // If the alarm has already been acknowledged, consider
                        // this a continuation of the previous alarm. Otherwise,
                        // it is a new alarm
                        if( Now() > IncMinute( m_alarmStatus[eAT_LowBattery].tLastAck, AckMinutesTimeout ) )
                        {
                            m_alarmStatus[eAT_LowBattery].eState = eAS_NewAlarm;
                            PostEventToClient( eCPE_Alarm, eAT_LowBattery );
                        }
                        else
                        {
                            m_alarmStatus[eAT_LowBattery].eState = eAS_AckdAlarm;
                        }
                        break;

                    case eAS_NewAlarm:
                    case eAS_AckdAlarm:
                        // Nothing to do here - will already have notified the user
                        break;
                }
                break;

            case TMPLDevice::eBL_Unknown:
                // Don't process alarms in this state
                break;
        }

        // Look for any ack'd alarms. These are still pending and a notification reposted
        // if the ack timer has expired.
        for( int iAlarm = 0; iAlarm < eAT_NbrAlarmTypes; iAlarm++ )
        {
            if( m_alarmStatus[iAlarm].eState == eAS_AckdAlarm )
            {
                if( Now() > IncMinute( m_alarmStatus[iAlarm].tLastAck, AckMinutesTimeout ) )
                {
                    // Alarm condition is still present. Time to nag the user again
                    m_alarmStatus[iAlarm].eState = eAS_NewAlarm;
                    PostEventToClient( eCPE_Alarm, iAlarm );
                }
            }
        }
    }
}


bool TCommPoller::GetHaveAlarm( void )
{
    for( int iAlarm = 0; iAlarm < eAT_NbrAlarmTypes; iAlarm++ )
    {
        if( m_alarmStatus[iAlarm].eState != eAS_NoAlarm )
            return true;
    }

    // Fall through means no alarm
    return false;
}


bool TCommPoller::GetAlarmActive( int eAlarmTypeEnum )
{
    if( ( eAlarmTypeEnum < 0 ) || ( eAlarmTypeEnum >= eAT_NbrAlarmTypes ) )
        return false;

    return ( m_alarmStatus[eAlarmTypeEnum].eState != eAS_NoAlarm );
}


TDateTime TCommPoller::GetAlarmAckTime( int eAlarmTypeEnum )
{
    if( ( eAlarmTypeEnum < 0 ) || ( eAlarmTypeEnum >= eAT_NbrAlarmTypes ) )
        return 0.0;

    return ( m_alarmStatus[eAlarmTypeEnum].tLastAck );
}


void TCommPoller::AckAlarm( AlarmType eWhichAlarm )
{
    if( ( eWhichAlarm < 0 ) || ( eWhichAlarm >= eAT_NbrAlarmTypes ) )
        return;

    // Note that this alarm has been ack'd
    m_alarmStatus[eWhichAlarm].tLastAck = Now();

    if( m_alarmStatus[eWhichAlarm].eState == eAS_NewAlarm )
        m_alarmStatus[eWhichAlarm].eState = eAS_AckdAlarm;
}

