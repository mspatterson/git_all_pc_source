#ifndef CommsMgrH
#define CommsMgrH

#include "WTTTSDefs.h"
#include "MPLDeviceBaseClass.h"
#include "BaseRadioDevice.h"
#include "MgmtPortDevice.h"


// The TCommPoller class does most of its work in a separate thread. It provides
// events notifications through a PostMessage() call back to a client window.
// The following enums are passed as the WParam member in the message. Depending
// on the WParam, an LParam value may also be provided as noted below.
typedef enum {
    eCPE_Initializing,          // Event posted while comm ports are being opened; gets posted multiple times
    eCPE_InitComplete,          // Ports are open and ready to start mode operating
    eCPE_PortLost,              // Event posted when a port has been lost
    eCPE_CommsLost,             // Event posted when we've lost comms with either the MPL or base radio
    eCPE_ThreadFailed,          // Unhandled exception in poll thread
    eCPE_RFCRecvd,              // RFC received, new status values will be available
    eCPE_PlugDetected,          // A plug-detection event has occurred
    eCPE_CleaningDone,          // Cleaning cycle is complete
    eCPE_ResetFlagDone,         // A reset flag cycle is complete
    eCPE_Alarm,                 // An alarm has occurred.
    eCPE_NbrCommPollerEvents
} CommPollerEvent;


class TCommPoller : public TThread
{
  public:

    typedef enum {
        PS_INITIALIZING,       // Thread is starting
        PS_BR_PORT_SCAN,       // Looking for base radio comm port
        PS_MPL_PORT_SCAN,      // Looking for MPL comm port
        PS_MPL_CHAN_SCAN,      // Looking for MPL radio channel
        PS_RUNNING,            // Ports open and have MPL
        PS_COMMS_FAILED,       // Comm error, see ConnectResult for more info
        PS_EXCEPTION,          // Thread loop failed due to exception being thrown
        NBR_POLLER_ERRORS
    } POLLER_STATE;

    typedef enum {
        eAT_None,              // No alarm
        eAT_LowBattery,        // Low battery alarm
        eAT_NbrAlarmTypes
    } AlarmType;

  private:

    typedef enum {
        eCR_Initializing,
        eCR_BadBaseRadioPort,
        eCR_BadMPLPort,
        eCR_BadMgmtPort,
        eCR_Connected,
        eCR_HuntingForBase,
        eCR_HuntingForMPL,
        eCR_WinsockError,
        eCR_ThreadException,
        eCR_NumConnectResult
    } CONNECT_RESULT;

    static const String ConnResultStrings[eCR_NumConnectResult];

    typedef enum {
        eUnknown,
        eRadio0,
        eRadio1,
        eBase,
        eMgmt
    } PORT_TYPE;

    TMPLDevice*       m_device;
    TBaseRadioDevice* m_radio;
    TMgmtPortDevice*  m_mgmtPort;
    MPL_TYPE          m_mplType;

    POLLER_STATE      m_pollerState;
    UnicodeString     m_connectResult;

    DWORD             m_dwRadioChanWaitTime;

    // Alarm management
    typedef enum {
        eAS_NoAlarm,
        eAS_NewAlarm,
        eAS_AckdAlarm,
        eAS_NbrAlarmStates
    } AlarmState;

    typedef struct {
        AlarmState eState;
        TDateTime  tLastAck;
    } ALARM_STATUS;

    ALARM_STATUS      m_alarmStatus[eAT_NbrAlarmTypes];

    // Event notification window handle
    HWND              m_hwndEvent;
    DWORD             m_dwEventMsg;

    bool IsThreadRunning( void ) { return( m_device != NULL ); }

    void SetPollerState( POLLER_STATE epsNew, CONNECT_RESULT eCRMsgEnum );
    void PostEventToClient( CommPollerEvent eEvent, DWORD dwLParam = 0 );

    bool OpenAssignedPorts( void );
    bool OpenAutomaticPorts( void );

    bool FindDevPorts( const COMMS_CFG& radioCommCfg, COMMS_CFG& deviceCommCfg, COMMS_CFG& mgmtCommCfg );
    PORT_TYPE GetPortType( CCommPort* pPort );
    bool WigglePort( CCommPort* pPort, BYTE& bInitCtrlStatus, BYTE& bHighCtrlStatus, BYTE& bLastCtrlStatus );

    void AfterCommConnect( void );

    void CreateDevices( void );
    void DeleteDevices( void );

    void InitAlarms( void );
    void ProcessAlarms( void );

    void __fastcall OnMPLConnected( TObject* pMPLDev );
    void __fastcall OnMPLChanged( TObject* pMPLDev );
    void __fastcall OnMPLRFCRecvd( TObject* pMPLDev );
    void __fastcall OnPlugDetected( TObject* pMPLDev );
    void __fastcall OnCleaningDone( TObject* pMPLDev );
    void __fastcall OnResetFlagDone( TObject* pMPLDev );

  protected:

    void __fastcall Execute();
        // Worker thread function

    void FlushPendingPktQueue( void );
        // Flushes all pending packets from the queue.

    void AddPktToPendingQueue( const TMPLDevice::MPL_READING& nextReading );
        // Adds a packet received by the port into the pending queue. Will
        // overwrite an old packet's data if the queue is full.

    bool GetPktFromQueue( TMPLDevice::MPL_READING& nextReading );
        // Gets the next reading from the queue. Returns true if a reading
        // was returned.

    // The max number of readings should be such that data won't be lost
    // during the longest possible delay in the calling process
    #define MAX_MPL_READINGS   100
    TMPLDevice::MPL_READING m_pktQueue[MAX_MPL_READINGS];

    int m_pktReadIndex;
    int m_pktWriteIndex;

    // Units of measure
    int      m_currUnits;
    int      m_newUnits;

    // The following flag is set by the main thread if this thread should
    // reload settings from the registry. Note that comms settings are
    // not reloaded by this process
    bool     m_refreshSettings;

    // A critical section controls the access of shared objects between threads
    CRITICAL_SECTION m_criticalSection;

    // Property getter methods
    String   GetMPLTypeText( void );
    String   GetMPLStatusText( void );
    String   GetMPLPortDesc( void );
    bool     GetMPLIsIdle( void );
    bool     GetMPLIsCalibrated( void );
    bool     GetMPLIsRecving( void );
    bool     GetMPLIsConn( void );
    bool     GetCanStartCycle( void );
    bool     GetPlugDetected( void );
    bool     GetCleanDone( void );
    bool     GetUsingMPLSim( void );
    bool     GetMPLPortLost( void );

    TMPLDevice::BatteryLevelType GetMPLBattLevel( void );

    String   GetBaseRadioStatus( void );
    bool     GetBaseRadioPortLost( void );
    bool     GetBaseRadioIsConn( void );
    bool     GetBaseRadioIsRecving( void );

    bool      GetHaveAlarm( void );
    bool      GetAlarmActive ( int eAlarmTypeEnum );
    TDateTime GetAlarmAckTime( int eAlarmTypeEnum );

  public:

    __fastcall TCommPoller( HWND hwndEvent, DWORD dwEventMsg );
    virtual __fastcall ~TCommPoller();
        // Constructor and destructor. Because the comm poller does most of
        // its work in a thread, direct event callbacks cannot be used. So
        // instead messages are posted to hwndEvent window when events
        // occur

    bool GetNextReading( TMPLDevice::MPL_READING& nextReading );

    __property bool          ThreadRunning  = { read = IsThreadRunning };
    __property POLLER_STATE  PollerState    = { read = m_pollerState };
    __property UnicodeString ConnectResult  = { read = m_connectResult };

    void RefreshSettings( int unitsOfMeasure );
        // Informs the poller is must reload registry-based settings

    __property bool          HaveAlarm         = { read = GetHaveAlarm };
    __property bool          AlarmActive[int]  = { read = GetAlarmActive };
    __property TDateTime     AlarmAckTime[int] = { read = GetAlarmAckTime };
        // Alarm properties. The [int] index must be an AlarmType enum

    void AckAlarm( AlarmType eWhichAlarm );
        // Ack'ing an alarm will suppress it for a given amount of time

    //
    // MPL Specific Methods
    //

    __property String        MPLTypeText     = { read = GetMPLTypeText };
    __property MPL_TYPE      MPLType         = { read = m_mplType };
    __property String        MPLPortDesc     = { read = GetMPLPortDesc };
    __property bool          MPLIsConnected  = { read = GetMPLIsConn };
    __property bool          UsingMPLSim     = { read = GetUsingMPLSim   };

    __property String        MPLStatus       = { read = GetMPLStatusText };
    __property bool          MPLIsIdle       = { read = GetMPLIsIdle };
    __property bool          MPLIsCalibrated = { read = GetMPLIsCalibrated };
    __property bool          MPLIsRecving    = { read = GetMPLIsRecving };
    __property bool          MPLPortLost     = { read = GetMPLPortLost };

    __property TMPLDevice::BatteryLevelType MPLBatteryLevel = { read = GetMPLBattLevel };

    __property bool          CanStartCycle   = { read = GetCanStartCycle };
    __property bool          PlugDetected    = { read = GetPlugDetected };
    __property bool          CleaningDone    = { read = GetCleanDone };

    bool LaunchPlug( int iPlug );
        // Instructs the MPL to activate the launch mechanism for the indicated plug.
        // Plug number must range in value from 1 to NBR_MPL_PLUGS. Returns true on
        // success.

    bool CleanPlug( int iPlug );
        // Instructs the MPL to activate the cleaning mechanism for the indicated plug.
        // Plug number must range in value from 1 to NBR_MPL_PLUGS. Returns true on
        // success.

    bool DoResetFlag( void );
        // Instructs the MPL to activate its 'reset flag' solenoid. Returns true
        // on success.

    void ClearPlugDetected( void );
    void ClearSW2Indication( void );

    bool AssertAlertOutput( void );
    void ClearAlertOutput( void );
        // Methods to assert and clear the alert output on the base radio

    bool CommsGetStats( PORT_STATS& portStats );
    void CommsClearStats( void );

    bool GetLastStatusPktInfo( TStringList* pCaptions, TStringList* pValues );
    bool GetLastStatusPkt( TDateTime& pktTime, DWORD& rfcCount, GENERIC_MPL_RFC_PKT& lastPkt );
        // Pass through function that pulls last status packet information
        // out of the underlying device object.

    bool GetLastAvgReading   ( TMPLDevice::MPL_READING&  lastPkt );
    bool GetLastMinMaxReading( TMPLDevice::MIN_MAX_DATA& lastPkt );
        // Pass through function that pulls last packet data out of
        // the underlying device object.

    bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues );
    bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
    bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
        // Pass through functions that get / set calibration data. Not
        // all devices support on-board calibration memory.

    bool ReloadCalDataFromDevice( void );
        // Causes the device to clear it internal cached cal data and
        // reload the data from the device.

    bool WriteCalDataToDevice( void );
        // Writes the calibration data currently cached in the software
        // to the WTTTS device.

    bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo );
        // Returns information about the device hardware

    bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo );
        // Returns current battery information


    //
    // Base Radio Specific Methods
    //

    __property String        BaseRadioStatus       = { read = GetBaseRadioStatus };
    __property bool          BaseRadioIsConnected  = { read = GetBaseRadioIsConn };
    __property bool          BaseRadioIsRecving    = { read = GetBaseRadioIsRecving };
    __property bool          BaseRadioPortLost     = { read = GetBaseRadioPortLost };

    bool GetRadioStats( PORT_STATS& radioStats );
    void ClearRadioStats( void );

    bool GetLastRadioStatusPktInfo( TStringList* pCaptions, TStringList* pValues );
        // Pass through function that pulls last status packet information
        // out of the underlying device object.

    int GetRadioChannel( TBaseRadioDevice::BASE_RADIO_NBR radioNbr );
    TBaseRadioDevice::SET_RADIO_CHAN_RESULT SetRadioChannel( int newChanNbr, TBaseRadioDevice::BASE_RADIO_NBR radioNbr );
        // Gets / sets a radio to operate on a given channel


    //
    // Management Port Specific Methods
    //

    bool MgmtGetStats( PORT_STATS& mgmtStats );
    void ClearMgmtStats( void );
};

#endif
