//************************************************************************
//
//  MPLDeviceBaseClass.h: base class for all MPL-like devices. This is
//       an abstract class. Therefore, descendent classes will need to
//       implemement a number of abstract functions.
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "MPLDeviceBaseClass.h"
#include "MPLRegistryInterface.h"
#include "TypeDefs.h"

#pragma package(smart_init)


//
// Private Declarations
//

// Maps a 1-based plug number to its 1-based cleaning solenoid nbr
static const int CleaningPlugMap[NBR_MPL_PLUGS+1] = { 0, 3, 4 };


const String TMPLDevice::CalFactorCaptions[TMPLDevice::NBR_CALIBRATION_ITEMS] = {
    "Cal Version",
    "Cal Revision",
    "Serial Number",
    "Manufacture Info",
    "Manufacture Date",
    "Calibration Date",
    "Pressure Offset",
    "Pressure Span",
    "Battery Type",
    "Battery Capacity",
};


BYTE TMPLDevice::CurrCalVersion  = 1;
BYTE TMPLDevice::CurrCalRevision = 0;


const String TMPLDevice::RFCFieldCaptions[NBR_RFC_FIELD_TYPES] = {
    "Link State",
    "Last RFC",
    "Battery Volts",
    "Battery Used",
    "Battery Type",
    "Temperature",
    "RPM",
    "Pressure",
    "Last Reset",
    "RF Channel",
    "Current Mode",
    "RFC Rate",
    "RFC Timeout",
    "Pairing Timeout",
    "Solenoid Timeout",
    "Sampling Rate",
    "Sensor Averaging",
    "Solenoid State",
    "Contact States",
    "Switch States",
    "Sensor Boards",
    "Sensor Inputs",
    "Sensor 1 Values",
    "Sensor 2 Values",
    "Sensor 3 Values",
    "Sensor 4 Values",
    "Device ID",
};


//
// Public Constants
//

const String MPLTypeText[MT_NBR_DEVICES] = {
    L"Null Device",
    L"MPL Device",
};


//
// Class Implementation
//

__fastcall TMPLDevice::TMPLDevice( MPL_TYPE mplType )
{
    // Save our type
    m_mplType     = mplType;
    m_mplTypeText = MPLTypeText[mplType];

    // Reset comms
    ClearCommStats();

    m_port.portType       = CT_UNUSED;
    m_port.connResult     = CCommObj::ERR_NONE;
    m_port.connResultText = "(closed)";

    m_portLost = false;

    // Set default averaging params
    m_avgRPM = new TNoAverage();

    ResetDeviceParams();
}


void TMPLDevice::ResetDeviceParams( void )
{
    // Set time param defaults
    m_timeParams.rfcRate         = DEFAULT_RFC_RATE;
    m_timeParams.rfcTimeout      = DEFAULT_RFC_TIMEOUT;
    m_timeParams.solenoidTimeout = DEFAULT_SOLENOID_TIMEOUT;
    m_timeParams.pairingTimeout  = DEFAULT_PAIRING_TIMEOUT;

    // Set the default packet timeout
    m_tPacketTimeout = DEFAULT_PACKET_TIMEOUT;

    // Clear dev ID until we get one
    m_sDevID = "";

    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
        m_bySensorBdAddrs[iBd] = 0;

    // No plug is detected
    m_ePlugDetState = ePDS_NoPlug;

    // Set defaults for these params for now
    m_actSensorAvgParam = 1;
    m_newSensorAvgParam = 1;

    m_actSensorSamplingRate = 1;
    m_newSensorSamplingRate = 1;

    m_activeSolenoid        = 0;
    m_solOnTimeLaunch       = 10000;
    m_solOnTimeClean        = 10000;
    m_solenoidActivatedTime = 0;
    m_solenoidLastCmdTime   = 0;

    m_battLevel = eBL_Unknown;

    // Clear switch settings
    for( int iSw = 0; iSw < NBR_MPL_PIB_SWITCHES; iSw++ )
    {
        m_pibSwitchState[iSw] = eBS_Unknown;
        m_pibSwitchCmd  [iSw] = eBC_NoCmd;
    }

    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
    {
        m_sensSW1State[iBd] = eBS_Unknown;
        m_sensSW2State[iBd] = eBS_Unknown;
        m_sensSW1Cmd  [iBd] = eBC_NoCmd;
        m_sensSW2Cmd  [iBd] = eBC_NoCmd;
    }
}


bool TMPLDevice::GetDevIsRecving( void )
{
    if( time( NULL ) > m_port.stats.tLastPkt + m_tPacketTimeout )
        return false;

    return true;
}


bool TMPLDevice::GetIsConnected( void )
{
    // Legacy sub only uses the data port
    if( m_port.portObj == NULL )
        return false;

    return m_port.portObj->isConnected;
}


void TMPLDevice::RefreshSettings( void )
{
    // Called to refresh any registry-based settings. Causes those settings
    // to be re-read from the registry.
    m_solOnTimeLaunch = GetSolenoidOnTime( SOTT_LAUNCH );
    m_solOnTimeClean  = GetSolenoidOnTime( SOTT_CLEAN );
    m_solOnTimeReset  = GetSolenoidOnTime( SOTT_RESET );
}


bool TMPLDevice::GetPlugDetected( void )
{
    // Return true only if a plug has been detected but not ack'd
    return( m_ePlugDetState == ePDS_PlugDetected );
}


void TMPLDevice::SetPlugDetected( void )
{
    // Protected method that derived classes can use to indicate
    // a plug is present. It is safe to repeatedly call this method
    // while a plug is being detected - it will only fire its callback
    // the first time.
    if( m_ePlugDetState == ePDS_NoPlug )
    {
        m_ePlugDetState = ePDS_PlugDetected;
        DoOnPlugDetected();
    }
}


void TMPLDevice::AckPlugDetected( void )
{
    // Called by the client when it has ack'd the presence of a plug.
    // Will cause PlugDetected to go false, and PlugDetected won't go
    // true again until a new plug is detected
    if( m_ePlugDetState == ePDS_PlugDetected )
        m_ePlugDetState = ePDS_DetectAckd;
}


int TMPLDevice::GetCleaningSolenoidNbr( int iPlugNbr )
{
    if( ( iPlugNbr > 0 ) && ( iPlugNbr <= NBR_MPL_PLUGS ) )
        return CleaningPlugMap[iPlugNbr];
    else
        return 0;
}


bool TMPLDevice::IsCleaningSolenoid( int iSolNbr )
{
    // First check trivial case
    if( iSolNbr <= 0 )
        return false;

    // Search the cleaning solenoid map for the passed solenoid number
    for( int iPlugNbr = 1; iPlugNbr <= NBR_MPL_PLUGS; iPlugNbr++ )
    {
        if( CleaningPlugMap[iPlugNbr] == iSolNbr )
            return true;
    }

    // Fall through means this is not a cleaning plug
    return false;
}


bool TMPLDevice::Connect( const COMMS_CFG& portCfg )
{
    if( portCfg.portType == CT_UNUSED )
    {
        m_port.connResultText = "port settings not specified";
        return false;
    }

    return CreateCommPort( portCfg, m_port );
}


void TMPLDevice::Disconnect( void )
{
    // Base class only disconnects and releases any created objects
    ReleaseCommPort( m_port );

    // Clear our data structs
    ResetDeviceParams();
}


void TMPLDevice::GetCommStats( PORT_STATS& commStats )
{
    commStats = m_port.stats;
}


void TMPLDevice::ClearCommStats( void )
{
    // Only the count members of the stats gets updated
    ClearPortStats( m_port.stats );
}


WORD TMPLDevice::GetTimeParam( WTTTS_SET_RATE_TYPE aType )
{
    switch( aType )
    {
        case SRT_RFC_RATE:          return m_timeParams.rfcRate;
        case SRT_RFC_TIMEOUT:       return m_timeParams.rfcTimeout;
        case SRT_PAIR_TIMEOUT:      return m_timeParams.pairingTimeout;
        case SRT_SOLENOID_TIMEOUT:  return m_timeParams.solenoidTimeout;
    }

    // Fall through means unknown type passed
    return 0;
}


void TMPLDevice::SetTimeParam( WTTTS_SET_RATE_TYPE aType, WORD newValue )
{
    switch( aType )
    {
        case SRT_RFC_RATE:          m_timeParams.rfcRate         = newValue;  break;
        case SRT_RFC_TIMEOUT:       m_timeParams.rfcTimeout      = newValue;  break;
        case SRT_PAIR_TIMEOUT:      m_timeParams.pairingTimeout  = newValue;  break;
        case SRT_SOLENOID_TIMEOUT:  m_timeParams.solenoidTimeout = newValue;  break;
    }
}


void TMPLDevice::SetRPMAveraging( const AVERAGING_PARAMS& avgParams )
{
    if( m_avgRPM != NULL )
    {
        delete m_avgRPM;
        m_avgRPM = NULL;
    }

    switch( avgParams.avgMethod )
    {
        case AM_BOXCAR:      m_avgRPM = new TMovingAverage( (int)( avgParams.param + 0.5 ), avgParams.absVariance );  break;
        case AM_EXPONENTIAL: m_avgRPM = new TExpAverage( avgParams.param, avgParams.absVariance );                    break;
        default:             m_avgRPM = new TNoAverage();  break;
    }
}


void TMPLDevice::SetSensorParams( int iAvgParam, int iSampleRate )
{
    // Record the new values for now. The Update() method will check if a change
    // in the device values is required and will do that work. Only save values
    // if they appear valid
    switch( iAvgParam )
    {
        case 1:
        case 2:
        case 4:
        case 8:
        case 16:
        case 32:
        case 64:
        case 128:
            m_newSensorAvgParam = iAvgParam;
            break;
    }

    if( ( iSampleRate >= 0 ) && ( iSampleRate <= 8 ) )
       m_newSensorSamplingRate = iSampleRate;
}


bool TMPLDevice::InitPropertyList( TStringList* pCaptions, TStringList* pValues, const String captionList[], int nbrCaptions )
{
    // Helper function that initializes the passed string lists used
    // to populate property editors.
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( !IsConnected )
        return false;

    // Add all of the captions first, with blank
    for( int iItem = 0; iItem < nbrCaptions; iItem++ )
    {
        pCaptions->Add( captionList[iItem] );
        pValues->Add( "" );
    }

    return true;
}


void TMPLDevice::ClearGenericRFCPkt( GENERIC_MPL_RFC_PKT& rfcPkt )
{
    rfcPkt.battType           = BT_UNKNOWN;
    rfcPkt.fBattVoltage       = 0.0;
    rfcPkt.battUsed           = 0;
    rfcPkt.rfChannel          = 0;
    rfcPkt.lastReset          = WDRT_POWER_UP;
    rfcPkt.currMode           = WDMT_LOW_POWER;
    rfcPkt.fTemperature       = 0.0;
    rfcPkt.fPressure          = 0.0;
    rfcPkt.fRPM               = 0.0;
    rfcPkt.activeSolenoid     = 0;
    rfcPkt.rfcRate            = 0;
    rfcPkt.rfcTimeout         = 0;
    rfcPkt.pairingTimeout     = 0;
    rfcPkt.solenoidTimeout    = 0;
    rfcPkt.sensorSamplingRate = 0;
    rfcPkt.sensorAvgFactor    = 1;
    rfcPkt.deviceID           = "";

    for( int iContact = 0; iContact < NBR_MPL_CONTACTS; iContact++ )
        rfcPkt.pibContacts[iContact] = eBS_Unknown;

    for( int iSwitch = 0; iSwitch < NBR_MPL_PIB_SWITCHES; iSwitch++ )
         rfcPkt.pibSwitches[iSwitch] = eBS_Unknown;

    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
    {
        rfcPkt.activeSensors[iBd] = 0;
        rfcPkt.sensSW1[iBd]       = eBS_Unknown;
        rfcPkt.sensSW2[iBd]       = eBS_Unknown;
        rfcPkt.sensXAvg[iBd]      = 0.0;
        rfcPkt.sensYAvg[iBd]      = 0.0;
        rfcPkt.sensZAvg[iBd]      = 0.0;
    }
}


String TMPLDevice::GetSensorSamplingRateText( BYTE byEnumVal )
{
    switch( byEnumVal )
    {
        case 0:  return "0 - standby mode";
        case 1:  return "1 - 800 Hz (1.25 ms)";
        case 2:  return "2 - 400 Hz (2.5 ms)";
        case 3:  return "3 - 200 Hz (5 ms)";
        case 4:  return "4 - 100 Hz (10 ms)";
        case 5:  return "5 - 50 Hz (20 ms)";
        case 6:  return "6 - 12.5 Hz (80 ms)";
        case 7:  return "7 - 6.25 Hz (160 ms)";
        case 8:  return "8 - 1.5625 Hz (640 ms)";
    }

    // Fall through means unknown enum value
    return "Unknown enum " + IntToStr( byEnumVal );
}


float TMPLDevice::ConvertRawBattVolts( WORD wVBatt )
{
    // Voltage in mV (eg 3.742V would be 3742)
    XFER_BUFFER xferBuff;

    xferBuff.wData[0] = wVBatt;
    xferBuff.wData[1] = 0;

    return (float)xferBuff.iData / 1000.0;
}


float TMPLDevice::ConvertRawTemp( BYTE byRawTemp )
{
    // Byte is a signed value, 0.5C per count with, -64C to +63.5C
    XFER_BUFFER xferBuff = { 0 };

    xferBuff.byData[0] = byRawTemp;

    // Manually sign extend
    if( byRawTemp & 0x80 )
    {
        xferBuff.byData[1] = 0xFF;
        xferBuff.byData[2] = 0xFF;
        xferBuff.byData[3] = 0xFF;
    }

    return (float)( xferBuff.iData * 5 ) / 10.0;
}


float TMPLDevice::ConvertRawRPM( BYTE byRawRPM )
{
    // For now, just report the value as a float
    XFER_BUFFER xferBuff = { 0 };

    xferBuff.byData[0] = byRawRPM;

    return (float)xferBuff.iData;
}


float TMPLDevice::ConvertRawReading( const BYTE sensVal[BI_BYTE_VAL_LEN] )
{
    XFER_BUFFER xferBuff = { 0 };

    xferBuff.byData[0] = sensVal[0];
    xferBuff.byData[1] = sensVal[1];

    // Manually sign extend
    if( sensVal[1] & 0x80 )
    {
        xferBuff.byData[2] = 0xFF;
        xferBuff.byData[3] = 0xFF;
    }

    return (float)xferBuff.iData;
}


void TMPLDevice::DoOnDeviceConnected( void )
{
    if( m_onDevConnected )
        m_onDevConnected( this );
}


void TMPLDevice::DoOnDeviceChanged( void )
{
    if( m_onDevChanged )
        m_onDevChanged( this );
}


void TMPLDevice::DoOnRFCRecvd( void )
{
    if( m_onRFCRecvd )
        m_onRFCRecvd( this );
}


void TMPLDevice::DoOnPlugDetected( void )
{
    if( m_onPlugDetected )
        m_onPlugDetected( this );
}


void TMPLDevice::DoOnCleaningDone( void )
{
    if( m_onCleaningDone )
        m_onCleaningDone( this );
}


void TMPLDevice::DoOnResetFlagDone( void )
{
    if( m_onResetFlagDone )
        m_onResetFlagDone( this );
}

