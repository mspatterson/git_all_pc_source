//************************************************************************
//
//  MPLDeviceBaseClass.h: base class for all MPL devices. This is
//       an abstract class. Therefore, descendent classes will need to
//       implemement a number of the abstract functions declared below.
//
//************************************************************************

#ifndef MPLDeviceBaseClassH
#define MPLDeviceBaseClassH

    #include "CommObj.h"
    #include "WTTTSDefs.h"
    #include "WTTTSProtocolUtils.h"
    #include "SerialPortUtils.h"
    #include "Averaging.h"
    #include "TescoShared.h"

    typedef enum {
        MT_NULL,
        MT_MPL,
        MT_NBR_DEVICES
    } MPL_TYPE;

    extern const String MPLTypeText[];

    #define NBR_MPL_PLUGS           2
    #define NBR_MPL_CONTACTS        4
    #define NBR_MPL_PIB_SWITCHES    2

    #define MPL_RESET_FLAG_SOLENOID 5

    typedef struct {
        BATTERY_TYPE battType;                      // 1 = lithium, 2 = NiMH
        float        fBattVoltage;                  // Voltage in mV (eg 3.742V would be 3742)
        WORD         battUsed;                      // Value returns number of mA/h used since last time the battery was inserted
        BYTE         rfChannel;                     // 11 through 26
        WIRELESS_DEV_RESET_TYPE lastReset;
        WIRELESS_DEV_MODE_TYPE  currMode;
        float        fTemperature;                  // signed value, 0.5C per count with, -64C to +63.5C
        float        fPressure;                     // Signed 24-bit value
        float        fRPM;                          // current RPM
        BoolState    pibContacts[NBR_MPL_CONTACTS];
        BoolState    pibSwitches[NBR_MPL_PIB_SWITCHES];
        BoolState    sensSW1[MAX_NBR_MPL_SENSOR_BOARDS];
        BoolState    sensSW2[MAX_NBR_MPL_SENSOR_BOARDS];
        int          activeSolenoid;                // Values 1 through 6, 0 if no solenoid is active
        WORD         rfcRate;
        WORD         rfcTimeout;
        WORD         pairingTimeout;
        WORD         solenoidTimeout;
        BYTE         sensorSamplingRate;            // Enum value: 0 through 8, refer to spec
        int          sensorAvgFactor;               // Allowed values: 1, 2, 4, 8, 16, 32, 64, or 128
        String       deviceID;
        BYTE         activeSensors[MAX_NBR_MPL_SENSOR_BOARDS];  // Sensors being read and reported in this packet
        float        sensXAvg[MAX_NBR_MPL_SENSOR_BOARDS];
        float        sensYAvg[MAX_NBR_MPL_SENSOR_BOARDS];
        float        sensZAvg[MAX_NBR_MPL_SENSOR_BOARDS];
    } GENERIC_MPL_RFC_PKT;


    class TMPLDevice : public TObject {

      public:

        typedef enum {
            eBL_Unknown,
            eBL_Low,
            eBL_Med,
            eBL_OK,
            eNbrBattLevels
        } BatteryLevelType;

      private:

        MPL_TYPE      m_mplType;
        String        m_mplTypeText;

        TNotifyEvent  m_onDevConnected;
        TNotifyEvent  m_onDevChanged;
        TNotifyEvent  m_onRFCRecvd;
        TNotifyEvent  m_onPlugDetected;
        TNotifyEvent  m_onCleaningDone;
        TNotifyEvent  m_onResetFlagDone;

      protected:

        DEVICE_PORT   m_port;

        bool          m_portLost;

        time_t        m_tPacketTimeout;

        // Averaging parameters - we do RPM averaging ourself, but the device
        // does averaging on the sensor readings
        TAverage*     m_avgRPM;

        // Device parameters
        String        m_sDevID;
        BYTE          m_bySensorBdAddrs[MAX_NBR_MPL_SENSOR_BOARDS];

        int           m_actSensorAvgParam;       // Act rate is what the device has told us it is at
        int           m_newSensorAvgParam;       // New rate is what the client wants to be at

        int           m_actSensorSamplingRate;
        int           m_newSensorSamplingRate;

        int           m_activeSolenoid;          // The solenoid we currently want activated, 0 = no solenoid
        DWORD         m_solOnTimeLaunch;         // How long the solenoid is to be activate for a launch
        DWORD         m_solOnTimeClean;          // How long the solenoid is to be activate for a clean
        DWORD         m_solOnTimeReset;          // How long the solenoid is to be activate when issuing a reset flag
        DWORD         m_solenoidActivatedTime;   // When the solenoid was first activated
        DWORD         m_solenoidLastCmdTime;     // Tick count when last solenoid command was sent

        BoolState     m_pibSwitchState[NBR_MPL_PIB_SWITCHES];     // Current state of switches
        BoolState     m_sensSW1State[MAX_NBR_MPL_SENSOR_BOARDS];
        BoolState     m_sensSW2State[MAX_NBR_MPL_SENSOR_BOARDS];

        BoolCmd       m_pibSwitchCmd[NBR_MPL_PIB_SWITCHES];       // Desired state of switches
        BoolCmd       m_sensSW1Cmd[MAX_NBR_MPL_SENSOR_BOARDS];
        BoolCmd       m_sensSW2Cmd[MAX_NBR_MPL_SENSOR_BOARDS];

        BatteryLevelType m_battLevel;

        virtual BatteryLevelType GetBattLevel( void ) { return m_battLevel; }

        typedef enum
        {
           ePDS_NoPlug,          // No plug present
           ePDS_PlugDetected,    // Plug detected, not yet ack'd by client
           ePDS_DetectAckd       // Client has ack'd that a plug is present
        } PlugDetectState;

        PlugDetectState m_ePlugDetState;

                __fastcall  TMPLDevice( MPL_TYPE mplType );
                            // Can only construct descendant classes

                void          ResetDeviceParams( void );

        virtual String        GetDevStatus( void ) = 0;
        virtual bool          GetDevIsIdle( void ) = 0;
        virtual bool          GetDevIsRecving( void );
        virtual bool          GetDevIsCalibrated( void ) = 0;
        virtual bool          GetIsConnected( void );

        virtual String        GetPortDesc( void )  { return m_port.stats.portDesc; }

                bool          InitPropertyList( TStringList* pCaptions, TStringList* pValues, const String captionList[], int nbrCaptions );

        // The following structure stores the timing values for the device.
        // Note that this structure is designed with the WTTTS device in mind.
        // The legacy device class can interpret these values best it can.
        typedef struct {
            WORD rfcRate;                // Sets RFC tx rate, in msecs
            WORD rfcTimeout;             // Sets how long the device receiver remains active after sending a RFC
            WORD pairingTimeout;         // Sets how long before the device enters 'deep sleep' if it can't connect
            WORD solenoidTimeout;        // Sets how long the device will keep a solenoid active before deactivating it
        } TIME_PARAMS;

        TIME_PARAMS m_timeParams;

        WORD GetTimeParam( WTTTS_SET_RATE_TYPE aType );
        void SetTimeParam( WTTTS_SET_RATE_TYPE aType, WORD newValue );

        // Calibration support
        typedef struct {
            int   pressure_Offset;
            float pressure_Span;
            WORD  battCapacity;
            BYTE  battType;
        } MPL_CAL_DATA_STRUCT_V1;

        static BYTE CurrCalVersion;
        static BYTE CurrCalRevision;

        // RFC support
        typedef enum {
            RFT_DEV_STATE,
            RFT_LAST_RFC_TIME,
            RFT_BATT_VOLTS,
            RFT_BATT_USED,
            RFT_BATT_TYPE,
            RFT_TEMPERATURE,
            RFT_RPM,
            RFT_PRESSURE,
            RFT_LAST_RESET,
            RFT_RF_CHAN,
            RFT_CURR_MODE,
            RFT_RFC_RATE,
            RFT_RFC_TIMEOUT,
            RFT_PAIR_TIMEOUT,
            RFT_SOLENOID_TIMEOUT,
            RFT_SENSOR_RATE,
            RFT_SENSOR_AVGING,
            RFT_SOLENOID_STATE,
            RFT_CONTACTS_STATE,
            RFT_SWITCHES_STATE,
            RFT_SENSOR_BOARDS,
            RFT_SENSOR_INPUTS,
            RFT_SENSOR1_VALS,
            RFT_SENSOR2_VALS,
            RFT_SENSOR3_VALS,
            RFT_SENSOR4_VALS,
            RFT_DEV_ID,
            NBR_RFC_FIELD_TYPES
        } RFC_FIELD_TYPE;

        static const String RFCFieldCaptions[];

        void ClearGenericRFCPkt( GENERIC_MPL_RFC_PKT& rfcPkt );

        String GetSensorSamplingRateText( BYTE byEnumVal );

        // Plug detection
        bool GetPlugDetected( void );
        void SetPlugDetected( void );

        // Launch / clean / reset support
        virtual bool GetCanStartCycle( void ) { return false; }
        virtual bool GetCleaningDone( void )  { return true;  }

        int  GetCleaningSolenoidNbr( int iPlugNbr );
        bool IsCleaningSolenoid( int iSolNbr );

        // Conversion support
        static float ConvertRawBattVolts( WORD wVBatt );
        static float ConvertRawTemp( BYTE byRawTemp );
        static float ConvertRawRPM( BYTE byRawRPM );
        static float ConvertRawReading( const BYTE sensVal[BI_BYTE_VAL_LEN] );

        // Notification support
        void DoOnDeviceConnected( void );
        void DoOnDeviceChanged( void );
        void DoOnRFCRecvd( void );
        void DoOnPlugDetected( void );
        void DoOnCleaningDone( void );
        void DoOnResetFlagDone( void );

      public:

        virtual __fastcall ~TMPLDevice() { Disconnect(); }

        __property MPL_TYPE      DeviceType         = { read = m_mplType };
        __property String        DeviceTypeText     = { read = m_mplTypeText };

        __property String        DeviceStatus       = { read = GetDevStatus };
        __property bool          DeviceIsIdle       = { read = GetDevIsIdle };
        __property bool          DeviceIsCalibrated = { read = GetDevIsCalibrated };
        __property bool          DeviceIsRecving    = { read = GetDevIsRecving };
        __property String        DeviceID           = { read = m_sDevID };

        __property BatteryLevelType BatteryLevel    = { read = GetBattLevel };

        virtual bool Connect( const COMMS_CFG& portCfg );
        virtual void Disconnect( void );

        __property bool          IsConnected   = { read = GetIsConnected };
        __property String        ConnectResult = { read = m_port.connResultText };
        __property bool          PortLost      = { read = m_portLost };
        __property String        PortDesc      = { read = GetPortDesc };

        __property WORD          ParamRate[WTTTS_SET_RATE_TYPE] = { read = GetTimeParam, write = SetTimeParam };

        __property bool          CanStartCycle  = { read = GetCanStartCycle };
        __property bool          PlugDetected   = { read = GetPlugDetected };
        __property bool          CleaningDone   = { read = GetCleaningDone };

        __property DWORD         SolenoidOnTimeLaunch = { read = m_solOnTimeLaunch, write = m_solOnTimeLaunch };
        __property DWORD         SolenoidOnTimeClean  = { read = m_solOnTimeClean,  write = m_solOnTimeClean };
        __property DWORD         SolenoidOnTimeReset  = { read = m_solOnTimeReset,  write = m_solOnTimeReset};

        __property TNotifyEvent  OnDeviceConnected = { read = m_onDevConnected,  write = m_onDevConnected };
        __property TNotifyEvent  OnDeviceChanged   = { read = m_onDevChanged,    write = m_onDevChanged };
        __property TNotifyEvent  OnRFCRecvd        = { read = m_onRFCRecvd,      write = m_onRFCRecvd };
        __property TNotifyEvent  OnPlugDetected    = { read = m_onPlugDetected,  write = m_onPlugDetected };
        __property TNotifyEvent  OnCleaningDone    = { read = m_onCleaningDone,  write = m_onCleaningDone };
        __property TNotifyEvent  OnResetFlagDone   = { read = m_onResetFlagDone, write = m_onResetFlagDone };

        typedef struct {
            BYTE      bySensorID;
            BoolState bsSW1;
            BoolState bsSW2;
            float     fX;
            float     fY;
            float     fZ;
        } SENSOR_READING;

        typedef struct {
            TDateTime tTime;
            BoolState bsContacts[NBR_MPL_CONTACTS];
            BoolState bsPIBSwitches[NBR_MPL_PIB_SWITCHES];
            int       iActiveSolenoid;  // 1 - 6, 0 if non active
            SENSOR_READING avgReadings[MAX_NBR_MPL_SENSOR_BOARDS];
        } MPL_READING;

        typedef struct {
            BYTE  bySensorID;
            float fMinX;
            float fMaxX;
            float fMinY;
            float fMaxY;
            float fMinZ;
            float fMaxZ;
        } MIN_MAX_REC;

        typedef struct {
            TDateTime tTime;
            MIN_MAX_REC minMaxRecs[MAX_NBR_MPL_SENSOR_BOARDS];
        } MIN_MAX_DATA;

        virtual bool GetLastAvgReading   ( MPL_READING&  lastPkt ) = 0;
        virtual bool GetLastMinMaxReading( MIN_MAX_DATA& lastPkt ) = 0;
            // Returns the last readings

        void SetRPMAveraging( const AVERAGING_PARAMS& avgParams );
        void SetSensorParams( int iAvgParam, int iSampleRate );
            // Updates the passed averaging type to use the new averaging method.
            // We do RPM averaging, so we can support different averaging types.
            // The sensor board averaging type is fixed, so we just give it a param

        virtual bool Update( void ) = 0;
            // Descendant classes must implement this function. It will get called
            // regularly to allow the class to receive and process information.

        virtual void RefreshSettings( void );
            // Called to refresh any registry-based settings. Causes those settings
            // to be re-read from the registry.

        virtual bool LaunchPlug( int iPlug ) = 0;
            // Instructs the MPL to activate the launch mechanism for the indicated plug.
            // Plug number must range in value from 1 to NBR_MPL_PLUGS

        virtual void AckPlugDetected( void );
            // Called by the client when it has ack'd the presence of a plug.
            // Will cause PlugDetected to go false, and PlugDetected won't go
            // true again until a new plug is detected

        virtual bool CleanPlug( int iPlug ) = 0;
            // Instructs the MPL to activate the cleaning mechanism for the indicated plug.
            // Plug number must range in value from 1 to NBR_MPL_PLUGS

        virtual bool ActivateResetFlag( void ) = 0;
            // Activates the reset flag solenoid

        virtual void GetCommStats( PORT_STATS& commStats );
        virtual void ClearCommStats( void );

        // RFC support
        virtual bool ShowLastRFCPkt( TStringList* pCaptions, TStringList* pValues ) = 0;
        virtual bool GetLastRFCPkt ( TDateTime& tPktTime, DWORD& rfcCount, GENERIC_MPL_RFC_PKT& rfcPkt ) = 0;

        virtual bool ClearPIBSwitchOn   ( int iPIBSwitch ) = 0;
        virtual bool ClearSensorSwitchOn( BYTE bySensorAddr, int iSwitch ) = 0;

        // Calibration constants and methods
        typedef enum {
            CI_VERSION,
            CI_REVISION,
            CI_SERIAL_NBR,
            CI_MFG_INFO,
            CI_MFG_DATE,
            CI_CALIBRATION_DATE,
            CI_PRESSURE_OFFSET,
            CI_PRESSURE_SPAN,
            CI_BATTERY_TYPE,
            CI_BATTERY_CAPACITY,
            NBR_CALIBRATION_ITEMS
        } CALIBRATION_ITEM;

        static const String CalFactorCaptions[];

        virtual bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues ) = 0;
        virtual bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues ) = 0;
        virtual bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues ) = 0;
            // Calibration support routines. The get 'raw' function returns data as
            // binary bytes. The 'formatted' get/set functions provide access to
            // data as named values.

        virtual bool ReloadCalDataFromDevice( void ) = 0;
            // Causes the device to clear it internal cached cal data and
            // reload the data from the device.

        virtual bool WriteCalDataToDevice( void ) = 0;
            // Writes the calibration data currently cached in the software
            // to the WTTTS device.

        virtual bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo ) = 0;
            // All devices must provide a way to report back the above hw info

        virtual bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo ) = 0;
            // All devices must report back battery information

    };

#endif

