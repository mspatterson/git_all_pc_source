#ifndef MPLDeviceNoneH
#define MPLDeviceNoneH

    //
    // Class Declaration for 'null' MPL device
    //

    #include "MPLDeviceBaseClass.h"

    class TNullMPLDevice: public TMPLDevice {

      protected:

        virtual String GetDevStatus( void );
        virtual bool   GetDevIsIdle( void );
        virtual bool   GetDevIsCalibrated( void );

      public:

        __fastcall  TNullMPLDevice( void );
        __fastcall ~TNullMPLDevice();

        bool Connect( const COMMS_CFG& portCfg );

        bool Update( void );

        virtual bool GetLastAvgReading   ( MPL_READING&  lastPkt );
        virtual bool GetLastMinMaxReading( MIN_MAX_DATA& lastPkt );
            // Returns the last readings

        virtual bool ShowLastRFCPkt( TStringList* pCaptions, TStringList* pValues );
        virtual bool GetLastRFCPkt ( TDateTime& tPktTime, DWORD& rfcCount, GENERIC_MPL_RFC_PKT& rfcPkt );

        virtual bool LaunchPlug( int iPlug );
        virtual bool CleanPlug( int iPlug );
        virtual bool ActivateResetFlag( void );

        virtual bool ClearPIBSwitchOn   ( int iPIBSwitch );
        virtual bool ClearSensorSwitchOn( BYTE bySensorAddr, int iSwitch );

        bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues );
        bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
        bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );

        bool ReloadCalDataFromDevice( void );
        bool WriteCalDataToDevice( void );

        bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo );

        bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo );

      private:

    };

#endif
