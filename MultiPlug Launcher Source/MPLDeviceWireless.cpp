//
// Device class for MPL module
//
#include <vcl.h>
#pragma hdrstop

#include "MPLDeviceWireless.h"
#include "CUDPPort.h"
#include "ApplUtils.h"
#include "MPLRegistryInterface.h"

#pragma package(smart_init)


//
// Private Functions
//

static const WORD  StartupRFCInterval  =  100;   // When initializing link, start with fast RFC
static const WORD  StartupRFCTimeout   =  250;   // rate with a long response timeout.

static const DWORD ClearSwCmdInterval  =  500;   // We send 'clear switch' commands no faster than at this rate
static const DWORD SetSolenoidInterval = 1000;   // While a solenoid is active, we send the command every this many msecs

static const DWORD RxBuffLen = 2048;

static const DWORD MSecsPerPktTick = 10;   // Each count in the pkt header equals this many msecs


static float DoSpanOffsetCal( int rawValue, int offset, float span )
{
    float fResult = 0.0;

    try
    {
        fResult = (float)(rawValue - offset ) * span;
    }
    catch( ... )
    {
    }

    return fResult;
}


//
// Configuration Data Definitions
//

const TMPLDevice::MPL_CAL_DATA_STRUCT_V1 TWirelessMPLDevice::defaultMPLCalData = {
    /* pressure_Offset   */    0,
    /* pressure_Span     */  1.0,
    /* battCapacity      */    0,   // 0 mAh
    /* battType          */    0,   // unknown type
};


//
// Class Implementation
//

__fastcall TWirelessMPLDevice::TWirelessMPLDevice( void ) : TMPLDevice( MT_MPL )
{
    // Init state vars
    m_linkState = LS_CLOSED;

    // Init rx control vars
    m_rxBuffer    = new BYTE[RxBuffLen];
    m_rxBuffCount = 0;

    // UDP control vars
    m_bUsingUDP   = false;    // True if we're using UDP comms
    m_sMPLDevAddr = "";       // If using UDP, this is the IP addr from which the last event was received
    m_wMPLDevPort = 0;        // If using UDP, this is the port from which the last event was received

    // Reset last RFC control struct
    ResetRFCControlStruct();

    // Reset the last reading struct
    m_lastReading.haveMPLReading = false;

    // Reset flag indicating we have to post a 'done' event
    m_bCleanDoneEventPending = false;
    m_bResetFlagEventPending = false;

    // We don't create the plug hysteresis object until we know the current
    // state of the plug reading
    m_plugState = NULL;

    // Init battery averaging - averaging is hard-coded for now
    m_batteryInfo.fLastBattVolts = 0.0;
    m_batteryInfo.iLastBattUsed  = 0;
    m_batteryInfo.fValidThresh   = 0.0;   // Will be updated by RefreshSettings() call below
    m_batteryInfo.fMinOperThresh = 0.0;   // Will be updated by RefreshSettings() call below
    m_batteryInfo.fGoodThresh    = 0.0;   // Will be updated by RefreshSettings() call below
    m_batteryInfo.avgBattVolts   = new TExpAverage( 100, 0 );
    m_batteryInfo.avgBattUsed    = new TExpAverage( 100, 0 );

    // Init command cache
    for( int iCachedItem = 0; iCachedItem < NBR_MPL_CMD_TYPES; iCachedItem++ )
        InitMPLCommandPkt( (MPL_CMD_TYPE)iCachedItem );

    // Clear other device info
    ClearConfigurationData();
    ClearHardwareInfo();

    // Read registry-based data
    RefreshSettings();
}


__fastcall TWirelessMPLDevice::~TWirelessMPLDevice()
{
    Disconnect();

    if( m_rxBuffer != NULL )
        delete [] m_rxBuffer;

    // Free battery averages
    delete m_batteryInfo.avgBattVolts;
    delete m_batteryInfo.avgBattUsed;
}


bool TWirelessMPLDevice::Connect( const COMMS_CFG& portCfg )
{
    // Reset our state first
    m_linkState = LS_CLOSED;

    // Let base class do its work
    if( !TMPLDevice::Connect( portCfg ) )
        return false;

    // Note if we're using UDP
    if( portCfg.portType == CT_UDP )
        m_bUsingUDP = true;

    // Once the physical connection is established, we go into a hunt
    // mode. In this mode, we are looking for any type of response
    // from the sub. This tells us we have the correct physical link.
    m_linkState = LS_HUNTING;

    ClearConfigurationData();
    ClearHardwareInfo();
    ResetRFCControlStruct();

    // Set the initial timing parameters so that the configuration download
    // happens quickly.
    m_timeParams.rfcRate    = StartupRFCInterval;
    m_timeParams.rfcTimeout = StartupRFCTimeout;

    // Reset the last time that a 'clear sw' command was sent
    m_dwLastClearSwitchCmdTime = 0;

    DoOnDeviceConnected();

    return true;
}


bool TWirelessMPLDevice::Update( void )
{
    // Update our state machine here. First, if we have no port, there is
    // nothing to do
    if( m_linkState == LS_CLOSED )
        return false;

    // The nature of the protocol is such that we need to process any requests
    // or events from the device first.
    DWORD bytesRxd;

    if( m_bUsingUDP )
        bytesRxd = ((CUDPPort*)m_port.portObj)->RecvFrom( &(m_rxBuffer[m_rxBuffCount]), RxBuffLen - m_rxBuffCount, m_sMPLDevAddr, m_wMPLDevPort );
    else
        bytesRxd = m_port.portObj->CommRecv( &(m_rxBuffer[m_rxBuffCount]), RxBuffLen - m_rxBuffCount );

    if( bytesRxd > 0 )
    {
        m_lastRxByteTime = GetTickCount();

        IncStat( m_port.stats.bytesRecv, bytesRxd );

        m_rxBuffCount += bytesRxd;
    }
    else
    {
        // No data received - check if any bytes in the buffer have gone stale
        if( HaveTimeout( m_lastRxByteTime, 250 ) )
            m_rxBuffCount = 0;
    }

    // After checking for received data, confirm the port is still open
    if( m_port.portObj->portLost )
    {
        // This is not good!
        m_portLost = true;
        Disconnect();

        m_linkState = LS_CLOSED;

        m_avgRPM->Reset();

        return false;
    }

    // Have the parser check for any responses from the unit.
    bool bRFCPending = false;

    WTTTS_PKT_HDR   pktHdr;
    MPL_DATA_UNION  pktData;

    if( HaveMPLRespPkt( m_rxBuffer, m_rxBuffCount, pktHdr, pktData ) )
    {
        IncStat( m_port.stats.respRecv );

        m_port.stats.tLastPkt = time( NULL );
        m_lastRxPktTime       = GetTickCount();

        // Process the received packet. Firstly, if we are in the hunting
        // state, receiving a packet means we are on the right channel.
        // The next thing we have to do is download the cal table.
        if( m_linkState == LS_HUNTING )
        {
            m_linkState = LS_CFG_DOWNLOAD;

            m_currCfgReqCycle = 1;
        }

        if( pktHdr.pktType == WTTTS_RESP_VER_PKT )
        {
            // Save the version info
            m_cfgStraps =  pktData.verPkt.hardwareSettings;

            memcpy( m_firmwareVer, pktData.verPkt.fwVer, WTTTS_FW_VER_LEN );
        }
        else if( pktHdr.pktType == WTTTS_RESP_CFG_DATA )
        {
            // This is valid only in the upload or download cfg states.
            // If downloading cfg data, save the data and mark the page
            // as such. If uploading, this is an ack to a previous
            // 'set cfg' command - verify the response.
            if( m_linkState == LS_CFG_DOWNLOAD )
            {
                if( pktData.cfgData.pageNbr < NBR_WTTTS_CFG_PAGES )
                {
                    m_cfgPgStatus[pktData.cfgData.pageNbr].havePage = true;

                    int pageOffset = pktData.cfgData.pageNbr * NBR_BYTES_PER_WTTTS_PAGE;

                    memcpy( &( m_rawCfgData[pageOffset] ), pktData.cfgData.pageData, NBR_BYTES_PER_WTTTS_PAGE );
                }
            }
            else if( m_linkState == LS_CFG_UPLOAD )
            {
                // The WTTTS will have echoed back in response to a previous
                // 'set' packet command. If the values echoed back match
                // the values in the table, then that item has been
                // successfully uploaded. First, check the response code.
                if( pktData.cfgData.result != WTTTS_PG_RESULT_SUCCESS )
                {
                    // WTTTS is indicating we cannot download this page.
                    // Clear the download pending bit and move on.
                    m_cfgPgStatus[pktData.cfgData.pageNbr].setPending = false;
                }
                else if( pktData.cfgData.pageNbr < NBR_WTTTS_CFG_PAGES )
                {
                    // WTTTS liked the page number. If our cached data matches
                    // the data echoed back, this page has been updated.
                    int   pageOffset = pktData.cfgData.pageNbr * NBR_BYTES_PER_WTTTS_PAGE;
                    BYTE* pgPointer  = &( m_rawCfgData[pageOffset] );

                    if( memcmp( pgPointer, pktData.cfgData.pageData, NBR_BYTES_PER_WTTTS_PAGE ) == 0 )
                        m_cfgPgStatus[pktData.cfgData.pageNbr].setPending = false;
                }
            }
        }
        else if( pktHdr.pktType == WTTTS_RESP_MPL_MIN_MAX_VALS )
        {
            // TODO
        }
        else if( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_MPL )
        {
            // The MPL has requested a command from us. If we have
            // none pending, send a 'no command' response back.
            m_lastRFC.dwRFCTick = GetTickCount();
            m_lastRFC.tRFCTime  = Now();;

            // Before saving the RFC packet, check if we have a new device attached.
            // If so, we need to download its cal data (and will need to advise
            // clients that the device changed)
            String sIDInRFC = DeviceIDToStr( pktData.rfcPkt.deviceID );

            if( sIDInRFC.CompareIC( m_sDevID ) != 0 )
            {
                // If we had a device ID and now its changed, then let
                // we need to let the client know
                bool bHadPrevDev = ( m_sDevID.Length() > 0 );

                // Reload config data. This will only succeed if we are in an idle
                // state. But that should be the case if we're receiving RFC's
                ReloadCalDataFromDevice();
                ClearBatteryVolts();

                m_sDevID = sIDInRFC;

                if( bHadPrevDev )
                    DoOnDeviceChanged();
            }
            else
            {
                CreateGenericRFC( m_lastRFC.rfcPkt, pktData.rfcPkt );

                m_lastRFC.rawRFCPkt = pktData.rfcPkt;
                m_lastRFC.rfcCount++;

                CreateMPLReading( m_lastReading.mplReading, Now(), m_lastRFC.rfcPkt );

                m_lastReading.haveMPLReading = true;

                UpdateAveraging();
                UpdateBatteryVolts( m_lastRFC.rfcPkt.activeSolenoid != 0 );
                UpdateSwitches();
                CheckForPlug();
            }

            bRFCPending = true;

            DoOnRFCRecvd();
        }
    }

    // If a RFC is pending, check that the time params are up to date. If they
    // are not, a command will be sent to update those first.
    if( bRFCPending )
        bRFCPending = TimeParamsGood();

    // Next check if we need to send an activate solenoid command
    if( bRFCPending )
        bRFCPending = SolenoidsUpToDate();

    // Last check to see if any switches need to be cleared. Note: this check
    // must be the last in the above sequence that may send a command because
    // if a switch clear request is pending that request will be send continuously
    // until the causing condition finally allows the switch to be cleared.
    if( bRFCPending )
        bRFCPending = SwitchesUpToDate();

    // Now update the state machine
    switch( m_linkState )
    {
        case LS_IDLE:
            // If we have a RFC timeout, could have lost the link. Could also
            // scale back RFC interval based on the last time we got a command
            // from client modules.
            // TODO
            break;

        case LS_HUNTING:
            // We are currently waiting to receive a command from the sub.
            // Packet reception is captured in receive processing above,
            // so there is nothing to do here.
            break;

        case LS_CFG_DOWNLOAD:
            // We are downloading the calibration table. Check if we are
            // done - switch to idle if so.
            if( bRFCPending )
            {
                bool bNeedPages = false;

                // Loop until we send a config request or until we know we have
                // all config pages
                while( true )
                {
                    // Scan the table for any pending entries. If there is
                    // one, request it. Otherwise, switch to the idle state.
                    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
                    {
                        if( m_cfgPgStatus[iPg].havePage == false )
                        {
                            // We still need a page
                            bNeedPages = true;

                            // Send a request for this page if we haven't already
                            // done so in this page request cycle
                            if ( m_cfgPgStatus[iPg].lastCfgReqCycle < m_currCfgReqCycle )
                            {
                                MPL_DATA_UNION pktPayload;
                                pktPayload.cfgRequest.pageNbr = iPg;

                                SendMPLCommand( MCT_GET_CFG_PAGE, &pktPayload );

                                m_cfgPgStatus[iPg].lastCfgReqCycle = m_currCfgReqCycle;
                                bRFCPending = false;
                                break;
                            }
                        }
                    }

                    // Check if we've sent a request
                    if( !bRFCPending )
                        break;

                    // Check if we have all pages
                    if( !bNeedPages )
                        break;

                    // Step the page request cycle and start through the loop again.
                    // This will allow us to re-sent requests for pages that have
                    // not yet been received for some reason.
                    m_currCfgReqCycle += 1;
                }

                // If the RFC is pending, it means all pages have been downloaded.
                // Send the 'get version' command, and also put the link into the
                // idle state
                if( bRFCPending )
                {
                    // Calibration data all downloaded. Check the version of the
                    // cal data, and update if necessary.
                    CheckCalDataVersion();

                    // Send a request to get the verison info from the WTTTS.
                    // We don't implement a state for this, as the response
                    // consists of a single packet only.
                    SendMPLCommand( MCT_REQUEST_VER_INFO );
                    m_linkState = LS_IDLE;

                    // Also slow down the RFC rate
                    m_timeParams.rfcRate    = DEFAULT_RFC_RATE;
                    m_timeParams.rfcTimeout = DEFAULT_RFC_TIMEOUT;
                }
            }
            break;

        case LS_CFG_UPLOAD:
            // We are uploading the calibration table. Check if we are
            // done - switch to idle if so.
            if( bRFCPending )
            {
                // Scan the table for any pending entries. If there is
                // one, request it. Otherwise, switch to the idle state.
                for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
                {
                    if( m_cfgPgStatus[iPg].setPending )
                    {
                        MPL_DATA_UNION pktPayload;

                        pktPayload.cfgData.pageNbr = iPg;
                        pktPayload.cfgData.result  = 0;

                        int   pageOffset = iPg * NBR_BYTES_PER_WTTTS_PAGE;
                        BYTE* pgPointer  = &( m_rawCfgData[pageOffset] );

                        memcpy( pktPayload.cfgData.pageData, pgPointer, NBR_BYTES_PER_WTTTS_PAGE );

                        SendMPLCommand( MCT_SET_CFG_PAGE, &pktPayload );

                        bRFCPending = false;

                        break;
                    }
                }

                // If the RFC is pending, it means all pages have been set. Switch
                // to the idle state.
                if( bRFCPending )
                {
                    m_linkState = LS_IDLE;

                    // Also slow down the RFC rate
                    m_timeParams.rfcRate    = DEFAULT_RFC_RATE;
                    m_timeParams.rfcTimeout = DEFAULT_RFC_TIMEOUT;
                }
            }
            break;
    }

    // If at this point the RFC is still pending, send a 'no command' response
    if( bRFCPending )
        SendMPLCommand( MCT_NO_COMMAND );

    // Finally, data values are only valid in certain states. In any other
    // state the averaged data values must be reset. Don't update the averages
    // here (that happens above with the reception of each packet). Only
    // reset the averaging if required.
    switch( m_linkState )
    {
        case LS_IDLE:
            break;

        default:

            // In any other state, data from the tool is not valid.
            // Reset the data averages
            m_avgRPM->Reset();

            m_batteryInfo.avgBattVolts->Reset();
            m_batteryInfo.avgBattUsed->Reset();

            break;
    }

    return true;
}


void TWirelessMPLDevice::RefreshSettings( void )
{
    // Called to refresh any registry-based settings. Causes those settings
    // to be re-read from the registry. Do base class refresh first,
    TMPLDevice::RefreshSettings();

    BATT_THRESH_VALS battThresh;
    GetBatteryThresholds( battThresh );

    m_batteryInfo.fValidThresh   = battThresh.fReadingValidThresh;
    m_batteryInfo.fMinOperThresh = battThresh.fMinOperValue;
    m_batteryInfo.fGoodThresh    = battThresh.fGoodThresh;
}


bool TWirelessMPLDevice::GetLastAvgReading( MPL_READING& lastPkt )
{
    if( !m_lastReading.haveMPLReading )
        return false;

    lastPkt = m_lastReading.mplReading;

    m_lastReading.haveMPLReading = false;

    return true;
}


bool TWirelessMPLDevice::GetLastMinMaxReading( MIN_MAX_DATA& lastPkt )
{
    // TODO
    return false;
}


bool TWirelessMPLDevice::ShowLastRFCPkt( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, RFCFieldCaptions, NBR_RFC_FIELD_TYPES ) )
        return false;

    // Always show the device state
    pValues->Strings[RFT_DEV_STATE] = GetDevStatus();

    // Make sure we have a RFC packet
    if( ( m_linkState != LS_IDLE ) || ( m_lastRFC.rfcCount == 0 ) )
        return true;

    // Have a RFC - populate the value string list
    pValues->Strings[RFT_DEV_ID]           = m_sDevID;
    pValues->Strings[RFT_LAST_RFC_TIME]    = m_lastRFC.tRFCTime.TimeString();
    pValues->Strings[RFT_RF_CHAN]          = IntToStr( m_lastRFC.rfcPkt.rfChannel );
    pValues->Strings[RFT_BATT_TYPE]        = GetBattTypeText( m_lastRFC.rfcPkt.battType );
    pValues->Strings[RFT_BATT_VOLTS]       = FloatToStrF( m_lastRFC.rfcPkt.fBattVoltage, ffFixed, 7, 3 );
    pValues->Strings[RFT_BATT_USED]        = IntToStr( m_lastRFC.rfcPkt.battUsed );
    pValues->Strings[RFT_RFC_RATE]         = IntToStr( m_lastRFC.rfcPkt.rfcRate )         + " msecs";
    pValues->Strings[RFT_RFC_TIMEOUT]      = IntToStr( m_lastRFC.rfcPkt.rfcTimeout )      + " msecs";
    pValues->Strings[RFT_PAIR_TIMEOUT]     = IntToStr( m_lastRFC.rfcPkt.pairingTimeout )  + " secs";
    pValues->Strings[RFT_SOLENOID_TIMEOUT] = IntToStr( m_lastRFC.rfcPkt.solenoidTimeout ) + " secs";

    pValues->Strings[RFT_TEMPERATURE]      = FloatToStrF( m_lastRFC.rfcPkt.fTemperature, ffFixed, 7, 1 );
    pValues->Strings[RFT_RPM]              = FloatToStrF( m_lastRFC.rfcPkt.fRPM,         ffFixed, 7, 0 );
    pValues->Strings[RFT_PRESSURE]         = FloatToStrF( m_lastRFC.rfcPkt.fPressure,    ffFixed, 7, 1 );

    pValues->Strings[RFT_SENSOR_AVGING]    = IntToStr( m_lastRFC.rfcPkt.sensorAvgFactor );
    pValues->Strings[RFT_SENSOR_RATE]      = GetSensorSamplingRateText( m_lastRFC.rfcPkt.sensorSamplingRate );

    switch( m_lastRFC.rfcPkt.lastReset )
    {
        case WDRT_POWER_UP :   pValues->Strings[RFT_LAST_RESET] = "Power-up";   break;
        case WDRT_WATCH_DOG:   pValues->Strings[RFT_LAST_RESET] = "WDT";        break;
        case WDRT_BROWN_OUT:   pValues->Strings[RFT_LAST_RESET] = "Brown out";  break;
        default:               pValues->Strings[RFT_LAST_RESET] = "Type " + IntToStr( m_lastRFC.rfcPkt.lastReset );  break;
    }

    switch( m_lastRFC.rfcPkt.currMode )
    {
        case WDMT_DEEP_SLEEP:   pValues->Strings[RFT_CURR_MODE] = "Entering deep sleep";  break;
        case WDMT_NORMAL    :   pValues->Strings[RFT_CURR_MODE] = "Normal";               break;
        case WDMT_LOW_POWER :   pValues->Strings[RFT_CURR_MODE] = "Low power";            break;
        default:                pValues->Strings[RFT_CURR_MODE] = "Type " + IntToStr( m_lastRFC.rfcPkt.lastReset );  break;
    }

    if( m_lastRFC.rfcPkt.activeSolenoid == 0 )
        pValues->Strings[RFT_SOLENOID_STATE] = "All Off";
    else
        pValues->Strings[RFT_SOLENOID_STATE] = "Solenoid " + IntToStr( m_lastRFC.rfcPkt.activeSolenoid ) + " on";

    String sValue;

    for( int iContact = 0; iContact < NBR_MPL_CONTACTS; iContact++ )
    {
        switch( m_lastRFC.rfcPkt.pibContacts[iContact] )
        {
            case eBS_On:   sValue += IntToStr( iContact + 1 ) + " = On  "; break;
            case eBS_Off:  sValue += IntToStr( iContact + 1 ) + " = Off "; break;
            default:       sValue += IntToStr( iContact + 1 ) + " = --- "; break;
        }
    }

    pValues->Strings[RFT_CONTACTS_STATE] = sValue;

    sValue = "";

    for( int iSwitch = 0; iSwitch < NBR_MPL_PIB_SWITCHES; iSwitch++ )
    {
        switch( m_lastRFC.rfcPkt.pibSwitches[iSwitch] )
        {
            case eBS_On:   sValue += IntToStr( iSwitch + 1 ) + " = On  "; break;
            case eBS_Off:  sValue += IntToStr( iSwitch + 1 ) + " = Off "; break;
            default:       sValue += IntToStr( iSwitch + 1 ) + " = --- "; break;
        }
    }

    pValues->Strings[RFT_SWITCHES_STATE] = sValue;

    // Lastly, do each board entry
    String sSensorAddrs[MAX_NBR_MPL_SENSOR_BOARDS];

    sValue = "";

    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
    {
        if( m_lastRFC.rfcPkt.activeSensors[iBd] != 0 )
            sSensorAddrs[iBd] = IntToStr( m_lastRFC.rfcPkt.activeSensors[iBd] );
        else
            sSensorAddrs[iBd] = "---";

        sValue += sSensorAddrs[iBd] + " ";
    }

    pValues->Strings[RFT_SENSOR_BOARDS] = sValue;

    // Do board switches - only show switches for active boards
    sValue = "";

    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
    {
        if( m_lastRFC.rfcPkt.activeSensors[iBd] != 0 )
        {
            String sSwitches = sSensorAddrs[iBd] + ": ";

            switch( m_lastRFC.rfcPkt.sensSW1[iBd] )
            {
                case eBS_On:   sSwitches += "On  / "; break;
                case eBS_Off:  sSwitches += "Off / "; break;
                default:       sSwitches += "--- / "; break;
            }

            switch( m_lastRFC.rfcPkt.sensSW2[iBd] )
            {
                case eBS_On:   sSwitches += "On;  ";  break;
                case eBS_Off:  sSwitches += "Off; "; break;
                default:       sSwitches += "---; "; break;
            }

            sValue += sSwitches;
        }
    }

    pValues->Strings[RFT_SENSOR_INPUTS] = sValue;

    // Lastly do sensor readings
    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
    {
        if( m_lastRFC.rfcPkt.activeSensors[iBd] != 0 )
            sValue.printf( L"%7.0f, %7.0f, %7.0f", m_lastRFC.rfcPkt.sensXAvg[iBd], m_lastRFC.rfcPkt.sensYAvg[iBd], m_lastRFC.rfcPkt.sensZAvg[iBd] );
        else
            sValue = "";

        switch( iBd )
        {
            case 0:  pValues->Strings[RFT_SENSOR1_VALS] = sValue;  break;
            case 1:  pValues->Strings[RFT_SENSOR2_VALS] = sValue;  break;
            case 2:  pValues->Strings[RFT_SENSOR3_VALS] = sValue;  break;
            case 3:  pValues->Strings[RFT_SENSOR4_VALS] = sValue;  break;
        }
    }

    return true;
}


bool TWirelessMPLDevice::GetLastRFCPkt( TDateTime& tPktTime, DWORD& rfcCount, GENERIC_MPL_RFC_PKT& rfcPkt )
{
    if( ( m_linkState == LS_IDLE ) && ( m_lastRFC.rfcCount != 0 ) )
    {
        tPktTime = m_lastRFC.tRFCTime;
        rfcCount = m_lastRFC.rfcCount;
        rfcPkt   = m_lastRFC.rfcPkt;

        return true;
    }

    // Fall through means we have no packet
    tPktTime.Val = 0.0;
    rfcCount     = 0;

    ClearGenericRFCPkt( rfcPkt );

    return false;
}


void TWirelessMPLDevice::InitMPLCommandPkt( MPL_CMD_TYPE cmdType )
{
    // Clear the memory area first
    memset( &( m_cachedCmds[cmdType] ), 0, sizeof( CACHED_CMD_PKT ) );

    // Determine the packet type byte, and payload length (if any)
    BYTE* byBuff = m_cachedCmds[cmdType].byTxData;

    WTTTS_PKT_HDR* pHdr = (WTTTS_PKT_HDR*)byBuff;

    // Setup header defaults
    pHdr->pktHdr    = MPL_HDR_TX;
    pHdr->pktType   = 0;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = 0;

    // Now initialize specific commands
    switch( cmdType )
    {
        case MCT_NO_COMMAND:
            pHdr->pktType = WTTTS_CMD_NO_CMD;
            pHdr->dataLen = 0;
            break;

        case MCT_SET_RATE_CMD:
            pHdr->pktType = WTTTS_CMD_SET_RATE;
            pHdr->dataLen = sizeof( WTTTS_RATE_PKT );
            break;

        case MCT_REQUEST_VER_INFO:
            pHdr->pktType = WTTTS_CMD_QUERY_VER;
            pHdr->dataLen = 0;
            break;

        case MCT_GET_CFG_PAGE:
            pHdr->pktType = WTTTS_CMD_GET_CFG_DATA;
            pHdr->dataLen = sizeof( WTTTS_CFG_ITEM );
            break;

        case MCT_SET_CFG_PAGE:
            pHdr->pktType = WTTTS_CMD_SET_CFG_DATA;
            pHdr->dataLen = sizeof( WTTTS_CFG_DATA );
            break;

        case MCT_SET_RF_CHANNEL:
            pHdr->pktType = WTTTS_CMD_SET_RF_CHANNEL;
            pHdr->dataLen = sizeof( WTTTS_SET_CHAN_PKT );
            break;

        case MCT_ENTER_DEEP_SLEEP:
            pHdr->pktType = WTTTS_CMD_ENTER_DEEP_SLEEP;
            pHdr->dataLen = 0;
            break;

        case MCT_GET_MIN_MAX_VALS:
            pHdr->pktType = WTTTS_CMD_GET_MIN_MAX_VALS;
            pHdr->dataLen = sizeof( WTTTS_REQ_MIN_MAX_PKT );
            break;

        case MCT_SET_PIB_CONTACTS:
            pHdr->pktType = WTTTS_CMD_SET_CONTACTS;
            pHdr->dataLen = sizeof( WTTTS_PIB_CONTACT_PKT );
            break;

        case MCT_SET_PIB_SOLENOID:
            pHdr->pktType = WTTTS_CMD_SET_SOLENOIDS;
            pHdr->dataLen = sizeof( WTTTS_PIB_SOLENOID_PKT );
            break;

        case MCT_SET_SENSOR_PARAMS:
            pHdr->pktType = WTTTS_CMD_SET_PIB_PARAMS;
            pHdr->dataLen = sizeof( WTTTS_PIB_PARAMS_PKT );
            break;

        case MCT_CLEAR_SWITCH:
            pHdr->pktType = WTTTS_CMD_CLR_PIB_SWITCH;
            pHdr->dataLen = sizeof( WTTTS_PIB_CLR_INPUT_PKT );
            break;

        default:
            // Don't know this command!
            throw( "TWirelessMPLDevice::InitMPLCommandPkt() - invalid MPL_CMD_TYPE enum" );
    }

    // If we don't need a payload, can finish initializing the command.
    // Otherwise, we have to finish the command each time it is sent.
    if( pHdr->dataLen == 0 )
    {
        m_cachedCmds[cmdType].needsPayload = false;
        m_cachedCmds[cmdType].cmdLen       = MIN_MPL_PKT_LEN;

        BYTE byCRC = CalcWTTTSChecksum( byBuff, sizeof( WTTTS_PKT_HDR ) );

        byBuff[ sizeof( WTTTS_PKT_HDR ) ] = byCRC;
    }
    else
    {
        m_cachedCmds[cmdType].needsPayload = true;
        m_cachedCmds[cmdType].cmdLen       = MIN_MPL_PKT_LEN + pHdr->dataLen;
    }
}


bool TWirelessMPLDevice::SendMPLCommand( MPL_CMD_TYPE aCmd, const MPL_DATA_UNION* pPayload )
{
    if( !IsConnected )
        return false;

    // If the command needs a payload, add it on
    if( m_cachedCmds[aCmd].needsPayload )
    {
        if( pPayload == NULL )
            return false;

        BYTE* txBuff   = m_cachedCmds[aCmd].byTxData;
        int   crcCount = m_cachedCmds[aCmd].cmdLen - 1;

        MPL_DATA_UNION* pData = (MPL_DATA_UNION*)&( txBuff[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pData, pPayload, sizeof( MPL_DATA_UNION ) );

        txBuff[crcCount] = CalcWTTTSChecksum( txBuff, crcCount );
    }

    BYTE* pBuff       = m_cachedCmds[aCmd].byTxData;
    DWORD bytesToSend = m_cachedCmds[aCmd].cmdLen;

    DWORD bytesSent = 0;

    // When sending the data, if sending on UDP we want to use a 'send to' command
    if( m_bUsingUDP )
        bytesSent = ((CUDPPort*)m_port.portObj)->SendTo( pBuff, bytesToSend, m_sMPLDevAddr, m_wMPLDevPort );
    else
        bytesSent = m_port.portObj->CommSend( pBuff, bytesToSend );

    IncStat( m_port.stats.cmdsSent );
    IncStat( m_port.stats.bytesSent, bytesSent );

    return( bytesSent == bytesToSend );
}


UnicodeString TWirelessMPLDevice::GetDevStatus( void )
{
    switch( m_linkState )
    {
        case LS_CLOSED:        return "Port closed";
        case LS_HUNTING:       return "Hunting";
        case LS_CFG_DOWNLOAD:  return "Downloading config data";
        case LS_CFG_UPLOAD:    return "Uploading config data";

        case LS_IDLE:
            if( DeviceIsRecving )
                return "Active";
            else
                return "Packet Timeout";
    }

    // Fall through means unknown state!
    return "State " + IntToStr( m_linkState );
}


bool TWirelessMPLDevice::GetDevIsIdle( void )
{
    return( m_linkState == LS_IDLE );
}


bool TWirelessMPLDevice::GetDevIsCalibrated( void )
{
    // Return true if all cal pages have been downloaded
    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        if( !m_cfgPgStatus[iPg].havePage )
            return false;
    }

    // Fall through means we have config data
    return true;
}


bool TWirelessMPLDevice::GetCanStartCycle( void )
{
    // Can't launch or clean if not in idle state
    if( m_linkState != LS_IDLE )
        return false;

    // Can only launch or clean if we don't have something in progress
    return( m_activeSolenoid == 0 );
}


bool TWirelessMPLDevice::GetCleaningDone( void )
{
    // Cleaning is done if the active solenoid is not a cleaning solenoid
    return !IsCleaningSolenoid( m_activeSolenoid );
}


void TWirelessMPLDevice::UpdateAveraging( void )
{
    // Method called to update averages when a RFC packet is received
    m_avgRPM->AddValue( m_lastRFC.rfcPkt.fRPM );
}


void TWirelessMPLDevice::ClearHardwareInfo( void )
{
    m_cfgStraps = 0;
    memset( m_firmwareVer, 0, WTTTS_FW_VER_LEN );
}


void TWirelessMPLDevice::UpdateSwitches( void )
{
    // Called after a RFC is received. Update the state of the switches
    // and clear any pending command if the switch is in the requested state.

    // First, copy the switches from the rfc to the module vars
    for( int iSw = 0; iSw < NBR_MPL_PIB_SWITCHES; iSw++ )
        m_pibSwitchState[iSw] = m_lastRFC.rfcPkt.pibSwitches[iSw];

    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
    {
        m_sensSW1State[iBd] = m_lastRFC.rfcPkt.sensSW1[iBd];
        m_sensSW2State[iBd] = m_lastRFC.rfcPkt.sensSW1[iBd];
    }

    // We can only command a switch to be turned off. If any switch
    // is off, then clear its corresponding command
    for( int iSw = 0; iSw < NBR_MPL_PIB_SWITCHES; iSw++ )
    {
        if( m_pibSwitchState[iSw] == eBS_Off )
            m_pibSwitchCmd[iSw] = eBC_NoCmd;
    }

    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
    {
        // Commands are cleared if we have no active board for a given slot
        // or if the switch state is off (can only clear a switch state)
        if( m_lastRFC.rfcPkt.activeSensors[iBd] == 0 )
        {
            m_sensSW1Cmd[iBd] = eBC_NoCmd;
            m_sensSW2Cmd[iBd] = eBC_NoCmd;
        }
        else
        {
            if( m_sensSW1State[iBd] == eBS_Off )
                m_sensSW1Cmd[iBd] = eBC_NoCmd;

            if( m_sensSW2State[iBd] == eBS_Off )
                m_sensSW2Cmd[iBd] = eBC_NoCmd;
        }
    }
}


void TWirelessMPLDevice::CheckForPlug( void )
{
    // Plug is detected using the auxilliary sensor boards using a magnet
    // and hall effect sensors. But the magnet may get installed backwards,
    // so we don't know if we're looking for a positive going or negative
    // going transition until we get our first reading. Assume that the
    // switch is *not* active when the first reading comes through.
    #define PLUG_DETECT_BD_NBR   0

    if( m_linkState == LS_IDLE )
    {
        if( m_lastRFC.rfcCount != 0 )
        {
            if( m_plugState == NULL )
            {
                if( m_lastRFC.rfcPkt.activeSensors[PLUG_DETECT_BD_NBR] )
                {
                    // Create the object only if we have a non-zero reading
                    float MinValidReading = 1000.0;
                    float fCurrReading    = m_lastRFC.rfcPkt.sensXAvg[PLUG_DETECT_BD_NBR];

                    if( fCurrReading > MinValidReading )
                        m_plugState = new TIntHysteresis( (int)MinValidReading, TIntHysteresis::TT_VALUE_DECREASING );
                    else if( fCurrReading < -MinValidReading )
                        m_plugState = new TIntHysteresis( (int)(-MinValidReading), TIntHysteresis::TT_VALUE_INCREASING );

                    // If we've constructed an object, set its timing params
                    if( m_plugState != NULL )
                    {
                        m_plugState->OffToOnDelay = 0;
                        m_plugState->OnToOffDelay = 10000;
                    }
                }
            }
            else
            {
                if( m_lastRFC.rfcPkt.activeSensors[PLUG_DETECT_BD_NBR] )
                {
                    float fCurrReading = m_lastRFC.rfcPkt.sensXAvg[PLUG_DETECT_BD_NBR];

                    m_plugState->SetCurrentValue( (int)fCurrReading );

                    if( m_plugState->IsActive )
                        SetPlugDetected();
                    else if( m_ePlugDetState == ePDS_DetectAckd )
                        m_ePlugDetState = ePDS_NoPlug;
                }
            }
        }
    }
}


bool TWirelessMPLDevice::LaunchPlug( int iPlug  )
{
    // Activates the launch mechanism
    if( m_linkState != LS_IDLE )
        return false;

    // Validate plug number
    if( ( iPlug <= 0 ) || ( iPlug > NBR_MPL_PLUGS ) )
        return false;

    // Ingore if the plug is currently launched
    if( m_activeSolenoid == iPlug )
        return true;

    // Cannot activate if we're already activated.
    if( m_activeSolenoid != 0 )
        return false;

    // Plug is launched by activating its solenoid
    m_activeSolenoid      = iPlug;
    m_solenoidLastCmdTime = 0;

    return true;
}


void TWirelessMPLDevice::AckPlugDetected( void )
{
    // Let the base class dd its work first
    TMPLDevice::AckPlugDetected();

    // If we're in the ack'd state, then clear the switch indication
    if( m_ePlugDetState == ePDS_DetectAckd )
        ClearPIBSwitchOn( 0 );
}


bool TWirelessMPLDevice::CleanPlug( int iPlug  )
{
    // Activates the cleaning mechanism for the passed plug
    if( m_linkState != LS_IDLE )
        return false;

    // Validate plug number
    if( ( iPlug <= 0 ) || ( iPlug > NBR_MPL_PLUGS ) )
        return false;

    int iCleaningPlug = GetCleaningSolenoidNbr( iPlug );

    // Ingore if we are currently doing this
    if( m_activeSolenoid == iCleaningPlug )
        return true;

    // Cannot activate if we're already activated.
    if( m_activeSolenoid != 0 )
        return false;

    // Cleaning is started by activating its solenoid
    m_activeSolenoid      = iCleaningPlug;
    m_solenoidLastCmdTime = 0;

    return true;
}


bool TWirelessMPLDevice::ActivateResetFlag( void )
{
    // Activates the reset flag solenoid
    if( m_linkState != LS_IDLE )
        return false;

    // Ingore if we are currently doing this
    if( m_activeSolenoid == MPL_RESET_FLAG_SOLENOID )
        return true;

    // Cannot activate if we're already activated.
    if( m_activeSolenoid != 0 )
        return false;

    // Reset operation starts by activating its solenoid
    m_activeSolenoid      = MPL_RESET_FLAG_SOLENOID;
    m_solenoidLastCmdTime = 0;

    return true;
}


bool TWirelessMPLDevice::TimeParamsGood( void )
{
    // Check that the current time params match those in the last RFC packet.
    // Only call this function if a RFC has been received and no other command
    // has been sent. Return true if all timing params match, otherwise
    // return false.
    MPL_DATA_UNION pktUnion;
    memset( &pktUnion, 0, sizeof( MPL_DATA_UNION ) );

    bool paramsGood = true;

    if( m_lastRFC.rawRFCPkt.rfcRate != m_timeParams.rfcRate )
    {
        pktUnion.ratePkt.rateType = SRT_RFC_RATE;
        pktUnion.ratePkt.newValue = m_timeParams.rfcRate;

        paramsGood = false;
    }
    else if( m_lastRFC.rawRFCPkt.rfcTimeout != m_timeParams.rfcTimeout )
    {
        pktUnion.ratePkt.rateType = SRT_RFC_TIMEOUT;
        pktUnion.ratePkt.newValue = m_timeParams.rfcTimeout;

        paramsGood = false;
    }
    else if( m_lastRFC.rawRFCPkt.pairingTimeout != m_timeParams.pairingTimeout )
    {
        pktUnion.ratePkt.rateType = SRT_PAIR_TIMEOUT;
        pktUnion.ratePkt.newValue = m_timeParams.pairingTimeout;

        paramsGood = false;
    }
    else if( m_lastRFC.rawRFCPkt.solenoidTimeout != m_timeParams.solenoidTimeout )
    {
        pktUnion.ratePkt.rateType = SRT_SOLENOID_TIMEOUT;
        pktUnion.ratePkt.newValue = m_timeParams.solenoidTimeout;

        paramsGood = false;
    }

    if( paramsGood )
        return true;

    // Fall through means we have to change a rate
    SendMPLCommand( MCT_SET_RATE_CMD, &pktUnion );

    return false;
}


bool TWirelessMPLDevice::SwitchesUpToDate( void )
{
    // Check that the current time params match those in the last RFC packet.
    // Only call this function if a RFC has been received and no other command
    // has been sent. Return true if all timing params match, otherwise
    // return false.
    MPL_DATA_UNION pktUnion;
    memset( &pktUnion, 0, sizeof( MPL_DATA_UNION ) );

    bool switchesOK = true;

    if( ( m_pibSwitchState[0] == eBS_On ) && ( m_pibSwitchCmd[0] == eBC_TurnOff ) )
    {
        pktUnion.clearPIBInput.deviceAddr  = 0;
        pktUnion.clearPIBInput.switchFlags = 0x01;

        switchesOK = false;
    }
    else if( ( m_pibSwitchState[1] == eBS_On ) && ( m_pibSwitchCmd[1] == eBC_TurnOff ) )
    {
        pktUnion.clearPIBInput.deviceAddr  = 0;
        pktUnion.clearPIBInput.switchFlags = 0x02;

        switchesOK = false;
    }
    else
    {
        for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
        {
            if( ( m_sensSW1State[iBd] == eBS_On ) && ( m_sensSW1Cmd[iBd] == eBC_TurnOff ) )
            {
                pktUnion.clearPIBInput.deviceAddr  = m_bySensorBdAddrs[iBd];
                pktUnion.clearPIBInput.switchFlags = 0x01;

                switchesOK = false;
                break;
            }

            if( ( m_sensSW2State[iBd] == eBS_On ) && ( m_sensSW2State[iBd] == eBC_TurnOff ) )
            {
                pktUnion.clearPIBInput.deviceAddr  = m_bySensorBdAddrs[iBd];
                pktUnion.clearPIBInput.switchFlags = 0x02;

                switchesOK = false;
                break;
            }
        }
    }

    if( switchesOK )
        return true;

    // Fall through means we have to change a rate. To prevent a repeating cycle
    // of sending this command while a switch is potentially held active physically,
    // we only send it every few msecs
    if( !HaveTimeout( m_dwLastClearSwitchCmdTime, ClearSwCmdInterval ) )
        return true;

    SendMPLCommand( MCT_CLEAR_SWITCH, &pktUnion );

    m_dwLastClearSwitchCmdTime = GetTickCount();

    return false;
}


bool TWirelessMPLDevice::SolenoidsUpToDate( void )
{
    // Call this method when there is an RFC pending to check if we need
    // to send an 'activate solenoid' command. Returns true if the solenoids
    // are up to date (eg, no command was sent), or false if a command was
    // sent (and therefore an RFC is no longer pending).

    // We send a command if:
    //   - The currently active solenoid in the last RFC does not
    //     match the solenoid we want active, or
    //   - We have an active solenoid that matches what was in the
    //     last RFC and a command timeout has expired
    MPL_DATA_UNION pktUnion;
    memset( &pktUnion, 0, sizeof( MPL_DATA_UNION ) );

    bool bSolenoidsOk = true;

    // First check for a solenoid 'mismatch'
    if( m_lastRFC.rfcPkt.activeSolenoid != m_activeSolenoid )
    {
        if( m_activeSolenoid == 0 )
        {
            pktUnion.solenoidPkt.solenoidNbr = 0;
            pktUnion.solenoidPkt.state       = 0;
        }
        else
        {
            // Turning on a solenoid in this case
            pktUnion.solenoidPkt.solenoidNbr = m_activeSolenoid;
            pktUnion.solenoidPkt.state       = 1;

            m_solenoidActivatedTime = GetTickCount();
        }

        bSolenoidsOk = false;
    }
    else
    {
        // In this case, the active solenoid in the PIB matches our desired solenoid.
        // If we have an active solenoid, re-issue the command periodically
        if( m_activeSolenoid != 0 )
        {
            // Determine the solenoid type that is currently on: launch and
            // cleaning cycles have different durations.
            bool bIsCleaningSolenoid  = IsCleaningSolenoid( m_activeSolenoid );
            bool bIsResetFlagSolenoid = ( m_activeSolenoid == MPL_RESET_FLAG_SOLENOID );

            DWORD dwSolOnTime;

            if( bIsCleaningSolenoid )
                dwSolOnTime = m_solOnTimeClean;
            else if( bIsResetFlagSolenoid )
                dwSolOnTime = m_solOnTimeReset;
            else
                dwSolOnTime = m_solOnTimeLaunch;

            if( HaveTimeout( m_solenoidActivatedTime, dwSolOnTime ) )
            {
                // Solenoid has been on for the required duration - turn it off now
                m_activeSolenoid = 0;

                pktUnion.solenoidPkt.solenoidNbr = 0;
                pktUnion.solenoidPkt.state       = 0;

                bSolenoidsOk = false;

                if( bIsCleaningSolenoid )
                    m_bCleanDoneEventPending = true;
                else if( bIsResetFlagSolenoid )
                    m_bResetFlagEventPending = true;
            }
            else if( HaveTimeout( m_solenoidLastCmdTime, SetSolenoidInterval ) )
            {
                // Re-assert the solenoid on command
                pktUnion.solenoidPkt.solenoidNbr = m_activeSolenoid;
                pktUnion.solenoidPkt.state       = 1;

                bSolenoidsOk = false;
            }
        }
    }

    if( bSolenoidsOk )
    {
        // Solenoid in device matches our internal state. Can fire any pending
        // 'done' events now
        if( m_bCleanDoneEventPending )
        {
            m_bCleanDoneEventPending = false;

            DoOnCleaningDone();
        }

        if( m_bResetFlagEventPending )
        {
            m_bResetFlagEventPending = false;

            DoOnResetFlagDone();
        }

        return true;
    }

    // Solenoids need updating
    SendMPLCommand( MCT_SET_PIB_SOLENOID, &pktUnion );

    m_solenoidLastCmdTime = GetTickCount();

    return false;
}


//
// WTTTS Onboard Configuration Data Support Functions
//

bool TWirelessMPLDevice::GetCalDataRaw( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( !IsConnected )
        return false;

    // Report the raw data as array of pages
    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        UnicodeString pageDataStr;

        BYTE* pData = &( m_rawCfgData[ iPg * NBR_BYTES_PER_WTTTS_PAGE ] );

        for( int iByte = 0; iByte < NBR_BYTES_PER_WTTTS_PAGE; iByte++ )
            pageDataStr = pageDataStr + IntToHex( pData[iByte], 2 ) + " ";

        pCaptions->Add( "Page " + IntToStr( iPg ) );
        pValues->Add( pageDataStr );
    }

    return true;
}


static UnicodeString CharsToStr( const char* szString, int maxLength )
{
    char* szTemp = new char[maxLength];

    memset( szTemp, 0, maxLength );

    for( int iChar = 0; iChar < maxLength - 1; iChar++ )
    {
        if( szString[iChar] == '\0' )
            break;

        szTemp[iChar] = szString[iChar];
    }

    UnicodeString sResult = szTemp;

    delete [] szTemp;

    return sResult;
}


static UnicodeString MfgDateToStr( BYTE relYear, BYTE month )
{
    int year = 2000 + (int)relYear;

    AnsiString sMonth;

    switch( month )
    {
        case 0:   sMonth = "Jan";  break;
        case 1:   sMonth = "Feb";  break;
        case 2:   sMonth = "Mar";  break;
        case 3:   sMonth = "Apr";  break;
        case 4:   sMonth = "May";  break;
        case 5:   sMonth = "Jun";  break;
        case 6:   sMonth = "Jul";  break;
        case 7:   sMonth = "Aug";  break;
        case 8:   sMonth = "Sep";  break;
        case 9:   sMonth = "Oct";  break;
        case 10:  sMonth = "Nov";  break;
        case 11:  sMonth = "Dec";  break;
        default:  sMonth = "Month " + IntToStr( month );
    }

    AnsiString sResult;
    sResult.printf( "%d %s", year, sMonth.c_str() );

    return UnicodeString( sResult );
}


bool TWirelessMPLDevice::GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo )
{
    WTTTS_MFG_DATA_STRUCT* pMfgData = (WTTTS_MFG_DATA_STRUCT*)m_rawCfgData;

    const UnicodeString notSet( "(not set)" );

    if( pMfgData->devSN != 0xFFFF )
        devHWInfo.serialNbr = IntToStr( pMfgData->devSN );
    else
        devHWInfo.serialNbr = notSet;

    if( pMfgData->szMfgInfo[0] != (char)0xFF )
        devHWInfo.mfgInfo = CharsToStr( pMfgData->szMfgInfo, 8 );
    else
        devHWInfo.mfgInfo = notSet;

    if( pMfgData->yearOfMfg != 0xFF )
        devHWInfo.mfgDate = MfgDateToStr( pMfgData->yearOfMfg, pMfgData->monthOfMfg );
    else
        devHWInfo.mfgDate = notSet;

    if( m_firmwareVer[0] != 0xFF )
        devHWInfo.fwRev = IntToStr( m_firmwareVer[0] ) + "." + IntToStr( m_firmwareVer[1] ) + "." + IntToStr( m_firmwareVer[2] ) + "." + IntToStr( m_firmwareVer[3] );
    else
        devHWInfo.fwRev = notSet;

    devHWInfo.cfgStraps = IntToHex( m_cfgStraps, 2 );

    return true;
}


bool TWirelessMPLDevice::GetDeviceBatteryInfo( BATTERY_INFO& battInfo )
{
    if( !m_batteryInfo.avgBattVolts->IsValid )
        return false;

    MPL_CAL_DATA_STRUCT_V1* pCalData = (MPL_CAL_DATA_STRUCT_V1*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    battInfo.battType        = m_lastRFC.rfcPkt.battType;
    battInfo.battTypeText    = GetBattTypeText( m_lastRFC.rfcPkt.battType );
    battInfo.initialCapacity = pCalData->battCapacity;
    battInfo.currVolts       = m_batteryInfo.fLastBattVolts;
    battInfo.currUsage       = m_batteryInfo.iLastBattUsed;
    battInfo.avgVolts        = m_batteryInfo.avgBattVolts->AverageValue;
    battInfo.avgUsage        = (int)( m_batteryInfo.avgBattUsed->AverageValue );

    return true;
}


bool TWirelessMPLDevice::GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, CalFactorCaptions, NBR_CALIBRATION_ITEMS ) )
        return false;

    WTTTS_MFG_DATA_STRUCT*  pMfgData = (WTTTS_MFG_DATA_STRUCT*)    m_rawCfgData;
    MPL_CAL_DATA_STRUCT_V1* pCalData = (MPL_CAL_DATA_STRUCT_V1*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*   pVerData = (WTTTS_CAL_VER_STRUCT*)  &( m_rawCfgData[ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    for( int iItem = 0; iItem < NBR_CALIBRATION_ITEMS; iItem++ )
    {
        UnicodeString sValue;

        switch( iItem )
        {
            case CI_VERSION:           sValue = IntToStr    ( pVerData->byCalVerNbr );      break;
            case CI_REVISION:          sValue = IntToStr    ( pVerData->byCalRevNbr );      break;
            case CI_SERIAL_NBR:        sValue = IntToStr    ( pMfgData->devSN );            break;
            case CI_MFG_INFO:          sValue = CharsToStr  ( pMfgData->szMfgInfo, 8 );     break;
            case CI_MFG_DATE:          sValue = MfgDateToStr( pMfgData->yearOfMfg, pMfgData->monthOfMfg );  break;
            case CI_PRESSURE_OFFSET:   sValue = IntToStr    ( pCalData->pressure_Offset );  break;
            case CI_PRESSURE_SPAN:     sValue = FloatToStrF ( pCalData->pressure_Span, ffExponent, 7, 2 );  break;
            case CI_BATTERY_CAPACITY:  sValue = IntToStr    ( pCalData->battCapacity );     break;
            case CI_BATTERY_TYPE:      sValue = GetBattTypeText( (BATTERY_TYPE)pCalData->battType );        break;

            case CI_CALIBRATION_DATE:
                if( pVerData->calYear != 0xFF )
                {
                    sValue.printf( L"%d/%02d/%02d", (int)pVerData->calYear + 2000,
                                                    (int)pVerData->calMonth,
                                                    (int)pVerData->calDay );
                }
                else
                {
                    sValue = "----/--/--";
                }
                break;
        }

        pValues->Strings[iItem] = sValue;
    }

    return true;
}


bool TWirelessMPLDevice::SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    // Although the user can try to pass values for all items in configuration
    // memory, we only change the values of items in the host config area.
    // Note that this function only updates the data cached in the host memory
    // area - it does not commit the data to the device.
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    MPL_CAL_DATA_STRUCT_V1* pCalData = (MPL_CAL_DATA_STRUCT_V1*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*   pVerData = (WTTTS_CAL_VER_STRUCT*)  &( m_rawCfgData[ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    try
    {
        for( int iParam = 0; iParam < pCaptions->Count; iParam++ )
        {
            UnicodeString sKey   = pCaptions->Strings[iParam];
            UnicodeString sValue = pValues->Strings[iParam];

            for( int paramIndex = 0; paramIndex < NBR_CALIBRATION_ITEMS; paramIndex++ )
            {
                if( sKey == CalFactorCaptions[paramIndex] )
                {
                    switch( paramIndex )
                    {
                        case CI_REVISION:           pVerData->byCalRevNbr       = sValue.ToInt();     break;
                        case CI_PRESSURE_OFFSET:    pCalData->pressure_Offset   = sValue.ToInt();     break;
                        case CI_PRESSURE_SPAN:      pCalData->pressure_Span     = sValue.ToDouble();  break;
                        case CI_BATTERY_CAPACITY:   pCalData->battCapacity      = sValue.ToInt();     break;
                        case CI_BATTERY_TYPE:       pCalData->battType          = GetBattTypeEnum( sValue ); break;
                    }

                    // Can break out of search loop when an item is found
                    break;
                }
            }
        }
    }
    catch( ... )
    {
        // A catch means either the pValues array was insufficiently populated
        // or one of the entries was an invalid value.
        return false;
    }

    // Fall through means success - update the current cal version
    pVerData->byCalVerNbr = CurrCalVersion;
    pVerData->byCalRevNbr = CurrCalRevision;

    return true;
}


bool TWirelessMPLDevice::ReloadCalDataFromDevice( void )
{
    if( !IsConnected )
        return false;

    // Can only start the upload to device if we are currently idle
    if( m_linkState != LS_IDLE )
        return false;

    ClearConfigurationData();

    // Set the link state, and bump up the RFC timers for the data exchange
    m_timeParams.rfcRate    = StartupRFCInterval;
    m_timeParams.rfcTimeout = StartupRFCTimeout;

    m_linkState = LS_CFG_DOWNLOAD;

    m_currCfgReqCycle = 1;

    return true;
}


bool TWirelessMPLDevice::WriteCalDataToDevice( void )
{
    if( !IsConnected )
        return false;

    // Can only start the upload to device if we are currently idle
    if( m_linkState != LS_IDLE )
        return false;

    for( int iPg = WTTTS_CFG_FIRST_HOST_PAGE; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
        m_cfgPgStatus[iPg].setPending = true;

    // Set the link state, and bump up the RFC timers for the data exchange
    m_timeParams.rfcRate    = StartupRFCInterval;
    m_timeParams.rfcTimeout = StartupRFCTimeout;

    m_linkState = LS_CFG_UPLOAD;

    return true;
}


void TWirelessMPLDevice::ClearConfigurationData( void )
{
    // Set all pages to the EEPROM 'empty' value first (0xFF)
    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        m_cfgPgStatus[iPg].havePage        = false;
        m_cfgPgStatus[iPg].lastCfgReqCycle = 0;
        m_cfgPgStatus[iPg].setPending      = false;

        memset( m_rawCfgData, 0xFF, NBR_WTTTS_CFG_PAGES * NBR_BYTES_PER_WTTTS_PAGE );
    }

    m_currCfgReqCycle = 0;

    // Call CheckCalDataVersion next - this will initialize our local
    // memory to default values while we wait for the config data
    // to be downloaded.
    CheckCalDataVersion();
}


void TWirelessMPLDevice::CheckCalDataVersion( void )
{
    // This is the first version / rev of the MPL cal struct
    MPL_CAL_DATA_STRUCT_V1* pCalData1v0 = (MPL_CAL_DATA_STRUCT_V1*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*   pVerData    = (WTTTS_CAL_VER_STRUCT*)  &( m_rawCfgData[ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    // Check for the current version. If we're at it, we are done.
    if( ( pVerData->byCalVerNbr == CurrCalVersion )
          &&
        ( pVerData->byCalRevNbr == CurrCalRevision )
      )
    {
        return;
    }

    // Fall through means we don't know this cal struct. Init it to default values
    memcpy( pCalData1v0, &defaultMPLCalData, sizeof( MPL_CAL_DATA_STRUCT_V1 ) );

    pVerData->byCalVerNbr = CurrCalVersion;
    pVerData->byCalRevNbr = CurrCalRevision;
}


void TWirelessMPLDevice::ResetRFCControlStruct( void )
{
    m_lastRFC.rfcCount     = 0;
    m_lastRFC.dwRFCTick    = 0;
    m_lastRFC.tRFCTime.Val = 0.0;

    memset( &m_lastRFC.rawRFCPkt, 0, sizeof( WTTTS_MPL_RFC_PKT ) );

    ClearGenericRFCPkt( m_lastRFC.rfcPkt );
}


void TWirelessMPLDevice::CreateGenericRFC( GENERIC_MPL_RFC_PKT& rfcPkt, const WTTTS_MPL_RFC_PKT& rawRFCPkt )
{
    MPL_CAL_DATA_STRUCT_V1* pCalData1v0 = (MPL_CAL_DATA_STRUCT_V1*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    rfcPkt.battType           = (BATTERY_TYPE)rawRFCPkt.battType;
    rfcPkt.fBattVoltage       = ConvertRawBattVolts( rawRFCPkt.battVoltage );
    rfcPkt.battUsed           = rawRFCPkt.battUsed;
    rfcPkt.rfChannel          = rawRFCPkt.rfChannel;
    rfcPkt.lastReset          = (WIRELESS_DEV_RESET_TYPE)rawRFCPkt.lastReset;
    rfcPkt.currMode           = (WIRELESS_DEV_MODE_TYPE)rawRFCPkt.currMode;
    rfcPkt.fTemperature       = ConvertRawTemp( rawRFCPkt.temperature );
    rfcPkt.fPressure          = DoSpanOffsetCal( TriByteToInt( rawRFCPkt.pressure ), pCalData1v0->pressure_Offset, pCalData1v0->pressure_Span );
    rfcPkt.fRPM               = ConvertRawRPM( rawRFCPkt.rpm );
    rfcPkt.rfcRate            = rawRFCPkt.rfcRate;
    rfcPkt.rfcTimeout         = rawRFCPkt.rfcTimeout;
    rfcPkt.pairingTimeout     = rawRFCPkt.pairingTimeout;
    rfcPkt.solenoidTimeout    = rawRFCPkt.solenoidTimeout;
    rfcPkt.sensorSamplingRate = rawRFCPkt.sensorSamplingRate;
    rfcPkt.sensorAvgFactor    = rawRFCPkt.sensorAvgFactor;
    rfcPkt.deviceID           = DeviceIDToStr( rawRFCPkt.deviceID );
    rfcPkt.activeSolenoid     = rawRFCPkt.activeSolenoid;
    rfcPkt.pibSwitches[0]     = ( rawRFCPkt.contactState & 0x10 ) ? eBS_On : eBS_Off;
    rfcPkt.pibSwitches[1]     = ( rawRFCPkt.contactState & 0x20 ) ? eBS_On : eBS_Off;

    BYTE byContactFlag = 0x01;

    for( int iCont = 0; iCont < NBR_MPL_CONTACTS; iCont++ )
    {
        rfcPkt.pibContacts[iCont] = ( rawRFCPkt.contactState & byContactFlag ) ? eBS_On : eBS_Off;

        byContactFlag <<= 1;
    }

    // As with the PIB switches, sensor switches latch an 'on' indication.
    // On indications must be cleared by the client.
    BYTE bySW1Flag = 0x01;
    BYTE bySW2Flag = 0x02;

    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
    {
        rfcPkt.activeSensors[iBd] = rawRFCPkt.activeSensors[iBd];

        // Only show switch state for reported boards
        if( rfcPkt.activeSensors[iBd] != 0 )
        {
            rfcPkt.sensSW1[iBd] = ( rawRFCPkt.inputState & bySW1Flag ) ? eBS_On : eBS_Off;
            rfcPkt.sensSW2[iBd] = ( rawRFCPkt.inputState & bySW2Flag ) ? eBS_On : eBS_Off;
        }
        else
        {
            rfcPkt.sensSW1[iBd] = eBS_Unknown;
            rfcPkt.sensSW2[iBd] = eBS_Unknown;
        }

        bySW1Flag <<= 2;
        bySW2Flag <<= 2;

        rfcPkt.sensXAvg[iBd] = ConvertRawReading( rawRFCPkt.sensXAvg[iBd] );
        rfcPkt.sensYAvg[iBd] = ConvertRawReading( rawRFCPkt.sensYAvg[iBd] );
        rfcPkt.sensZAvg[iBd] = ConvertRawReading( rawRFCPkt.sensZAvg[iBd] );
    }
}


void TWirelessMPLDevice::CreateMPLReading( MPL_READING& mplReading, TDateTime tReadingTime, const GENERIC_MPL_RFC_PKT& rfcPkt )
{
    mplReading.tTime = tReadingTime;

    for( int iContact  = 0; iContact < NBR_MPL_CONTACTS; iContact++ )
         mplReading.bsContacts[iContact] = rfcPkt.pibContacts[iContact];

    for( int iSw = 0; iSw < NBR_MPL_PIB_SWITCHES; iSw++ )
         mplReading.bsPIBSwitches[iSw] = rfcPkt.pibSwitches[iSw];

    mplReading.iActiveSolenoid = rfcPkt.activeSolenoid;

    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
    {
        mplReading.avgReadings[iBd].bySensorID = rfcPkt.activeSensors[iBd];
        mplReading.avgReadings[iBd].bsSW1      = rfcPkt.sensSW1[iBd];
        mplReading.avgReadings[iBd].bsSW2      = rfcPkt.sensSW2[iBd];
        mplReading.avgReadings[iBd].fX         = rfcPkt.sensXAvg[iBd];
        mplReading.avgReadings[iBd].fY         = rfcPkt.sensYAvg[iBd];
        mplReading.avgReadings[iBd].fZ         = rfcPkt.sensZAvg[iBd];
    }
}


void TWirelessMPLDevice::ClearBatteryVolts( void )
{
    m_batteryInfo.avgBattVolts->Reset();
    m_batteryInfo.avgBattUsed->Reset();
}


void TWirelessMPLDevice::UpdateBatteryVolts( bool bSolenoidActive )
{
    // Assumes that an RFC packet was just received. Only update these
    // values if the battery voltage is above the valid reading thresh.
    if( m_lastRFC.rfcPkt.fBattVoltage >= m_batteryInfo.fValidThresh )
    {
        m_batteryInfo.fLastBattVolts = m_lastRFC.rfcPkt.fBattVoltage;
        m_batteryInfo.iLastBattUsed  = m_lastRFC.rfcPkt.battUsed;

        m_batteryInfo.avgBattUsed->AddValue( m_lastRFC.rfcPkt.battUsed );

        // The battery voltage will drop while a solenoid is active. If
        // one is currently on, then do not update the average.
        if( !bSolenoidActive )
            m_batteryInfo.avgBattVolts->AddValue( m_lastRFC.rfcPkt.fBattVoltage );

        if( m_batteryInfo.avgBattVolts->AverageValue < m_batteryInfo.fMinOperThresh )
            m_battLevel = eBL_Low;
        else if( m_batteryInfo.avgBattVolts->AverageValue < m_batteryInfo.fGoodThresh )
            m_battLevel = eBL_Med;
        else
            m_battLevel = eBL_OK;
    }
}


bool TWirelessMPLDevice::ClearPIBSwitchOn( int iPIBSwitch )
{
    if( m_linkState != LS_IDLE )
        return false;

    if( iPIBSwitch == 0 )
        m_pibSwitchCmd[0] = eBC_TurnOff;
    else if( iPIBSwitch == 1 )
        m_pibSwitchCmd[1] = eBC_TurnOff;

    return true;
}


bool TWirelessMPLDevice::ClearSensorSwitchOn( BYTE bySensorAddr, int iSwitch )
{
    if( m_linkState != LS_IDLE )
        return false;

    if( bySensorAddr == 0 )
        return false;

    for( int iBd = 0; iBd < MAX_NBR_MPL_SENSOR_BOARDS; iBd++ )
    {
        if( m_bySensorBdAddrs[iBd] == bySensorAddr )
        {
            if( iSwitch == 0 )
                m_sensSW1Cmd[0] = eBC_TurnOff;

            if( iSwitch == 1 )
                m_sensSW1Cmd[1] = eBC_TurnOff;

            return true;
        }
    }

    // Fall through means a bad address given
    return false;
}


//
// Data Logging
//

void TWirelessMPLDevice::LogRawRFCPacket( const WTTTS_PKT_HDR& pktHdr, const WTTTS_MPL_RFC_PKT& rfcPkt )
{
    // Write the record to file as text, if logging is enabled
    UnicodeString logFileName;

    if( !GetDataLogging( DLT_RAW_DATA, logFileName ) )
        return;

    TFileStream* logStream = NULL;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            AnsiString sHeader( "RxTime,TDB\r" );

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        // Convert all structure members first
//        int   iTorque045  = TriByteToInt    ( streamData.torque045 );
//        int   iTorque225  = TriByteToInt    ( streamData.torque225 );
//        int   iTension000 = TriByteToInt    ( streamData.tension000 );
//        int   iTension090 = TriByteToInt    ( streamData.tension090 );
//
//        AnsiString sTime = Time().TimeString();
//
        AnsiString sLine;
//        sLine.printf( "%s,%hu,%u,%i,%i,%i,%i,%i,%i,%i,%hi,%hi,%hi,%hi,%hi,%hi\r",
//                      sTime.c_str(), pktHdr.seqNbr, pktHdr.timeStamp * MSecsPerPktTick,
//                      iTorque045, iTorque225, iTension000, iTension090, iTension180, iTension270,
//                      iRotation, siCompassX, siCompassY, siCompassZ, siAccelX, siAccelY, siAccelZ );

        logStream->Write( sLine.c_str(), sLine.Length() );
    }
    catch( ... )
    {
    }

    if( logStream != NULL )
        delete logStream;
}



void TWirelessMPLDevice::LogConvRFCPacket( const WTTTS_PKT_HDR& pktHdr, const GENERIC_MPL_RFC_PKT& rfcPkt )
{
    // Write the record to file as text, if logging is enabled
    UnicodeString logFileName;

    if( !GetDataLogging( DLT_CAL_DATA, logFileName ) )
        return;

    TFileStream* logStream = NULL;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            AnsiString sHeader( "RxTime,TDB\r" );

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        // Convert all structure members first
//        int   iTorque045  = TriByteToInt    ( streamData.torque045 );
//        int   iTorque225  = TriByteToInt    ( streamData.torque225 );
//        int   iTension000 = TriByteToInt    ( streamData.tension000 );
//        int   iTension090 = TriByteToInt    ( streamData.tension090 );
//
//        AnsiString sTime = Time().TimeString();
//
        AnsiString sLine;
//        sLine.printf( "%s,%hu,%u,%i,%i,%i,%i,%i,%i,%i,%hi,%hi,%hi,%hi,%hi,%hi\r",
//                      sTime.c_str(), pktHdr.seqNbr, pktHdr.timeStamp * MSecsPerPktTick,
//                      iTorque045, iTorque225, iTension000, iTension090, iTension180, iTension270,
//                      iRotation, siCompassX, siCompassY, siCompassZ, siAccelX, siAccelY, siAccelZ );

        logStream->Write( sLine.c_str(), sLine.Length() );
    }
    catch( ... )
    {
    }

    if( logStream != NULL )
        delete logStream;
}

