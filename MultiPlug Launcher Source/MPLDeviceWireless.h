#ifndef MPLDeviceWirelessH
#define MPLDeviceWirelessH

    //
    // Class Declaration for new WTTTS Sub handler
    //

    #include "MPLDeviceBaseClass.h"
    #include "Hysteresis.h"

    class TWirelessMPLDevice : public TMPLDevice {

      protected:

        bool m_bCleanDoneEventPending;
        bool m_bResetFlagEventPending;

        virtual String GetDevStatus( void );
        virtual bool   GetDevIsIdle( void );
        virtual bool   GetDevIsCalibrated( void );
        virtual bool   GetCanStartCycle( void );
        virtual bool   GetCleaningDone( void );

        void UpdateAveraging( void );
            // Call this function to update averages when a RFC packet is received

      public:

        __fastcall  TWirelessMPLDevice( void );
        __fastcall ~TWirelessMPLDevice();

        bool Connect( const COMMS_CFG& portCfg );

        bool CheckPort( void );

        virtual bool Update( void );

        virtual void RefreshSettings( void );
            // Called to refresh any registry-based settings. Causes those settings
            // to be re-read from the registry.

        virtual bool LaunchPlug( int iPlug  );
            // Activates the launch mechanism for the passed plug

        virtual void AckPlugDetected( void );
            // Called by the client when it has ack'd the presence of a plug.
            // Will cause PlugDetected to go false, and PlugDetected won't go
            // true again until a new plug is detected

        virtual bool CleanPlug( int iPlug );
            // Activates the cleaning mechanism for the passed plug

        virtual bool ActivateResetFlag( void );
            // Activates the reset flag solenoid

        virtual bool GetLastAvgReading   ( MPL_READING&  lastPkt );
        virtual bool GetLastMinMaxReading( MIN_MAX_DATA& lastPkt );
            // Returns the last readings

        virtual bool ShowLastRFCPkt( TStringList* pCaptions, TStringList* pValues );
        virtual bool GetLastRFCPkt ( TDateTime& tPktTime, DWORD& rfcCount, GENERIC_MPL_RFC_PKT& rftPkt );

        virtual bool ClearPIBSwitchOn   ( int iPIBSwitch );
        virtual bool ClearSensorSwitchOn( BYTE bySensorAddr, int iSwitch );

        bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues );
        bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
        bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
            // Note: users should not rely on the items in the above string lists
            // being in the same order as the calibration enum list above.

        bool ReloadCalDataFromDevice( void );
        bool WriteCalDataToDevice( void );

        bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo );
        bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo );

      private:

        // We use different comm object routines for UDP comms - these flags track that info
        bool       m_bUsingUDP;    // True if we're using UDP comms
        AnsiString m_sMPLDevAddr;  // If using UDP, this is the IP addr from which the last event was received
        WORD       m_wMPLDevPort;  // If using UDP, this is the port from which the last event was received

        DWORD      m_dwLastClearSwitchCmdTime;   // Last time that a clear switch command was sent

        typedef enum {
            LS_CLOSED,         // Comm port closed
            LS_HUNTING,        // Waiting for first RFC command from sub
            LS_CFG_DOWNLOAD,   // Downloading config data from device
            LS_CFG_UPLOAD,     // Uploading config data to device
            LS_IDLE,           // Idle, responding to sub RFC requests with 'no command'
            NBR_LINK_STATES
        } LINK_STATE;

        LINK_STATE m_linkState;

        // WTTTS hardware info
        BYTE m_cfgStraps;
        BYTE m_firmwareVer[WTTTS_FW_VER_LEN];

        void ClearHardwareInfo( void );

        // Command management
        typedef enum {
            MCT_NO_COMMAND,
            MCT_SET_RATE_CMD,
            MCT_REQUEST_VER_INFO,
            MCT_SET_RF_CHANNEL,
            MCT_GET_CFG_PAGE,
            MCT_SET_CFG_PAGE,
            MCT_ENTER_DEEP_SLEEP,
            MCT_GET_MIN_MAX_VALS,
            MCT_SET_PIB_CONTACTS,
            MCT_SET_PIB_SOLENOID,
            MCT_SET_SENSOR_PARAMS,
            MCT_CLEAR_SWITCH,
            NBR_MPL_CMD_TYPES
        } MPL_CMD_TYPE;

        // Create an array of the sub commands. For no payload commands, the
        // cached data can be sent as is.
        typedef struct {
            bool  needsPayload;
            DWORD cmdLen;
            BYTE  byTxData[MAX_MPL_PKT_LEN];
        } CACHED_CMD_PKT;

        CACHED_CMD_PKT m_cachedCmds[NBR_MPL_CMD_TYPES];

        void InitMPLCommandPkt( MPL_CMD_TYPE cmdType );
        bool SendMPLCommand( MPL_CMD_TYPE aCmd, const MPL_DATA_UNION* pPayload = NULL );

        // Receive data vars
        BYTE* m_rxBuffer;              // received data holding buffer
        DWORD m_rxBuffCount;           // number of bytes currently in the buffer
        DWORD m_lastRxByteTime;        // time (tick count) of last byte received in buffer
        DWORD m_lastRxPktTime;         // time last valid packet of any type received from sub

        struct {
            DWORD               rfcCount;
            DWORD               dwRFCTick;   // time (tick count) of last RFC from device
            TDateTime           tRFCTime;    // time_t of last RFC from device
            WTTTS_MPL_RFC_PKT   rawRFCPkt;   // Raw RFC packet
            GENERIC_MPL_RFC_PKT rfcPkt;      // Converted RFC packet
        } m_lastRFC;

        void ResetRFCControlStruct( void );
        void CreateGenericRFC( GENERIC_MPL_RFC_PKT& rfcPkt, const WTTTS_MPL_RFC_PKT& rawRFCPkt );

        // Battery averaging
        struct {
            float        fLastBattVolts;
            int          iLastBattUsed;
            float        fValidThresh;
            float        fMinOperThresh;
            float        fGoodThresh;
            TExpAverage* avgBattVolts;
            TExpAverage* avgBattUsed;
        } m_batteryInfo;

        void ClearBatteryVolts( void );
        void UpdateBatteryVolts( bool bSolenoidActive );

        // MPL plug detection
        TIntHysteresis* m_plugState;

        void CheckForPlug( void );

        // The last reading struct is used to save a MPL_READING for
        // later access by the polling thread. This struct cannot be
        // used for saving a MPL_READING for any other purpose!
        struct {
            bool        haveMPLReading;
            MPL_READING mplReading;
        } m_lastReading;

        void CreateMPLReading( MPL_READING& mplReading, TDateTime tReadingTime, const GENERIC_MPL_RFC_PKT& rfcPkt );

        void UpdateSwitches( void );

        bool TimeParamsGood( void );
        bool SwitchesUpToDate( void );
        bool SolenoidsUpToDate( void );

        // Configuration data support
        BYTE m_rawCfgData[NBR_WTTTS_CFG_PAGES * NBR_BYTES_PER_WTTTS_PAGE];

        typedef struct {
            bool  havePage;
            int   lastCfgReqCycle;
            bool  setPending;
        } CFG_PAGE_STATUS;

        static const MPL_CAL_DATA_STRUCT_V1 defaultMPLCalData;

        // Used to prevent multiple requests for each config page
        int m_currCfgReqCycle;

        CFG_PAGE_STATUS m_cfgPgStatus[NBR_WTTTS_CFG_PAGES];

        void ClearConfigurationData( void );
        void CheckCalDataVersion( void );

        // Logging
        void LogRawRFCPacket ( const WTTTS_PKT_HDR& pktHdr, const WTTTS_MPL_RFC_PKT&   rfcPkt );
        void LogConvRFCPacket( const WTTTS_PKT_HDR& pktHdr, const GENERIC_MPL_RFC_PKT& rfcPkt );
    };

#endif
