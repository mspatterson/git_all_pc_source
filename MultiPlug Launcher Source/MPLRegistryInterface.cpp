#include <vcl.h>
#pragma hdrstop

#include <registry.hpp>

#include "MPLRegistryInterface.h"
#include "ApplUtils.h"

#pragma package(smart_init)


//
// Private Registry Defines
//

static const UnicodeString m_BootSection( "Boot" );
    static const UnicodeString m_LastShutdown     ( "LastShutdown" );
    static const UnicodeString m_ProcPriority     ( "ProcessPriority" );

static const UnicodeString m_JobsMRUSection( "MostRecentJobs" );
    static const UnicodeString m_MRUItem          ( "LastJob%d" );

static const UnicodeString m_RecentNameSection( "RecentNames %d" );
    static const UnicodeString m_recentNameItem   ( "Name %d" );

static const UnicodeString m_SettingsSection( "Settings" );
    static const UnicodeString m_JobsDir            ( "DataDir" );
    static const UnicodeString m_DefaultUOM         ( "Default UOM" );
    static const UnicodeString m_Password           ( "WindowAttributes" );
    static const UnicodeString m_AvgSettingMethod   ( "Avg %d Method" );
    static const UnicodeString m_AvgSettingParam    ( "Avg %d Param" );
    static const UnicodeString m_AvgSettingVariance ( "Avg %d Variance" );
    static const UnicodeString m_ForceBackupSave    ( "Force Backup" );
    static const UnicodeString m_ForceBackupSaveDir ( "Force Backup Dir" );
    static const UnicodeString m_LaunchClkTimeout   ( "Launch Click Timeout" );
    static const UnicodeString m_SolenoidOnTime     ( "Solenoid On Time %d" );
    static const UnicodeString m_PlugDetectTimeout  ( "Plug Detect Timeout" );
    static const UnicodeString m_BattValidThresh    ( "Battery Valid Threshold" );
    static const UnicodeString m_BattMinOperV       ( "Battery Min Oper Volts" );
    static const UnicodeString m_BattGoodV          ( "Battery Good Volts" );

static const UnicodeString m_CommsSection( "Comms Settings" );
    static const UnicodeString m_devType          ( "Device Type" );
    static const UnicodeString m_portType         ( "Port %d Type" );
    static const UnicodeString m_portName         ( "Port %d Name" );
    static const UnicodeString m_portParam        ( "Port %d Param" );
    static const UnicodeString m_autoAssignPorts  ( "AutoAssignPorts" );
    static const UnicodeString m_radioChanTimeout ( "Radio Chan Timeout" );
    static const UnicodeString m_defaultRadioChan ( "Default Radio Chan" );


#define NBR_MRU_JOBS  4


static TRegistryIniFile* CreateRegIni( void );
static TRegistryIniFile* CreateSharedRegIni( void );
    // Creates a reg ini file object. Caller is responsible for releasing it


// Some properties only persist through the execution of the application.
// Those are stored here.

static bool          m_loggingEnabled[NBR_DATA_LOG_TYPES] = { false, false };
static UnicodeString m_logFileName[NBR_DATA_LOG_TYPES]    = { "",    ""    };


//
// Public Functions
//

SHUT_DOWN_TYPE GetLastShutdown( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    SHUT_DOWN_TYPE sdtReturn = (SHUT_DOWN_TYPE)(regIni->ReadInteger( m_BootSection, m_LastShutdown, SDT_NEW ) );

    delete regIni;

    return sdtReturn;
}


void SetProgramInUse( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_LastShutdown, SDT_CRASH );

    delete regIni;
}


void SetProgramShutdownGood( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_LastShutdown, SDT_GOOD );

    delete regIni;
}


DWORD GetProcessPriority( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    DWORD procPriority = regIni->ReadInteger( m_BootSection, m_ProcPriority, NORMAL_PRIORITY_CLASS );

    delete regIni;

    return procPriority;
}


void SetProcessPriority( DWORD newPriority )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_ProcPriority, newPriority );

    delete regIni;
}


void GetMostRecentJobsList( TStrings* slJobList )
{
    if( slJobList == NULL )
        return;

    slJobList->Clear();

    TRegistryIniFile* regIni = CreateRegIni();

    // Add strings from the MRU list until we've either max'd out or
    // we encounter the first empty string.
    for( int iItem = 0; iItem < NBR_MRU_JOBS; iItem++ )
    {
        UnicodeString sKey;
        sKey.printf( m_MRUItem.w_str(), iItem + 1 );

        UnicodeString sEntry = regIni->ReadString( m_JobsMRUSection, sKey, L"" );

        if( sEntry.Length() == 0 )
            break;

        if( FileExists( sEntry ) )
            slJobList->Add( sEntry );

    }

    delete regIni;
}


UnicodeString GetMostRecentJob( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    // Return the most recent entry from the MRU list
    UnicodeString sKey;
    sKey.printf( m_MRUItem.w_str(), 1 );

    UnicodeString sEntry = regIni->ReadString( m_JobsMRUSection, sKey, L"" );

    delete regIni;

    return sEntry;
}


void AddToMostRecentJobsList( UnicodeString sNewJob )
{
    TRegistryIniFile* regIni = CreateRegIni();

    // Copy all of the existing strings in order to a string list. It
    // will be easier to work with than the actual registry entries.
    TStringList* mruStrings = new TStringList();

    GetMostRecentJobsList( mruStrings );

    // Check if the entry exists. If it does, make sure it is the topmost
    // entry in the list
    int currIndex = mruStrings->IndexOf( sNewJob );

    if( currIndex > 0 )
    {
        // The string exists, but is not the top-most entry. Delete it
        // from its current position, then add it back at the top of
        // the list.
        mruStrings->Delete( currIndex );
        mruStrings->Insert( 0, sNewJob );
    }
    else if( currIndex < 0 )
    {
        // The string does not exist. Insert it at the top of the
        // list. We don't need to delete items in this case because
        // of how we store the strings back in the registry below.
        mruStrings->Insert( 0, sNewJob );
    }
    else
    {
        // currIndex == 0 in this case, meaning the string exists and
        // is at the top of list. No work needs to be done.
    }

    // mruStrings has been setup. Now re-create the section. First,
    // delete all existing entries.
    regIni->EraseSection( m_JobsMRUSection );

    // Now add all items to the section
    for( int iItem = 0; iItem < mruStrings->Count; iItem++ )
    {
        // Only add up to NBR_MRU_JOBS items
        if( iItem >= NBR_MRU_JOBS )
            break;

        // Now add this job to the top of the list
        UnicodeString sKey;
        sKey.printf( m_MRUItem.w_str(), iItem + 1 );

        regIni->WriteString( m_JobsMRUSection, sKey, mruStrings->Strings[iItem] );
    }

    delete mruStrings;
    delete regIni;
}


UnicodeString GetJobDataDir( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    UnicodeString sReturn = regIni->ReadString( m_SettingsSection, m_JobsDir, L"" );

    delete regIni;

    return sReturn;
}


void SetJobDataDir( UnicodeString newDir )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteString( m_SettingsSection, m_JobsDir, newDir );

    delete regIni;
}


int GetDefaultUnitsOfMeasure( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    int iResult = regIni->ReadInteger( m_SettingsSection, m_DefaultUOM, 1 /*imperial*/ );

    delete regIni;

    return iResult;
}


void SetDefaultUnitsOfMeasure( int newDefaultUOM )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_SettingsSection, m_DefaultUOM, newDefaultUOM );

    delete regIni;
}


void GetAveragingSettings( AVG_TYPE avgType, AVERAGING_PARAMS& avgParams )
{
    // Set default return values first
    avgParams.avgMethod   = AM_NONE;
    avgParams.avgLevel    = AL_OFF;
    avgParams.param       = 1.0;
    avgParams.absVariance = 0.0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_AvgSettingMethod.w_str(), avgType );
        avgParams.avgMethod = regIni->ReadInteger( m_SettingsSection, sKey, avgParams.avgMethod );

        sKey.printf( m_AvgSettingParam.w_str(), avgType );
        avgParams.param = regIni->ReadFloat( m_SettingsSection, sKey, avgParams.param );

        sKey.printf( m_AvgSettingVariance.w_str(), avgType );
        avgParams.absVariance = regIni->ReadFloat( m_SettingsSection, sKey, avgParams.absVariance );

        delete regIni;
    }
}


void SaveAveragingSettings( AVG_TYPE avgType, const AVERAGING_PARAMS& avgParams )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_AvgSettingMethod.w_str(), avgType );
        regIni->WriteInteger( m_SettingsSection, sKey, avgParams.avgMethod );

        sKey.printf( m_AvgSettingParam.w_str(), avgType );
        regIni->WriteFloat( m_SettingsSection, sKey, avgParams.param );

        sKey.printf( m_AvgSettingVariance.w_str(), avgType );
        regIni->WriteFloat( m_SettingsSection, sKey, avgParams.absVariance );

        delete regIni;
    }
}


UnicodeString GetSysAdminPassword( void )
{
    // Initialize return result with default password
    UnicodeString sPassword( "tesco" );

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TMemoryStream* pStream = new TMemoryStream();

        if( pStream != NULL )
        {
            int bytesRead = regIni->ReadBinaryStream( m_SettingsSection, m_Password, pStream );

            if( bytesRead > 0 )
            {
                // Decode the password. Byte values are xor'd with 0x85
                AnsiString sTemp;
                sTemp.SetLength( bytesRead );

                BYTE* pData = (BYTE*)( pStream->Memory );

                for( int iByte = 0; iByte < bytesRead; iByte++ )
                    sTemp[iByte+1] = (char)( pData[iByte] ^ 0x85 );

                sPassword = sTemp;
            }

            delete pStream;

        }

        delete regIni;
    }

    return sPassword;
}


void SaveSysAdminPassword( UnicodeString newPW )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TMemoryStream* pStream = new TMemoryStream();

        if( pStream != NULL )
        {
            AnsiString sTemp( newPW );

            pStream->SetSize( sTemp.Length() );

            BYTE* pData = (BYTE*)( pStream->Memory );

            for( int iByte = 0; iByte < sTemp.Length(); iByte++ )
                pData[iByte] = 0x85 ^ sTemp[iByte+1];

            pStream->Position = 0;

            regIni->WriteBinaryStream( m_SettingsSection, m_Password, pStream );

            delete pStream;
        }

        delete regIni;
    }
}


int GetMPLDeviceType( int iDefaultType )
{
    int devType = iDefaultType;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        devType = regIni->ReadInteger( m_CommsSection, m_devType, devType );
        delete regIni;
    }

    return devType;
}


void SaveMPLDeviceType( int iNewType )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_CommsSection, m_devType, iNewType );
        delete regIni;
    }
}


void GetCommSettings( DEV_COMM_TYPE devType, COMMS_CFG& commCfg )
{
    // Init defaults
    commCfg.portType  = CT_UNUSED;
    commCfg.portName  = "";
    commCfg.portParam = 0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sPortTypeKey;
        UnicodeString sPortNameKey;
        UnicodeString sPortParamKey;

        sPortTypeKey.printf ( m_portType.w_str(),  devType );
        sPortNameKey.printf ( m_portName.w_str(),  devType );
        sPortParamKey.printf( m_portParam.w_str(), devType );

        commCfg.portType  = (COMM_TYPE) regIni->ReadInteger( m_CommsSection, sPortTypeKey,  commCfg.portType );
        commCfg.portName  =             regIni->ReadString ( m_CommsSection, sPortNameKey,  commCfg.portName );
        commCfg.portParam =             regIni->ReadInteger( m_CommsSection, sPortParamKey, commCfg.portParam );

        delete regIni;
    }
}


void SaveCommSettings( DEV_COMM_TYPE devType, const COMMS_CFG& commCfg )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sPortTypeKey;
        UnicodeString sPortNameKey;
        UnicodeString sPortParamKey;

        sPortTypeKey.printf ( m_portType.w_str(),  devType );
        sPortNameKey.printf ( m_portName.w_str(),  devType );
        sPortParamKey.printf( m_portParam.w_str(), devType );

        regIni->WriteInteger( m_CommsSection, sPortTypeKey,  commCfg.portType );
        regIni->WriteString ( m_CommsSection, sPortNameKey,  commCfg.portName );
        regIni->WriteInteger( m_CommsSection, sPortParamKey, commCfg.portParam );

        delete regIni;
    }
}


bool GetAutoAssignCommPorts( void )
{
    // Init defaults
    bool bAutoAssignPorts = true;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bAutoAssignPorts = regIni->ReadBool( m_CommsSection, m_autoAssignPorts, bAutoAssignPorts );

        delete regIni;
    }

    return bAutoAssignPorts;
}


void SaveAutoAssignCommPorts( bool bAutoAssignPorts )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool( m_CommsSection, m_autoAssignPorts, bAutoAssignPorts );

        delete regIni;
    }
}


DWORD GetRadioChanWaitTimeout( void )
{
    // Init defaults
    DWORD dwTimeout = 30000;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        dwTimeout = regIni->ReadInteger( m_CommsSection, m_radioChanTimeout, dwTimeout );

        delete regIni;
    }

    return dwTimeout;
}


void SaveRadioChanWaitTimeout( DWORD dwNewTimeout )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_CommsSection, m_radioChanTimeout, dwNewTimeout );

        delete regIni;
    }
}


int GetDefaultRadioChan( void )
{
    // Init defaults
    int iDefaultChan = 20;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        iDefaultChan = regIni->ReadInteger( m_CommsSection, m_defaultRadioChan, iDefaultChan );

        delete regIni;
    }

    return iDefaultChan;
}


DWORD GetLaunchClickTimeout( void )
{
    // Init defaults
    DWORD dwTimeout = 4000;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        dwTimeout = regIni->ReadInteger( m_SettingsSection, m_LaunchClkTimeout, dwTimeout );

        delete regIni;
    }

    return dwTimeout;
}


void SaveLaunchClickTimeout( DWORD dwNewTimeout )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_SettingsSection, m_LaunchClkTimeout, dwNewTimeout );

        delete regIni;
    }
}


DWORD GetPlugDetectTimeout( void )
{
    // Init defaults
    DWORD dwTimeout = 60000;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        dwTimeout = regIni->ReadInteger( m_SettingsSection, m_PlugDetectTimeout, dwTimeout );

        delete regIni;
    }

    return dwTimeout;
}


void SavePlugDetectTimeout( DWORD dwNewTimeout )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_SettingsSection, m_PlugDetectTimeout, dwNewTimeout );

        delete regIni;
    }
}


DWORD GetSolenoidOnTime( SOLENOID_ON_TIME_TYPE eType )
{
    // Init defaults
    DWORD dwOnTime = 10000;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sKey;
        sKey.printf( m_SolenoidOnTime.w_str(), eType );

        dwOnTime = regIni->ReadInteger( m_SettingsSection, sKey, dwOnTime );

        delete regIni;
    }

    return dwOnTime;
}


void SaveSolenoidOnTime( SOLENOID_ON_TIME_TYPE eType, DWORD dwNewTime )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sKey;
        sKey.printf( m_SolenoidOnTime.w_str(), eType );

        regIni->WriteInteger( m_SettingsSection, sKey, dwNewTime );

        delete regIni;
    }
}


bool GetDataLogging( DATA_LOG_TYPE logType, UnicodeString& logFileName )
{
    // Non-persistent property
    logFileName = m_logFileName[logType];

    return m_loggingEnabled[logType];
}


void SaveDataLogging( DATA_LOG_TYPE logType, bool loggingOn, UnicodeString logFileName )
{
    // Non-persistent property
    m_logFileName[logType]    = logFileName;
    m_loggingEnabled[logType] = loggingOn;
}


void GetRecentNameList( RECENT_NAME_LIST aList, TStringList* nameList )
{
    if( nameList == NULL )
        return;

    nameList->Clear();
    nameList->Sorted = false;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;
        sKey.printf( m_RecentNameSection.w_str(), aList );

        for( int iItem = 0; iItem < 30; iItem++ )
        {
           UnicodeString sItem;
           sItem.printf( m_recentNameItem.w_str(), iItem );

           UnicodeString sEntry = regIni->ReadString( sKey, sItem, L"" );

            if( sEntry.Length() == 0 )
                break;

            nameList->Add( sEntry );
        }

        delete regIni;
    }
}


void SaveRecentName( RECENT_NAME_LIST aList, const UnicodeString newName )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TStringList* currNames = new TStringList;

        // Get current name list on registry for a section
        GetRecentNameList( aList, currNames );

        // If the newName is in the list, delete it.
        int currIndex = currNames->IndexOf( newName );

        if( currIndex >= 0 )
            currNames->Delete( currIndex );

        // Insert to the top of list
        currNames->Insert( 0, newName );

        // Make sure we have no more thean the max items in the list
        const int MaxNames = 30;

        while( currNames->Count > MaxNames )
            currNames->Delete( currNames->Count - 1 );

        // Write the list to section. Erase any current entries first
        UnicodeString sKey;
        sKey.printf( m_RecentNameSection.w_str(), aList );

        regIni->EraseSection( sKey );

        for( int iItem = 0; iItem < currNames->Count; iItem++ )
        {
            UnicodeString sItem;
            sItem.printf( m_recentNameItem.w_str(), iItem );

            regIni->WriteString( sKey, sItem, currNames->Strings[iItem] );
        }

        delete currNames;
        delete regIni;
    }
}


bool GetForceBackup( String& sDir )
{
    bool bResult = false;
    sDir         = "";

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bResult = regIni->ReadBool  ( m_SettingsSection, m_ForceBackupSave,    bResult );
        sDir    = regIni->ReadString( m_SettingsSection, m_ForceBackupSaveDir, sDir    );

        delete regIni;
    }

    return bResult;
}


void SaveForceBackup( bool bNewValue, String sDir )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool  ( m_SettingsSection, m_ForceBackupSave,    bNewValue );
        regIni->WriteString( m_SettingsSection, m_ForceBackupSaveDir, sDir      );

        delete regIni;
    }
}


void GetBatteryThresholds( BATT_THRESH_VALS& battThresh )
{
    // Set defaults
    battThresh.fReadingValidThresh = 2.5;   // Battery readings below this value are ignored, in V
    battThresh.fMinOperValue       = 3.8;   // Operation not allowed below this threshold, in V
    battThresh.fGoodThresh         = 3.9;   // Battery is in a good state at or above this, in V

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        battThresh.fReadingValidThresh = regIni->ReadFloat( m_SettingsSection, m_BattValidThresh, battThresh.fReadingValidThresh );
        battThresh.fMinOperValue       = regIni->ReadFloat( m_SettingsSection, m_BattMinOperV,    battThresh.fMinOperValue       );
        battThresh.fGoodThresh         = regIni->ReadFloat( m_SettingsSection, m_BattGoodV,       battThresh.fGoodThresh         );

        delete regIni;
    }
}


void SaveBatteryThresholds( const BATT_THRESH_VALS& battThresh )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteFloat( m_SettingsSection, m_BattValidThresh, battThresh.fReadingValidThresh );
        regIni->WriteFloat( m_SettingsSection, m_BattMinOperV,    battThresh.fMinOperValue       );
        regIni->WriteFloat( m_SettingsSection, m_BattGoodV,       battThresh.fGoodThresh         );

        delete regIni;
    }
}



//
//  Private Functions
//

TRegistryIniFile* CreateRegIni( void )
{
    // Caller must free the created object
    return new TRegistryIniFile( String( "Software\\Tesco\\MultiPlugLauncher\\V" ) + IntToStr( GetApplMajorVer() ) );
}


TRegistryIniFile* CreateSharedRegIni( void )
{
    // Caller must free the created object
    return new TRegistryIniFile( "Software\\Tesco\\Shared" );
}

