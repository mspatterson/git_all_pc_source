#ifndef MPLRegistryInterfaceH
#define MPLRegistryInterfaceH

    #include "SerialPortUtils.h"
    #include "Averaging.h"

    typedef enum {
        SDT_NEW,       // First time this program has been run
        SDT_GOOD,      // Program has been run before, and closed properly
        SDT_CRASH,     // Program has been run before, but not closed properly
        NBR_SHUT_DOWN_TYPES
    } SHUT_DOWN_TYPE;

    SHUT_DOWN_TYPE GetLastShutdown( void );
    void           SetProgramInUse( void );
    void           SetProgramShutdownGood( void );

    DWORD          GetProcessPriority( void );
    void           SetProcessPriority( DWORD newPriority );

    UnicodeString  GetJobDataDir( void );
    void           SetJobDataDir( UnicodeString newDir );

    void           GetMostRecentJobsList( TStrings* slJobList );
    UnicodeString  GetMostRecentJob( void );
    void           AddToMostRecentJobsList( UnicodeString sNewJob );

    int            GetDefaultUnitsOfMeasure( void );
    void           SetDefaultUnitsOfMeasure( int newDefaultUOM );

    // Data Averaging setting
    typedef enum{
        AT_RPM,
        NBR_AVG_TYPES
    } AVG_TYPE;

    void GetAveragingSettings ( AVG_TYPE avgType,       AVERAGING_PARAMS& avgParams );
    void SaveAveragingSettings( AVG_TYPE avgType, const AVERAGING_PARAMS& avgParams );

    // Misc functions
    UnicodeString  GetSysAdminPassword( void );
    void           SaveSysAdminPassword( UnicodeString newPW );

    // Devices
    int  GetMPLDeviceType( int iDefaultType );
    void SaveMPLDeviceType( int iNewType );

    // Comms settings
    typedef enum {
        DCT_MPL,
        DCT_BASE,
        DCT_MGMT,
        NBR_DEV_COMM_TYPES
    } DEV_COMM_TYPE;

    void GetCommSettings ( DEV_COMM_TYPE devType, COMMS_CFG& commCfg );
    void SaveCommSettings( DEV_COMM_TYPE devType, const COMMS_CFG& commCfg );

    bool GetAutoAssignCommPorts( void );
    void SaveAutoAssignCommPorts( bool bAutoAssignPorts );

    DWORD GetRadioChanWaitTimeout( void );
    void  SaveRadioChanWaitTimeout( DWORD dwNewTimeout );

    int   GetDefaultRadioChan( void );

    DWORD GetLaunchClickTimeout( void );
    void  SaveLaunchClickTimeout( DWORD dwNewTimeout );

    DWORD GetPlugDetectTimeout( void );
    void  SavePlugDetectTimeout( DWORD dwNewTimeout );

    typedef enum {
        SOTT_LAUNCH,
        SOTT_CLEAN,
        SOTT_RESET
    } SOLENOID_ON_TIME_TYPE;

    DWORD GetSolenoidOnTime( SOLENOID_ON_TIME_TYPE eType );
    void  SaveSolenoidOnTime( SOLENOID_ON_TIME_TYPE eType, DWORD dwNewTime );

    typedef enum {
        DLT_RAW_DATA,
        DLT_CAL_DATA,
        NBR_DATA_LOG_TYPES
    } DATA_LOG_TYPE;

    bool           GetDataLogging ( DATA_LOG_TYPE logType, UnicodeString& logFileName );
    void           SaveDataLogging( DATA_LOG_TYPE logType, bool loggingOn, UnicodeString logFileName );

    typedef enum {
        RNL_CLIENT_REP,
        RNL_TESCO_REP,
        RNL_THREAD_REP,
        RNL_MPL_OPERATOR,
        NBR_RECENT_NAME_LISTS
    } RECENT_NAME_LIST;

    void GetRecentNameList( RECENT_NAME_LIST aList, TStringList* nameList );
    void SaveRecentName( RECENT_NAME_LIST aList, const UnicodeString newName );

    bool GetForceBackup( String& sDir );
    void SaveForceBackup( bool bNewValue, String sDir );

    typedef struct {
        float fReadingValidThresh;   // Battery readings below this value are ignored, in V
        float fMinOperValue;         // Operation not allowed below this threshold, in V
        float fGoodThresh;           // Battery is in a good state at or above this, in V
    } BATT_THRESH_VALS;

    void GetBatteryThresholds( BATT_THRESH_VALS& battThresh );
    void SaveBatteryThresholds( const BATT_THRESH_VALS& battThresh );


#endif
