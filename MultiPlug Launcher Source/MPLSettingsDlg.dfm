object SystemSettingsForm: TSystemSettingsForm
  Left = 0
  Top = 0
  Caption = 'MPL System Settings'
  ClientHeight = 525
  ClientWidth = 698
  Color = clBtnFace
  Constraints.MinHeight = 564
  Constraints.MinWidth = 710
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    698
    525)
  PixelsPerInch = 96
  TextHeight = 13
  object PgCtrl: TPageControl
    Left = 8
    Top = 8
    Width = 684
    Height = 478
    ActivePage = MiscSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object CommsSheet: TTabSheet
      Caption = 'Communications'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        676
        450)
      object DevConnPgCtrl: TPageControl
        Left = 3
        Top = 3
        Width = 670
        Height = 444
        ActivePage = MPLSheet
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object MPLSheet: TTabSheet
          Caption = 'MPL'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            662
            416)
          object DataLoggingGB: TGroupBox
            Left = 316
            Top = 12
            Width = 339
            Height = 129
            Anchors = [akTop, akRight]
            Caption = ' Data Logging '
            TabOrder = 1
            object EnableRawLogCB: TCheckBox
              Left = 14
              Top = 24
              Width = 187
              Height = 17
              Caption = 'Enable Raw Data Logging'
              TabOrder = 0
            end
            object RawLogFileNameEdit: TEdit
              Left = 13
              Top = 45
              Width = 273
              Height = 21
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 1
            end
            object BrowseRawLogFileBtn: TButton
              Left = 295
              Top = 42
              Width = 33
              Height = 25
              Caption = '...'
              TabOrder = 2
              OnClick = BrowseRawLogFileBtnClick
            end
            object EnableCalLogCB: TCheckBox
              Left = 14
              Top = 75
              Width = 187
              Height = 17
              Caption = 'Enable Calibrated Data Logging'
              TabOrder = 3
            end
            object CalLogFileNameEdit: TEdit
              Left = 13
              Top = 96
              Width = 273
              Height = 21
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 4
            end
            object BrowseCalLogFileBtn: TButton
              Left = 295
              Top = 93
              Width = 33
              Height = 25
              Caption = '...'
              TabOrder = 5
              OnClick = BrowseCalLogFileBtnClick
            end
          end
          object DiagnosticsGB: TGroupBox
            Left = 12
            Top = 12
            Width = 295
            Height = 400
            Anchors = [akLeft, akTop, akRight, akBottom]
            Caption = ' Diagnostics '
            TabOrder = 0
            DesignSize = (
              295
              400)
            object Label7: TLabel
              Left = 12
              Top = 16
              Width = 43
              Height = 13
              Caption = 'Last RFC'
            end
            object LastRFCEditor: TValueListEditor
              Left = 12
              Top = 32
              Width = 271
              Height = 357
              Anchors = [akLeft, akTop, akRight, akBottom]
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
              TabOrder = 0
              TitleCaptions.Strings = (
                'Property'
                'Value')
              OnSelectCell = ListEditorSelectCell
              ColWidths = (
                188
                77)
            end
          end
          object GroupBox1: TGroupBox
            Left = 316
            Top = 147
            Width = 339
            Height = 54
            Anchors = [akTop, akRight]
            Caption = ' Settings '
            TabOrder = 2
            object Label2: TLabel
              Left = 14
              Top = 23
              Width = 110
              Height = 13
              Caption = 'Radio Channel Timeout'
            end
            object Label3: TLabel
              Left = 295
              Top = 23
              Width = 29
              Height = 13
              Caption = '(secs)'
            end
            object RadioChanTimeoutEdit: TEdit
              Left = 165
              Top = 20
              Width = 121
              Height = 21
              TabOrder = 0
              Text = '30'
            end
          end
        end
        object BaseRadioSheet: TTabSheet
          Caption = 'Base Radio'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            662
            416)
          object BRDiagnosticsGB: TGroupBox
            Left = 12
            Top = 12
            Width = 295
            Height = 400
            Anchors = [akLeft, akTop, akRight, akBottom]
            Caption = ' Diagnostics '
            TabOrder = 0
            DesignSize = (
              295
              400)
            object Label37: TLabel
              Left = 12
              Top = 16
              Width = 54
              Height = 13
              Caption = 'Last Status'
            end
            object LastStatusEditor: TValueListEditor
              Left = 12
              Top = 32
              Width = 271
              Height = 357
              Anchors = [akLeft, akTop, akRight, akBottom]
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
              TabOrder = 0
              TitleCaptions.Strings = (
                'Property'
                'Value')
              OnSelectCell = ListEditorSelectCell
              ColWidths = (
                188
                77)
            end
          end
          object BRCmdGB: TGroupBox
            Left = 316
            Top = 12
            Width = 339
            Height = 66
            Anchors = [akTop, akRight]
            Caption = ' Commands '
            TabOrder = 1
            object Label39: TLabel
              Left = 14
              Top = 31
              Width = 74
              Height = 13
              Caption = 'Set Radio Chan'
            end
            object SendChangeChanBtn: TButton
              Left = 249
              Top = 26
              Width = 75
              Height = 25
              Caption = 'Send'
              TabOrder = 2
              OnClick = SendChangeChanBtnClick
            end
            object BRChanCombo: TComboBox
              Left = 192
              Top = 28
              Width = 51
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 1
              Text = '11'
              Items.Strings = (
                '11'
                '15'
                '20'
                '25')
            end
            object BRRadNbrCombo: TComboBox
              Left = 101
              Top = 28
              Width = 85
              Height = 21
              Style = csDropDownList
              ItemIndex = 1
              TabOrder = 0
              Text = 'MPL'
              Items.Strings = (
                'TesTORK'
                'MPL')
            end
          end
        end
        object PortsSheet: TTabSheet
          Caption = 'Ports'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PortsGB: TGroupBox
            Left = 12
            Top = 12
            Width = 445
            Height = 157
            Caption = ' Ports '
            TabOrder = 0
            object Label19: TLabel
              Left = 16
              Top = 67
              Width = 19
              Height = 13
              Caption = 'MPL'
            end
            object Label21: TLabel
              Left = 16
              Top = 95
              Width = 85
              Height = 13
              Caption = 'Management Port'
            end
            object Label22: TLabel
              Left = 232
              Top = 17
              Width = 69
              Height = 13
              Caption = 'Port / Address'
            end
            object Label23: TLabel
              Left = 338
              Top = 17
              Width = 50
              Height = 13
              Caption = 'Parameter'
            end
            object Label24: TLabel
              Left = 115
              Top = 17
              Width = 47
              Height = 13
              Caption = 'Port Type'
            end
            object Label20: TLabel
              Left = 16
              Top = 39
              Width = 53
              Height = 13
              Caption = 'Base Radio'
            end
            object AutoAssignPortsCB: TCheckBox
              Left = 115
              Top = 127
              Width = 169
              Height = 17
              Caption = 'Automatically Assign Ports'
              TabOrder = 9
              OnClick = AutoAssignPortsCBClick
            end
            object MPLPortTypeCombo: TComboBox
              Left = 115
              Top = 64
              Width = 95
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 3
              Text = 'Serial'
              OnClick = MPLPortTypeComboClick
              Items.Strings = (
                'Serial'
                'UDP'
                'No Device')
            end
            object MgmtDevPortTypeCombo: TComboBox
              Left = 115
              Top = 92
              Width = 95
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 6
              Text = 'Serial'
              OnClick = MgmtDevPortTypeComboClick
              Items.Strings = (
                'Serial'
                'UDP'
                'No Device')
            end
            object MPLPortEdit: TEdit
              Left = 232
              Top = 64
              Width = 85
              Height = 21
              TabOrder = 4
              Text = '1'
            end
            object MPLParamEdit: TEdit
              Left = 338
              Top = 64
              Width = 85
              Height = 21
              TabOrder = 5
              Text = '115200'
            end
            object MgmtDevPortEdit: TEdit
              Left = 232
              Top = 92
              Width = 85
              Height = 21
              TabOrder = 7
              Text = '1'
            end
            object MgmtDevParamEdit: TEdit
              Left = 338
              Top = 91
              Width = 85
              Height = 21
              TabOrder = 8
              Text = '115200'
            end
            object BaseRadioPortTypeCombo: TComboBox
              Left = 115
              Top = 36
              Width = 95
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 0
              Text = 'Serial'
              OnClick = BaseRadioPortTypeComboClick
              Items.Strings = (
                'Serial'
                'UDP'
                'No Device')
            end
            object BaseRadioPortEdit: TEdit
              Left = 232
              Top = 36
              Width = 85
              Height = 21
              TabOrder = 1
              Text = '1'
            end
            object BaseRadioParamEdit: TEdit
              Left = 338
              Top = 36
              Width = 85
              Height = 21
              TabOrder = 2
              Text = '115200'
            end
          end
        end
      end
    end
    object CalSheet: TTabSheet
      Caption = 'Calibration'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        676
        450)
      object CalDataPgCtrl: TPageControl
        Left = 8
        Top = 8
        Width = 398
        Height = 394
        ActivePage = DevMemorySheet
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object DevMemorySheet: TTabSheet
          Caption = 'Device Memory'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            390
            366)
          object FormattedCalDataEditor: TValueListEditor
            Left = 8
            Top = 8
            Width = 374
            Height = 352
            Anchors = [akLeft, akTop, akRight, akBottom]
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
            TabOrder = 0
            TitleCaptions.Strings = (
              'Property'
              'Value')
            OnSelectCell = ListEditorSelectCell
            ColWidths = (
              211
              157)
          end
        end
        object RawDataSheet: TTabSheet
          Caption = 'Raw Data View'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            390
            366)
          object RawCalDataEditor: TValueListEditor
            Left = 8
            Top = 8
            Width = 374
            Height = 352
            Anchors = [akLeft, akTop, akRight, akBottom]
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
            TabOrder = 0
            TitleCaptions.Strings = (
              'Page'
              'Data')
            OnSelectCell = ListEditorSelectCell
            ColWidths = (
              76
              292)
          end
        end
      end
      object LoadCalDatBtn: TButton
        Left = 113
        Top = 413
        Width = 99
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Load From File'
        TabOrder = 1
        OnClick = LoadCalDatBtnClick
      end
      object SaveCalDatBtn: TButton
        Left = 7
        Top = 413
        Width = 99
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Save To File'
        TabOrder = 2
        OnClick = SaveCalDatBtnClick
      end
      object WriteToDevBtn: TButton
        Left = 200
        Top = 413
        Width = 99
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Write To Device'
        TabOrder = 3
        OnClick = WriteToDevBtnClick
      end
      object ReadFromDevBtn: TButton
        Left = 305
        Top = 413
        Width = 99
        Height = 25
        Anchors = [akRight, akBottom]
        Caption = 'Read From Device'
        TabOrder = 4
        OnClick = ReadFromDevBtnClick
      end
      object NewBattBtn: TButton
        Left = 419
        Top = 32
        Width = 99
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'New Battery'
        TabOrder = 5
        OnClick = NewBattBtnClick
      end
    end
    object MiscSheet: TTabSheet
      Caption = 'Misc'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        676
        450)
      object JobDirGB: TGroupBox
        Left = 8
        Top = 8
        Width = 657
        Height = 54
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Job Directory '
        TabOrder = 0
        DesignSize = (
          657
          54)
        object JobDirEdit: TEdit
          Left = 14
          Top = 21
          Width = 591
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 0
        end
        object BrowseJobDirBtn: TButton
          Left = 613
          Top = 19
          Width = 33
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '...'
          TabOrder = 1
          OnClick = BrowseJobDirBtnClick
        end
      end
      object SysAdminPWGB: TGroupBox
        Left = 8
        Top = 68
        Width = 335
        Height = 139
        Caption = ' Sys Admin Password '
        TabOrder = 1
        object Label8: TLabel
          Left = 14
          Top = 24
          Width = 117
          Height = 13
          Caption = 'Enter current password:'
        end
        object Label9: TLabel
          Left = 14
          Top = 51
          Width = 102
          Height = 13
          Caption = 'Enter new password:'
        end
        object Label10: TLabel
          Left = 14
          Top = 78
          Width = 113
          Height = 13
          Caption = 'Confirm new password:'
        end
        object CurrPWEdit: TEdit
          Left = 148
          Top = 21
          Width = 171
          Height = 21
          PasswordChar = '*'
          TabOrder = 0
        end
        object NewPWEdit1: TEdit
          Left = 148
          Top = 48
          Width = 171
          Height = 21
          PasswordChar = '*'
          TabOrder = 1
        end
        object NewPWEdit2: TEdit
          Left = 148
          Top = 75
          Width = 171
          Height = 21
          PasswordChar = '*'
          TabOrder = 2
        end
        object SavePWBtn: TButton
          Left = 244
          Top = 103
          Width = 75
          Height = 25
          Caption = 'Save'
          TabOrder = 3
          OnClick = SavePWBtnClick
        end
      end
      object JobStatusGB: TGroupBox
        Left = 8
        Top = 211
        Width = 335
        Height = 55
        Caption = ' Job Status '
        TabOrder = 2
        object CompleteJobBtn: TButton
          Left = 12
          Top = 19
          Width = 95
          Height = 25
          Caption = 'Complete Job'
          TabOrder = 0
          OnClick = CompleteJobBtnClick
        end
        object ReopenJobBtn: TButton
          Left = 117
          Top = 19
          Width = 95
          Height = 25
          Caption = 'Re-Open Job'
          TabOrder = 1
          OnClick = ReopenJobBtnClick
        end
      end
      object PriorityGB: TGroupBox
        Left = 8
        Top = 354
        Width = 335
        Height = 55
        Caption = ' Process Priority '
        TabOrder = 4
        object ProcPriorityCombo: TComboBox
          Left = 11
          Top = 21
          Width = 199
          Height = 21
          Style = csDropDownList
          TabOrder = 0
        end
      end
      object AvgingGB: TGroupBox
        Left = 353
        Top = 70
        Width = 312
        Height = 85
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Sensor Averaging '
        TabOrder = 5
        object TorqueLB: TLabel
          Left = 14
          Top = 24
          Width = 63
          Height = 13
          Caption = 'Sensor Alpha'
        end
        object Label1: TLabel
          Left = 14
          Top = 56
          Width = 48
          Height = 13
          Caption = 'RPM Type'
        end
        object SensorAlphaCombo: TComboBox
          Left = 84
          Top = 21
          Width = 101
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = '1'
          Items.Strings = (
            '1'
            '2'
            '4'
            '8'
            '16'
            '32'
            '64'
            '128')
        end
        object RPMAvgCombo: TComboBox
          Left = 84
          Top = 53
          Width = 101
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = '1'
          OnClick = RPMAvgComboClick
          Items.Strings = (
            '1'
            '2'
            '4'
            '8'
            '16'
            '32'
            '64'
            '128')
        end
        object RPMAvgParam: TEdit
          Left = 200
          Top = 53
          Width = 98
          Height = 21
          TabOrder = 2
          Text = '1'
        end
      end
      object BackupSaveGB: TGroupBox
        Left = 8
        Top = 270
        Width = 335
        Height = 80
        Caption = ' Backup Save Directory '
        TabOrder = 3
        DesignSize = (
          335
          80)
        object BackupSaveCB: TCheckBox
          Left = 14
          Top = 47
          Width = 148
          Height = 17
          Caption = 'Force backup save on exit'
          TabOrder = 0
        end
        object BackupDirEdit: TEdit
          Left = 14
          Top = 20
          Width = 269
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 1
        end
        object BrowseBackupDirBtn: TButton
          Left = 291
          Top = 18
          Width = 33
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '...'
          TabOrder = 2
          OnClick = BrowseBackupDirBtnClick
        end
      end
      object LaunchParamsGB: TGroupBox
        Left = 353
        Top = 160
        Width = 312
        Height = 163
        Anchors = [akLeft, akTop, akRight]
        Caption = ' Launch Parameters  '
        TabOrder = 6
        object Label4: TLabel
          Left = 14
          Top = 51
          Width = 133
          Height = 13
          Caption = 'Plug Launch Duration (secs)'
        end
        object Label5: TLabel
          Left = 14
          Top = 134
          Width = 142
          Height = 13
          Caption = 'Plug Detection Timeout (secs)'
        end
        object Label13: TLabel
          Left = 14
          Top = 24
          Width = 166
          Height = 13
          Caption = 'Launch Button Click Timeout (secs)'
        end
        object Label6: TLabel
          Left = 14
          Top = 78
          Width = 140
          Height = 13
          Caption = 'Plug Cleaning Duration (secs)'
        end
        object Label15: TLabel
          Left = 14
          Top = 106
          Width = 127
          Height = 13
          Caption = 'Reset Flag Duration (secs)'
        end
        object DetectTimeoutEdit: TEdit
          Left = 200
          Top = 131
          Width = 98
          Height = 21
          TabOrder = 3
          Text = '1'
        end
        object LaunchDurEdit: TEdit
          Left = 200
          Top = 48
          Width = 98
          Height = 21
          TabOrder = 1
          Text = '1'
        end
        object LaunchClkTimeoutEdit: TEdit
          Left = 200
          Top = 21
          Width = 98
          Height = 21
          TabOrder = 0
          Text = '1'
        end
        object CleanDurEdit: TEdit
          Left = 200
          Top = 75
          Width = 98
          Height = 21
          TabOrder = 2
          Text = '1'
        end
        object ResetFlagDurEdit: TEdit
          Left = 200
          Top = 103
          Width = 98
          Height = 21
          TabOrder = 4
          Text = '1'
        end
      end
      object GroupBox2: TGroupBox
        Left = 353
        Top = 330
        Width = 312
        Height = 107
        Caption = ' Battery Thresholds '
        TabOrder = 7
        object Label11: TLabel
          Left = 14
          Top = 24
          Width = 124
          Height = 13
          Caption = 'Minimum Valid Reading (V)'
        end
        object Label12: TLabel
          Left = 14
          Top = 51
          Width = 147
          Height = 13
          Caption = 'Minimum Operating Voltage (V)'
        end
        object Label14: TLabel
          Left = 14
          Top = 78
          Width = 131
          Height = 13
          Caption = 'Good Voltage Threshold (V)'
        end
        object BattMinValidVoltsEdit: TEdit
          Left = 200
          Top = 21
          Width = 98
          Height = 21
          TabOrder = 0
          Text = '1'
        end
        object BattMinOperVoltsEdit: TEdit
          Left = 200
          Top = 48
          Width = 98
          Height = 21
          TabOrder = 1
          Text = '1'
        end
        object BattGoodVoltsEdit: TEdit
          Left = 200
          Top = 75
          Width = 98
          Height = 21
          TabOrder = 2
          Text = '1'
        end
      end
    end
  end
  object OKBtn: TButton
    Left = 525
    Top = 494
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'O&K'
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 615
    Top = 494
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object SaveLogDlg: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All File (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Log File As...'
    Left = 608
    Top = 8
  end
  object RFCPollTimer: TTimer
    Enabled = False
    OnTimer = RFCPollTimerTimer
    Left = 552
    Top = 8
  end
  object SaveCalDataDlg: TSaveDialog
    DefaultExt = 'ini'
    Filter = 'Calibrated Data Files (*.ini)|*.ini|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Calibration Data to File'
    Left = 448
    Top = 8
  end
  object OpenCalDataDlg: TOpenDialog
    DefaultExt = 'ini'
    Filter = 'Calibrated Data Files (*.ini)|*.ini|All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Load Calibration Data from File'
    Left = 504
    Top = 8
  end
end
