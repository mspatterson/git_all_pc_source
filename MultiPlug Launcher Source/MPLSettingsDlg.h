#ifndef MPLSettingsDlgH
#define MPLSettingsDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.Menus.hpp>

#include "TPlugJob.h"
#include "CommsMgr.h"
#include "MPLRegistryInterface.h"


class TSystemSettingsForm : public TForm
{
__published:    // IDE-managed Components
    TPageControl *PgCtrl;
    TButton *OKBtn;
    TButton *CancelBtn;
    TTabSheet *CommsSheet;
    TTabSheet *MiscSheet;
    TSaveDialog *SaveLogDlg;
    TGroupBox *JobDirGB;
    TEdit *JobDirEdit;
    TButton *BrowseJobDirBtn;
    TGroupBox *SysAdminPWGB;
    TLabel *Label8;
    TLabel *Label9;
    TLabel *Label10;
    TEdit *CurrPWEdit;
    TEdit *NewPWEdit1;
    TEdit *NewPWEdit2;
    TButton *SavePWBtn;
    TTimer *RFCPollTimer;
    TGroupBox *JobStatusGB;
    TButton *CompleteJobBtn;
    TButton *ReopenJobBtn;
    TPageControl *DevConnPgCtrl;
    TTabSheet *MPLSheet;
    TTabSheet *BaseRadioSheet;
    TGroupBox *DataLoggingGB;
    TCheckBox *EnableRawLogCB;
    TEdit *RawLogFileNameEdit;
    TButton *BrowseRawLogFileBtn;
    TCheckBox *EnableCalLogCB;
    TEdit *CalLogFileNameEdit;
    TButton *BrowseCalLogFileBtn;
    TGroupBox *DiagnosticsGB;
    TLabel *Label7;
    TValueListEditor *LastRFCEditor;
    TGroupBox *BRDiagnosticsGB;
    TLabel *Label37;
    TGroupBox *BRCmdGB;
    TButton *SendChangeChanBtn;
    TComboBox *BRChanCombo;
    TLabel *Label39;
    TValueListEditor *LastStatusEditor;
    TComboBox *BRRadNbrCombo;
    TGroupBox *PriorityGB;
    TComboBox *ProcPriorityCombo;
    TGroupBox *AvgingGB;
    TLabel *TorqueLB;
    TTabSheet *PortsSheet;
    TGroupBox *PortsGB;
    TLabel *Label19;
    TCheckBox *AutoAssignPortsCB;
    TLabel *Label21;
    TLabel *Label22;
    TLabel *Label23;
    TComboBox *MPLPortTypeCombo;
    TComboBox *MgmtDevPortTypeCombo;
    TEdit *MPLPortEdit;
    TEdit *MPLParamEdit;
    TEdit *MgmtDevPortEdit;
    TEdit *MgmtDevParamEdit;
    TLabel *Label24;
    TLabel *Label20;
    TComboBox *BaseRadioPortTypeCombo;
    TEdit *BaseRadioPortEdit;
    TEdit *BaseRadioParamEdit;
    TGroupBox *BackupSaveGB;
    TCheckBox *BackupSaveCB;
    TEdit *BackupDirEdit;
    TButton *BrowseBackupDirBtn;
    TComboBox *SensorAlphaCombo;
    TLabel *Label1;
    TComboBox *RPMAvgCombo;
    TEdit *RPMAvgParam;
    TTabSheet *CalSheet;
    TPageControl *CalDataPgCtrl;
    TTabSheet *DevMemorySheet;
    TValueListEditor *FormattedCalDataEditor;
    TTabSheet *RawDataSheet;
    TValueListEditor *RawCalDataEditor;
    TButton *LoadCalDatBtn;
    TButton *SaveCalDatBtn;
    TButton *WriteToDevBtn;
    TButton *ReadFromDevBtn;
    TButton *NewBattBtn;
    TSaveDialog *SaveCalDataDlg;
    TOpenDialog *OpenCalDataDlg;
    TGroupBox *GroupBox1;
    TLabel *Label2;
    TEdit *RadioChanTimeoutEdit;
    TLabel *Label3;
    TGroupBox *LaunchParamsGB;
    TLabel *Label4;
    TLabel *Label5;
    TEdit *DetectTimeoutEdit;
    TEdit *LaunchDurEdit;
    TLabel *Label13;
    TEdit *LaunchClkTimeoutEdit;
    TEdit *CleanDurEdit;
    TLabel *Label6;
    TGroupBox *GroupBox2;
    TLabel *Label11;
    TEdit *BattMinValidVoltsEdit;
    TLabel *Label12;
    TEdit *BattMinOperVoltsEdit;
    TLabel *Label14;
    TEdit *BattGoodVoltsEdit;
    TLabel *Label15;
    TEdit *ResetFlagDurEdit;
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall CommTypeClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall BrowseRawLogFileBtnClick(TObject *Sender);
    void __fastcall SavePWBtnClick(TObject *Sender);
    void __fastcall BrowseJobDirBtnClick(TObject *Sender);
    void __fastcall BrowseCalLogFileBtnClick(TObject *Sender);
    void __fastcall RFCPollTimerTimer(TObject *Sender);
    void __fastcall ListEditorSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
    void __fastcall SaveCalDatBtnClick(TObject *Sender);
    void __fastcall LoadCalDatBtnClick(TObject *Sender);
    void __fastcall WriteToDevBtnClick(TObject *Sender);
    void __fastcall ReadFromDevBtnClick(TObject *Sender);
    void __fastcall CompleteJobBtnClick(TObject *Sender);
    void __fastcall ReopenJobBtnClick(TObject *Sender);
    void __fastcall SendChangeChanBtnClick(TObject *Sender);
    void __fastcall NewBattBtnClick(TObject *Sender);
    void __fastcall AutoAssignPortsCBClick(TObject *Sender);
    void __fastcall BrowseBackupDirBtnClick(TObject *Sender);
    void __fastcall BaseRadioPortTypeComboClick(TObject *Sender);
    void __fastcall MPLPortTypeComboClick(TObject *Sender);
    void __fastcall MgmtDevPortTypeComboClick(TObject *Sender);
    void __fastcall RPMAvgComboClick(TObject *Sender);

private:
    TCommPoller* m_poller;
    TPlugJob*    m_currJob;

    DWORD        m_dwSettingsChanged;

    UNITS_OF_MEASURE m_uomType;

    typedef struct {
        TComboBox* pTypeCombo;
        TEdit*     pPortNbrEdit;
        TEdit*     pParamEdit;
    } COMMS_SHEET_CTRLS;

    COMMS_SHEET_CTRLS m_MPLCommsCtrls;
    COMMS_SHEET_CTRLS m_BaseCommsCtrls;
    COMMS_SHEET_CTRLS m_MgmtCommsCtrls;

    typedef struct {
        COMMS_CFG   mplCommCfg;
        COMMS_CFG   baseCommCfg;
        COMMS_CFG   mgmtCommCfg;
        bool        bAutoAssignPorts;
        bool        bEnableRawLog;
        bool        bEnableCalLog;
        String      sRawLogFileName;
        String      sCalLogFileName;
        DWORD       dwRadioChanTimeout;
    } COMMS_PAGE_SETTINGS;

    COMMS_PAGE_SETTINGS m_OrigCommsSettings;

    typedef struct {
        String           sJobDir;
        DWORD            dwProcPriority;
        String           sBackupDir;
        bool             bForceBackupSave;
        int              iSensorAlpha;
        int              iSamplingRate;
        DWORD            dwLaunchClkTimeout;
        DWORD            dwLaunchDuration;
        DWORD            dwCleanDuration;
        DWORD            dwResetDuration;
        DWORD            dwPlugDetectTimeout;
        AVERAGING_PARAMS rpmAvgParams;
        BATT_THRESH_VALS battThresh;
    } MISC_PAGE_SETTINGS;

    MISC_PAGE_SETTINGS m_OrigMiscSettings;

    void         SetCommsCtrls( const COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls );
    TWinControl* GetCommsCtrls(       COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls );

    void LoadCommsPage( void );
    void LoadCalPage( void );
    void LoadMiscPage( void );

    TWinControl* CheckCommsPage( void );
    TWinControl* CheckCalPage( void );
    TWinControl* CheckMiscPage( void );

    // All controls must be valid when calling the following 'Save' functions
    DWORD SaveCommsPage( void );
    DWORD SaveCalPage( void );
    DWORD SaveMiscPage( void );

    // Support functions
    void SetJobStatusBtns( void );

public:
    __fastcall TSystemSettingsForm(TComponent* Owner);

    __property TCommPoller* PollObject      = { read = m_poller,  write = m_poller  };
    __property TPlugJob*    CurrentJob      = { read = m_currJob, write = m_currJob };
    __property DWORD        SettingsChanged = { read = m_dwSettingsChanged };

    typedef enum {
        COMMS_SETTINGS_CHANGED = 0x0001,
        MISC_SETTINGS_CHANGED  = 0x0002,
        CAL_SETTINGS_CHANGED   = 0x0004,
    } SETTINGS_CHANGED;

    // This is not an auto-created form. Must call the static ShowDlg() method
    // to construct, show, and then destroy the dialog.
    static DWORD ShowDlg( TCommPoller* pPoller, TPlugJob* currJob );

    static bool IsValidJobLogFolder( const String& sFolderName, bool bCreateFolder );
        // Returns true if the folder exists and can be written to. If the
        // folder does not exist, the method will attempt to create the
        // folder first if bCreateFolder is true.

};

extern PACKAGE TSystemSettingsForm *SystemSettingsForm;

#endif
