//
// This unit implements support routines for creating, enumerating,
// and updating job files.
//

#include <vcl.h>
#pragma hdrstop

#include <IOUtils.hpp>
#include <msxmldom.hpp>
#include <XMLDoc.hpp>
#include <xmldom.hpp>

#include "PlugJobManager.h"
#include "XMLHelpers.h"

#pragma package(smart_init)


//
// Private Declarations
//

static const UnicodeString m_JobExt( ".pjb" );

#define CURR_JOB_FILE_VER  1000


//
// Public constants
//


//
// Root-level XML doc keys and subkeys.
//
static const UnicodeString sRootNode   = "TESCOPlugJobFile";
static const UnicodeString rnFileVer   = "FileVer";

static const UnicodeString sJobInfoNode = "JobInfo";
    static const UnicodeString jiClient           = "Client";
    static const UnicodeString jiLocn             = "Locn";
    static const UnicodeString jiUOM              = "Units";
    static const UnicodeString jiStartTime        = "StartTime";

static const UnicodeString sJobParamsNode = "JobParams";
    static const UnicodeString jpStatus           = "Done";
    static const UnicodeString jpCreateNbr        = "CreateNbr";


// Record handlers. These handlers return true if keyName was found in aNode,
// false otherwise (for both get...() and set...() functions). The functions will
// also return false if the underlying XML doc is not active or an exception occurs
// during the get...() or set... () operation.
//
// All record handlers for the same type of operation have the same function name
// and are overloaded by the param list. This allows the functions to be called
// from within the template class implementation.

static bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName,       PLUG_JOB_INFO& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, const String& keyName, const PLUG_JOB_INFO& entry );

static bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName,       JOB_PARAMS& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, const String& keyName, const JOB_PARAMS& entry );


//
// Private Methods
//

static void SetXMLDocOptions( _di_IXMLDocument& xmlDoc, WORD objFileVer, const PLUG_JOB_INFO& jobInfo )
{
    // Set the basic structure for an empty xml doc
    xmlDoc->NodeIndentStr = "  ";
    xmlDoc->Options  = TXMLDocOptions() << doNodeAutoIndent << doAttrNull << doAutoPrefix << doNamespaceDecl;
    xmlDoc->Encoding = "ISO-8859-1";
    xmlDoc->Version  = "1.0";

    // Add the root node
    _di_IXMLNode rootNode = xmlDoc->AddChild( sRootNode );

    // Add the file version
    _di_IXMLNode verNode = rootNode->AddChild( rnFileVer );
    verNode->Text = IntToStr( objFileVer );

    // Add the job info
    SaveEntryToNode( rootNode, sJobInfoNode, jobInfo );
}


static bool GetXMLDocOptions( _di_IXMLDocument& xmlDoc, WORD& objFileVer, PLUG_JOB_INFO& jobInfo )
{
    // Get the root node.
    _di_IXMLNode rootNode = xmlDoc->DocumentElement;

    if( rootNode == NULL )
        return false;

    // Return the file version
    objFileVer = (WORD)GetValueFromNode( rootNode, rnFileVer, 0 );

    if( !GetEntryFromNode( rootNode, sJobInfoNode, jobInfo ) )
        return false;

     return true;
}


//
// GetEntry...() / SaveEntry...() functions are overloaded implementations
// for getting and saving structure types
//

bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName, PLUG_JOB_INFO& entry )
{
    // Initialize to default values
    entry.clientName     = L"";
    entry.location       = L"";
    entry.unitsOfMeasure = UOM_METRIC;
    entry.jobStartTime   = 0;

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.clientName     = GetValueFromNode( infoNode, jiClient,    entry.clientName     );
        entry.location       = GetValueFromNode( infoNode, jiLocn,      entry.location       );
        entry.unitsOfMeasure = GetValueFromNode( infoNode, jiUOM,       entry.unitsOfMeasure );
        entry.jobStartTime   = GetValueFromNode( infoNode, jiStartTime, entry.jobStartTime   );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode( _di_IXMLNode aNode, const String& keyName, const PLUG_JOB_INFO& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, jiClient,    entry.clientName     );
        SaveValueToNode( infoNode, jiLocn,      entry.location       );
        SaveValueToNode( infoNode, jiUOM,       entry.unitsOfMeasure );
        SaveValueToNode( infoNode, jiStartTime, entry.jobStartTime   );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName, JOB_PARAMS& entry )
{
    // Initialize to default values
    entry.isDone          = false;
    entry.lastCreationNbr = 0;

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.isDone          = GetValueFromNode( infoNode, jpStatus,      entry.isDone          );
        entry.lastCreationNbr = GetValueFromNode( infoNode, jpCreateNbr,   entry.lastCreationNbr );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode( _di_IXMLNode aNode, const String& keyName, const JOB_PARAMS& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, jpStatus,      entry.isDone          );
        SaveValueToNode( infoNode, jpCreateNbr,   entry.lastCreationNbr );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


//
// TJobList Implementation
//

__fastcall TJobList::TJobList( UnicodeString dataDir )
{
    pJobList = new TList();

    TStringDynArray list;
    TSearchOption searchOption = TSearchOption::soTopDirectoryOnly;

    try
    {
        list = TDirectory::GetFiles( dataDir, L"*" + m_JobExt, searchOption );
    }
    catch( ... )
    {
    }

    for( int iItem = 0; iItem < list.Length; iItem++ )
    {
        // Only list valid jobs
        PLUG_JOB_INFO jobInfo;

        if( GetJobInfo( list[iItem], jobInfo ) )
        {
             JOB_LIST_ITEM* pJobItem = new JOB_LIST_ITEM;

             // Assign the items to be stored in the struct. Force the
             // job file name string to be unique.
             pJobItem->jobInfo     = jobInfo;
             pJobItem->jobFileName = list[iItem];
             pJobItem->jobFileName.Unique();

             pJobList->Add( pJobItem );
        }
    }
}


__fastcall TJobList::TJobList( TStrings* fileNameList )
{
    pJobList = new TList();

    if( fileNameList == NULL )
        return;

    for( int iItem = 0; iItem < fileNameList->Count; iItem++ )
    {
        // Only list valid jobs
        PLUG_JOB_INFO jobInfo;

        if( GetJobInfo( fileNameList->Strings[iItem], jobInfo ) )
        {
             JOB_LIST_ITEM* pJobItem = new JOB_LIST_ITEM;

             // Assign the items to be stored in the struct. Force the
             // job file name string to be unique.
             pJobItem->jobInfo     = jobInfo;
             pJobItem->jobFileName = fileNameList->Strings[iItem];
             pJobItem->jobFileName.Unique();

             pJobList->Add( pJobItem );
        }
    }
}


__fastcall TJobList::~TJobList( void )
{
    while( pJobList->Count > 0 )
    {
        JOB_LIST_ITEM* pJobItem = (JOB_LIST_ITEM*)( pJobList->Items[0] );

        delete pJobItem;

        pJobList->Delete( 0 );
    }
}


bool TJobList::GetItem( int index, JOB_LIST_ITEM& jobItem )
{
    if( ( index < 0 ) || ( index >= pJobList->Count ) )
        return false;

    JOB_LIST_ITEM* pJobItem = (JOB_LIST_ITEM*)( pJobList->Items[index] );

    jobItem = *pJobItem;

    return true;
}


//
//  Public Methods
//

int JobsExistFor( const UnicodeString& dataDir, const UnicodeString& clientName, const UnicodeString& location )
{
    // Returns how many jobs exists for the passed client and location pair
    // in the dataDir. If none exist, return zero.
    TStringDynArray list;
    TSearchOption searchOption = TSearchOption::soTopDirectoryOnly;

    try
    {
        list = TDirectory::GetFiles( dataDir, L"*" + m_JobExt, searchOption );
    }
    catch( ... )
    {
    }

    int nbrJobs = 0;

    for( int iItem = 0; iItem < list.Length; iItem++ )
    {
        // Only list valid jobs
        PLUG_JOB_INFO jobInfo;

        if( GetJobInfo( list[iItem], jobInfo ) )
        {
            if( ( jobInfo.clientName.CompareIC( clientName ) == 0 )
                  &&
                ( jobInfo.location.CompareIC( location ) == 0 )
              )
            {
                nbrJobs++;
            }
        }
    }

    return nbrJobs;
}


bool GetJobInfo( const String& jobFileName, PLUG_JOB_INFO& jobInfo )
{
    // Returns true if the job name exists and populates the jobInfo struct.
    // Returns false if the job is not found.
    if( !FileExists( jobFileName ) )
        return false;

    bool bSuccess = false;

    _di_IXMLDocument xmlDoc = NULL;

    try
    {
        _di_IXMLDocument xmlDoc = LoadXMLDocument( jobFileName );

        /* This is where we'd validate the MD5 checksum */

        // Validate the root node contents
        WORD jobFileVer;

        if( !GetXMLDocOptions( xmlDoc, jobFileVer, jobInfo ) )
            Abort();

        if( jobFileVer != CURR_JOB_FILE_VER )
            Abort();

        // Fall through means success
        bSuccess = true;
    }
    catch( ... )
    {
    }

    if( xmlDoc != NULL )
        xmlDoc.Release();

    return bSuccess;
}

