#ifndef PlugJobManagerH
#define PlugJobManagerH

    //
    // This file implements support routines for creating, enumerating,
    // and updating plug job files.
    //

    #include <time.h>
    #include <XMLIntf.hpp>

    #include "TPlugJob.h"

    // The JOB_LIST_ITEM is used by TJobList to describe each job in the list
    typedef struct {
        PLUG_JOB_INFO jobInfo;
        String        jobFileName;
    } JOB_LIST_ITEM;


    // TJobList manages a list of jobs. The object can be constructed by
    // passing a directory that contains a list of jobs, or passing a
    // list of file names of jobs.
    class TJobList : public TObject
    {
        private:

        protected:
            TList* pJobList;

            __fastcall int GetJobCount( void ) { return pJobList->Count; }

        public:

            virtual __fastcall TJobList( const UnicodeString dataDir );
            virtual __fastcall TJobList( TStrings* fileNameList );

            virtual __fastcall ~TJobList( void );

            __property int Count = { read = GetJobCount };

            bool GetItem( int index, JOB_LIST_ITEM& jobItem );
    };


    int JobsExistFor( const String& dataDir, const String& clientName, const String& location );
        // Returns how many jobs exists for the passed client and location pair
        // in the dataDir. If none exist, return zero.

    bool GetJobInfo( const String& jobFileName, PLUG_JOB_INFO& jobInfo );
        // Returns true if the job name exists and populates the jobInfo struct.
        // Returns false if the job is not found.


#endif
