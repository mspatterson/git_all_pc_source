//
// This form will display event messages that must be acknowledged by
// the user. Multiple events can be queued up for display and in this
// case the form will display individually for each event. On ack by
// the user, a message is posted to the main form allowing it to
// update states based on this acknowledgement.
//
// Because this form maintains a queue of activity, a single instance
// of it must be constructed and used throughout program execution.
//
#include <vcl.h>
#pragma hdrstop

#include "TAckDlg.h"
#include "TescoShared.h"

#pragma package(smart_init)
#pragma link "AdvSmoothToggleButton"
#pragma link "AdvSmoothLabel"
#pragma resource "*.dfm"


TAckForm *AckForm;


#define ET_ALARM_BIT  0x80000000


__fastcall TAckForm::TAckForm(TComponent* Owner) : TForm(Owner)
{
    m_pAckQueue = new TList();

    m_hwndMsg = NULL;
    m_msgNbr  = 0;

    AckBtn->ColorDisabled    = (TColor)0x404040;
    AckBtn->BorderColor      = clTESCOBlue;
    AckBtn->BorderInnerColor = clTESCOBlue;

    EventMsgPanel->BorderColor        = clTESCOBlue;
    EventMsgPanel->BorderInnerColor   = clTESCOBlue;
    EventMsgPanel->BevelColorDisabled = clTESCOBlue;
}


void __fastcall TAckForm::FormDestroy(TObject *Sender)
{
    delete m_pAckQueue;
}


void TAckForm::SetAckEventMessage( HWND hDestWindow, int uMsgNbr )
{
    // Call to set the message number of send to a window
    // when an event is ack'd. WParam will be an AckEventType
    // enum, LParam is not used.
    m_hwndMsg = hDestWindow;
    m_msgNbr  = uMsgNbr;
}


void TAckForm::DisplayEvent( AckEventType aetType, bool bIsAlarm )
{
    // Queues the passed event for display and ack by the user.
    // The dialog appears immediately if there are no pending
    // events waiting ack. Otherwise, the event is queued for
    // display after all prior events have been ack'd

    // Just add the event to the queue. The timer tick will
    // pick this up and display the dialog. The alarm indication
    // will set the higher order bit - a hack indeed.
    DWORD dwItem = aetType;

    if( bIsAlarm )
        dwItem |= ET_ALARM_BIT;

    m_pAckQueue->Add( (void*)dwItem );
}


bool TAckForm::HaveEvents( void )
{
    // Returns true if one or more events are queued for ack'ing
    return( m_pAckQueue->Count > 0 );
}


void TAckForm::Flush( void )
{
    // Call to flush the event queue of any pending events
    // and close the form if visible.
    m_pAckQueue->Clear();

    // Be sure we close the form on flush as well
    ModalResult = mrCancel;
}


void __fastcall TAckForm::DisplayTimerTimer(TObject *Sender)
{
    // If we have no events, can return now
    if( m_pAckQueue->Count == 0 )
        return;

    // We have at least one event to show. Kill the timer
    // while we go modal.
    if( m_pAckQueue->Count > 0 )
    {
        DisplayTimer->Enabled = false;

        // Extract the next item. It will consist of an event type enum and
        // an alarm flag.
        DWORD dwCurrItem = (DWORD)( m_pAckQueue->Items[0] );

        AckEventType eCurrEvent = (AckEventType)( dwCurrItem & ~ET_ALARM_BIT );
        bool         bIsAlarm   = ( dwCurrItem & ET_ALARM_BIT ) ? true : false;

        String sEventText;

        switch( eCurrEvent )
        {
            case eAET_Plug1Detected:  sEventText = "Plug 1 has been detected.";                       break;
            case eAET_Plug1Lost:      sEventText = "Plug 1 was launched but has not been detected.";  break;
            case eAET_Plug2Detected:  sEventText = "Plug 2 has been detected.";                       break;
            case eAET_Plug2Lost:      sEventText = "Plug 2 was launched but has not been detected.";  break;
            case eAET_Plug3Detected:  sEventText = "Plug 3 has been detected.";                       break;
            case eAET_Plug3Lost:      sEventText = "Plug 3 was launched but has not been detected.";  break;
            case eAET_Plug4Detected:  sEventText = "Plug 4 has been detected.";                       break;
            case eAET_Plug4Lost:      sEventText = "Plug 4 was launched but has not been detected.";  break;
            case eAET_Plug5Detected:  sEventText = "Plug 5 has been detected.";                       break;
            case eAET_Plug5Lost:      sEventText = "Plug 5 was launched but has not been detected.";  break;
            case eAET_OrphanDetected: sEventText = "A detection event has occurred, but no plugs are in the launched state.";  break;
            case eAET_LowBattery:     sEventText = "The battery voltage is too low. Please replace the battery immediately.";  break;
            default:                  sEventText = "Unknown event " + IntToStr( eCurrEvent ) + " reported";                    break;
        }

        EventMsgPanel->Caption = sEventText;

        if( bIsAlarm )
        {
            Caption = "Acknowledge MPL Alarm";

            EventMsgPanel->BorderColor        = clTESCORed;
            EventMsgPanel->BorderInnerColor   = clTESCORed;
            EventMsgPanel->BevelColorDisabled = clTESCORed;
        }
        else
        {
            Caption = "Acknowledge MPL Event";

            EventMsgPanel->BorderColor        = clTESCOBlue;
            EventMsgPanel->BorderInnerColor   = clTESCOBlue;
            EventMsgPanel->BevelColorDisabled = clTESCOBlue;
        }

        // Show the dialog. If the user presses OK, then we can delete
        // the event from the queue. Watch for the special case where
        // we've been forced closed, which will have flushed the queue.
        if( ShowModal() == mrOk )
        {
            if( m_pAckQueue->Count > 0 )
                m_pAckQueue->Delete( 0 );
        }

        // Okay to re-enable time
        DisplayTimer->Enabled = true;
    }
}


void __fastcall TAckForm::AckBtnClick(TObject *Sender)
{
    // On Ack, call back the event window. Event being displayed is at the
    // top of the list. Remember to strip off the alarm bit.
    if( m_pAckQueue->Count > 0 )
    {
        DWORD dwCurrItem = (DWORD)( m_pAckQueue->Items[0] );

        if( ( m_hwndMsg != NULL ) && ( m_msgNbr != 0 ) )
            ::PostMessage( m_hwndMsg, m_msgNbr, dwCurrItem & ~ET_ALARM_BIT, 0 );
    }

    ModalResult = mrOk;
}
