object AckForm: TAckForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Acknowledge MPL Event'
  ClientHeight = 152
  ClientWidth = 375
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object AckBtn: TAdvSmoothToggleButton
    Left = 40
    Top = 84
    Width = 297
    Height = 56
    AllowAllUp = True
    BorderColor = clBlue
    BorderInnerColor = clBlue
    AutoToggle = False
    Appearance.Font.Charset = DEFAULT_CHARSET
    Appearance.Font.Color = clWindowText
    Appearance.Font.Height = -15
    Appearance.Font.Name = 'Tahoma'
    Appearance.Font.Style = []
    Caption = 'Acknowledge'
    Version = '1.4.3.0'
    Status.Caption = '0'
    Status.Appearance.Fill.Color = clRed
    Status.Appearance.Fill.ColorMirror = clNone
    Status.Appearance.Fill.ColorMirrorTo = clNone
    Status.Appearance.Fill.GradientType = gtSolid
    Status.Appearance.Fill.GradientMirrorType = gtSolid
    Status.Appearance.Fill.BorderColor = clGray
    Status.Appearance.Fill.Rounding = 0
    Status.Appearance.Fill.ShadowOffset = 0
    Status.Appearance.Fill.Glow = gmNone
    Status.Appearance.Font.Charset = DEFAULT_CHARSET
    Status.Appearance.Font.Color = clWhite
    Status.Appearance.Font.Height = -11
    Status.Appearance.Font.Name = 'Tahoma'
    Status.Appearance.Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = AckBtnClick
  end
  object EventMsgPanel: TAdvSmoothToggleButton
    Left = 8
    Top = 8
    Width = 359
    Height = 65
    AllowAllUp = True
    BorderColor = clBlue
    BorderInnerColor = clBlue
    BevelColorDisabled = clBlue
    AutoToggle = False
    Enabled = False
    Appearance.Font.Charset = DEFAULT_CHARSET
    Appearance.Font.Color = clWindowText
    Appearance.Font.Height = -15
    Appearance.Font.Name = 'Tahoma'
    Appearance.Font.Style = []
    Caption = 'MPL Event'
    Version = '1.4.3.0'
    Status.Caption = '0'
    Status.Appearance.Fill.Color = clRed
    Status.Appearance.Fill.ColorMirror = clNone
    Status.Appearance.Fill.ColorMirrorTo = clNone
    Status.Appearance.Fill.GradientType = gtSolid
    Status.Appearance.Fill.GradientMirrorType = gtSolid
    Status.Appearance.Fill.BorderColor = clGray
    Status.Appearance.Fill.Rounding = 0
    Status.Appearance.Fill.ShadowOffset = 0
    Status.Appearance.Fill.Glow = gmNone
    Status.Appearance.Font.Charset = DEFAULT_CHARSET
    Status.Appearance.Font.Color = clWhite
    Status.Appearance.Font.Height = -11
    Status.Appearance.Font.Name = 'Tahoma'
    Status.Appearance.Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnClick = AckBtnClick
  end
  object DisplayTimer: TTimer
    Interval = 500
    OnTimer = DisplayTimerTimer
    Left = 8
    Top = 48
  end
end
