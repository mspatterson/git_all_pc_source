#include <vcl.h>
#pragma hdrstop

#include "TJobParamsDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TJobParamsDlg *JobParamsDlg;


__fastcall TJobParamsDlg::TJobParamsDlg(TComponent* Owner) : TForm(Owner)
{
}


bool TJobParamsDlg::ShowForm( TJobParamsDlg::JOB_PARAMS& jobParams )
{
    TJobParamsDlg *pDlg = new TJobParamsDlg( Application );

    bool bClosedOK = false;

    try
    {
        if( ( jobParams.iNbrPlugs >= 1 ) && ( jobParams.iNbrPlugs <= pDlg->NbrPlugsCombo->Items->Count ) )
            pDlg->NbrPlugsCombo->ItemIndex = jobParams.iNbrPlugs - 1;
        else
            pDlg->NbrPlugsCombo->ItemIndex = pDlg->NbrPlugsCombo->Items->Count - 1;

        pDlg->ElectDetectCB->Checked = jobParams.bUseElecDetect;

        if( pDlg->ShowModal() == mrOk )
        {
            jobParams.iNbrPlugs      = pDlg->NbrPlugsCombo->ItemIndex + 1;
            jobParams.bUseElecDetect = pDlg->ElectDetectCB->Checked;

            bClosedOK = true;
        }
    }
    catch( ... )
    {
    }

    delete pDlg;

    return bClosedOK;
}


