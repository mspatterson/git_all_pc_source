object JobParamsDlg: TJobParamsDlg
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'MultiPlug Launcher Job Parameters'
  ClientHeight = 143
  ClientWidth = 272
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    272
    143)
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 100
    Top = 110
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    ExplicitLeft = 290
    ExplicitTop = 257
  end
  object CancelBtn: TButton
    Left = 189
    Top = 110
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    ExplicitLeft = 379
    ExplicitTop = 257
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 256
    Height = 96
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Select Job Parameters '
    TabOrder = 2
    ExplicitWidth = 446
    ExplicitHeight = 243
    object Label1: TLabel
      Left = 24
      Top = 28
      Width = 82
      Height = 13
      Caption = 'Number of Plugs:'
    end
    object ElectDetectCB: TCheckBox
      Left = 24
      Top = 60
      Width = 169
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Use Electronic Plug Detection:'
      TabOrder = 0
    end
    object NbrPlugsCombo: TComboBox
      Left = 180
      Top = 25
      Width = 45
      Height = 21
      Style = csDropDownList
      ItemIndex = 1
      TabOrder = 1
      Text = '2'
      Items.Strings = (
        '1'
        '2')
    end
  end
end
