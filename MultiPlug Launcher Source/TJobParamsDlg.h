//---------------------------------------------------------------------------
#ifndef TJobParamsDlgH
#define TJobParamsDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TJobParamsDlg : public TForm
{
__published:	// IDE-managed Components
    TButton *OKBtn;
    TButton *CancelBtn;
    TGroupBox *GroupBox1;
    TLabel *Label1;
    TCheckBox *ElectDetectCB;
    TComboBox *NbrPlugsCombo;
private:	// User declarations
public:		// User declarations
    __fastcall TJobParamsDlg(TComponent* Owner);

    typedef struct {
        int iNbrPlugs;
        bool bUseElecDetect;
    } JOB_PARAMS;

    static bool ShowForm( JOB_PARAMS& jobParams );
};
//---------------------------------------------------------------------------
extern PACKAGE TJobParamsDlg *JobParamsDlg;
//---------------------------------------------------------------------------
#endif
