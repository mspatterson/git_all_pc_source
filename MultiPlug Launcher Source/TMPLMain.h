//---------------------------------------------------------------------------
#ifndef TMPLMainH
#define TMPLMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
#include <Vcl.ImgList.hpp>
#include "AdvSmoothButton.hpp"
#include "AdvSmoothLabel.hpp"
#include "AdvSmoothStatusIndicator.hpp"
#include "AdvSmoothPanel.hpp"
#include "AdvSmoothToggleButton.hpp"
#include "AdvSmoothProgressBar.hpp"
#include "GDIPPictureContainer.hpp"
#include "MPLRegistryInterface.h"
#include "TPlugControl.h"
#include "PlugJobManager.h"
#include "CommsMgr.h"
#include "Messages.h"
//---------------------------------------------------------------------------
class TMPLMainForm : public TForm
{
__published:	// IDE-managed Components
    TAdvSmoothPanel *MenuPanel;
    TImage *TESCOLogoImage;
    TTimer *SystemTimer;
    TAdvSmoothPanel *ControlPanel;
    TAdvSmoothToggleButton* EnableLaunchBtn;
    TAdvSmoothToggleButton *Launch1Btn;
    TAdvSmoothToggleButton *Launch2Btn;
    TAdvSmoothToggleButton *ProjSettingsBtn;
    TAdvSmoothToggleButton *ProjLogBtn;
    TAdvSmoothToggleButton *SysSettingsBtn;
    TAdvSmoothToggleButton *AboutBtn;
    TAdvSmoothPanel *BaseRadioStatusPanel;
    TAdvSmoothPanel *BattLevelStatusPanel;
    TAdvSmoothPanel *MPLLinkStatusPanel;
    TAdvSmoothPanel *RPMStatusPanel;
    TAdvSmoothPanel *TempStatusPanel;
    TAdvSmoothPanel *PressStatusPanel;
    TAdvSmoothProgressBar *BaseRadioProgBar;
    TAdvSmoothProgressBar *MPLLinkProgBar;
    TAdvSmoothToggleButton *Clean1Btn;
    TAdvSmoothToggleButton *Clean2Btn;
    TAdvSmoothToggleButton *ResetFlagBtn;
    void __fastcall AboutBtnClick(TObject *Sender);
    void __fastcall SystemTimerTimer(TObject *Sender);
    void __fastcall ProjSettingsBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall ProjLogBtnClick(TObject *Sender);
    void __fastcall EnableLaunchBtnClick(TObject *Sender);
    void __fastcall LaunchPlugBtnClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall SysSettingsBtnClick(TObject *Sender);
    void __fastcall CleanPlugBtnClick(TObject *Sender);
    void __fastcall ResetFlagBtnClick(TObject *Sender);

private:
    TCommPoller* m_devPoller;
    TPlugJob*    m_currJob;

    SHUT_DOWN_TYPE   m_lastShutdownType;
    UNITS_OF_MEASURE m_defaultUOM;

    int          m_iNbrPlugs;
    bool         m_bUsingElecDetection;

    DWORD        m_dwLaunchClickTimeout;
    DWORD        m_dwEnableBtnTimeout;
    DWORD        m_dwPlugDetectTimeout;

    DWORD        m_dwBtnAnimationTimer;

    #define NBR_USED_MPL_PLUGS 2
    TPlugControl*           m_pPlugControls[NBR_USED_MPL_PLUGS];
    TAdvSmoothToggleButton* m_LaunchBtns[NBR_USED_MPL_PLUGS];
    TAdvSmoothToggleButton* m_CleanBtns[NBR_USED_MPL_PLUGS];

    typedef enum {
        eLS_Loading,     // Loading a job
        eLS_Disabled,    // Cannot launch any plugs
        eLS_HWWait,      // Waiting for hardware to complete an init or calibration
        eLS_Ready,       // Can enable a launch
        eLS_Enabled,     // Can launch or clean a plug
        eLS_Busy,        // Launching or cleaning a plug - must wait for the action to finish
        eLS_Complete,    // Like disabled, but after job marked as complete by user
        eLS_NbrLaunchStates
    } eLaunchState;

    eLaunchState m_eLaunchState;

    void SetLaunchState( eLaunchState eNewState );
        // Generally, SetMainState() will be the only one calling this method,
        // but there are some special circumstances where the main state isn't
        // changing but the launch state has to.

    typedef enum {
        eMS_StartUp,
        eMS_ViewOnly,
        eMS_DiagnosticChk,
        eMS_Running,
        eMS_NbrMainStates,
    } MainStates;

    MainStates m_msState;

    void SetMainState( MainStates eNewState );

    void UpdateStatusValues( void );
    void UpdateStatusScanning( void );
    void RefreshCaption( void );
    void SetSmoothPanelCaption( TAdvSmoothPanel* pPanel, const String& sText );

    void DisableActionBtns( void );
    void AnimateActionBtn( TAdvSmoothToggleButton* pBtn );

    void EnterSimMode( bool bIsSimulating );

    bool HaveJobLogDir( void );

    void __fastcall OnPlugLaunchedClick( TObject* Sender );

    void ProcessPlugDetected( void );
    void ProcessPlugCleaned( void );
    void ProcessResetFlagDone( void );
    void ProcessAlarm( TCommPoller::AlarmType eAlarmType );

protected:
    void __fastcall WMCompleteJob       ( TMessage &Message );
    void __fastcall WMReopenJob         ( TMessage &Message );
    void __fastcall WMShowSysSettingsDlg( TMessage &Message );
    void __fastcall WMCommsEvent        ( TMessage &Message );
    void __fastcall WMAckEvent          ( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_COMPLETE_JOB,          TMessage, WMCompleteJob    )
      MESSAGE_HANDLER( WM_REOPEN_JOB,            TMessage, WMReopenJob      )
      MESSAGE_HANDLER( WM_SHOW_SYS_SETTINGS_DLG, TMessage, WMShowSysSettingsDlg )
      MESSAGE_HANDLER( WM_POLLER_EVENT,          TMessage, WMCommsEvent )
      MESSAGE_HANDLER( WM_ACK_EVENT,             TMessage, WMAckEvent )
    END_MESSAGE_MAP(TForm)

public:		// User declarations
    __fastcall TMPLMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMPLMainForm *MPLMainForm;
//---------------------------------------------------------------------------
#endif
