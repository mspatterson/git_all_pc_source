#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>

#include "TPlugControl.h"

#pragma package(smart_init)


__fastcall TPlugControl::TPlugControl( TComponent* pOwner, TWinControl* pParent ) : TPaintBox( pOwner )
{
    Parent = pParent;

    m_pcsState   = ePCS_NotLoaded;
    m_sCaption   = "";
    m_dtLaunch   = Now();
    m_dtDetected = Now();
    m_dtLost     = Now();

    // Improve paint performance by making this control opaque
    ControlStyle = ControlStyle << csOpaque;
}


__fastcall TPlugControl::~TPlugControl()
{
}


void __fastcall TPlugControl::Paint( void )
{
    TCanvas* pCanvas = Canvas;

    // Increase the point size of the font beyond the default (parent) font
    pCanvas->Font->Size = 11;

    const int BorderWidth = 2;

    // Top of the control is a fixed label. Oversize for text height a bit
    int iLabelHeight = 140 * pCanvas->TextHeight( "X" ) / 100;

    // Fill the label area and draw its text
    pCanvas->Brush->Color = clLtGray;
    pCanvas->Brush->Style = bsSolid;

    pCanvas->Pen->Color = clDkGray;
    pCanvas->Pen->Style = psSolid;
    pCanvas->Pen->Width = BorderWidth;

    pCanvas->Rectangle( TRect( 1, 1, Width, iLabelHeight ) );

    RECT rCaption;
    rCaption.left   = BorderWidth;
    rCaption.top    = 0;
    rCaption.right  = Width - BorderWidth;
    rCaption.bottom = iLabelHeight;

    DrawText( pCanvas->Handle, m_sCaption.w_str(), m_sCaption.Length(), &rCaption, DT_SINGLELINE | DT_VCENTER | DT_CENTER );

    // Now draw the status area
    TPoint ptOutline[4];

    ptOutline[0].x = BorderWidth / 2;
    ptOutline[0].y = iLabelHeight;

    ptOutline[1].x = BorderWidth / 2;
    ptOutline[1].y = Height - BorderWidth / 2;

    ptOutline[2].x = Width - BorderWidth / 2;
    ptOutline[2].y = Height - BorderWidth / 2;

    ptOutline[3].x = Width - BorderWidth / 2;
    ptOutline[3].y = iLabelHeight;

    pCanvas->Pen->Color = clDkGray;
    pCanvas->Pen->Style = psSolid;
    pCanvas->Pen->Width = BorderWidth;

    pCanvas->Polyline( ptOutline, 3 );

    // Determine how high the gradient area is. This only varies if we are
    // in the launched state
    int iHeight = Height - iLabelHeight - BorderWidth;

    if( ( m_pcsState == ePCS_Launched ) && ( m_pcmMode == ePCM_Electronic ) )
        iHeight = ( Height - iLabelHeight - BorderWidth ) * ( Tag + 1 ) / 16;

    // Update the animation counter
    Tag++;

    if( Tag > 15 )
        Tag = 0;

    // Determine the start and end colors. This again depends on state
    TRIVERTEX     vert[2];
    GRADIENT_RECT gRect;

    vert[0].x = BorderWidth;
    vert[0].y = iLabelHeight;

    vert[1].x = Width - BorderWidth;
    vert[1].y = iLabelHeight + iHeight; //Height;

    switch( m_pcsState )
    {
        case ePCS_Loaded:
            vert[0].Red    = 0xF000;
            vert[0].Green  = 0xF000;
            vert[0].Blue   = 0x0F00;
            vert[0].Alpha  = 0x2400;

            vert[1].Red    = 0xF000;
            vert[1].Green  = 0xF000;
            vert[1].Blue   = 0x2400;
            vert[1].Alpha  = 0x0000;
            break;

        case ePCS_Launched:
            vert[0].Red    = 0x0300;
            vert[0].Green  = 0xAF00;
            vert[0].Blue   = 0x0F00;
            vert[0].Alpha  = 0x2400;

            vert[1].Red    = 0xFF00;
            vert[1].Green  = 0xFF00;
            vert[1].Blue   = 0xFF00;
            vert[1].Alpha  = 0x0000;
            break;

        case ePCS_Detected:
            vert[0].Red    = 0x0300;
            vert[0].Green  = 0xAF00;
            vert[0].Blue   = 0x0F00;
            vert[0].Alpha  = 0x2400;

            vert[1].Red    = 0x0300;
            vert[1].Green  = 0xAF00;
            vert[1].Blue   = 0x0F00;
            vert[1].Alpha  = 0x0000;
            break;

        case ePCS_Lost:
            vert[0].Red    = 0xC000;
            vert[0].Green  = 0x0C00;
            vert[0].Blue   = 0x0C00;
            vert[0].Alpha  = 0x2400;

            vert[1].Red    = 0xA000;
            vert[1].Green  = 0x0800;
            vert[1].Blue   = 0x0800;
            vert[1].Alpha  = 0x0000;
            break;

        case ePCS_NotLoaded:
        default:
            vert[0].Red    = 0x8000;
            vert[0].Green  = 0x8000;
            vert[0].Blue   = 0x8000;
            vert[0].Alpha  = 0x2400;

            vert[1].Red    = 0x8000;
            vert[1].Green  = 0x8000;
            vert[1].Blue   = 0x8000;
            vert[1].Alpha  = 0x0000;
            break;
    }

    gRect.UpperLeft  = 0;
    gRect.LowerRight = 1;

    GradientFill( pCanvas->Handle, vert, 2, &gRect, 1, GRADIENT_FILL_RECT_V );

    // Now draw the launch status. Need to position this manually
    int iStatusHeight = 2 * pCanvas->TextHeight( "X" );

    int iStatusTop = iLabelHeight + ( Height - iLabelHeight - iStatusHeight ) / 2;

    pCanvas->Brush->Color = Vcl::Graphics::clNone;
    pCanvas->Brush->Style = bsClear;

    RECT rStatus;
    rStatus.left   = BorderWidth;
    rStatus.top    = iStatusTop;
    rStatus.right  = Width - BorderWidth;
    rStatus.bottom = Height;

    // Determine the elapsed time (only for applicable states
    TDateTime tElapsed = Now();

    switch( m_pcsState )
    {
        case ePCS_Launched:  tElapsed = Now()        - m_dtLaunch;  break;
        case ePCS_Detected:  tElapsed = m_dtDetected - m_dtLaunch;  break;
        case ePCS_Lost:      tElapsed = m_dtLost     - m_dtLaunch;  break;
    }

    String sElapsed = tElapsed.FormatString( "nn:ss" );

    // Now build the caption string. This depends on the plug mode
    String sStatusText;

    if( m_pcmMode == ePCM_Electronic )
    {
        switch( m_pcsState )
        {
            case ePCS_NotLoaded: sStatusText = "Not Loaded";            break;
            case ePCS_Loaded:    sStatusText = "Loaded";                break;
            case ePCS_Launched:  sStatusText = "Launched\r" + sElapsed; break;
            case ePCS_Detected:  sStatusText = "Detected\r" + sElapsed; break;
            case ePCS_Lost:      sStatusText = "LOST\r" +     sElapsed; break;
            default:             sStatusText = "???";                   break;
        }
    }
    else
    {
        // Only 3 states can happen in manual mode.
        if( m_pcsState == ePCS_NotLoaded )
        {
            sStatusText = "Not Loaded";
        }
        else if( m_pcsState == ePCS_Loaded )
        {
            sStatusText = "Loaded";
        }
        else if( m_pcsState == ePCS_Launched )
        {
            sStatusText = "Launched at " + m_dtLaunch.FormatString( "hh:nn" );

            if( m_dtCleaned.Val > 0.0 )
                sStatusText += "\rCleaned at " + m_dtLaunch.FormatString( "hh:nn" );
        }
        else
        {
            sStatusText = "Bad state " + IntToStr( m_pcsState );
        }
    }

    DrawText( pCanvas->Handle, sStatusText.w_str(), sStatusText.Length(), &rStatus, /*DT_SINGLELINE |*/ DT_VCENTER | DT_CENTER );
}


void TPlugControl::Initialize( PlugControlState eNewState, PlugControlMode eMode )
{
    // Initializes a plug control to the passed state.
    m_pcsState = eNewState;
    m_pcmMode  = eMode;

    // Should never really initialize to the launched state, but just in case...
    if( eNewState == ePCS_Launched )
        m_dtLaunch = Now();

    Invalidate();
}


bool TPlugControl::Launch( void )
{
    // Launches a loaded plug. Will fail if the plug is not in a launchable state
    switch( m_pcsState )
    {
        case ePCS_Loaded:
        case ePCS_Detected:
        case ePCS_Launched:
        case ePCS_Lost:
            // Can (re)launch in these states
            break;

        default:
            // Cannot (re)launch in these states
            return false;
    }

    m_pcsState = ePCS_Launched;

    m_dtLaunch   = Now();
    m_dtDetected = Now();
    m_dtLost     = Now();
    m_dtCleaned.Val = 0.0;

    Invalidate();

    return true;
}


bool TPlugControl::Detected( void )
{
    // Call when a plug has been detected. Will fail if the plug is not
    // currently in the launched state
    if( ( m_pcsState != ePCS_Launched ) || ( m_pcmMode != ePCM_Electronic ) )
        return false;

    m_pcsState   = ePCS_Detected;
    m_dtDetected = Now();

    Invalidate();

    return true;
}


bool TPlugControl::Lost( void )
{
    // Call when a plug has not been detected. Will fail if the plug is not
    // currently in the launched state
    if( ( m_pcsState != ePCS_Launched ) || ( m_pcmMode != ePCM_Electronic ) )
        return false;

    m_pcsState = ePCS_Lost;
    m_dtLost   = Now();

    Invalidate();

    return true;
}


bool TPlugControl::Acknowledge( void )
{
    // Indicates the user is acknowledging the plug entering either the
    // detected or lost state.
    if( ( m_pcsState != ePCS_Detected ) && ( m_pcsState != ePCS_Lost ) )
        return false;

    if( m_pcmMode != ePCM_Electronic )
        return false;

     // If we're already ack'd, just exit
    if( m_bAckd )
        return true;

    // Note the ack
    m_bAckd  = true;
    m_dtAckd = Now();

    return true;
}


void TPlugControl::UpdateControl( void )
{
    Invalidate();
}


void TPlugControl::SetPlugNbr( int iNewNbr )
{
    m_iPlugNbr = iNewNbr;

    SetCaption( "Plug " + IntToStr( iNewNbr ) );
}


void TPlugControl::SetCaption( const String& newCaption )
{
    m_sCaption = newCaption;
    Invalidate();
}


DWORD TPlugControl::GetLaunchDur( void )
{
    // Returns how long, in seconds, a plug has been launched for.
    switch( m_pcsState )
    {
        case ePCS_Launched: return SecondsBetween( Now(), m_dtLaunch );
        case ePCS_Detected: return SecondsBetween( m_dtDetected, m_dtLaunch );
    }

    // Launch duration not valid in any other state
    return 0;
}

