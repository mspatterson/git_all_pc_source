#ifndef TPlugControlH
#define TPlugControlH


#include "TPlugJob.h"


typedef enum {
    ePCM_Electronic,     // Electronic detection being used, plug control is animated
    ePCM_Manual          // Manual detection being used, plug control is static
} PlugControlMode;


class TPlugControl : public TPaintBox
{
  private:

    PlugControlState m_pcsState;
    PlugControlMode  m_pcmMode;
    int              m_iPlugNbr;
    String           m_sCaption;
    TDateTime        m_dtLaunch;
    TDateTime        m_dtDetected;
    TDateTime        m_dtLost;
    TDateTime        m_dtCleaned;

    bool             m_bAckd;
    TDateTime        m_dtAckd;

    void SetCaption( const String& newCaption );
    void SetPlugNbr( int iNewNbr );

    DWORD GetLaunchDur( void );

  protected:

    virtual void __fastcall Paint( void );

  public:

    virtual __fastcall TPlugControl( TComponent* pOwner, TWinControl* pParent );
    virtual __fastcall ~TPlugControl();

    void Initialize( PlugControlState eNewState, PlugControlMode eMode );
        // Initializes a plug control to the passed state.

    bool Launch( void );
        // Launches a ready plug. Will fail if the plug is not in the ready state

    bool Detected( void );
        // Call when a plug has been detected. Will fail if the plug is not
        // currently in the launched state

    bool Lost( void );
        // Call when a plug has not been detected. Will fail if the plug is not
        // currently in the launched state

    bool Acknowledge( void );
        // Indicates the user is acknowledging the plug entering either the
        // detected or lost state.

    void UpdateControl( void );
        // Must be called to animate or refresh the control

    __property PlugControlState State           = { read = m_pcsState };
    __property int              PlugNbr         = { read = m_iPlugNbr, write = SetPlugNbr };
    __property DWORD            LaunchDuration  = { read = GetLaunchDur };

    __property bool             Acknowledged    = { read = m_bAckd };
    __property TDateTime        AckDateTime     = { read = m_dtAckd };
    __property TDateTime        CleanedDateTime = { read = m_dtCleaned, write = m_dtCleaned };

    __property OnClick;

};

#endif
