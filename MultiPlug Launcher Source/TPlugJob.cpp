#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>

#include "TPlugJob.h"

#pragma package(smart_init)


//
// TStructArray
//

// We have to do a trick here to force the compiler to instantiate the
// template class code. If you get an 'unresolved external', try adding
// that function to this initializer.

static bool DoInit( void )
{
    TStructArray<JOB_COMMENT> sectionRecs;

    JOB_COMMENT temp;

    sectionRecs.Append( temp );
    sectionRecs.Modify( 0, temp );

    return true;
}

static bool initDone = DoInit();


//
// NOTE! NOTE! NOTE! Do not use memmove(), memcpy(), etc, where
// copying or moving members of the templated class. If the
// class contains Unicode strings, using mem...() will make bad
// things happen. You have to do an item by item copy instead.
//

template <class T> TStructArray<T>::TStructArray()
{
   m_allocElements = 16;
   m_allocIncrease = 16;
   m_nbrElements   = 0;

   m_pData = new T[m_allocElements];
}


template <class T> TStructArray<T>::~TStructArray()
{
  if( m_allocElements )
      delete [] m_pData;
}


template <class T> void TStructArray<T>::CheckAndAllocMem( int newNbrElements )
{
    // Checks to see if the object contains the number of records passed.
    // If not, allocates new memory and copies existing data over.
    if( newNbrElements >= m_allocElements )
    {
        // More memory required. Over-allocate it
        T *pTmp = new T[ newNbrElements + m_allocIncrease ];

        // Copy any existing elements over. Cannot use memcpy in case
        // element members are classes (eg, UnicodeString)
        if( m_nbrElements )
        {
            for( int iEl = 0; iEl < m_nbrElements; iEl++ )
                pTmp[iEl] = m_pData[iEl];

            delete [] m_pData;
        }

        m_pData = pTmp;
        m_allocElements = newNbrElements + m_allocIncrease;
    }
}


template <class T> TStructArray<T>& TStructArray<T>::operator =(const TStructArray &equ)
{
    try
    {
        if( this == &equ )
            return *this;

        CheckAndAllocMem( equ.m_nbrElements );

        // Copy data element by element. Cannot use memcpy() in case an
        // element member is a class
        for( int iEl = 0; iEl < equ.m_nbrElements; iEl++ )
            m_pData[iEl] = equ.m_pData[iEl];

        m_nbrElements = equ.m_nbrElements;

        return *this;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::operator =" );
        return *this;
    }
}


template <class T> TStructArray<T>& TStructArray<T>::operator += (const TStructArray &plus)
{
    try
    {
        CheckAndAllocMem( m_nbrElements + plus.m_nbrElements );

        // Copy data element by element. Cannot use memcpy() in case an
        // element member is a class
        for( int iEl = 0; iEl < plus.m_nbrElements; iEl++ )
            m_pData[m_nbrElements+iEl] = plus.m_pData[iEl];

        m_nbrElements += plus.m_nbrElements;

        return *this;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::operator +=" );
        return *this;
    }
}


template <class T> void TStructArray<T>::Append( const T& add )
{
    try
    {
        CheckAndAllocMem( m_nbrElements + 1 );

        m_pData[m_nbrElements++] = add;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::Append()" );
    }
}


template <class T> void TStructArray<T>::Insert( int index, const T& insert )
{
    // Inserts an item at the given index
    try
    {
        CheckAndAllocMem( m_nbrElements + 1 );

        for( int i = m_nbrElements; i > index; i-- )
            m_pData[i] = m_pData[i-1];

        m_pData[index] = insert;

        m_nbrElements++;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::Insert()" );
    }
}


template <class T> void TStructArray<T>::Modify( int index, const T& modify )
{
    if( ( index >= 0 ) && ( index < m_nbrElements ) )
    {
        m_pData[index] = modify;
    }
}


template <class T> void TStructArray<T>::Del( int index )
{
    if( ( index >= 0 ) && ( index < m_nbrElements ) )
    {
        for( int i = index; i < m_nbrElements - 1; i++ )
            m_pData[i] = m_pData[i+1];

        m_nbrElements--;
    }
}


template <class T> void TStructArray<T>::LoadFromNode( _di_IXMLNode aNode )
{
    // Assumes that each element T is stored under aNode under 0-based
    // key values (eg, "0" to "n-1" where n is the number of elements
    // in the array.

    // Clear all elements from array first
    Clear();

    if( aNode != NULL )
    {
        // The ChildNodes->Count property gives bogus values at times for some
        // reason. So don't use it. Instead, loop here until the next item
        // number is not found.
        int itemNbr = 0;

        while( itemNbr >= 0 )
        {
            T tempData;

            if( !GetEntryFromNode( aNode, ItemKeyName( itemNbr ), tempData ) )
                break;

            Append( tempData );

            itemNbr++;
        }
    }
}


template <class T> void TStructArray<T>::SaveToNode( _di_IXMLNode aNode )
{
    // Stores each element T under aNode using 0-based key values
    // (eg, "0" to "n-1" where n is the number of elements in the array).

    if( aNode != NULL )
    {
        // Delete any existing children (and presumably leaf nodes)
        aNode->ChildNodes->Clear();

        for( int iItem = 0; iItem < m_nbrElements; iItem++ )
            SaveEntryToNode( aNode, ItemKeyName( iItem ), m_pData[iItem] );
    }
}


//
// TPlugJog
//

__fastcall TPlugJob::TPlugJob( const PLUG_JOB_INFO& jobInfo )
{
    // Save job start information.
    m_jobInfo = jobInfo;

    m_jobParams.isDone = false;
    m_jobParams.lastCreationNbr = 1;

    m_jobLoaded  = true;
    m_bInSimMode = false;

    m_jobLogDir  = "";
    m_jobLogName = "Job " + FormatDateTime( "yyyy-dd-mm hh-nn", jobInfo.jobStartTime ) + ".txt";

    // Initialize the plug state. In a later version, should check
    // that the passed launch order makes sense.
    for( int iPlug = 0; iPlug < NBR_OF_PLUGS; iPlug++ )
    {
        // Set initial plug state
        if( jobInfo.launchOrder[iPlug] >= 0 )
        {
            m_PlugInfo[iPlug].plugState    = ePCS_Loaded;
            m_PlugInfo[iPlug].iLaunchOrder = jobInfo.launchOrder[iPlug];
        }
        else
        {
            m_PlugInfo[iPlug].plugState    = ePCS_NotLoaded;
            m_PlugInfo[iPlug].iLaunchOrder = -1;
        }

        m_PlugInfo[iPlug].dtLaunch.Val  = 0.0;
        m_PlugInfo[iPlug].iSecsToDetect = 0;
    }

    // Set initial job comments
    String sComment = "New job created for " + Client;

    if( Location.Length() > 0 )
        sComment = sComment + " / " + Location;

    sComment = sComment + " on " + StartTime.DateTimeString();

    AddJobComment( sComment );

    // Add comment for plug order if launching more than 1 plug
    if( jobInfo.nbrPlugs > 1 )
    {
        String sPlugOrder;

        for( int iOrder = 0; iOrder < NBR_OF_PLUGS; iOrder++ )
        {
            for( int iPlug = 0; iPlug < NBR_OF_PLUGS; iPlug++ )
            {
                if( jobInfo.launchOrder[iPlug] == iOrder )
                    sPlugOrder += IntToStr( iPlug + 1 ) + " ";
            }
        }

        AddJobComment( "Plug launch order: " + sPlugOrder );
    }
    else
    {
        AddJobComment( "Single plug job" );
    }

    if( jobInfo.usingElecDetect )
        AddJobComment( "Using electronic detection" );
    else
        AddJobComment( "Not using electronic detection" );
}


__fastcall TPlugJob::~TPlugJob()
{
}


int TPlugJob::GetNextCreationNbr( void )
{
    // Returns a unique sequence number for a section rec, connection
    // rec, or main comment
    m_jobParams.lastCreationNbr++;

    return m_jobParams.lastCreationNbr;
}


bool TPlugJob::Save( void )
{
    // Internal Save() handler. This function saves the file even if
    // m_jobInfo.isOpen is false, so it is up to calling functions to
    // ensure a job is open before properties are modified.

    // If in sim mode, we don't save the file but report that we did
    if( m_bInSimMode )
        return true;

    bool bSuccess = false;

    _di_IXMLDocument xmlDoc = NULL;

/*
    try
    {
        xmlDoc = LoadXMLDocument( m_fileName );

        if( xmlDoc == NULL )
            Abort();

        xmlDoc->NodeIndentStr = "  ";
        xmlDoc->Options  = TXMLDocOptions() << doNodeAutoIndent << doAttrNull << doAutoPrefix << doNamespaceDecl;

        _di_IXMLNode rootNode = xmlDoc->DocumentElement;

        if( rootNode == NULL )
            Abort();

        // Update job params and rep info
        if( !SaveEntryToNode( rootNode, sJobParamsNode, m_jobParams ) )
            Abort();

        // Refresh the main comments node
        rootNode->ChildNodes->Delete( sMainComments );

        _di_IXMLNode commentsNode = FindOrCreateNode( rootNode, sMainComments );
        m_mainComments.SaveToNode( commentsNode );

        // Refresh the sections list
        rootNode->ChildNodes->Delete( sSectionRecs );

        _di_IXMLNode sectionsNode = FindOrCreateNode( rootNode, sSectionRecs );
        m_sectionRecs.SaveToNode( sectionsNode );

        // Refresh the connections
        rootNode->ChildNodes->Delete( sConnRecs );

        _di_IXMLNode connsNode = FindOrCreateNode( rootNode, sConnRecs );
        m_connections.SaveToNode( connsNode );

        // Refresh the calibration
        rootNode->ChildNodes->Delete( sCalibrationRecs );

        _di_IXMLNode calNode = FindOrCreateNode( rootNode, sCalibrationRecs );
        m_calibrationRecs.SaveToNode( calNode );

        // Done adding elements - save the file now
        xmlDoc->SaveToFile( m_fileName );

        // Fall through mean succcess
        bSuccess = true;
    }
    catch( ... )
    {
    }

    if( xmlDoc != NULL )
        xmlDoc.Release();
*/
    return bSuccess;
}


bool TPlugJob::GetJobComment( int whichComment, JOB_COMMENT& comment )
{
    if( ( whichComment >= 0 ) && ( whichComment < m_jobComments.Count ) )
    {
        comment = m_jobComments[whichComment];
        return true;
    }

    return false;
}


void TPlugJob::AddJobComment( const String& commentText )
{
    if( m_jobParams.isDone )
        return;

    JOB_COMMENT newComment;

    newComment.creationNbr = GetNextCreationNbr();
    newComment.entryTime   = Now();
    newComment.entryText   = commentText;

    m_jobComments.Append( newComment );

    Save();

    if( m_onCommentAdded )
        m_onCommentAdded( this );

    // Add this comment to the current job log.
    LogComment( newComment );
}


bool TPlugJob::GetCalibrationRec( int whichCalibrationRec, CALIBRATION_REC& calibrationRecs )
{
    return false;
}


void TPlugJob::AddCalibrationRec( CALIBRATION_REC& newCalibrationRec )
{
}


void TPlugJob::SaveBackup( void )
{
}


void TPlugJob::SetJobLogDirectory( const String& sJobLogDir )
{
    // If we are being given a new job dir, must recreate the
    // job log there
    String newLogDir = IncludeTrailingBackslash( sJobLogDir );

    bool bRecreateJobLog = ( newLogDir.CompareIC( m_jobLogDir ) != 0 );

    m_jobLogDir = newLogDir;

    if( bRecreateJobLog && DirectoryExists( newLogDir.w_str() ) )
    {
        // Delete the log if it already exists
        String sLogName = m_jobLogDir + m_jobLogName;

        DeleteFile( sLogName );

        // Now recreate it
        for( int iComment = 0; iComment < m_jobComments.Count; iComment++ )
        {
            JOB_COMMENT jobComment = m_jobComments[iComment];
            LogComment( jobComment );
        }
    }
}


void TPlugJob::SetCompleted( bool bIsCompleted )
{
    if( bIsCompleted && !m_jobParams.isDone )
    {
        m_jobParams.isDone = true;
        AddJobComment( "Job completed." );
    }
    else if( !bIsCompleted && m_jobParams.isDone )
    {
        m_jobParams.isDone = false;
        AddJobComment( "Job re-opened." );
    }
}


void TPlugJob::SetInSimMode( bool bInSimMode )
{
    bool bWasInSimMode = m_bInSimMode;

    m_bInSimMode = bInSimMode;

    // If we are now in sim mode, add a job comment
    if( !bWasInSimMode && m_bInSimMode )
        AddJobComment( "In simulation mode." );
}


PlugControlState TPlugJob::GetPlugState( int iPlugNbr )
{
    if( ( iPlugNbr < 1 ) || ( iPlugNbr > NBR_OF_PLUGS ) )
        return ePCS_NotLoaded;

    // Note that plug number is 1-based
    return m_PlugInfo[iPlugNbr-1].plugState;
}


void TPlugJob::SetPlugState( int iPlugNbr, PlugControlState eNewState )
{
    if( ( iPlugNbr < 1 ) || ( iPlugNbr > NBR_OF_PLUGS ) )
        return;

    // Make plug number 0-based
    iPlugNbr--;

    // We don't implement any business rules in this class, so if a state
    // change is made record it as given
    if( eNewState == ePCS_Launched )
    {
        if( m_PlugInfo[iPlugNbr].plugState == ePCS_Loaded )
            AddJobComment( "Plug " + IntToStr( iPlugNbr + 1 ) + " launched." );
        else
            AddJobComment( "Plug " + IntToStr( iPlugNbr + 1 ) + " re-launched." );

        m_PlugInfo[iPlugNbr].plugState = ePCS_Launched;
        m_PlugInfo[iPlugNbr].dtLaunch  = Now();
        m_PlugInfo[iPlugNbr].iSecsToDetect = 0;
    }
    else if( m_PlugInfo[iPlugNbr].plugState == ePCS_Launched )
    {
        if( eNewState == ePCS_Detected )
        {
            m_PlugInfo[iPlugNbr].plugState     = ePCS_Detected;
            m_PlugInfo[iPlugNbr].iSecsToDetect = SecondsBetween( Now(), m_PlugInfo[iPlugNbr].dtLaunch );

            AddJobComment( "Plug " + IntToStr( iPlugNbr + 1 ) + " detected, " + IntToStr( m_PlugInfo[iPlugNbr].iSecsToDetect ) + " seconds in transit." );
        }
        else if( eNewState == ePCS_Lost )
        {
            m_PlugInfo[iPlugNbr].plugState     = ePCS_Lost;
            m_PlugInfo[iPlugNbr].iSecsToDetect = SecondsBetween( Now(), m_PlugInfo[iPlugNbr].dtLaunch );

            AddJobComment( "Plug " + IntToStr( iPlugNbr + 1 ) + " lost after " + IntToStr( m_PlugInfo[iPlugNbr].iSecsToDetect ) + " seconds." );
        }
    }
}


bool TPlugJob::LogComment( const JOB_COMMENT& jobComment )
{
    if( !DirectoryExists( m_jobLogDir.w_str() ) )
        return false;

    String sLine;
    sLine.printf( L"%s\t%s\r\n", jobComment.entryTime.DateTimeString().w_str(), jobComment.entryText.w_str() );

    String sLogName = m_jobLogDir + m_jobLogName;

    // Check if we have to write a header line first
    bool bNeedHeader = !FileExists( sLogName );

    // Attempt to write to the log file.
    TFileStream* logStream = NULL;
    bool         logGood   = false;

    try
    {
        if( !FileExists( sLogName ) )
        {
            // File doesn't exist. Create it and write the header.
            logStream = new TFileStream( sLogName, fmCreate );
        }
        else
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( sLogName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }

        if( bNeedHeader )
        {
            AnsiString sHdrLine = "Date\tEvent\r\n";
            logStream->Write( sHdrLine.c_str(), sHdrLine.Length() );
        }

        AnsiString sALine = sLine;
        logStream->Write( sALine.c_str(), sALine.Length() );

        logGood = true;
    }
    catch( ... )
    {
        logGood = false;
    }

    // Close the file (if it is open).
    if( logStream != NULL )
        delete logStream;

    return logGood;
}

