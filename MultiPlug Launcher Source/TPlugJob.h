#ifndef TPlugJobH
#define TPlugJobH

#include <XMLIntf.hpp>

#include "UnitsOfMeasure.h"


#define NBR_OF_PLUGS  2

// The JOB_INFO contains 'unchangeable' information about a job.
// The launch order array contains -1 for any elements that are
// not loaded, otherwise the array element contains a zero-based
// launch order.
typedef struct {
    String           clientName;
    String           location;
    String           customerRep;
    String           tescoTech;
    UNITS_OF_MEASURE unitsOfMeasure;
    TDateTime        jobStartTime;
    int              nbrPlugs;
    bool             usingElecDetect;
    int              launchOrder[NBR_OF_PLUGS];
} PLUG_JOB_INFO;

// JOB_PARAMS contains information about a job that can change
// through the job
typedef struct {
    bool             isDone;
    int              lastCreationNbr;
} JOB_PARAMS;


typedef struct {
    int       creationNbr;
    TDateTime entryTime;
    String    entryText;
} JOB_COMMENT;

typedef struct {
    int       creationNbr;
    String    serNbr;
    String    fwRev;
    String    cfgStraps;
    String    mfgInfo;
    String    mfgDate;
    String    lastCalDate;
    String    pressureOffset;
    String    pressureSpan;
    String    battCapacity;
    String    battType;
} CALIBRATION_REC;


// Plugs can exist in one of the following states
typedef enum {
    ePCS_NotLoaded,
    ePCS_Loaded,
    ePCS_Launched,
    ePCS_Detected,
    ePCS_Lost,
    ePCS_NbrStates
} PlugControlState;


// Define a template class to manage the arrays of a structure type
template <class T> class TStructArray
{
  private:
    int m_allocElements;      // number of elements memory currently allocated for
    int m_allocIncrease;      // incremental size of memory added when more memory needed
    T*  m_pData;              // pointer to the element array
    int m_nbrElements;        // number of elements in the array (<= m_allocElements)

    void CheckAndAllocMem( int nbrElements );

  public:
    TStructArray();
    ~TStructArray();

    const T& operator[] (int i) const { return m_pData[i]; }

    TStructArray& operator  = ( const TStructArray& equ );
    TStructArray& operator += ( const TStructArray& plus );

    void Clear( void ) { m_nbrElements = 0; }

    void Append( const T &add );
    void Insert( int index, const T& insert );
    void Del( int index );
    void Modify( int index, const T& modify );

    void LoadFromNode( _di_IXMLNode aNode );
    void SaveToNode( _di_IXMLNode aNode );

    __property int Count = { read = m_nbrElements };
};


class TPlugJob : public TObject
{
  private:

  protected:

    String           m_jobLogDir;
    String           m_jobLogName;

    bool             m_jobLoaded;
    PLUG_JOB_INFO    m_jobInfo;
    JOB_PARAMS       m_jobParams;

    bool             m_bInSimMode;

    struct {
        PlugControlState plugState;
        int              iLaunchOrder;
        TDateTime        dtLaunch;
        int              iSecsToDetect;
    } m_PlugInfo[NBR_OF_PLUGS];

    int GetNextCreationNbr( void );
        // Returns a unique sequence number for a section rec, connection
        // rec, or main comment

    bool Save( void );
        // Internal Save() handler. This function saves the file even if
        // m_jobInfo.isOpen is false, so it is up to calling functions to
        // ensure a job is open before properties are modified.

    TStructArray<JOB_COMMENT> m_jobComments;
    int GetNbrJobComments( void ) { return m_jobComments.Count; }

    int GetNbrCalibrationRecs( void ) { return 0; }

    void SetCompleted( bool bIsCompleted );
    void SetInSimMode( bool bInSimMode );

    bool LogComment( const JOB_COMMENT& jobComment );

    PlugControlState GetPlugState( int iPlugNbr );
    void             SetPlugState( int iPlugNbr, PlugControlState eNewState );

    // Event handlers
    TNotifyEvent m_onCommentAdded;

  public:

    virtual __fastcall TPlugJob( const PLUG_JOB_INFO& jobInfo );
    virtual __fastcall ~TPlugJob();

    __property UNITS_OF_MEASURE Units = { read = m_jobInfo.unitsOfMeasure };
        // Returns the units of measure in use for this job

    __property bool Loaded = { read = m_jobLoaded };
        // Returns true if the job was successfully loaded

    __property bool Completed = { read = m_jobParams.isDone, write = SetCompleted };
        // Gets / sets completion status. No changes are allowed to completed jobs

    // Generic read-only properties
    __property UnicodeString    Client    = { read = m_jobInfo.clientName };
    __property UnicodeString    Location  = { read = m_jobInfo.location };
    __property TDateTime        StartTime = { read = m_jobInfo.jobStartTime };
    __property bool             InSimMode = { read = m_bInSimMode, write = SetInSimMode };

    // Plugs
    __property PlugControlState PlugState[int] = { read = GetPlugState, write = SetPlugState };
        // Note: plug index is 1-based here

    // Main Comments
    __property int NbrJobComments  = { read = GetNbrJobComments };
    bool           GetJobComment( int whichComment, JOB_COMMENT& comment );
    void           AddJobComment( const String& commentText );

    __property TNotifyEvent OnCommentAdded = { read = m_onCommentAdded, write = m_onCommentAdded };

    // Calibration Records
    __property int NbrCalibrationRecs = { read = GetNbrCalibrationRecs };
    bool           GetCalibrationRec( int whichCalibrationRec, CALIBRATION_REC& calibrationRecs );
    void           AddCalibrationRec( CALIBRATION_REC& newCalibrationRec );

    void           SaveBackup( void );

    void           SetJobLogDirectory( const String& sJobLogDir );
};

#endif
