#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>

#include "TPlugJobDetailsDlg.h"
#include "MPLRegistryInterface.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TPlugJobDetailsForm *PlugJobDetailsForm;


//
// Class Functions
//

__fastcall TPlugJobDetailsForm::TPlugJobDetailsForm(TComponent* Owner) : TForm(Owner)
{
    // Some edits are not required, while other edits must contain numeric
    // values. Assign a bit flag to each edit's tag member here defining
    // its requirements.
    #define CTRL_MANDATORY  0x0001
    #define EDIT_INTEGER    0x0002
    #define EDIT_POS_FLOAT  0x0004
    #define EDIT_FLOAT      0x0008

    ClientEdit->Tag     = CTRL_MANDATORY;
    LocnEdit->Tag       = CTRL_MANDATORY;
    RepCombo->Tag       = CTRL_MANDATORY;
    TescoTechCombo->Tag = CTRL_MANDATORY;
}


void __fastcall TPlugJobDetailsForm::CreateParams(Controls::TCreateParams &Params)
{
    TForm::CreateParams( Params );
    Params.ExStyle   = Params.ExStyle | WS_EX_APPWINDOW;
    Params.WndParent = ParentWindow;
}


bool TPlugJobDetailsForm::CreateNewJob( PLUG_JOB_INFO& newJobInfo, UnicodeString& jobFileName )
{
    // Presents the job details dialog and creates a new job if all entered params are legit.
    // Returns true on success, populating jobName with the new job's filename.

    // First, clear all edits
    ClearGBEdits( DetailsGB );

    // All group boxes are enabled when creating a new job
    EnableGBControls( DetailsGB, true );

    // Okay and cancel btns are visible when creating a new job
    OKBtn->Visible     = true;
    CancelBtn->Caption = L"Cancel";

    // Populate client and location fields - these are read-only fields
    ClientEdit->Text = newJobInfo.clientName;
    LocnEdit->Text   = newJobInfo.location;

    // Populate name drop-downlists
    PopulateRepCombos();

    // Update the caption
    Caption = L"Create New Job";

    ActiveControl = RepCombo;

    // Show the dialog. Validation is performed on the OK btn click.
    if( ShowModal() != mrOk )
        return false;

    // Ok was pressed, and all params are valid. Return the information
    // TODO
    return true;
}


void TPlugJobDetailsForm::ShowJobInfo( const PLUG_JOB_INFO& newJobInfo )
{
    ShowModal();
}


void TPlugJobDetailsForm::PopulateJobInfo( const PLUG_JOB_INFO& jobInfo )
{
    ClientEdit->Text     = jobInfo.clientName;
    LocnEdit->Text       = jobInfo.location;
    RepCombo->Text       = jobInfo.customerRep;
    TescoTechCombo->Text = jobInfo.tescoTech;
}


void TPlugJobDetailsForm::PopulateRepCombos( void )
{
    TStringList* nameList = new TStringList();

    GetRecentNameList( RNL_CLIENT_REP, nameList );
    nameList->Sorted = true;
    nameList->Sort();

    RepCombo->Items->Assign( nameList );

    GetRecentNameList( RNL_TESCO_REP, nameList );
    nameList->Sorted = true;
    nameList->Sort();

    TescoTechCombo->Items->Assign( nameList );

    delete nameList;

    // Clear any existing text from the combos
    RepCombo->Text       = "";
    TescoTechCombo->Text = "";
}


void TPlugJobDetailsForm::ClearGBEdits( TGroupBox* aGB )
{
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
    {
        TEdit* currEdit = dynamic_cast<TEdit*>( aGB->Controls[iCtrl] );

        if( currEdit != NULL )
        {
            currEdit->Text = L"";
            continue;
        }

        // This function also clears combo boxes
        TComboBox* currCombo = dynamic_cast<TComboBox*>( aGB->Controls[iCtrl] );

        if( currCombo != NULL )
        {
            currCombo->Items->Clear();
            currCombo->Text = L"";
        }
    }
}


void TPlugJobDetailsForm::EnableGBControls( TGroupBox* aGB, bool isEnabled )
{
    // Handle special case for DetailsGB
    if( aGB == DetailsGB )
    {
        RepCombo->Enabled       = isEnabled;
        TescoTechCombo->Enabled = isEnabled;
    }
    else
    {
        for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
            aGB->Controls[iCtrl]->Enabled = isEnabled;
    }
}


TWinControl* TPlugJobDetailsForm::FindEmptyGBCtrl( TGroupBox* aGB )
{
    // Look for an empty edit or combo in the group box. Ignore
    // controls that are not mandatory or ones that are disabled
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
    {
        if( !aGB->Controls[iCtrl]->Enabled )
            continue;

        if( ( aGB->Controls[iCtrl]->Tag & CTRL_MANDATORY ) != 0 )
        {
            TEdit* currEdit = dynamic_cast<TEdit*>( aGB->Controls[iCtrl] );

            if( ( currEdit != NULL ) && ( currEdit->Enabled ) )
            {
                if( currEdit->Text.Trim().Length() == 0 )
                    return currEdit;
            }

            TComboBox* currCombo = dynamic_cast<TComboBox*>( aGB->Controls[iCtrl] );

            if( ( currCombo != NULL ) && ( currCombo->Enabled ) )
            {
                if( currCombo->Text.Trim().Length() == 0 )
                    return currCombo;
            }
        }
    }

     return NULL;
}


void __fastcall TPlugJobDetailsForm::OKBtnClick(TObject *Sender)
{
    // Validate fields. If all are good, then set modal result to mrOk

    // First, no edits can contain blank values
    TWinControl* blankCtrl = FindEmptyGBCtrl( DetailsGB );

    if( blankCtrl != NULL )
    {
        MessageDlg( "Field cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        if( blankCtrl->Enabled )
            ActiveControl = blankCtrl;

        return;
    }

    // Validate plug configuration. At least one plug must be loaded and
    // multiple plugs cannot be launched at the same time. -1 is used to
    // indicate a plug is not loaded. The item index is one greater than
    // the values passed to data manager.
    int iLaunchOrders[NBR_OF_PLUGS];

    iLaunchOrders[0] = Plug1CfgCombo->ItemIndex - 1;
    iLaunchOrders[1] = Plug2CfgCombo->ItemIndex - 1;
    iLaunchOrders[2] = Plug3CfgCombo->ItemIndex - 1;
    iLaunchOrders[3] = Plug4CfgCombo->ItemIndex - 1;
    iLaunchOrders[4] = Plug5CfgCombo->ItemIndex - 1;

    // Check for at least one loaded plug first
    bool bHaveLoadedPlug = false;

    for( int iPlug = 0; iPlug < NBR_OF_PLUGS; iPlug++ )
    {
        if( iLaunchOrders[iPlug] != -1 )
        {
            bHaveLoadedPlug = true;
            break;
        }
    }

    if( !bHaveLoadedPlug )
    {
        MessageDlg( "At least one plug must be loaded.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    // Now check for duplicate launch order
    bool bHaveDuplOrder = false;

    for( int iPlug = 0; iPlug < NBR_OF_PLUGS; iPlug++ )
    {
        for( int iCheck = 0; iCheck < NBR_OF_PLUGS; iCheck++ )
        {
            // Don't check against reference plug
            if( iCheck == iPlug )
                continue;

            // Ignore unloaded plugs
            if( iLaunchOrders[iCheck] == -1 )
                continue;

            // Cannot have a duplicate here
            if( iLaunchOrders[iPlug] == iLaunchOrders[iCheck] )
            {
                bHaveDuplOrder = true;
                break;
            }
        }

        if( bHaveDuplOrder )
            break;
    }

    if( bHaveDuplOrder )
    {
        MessageDlg( "You cannot configure multiple plugs to launch at the same time.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    // Lastly check for contiguous launch order.
    bool bHaveLaunch[NBR_OF_PLUGS] = { false };

    for( int iPlug = 0; iPlug < NBR_OF_PLUGS; iPlug++ )
    {
        if( iLaunchOrders[iPlug] != -1 )
            bHaveLaunch[ iLaunchOrders[iPlug] ] = true;
    }

    // Must start with launches. Find the first empty launch number
    int iLaunchIndex = 0;

    while( iLaunchIndex < NBR_OF_PLUGS )
    {
        if( !bHaveLaunch[ iLaunchIndex ] )
            break;

        iLaunchIndex++;
    }

    // The remaining launch positions must be unused
    while( iLaunchIndex < NBR_OF_PLUGS )
    {
        if( bHaveLaunch[ iLaunchIndex ] )
        {
            MessageDlg( "The plug launch sequence is not in order.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return;
        }

        iLaunchIndex++;
    }

    // Fall through means values are good
    ModalResult = mrOk;
}

