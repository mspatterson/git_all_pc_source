object PlugJobDetailsForm: TPlugJobDetailsForm
  Left = 0
  Top = 0
  Caption = 'Job Details'
  ClientHeight = 236
  ClientWidth = 492
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    492
    236)
  PixelsPerInch = 96
  TextHeight = 13
  object CancelBtn: TButton
    Left = 409
    Top = 203
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    ExplicitTop = 356
  end
  object OKBtn: TButton
    Left = 328
    Top = 203
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'O&K'
    Default = True
    TabOrder = 1
    OnClick = OKBtnClick
    ExplicitTop = 356
  end
  object DetailsGB: TGroupBox
    Left = 8
    Top = 8
    Width = 476
    Height = 105
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Job Details '
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 20
      Width = 75
      Height = 13
      Caption = 'Client Company'
    end
    object Label2: TLabel
      Left = 255
      Top = 20
      Width = 40
      Height = 13
      Caption = 'Location'
    end
    object Label4: TLabel
      Left = 12
      Top = 60
      Width = 123
      Height = 13
      Caption = 'Customer Representative'
    end
    object Label5: TLabel
      Left = 255
      Top = 60
      Width = 50
      Height = 13
      Caption = 'Technician'
    end
    object ClientEdit: TEdit
      Left = 12
      Top = 35
      Width = 221
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
    end
    object LocnEdit: TEdit
      Left = 255
      Top = 35
      Width = 211
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
    end
    object RepCombo: TComboBox
      Left = 12
      Top = 75
      Width = 221
      Height = 21
      TabOrder = 2
    end
    object TescoTechCombo: TComboBox
      Left = 255
      Top = 75
      Width = 211
      Height = 21
      TabOrder = 3
    end
  end
  object PlugCfgGB: TGroupBox
    Left = 8
    Top = 119
    Width = 476
    Height = 78
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Plug Configuration '
    TabOrder = 3
    ExplicitHeight = 87
    object Label3: TLabel
      Left = 36
      Top = 25
      Width = 29
      Height = 13
      Caption = 'Plug 1'
    end
    object Label6: TLabel
      Left = 128
      Top = 25
      Width = 29
      Height = 13
      Caption = 'Plug 2'
    end
    object Label7: TLabel
      Left = 220
      Top = 25
      Width = 29
      Height = 13
      Caption = 'Plug 3'
    end
    object Label8: TLabel
      Left = 312
      Top = 25
      Width = 29
      Height = 13
      Caption = 'Plug 4'
    end
    object Label9: TLabel
      Left = 404
      Top = 25
      Width = 29
      Height = 13
      Caption = 'Plug 5'
    end
    object Plug1CfgCombo: TComboBox
      Left = 12
      Top = 44
      Width = 85
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 0
      Text = 'Not Loaded'
      Items.Strings = (
        'Not Loaded'
        'Launch 1st'
        'Launch 2nd'
        'Launch 3rd'
        'Launch 4th'
        'Launch 5th')
    end
    object Plug2CfgCombo: TComboBox
      Left = 104
      Top = 44
      Width = 85
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 1
      Text = 'Not Loaded'
      Items.Strings = (
        'Not Loaded'
        'Launch 1st'
        'Launch 2nd'
        'Launch 3rd'
        'Launch 4th'
        'Launch 5th')
    end
    object Plug3CfgCombo: TComboBox
      Left = 196
      Top = 44
      Width = 85
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 2
      Text = 'Not Loaded'
      Items.Strings = (
        'Not Loaded'
        'Launch 1st'
        'Launch 2nd'
        'Launch 3rd'
        'Launch 4th'
        'Launch 5th')
    end
    object Plug4CfgCombo: TComboBox
      Left = 288
      Top = 44
      Width = 85
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 3
      Text = 'Not Loaded'
      Items.Strings = (
        'Not Loaded'
        'Launch 1st'
        'Launch 2nd'
        'Launch 3rd'
        'Launch 4th'
        'Launch 5th')
    end
    object Plug5CfgCombo: TComboBox
      Left = 380
      Top = 44
      Width = 85
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 4
      Text = 'Not Loaded'
      Items.Strings = (
        'Not Loaded'
        'Launch 1st'
        'Launch 2nd'
        'Launch 3rd'
        'Launch 4th'
        'Launch 5th')
    end
  end
end
