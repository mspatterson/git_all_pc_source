#ifndef TPlugJobDetailsDlgH
#define TPlugJobDetailsDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>

#include "PlugJobManager.h"

class TPlugJobDetailsForm : public TForm
{
__published:    // IDE-managed Components
    TButton *CancelBtn;
    TButton *OKBtn;
    TGroupBox *DetailsGB;
    TLabel *Label1;
    TLabel *Label2;
    TEdit *ClientEdit;
    TEdit *LocnEdit;
    TLabel *Label4;
    TLabel *Label5;
    TComboBox *RepCombo;
    TComboBox *TescoTechCombo;
    TGroupBox *PlugCfgGB;
    TLabel *Label3;
    TComboBox *Plug1CfgCombo;
    TLabel *Label6;
    TComboBox *Plug2CfgCombo;
    TLabel *Label7;
    TComboBox *Plug3CfgCombo;
    TLabel *Label8;
    TComboBox *Plug4CfgCombo;
    TLabel *Label9;
    TComboBox *Plug5CfgCombo;
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall CreateParams(Controls::TCreateParams &Params);

private:
    void   ClearGBEdits( TGroupBox* aGB );
    void   EnableGBControls( TGroupBox* aGB, bool isEnabled );

    TWinControl* FindEmptyGBCtrl( TGroupBox* aGB );

    void   PopulateJobInfo( const PLUG_JOB_INFO& jobInfo );
    void   PopulateRepCombos( void );

public:
    __fastcall TPlugJobDetailsForm(TComponent* Owner);

    bool CreateNewJob( PLUG_JOB_INFO& newJobInfo, UnicodeString& jobFileName );
        // Presents the job details dialog and returns the user entered params.
        // Returns true on success.

    void ShowJobInfo( const PLUG_JOB_INFO& newJobInfo );
        // Presents the job details dialog and creates a new job if all entered params are legit.
        // Returns true on success, populating jobName with the new job's filename.
};

extern PACKAGE TPlugJobDetailsForm *PlugJobDetailsForm;

#endif
