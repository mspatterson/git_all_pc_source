#include <vcl.h>
#pragma hdrstop

#include "TPlugJobsDlg.h"
#include "TPlugJobDetailsDlg.h"
#include "MPLRegistryInterface.h"
#include "ApplUtils.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TPlugJobsForm *PlugJobsForm;


// Listview columns
typedef enum {
    LVC_CLIENT_NAME,
    LVC_LOCATION,
    LVC_START_TIME,
    NBR_LISTVIEW_COLS
} LIST_VIEW_COL;


__fastcall TPlugJobsForm::TPlugJobsForm(TComponent* Owner) : TForm(Owner)
{
    UnitsRG->Items->Strings[UOM_METRIC  ] = GetUnitsText( UOM_METRIC,   MT_MEASURE_NAME );
    UnitsRG->Items->Strings[UOM_IMPERIAL] = GetUnitsText( UOM_IMPERIAL, MT_MEASURE_NAME );

    UnitsRG->ItemIndex = GetDefaultUnitsOfMeasure();

    pJobList = NULL;
}


void __fastcall TPlugJobsForm::FormDestroy(TObject *Sender)
{
    if( pJobList != NULL )
        delete pJobList;
}


bool TPlugJobsForm::OpenJob( String& jobFileName, String preferredJobFileName )
{
    // Return true if an existing job is selected for new job is created.
    // Return false is cancel is pressed.
    JobPgCtrl->ActivePageIndex = 0;
    JobsFilterRG->ItemIndex    = 0;

    JobsFilterClick( JobsFilterRG );

    // If the preferredJobName is not blank, select it in the list of jobs
    JobsListView->ItemIndex = -1;

    if( ( preferredJobFileName.Length() > 0 ) && ( pJobList != NULL ) )
    {
        int preferredJobIndex = -1;

        for( int iJob = 0; iJob < pJobList->Count; iJob++ )
        {
            JOB_LIST_ITEM jobItem;

            if( pJobList->GetItem( iJob, jobItem ) )
            {
                if( jobItem.jobFileName.CompareIC( preferredJobFileName ) == 0 )
                {
                    preferredJobIndex = iJob;
                    break;
                }
            }
        }

        // Now need to find this item in the listview
        if( preferredJobIndex >= 0 )
        {
            for( int iItem = 0; iItem < JobsListView->Items->Count; iItem++ )
            {
                TListItem* pItem = JobsListView->Items->Item[iItem];

                if( (int)( pItem->Data ) == preferredJobIndex )
                {
                    JobsListView->ItemIndex = iItem;
                    break;
                }
            }
        }
    }

    m_selJobName = L"";

    if( ShowModal() != mrOk )
        return false;

    jobFileName = m_selJobName;

    AddToMostRecentJobsList( jobFileName );

    return true;
}


void __fastcall TPlugJobsForm::JobsFilterClick(TObject *Sender)
{
    // If the box is checked, show all jobs in the jobs data dir.
    // Otherwise, show the MRU list of jobs

    // First, delete any existing job list
    if( pJobList != NULL )
    {
        delete pJobList;
        pJobList = NULL;
    }

    // Create the job list based on the selected filter option
    if( JobsFilterRG->ItemIndex == 0 )
    {
        TStringList* slRecentJobs = new TStringList();

        GetMostRecentJobsList( slRecentJobs );

        pJobList = new TJobList( slRecentJobs );

        delete slRecentJobs;
    }
    else
    {
        pJobList = new TJobList( GetJobDataDir() );
    }

    PopulateJobsList();
}


void __fastcall TPlugJobsForm::JobsListViewDblClick(TObject *Sender)
{
    OKBtnClick( OKBtn );
}


void __fastcall TPlugJobsForm::OKBtnClick(TObject *Sender)
{
    // Okay action depends on the sheet we are on.
    if( JobPgCtrl->ActivePageIndex == 0 )
    {
        // Showing list of jobs. If an item is selected, validate it. The
        // job file name will be the index in the jobs list that is saved
        // in the listview item's Data member.
        if( JobsListView->ItemIndex >= 0 )
        {
            TListItem* pItem = JobsListView->Items->Item[ JobsListView->ItemIndex ];

            int jobListIndex = (int)( pItem->Data );

            JOB_LIST_ITEM jobItem;

            if( pJobList->GetItem( jobListIndex, jobItem ) )
            {
                m_selJobName = jobItem.jobFileName;

                ModalResult = mrOk;
                return;
            }
        }

        // Fall through means nothing selected or there was an error opening
        // the selected item
        Beep();

        return;
    }

    // Fall through means creating a new job. Validate entries
    UnicodeString clientName = NewClientEdit->Text.Trim();
    UnicodeString jobLocn    = NewLocnEdit->Text.Trim();

    if( clientName.Length() == 0 )
    {
        MessageDlg( "Client name cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewClientEdit;
        return;
    }

    if( jobLocn.Length() == 0 )
    {
        MessageDlg( "Location cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewLocnEdit;
        return;
    }

    if( JobsExistFor( GetJobDataDir(), clientName, jobLocn ) > 0 )
    {
        int mrResult = MessageDlg( "One or more jobs for this client at this location already exists."
                                   "\r  Press Yes to create a new job at this location anyway."
                                   "\r  Press Cancel to enter different parameters for this job.", mtInformation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

        if( mrResult != mrYes )
        {
            ActiveControl = NewClientEdit;
            return;
        }
    }

    // New job info looks good... try to create the job
    PLUG_JOB_INFO pjiNewJob;
    UnicodeString newJobFileName;

    pjiNewJob.clientName     = clientName;
    pjiNewJob.location       = jobLocn;
    pjiNewJob.customerRep    = "";
    pjiNewJob.tescoTech      = "";
    pjiNewJob.unitsOfMeasure = (UNITS_OF_MEASURE)( UnitsRG->ItemIndex );

    // If the job cannot be created, the JobDetailsForm would have displayed
    // an error message. Just quit in that case
    if( !PlugJobDetailsForm->CreateNewJob( pjiNewJob, newJobFileName ) )
        return;

    // Save used units of entry for next job
    SetDefaultUnitsOfMeasure( UnitsRG->ItemIndex );

    m_selJobName = newJobFileName;

    ModalResult = mrOk;
}


void __fastcall TPlugJobsForm::JobsListBoxDblClick(TObject *Sender)
{
    OKBtnClick( OKBtn );
}


void TPlugJobsForm::PopulateJobsList( void )
{
    // Populate the listview. Need to bear in mind that the listview
    // sorts items based on each item's Caption. Therefore, the order
    // of the items in the listview may not be the same as the order
    // in the jobs list.
    JobsListView->Items->Clear();

    for( int iJob = 0; iJob < pJobList->Count; iJob++ )
    {
        JOB_LIST_ITEM jobItem;

        if( pJobList->GetItem( iJob, jobItem ) )
        {
            TListItem* pNewItem = JobsListView->Items->Add();

            pNewItem->Caption = jobItem.jobInfo.clientName;
            pNewItem->SubItems->Add( jobItem.jobInfo.location );
            pNewItem->SubItems->Add( jobItem.jobInfo.jobStartTime.FormatString( "yyyy-mm-dd hh:nn:ss" ) );

            // Store the index of the item in the job list in the listview's data member.
            // We'll need this index later
            pNewItem->Data = (void *)iJob;
        }
    }
}

