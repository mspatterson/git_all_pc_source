object PlugJobsForm: TPlugJobsForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Select Job'
  ClientHeight = 334
  ClientWidth = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  DesignSize = (
    482
    334)
  PixelsPerInch = 96
  TextHeight = 13
  object JobPgCtrl: TPageControl
    Left = 4
    Top = 4
    Width = 474
    Height = 294
    ActivePage = ExistingSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object ExistingSheet: TTabSheet
      Caption = 'Existing Jobs'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        466
        266)
      object JobsListView: TListView
        Left = 11
        Top = 12
        Width = 440
        Height = 165
        Anchors = [akLeft, akTop, akRight, akBottom]
        Columns = <
          item
            Caption = 'Client'
            Width = 150
          end
          item
            Caption = 'Location'
            Width = 150
          end
          item
            Caption = 'Date'
            Width = 100
          end>
        GridLines = True
        ReadOnly = True
        RowSelect = True
        SortType = stText
        TabOrder = 0
        ViewStyle = vsReport
        OnDblClick = JobsListViewDblClick
      end
      object JobsFilterRG: TRadioGroup
        Left = 11
        Top = 187
        Width = 440
        Height = 68
        Caption = ' Display Filter '
        ItemIndex = 0
        Items.Strings = (
          'Show most recent jobs'
          'Show all jobs')
        TabOrder = 1
        OnClick = JobsFilterClick
      end
    end
    object NewSheet: TTabSheet
      Caption = 'New Job'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        466
        266)
      object NewJobPanel: TPanel
        Left = 11
        Top = 12
        Width = 442
        Height = 241
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkTile
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Client'
        end
        object Label2: TLabel
          Left = 12
          Top = 48
          Width = 40
          Height = 13
          Caption = 'Location'
        end
        object NewClientEdit: TEdit
          Left = 76
          Top = 13
          Width = 341
          Height = 21
          TabOrder = 0
        end
        object NewLocnEdit: TEdit
          Left = 76
          Top = 45
          Width = 341
          Height = 21
          TabOrder = 1
        end
        object UnitsRG: TRadioGroup
          Left = 12
          Top = 80
          Width = 185
          Height = 75
          Caption = ' Units of Measure '
          ItemIndex = 0
          Items.Strings = (
            'Metric'
            'Imperial')
          TabOrder = 2
        end
      end
    end
  end
  object CancelBtn: TButton
    Left = 399
    Top = 304
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object OKBtn: TButton
    Left = 318
    Top = 304
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'O&K'
    Default = True
    TabOrder = 1
    OnClick = OKBtnClick
  end
end
