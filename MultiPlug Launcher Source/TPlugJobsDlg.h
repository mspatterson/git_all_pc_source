//---------------------------------------------------------------------------
#ifndef TPlugJobsDlgH
#define TPlugJobsDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "PlugJobManager.h"
//---------------------------------------------------------------------------
class TPlugJobsForm : public TForm
{
__published:    // IDE-managed Components
    TPageControl *JobPgCtrl;
    TButton *CancelBtn;
    TButton *OKBtn;
    TTabSheet *ExistingSheet;
    TTabSheet *NewSheet;
    TPanel *NewJobPanel;
    TLabel *Label1;
    TLabel *Label2;
    TEdit *NewClientEdit;
    TEdit *NewLocnEdit;
    TRadioGroup *UnitsRG;
    TListView *JobsListView;
    TRadioGroup *JobsFilterRG;
    void __fastcall JobsFilterClick(TObject *Sender);
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall JobsListBoxDblClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall JobsListViewDblClick(TObject *Sender);

private:
    TJobList* pJobList;

    UnicodeString m_selJobName;

    void PopulateJobsList( void );

public:
    __fastcall TPlugJobsForm(TComponent* Owner);

    bool OpenJob( String& jobFileName, String preferredJobFileName );
};
//---------------------------------------------------------------------------
extern PACKAGE TPlugJobsForm *PlugJobsForm;
//---------------------------------------------------------------------------
#endif
