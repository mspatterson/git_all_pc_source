//
// Custom Messages passed by classes to the main form
//

#ifndef Messages_h
#define Messages_h

    #define WM_COMPLETE_JOB          ( WM_APP + 1 )
         // WParam: RFU
         // LParam: RFU

    #define WM_REOPEN_JOB            ( WM_APP + 2 )
         // WParam: RFU
         // LParam: RFU

    #define WM_SHOW_SYS_SETTINGS_DLG ( WM_APP + 3 )
         // WParam: RFU
         // LParam: RFU

    #define WM_POLLER_EVENT          ( WM_APP + 4 )
         // WParam: See CommsMgr.h
         // LParam: See CommsMgr.h

    #define WM_ACK_EVENT             ( WM_APP + 5 )
         // WParam: An AckEventType type
         // LParam: RFU

#endif

 