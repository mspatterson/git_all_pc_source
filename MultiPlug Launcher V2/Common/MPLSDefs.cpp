//******************************************************************************
//
//  MPLSDefs.cpp: low-level helper methods common across the application
//
//******************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "MPLSDefs.h"

#pragma package(smart_init)


//
// Private Methods
//

static int BoolToInt( bool bData )
{
    return( bData ? 1 : 0 );
}


static bool IntToBool( int iData )
{
    return( ( iData != 0 ) ? true : false );
}


static String BoolStateToString( BoolState eBS )
{
    switch( eBS )
    {
        case eBS_On:   return "On";
        case eBS_Off:  return "Off";
        default:       return "?";
    }
}


static BoolState StringToBoolState( const String& sString )
{
    if( sString == BoolStateToString( eBS_Off ) )
        return eBS_Off;

    if( sString == BoolStateToString( eBS_On ) )
        return eBS_On;

    return eBS_Unknown;
}


//
// Public Constants
//

const MPLS_CAL_DATA_STRUCT_V2 DefaultMPLSV2CalData = {
    0,           // tankPressOffset
    0.0005025,   // tankPressSpan
    0,           // regPressOffset
    0.00005044,  // regPressSpan
};


const String CanisterImageResNames[eCI_NbrCanisterImages] = {
    "MPLSCfg_Empty",
    "MPLSCfg_2_Short",
    "MPLSCfg_1_Long",
    "MPLSCfg_1_Short",
    "MPLSCfg_Lower_Dropped",
};


const String SystemEventTypeText[eSE_NbrSystemEvents] = {
    "None",                     // eSE_None
    "Connected to MPLS",        // eSE_Connected
    "Configuration Mismatch",   // eSE_ConfigMismatch
    "State Mismatch",           // eSE_StateMismatch
    "Launching",                // eSE_Launching
    "Launch done",              // eSE_LaunchDone
    "Cleaning",                 // eSE_Cleaning
    "Cleaning done",            // eSE_CleanDone
    "Flag triggered",           // eSE_FlagTriggered
    "Clearing flag",            // eSE_FlagClearing
    "Flag Cleared",             // eSE_FlagCleared
    "Hardware error",           // eSE_HWError
    "Low battery alarm",        // eSE_LowBattery
    "Other alarm",              // eSE_OtherAlarm,
};


//
// Public Methods
//

bool ConvertStringToRFCRec( LOGGED_RFC_DATA& rfcRec, const String& sString )
{
    // Private method that tries to convert a CSV string to a LOGGED_RFC_DATA struct.
    TStringList* pCSVStrings = new TStringList();

    pCSVStrings->CommaText = sString;

    bool bStringGood = false;

    if( pCSVStrings->Count == 2 + 6 + 2 * ( 6 + NBR_ISOL_OUTS_PER_TEC ) + 1 + MAX_PLUG_DET_READINGS )
    {
        // Note: we do not parse out the first two fields (date, time)
        rfcRec.tRFC              = pCSVStrings->Strings[2].ToIntDef( 0 );
        rfcRec.wMsecs            = pCSVStrings->Strings[3].ToIntDef( 0 );
        rfcRec.systemStatusBits  = pCSVStrings->Strings[4].ToIntDef( 0 );
        rfcRec.rpm               = pCSVStrings->Strings[5].ToIntDef( 0 );
        rfcRec.temp              = StrToFloatDef( pCSVStrings->Strings[6], 0.0 );
        rfcRec.flagInActive      = IntToBool( pCSVStrings->Strings[7].ToIntDef( 0 ) );

        rfcRec.tecState[eMU_MasterMPLS].tankPress  = StrToFloatDef( pCSVStrings->Strings[8], 0.0 );
        rfcRec.tecState[eMU_MasterMPLS].regPress   = StrToFloatDef( pCSVStrings->Strings[9], 0.0 );
        rfcRec.tecState[eMU_MasterMPLS].battVolts  = StrToFloatDef( pCSVStrings->Strings[10], 0.0 );
        rfcRec.tecState[eMU_MasterMPLS].battType   = pCSVStrings->Strings[11].ToIntDef( 0 );
        rfcRec.tecState[eMU_MasterMPLS].solCurrent = pCSVStrings->Strings[12].ToIntDef( 0 );
        rfcRec.tecState[eMU_MasterMPLS].solActive  = pCSVStrings->Strings[13].ToIntDef( 0 );

        for( int iReading = 0; iReading < NBR_ISOL_OUTS_PER_TEC; iReading++ )
            rfcRec.tecState[eMU_MasterMPLS].eIsolOutState[iReading] = StringToBoolState( pCSVStrings->Strings[14 + iReading]  );

        rfcRec.tecState[eMU_SlaveMPLS].tankPress  = StrToFloatDef( pCSVStrings->Strings[18], 0.0 );
        rfcRec.tecState[eMU_SlaveMPLS].regPress   = StrToFloatDef( pCSVStrings->Strings[19], 0.0 );
        rfcRec.tecState[eMU_SlaveMPLS].battVolts  = StrToFloatDef( pCSVStrings->Strings[20], 0.0 );
        rfcRec.tecState[eMU_SlaveMPLS].battType   = pCSVStrings->Strings[21].ToIntDef( 0 );
        rfcRec.tecState[eMU_SlaveMPLS].solCurrent = pCSVStrings->Strings[22].ToIntDef( 0 );
        rfcRec.tecState[eMU_SlaveMPLS].solActive  = pCSVStrings->Strings[23].ToIntDef( 0 );

        for( int iReading = 0; iReading < NBR_ISOL_OUTS_PER_TEC; iReading++ )
            rfcRec.tecState[eMU_SlaveMPLS].eIsolOutState[iReading] = StringToBoolState( pCSVStrings->Strings[24 + iReading] );

        rfcRec.nbrPlugDetReadings = pCSVStrings->Strings[28].ToIntDef( 0 );

        for( int iReading = 0; iReading < MAX_PLUG_DET_READINGS; iReading++ )
            rfcRec.plugDetReadings[iReading] = pCSVStrings->Strings[29 + iReading].ToIntDef( 0 );

        // Fall through means all fields parsed. The rec is good if the RFC timestamp is not zero
        if( rfcRec.tRFC != 0 )
            bStringGood = true;
    }

    return bStringGood;
}


String ConvertRFCRecToString( const LOGGED_RFC_DATA& rfcRec )
{
    // Private method that converts a RFC record to a string.
    String sDateTime;

    struct tm* pGMT = gmtime( &rfcRec.tRFC );

    if( pGMT != NULL )
        sDateTime.printf( L"%d-%02d-%02d,%02d:%02d:%02d,", pGMT->tm_year + 1900, pGMT->tm_mon + 1, pGMT->tm_mday, pGMT->tm_hour, pGMT->tm_min, pGMT->tm_sec );
    else
        sDateTime = ( "BadDate,BadTime," );

    String sBaseData;
    sBaseData.printf( L"%d,%hu,%hu,%d,%7.1f,%d,", rfcRec.tRFC, rfcRec.wMsecs, rfcRec.systemStatusBits, rfcRec.rpm, rfcRec.temp, BoolToInt( rfcRec.flagInActive ) );

    String sMasterData;
    sMasterData.printf( L"%7.0f,%7.0f,%7.3f,%d,%d,%d,",
                             rfcRec.tecState[eMU_MasterMPLS].tankPress,
                             rfcRec.tecState[eMU_MasterMPLS].regPress,
                             rfcRec.tecState[eMU_MasterMPLS].battVolts,
                             rfcRec.tecState[eMU_MasterMPLS].battType,
                             rfcRec.tecState[eMU_MasterMPLS].solCurrent,
                             rfcRec.tecState[eMU_MasterMPLS].solActive );

    String sMasterIsolOuts;
    sMasterIsolOuts.printf( L"%s,%s,%s,%s,", BoolStateToString( rfcRec.tecState[eMU_MasterMPLS].eIsolOutState[0] ),
                                             BoolStateToString( rfcRec.tecState[eMU_MasterMPLS].eIsolOutState[1] ),
                                             BoolStateToString( rfcRec.tecState[eMU_MasterMPLS].eIsolOutState[2] ),
                                             BoolStateToString( rfcRec.tecState[eMU_MasterMPLS].eIsolOutState[3] ) );

    String sSlaveData;
    sSlaveData.printf( L"%7.0f,%7.0f,%7.3f,%d,%d,%d,",
                             rfcRec.tecState[eMU_SlaveMPLS].tankPress,
                             rfcRec.tecState[eMU_SlaveMPLS].regPress,
                             rfcRec.tecState[eMU_SlaveMPLS].battVolts,
                             rfcRec.tecState[eMU_SlaveMPLS].battType,
                             rfcRec.tecState[eMU_SlaveMPLS].solCurrent,
                             rfcRec.tecState[eMU_SlaveMPLS].solActive );

    String sSlaveIsolOuts;
    sSlaveIsolOuts.printf( L"%s,%s,%s,%s,", BoolStateToString( rfcRec.tecState[eMU_SlaveMPLS].eIsolOutState[0] ),
                                            BoolStateToString( rfcRec.tecState[eMU_SlaveMPLS].eIsolOutState[1] ),
                                            BoolStateToString( rfcRec.tecState[eMU_SlaveMPLS].eIsolOutState[2] ),
                                            BoolStateToString( rfcRec.tecState[eMU_SlaveMPLS].eIsolOutState[3] ) );

    String sPlugData = IntToStr( rfcRec.nbrPlugDetReadings );

    for( int iReading = 0; iReading < MAX_PLUG_DET_READINGS; iReading++ )
    {
        String sReading;
        sReading.printf( L",%hu", rfcRec.plugDetReadings[iReading] );

        sPlugData = sPlugData + sReading;
    }

    return sDateTime + sBaseData + sMasterData + sMasterIsolOuts + sSlaveData + sSlaveIsolOuts + sPlugData;
}


String GetRFCRecHdrString( void )
{
    // Returns a string containing the header line for a log file
    return "Date,Time,TimeStamp,MSecs,StatusBits,RPM,Temp,FlagIn,"
           "MasterTankPress,MasterRegPress,MasterBattVolts,MasterBattType,MasterSolCurr,MasterSolActive,"
           "MasterOut1,MasterOut2,MasterOut3,MasterOut4,"
           "SlaveTankPress,SlaveRegPress,SlaveBattVolts,SlaveBattType,SlaveSolCurr,SlaveSolActive,"
           "SlaveOut1,SlaveOut2,SlaveOut3,SlaveOut4,"
           "NbrDetReadings";
}

