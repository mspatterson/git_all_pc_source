#ifndef MPLSDefsH
#define MPLSDefsH

    //******************************************************************************
    //
    //  MPLSDefs.h: low-level definitions common across the application
    //
    //******************************************************************************
    #include <time.h>

    #include "WTTTSProtocolUtils.h"
    #include "TescoShared.h"
    #include "TypeDefs.h"

    // Custom cursors
    typedef enum {
        CC_HANDGRAB = 1,
        CC_HANDFLAT = 2
    } CUSTOM_CURSOR;

    // There is always a master MPLS unit in the system. There can be an
    // optional slave unit as well.
    typedef enum {
        eMU_MasterMPLS,
        eMU_SlaveMPLS,
        eMU_NbrMPLSUnits
    } eMPLSUnit;

    // There are a number of components that make up a MPLS system. This
    // enum identifies them.
    typedef enum {
        eMC_MasterTEC,
        eMC_MasterPIB,
        eMC_SlaveTEC,
        eMC_SlavePIB,
        eMC_PlugDet,
        eMC_NbrMPLSComps
    } eMPLSComponent;

    // Each component identifies itself with the following attributes.
    // Note that PIBs don't report a serial number though.
    typedef struct {
        String sSN;
        DWORD  dwHWRev;
        DWORD  dwFWRev;
    } MPLS_COMP_ID;

    // A MPLS master or slave can be configured the following way
    typedef enum {
        eCC_NoPlug,            // Each of the units (master, and slave if present)
        eCC_OneShort,          // can be set to one of these config enums. However,
        eCC_TwoShort,          // the master can never be set to 'no plug'.
        eCC_OneLong,
        eCC_NbrCanisterConfigs
    } eCanisterConfig;

    // A system can consist of a maximum of four canisters
    typedef enum {
        eCN_NotApplicable = -1,
        eCN_Master1       =  0,
        eCN_Master2       =  1,
        eCN_Slave1        =  2,
        eCN_Slave2        =  3,
        eCN_NbrCanisters  =  4
    } eCanisterNbr;

    // The following enums define system level events that are recorded
    // in a job.
    typedef enum {
        eSE_None,
        eSE_Connected,
        eSE_ConfigMismatch,
        eSE_StateMismatch,
        eSE_Launching,
        eSE_LaunchDone,
        eSE_Cleaning,
        eSE_CleanDone,
        eSE_FlagTriggered,
        eSE_FlagClearing,
        eSE_FlagCleared,
        eSE_HWError,
        eSE_LowBattery,
        eSE_OtherAlarm,
        eSE_NbrSystemEvents
    } eSystemEvent;

    extern const String SystemEventTypeText[];

    // Plugs can exist in one of the following states
    typedef enum {
        ePS_NotLoaded,
        ePS_Loaded,
        ePS_Launched,
        ePS_NbrStates
    } ePlugState;

    // MPLS Canister Image Names
    typedef enum {
        eCI_CanEmpty,
        eCI_Loaded2Short,
        eCI_Loaded1Long,
        eCI_Loaded1Short,
        eCI_LowerDropped,
        eCI_NbrCanisterImages
    } eCanisterImage;

    extern const String CanisterImageResNames[];


    // Solenoid defines
    #define NBR_SOLENOIDS_PER_TEC    5

    // Isolated output defines
    #define NBR_ISOL_OUTS_PER_TEC    4

    // The following struct defines what RFC data gets written to log
    #define MAX_PLUG_DET_READINGS   32

    // System status bit flags
    #define SYS_STATUS_FIELD_PWR_UP             0x0003
    #define SYS_STATUS_FIELD_MASTER_BATT_TYPE   0x000C
    #define SYS_STATUS_FIELD_SLAVE_BATT_TYPE    0x0030
    #define SYS_STATUS_FIELD_SLAVE_TEC_ERR      0x0400
    #define SYS_STATUS_FIELD_SLAVE_PIB_ERR      0x0800
    #define SYS_STATUS_FIELD_MASTER_PIB_ERR     0x1000
    #define SYS_STATUS_FIELD_SLAVE_PD_ERR       0x2000
    #define SYS_STATUS_FIELD_FLAG_TRIGGERED     0x4000

    typedef struct {
        time_t tRFC;             // time of RFC as represented by time() function
        WORD   wMsecs;           // msecs of current second
        WORD   systemStatusBits; // Bit flags as reported in RFC
        int    rpm;
        float  temp;             // int 10ths of temperature units of measure
        bool   flagInActive;     // state of flag-deployed input

        struct {
            float     tankPress;
            float     regPress;
            float     battVolts;
            int       battType;     // Really a BATTERY_TYPE enum
            int       solActive;    // 0 = no solenoid active, 1 - 5 = active sol
            int       solCurrent;   // in mA
            BoolState eIsolOutState[NBR_ISOL_OUTS_PER_TEC];
        } tecState[eMU_NbrMPLSUnits];

        int    nbrPlugDetReadings;
        WORD   plugDetReadings[MAX_PLUG_DET_READINGS];
    } LOGGED_RFC_DATA;

    // Helper methods for converting RFC to / from string
    bool   ConvertStringToRFCRec( LOGGED_RFC_DATA& rfcRec, const String& sString );
    String ConvertRFCRecToString( const LOGGED_RFC_DATA& rfcRec );
    String GetRFCRecHdrString( void );

    // Calibration support
    typedef struct {
        int   tankPressOffset;
        float tankPressSpan;
        int   regPressOffset;
        float regPressSpan;
    } MPLS_CAL_DATA_STRUCT_V2;

    extern const MPLS_CAL_DATA_STRUCT_V2 DefaultMPLSV2CalData;


#endif
