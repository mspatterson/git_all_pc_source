object AutoConnForm: TAutoConnForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Auto-Connect In Progress'
  ClientHeight = 99
  ClientWidth = 268
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    268
    99)
  PixelsPerInch = 96
  TextHeight = 13
  object ConnectingLB: TLabel
    Left = 72
    Top = 19
    Width = 116
    Height = 13
    Anchors = [akTop]
    Caption = 'Scanning for Base Radio'
  end
  object CancelBtn: TButton
    Left = 96
    Top = 55
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'Cancel'
    TabOrder = 0
    OnClick = CancelBtnClick
  end
  object Animation: TTimer
    Enabled = False
    OnTimer = AnimationTimer
    Left = 16
    Top = 32
  end
end
