#include <vcl.h>
#pragma hdrstop

#include "TMPLSMonMain.h"
#include "MPLSDefs.h"
#include "ApplUtils.h"
#include "Comm32.h"
#include "CUDPPort.h"
#include "TypeDefs.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TMPLSMonMain *MPLSMonMain;


// Common Status Grid Key Values
static const String sPortState    ( "Port State" );
static const String sLastCmdTime  ( "Last Cmd Sent" );
static const String sLastRxTime   ( "Last Pkt Rcvd At" );
static const String sLastRxType   ( "Last Pkt Type" );
static const String sLastPktTime  ( "Last Pkt Time" );
static const String sLastPktSeqNbr( "Last Pkt SeqNbr" );
static const String sLastPktTStamp( "Last Pkt Timestamp" );
static const String sPktFlushes   ( "Packet Flushes" );
static const String sPktErrors    ( "Packet Errors" );

// Base Radio Specific Values
static const String sBR0ChanNbr    ( "Radio 0 Chan" );
static const String sBR0RSSI       ( "Radio 0 RSSI" );
static const String sBR0BitRate    ( "Radio 0 Bit Rate" );
static const String sBR1ChanNbr    ( "Radio 1 Chan" );
static const String sBR1RSSI       ( "Radio 1 RSSI " );
static const String sBR1BitRate    ( "Radio 1 Bit Rate" );
static const String sBROutputStatus( "Output Status" );
static const String sBRUpdateRate  ( "Update Rate" );
static const String sBRFWVer       ( "Firmware Ver" );
static const String sBRCtrlSigs    ( "USB Ctrl Sigs" );
static const String sBRRFU0        ( "RFU 0" );
static const String sBRRFU1        ( "RFU 1" );
static const String sBRLastReset   ( "Last Reset" );

// MPLS V2 RFC Values
static const String sMissedRFCs      ( "Missed RFCs" );
static const String sDuplicateRFCs   ( "Duplicate RFCs" );
static const String sSlBattVolts     ( "Slave Battery Volts" );
static const String sSlTankPress     ( "Slave Tank Presure" );
static const String sSlRegPress      ( "Slave Reg Pressure" );
static const String sMastrBattVolts  ( "Master Battery Volts" );
static const String sMastrTankPress  ( "Master Tank Pressure" );
static const String sMastrRegPress   ( "Master Reg Pressure" );
static const String sRPM             ( "RPM" );
static const String sTemp            ( "Temperature" );
static const String sPlugData        ( "Plug Data" );
static const String sSysStatusBits   ( "System Status Bits" );
static const String sSolStatus       ( "Sol Status" );
static const String sIsolOutStatus   ( "Isol Out Status" );
static const String sSlSolCurrent    ( "Slave Sol Current" );
static const String sMastrSolCurrent ( "Master Sol Current" );

// MPLS V2 Event Packet Values
static const String sCfgPktPgNbr     ( "Page Number" );
static const String sCfgPktResult    ( "Result" );
static const String sCfgPktData      ( "Page Data" );
static const String sAckPktCmd       ( "Ack'd Command" );
static const String sAckPktResult    ( "Ack Result" );
static const String sVerPktUnitID    ( "Unit ID" );
static const String sVerPktTECSN     ( "TEC SN" );
static const String sVerPktTECFW     ( "TEC FW" );
static const String sVerPktTECHW     ( "TEC HW" );
static const String sVerPktPIBFW     ( "PIB FW" );
static const String sVerPktPIBHW     ( "PIB HW" );
static const String sVerPktDetSN     ( "Detector SN" );
static const String sVerPktDetFW     ( "Detector FW" );
static const String sVerPktDetHW     ( "Detector HW" );


static const WORD RFCSeqNbrNone = 0xFFFF;


static void EnableGBCtrls( TGroupBox* aGB, bool isEnabled )
{
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
        aGB->Controls[iCtrl]->Enabled = isEnabled;
}


static void ClearGBTags( TGroupBox* aGB )
{
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
        aGB->Controls[iCtrl]->Tag = 0;
}


static void SetEditState( TEdit* anEdit, bool isEnabled )
{
    if( isEnabled )
    {
        anEdit->Color   = clWindow;
        anEdit->Enabled = true;
    }
    else
    {
        anEdit->Color   = clBtnFace;
        anEdit->Enabled = false;
    }
}


typedef struct {
    BYTE   byCmd;
    String sDesc;
} MPLSCmdEntry;

static const MPLSCmdEntry MPLSCommandArray[] =
{
    { WTTTS_CMD_SET_RATE,          "Set Rates" },
    { WTTTS_CMD_MPLS2_QUERY_VER,   "Request Version Info" },
    { WTTTS_CMD_GET_CFG_DATA,      "Request Config Data" },
    { WTTTS_CMD_SET_RF_CHANNEL,    "Set RF Chan" },
    { WTTTS_CMD_MARK_CHAN_IN_USE,  "Mark Channel In Use" },
    { WTTTS_CMD_MPLS2_SET_PIB_OUT, "Set PIB Outputs" },
    { WTTTS_CMD_MPLS2_SET_TEC_PAR, "Set TEC Params" },
    { WTTTS_CMD_MPLS2_SET_DET_PAR, "Set Detector Params" },
    { 0, ""      },   // Last item always has a zero-length string
};

static const MPLSCmdEntry MPLSEventArray[] =
{
    { WTTTS_RESP_MPLS2_RFC,        "RFC" },
    { WTTTS_RESP_MPLS2_VER_PKT,    "Version Packet" },
    { WTTTS_RESP_MPLS2_CFG_DATA,   "Config Data" },
    { WTTTS_RESP_MPLS2_ACK_PKT,    "Ack Packet" },
    { 0, ""      },   // Last item always has a zero-length string
};


static String MPLSEventTypeToStr( BYTE byEventByte )
{
    int iTableIndex = 0;

    String sResult;

    while( !MPLSEventArray[iTableIndex].sDesc.IsEmpty() )
    {
        if( MPLSEventArray[iTableIndex].byCmd == byEventByte )
        {
            sResult = MPLSEventArray[iTableIndex].sDesc;
            break;
        }

        iTableIndex++;
    }

    if( sResult.IsEmpty() )
        sResult = "0x" + IntToHex( byEventByte, 2 );

    return sResult;
}


static String MPLSCmdTypeToStr( BYTE byCmdByte )
{
    int iTableIndex = 0;

    String sResult;

    while( !MPLSCommandArray[iTableIndex].sDesc.IsEmpty() )
    {
        if( MPLSCommandArray[iTableIndex].byCmd == byCmdByte )
        {
            sResult = MPLSCommandArray[iTableIndex].sDesc;
            break;
        }

        iTableIndex++;
    }

    if( sResult.IsEmpty() )
        sResult = "0x" + IntToHex( byCmdByte, 2 );

    return sResult;
}


static String DevSNToString( const BYTE bySNBytes[] )
{
    // An empty array of bytes means the device is not present.
    // 0xFF means SN not programmed
    String sResult;

    if( bySNBytes[0] == 0x00 )
    {
        sResult = "(no device)";
    }
    else if( bySNBytes[0] == 0xFF )
    {
        sResult = "(not programmed)";
    }
    else
    {
        int iByteIndex = 0;

        while( bySNBytes[iByteIndex] != 0xFF )
        {
           if( bySNBytes[iByteIndex] == 0x00 )
               break;

           // Make sure this is a 'printable' char
           if( ( bySNBytes[iByteIndex] >= 0x20 ) && ( bySNBytes[iByteIndex] <= 0x7E ) )
               sResult = sResult + (char)( bySNBytes[iByteIndex] );
           else
               sResult = sResult + "?";

           iByteIndex++;

           if( iByteIndex >= 32 )
               break;
        }
    }

    return sResult;
}


__fastcall TMPLSMonMain::TMPLSMonMain(TComponent* Owner) : TForm(Owner)
{
    MainPgCtrl->ActivePageIndex = 0;

    memset( &m_BRPort,   0, sizeof( PORT_INFO ) );
    memset( &m_MPLSPort, 0, sizeof( PORT_INFO ) );

    // There's only one packet timeout that's used for both ports
    m_dwPktTimeout = 250;

    // Populate the MPLS cmd combo
    MPLSCmdCombo->Items->Clear();

    int iItem = 0;

    while( MPLSCommandArray[iItem].sDesc.Length() > 0 )
    {
        MPLSCmdCombo->Items->AddObject( MPLSCommandArray[iItem].sDesc, (TObject*)MPLSCommandArray[iItem].byCmd );

        iItem++;
    }

    MPLSCmdCombo->ItemIndex = 0;
    SetParamsVisibility( "RFC RATE", "RFC Timeout", "Pair Timeout", "" );
}


void __fastcall TMPLSMonMain::FormShow(TObject *Sender)
{
   // Set initial button states
    BRConnectBtn->Enabled    = true;
    BRDisconnectBtn->Enabled = false;

    SetEditState( BRPortEdit,  true );
    SetEditState( BRSpeedEdit, true );

    MPLSConnectBtn->Enabled    = true;
    MPLSDisconnectBtn->Enabled = false;

    SetEditState( MPLSPortEdit,  true );
    SetEditState( MPLSSpeedEdit, true );

    EnableGBCtrls( BRActionsGB,   false );
    EnableGBCtrls( MPLSActionsGB, false );

    // Auto page is our front page
    MainPgCtrl->ActivePage = AutoSheet;

    // Start the Auto-Connection
    ::PostMessage( Handle, WM_DO_AUTO_CONN, 0, 0 );
}


void __fastcall TMPLSMonMain::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Release resources
    BRDisconnectBtnClick( BRDisconnectBtn );
    MPLSDisconnectBtnClick( MPLSDisconnectBtn );
}


void __fastcall TMPLSMonMain::ExitBtnClick(TObject *Sender)
{
    Application->Terminate();
}


//
// Base Radio
//

void __fastcall TMPLSMonMain::BRConnectBtnClick(TObject *Sender)
{
    if( !ValidatePortSettings( BRPortEdit, BRSpeedEdit ) )
        return;

    String sErrMsg;

    if( !DoBRConnect( BRPortEdit->Text.ToInt(), BRSpeedEdit->Text.ToInt(), sErrMsg ) )
        MessageDlg( sErrMsg, mtError, TMsgDlgButtons() << mbOK, 0 );
}


bool TMPLSMonMain::DoBRConnect( int iBRPort, DWORD dwBitRate, String& sErrMsg )
{
    // Try to create the comm obj
    if( !CreatePort( iBRPort, dwBitRate, m_BRPort, sErrMsg ) )
        return false;

    if( m_BRPort.bUsingUDP )
        UpdateStatusGrid( BRStatusGrid, sPortState, "Open on UDP " + BRPortEdit->Text );
    else
        UpdateStatusGrid( BRStatusGrid, sPortState, "Open on COM" + BRPortEdit->Text );

    // On success, set button states
    BRConnectBtn->Enabled    = false;
    BRDisconnectBtn->Enabled = true;

    SetEditState( BRPortEdit,  false );
    SetEditState( BRSpeedEdit, false );

    EnableGBCtrls( BRActionsGB, true );
    ClearGBTags( BRActionsGB );

    // Enable poll timer
    BRPollTimer->Enabled = true;

    return true;
}


void __fastcall TMPLSMonMain::BRDisconnectBtnClick(TObject *Sender)
{
    // Stop poll timer and release comm obj
    BRPollTimer->Enabled = false;

    ReleasePort( m_BRPort );

    ClearStatusGrid( BRStatusGrid );
    UpdateStatusGrid( BRStatusGrid, sPortState, "Closed" );

    // Set button states
    BRConnectBtn->Enabled    = true;
    BRDisconnectBtn->Enabled = false;

    SetEditState( BRPortEdit,  true );
    SetEditState( BRSpeedEdit, true );

    EnableGBCtrls( BRActionsGB, false );
}


void __fastcall TMPLSMonMain::SetBRChanBtnClick(TObject *Sender)
{
    BASE_STN_DATA_UNION payloadData;
    payloadData.setChanPkt.radioNbr   = GetSelectedRadioNbr();
    payloadData.setChanPkt.newChanNbr = GetChanNbr( BRChanCombo );

    if( SendBRCmd( BASE_STN_CMD_SET_CHAN_NBR, payloadData, sizeof( BASE_STN_SET_RADIO_CHAN ) ) )
        UpdateStatusGrid( BRStatusGrid, sLastCmdTime, Time().TimeString() );
    else
        Beep();
}


void __fastcall TMPLSMonMain::BRSetOutputBtnClick(TObject *Sender)
{
    BASE_STN_DATA_UNION payloadData;
    payloadData.setOutputsPkt.outputFlags = 0;

    if( BROutput1CB->Checked )
        payloadData. setOutputsPkt.outputFlags |= 0x01;

    if( BROutput2CB->Checked )
        payloadData. setOutputsPkt.outputFlags |= 0x02;

    if( BROutput3CB->Checked )
        payloadData. setOutputsPkt.outputFlags |= 0x04;

    if( BROutput4CB->Checked )
        payloadData. setOutputsPkt.outputFlags |= 0x08;

    if( SendBRCmd(BASE_STN_CMD_SET_OUTPUTS, payloadData, sizeof(BASE_STN_SET_OUTPUTS_PKT ) ) )
        UpdateStatusGrid( BRStatusGrid, sLastCmdTime, Time().TimeString() );
    else
        Beep();
}


void __fastcall TMPLSMonMain::BRPollTimerTimer(TObject *Sender)
{
    PollPortForData( m_BRPort );

    // Have the parser check for any responses from the unit.
    WTTTS_PKT_HDR       pktHdr;
    BASE_STN_DATA_UNION pktData;

    while( HaveBaseStnRespPkt( m_BRPort.rxBuffer, m_BRPort.rxBuffCount, pktHdr, pktData ) )
    {
        m_BRPort.lastPktTime = GetTickCount();

        UpdateStatusGrid( BRStatusGrid, sLastRxTime, Time().TimeString() );
        UpdateStatusGrid( BRStatusGrid, sLastRxType, IntToStr( pktHdr.pktType ) );

        if( pktHdr.pktType == BASE_STN_RESP_STATUS )
            UpdateBRStatusValues( pktData.statusPkt );
    }
}


bool TMPLSMonMain::SendBRCmd( BYTE cmdType, BASE_STN_DATA_UNION& payloadData, BYTE payloadLen )
{
    if( m_BRPort.commPort == NULL )
        return false;

    // Allocate and clear command area
    BYTE txBuffer[MAX_BASE_STN_PKT_LEN];
    memset( txBuffer, 0, MAX_BASE_STN_PKT_LEN );

    WTTTS_PKT_HDR* pHdr  = (WTTTS_PKT_HDR*)txBuffer;

    // Setup header defaults
    pHdr->pktHdr    = BASE_STN_HDR_TX;
    pHdr->pktType   = cmdType;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = payloadLen;

    if( payloadLen > 0 )
    {
        BYTE* pPayload = &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pPayload, &payloadData, payloadLen );
    }

    int hdrAndPayLoadLen = sizeof(WTTTS_PKT_HDR) + payloadLen;

    txBuffer[hdrAndPayLoadLen] = CalcWTTTSChecksum( txBuffer, hdrAndPayLoadLen );

    DWORD pktLen = hdrAndPayLoadLen + 1;

    DWORD bytesSent = m_BRPort.commPort->CommSend( txBuffer, pktLen );

    return( bytesSent == pktLen );
}


void TMPLSMonMain::UpdateBRStatusValues( const BASE_STATUS_STATUS_PKT& statusPkt )
{
    UpdateStatusGrid( BRStatusGrid, sBR0ChanNbr, IntToStr(      statusPkt.radioInfo[0].chanNbr ) );
    UpdateStatusGrid( BRStatusGrid, sBR0RSSI,    IntToStr(      statusPkt.radioInfo[0].rssi ) );
    UpdateStatusGrid( BRStatusGrid, sBR0BitRate, IntToStr( (int)statusPkt.radioInfo[0].bitRate ) );

    UpdateStatusGrid( BRStatusGrid, sBR1ChanNbr, IntToStr(      statusPkt.radioInfo[1].chanNbr ) );
    UpdateStatusGrid( BRStatusGrid, sBR1RSSI,    IntToStr(      statusPkt.radioInfo[1].rssi ) );
    UpdateStatusGrid( BRStatusGrid, sBR1BitRate, IntToStr( (int)statusPkt.radioInfo[1].bitRate ) );

    UpdateStatusGrid( BRStatusGrid, sBROutputStatus, IntToHex( statusPkt.outputStatus, 2 ) );
    UpdateStatusGrid( BRStatusGrid, sBRUpdateRate,   IntToStr( statusPkt.statusUpdateRate ) );
    UpdateStatusGrid( BRStatusGrid, sBRFWVer,        IntToStr( statusPkt.fwVer[0] ) + IntToStr( statusPkt.fwVer[1] ) + IntToStr( statusPkt.fwVer[2] ) + IntToStr( statusPkt.fwVer[3] ) );
    UpdateStatusGrid( BRStatusGrid, sBRCtrlSigs,     IntToHex( statusPkt.usbCtrlSignals, 2 ) );
    UpdateStatusGrid( BRStatusGrid, sBRRFU0,         IntToHex( statusPkt.byRFU[0], 2 ) );
    UpdateStatusGrid( BRStatusGrid, sBRRFU1,         IntToHex( statusPkt.byRFU[1], 2 ) );

    switch( statusPkt.lastResetType )
    {
        case 1:   UpdateStatusGrid( BRStatusGrid, sBRLastReset, "Power-up"  );  break;
        case 2:   UpdateStatusGrid( BRStatusGrid, sBRLastReset, "WDT"       );  break;
        case 3:   UpdateStatusGrid( BRStatusGrid, sBRLastReset, "Brown out" );  break;
        default:  UpdateStatusGrid( BRStatusGrid, sBRLastReset, "Type " + IntToStr( statusPkt.lastResetType ) );  break;
    }
}


BYTE TMPLSMonMain::GetSelectedRadioNbr( void )
{
    if( BRRadioNbrCombo->ItemIndex < 0 )
        BRRadioNbrCombo->ItemIndex = 0;

    return BRRadioNbrCombo->ItemIndex;
}


//
// WTTTS
//

void __fastcall TMPLSMonMain::MPLSConnectBtnClick(TObject *Sender)
{
    if( !ValidatePortSettings( MPLSPortEdit, MPLSSpeedEdit ) )
        return;

    String sErrMsg;

    if( !DoMPLSConnect( MPLSPortEdit->Text.ToInt(), MPLSSpeedEdit->Text.ToInt(), sErrMsg ) )
        MessageDlg( sErrMsg, mtError, TMsgDlgButtons() << mbOK, 0 );
}


bool TMPLSMonMain::DoMPLSConnect( int iMPLSPort, DWORD dwBitRate, String& sErrMsg )
{
    // Try to create the comm obj.
    if( !CreatePort( iMPLSPort, dwBitRate, m_MPLSPort, sErrMsg ) )
        return false;

    if( m_MPLSPort.bUsingUDP )
        UpdateStatusGrid( MPLSStatusGrid, sPortState, "Open on UDP " + MPLSPortEdit->Text );
    else
        UpdateStatusGrid( MPLSStatusGrid, sPortState, "Open on COM" + MPLSPortEdit->Text );

    // On success, set button states
    MPLSConnectBtn->Enabled    = false;
    MPLSDisconnectBtn->Enabled = true;

    SetEditState( MPLSPortEdit,  false );
    SetEditState( MPLSSpeedEdit, false );

    EnableGBCtrls( MPLSActionsGB, true );
    ClearGBTags( MPLSActionsGB );

    // Clear error counts
    ClearErrCountsBtnClick( NULL );

    m_RFCStats.wLastSeqNbr = RFCSeqNbrNone;

    // Clear pending command
    m_pendingMPLSCmd.cmdPending = false;

    // Enable poll timer
    WTTTSPollTimer->Enabled = true;

    return true;
}


void __fastcall TMPLSMonMain::MPLSDisconnectBtnClick(TObject *Sender)
{
    // Stop poll timer and release comm obj
    WTTTSPollTimer->Enabled = false;

    ReleasePort( m_MPLSPort );

    ClearStatusGrid( MPLSStatusGrid );
    UpdateStatusGrid( MPLSStatusGrid, sPortState, "Closed" );

    // Set button states
    MPLSConnectBtn->Enabled    = true;
    MPLSDisconnectBtn->Enabled = false;

    SetEditState( MPLSPortEdit,  true );
    SetEditState( MPLSSpeedEdit, true );

    EnableGBCtrls( MPLSActionsGB, false );
}


void __fastcall TMPLSMonMain::MPLSCmdComboClick(TObject *Sender)
{
    // Combo objects array contains the current command number
    BYTE byCmd = (BYTE)( MPLSCmdCombo->Items->Objects[ MPLSCmdCombo->ItemIndex ] );

    switch( byCmd )
    {
        case WTTTS_CMD_SET_RATE:
                SetParamsVisibility( "RFC Rate", "RFC Timeout", "Pair Timeout", "" );
                break;

        case WTTTS_CMD_MPLS2_QUERY_VER:
                SetParamsVisibility( "Unit ID", "", "", "" );
                break;

        case WTTTS_CMD_GET_CFG_DATA:
                SetParamsVisibility( "Page Number", "", "", "" );
                break;

        case WTTTS_CMD_SET_RF_CHANNEL:
                SetParamsVisibility( "New Chanel", "", "", "" );
                break;

        case WTTTS_CMD_MARK_CHAN_IN_USE:
                SetParamsVisibility( "", "", "", "" );
                break;

        case WTTTS_CMD_MPLS2_SET_PIB_OUT:
                SetParamsVisibility( "Target PIB", "Solendoid State", "Solenoid Timeout", "Isolated Output" );
                break;

        case WTTTS_CMD_MPLS2_SET_TEC_PAR:
                SetParamsVisibility( "Device Address", "Current Output Level", "Readings to Average", "" );
                break;

        case WTTTS_CMD_MPLS2_SET_DET_PAR:
                SetParamsVisibility( "Sampling Rate", "Averaging Factor", "", "" );
                break;

        default:
        // Hide edits and labels
            SetParamsVisibility( "", "", "", "" );
            break;
    }
}


void __fastcall TMPLSMonMain::SendMPLSCmdBtnClick(TObject *Sender)
{
    // Combo objects array contains the current command number
    BYTE byCmd = (BYTE)( MPLSCmdCombo->Items->Objects[ MPLSCmdCombo->ItemIndex ] );

    // Clear payload data to start
    memset( &m_pendingMPLSCmd.payloadData, 0, sizeof( MPLS2_DATA_UNION ) );

    switch( byCmd )
    {
        case WTTTS_CMD_SET_RATE:
            m_pendingMPLSCmd.payloadLen = sizeof( MPLS2_RATES_PKT );
            m_pendingMPLSCmd.payloadData.ratePkt.rfcRate     = Param1Edit->Text.ToInt();
            m_pendingMPLSCmd.payloadData.ratePkt.rfcTimeout  = Param2Edit->Text.ToInt();
            m_pendingMPLSCmd.payloadData.ratePkt.pairTimeout = Param3Edit->Text.ToInt();
            break;

        case WTTTS_CMD_MPLS2_QUERY_VER:
            m_pendingMPLSCmd.payloadLen = sizeof( MPLS2_QUERY_VER_PKT );
            m_pendingMPLSCmd.payloadData.queryVerPkt.byUnitID = Param1Edit->Text.ToInt();
            break;

        case WTTTS_CMD_GET_CFG_DATA:
            m_pendingMPLSCmd.payloadLen = sizeof( WTTTS_CFG_ITEM );
            m_pendingMPLSCmd.payloadData.cfgRequest.pageNbr = Param1Edit->Text.ToInt();
            break;

        case WTTTS_CMD_SET_RF_CHANNEL:
            m_pendingMPLSCmd.payloadLen = sizeof( WTTTS_SET_CHAN_PKT );
            m_pendingMPLSCmd.payloadData.chanPkt.chanNbr = Param1Edit->Text.ToInt();
            break;

        case WTTTS_CMD_MARK_CHAN_IN_USE:
            m_pendingMPLSCmd.payloadLen = 0;
            break;

        case WTTTS_CMD_MPLS2_SET_PIB_OUT:
            m_pendingMPLSCmd.payloadLen = sizeof( MPLS2_PIB_OUTS_PKT );
            m_pendingMPLSCmd.payloadData.pibOutsPkt.targetPIB  = Param1Edit->Text.ToInt();
            m_pendingMPLSCmd.payloadData.pibOutsPkt.activeSol  = Param2Edit->Text.ToInt();
            m_pendingMPLSCmd.payloadData.pibOutsPkt.solTimeout = Param3Edit->Text.ToInt();
            m_pendingMPLSCmd.payloadData.pibOutsPkt.isolOuts   = Param4Edit->Text.ToInt();
            break;

        case WTTTS_CMD_MPLS2_SET_TEC_PAR:
            m_pendingMPLSCmd.payloadLen = sizeof( MPLS2_TEC_PARAMS_PKT );
            m_pendingMPLSCmd.payloadData.tecParamsPkt.devAddr     = Param1Edit->Text.ToInt();
            m_pendingMPLSCmd.payloadData.tecParamsPkt.setting     = Param2Edit->Text.ToInt();
            m_pendingMPLSCmd.payloadData.tecParamsPkt.readsPerRFC = Param3Edit->Text.ToInt();
            break;

        case WTTTS_CMD_MPLS2_SET_DET_PAR:
            m_pendingMPLSCmd.payloadLen = sizeof( MPLS2_DET_PARAMS_PKT );
            m_pendingMPLSCmd.payloadData.detParamsPkt.sampleRate   = Param1Edit->Text.ToInt();
            m_pendingMPLSCmd.payloadData.detParamsPkt.sampleAvging = Param2Edit->Text.ToInt();
            break;

        default:
            // Do nothing
            return;
    }

    // Have a valid command. Set our flags
    m_pendingMPLSCmd.cmdType    = byCmd;
    m_pendingMPLSCmd.cmdPending = true;

    SendMPLSCmdBtn->Enabled = false;
}


void __fastcall TMPLSMonMain::WTTTSPollTimerTimer(TObject *Sender)
{
    // First look for any data from the device
    PollPortForData( m_MPLSPort );

    // Have the parser check for any responses from the unit.
    bool bRFCPending = false;

    WTTTS_PKT_HDR    pktHdr;
    MPLS2_DATA_UNION pktData;

    DWORD dwPreParseBuffCount = m_MPLSPort.rxBuffCount;

    if( !HaveMPLS2RespPkt( m_MPLSPort.rxBuffer, m_MPLSPort.rxBuffCount, pktHdr, pktData ) )
    {
        // We didn't get a packet. If the parser turfed some bytes,
        // it means there was a packet error (bad header or bad CRC)
        if( m_MPLSPort.rxBuffCount != dwPreParseBuffCount )
            IncStat( m_MPLSPort.pktErrors );
    }
    else
    {
        m_MPLSPort.lastPktTime = GetTickCount();

        // RFC responses go into the RFC status grid. Other packets go into
        // the Data grid
        if( pktHdr.pktType == WTTTS_RESP_MPLS2_RFC )
        {
            // Check for missed sequence number
            if( m_RFCStats.wLastSeqNbr != RFCSeqNbrNone )
            {
                BYTE byExpectedSeqNbr = (BYTE)( m_RFCStats.wLastSeqNbr + 1 );

                if( pktHdr.seqNbr == (BYTE)m_RFCStats.wLastSeqNbr )
                    IncStat( m_RFCStats.duplicates );
                else if( pktHdr.seqNbr != byExpectedSeqNbr )
                    IncStat( m_RFCStats.missedCount );
            }

            m_RFCStats.wLastSeqNbr = pktHdr.seqNbr;

            // Update the display grid
            UpdateMPLSRFCValues( pktHdr, pktData.rfcPkt );

            bRFCPending = true;

            if( RFCDataCB->Checked )
                RFCDataCB->Checked = LogRFCPacket( RFCDataFileEdit->Text, pktHdr, pktData.rfcPkt );
        }
        else
        {
            ShowMPLSEvent( pktHdr.pktType, pktData );
        }
    }

    if( bRFCPending )
    {
        // Check if a command is pending
        if( m_pendingMPLSCmd.cmdPending )
        {
            if( SendMPLS2Cmd( m_pendingMPLSCmd.cmdType, m_pendingMPLSCmd.payloadData, m_pendingMPLSCmd.payloadLen ) )
                UpdateStatusGrid( EventPktEditor, sLastCmdTime, Now().FormatString( "hh:nn:ss" ) );

            // Whether the command got sent or not, clear the flag
            m_pendingMPLSCmd.cmdPending = false;
            SendMPLSCmdBtn->Enabled = true;
        }
        else
        {
            MPLS2_DATA_UNION payloadData = { 0 };

            SendMPLS2Cmd( WTTTS_CMD_NO_CMD, payloadData, 0 );
        }
    }
}


bool TMPLSMonMain::SendMPLS2Cmd( BYTE cmdType, MPLS2_DATA_UNION& payloadData, BYTE payloadLen )
{
    if( m_MPLSPort.commPort == NULL )
        return false;

    // Allocate and clear command area
    BYTE txBuffer[MAX_MPLS2_PKT_LEN];
    memset( txBuffer, 0, MAX_MPLS2_PKT_LEN);

    WTTTS_PKT_HDR* pHdr  = (WTTTS_PKT_HDR*)txBuffer;

    // Setup header defaults
    pHdr->pktHdr    = MPLS2_HDR_TX;
    pHdr->pktType   = cmdType;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = payloadLen;

    if( payloadLen > 0 )
    {
        BYTE* pPayload = &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pPayload, &payloadData, payloadLen );
    }

    int hdrAndPayLoadLen = sizeof(WTTTS_PKT_HDR) + payloadLen;

    txBuffer[hdrAndPayLoadLen] = CalcWTTTSChecksum( txBuffer, hdrAndPayLoadLen );

    DWORD pktLen = hdrAndPayLoadLen + 1;

    DWORD bytesSent;

    if( m_MPLSPort.bUsingUDP )
        bytesSent = ((CUDPPort*)m_MPLSPort.commPort)->SendTo( txBuffer, pktLen, m_MPLSPort.sIPAddr, m_MPLSPort.wUDPPort );
    else
        bytesSent = m_MPLSPort.commPort->CommSend( txBuffer, pktLen );

    return( bytesSent == pktLen );
}


void TMPLSMonMain::UpdateMPLSRFCValues( const WTTTS_PKT_HDR& pktHdr, const MPLS2_RFC_PKT& rfcPkt )
{
    UpdateStatusGrid( MPLSStatusGrid, sLastRxTime,      Time().TimeString() );
    UpdateStatusGrid( MPLSStatusGrid, sLastRxType,      MPLSEventTypeToStr( WTTTS_RESP_MPLS2_RFC ) );
    UpdateStatusGrid( MPLSStatusGrid, sLastPktSeqNbr,   UIntToStr( (UINT)pktHdr.seqNbr ) );
    UpdateStatusGrid( MPLSStatusGrid, sLastPktTStamp,   UIntToStr( (UINT)pktHdr.timeStamp ) );
    UpdateStatusGrid( MPLSStatusGrid, sMissedRFCs,      UIntToStr( (UINT)m_RFCStats.missedCount ) );
    UpdateStatusGrid( MPLSStatusGrid, sDuplicateRFCs,   UIntToStr( (UINT)m_RFCStats.duplicates ) );
    UpdateStatusGrid( MPLSStatusGrid, sPktFlushes,      UIntToStr( (UINT)m_MPLSPort.pktFlushes ) );
    UpdateStatusGrid( MPLSStatusGrid, sPktErrors,       UIntToStr( (UINT)m_MPLSPort.pktErrors ) );
    UpdateStatusGrid( MPLSStatusGrid, sSlBattVolts,     IntToStr( rfcPkt.slaveBattVolts ) + " mV" );
    UpdateStatusGrid( MPLSStatusGrid, sSlTankPress,     IntToStr( TriByteToInt( rfcPkt.slaveTankPress ) ) );
    UpdateStatusGrid( MPLSStatusGrid, sSlRegPress,      IntToStr( TriByteToInt( rfcPkt.slaveRegPress  ) ) );
    UpdateStatusGrid( MPLSStatusGrid, sMastrBattVolts,  IntToStr( rfcPkt.masterBattVolts ) + " mV" );
    UpdateStatusGrid( MPLSStatusGrid, sMastrTankPress,  IntToStr( TriByteToInt( rfcPkt.masterTankPress) ) );
    UpdateStatusGrid( MPLSStatusGrid, sMastrRegPress,   IntToStr( TriByteToInt( rfcPkt.masterRegPress ) ) );
    UpdateStatusGrid( MPLSStatusGrid, sRPM,             IntToStr( rfcPkt.rpm ) );
    UpdateStatusGrid( MPLSStatusGrid, sTemp,            FloatToStrF ( (float)rfcPkt.temperature / 2.0, ffFixed, 7, 1 ) + " C" );
    UpdateStatusGrid( MPLSStatusGrid, sSysStatusBits,   "0x" + IntToHex( rfcPkt.sysStatusBits, 4 ) );
    UpdateStatusGrid( MPLSStatusGrid, sSolStatus,       "0x" + IntToHex( rfcPkt.solStatus,     2 ) );
    UpdateStatusGrid( MPLSStatusGrid, sIsolOutStatus,   "0x" + IntToHex( rfcPkt.isolOutStatus, 2 ) );
    UpdateStatusGrid( MPLSStatusGrid, sSlSolCurrent,    IntToStr( rfcPkt.slaveSolCurrent  ) + " mA" );
    UpdateStatusGrid( MPLSStatusGrid, sMastrSolCurrent, IntToStr( rfcPkt.masterSolCurrent ) + " mA" );

    // Format plug data
    String sLine;

    for( int iItem = 0; iItem < 32; iItem++ )
       sLine = sLine + IntToHex( rfcPkt.plugData[iItem], 4 ) + " ";

    UpdateStatusGrid( MPLSStatusGrid, sPlugData, sLine );
}


void TMPLSMonMain::ShowMPLSEvent( BYTE byPktType, const MPLS2_DATA_UNION& pktData )
{
    // RFCs displayed in a different grid
    if( byPktType == WTTTS_RESP_MPLS2_RFC )
        return;

    // Clear grid
    EventPktEditor->Strings->Clear();

    // Add lines based on packet type
    UpdateStatusGrid( EventPktEditor, sLastPktTime, Now().FormatString( "hh:nn:ss" ) );
    UpdateStatusGrid( EventPktEditor, sLastRxType,  MPLSEventTypeToStr( byPktType ) );

    switch( byPktType )
    {
        case WTTTS_RESP_MPLS2_VER_PKT:
            UpdateStatusGrid( EventPktEditor, sVerPktUnitID, UIntToStr( (UINT)pktData.verPkt.byUnitID ) );
            UpdateStatusGrid( EventPktEditor, sVerPktTECSN,  DevSNToString( pktData.verPkt.tecSN ) );
            UpdateStatusGrid( EventPktEditor, sVerPktTECFW,  UIntToStr( (UINT)pktData.verPkt.tecFWRev ) );
            UpdateStatusGrid( EventPktEditor, sVerPktTECHW,  UIntToStr( (UINT)pktData.verPkt.tecHWRev ) );
            UpdateStatusGrid( EventPktEditor, sVerPktPIBFW,  UIntToStr( (UINT)pktData.verPkt.pibFWRev ) );
            UpdateStatusGrid( EventPktEditor, sVerPktPIBHW,  UIntToStr( (UINT)pktData.verPkt.pibHWRev ) );
            UpdateStatusGrid( EventPktEditor, sVerPktDetSN,  DevSNToString( pktData.verPkt.plugDetSN ) );
            UpdateStatusGrid( EventPktEditor, sVerPktDetFW,  UIntToStr( (UINT)pktData.verPkt.plugDetFWRev ) );
            UpdateStatusGrid( EventPktEditor, sVerPktDetHW,  UIntToStr( (UINT)pktData.verPkt.plugDetHWRev ) );
            break;

        case WTTTS_RESP_MPLS2_CFG_DATA:
            UpdateStatusGrid( EventPktEditor, sCfgPktPgNbr,  "0x" + IntToHex( pktData.cfgData.pageNbr, 2 ) );
            UpdateStatusGrid( EventPktEditor, sCfgPktResult, UIntToStr( (UINT)pktData.cfgData.result ) );
            UpdateStatusGrid( EventPktEditor, sCfgPktData,   "TO DO" );
            break;

        case WTTTS_RESP_MPLS2_ACK_PKT:
            UpdateStatusGrid( EventPktEditor, sAckPktCmd,    MPLSCmdTypeToStr( pktData.ackPkt.byCmdBeingAckd ) );
            UpdateStatusGrid( EventPktEditor, sAckPktResult, UIntToStr( (UINT)pktData.ackPkt.byCmdResult ) );
            break;
    }
}


//
// Common Handlers
//

bool TMPLSMonMain::CreatePort( int portNbr, DWORD portSpeed, PORT_INFO& portInfo, String& sErrMsg )
{
    // Clear return struct params first
    portInfo.commPort    = NULL;
    portInfo.rxBuffCount = 0;
    portInfo.lastRxTime  = 0;
    portInfo.lastPktTime = 0;
    portInfo.bUsingUDP   = false;
    portInfo.sIPAddr     = "";
    portInfo.wUDPPort    = 0;

    sErrMsg = "";

    // Create a comm port object. We do a cheat here: if the port number is < 100
    // then assume we are creating a serial port
    CCommObj* portObj;

    if( portNbr < 100 )
        portObj = new CCommPort();
    else
        portObj = new CUDPPort();

    if( portObj == NULL )
        throw Exception( "Could create comm object!" );

    CCommObj::COMM_RESULT connResult;

    if( portNbr < 100 )
    {
        CCommPort::CONNECT_PARAMS connParams;

        connParams.iPortNumber   = portNbr;
        connParams.dwLineBitRate = portSpeed;

        connResult = portObj->Connect( &connParams );
    }
    else
    {
        CUDPPort::CONNECT_PARAMS connParams;

        connParams.destAddr = "";
        connParams.destPort = 0;
        connParams.portNbr  = portNbr;
        connParams.acceptBroadcasts = true;

        InitWinsock();

        connResult = portObj->Connect( &connParams );

        if( connResult == CCommObj::ERR_NONE )
            portInfo.bUsingUDP = true;
        else
            ShutdownWinsock();
    }

    if( connResult != CCommObj::ERR_NONE )
    {
        delete portObj;
        portObj = NULL;

        String connResultText;

        switch( connResult )
        {
            case CCommObj::ERR_NONE:                 connResultText = "success";           break;
            case CCommObj::ERR_COMM_SET_COMM_STATE:  connResultText = "set state error";   break;
            case CCommObj::ERR_COMM_INV_BUFFER_LEN:  connResultText = "set buffer error";  break;
            case CCommObj::ERR_COMM_OPEN_FAIL:       connResultText = "open port error";   break;
            case CCommObj::ERR_COMM_GET_COMM_STATE:  connResultText = "get state error";   break;
            default:                                 connResultText = "error " + IntToStr( connResult );  break;
        }

        sErrMsg = "Could not open port: " + connResultText;

        return false;
    }

    // Fall through means success
    portInfo.commPort = portObj;

    return true;
}


void TMPLSMonMain::ReleasePort( PORT_INFO& portInfo )
{
    if( portInfo.commPort != NULL )
    {
        portInfo.commPort->Disconnect();

        delete portInfo.commPort;

        portInfo.commPort = NULL;

        if( portInfo.bUsingUDP )
            ShutdownWinsock();
    }

    portInfo.rxBuffCount = 0;
    portInfo.lastRxTime  = 0;
    portInfo.lastPktTime = 0;
    portInfo.bUsingUDP   = false;
    portInfo.sIPAddr     = "";
    portInfo.wUDPPort    = 0;
}


bool TMPLSMonMain::ValidatePortSettings( TEdit* portEdit, TEdit* speedEdit )
{
    try
    {
        int testResult = portEdit->Text.ToInt();

        if( testResult <= 0 )
            Abort();
    }
    catch( ... )
    {
        MessageDlg( "You have entered an invalid port number.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    try
    {
        int testResult = speedEdit->Text.ToInt();

        if( testResult <= 0 )
            Abort();
    }
    catch( ... )
    {
        MessageDlg( "You have entered an invalid speed.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return false;
    }

    // Fall through means success
    return true;
}


BYTE TMPLSMonMain::GetChanNbr( TComboBox* aCombo )
{
    if( aCombo->ItemIndex < 0 )
        aCombo->ItemIndex = 0;

    return aCombo->Items->Strings[ aCombo->ItemIndex ].ToInt();
}


void TMPLSMonMain::PollPortForData( PORT_INFO& portInfo )
{
    if( portInfo.commPort != NULL )
    {
        DWORD bytesRxd;

        if( portInfo.bUsingUDP )
            bytesRxd = ((CUDPPort*)portInfo.commPort)->RecvFrom( &(portInfo.rxBuffer[portInfo.rxBuffCount]), RX_BUFF_SIZE - portInfo.rxBuffCount, portInfo.sIPAddr, portInfo.wUDPPort );
        else
            bytesRxd = portInfo.commPort->CommRecv( &(portInfo.rxBuffer[portInfo.rxBuffCount]), RX_BUFF_SIZE - portInfo.rxBuffCount );

        if( bytesRxd > 0 )
        {
            portInfo.lastRxTime  = GetTickCount();
            portInfo.rxBuffCount += bytesRxd;
        }
        else if( portInfo.rxBuffCount > 0 )
        {
            // No data received - check if any bytes in the buffer have gone stale
            if( HaveTimeout( portInfo.lastRxTime, m_dwPktTimeout ) )
            {
                String sMsg = "Packet flush (" + IntToStr( (int)portInfo.rxBuffCount ) + "): ";

                for( int iByte = 0; iByte < portInfo.rxBuffCount; iByte++ )
                    sMsg = sMsg + " " + IntToHex( portInfo.rxBuffer[iByte], 2 );

                OutputDebugString( sMsg.w_str() );

                portInfo.rxBuffCount = 0;
                IncStat( portInfo.pktFlushes );
            }
        }
    }
}


void TMPLSMonMain::UpdateStatusGrid( TValueListEditor* aGrid, const String& key, const String& text )
{
    // Look for the key first. If not found, add it
    bool bKeyExists = false;

    for( int iItem = 1; iItem < aGrid->RowCount; iItem++ )
    {
        if( aGrid->Keys[iItem] == key )
        {
            bKeyExists = true;
            break;
        }
    }

    if( bKeyExists )
        aGrid->Values[ key ] = text;
    else
        aGrid->InsertRow( key, text, true );
}


void TMPLSMonMain::ClearStatusGrid( TValueListEditor* aGrid )
{
    // Clear all but the title row
    for( int iRow = 1; iRow < aGrid->RowCount; iRow++ )
        aGrid->Values[ aGrid->Keys[iRow] ] = "";
}


void __fastcall TMPLSMonMain::RFCOpenFileBtnClick(TObject *Sender)
{
    if( SaveLogDlg->Execute() )
        RFCDataFileEdit->Text = SaveLogDlg->FileName;
}


bool TMPLSMonMain::LogRFCPacket( const String& logFileName, const WTTTS_PKT_HDR& pktHdr, const MPLS2_RFC_PKT& rfcData )
{
    // Writes the passed stream packet to file
    TFileStream* logStream = NULL;

    bool logGood = false;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            // Create header line
            AnsiString sHeader = "Time,TickCount,HdrSeqNbr,HdrTime,"
                                 "SysStatus,SolStatus,IsolOutStatus,RPM,Temp,"
                                 "MasterVBatt,MasterSolCurr,MasterTankPress,MasterRegPress,"
                                 "SlaveVBatt,SlaveSolCurr,SlaveTankPress,SlaveRegPress,"
                                 "RFU,PlugData\r";

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        // Create data line
        String sLine;
        sLine = Now().TimeString() + "," +
                UIntToStr( (UINT)GetTickCount() ) + "," +
                UIntToStr( (UINT)pktHdr.seqNbr ) + "," +
                UIntToStr( (UINT)pktHdr.timeStamp ) + "," +
                "0x" + IntToHex( rfcData.sysStatusBits, 4 ) + "," +
                "0x" + IntToHex( rfcData.solStatus, 2 )     + "," +
                "0x" + IntToHex( rfcData.isolOutStatus, 2 ) + "," +
                UIntToStr( (UINT)rfcData.rpm )              + "," +
                UIntToStr( (UINT)rfcData.temperature )      + "," +
                UIntToStr( (UINT)rfcData.masterBattVolts )  + "," +
                UIntToStr( (UINT)rfcData.masterSolCurrent ) + "," +
                IntToStr ( TriByteToInt( rfcData.masterTankPress ) ) + "," +
                IntToStr ( TriByteToInt( rfcData.masterRegPress ) )  + "," +
                UIntToStr( (UINT)rfcData.slaveBattVolts )  + "," +
                UIntToStr( (UINT)rfcData.slaveSolCurrent ) + "," +
                IntToStr ( TriByteToInt( rfcData.slaveTankPress ) )  + "," +
                IntToStr ( TriByteToInt( rfcData.slaveRegPress ) )   + ",";

        String sRFU = IntToHex( rfcData.rfu[0], 2 ) + " " +
                      IntToHex( rfcData.rfu[1], 2 ) + " " +
                      IntToHex( rfcData.rfu[2], 2 ) + " " +
                      IntToHex( rfcData.rfu[3], 2 );

        String sPlugData;

        for( int iIndex = 0; iIndex < 32; iIndex++ )
            sPlugData = sPlugData + "," + UIntToStr( (UINT)rfcData.plugData[iIndex] );

        AnsiString asLine = sLine + sRFU + sPlugData + "\r";

        logStream->Write( asLine.c_str(), asLine.Length() );

        logGood = true;
    }
    catch( ... )
    {
        logGood = false;
    }

    if( logStream != NULL )
        delete logStream;

    return logGood;
}


void __fastcall TMPLSMonMain::WMDoAutoConn( TMessage &Message )
{
    // This message gets posted on form show. Start the auto-connect process
    AutoConnBtnClick( AutoConnBtn );
}


void __fastcall TMPLSMonMain::AutoConnBtnClick( TObject *Sender )
{
    // Ensure nothing else can be done during this time, disable main page
    MainPgCtrl->Enabled = false;

    // Be safe, attempt to disconnect both first
    BRDisconnectBtnClick( BRDisconnectBtn );
    MPLSDisconnectBtnClick( MPLSDisconnectBtn );

    // Ensure that we run until finished or cancelled
    m_bAutoScanCancelled = false;

    bool  bBRFound    = false;
    bool  bWTTTSFound = false;
    DWORD dwDefSpeed  = 115200;

    String sLastErrMsg;

    AutoConnForm->ShowDlg( Handle, WM_AUTO_CONN_CANCELLED, "Scanning for Base Radio" );

    // Scan for BR
    for( int iPortNum = 1; iPortNum < 100; iPortNum++ )
    {
        // Check if we can connect to this port
        BRPortEdit->Text = IntToStr( iPortNum );

        if( DoBRConnect( iPortNum, dwDefSpeed, sLastErrMsg ) )
        {
            // The port could be opened, wait here for 2 seconds for a
            // a message to appear from the base radio
            DWORD dwStartPktTime = m_BRPort.lastPktTime;
            DWORD dwEndWait      = GetTickCount() + 2500;

            while( GetTickCount() < dwEndWait )
            {
                // Ensure that we check our other threads in the meantime
                Application->ProcessMessages();

                if( dwStartPktTime != m_BRPort.lastPktTime )
                {
                    bBRFound = true;
                    break;
                }

                if( m_bAutoScanCancelled )
                    break;
            }

            // If we fall through to here and we haven't found the BR,
            // be sure to do a disconnect to release the port
            if( !bBRFound )
                BRDisconnectBtnClick( BRDisconnectBtn );
        }

        // Check if we're done
        if( m_bAutoScanCancelled || bBRFound )
            break;
    }

    // Ensure we can continue
    if( !m_bAutoScanCancelled && bBRFound )
    {
        // Scan for MPLS
        AutoConnForm->InfoMsg = "Scanning for MPLS";

        for( int iPortNum = 1; iPortNum < 100; iPortNum++ )
        {
            // Check if we can connect to this port
            MPLSPortEdit->Text = IntToStr( iPortNum );

            if( DoMPLSConnect( iPortNum, dwDefSpeed, sLastErrMsg ) )
            {
                // The port could be opened, wait here for 2 seconds for a
                // a message to appear from the base radio
                DWORD dwStartPktTime = m_MPLSPort.lastPktTime;
                DWORD dwEndWait      = GetTickCount() + 5000;

                while( GetTickCount() < dwEndWait )
                {
                    // Ensure that we check our other threads in the meantime
                    Application->ProcessMessages();

                    if( dwStartPktTime != m_MPLSPort.lastPktTime )
                    {
                        bWTTTSFound = true;
                        break;
                    }

                    if( m_bAutoScanCancelled )
                        break;
                }

                // If we fall through to here and we haven't found the TT,
                // be sure to do a disconnect to release the port
                if( !bWTTTSFound )
                    MPLSDisconnectBtnClick( MPLSDisconnectBtn );
            }

            // Check if we're done
            if( m_bAutoScanCancelled || bWTTTSFound )
                break;
        }
    }

    // If cancelled, clean-up
    if( m_bAutoScanCancelled )
    {
        // If connection attempts were made but failed, disconnect
        if( !bBRFound )
            BRDisconnectBtnClick( BRDisconnectBtn );
        else if( !bWTTTSFound )
            MPLSDisconnectBtnClick( MPLSDisconnectBtn );

    }
    else if( !bBRFound )
    {
        MessageDlg( "A Base Radio was not found. Please check the connections to the Base Radio.", mtError, TMsgDlgButtons() << mbOK, 0 );
        BRDisconnectBtnClick( BRDisconnectBtn );
    }
    else if( !bWTTTSFound )
    {
        MessageDlg( "A MPLS was not found. Please ensure it is powered up. You may have to change the MPLS radio channel on the Base Radio tab.", mtError, TMsgDlgButtons() << mbOK, 0 );
        MPLSDisconnectBtnClick( MPLSDisconnectBtn );
    }

    // Fall through means we're done, close the Auto-Connect Progress form
    AutoConnForm->CloseDlg();

    // Re-Enable main page
    MainPgCtrl->Enabled = true;
}


void __fastcall TMPLSMonMain::WMDoAutoConnCancelled( TMessage &Message )
{
    m_bAutoScanCancelled = true;
}


void TMPLSMonMain::SetParamsVisibility( const UnicodeString& sParam1, const UnicodeString& sParam2, const UnicodeString& sParam3, const UnicodeString& sParam4 )
{
    Param1Label->Caption = sParam1;
    Param2Label->Caption = sParam2;
    Param3Label->Caption = sParam3;
    Param4Label->Caption = sParam4;

    Param1Label->Visible = !sParam1.IsEmpty();
    Param1Edit->Visible  = !sParam1.IsEmpty();
    Param2Label->Visible = !sParam2.IsEmpty();
    Param2Edit->Visible  = !sParam2.IsEmpty();
    Param3Label->Visible = !sParam3.IsEmpty();
    Param3Edit->Visible  = !sParam3.IsEmpty();
    Param4Label->Visible = !sParam4.IsEmpty();
    Param4Edit->Visible  = !sParam4.IsEmpty();
}


void __fastcall TMPLSMonMain::SetPktTimeoutBtnClick(TObject *Sender)
{
    String sPktTimeout = UIntToStr( (UINT)m_dwPktTimeout );

    if( InputQuery( "Set Base Radio and MPLS Packet Timeout", "Timeout, in msecs: ", sPktTimeout ) )
    {
        int iNewTimeout = sPktTimeout.ToIntDef( 0 );

        if( ( iNewTimeout > 0 ) && ( iNewTimeout < 1000 ) )
            m_dwPktTimeout = (DWORD)iNewTimeout;
        else
            Beep();
    }
}


void __fastcall TMPLSMonMain::ClearErrCountsBtnClick(TObject *Sender)
{
    m_MPLSPort.pktErrors  = 0;
    m_MPLSPort.pktFlushes = 0;

    m_RFCStats.missedCount = 0;
    m_RFCStats.duplicates  = 0;
}

