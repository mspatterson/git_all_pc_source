object MPLSMonMain: TMPLSMonMain
  Left = 0
  Top = 0
  Caption = 'MPLS Monitor'
  ClientHeight = 566
  ClientWidth = 679
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    679
    566)
  PixelsPerInch = 96
  TextHeight = 13
  object MainPgCtrl: TPageControl
    Left = 8
    Top = 8
    Width = 663
    Height = 519
    ActivePage = MPLSSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object BaseRadioSheet: TTabSheet
      Caption = 'Base Radio'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        655
        491)
      object BRStatusGrid: TValueListEditor
        Left = 312
        Top = 16
        Width = 327
        Height = 459
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        TitleCaptions.Strings = (
          'Parameter'
          'Value')
        ColWidths = (
          150
          171)
      end
      object BaseRadioCommsGB: TGroupBox
        Left = 16
        Top = 16
        Width = 281
        Height = 91
        Caption = ' Comms '
        TabOrder = 1
        object Label1: TLabel
          Left = 12
          Top = 27
          Width = 20
          Height = 13
          Caption = 'Port'
        end
        object Label4: TLabel
          Left = 12
          Top = 58
          Width = 30
          Height = 13
          Caption = 'Speed'
        end
        object BRPortEdit: TEdit
          Left = 63
          Top = 24
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 0
          Text = '1'
        end
        object BRConnectBtn: TButton
          Left = 160
          Top = 22
          Width = 105
          Height = 25
          Caption = 'Connect'
          TabOrder = 2
          OnClick = BRConnectBtnClick
        end
        object BRDisconnectBtn: TButton
          Left = 160
          Top = 53
          Width = 105
          Height = 25
          Caption = 'Disconnect'
          Enabled = False
          TabOrder = 3
          OnClick = BRDisconnectBtnClick
        end
        object BRSpeedEdit: TEdit
          Left = 63
          Top = 55
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 1
          Text = '115200'
        end
      end
      object BRActionsGB: TGroupBox
        Left = 16
        Top = 114
        Width = 281
        Height = 266
        Caption = ' Actions '
        TabOrder = 2
        object Label5: TLabel
          Left = 12
          Top = 30
          Width = 27
          Height = 13
          Caption = 'Radio'
        end
        object SetBRChanBtn: TButton
          Left = 160
          Top = 60
          Width = 105
          Height = 25
          Caption = 'Set Chan'
          Enabled = False
          TabOrder = 2
          OnClick = SetBRChanBtnClick
        end
        object BRChanCombo: TComboBox
          Left = 12
          Top = 62
          Width = 128
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = '11'
          Items.Strings = (
            '11'
            '12'
            '13'
            '14'
            '15'
            '16'
            '17'
            '18'
            '19'
            '20'
            '21'
            '22'
            '23'
            '24'
            '25'
            '26')
        end
        object BRRadioNbrCombo: TComboBox
          Left = 63
          Top = 27
          Width = 77
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = '0'
          Items.Strings = (
            '0'
            '1')
        end
        object BROutput1CB: TCheckBox
          Left = 12
          Top = 99
          Width = 28
          Height = 17
          Caption = '1'
          TabOrder = 3
        end
        object BROutput2CB: TCheckBox
          Left = 46
          Top = 99
          Width = 28
          Height = 17
          Caption = '2'
          TabOrder = 4
        end
        object BROutput3CB: TCheckBox
          Left = 79
          Top = 99
          Width = 28
          Height = 17
          Caption = '3'
          TabOrder = 5
        end
        object BROutput4CB: TCheckBox
          Left = 112
          Top = 99
          Width = 28
          Height = 17
          Caption = '4'
          TabOrder = 6
        end
        object BRSetOutputBtn: TButton
          Left = 160
          Top = 95
          Width = 105
          Height = 25
          Caption = 'Set Output'
          TabOrder = 7
          OnClick = BRSetOutputBtnClick
        end
      end
    end
    object MPLSSheet: TTabSheet
      Caption = 'MPLS'
      ImageIndex = 1
      DesignSize = (
        655
        491)
      object WTTTSCommsGB: TGroupBox
        Left = 16
        Top = 16
        Width = 281
        Height = 91
        Caption = ' Comms '
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 27
          Width = 20
          Height = 13
          Caption = 'Port'
        end
        object Label3: TLabel
          Left = 12
          Top = 58
          Width = 30
          Height = 13
          Caption = 'Speed'
        end
        object MPLSPortEdit: TEdit
          Left = 63
          Top = 24
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 0
          Text = '1'
        end
        object MPLSConnectBtn: TButton
          Left = 160
          Top = 22
          Width = 105
          Height = 25
          Caption = 'Connect'
          TabOrder = 2
          OnClick = MPLSConnectBtnClick
        end
        object MPLSDisconnectBtn: TButton
          Left = 160
          Top = 53
          Width = 105
          Height = 25
          Caption = 'Disconnect'
          Enabled = False
          TabOrder = 3
          OnClick = MPLSDisconnectBtnClick
        end
        object MPLSSpeedEdit: TEdit
          Left = 63
          Top = 55
          Width = 77
          Height = 21
          NumbersOnly = True
          TabOrder = 1
          Text = '115200'
        end
      end
      object MPLSActionsGB: TGroupBox
        Left = 16
        Top = 113
        Width = 281
        Height = 205
        Caption = ' Commands '
        TabOrder = 1
        object Param1Label: TLabel
          Left = 12
          Top = 72
          Width = 61
          Height = 13
          Caption = 'Param1Label'
        end
        object Param2Label: TLabel
          Left = 12
          Top = 104
          Width = 61
          Height = 13
          Caption = 'Param2Label'
        end
        object Param3Label: TLabel
          Left = 12
          Top = 136
          Width = 61
          Height = 13
          Caption = 'Param3Label'
        end
        object Param4Label: TLabel
          Left = 12
          Top = 168
          Width = 61
          Height = 13
          Caption = 'Param4Label'
        end
        object SendMPLSCmdBtn: TButton
          Left = 160
          Top = 26
          Width = 105
          Height = 25
          Caption = 'Send'
          Enabled = False
          TabOrder = 1
          OnClick = SendMPLSCmdBtnClick
        end
        object MPLSCmdCombo: TComboBox
          Left = 12
          Top = 28
          Width = 128
          Height = 21
          Style = csDropDownList
          TabOrder = 0
          OnClick = MPLSCmdComboClick
        end
        object Param1Edit: TEdit
          Left = 160
          Top = 69
          Width = 105
          Height = 21
          TabOrder = 2
        end
        object Param2Edit: TEdit
          Left = 160
          Top = 101
          Width = 105
          Height = 21
          TabOrder = 3
        end
        object Param3Edit: TEdit
          Left = 160
          Top = 133
          Width = 105
          Height = 21
          TabOrder = 4
        end
        object Param4Edit: TEdit
          Left = 160
          Top = 165
          Width = 105
          Height = 21
          TabOrder = 5
        end
      end
      object TesTORKPgCtrl: TPageControl
        Left = 308
        Top = 22
        Width = 338
        Height = 459
        ActivePage = RFCSheet
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 2
        object RFCSheet: TTabSheet
          Caption = 'RFC / Status'
          DesignSize = (
            330
            431)
          object MPLSStatusGrid: TValueListEditor
            Left = 8
            Top = 8
            Width = 312
            Height = 414
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 0
            TitleCaptions.Strings = (
              'Parameter'
              'Value')
            ColWidths = (
              150
              156)
          end
        end
        object DataSheet: TTabSheet
          Caption = 'Data'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          DesignSize = (
            330
            431)
          object EventPktEditor: TValueListEditor
            Left = 8
            Top = 8
            Width = 312
            Height = 414
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 0
            TitleCaptions.Strings = (
              'Parameter'
              'Value')
            ColWidths = (
              150
              156)
          end
        end
      end
      object SetPktTimeoutBtn: TButton
        Left = 16
        Top = 336
        Width = 140
        Height = 25
        Caption = 'Set Packet Timeout'
        TabOrder = 3
        OnClick = SetPktTimeoutBtnClick
      end
      object ClearErrCountsBtn: TButton
        Left = 16
        Top = 367
        Width = 140
        Height = 25
        Caption = 'Clear Error Counts'
        TabOrder = 4
        OnClick = ClearErrCountsBtnClick
      end
    end
    object AutoSheet: TTabSheet
      Caption = 'Auto'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object LoggingGB: TGroupBox
        Left = 16
        Top = 81
        Width = 620
        Height = 64
        Caption = ' Logging '
        TabOrder = 0
        DesignSize = (
          620
          64)
        object RFCDataCB: TCheckBox
          Left = 12
          Top = 28
          Width = 89
          Height = 17
          Caption = 'RFC Data'
          TabOrder = 0
        end
        object RFCDataFileEdit: TEdit
          Left = 107
          Top = 26
          Width = 446
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 1
          OnDblClick = RFCOpenFileBtnClick
        end
        object RFCOpenFileBtn: TButton
          Left = 567
          Top = 24
          Width = 36
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '...'
          TabOrder = 2
          OnClick = RFCOpenFileBtnClick
        end
      end
      object PortsGB: TGroupBox
        Left = 16
        Top = 11
        Width = 620
        Height = 60
        Caption = ' Ports '
        TabOrder = 1
        object AutoConnBtn: TButton
          Left = 12
          Top = 22
          Width = 85
          Height = 25
          Caption = 'Auto Connect'
          TabOrder = 0
          OnClick = AutoConnBtnClick
        end
      end
    end
  end
  object ExitBtn: TButton
    Left = 595
    Top = 533
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Exit'
    TabOrder = 1
    OnClick = ExitBtnClick
  end
  object WTTTSPollTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = WTTTSPollTimerTimer
    Left = 8
    Top = 518
  end
  object BRPollTimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = BRPollTimerTimer
    Left = 80
    Top = 518
  end
  object SaveLogDlg: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All File (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Log File As...'
    Left = 144
    Top = 518
  end
end
