#ifndef TMPLSMonMainH
#define TMPLSMonMainH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <time.h>
#include <Vcl.Menus.hpp>
#include "AutoConnDlg.h"
#include "CommObj.h"
#include "WTTTSProtocolUtils.h"


class TMPLSMonMain : public TForm
{
__published:	// IDE-managed Components
    TPageControl *MainPgCtrl;
    TButton *ExitBtn;
    TTabSheet *BaseRadioSheet;
    TTabSheet *MPLSSheet;
    TValueListEditor *BRStatusGrid;
    TGroupBox *BaseRadioCommsGB;
    TEdit *BRPortEdit;
    TLabel *Label1;
    TButton *BRConnectBtn;
    TButton *BRDisconnectBtn;
    TGroupBox *BRActionsGB;
    TButton *SetBRChanBtn;
    TComboBox *BRChanCombo;
    TGroupBox *WTTTSCommsGB;
    TLabel *Label2;
    TEdit *MPLSPortEdit;
    TButton *MPLSConnectBtn;
    TButton *MPLSDisconnectBtn;
    TGroupBox *MPLSActionsGB;
    TButton *SendMPLSCmdBtn;
    TComboBox *MPLSCmdCombo;
    TLabel *Label3;
    TEdit *MPLSSpeedEdit;
    TLabel *Label4;
    TEdit *BRSpeedEdit;
    TTimer *WTTTSPollTimer;
    TTimer *BRPollTimer;
    TComboBox *BRRadioNbrCombo;
    TLabel *Label5;
    TPageControl *TesTORKPgCtrl;
    TTabSheet *RFCSheet;
    TTabSheet *DataSheet;
    TValueListEditor *MPLSStatusGrid;
    TValueListEditor *EventPktEditor;
    TTabSheet *AutoSheet;
    TGroupBox *LoggingGB;
    TCheckBox *RFCDataCB;
    TEdit *RFCDataFileEdit;
    TButton *RFCOpenFileBtn;
    TSaveDialog *SaveLogDlg;
    TCheckBox *BROutput1CB;
    TCheckBox *BROutput2CB;
    TCheckBox *BROutput3CB;
    TCheckBox *BROutput4CB;
    TButton *BRSetOutputBtn;
    TGroupBox *PortsGB;
    TButton *AutoConnBtn;
    TLabel *Param1Label;
    TLabel *Param2Label;
    TLabel *Param3Label;
    TLabel *Param4Label;
    TEdit *Param1Edit;
    TEdit *Param2Edit;
    TEdit *Param3Edit;
    TEdit *Param4Edit;
    TButton *SetPktTimeoutBtn;
    TButton *ClearErrCountsBtn;
    void __fastcall ExitBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall BRDisconnectBtnClick(TObject *Sender);
    void __fastcall BRConnectBtnClick(TObject *Sender);
    void __fastcall SetBRChanBtnClick(TObject *Sender);
    void __fastcall MPLSConnectBtnClick(TObject *Sender);
    void __fastcall MPLSDisconnectBtnClick(TObject *Sender);
    void __fastcall WTTTSPollTimerTimer(TObject *Sender);
    void __fastcall BRPollTimerTimer(TObject *Sender);
    void __fastcall RFCOpenFileBtnClick(TObject *Sender);
    void __fastcall BRSetOutputBtnClick(TObject *Sender);
    void __fastcall AutoConnBtnClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall MPLSCmdComboClick(TObject *Sender);
    void __fastcall SendMPLSCmdBtnClick(TObject *Sender);
    void __fastcall SetPktTimeoutBtnClick(TObject *Sender);
    void __fastcall ClearErrCountsBtnClick(TObject *Sender);

private:
    // MPLS specific functions
    bool DoMPLSConnect( int iMPLSPort, DWORD dwBitRate, String& sErrMsg );

    bool SendMPLS2Cmd( BYTE cmdType, MPLS2_DATA_UNION& payloadData, BYTE payloadLen );

    void UpdateMPLSRFCValues( const WTTTS_PKT_HDR& pktHdr, const MPLS2_RFC_PKT& rfcPkt );
    void ShowMPLSEvent( BYTE byPktType, const MPLS2_DATA_UNION& pktData );

    bool LogRFCPacket( const String& logFileName, const WTTTS_PKT_HDR& pktHdr, const MPLS2_RFC_PKT& rfcData );

    struct {
        bool cmdPending;
        BYTE cmdType;
        BYTE payloadLen;
        MPLS2_DATA_UNION payloadData;
    } m_pendingMPLSCmd;

    // Base Radio specific functions
    bool DoBRConnect( int iBRPort, DWORD dwBitRate, String& sErrMsg );

    bool SendBRCmd( BYTE cmdType, BASE_STN_DATA_UNION& payloadData, BYTE payloadLen );

    void UpdateBRStatusValues( const BASE_STATUS_STATUS_PKT& statusPkt );

    BYTE GetSelectedRadioNbr( void );

    // Comm objects
    #define RX_BUFF_SIZE 2048

    typedef struct {
        CCommObj*  commPort;
        BYTE       rxBuffer[RX_BUFF_SIZE];
        DWORD      rxBuffCount;
        DWORD      lastRxTime;
        DWORD      lastPktTime;
        DWORD      pktErrors;
        DWORD      pktFlushes;
        bool       bUsingUDP;   // True if using UDP comms
        AnsiString sIPAddr;     // If using UDP, IP addr that last sent a packet
        WORD       wUDPPort;    // If using UDP, port from which last packet came from
    } PORT_INFO;

    PORT_INFO m_BRPort;
    PORT_INFO m_MPLSPort;

    struct {
        DWORD missedCount;
        DWORD duplicates;
        WORD  wLastSeqNbr;
    } m_RFCStats;

    DWORD     m_dwPktTimeout;

    bool m_bAutoScanCancelled;

    // Common handlers
    bool      CreatePort( int portNbr, DWORD portSpeed, PORT_INFO& portInfo, String& sErrMsg );
    void      ReleasePort( PORT_INFO& portInfo );

    bool      ValidatePortSettings( TEdit* portEdit, TEdit* speedEdit );

    BYTE      GetChanNbr( TComboBox* pwrCombo );

    void      PollPortForData( PORT_INFO& portInfo );

    void      UpdateStatusGrid( TValueListEditor* aGrid, const UnicodeString& key, const UnicodeString& text );
    void      ClearStatusGrid( TValueListEditor* aGrid );

    void __fastcall WMDoAutoConn         ( TMessage &Message );
    void __fastcall WMDoAutoConnCancelled( TMessage &Message );

    #define WM_DO_AUTO_CONN         ( WM_APP + 1 )
    #define WM_AUTO_CONN_CANCELLED  ( WM_APP + 2 )

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_DO_AUTO_CONN,        TMessage, WMDoAutoConn          )
      MESSAGE_HANDLER( WM_AUTO_CONN_CANCELLED, TMessage, WMDoAutoConnCancelled )
    END_MESSAGE_MAP(TForm)

    void SetParamsVisibility ( const UnicodeString& sParam1, const UnicodeString& sParam2, const UnicodeString& sParam3, const UnicodeString& sParam4 );

public:		// User declarations
    __fastcall TMPLSMonMain(TComponent* Owner);
};

extern PACKAGE TMPLSMonMain *MPLSMonMain;

#endif
