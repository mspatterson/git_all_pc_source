#include <vcl.h>
#pragma hdrstop

#include "MPLSSimMain.h"
#include "TCalFactorsDlg.h"
#include "TypeDefs.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TMPLSimMainForm *MPLSimMainForm;


__fastcall TMPLSimMainForm::TMPLSimMainForm(TComponent* Owner) : TForm(Owner)
{
    // Winsock is required for UDP comms. Init it now
    InitWinsock();

    // Create frames
    m_pMasterTEC = new TTECFrame( this, MasterTECSheet, "MasterTECFrame", true  );
    m_pSlaveTEC  = new TTECFrame( this, SlaveTECSheet,  "SlaveTECFrame",  false );

    m_pMasterTEC->OnShowConfigData = OnShowConfigData;
    m_pSlaveTEC->OnShowConfigData  = OnShowConfigData;

    m_pLogger = new TLoggingFrame( this, LogSheet, "LoggingFrame" );

    // Position frames in respective holders
    m_pMasterTEC->Align = alClient;
    m_pSlaveTEC->Align  = alClient;
    m_pLogger->Align    = alClient;

    // Logger frame has no 'close' capability
    m_pLogger->EnableCloseBtn = false;

    // Create simulator object and hook its event handlers
    m_MPLSSim = new TMPLSSimObject();

    m_MPLSSim->OnWriteMsgToLog   = SimLogMsgEvent;
    m_MPLSSim->OnSendData        = SimSendDataEvent;
    m_MPLSSim->OnTECChanged      = SimTECChangedEvent;
    m_MPLSSim->OnDetectorChanged = SimDetChangedEvent;
    m_MPLSSim->OnGetDetectorData = SimGetDetDataEvent;
    m_MPLSSim->OnGetTECData      = SimGetTECDataEvent;
    m_MPLSSim->OnGetDeviceIDs    = SimGetDevIDsEvent;
}


void __fastcall TMPLSimMainForm::FormShow(TObject *Sender)
{
    // On show, force the frames to align their children
    m_pMasterTEC->DoOnShow();
    m_pSlaveTEC->DoOnShow();

    // Go to first tab
    MainPageControl->ActivePageIndex = 0;
}


void __fastcall TMPLSimMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Fake disconnect click
    DisconnectBtnClick( DisconnectBtn );

    ShutdownWinsock();

    // Delete sim object
    delete m_MPLSSim;
}


void __fastcall TMPLSimMainForm::ConnectBtnClick(TObject *Sender)
{
    // Create the comm object
    if( SerialModeRB->Checked )
    {
        CCommPort::CONNECT_PARAMS portParams;

        portParams.iPortNumber   = CommPortEdit->Text.ToInt();
        portParams.dwLineBitRate = BitRateEdit->Text.ToInt();

        m_commObj = new CCommPort();

        if( m_commObj->Connect( &portParams ) == CCommObj::ERR_NONE )
        {
            m_pLogger->AddLogMessage( "COM" + CommPortEdit->Text + " opened" );
        }
    }
    else
    {
        CUDPPort::CONNECT_PARAMS portParams;

        // The information entered here is the dest info for the MPLS app
        // We open an unbound port to transmit on
        portParams.destAddr = HostIPEdit->Text;
        portParams.destPort = UDPPortEdit->Text.ToInt();
        portParams.portNbr  = 0;
        portParams.acceptBroadcasts = true;

        m_commObj = new CUDPPort();

        if( m_commObj->Connect( &portParams ) == CCommObj::ERR_NONE )
        {
            m_pLogger->AddLogMessage( "UDP" + HostIPEdit->Text + ":" + UDPPortEdit->Text + " opened" );
        }
    }

    // Clear comms vars on connect
    m_dwLastRxTime = 0;
    m_RxBuffCount  = 0;
    m_TxBuffCount  = 0;

    // On success, set default values
    m_MPLSSim->ResetToDefaults();
    m_MPLSSim->HasSlave = DualTECSystemRB->Checked;

    RefreshControls();

    EnableCommControls( false /*connect disabled*/ );

    SimTimer->Enabled = true;
}


void __fastcall TMPLSimMainForm::DisconnectBtnClick(TObject *Sender)
{
    // Kill the timer first
    SimTimer->Enabled = false;

    if( m_commObj != NULL )
    {
        delete m_commObj;
        m_commObj = NULL;
    }

    EnableCommControls( true /*can connect*/ );

    // Clear comms vars on connect
    m_dwLastRxTime = 0;
    m_RxBuffCount  = 0;
    m_TxBuffCount  = 0;

    // Clear status panel dispaly
    StatusBar->Panels->Items[0]->Text = "";
}


void __fastcall TMPLSimMainForm::WMCloseConnection( TMessage &Message )
{
    DisconnectBtnClick( DisconnectBtn );
}


void TMPLSimMainForm::EnableCommControls( bool bCanConnect )
{
    for( int iCtrl = 0; iCtrl < ConnectionGB->ControlCount; iCtrl++ )
    {
        TRadioButton* pRB = dynamic_cast<TRadioButton*>( ConnectionGB->Controls[iCtrl] );

        if( pRB != NULL )
            pRB->Enabled = bCanConnect;

        TEdit* pEdit = dynamic_cast<TEdit*>( ConnectionGB->Controls[iCtrl] );

        if( pEdit != NULL )
        {
            pEdit->Enabled = bCanConnect;
            pEdit->Color   = bCanConnect ? clWindow : clBtnFace;
        }
    }

    for( int iCtrl = 0; iCtrl < SystemSetupGB->ControlCount; iCtrl++ )
    {
        TRadioButton* pRB = dynamic_cast<TRadioButton*>( SystemSetupGB->Controls[iCtrl] );

        if( pRB != NULL )
            pRB->Enabled = bCanConnect;
    }

    ConnectBtn->Enabled    =  bCanConnect;
    DisconnectBtn->Enabled = !bCanConnect;
}


void __fastcall TMPLSimMainForm::SimTimerTimer(TObject *Sender)
{
    // Check for inbound messages
    DWORD dwRxBytes = m_commObj->CommRecv( &(m_RxBuff[m_RxBuffCount]), COMM_BUFF_LEN - m_RxBuffCount );

    m_RxBuffCount += dwRxBytes;

    if( dwRxBytes > 0 )
        m_dwLastRxTime = GetTickCount();

    if( m_MPLSSim->ProcessReceivedData( m_RxBuff, m_RxBuffCount ) )
    {
        // MPLSSim object will have pulled off any bytes. Could update stats here
    }
    else
    {
        // Flush the data if its gone stale
        if( ( m_RxBuffCount > 0 ) && HaveTimeout( m_dwLastRxTime, 250 ) )
            m_RxBuffCount = 0;
    }

    // Update the status panel time.
    StatusBar->Panels->Items[1]->Text = Now().TimeString() + "   ";
}


void __fastcall TMPLSimMainForm::OnShowConfigData( TObject* pSender )
{
    //  1. Get config data from sim obj for master or slave TEC, based
    //     on sender type
    //  2. Copy config data to dlg, then show dlg modally
    //  3. If ok pressed, update config data in sim obj
    //  4. Might need to indicate to TEC frame that it needs to update its display data
    eMPLSUnit eUnit;

    if( pSender == m_pMasterTEC )
        eUnit = eMU_MasterMPLS;
    else if( pSender == m_pSlaveTEC )
        eUnit = eMU_SlaveMPLS;
    else
        return;

    for( BYTE byPage = 0; byPage < NBR_WTTTS_CFG_PAGES; byPage++ )
    {
        WTTTS_CFG_DATA pgData;
        pgData.pageNbr = byPage;
        pgData.result  = 0;

        if( m_MPLSSim->GetConfigData( eUnit, byPage, pgData.pageData, NBR_BYTES_PER_WTTTS_PAGE ) )
            CalFactorsDlg->SetConfigData( pgData );
    }

    if( CalFactorsDlg->ShowModal() == mrOk )
    {
        for( BYTE byPage = 0; byPage < NBR_WTTTS_CFG_PAGES; byPage++ )
        {
            WTTTS_CFG_DATA pgData;
            pgData.pageNbr = byPage;
            pgData.result  = 0;

            if( CalFactorsDlg->GetConfigData( byPage, pgData ) )
                m_MPLSSim->SetConfigData( eUnit, byPage, pgData.pageData, NBR_BYTES_PER_WTTTS_PAGE );
        }
    }

}


void TMPLSimMainForm::RefreshControls( void )
{
    // Syncs all visual controls with the current state of the sim object
    // Use the event handlers to do this
    SimTECChangedEvent( NULL, eMU_MasterMPLS );
    SimTECChangedEvent( NULL, eMU_SlaveMPLS );

    SimDetChangedEvent( NULL );
}


void __fastcall TMPLSimMainForm::SimLogMsgEvent( TObject* pSender, const String& sLogMsg )
{
    // Event handler fired when simulator has a message to be written
    // to the system log
    m_pLogger->AddLogMessage( sLogMsg );
}


bool __fastcall TMPLSimMainForm::SimSendDataEvent( TObject* pSender, const BYTE* pData, int nbrBytes )
{
    // Event called when the sim object wants to send data
    if( ( pData == NULL ) || ( nbrBytes == 0 ) )
        return false;

    DWORD bytesSent = m_commObj->CommSend( pData, nbrBytes );

    if( bytesSent == (DWORD)nbrBytes )
        return true;

    // On fall we have an error
    m_pLogger->AddLogMessage( "SimSendDataEvent(): could not transmit response" );

    return false;
}


void __fastcall TMPLSimMainForm::SimTECChangedEvent( TObject* pSender, eMPLSUnit eUnit )
{
    // Event handler that fires when the host has updated configuration data
    // for a TEC, indicating that the frame should update its display.
    // These events will fire during the call to ProcessReceivedData(). Event
    // also fired if no commands received but a device state has changed on its
    // own (eg, solenoid timing out ).
    TTECFrame* pFrame = ( eUnit == eMU_MasterMPLS ) ? m_pMasterTEC : m_pSlaveTEC;

    pFrame->SetSolenoidStatus( m_MPLSSim->ActiveSolenoid[eUnit] == 1,
                               m_MPLSSim->ActiveSolenoid[eUnit] == 2,
                               m_MPLSSim->ActiveSolenoid[eUnit] == 3,
                               m_MPLSSim->ActiveSolenoid[eUnit] == 4,
                               m_MPLSSim->ActiveSolenoid[eUnit] == 5 );

    pFrame->SetDigIOStatus( m_MPLSSim->IsolOutStatus[eUnit][0],
                            m_MPLSSim->IsolOutStatus[eUnit][1],
                            m_MPLSSim->IsolOutStatus[eUnit][2],
                            m_MPLSSim->IsolOutStatus[eUnit][3] );

    TTECFrame::TEC_PARAMS tecParams = { 0 };

    for( int iADNbr = 0; iADNbr < NBR_AD_INPUTS_PER_TEC; iADNbr++ )
        m_MPLSSim->GetADParams( eUnit, iADNbr, tecParams.adParama[iADNbr] );

    pFrame->SetTECParams( tecParams );

    // Set TEC timing params
    TTECFrame::TEC_TIMING tecTiming = { 0 };

    tecTiming.iRFCRate     = m_MPLSSim->RFCRate;
    tecTiming.iRFCTimeout  = m_MPLSSim->RFCTimeout;
    tecTiming.iPairTimeout = m_MPLSSim->PairTimeout;

    pFrame->SetTimingParams( tecTiming );
}


void __fastcall TMPLSimMainForm::SimDetChangedEvent( TObject* pSender )
{
    // Event handler that fires when the host has updated configuration data
    // for the Plug Det, indicating that the frame should update its display.
    // These events will fire during the call to ProcessReceivedData(). Event
    // also fired if no commands received but a device state has changed on its
    // own.
    int iRateEnum;
    int iAvging;

    m_MPLSSim->GetDetParams( iRateEnum, iAvging );

    SetDetectorParams( iRateEnum, iAvging );
}


void __fastcall TMPLSimMainForm::SimGetTECDataEvent( TObject* pSender, eMPLSUnit eUnit )
{
    // Event handler fired when the MPLS object needs current data
    // for a unit. Caller must call the sim's SetTECData() method
    // from within this event handler
    TMPLSSimObject::TEC_READINGS newReadings = { 0 };

    if( eUnit == eMU_MasterMPLS )
        m_pMasterTEC->GetTECData( newReadings );
    else
        m_pSlaveTEC->GetTECData( newReadings );

    m_MPLSSim->SetTECReadings( eUnit, newReadings );
}


WORD __fastcall TMPLSimMainForm::SimGetDetDataEvent( TObject* pSender, WORD* pReadings, WORD maxNbrReadings, bool& bHaveError )
{
    // Simulator needs current plug detector data. Event handler must fill the
    // pass buffer with data.
    return GetDetectorData( pReadings, maxNbrReadings, bHaveError );
}


void __fastcall TMPLSimMainForm::SimGetDevIDsEvent( TObject* pSender )
{
    // Simulator needs device IDs for all units. Call SetDeviceIDs() from within
    // this event handler. All device IDs are returned regardless of config.
    // Its up to the sim object to decide what to report
    TMPLSSimObject::DEVICE_ID masterTEC;
    TMPLSSimObject::DEVICE_ID masterPIB;
    TMPLSSimObject::DEVICE_ID slaveTEC;
    TMPLSSimObject::DEVICE_ID slavePIB;
    TMPLSSimObject::DEVICE_ID plugDet;

    m_pMasterTEC->GetDeviceIDs( masterTEC, masterPIB );
    m_pSlaveTEC->GetDeviceIDs ( slaveTEC,  slavePIB );

    GetPlugDetIDs( plugDet );

    m_MPLSSim->SetDeviceIDs( masterTEC, masterPIB, slaveTEC, slavePIB, plugDet );
}


//
// Plug Detector Methods
//

void TMPLSimMainForm::GetPlugDetIDs( TMPLSSimObject::DEVICE_ID& plugDet )
{
    plugDet.sSerialNbr = Trim( DevIDEdit->Text );
    plugDet.dwFWRev    = FWRevEdit->Text.ToInt();
    plugDet.dwHWRev    = HWRevEdit->Text.ToInt();
}


WORD TMPLSimMainForm::GetDetectorData( WORD* pReadings, WORD iMaxNbrReadings, bool& bHaveError )
{
    bHaveError = DetCommErrorCB->Checked;

    if( pReadings == NULL )
        return 0;

    int iReadingsSet = 0;

    if( DetRepeatDataCB->Checked )
    {
        // Assume fixed data in this case. Edit is in hex
        String sData = Trim( DetRepeatDataEdit->Text );

        iReadingsSet = 0;

        while( sData.Length() > 0 )
        {
            sData = Trim( sData );

            if( sData.Length() == 0 )
                break;

            int wordSepPos = sData.Pos( " " );

            if( wordSepPos == 0 )
            {
                WORD wData = (WORD)StrToIntDef( "$" + sData, 0 );

                pReadings[iReadingsSet] = wData;
                iReadingsSet++;

                sData = "";
            }
            else
            {
                String sNewWord = sData.SubString( 1, wordSepPos - 1 );

                WORD wData = (WORD)StrToIntDef( "$" + sNewWord, 0 );

                pReadings[iReadingsSet] = wData;
                iReadingsSet++;

                sData = sData.Delete( 1, wordSepPos );
            }

            if( iReadingsSet >= iMaxNbrReadings )
                break;
        }
    }
//    else if( DetFromFileCB->Checked )
//    {
// TODO
//    }
    else
    {
        // Assume fixed data in this case. Edit is in hex
        WORD wData = (WORD)StrToIntDef( "$" + DetFixedDataEdit->Text, 0 );

        for( int iItem = 0; iItem < iMaxNbrReadings; iItem++ )
            pReadings[iItem] = wData;

        iReadingsSet = iMaxNbrReadings;
    }

    return iReadingsSet;
}


void TMPLSimMainForm::SetDetectorParams( int iRateEnum, int iAvging )
{
    DetReadRateEdit->Text = IntToStr( iRateEnum );
    DetAvgingEdit->Text   = IntToStr( iAvging );
}

