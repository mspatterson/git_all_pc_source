//---------------------------------------------------------------------------
#ifndef MPLSSimMainH
#define MPLSSimMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include <Vcl.ComCtrls.hpp>
#include "WTTTSProtocolUtils.h"
#include "CUDPPort.h"
#include "Comm32.h"
#include "TTecFrame.h"
#include "TLoggingFrame.h"
#include "TMPLSSimObject.h"
//---------------------------------------------------------------------------
class TMPLSimMainForm : public TForm
{
__published:	// IDE-managed Components
    TTimer *SimTimer;
    TStatusBar *StatusBar;
    TPageControl *MainPageControl;
    TTabSheet *CommsSheet;
    TGroupBox *ConnectionGB;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *Label4;
    TEdit *CommPortEdit;
    TEdit *BitRateEdit;
    TEdit *HostIPEdit;
    TEdit *UDPPortEdit;
    TRadioButton *SerialModeRB;
    TRadioButton *UDPModeRB;
    TButton *ConnectBtn;
    TButton *DisconnectBtn;
    TTabSheet *MasterTECSheet;
    TTabSheet *SlaveTECSheet;
    TTabSheet *PlugDetSheet;
    TTabSheet *AboutSheet;
    TImage *TescoLogo;
    TTabSheet *LogSheet;
    TGroupBox *DevInfoGB;
    TLabel *Label11;
    TLabel *Label12;
    TLabel *Label13;
    TEdit *HWRevEdit;
    TEdit *FWRevEdit;
    TEdit *DevIDEdit;
    TGroupBox *PDParamsGB;
    TLabel *Label5;
    TEdit *DetReadRateEdit;
    TLabel *Label7;
    TGroupBox *PDDataGB;
    TRadioButton *DetRepeatDataCB;
    TLabel *Label9;
    TEdit *DetRepeatDataEdit;
    TLabel *Label10;
    TRadioButton *DetFromFileCB;
    TLabel *Label14;
    TEdit *DetFromFileEdit;
    TButton *DetBrowseDataFileBtn;
    TGroupBox *SystemSetupGB;
    TRadioButton *SingleTECSystemRB;
    TRadioButton *DualTECSystemRB;
    TCheckBox *DetCommErrorCB;
    TEdit *DetAvgingEdit;
    TRadioButton *DetFixedDataRB;
    TEdit *DetFixedDataEdit;
    TLabel *Label6;
    TLabel *Label8;
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall ConnectBtnClick(TObject *Sender);
    void __fastcall DisconnectBtnClick(TObject *Sender);
    void __fastcall SimTimerTimer(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);

private:

    // Comms vars
    CCommObj* m_commObj;

    #define COMM_BUFF_LEN  2048
    BYTE      m_RxBuff[COMM_BUFF_LEN];
    DWORD     m_RxBuffCount;
    DWORD     m_dwLastRxTime;

    BYTE      m_TxBuff[COMM_BUFF_LEN];
    DWORD     m_TxBuffCount;

    // Frame vars
    TTECFrame*     m_pMasterTEC;
    TTECFrame*     m_pSlaveTEC;
    TLoggingFrame* m_pLogger;

    // MPLS Sim object and event handlers
    TMPLSSimObject* m_MPLSSim;

    void __fastcall SimLogMsgEvent( TObject* pSender, const String& sLogMsg );

    bool __fastcall SimSendDataEvent( TObject* pSender, const BYTE* pData, int nbrBytes );

    void __fastcall SimTECChangedEvent( TObject* pSender, eMPLSUnit eUnit );
    void __fastcall SimDetChangedEvent( TObject* pSender );

    void __fastcall SimGetTECDataEvent( TObject* pSender, eMPLSUnit eUnit );
    WORD __fastcall SimGetDetDataEvent( TObject* pSender, WORD* pReadings, WORD maxNbrReadings, bool& bHaveError );
    void __fastcall SimGetDevIDsEvent ( TObject* pSender );

    // Event handlers
    void __fastcall OnShowConfigData( TObject* pSender );

    bool SendMPLEvent( BYTE pktType, WORD payloadLen, const void* pPayload );

    void EnableCommControls( bool bCanConnect );

    void RefreshControls( void );

    // Plug detector isn't a frame, but write helper methods to work with it
    // in case it ever becomes one
    void GetPlugDetIDs( TMPLSSimObject::DEVICE_ID& plugDet );
    WORD GetDetectorData( WORD* pReadings, WORD iMaxNbrReadings, bool& bHaveError );
    void SetDetectorParams( int iRateEnum, int iAvging );

    // Windows messages
    #define WM_CLOSE_CONNECTION  ( WM_APP + 1 )
         // WParam: RFU
         // LParam: RFU

    void __fastcall WMCloseConnection( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_CLOSE_CONNECTION, TMessage, WMCloseConnection )
    END_MESSAGE_MAP(TForm)

public:
    __fastcall TMPLSimMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMPLSimMainForm *MPLSimMainForm;
//---------------------------------------------------------------------------
#endif
