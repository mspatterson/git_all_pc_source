#include <vcl.h>
#pragma hdrstop

#include "TCalFactorsDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TCalFactorsDlg *CalFactorsDlg;


__fastcall TCalFactorsDlg::TCalFactorsDlg(TComponent* Owner) : TForm(Owner)
{
    // Initialize the grid on boot. Clear out all factors
    CalGrid->ColCount = NBR_BYTES_PER_WTTTS_PAGE + 1;
    CalGrid->RowCount = NBR_WTTTS_CFG_PAGES      + 1;

    CalGrid->Cells[0][0] = "Page";

    for( int iHdrCol = 1; iHdrCol < CalGrid->ColCount; iHdrCol++ )
        CalGrid->Cells[iHdrCol][0] = "Byte " + IntToStr( iHdrCol - 1 );

    for( int iRow = 1; iRow < CalGrid->RowCount; iRow++ )
    {
        CalGrid->Cells[0][iRow] = IntToStr( iRow - 1 );

        for( int iHdrCol = 1; iHdrCol < CalGrid->ColCount; iHdrCol++ )
            CalGrid->Cells[iHdrCol][iRow] = "255";
    }
}


void __fastcall TCalFactorsDlg::LoadCalFactorsBtnClick(TObject *Sender)
{
    if( OpenDlg->Execute() )
    {
        TStringList* cfgData  = new TStringList();
        TStringList* csvItems = new TStringList();

        int rowsLoaded = 0;

        try
        {
            cfgData->LoadFromFile( OpenDlg->FileName );

            // Load each line - assume there is a header row in the file
            for( int iLine = 1; iLine < cfgData->Count; iLine++ )
            {
                if( iLine >= CalGrid->RowCount )
                    break;

                csvItems->CommaText = cfgData->Strings[ iLine ];

                if( csvItems->Count >= CalGrid->ColCount )
                {
                    // Assume that the first is the cal factor number - skip it
                    for( int iItem = 1; iItem < CalGrid->ColCount; iItem++ )
                        CalGrid->Cells[iItem][iLine] = csvItems->Strings[iItem];
                }

                rowsLoaded++;
            }

            MessageDlg( "Config data file read, " + IntToStr( rowsLoaded ) + " rows loaded.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
        }
        catch( ... )
        {
            MessageDlg( "An error occurred loading the config data file! " + IntToStr( rowsLoaded ) + " rows loaded.", mtError, TMsgDlgButtons() << mbOK, 0 );
        }

        delete cfgData;
        delete csvItems;
    }
}


void __fastcall TCalFactorsDlg::SaveCalFactorsBtnClick(TObject *Sender)
{
    if( SaveDlg->Execute() )
    {
        TStringList* cfgData = new TStringList();
        TStringList* csvItems   = new TStringList();

        try
        {
            // Save each line - save the header line too
            for( int iLine = 0; iLine < CalGrid->RowCount; iLine++ )
            {
                csvItems->Clear();

                for( int iItem = 0; iItem < CalGrid->ColCount; iItem++ )
                    csvItems->Add( CalGrid->Cells[iItem][iLine] );

                cfgData->Add( csvItems->CommaText );
            }

            cfgData->SaveToFile( SaveDlg->FileName );

            MessageDlg( "Calbration file saved.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
        }
        catch( ... )
        {
            MessageDlg( "An error occurred save the cal factors!", mtError, TMsgDlgButtons() << mbOK, 0 );
        }

        delete cfgData;
        delete csvItems;
    }
}


bool TCalFactorsDlg::GetConfigData( BYTE whichPage, WTTTS_CFG_DATA& pgData )
{
    // Init response, assuming the worst
    pgData.pageNbr = whichPage;
    pgData.result  = WTTTS_PG_RESULT_BAD_PG;

    memset( pgData.pageData, 0xFF, NBR_BYTES_PER_WTTTS_PAGE );

    try
    {
        int tableRow = (int)whichPage + 1;

        if( tableRow >= CalGrid->RowCount )
            Abort();

        // All cal factors are displayed as integer values. Convert
        // directly to array slots.
        for( int iByte = 0; iByte < NBR_BYTES_PER_WTTTS_PAGE; iByte++ )
            pgData.pageData[iByte] = CalGrid->Cells[iByte+1][tableRow].ToIntDef( 255 );

        pgData.result = WTTTS_PG_RESULT_SUCCESS;
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool TCalFactorsDlg::SetConfigData( const WTTTS_CFG_DATA& pgData )
{
    try
    {
        int tableRow = (int)pgData.pageNbr + 1;

        if( tableRow >= CalGrid->RowCount )
            Abort();

        for( int iByte = 0; iByte < NBR_BYTES_PER_WTTTS_PAGE; iByte++ )
            CalGrid->Cells[iByte+1][tableRow] = IntToStr( pgData.pageData[iByte] );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


