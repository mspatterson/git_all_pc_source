#include <vcl.h>
#pragma hdrstop

#include <math.hpp>

#include "TMPLSSimObject.h"
#include "TypeDefs.h"

#pragma package(smart_init)


static BYTE IntToByte( int iValue )
{
    XFER_BUFFER xferBuff;

    xferBuff.iData = iValue;

    return xferBuff.byData[0];
}


static WORD IntToWORD( int iValue )
{
    XFER_BUFFER xferBuff;

    xferBuff.iData = iValue;

    return xferBuff.wData[0];
}


static void IntToBiByte( int iValue, BYTE* pBytes )
{
    XFER_BUFFER xferBuff;

    xferBuff.iData = iValue;

    pBytes[0] = xferBuff.byData[0];
    pBytes[1] = xferBuff.byData[1];
}


static void IntToTriByte( int iValue, BYTE* pBytes )
{
    XFER_BUFFER xferBuff;

    xferBuff.iData = iValue;

    pBytes[0] = xferBuff.byData[0];
    pBytes[1] = xferBuff.byData[1];
    pBytes[2] = xferBuff.byData[2];
}


static void ConvertToTriByte( int iValue, BYTE* pBytes, int iOffset, float fSpan )
{
    if( IsNan( fSpan ) || ( fSpan == 0.0 ) )
    {
        IntToTriByte( 0, pBytes );
    }
    else
    {
        float fValue = (float)iValue / fSpan + (float)iOffset;

        IntToTriByte( (int)fValue, pBytes );
    }
}


static void StringToSNArray( const String sSN, BYTE byArray[] )
{
    const int SNArrayLen = 32;

    if( sSN.Length() == 0 )
    {
        // Special case - SN missing
        memset( byArray, 0x00, SNArrayLen );
    }
    else
    {
        // Clear buffer to FLASH 'erased' value
        memset( byArray, 0xFF, SNArrayLen );

        int charsToCopy = sSN.Length();

        if( charsToCopy >= SNArrayLen )
            charsToCopy = SNArrayLen - 1;

        // Copy up to max number of permitted bytes
        AnsiString asSN = sSN;

        memcpy( byArray, asSN.c_str(), charsToCopy );

        // Null-terminate the array
        byArray[charsToCopy] = 0x00;
    }
}


// Ack packet error values
#define ACK_PKT_SUCCESS      0
#define ACK_PKT_PARAM_ERROR  1
#define ACK_PKT_SLAVE_ERROR  2
#define ACK_PKT_BAD_CMD      3


//
// Class Implementation
//

__fastcall TMPLSSimObject::TMPLSSimObject()
{
    // Init class vars
    ResetToDefaults();
}


__fastcall TMPLSSimObject::~TMPLSSimObject()
{
}


void TMPLSSimObject::ResetToDefaults( void )
{
    // Resets the object to default values. Set all values to zero first
    m_lastRFCTime = 0;
    m_seqNbr      = 0;

    m_bHasSlave = false;

    m_didMasterTEC.sSerialNbr = "";
    m_didMasterTEC.dwFWRev    = 0xFFFFFFFF;
    m_didMasterTEC.dwHWRev    = 0xFFFFFFFF;

    m_didMasterPIB.sSerialNbr = "";
    m_didMasterPIB.dwFWRev    = 0xFFFFFFFF;
    m_didMasterPIB.dwHWRev    = 0xFFFFFFFF;

    m_didSlaveTEC.sSerialNbr = "";
    m_didSlaveTEC.dwFWRev    = 0xFFFFFFFF;
    m_didSlaveTEC.dwHWRev    = 0xFFFFFFFF;

    m_didSlavePIB.sSerialNbr = "";
    m_didSlavePIB.dwFWRev    = 0xFFFFFFFF;
    m_didSlavePIB.dwHWRev    = 0xFFFFFFFF;

    m_didPlugDet.sSerialNbr = "";
    m_didPlugDet.dwFWRev    = 0xFFFFFFFF;
    m_didPlugDet.dwHWRev    = 0xFFFFFFFF;

    memset( &m_tecParams,    0, sizeof( m_tecParams ) );
    memset( &m_detParams,    0, sizeof( m_detParams ) );
    memset( &m_tecRates,     0, sizeof( MPLS2_RATES_PKT ) );
    memset( &m_detectorData, 0, sizeof( &m_detectorData ) );

    // Now override values with defaults
    m_tecRates.rfcRate     = 1000;   // msecs
    m_tecRates.rfcTimeout  =   10;   // msecs
    m_tecRates.pairTimeout =  900;   // secs

    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        m_tecParams[iUnit].currValues.bReportTECError = true;
        m_tecParams[iUnit].currValues.bReportPIBError = true;
    }

    // Initialize config memory
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        // Init to all FF first
        memset( m_tecParams[iUnit].cfgData, 0xFF, NBR_WTTTS_CFG_PAGES * NBR_BYTES_PER_WTTTS_PAGE );

        // Copy int default manufacturing info first
        BYTE byDefMfgRec[NBR_BYTES_PER_WTTTS_PAGE] = {
            21, 32,                             // WORD devSN, 43d 21d = 0x2B15 = 11029
            'M', 'L', 'Y', 'N', 'X', 0, 0, 0,   // char szMfgInfo[8]
            0, 19,                              // Mth (0 = Jan), Years since 2000
            0, 0, 0, 0                          // RFU
        };

        BYTE* pMfgDataArea = &( m_tecParams[iUnit].cfgData[0] );

        memcpy( pMfgDataArea, byDefMfgRec, NBR_BYTES_PER_WTTTS_PAGE );

        // Copy in default cal data next
        BYTE* pCalDataArea = &( m_tecParams[iUnit].cfgData[FIRST_RDWR_CFG_PAGE * NBR_BYTES_PER_WTTTS_PAGE] );

        memcpy( pCalDataArea, &DefaultMPLSV2CalData, sizeof( MPLS_CAL_DATA_STRUCT_V2 ) );
    }
}


bool TMPLSSimObject::ProcessReceivedData( BYTE* pRxBuffer, DWORD& dwBuffLen )
{
    // First, update the state of the solenoids
    UpdateSolenoids();

    // Look for inbound commands from the software
    WTTTS_PKT_HDR    pktHdr;
    MPLS2_DATA_UNION pktData;

    bool havePkt = false;

    if( HaveMPLS2CmdPkt( pRxBuffer, dwBuffLen, pktHdr, pktData ) )
    {
        // Process the command.
        havePkt = true;

        bool replyWithRFC = true;

        switch( pktHdr.pktType )
        {
            case WTTTS_CMD_NO_CMD:
                replyWithRFC = false;
                PostMessageToLog( "No Cmd received" );
                break;

            case WTTTS_CMD_SET_RATE:
                // SetRates() will reply with an ACK for this command
                replyWithRFC = false;
                SetRates( pktData.ratePkt );
                break;

            case WTTTS_CMD_MPLS2_QUERY_VER:
                // SendVersionPkt() will reply with a version command
                replyWithRFC = false;
                SendVersionPkt( pktData.queryVerPkt );
                break;

            case WTTTS_CMD_GET_CFG_DATA:
                // GetCfgData() will reply with config data command
                replyWithRFC = false;
                GetCfgData( pktData.cfgRequest );
                break;

            case WTTTS_CMD_SET_CFG_DATA:
                // Respond with RFC for this
                SetCfgData( pktData.cfgData );
                break;

            case WTTTS_CMD_SET_RF_CHANNEL:
                // Respond with RFC for this
                SetRFChan( pktData.chanPkt );
                break;

            case WTTTS_CMD_SET_RF_POWER:
                // Respond with RFC for this
                SetRFPower( pktData.pwrPkt );
                break;

            case WTTTS_CMD_MARK_CHAN_IN_USE:
                // Respond with RFC for this
                MarkChanInUse();
                break;

            case WTTTS_CMD_MPLS2_SET_PIB_OUT:
                // SetPIBOutputs() will reply with an ACK
                replyWithRFC = false;
                SetSolenoids( pktData.pibOutsPkt );
                break;

            case WTTTS_CMD_MPLS2_SET_TEC_PAR:
                // SetTECParams() will reply with an ACK
                replyWithRFC = false;
                SetTECParams( pktData.tecParamsPkt );
                break;

            case WTTTS_CMD_MPLS2_SET_DET_PAR:
                // SetDetParams() will reply with an ACK
                replyWithRFC = false;
                SetDetParams( pktData.detParamsPkt );
                break;

            default:
                replyWithRFC = false;
                SendACK( pktHdr.pktType, ACK_PKT_BAD_CMD );
                PostMessageToLog( "Unknown cmd received: " + IntToHex( pktHdr.pktType, 2 ) );
                break;
        }

        if( replyWithRFC )
            SendRequestForCmd( true /*send now*/ );
    }
    else
    {
        // Send a RFC, if its time to
        SendRequestForCmd( false /*send if time to*/ );
    }

    return havePkt;
}


void TMPLSSimObject::SetDeviceIDs( const DEVICE_ID& masterTEC, const DEVICE_ID& masterPIB,
                                   const DEVICE_ID& slaveTEC,  const DEVICE_ID& slavePIB,
                                   const DEVICE_ID& plugDet )
{
    m_didMasterTEC = masterTEC;
    m_didMasterPIB = masterPIB;
    m_didSlaveTEC  = slaveTEC;
    m_didSlavePIB  = slavePIB;
    m_didPlugDet   = plugDet;
}


void TMPLSSimObject::SetTECReadings( eMPLSUnit whichUnit, const TEC_READINGS& newReadings )
{
    // Call to set the simulators internal cache of data for a given
    // TEC to the passed values. These are typically values reported
    // in the RFC packet.
    m_tecParams[whichUnit].currValues = newReadings;
}


bool TMPLSSimObject::GetADParams( eMPLSUnit whichUnit, int iADNbr, AD_PARAMS& adParams )
{
    // Returns the params for a given A/D input. iADNbr is zero-based
    if( ( whichUnit == eMU_SlaveMPLS ) && !m_bHasSlave )
    {
        adParams.iBiasEnum = 0;
        adParams.iAvging   = 0;

        return false;
    }

    if( ( iADNbr >= 0 ) && ( iADNbr < NBR_AD_INPUTS_PER_TEC ) )
    {
        adParams = m_tecParams[whichUnit].adParam[iADNbr];
        return true;
    }

    return false;
}


void TMPLSSimObject::GetDetParams( int& iRateEnum, int& iAvging )
{
    // Returns the params for the plug detector
    iRateEnum = m_detParams.iRate;
    iAvging   = m_detParams.iAvging;
}


void TMPLSSimObject::SetHasSlave( bool bHasSlave )
{
     // For now, just note that we have (or don't have) a slave
     m_bHasSlave = bHasSlave;

     if( m_onTECChanged )
         m_onTECChanged( this, eMU_SlaveMPLS );
}


bool TMPLSSimObject::GetIsolOutStatus( eMPLSUnit whichUnit, int iIsolOut )
{
    if( ( whichUnit == eMU_SlaveMPLS ) && !m_bHasSlave )
        return false;

    if( ( iIsolOut >= 0 ) && ( iIsolOut < NBR_ISOL_OUTS_PER_TEC ) )
        return m_tecParams[whichUnit].bIsolOutState[iIsolOut];
    else
        return false;
}


int TMPLSSimObject::GetActiveSol( eMPLSUnit whichUnit )
{
    if( ( whichUnit == eMU_SlaveMPLS ) && !m_bHasSlave )
        return false;

    return m_tecParams[whichUnit].activeSol;
}


void TMPLSSimObject::UpdateSolenoids( void )
{
    // Check if a solenoid has timed out
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        if( m_tecParams[iUnit].activeSol != 0 )
        {
            if( HaveTimeout( m_tecParams[iUnit].dwSolOnTime, m_tecParams[iUnit].dwSolDuration ) )
            {
                m_tecParams[iUnit].activeSol = 0;

                if( m_onTECChanged )
                    m_onTECChanged( this, (eMPLSUnit)iUnit );
            }
        }
    }
}


bool TMPLSSimObject::GetConfigData( eMPLSUnit whichUnit, BYTE whichPage, BYTE* pPageData, int cbMaxBytes )
{
    // Validate params first
    if( ( whichUnit < 0 ) || ( whichUnit >= eMU_NbrMPLSUnits ) )
        return false;

    if( whichPage >= NBR_WTTTS_CFG_PAGES )
        return false;

    if( pPageData == NULL )
        return false;

    // Maxbytes <= 0 is would be weird, but technically legal
    if( cbMaxBytes <= 0 )
        return true;

    // Copy at most one page of data
    int bytesToCopy;

    if( cbMaxBytes <= NBR_BYTES_PER_WTTTS_PAGE )
        bytesToCopy = cbMaxBytes;
    else
        bytesToCopy = NBR_BYTES_PER_WTTTS_PAGE;

    memcpy( pPageData, &( m_tecParams[whichUnit].cfgData[whichPage * NBR_BYTES_PER_WTTTS_PAGE] ), bytesToCopy );

    return true;
}


bool TMPLSSimObject::SetConfigData( eMPLSUnit whichUnit, BYTE whichPage, BYTE* pPageData, int cbBytes )
{
    // Validate params first
    if( ( whichUnit < 0 ) || ( whichUnit >= eMU_NbrMPLSUnits ) )
        return false;

    if( whichPage >= NBR_WTTTS_CFG_PAGES )
        return false;

    if( pPageData == NULL )
        return false;

    // cbBytes <= 0 is would be weird, but technically legal
    if( cbBytes <= 0 )
        return true;

    // Copy at most one page of data
    int bytesToCopy;

    if( cbBytes <= NBR_BYTES_PER_WTTTS_PAGE )
        bytesToCopy = cbBytes;
    else
        bytesToCopy = NBR_BYTES_PER_WTTTS_PAGE;

    memcpy( &( m_tecParams[whichUnit].cfgData[whichPage * NBR_BYTES_PER_WTTTS_PAGE] ), pPageData, bytesToCopy );

    return true;
}


void TMPLSSimObject::PostMessageToLog( const String& sMsg )
{
    if( m_onWriteMsgToLog )
        m_onWriteMsgToLog( this, sMsg );
}


bool TMPLSSimObject::SendACK( BYTE pktType, BYTE ackResp )
{
    MPLS2_ACK_PKT ackPkt = { 0 };

    ackPkt.byCmdBeingAckd = pktType;
    ackPkt.byCmdResult    = ackResp;

    bool bAckSent = SendMPLEvent( WTTTS_RESP_MPLS2_ACK_PKT, sizeof( MPLS2_ACK_PKT ), &ackPkt );

    String sAckDetails = "ack'd command 0x" + IntToHex( (int)pktType, 2 ) + ", result " + UIntToStr( (UINT)ackResp );

    if( bAckSent )
        PostMessageToLog( "Ack sent: " + sAckDetails );
    else
        PostMessageToLog( "Ack send failed: " + sAckDetails );

    return bAckSent;
}


bool TMPLSSimObject::SendMPLEvent( BYTE pktType, WORD payloadLen, const void* pPayload )
{
    if( m_onSendData == NULL )
        return false;

    BYTE txBuffer[MAX_MPLS2_PKT_LEN];

    WTTTS_PKT_HDR* pPktHdr = (WTTTS_PKT_HDR*)txBuffer;

    pPktHdr->pktHdr    = MPLS2_HDR_RX;
    pPktHdr->pktType   = pktType;
    pPktHdr->seqNbr    = m_seqNbr++;
    pPktHdr->timeStamp = (WORD)GetTickCount();
    pPktHdr->dataLen   = payloadLen;

    BYTE* pData = (BYTE*) &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

    if( payloadLen > 0 )
        memcpy( pData, pPayload, payloadLen );

    int packetHdrAndDataLen = sizeof( WTTTS_PKT_HDR ) + payloadLen;

    txBuffer[ packetHdrAndDataLen ] = CalcWTTTSChecksum( txBuffer, packetHdrAndDataLen );

    DWORD buffLen = packetHdrAndDataLen + 1;

    return m_onSendData( this, txBuffer, buffLen );
}


bool TMPLSSimObject::SendRequestForCmd( bool sendNow )
{
    // Send a RFC command now (if param is true) or if the RFC timer has expired
    bool bRFCSent = false;

    if( sendNow || ( GetTickCount() >= m_lastRFCTime + m_tecRates.rfcRate ) )
    {
        // Get required data
        if( m_onGetTECData )
        {
            m_onGetTECData( this, eMU_MasterMPLS );
            m_onGetTECData( this, eMU_SlaveMPLS );
        }

        if( m_onGetDetectorData )
        {
            // Fire the event, then validate any updated data
            m_detectorData.nbrReadings = m_onGetDetectorData( this, m_detectorData.wReadings, MAX_PLUG_DET_READINGS, m_detectorData.haveCommError );

            if( m_detectorData.nbrReadings > MAX_PLUG_DET_READINGS )
                m_detectorData.nbrReadings = MAX_PLUG_DET_READINGS;
        }

        // Now build the packet. Null out any slave specific fields if there
        // is no slave (or slave is reporting an error)
        MPLS2_RFC_PKT rfcPkt = { 0 };

        rfcPkt.rpm              = IntToByte( m_tecParams[eMU_MasterMPLS].currValues.iRPM );
        rfcPkt.temperature      = IntToByte( m_tecParams[eMU_MasterMPLS].currValues.iTemp * 2 /*each step = 1/2 deg C*/ );
        rfcPkt.masterBattVolts  = IntToWORD( m_tecParams[eMU_MasterMPLS].currValues.iBattVolts );
        rfcPkt.masterSolCurrent = IntToByte( m_tecParams[eMU_MasterMPLS].currValues.iSolCurr );

        try
        {
            MPLS_CAL_DATA_STRUCT_V2* pCalData = (MPLS_CAL_DATA_STRUCT_V2*)&( m_tecParams[eMU_MasterMPLS].cfgData[FIRST_RDWR_CFG_PAGE * NBR_BYTES_PER_WTTTS_PAGE] );

            ConvertToTriByte( m_tecParams[eMU_MasterMPLS].currValues.iTankPress, rfcPkt.masterTankPress, pCalData->tankPressOffset, pCalData->tankPressSpan );
            ConvertToTriByte( m_tecParams[eMU_MasterMPLS].currValues.iRegPress,  rfcPkt.masterRegPress,  pCalData->regPressOffset,  pCalData->regPressSpan  );
        }
        catch( ... )
        {
            IntToTriByte( 0, rfcPkt.masterTankPress );
            IntToTriByte( 0, rfcPkt.masterRegPress );
        }

        if( m_tecParams[eMU_SlaveMPLS].currValues.bReportTECError || !m_bHasSlave )
        {
            rfcPkt.slaveBattVolts  = 0xFFFF;
            rfcPkt.slaveSolCurrent = 0xFF;

            IntToTriByte( -1, rfcPkt.slaveTankPress );
            IntToTriByte( -1, rfcPkt.slaveRegPress );
        }
        else
        {
            rfcPkt.slaveBattVolts  = IntToWORD( m_tecParams[eMU_SlaveMPLS].currValues.iBattVolts );
            rfcPkt.slaveSolCurrent = IntToByte( m_tecParams[eMU_SlaveMPLS].currValues.iSolCurr );

            try
            {
                MPLS_CAL_DATA_STRUCT_V2* pCalData = (MPLS_CAL_DATA_STRUCT_V2*)&( m_tecParams[eMU_SlaveMPLS].cfgData[FIRST_RDWR_CFG_PAGE * NBR_BYTES_PER_WTTTS_PAGE] );;

                ConvertToTriByte( m_tecParams[eMU_SlaveMPLS].currValues.iTankPress, rfcPkt.slaveTankPress, pCalData->tankPressOffset, pCalData->tankPressSpan );
                ConvertToTriByte( m_tecParams[eMU_SlaveMPLS].currValues.iRegPress,  rfcPkt.slaveRegPress,  pCalData->regPressOffset,  pCalData->regPressSpan  );
            }
            catch( ... )
            {
                IntToTriByte( 0, rfcPkt.slaveTankPress );
                IntToTriByte( 0, rfcPkt.slaveRegPress );
            }
        }

        // Get plug detector data - unused slots are filled with 0xFFFF.
        memset( rfcPkt.plugData, 0xFFFF, MAX_PLUG_DET_READINGS * sizeof( WORD ) );
        memcpy( rfcPkt.plugData, m_detectorData.wReadings, m_detectorData.nbrReadings * sizeof( WORD ) );

        // Build status fields now
        rfcPkt.solStatus     = 0;  // TODO - confirm on spec what the sol status should report on comm error!
        rfcPkt.isolOutStatus = 0;

        if( !m_tecParams[eMU_MasterMPLS].currValues.bReportTECError )
        {
            rfcPkt.solStatus = IntToByte( m_tecParams[eMU_MasterMPLS].activeSol );

            rfcPkt.isolOutStatus |= m_tecParams[eMU_MasterMPLS].bIsolOutState[0] ? 0x01 : 00;
            rfcPkt.isolOutStatus |= m_tecParams[eMU_MasterMPLS].bIsolOutState[1] ? 0x02 : 00;
            rfcPkt.isolOutStatus |= m_tecParams[eMU_MasterMPLS].bIsolOutState[2] ? 0x04 : 00;
            rfcPkt.isolOutStatus |= m_tecParams[eMU_MasterMPLS].bIsolOutState[3] ? 0x08 : 00;
        }

        if( !m_tecParams[eMU_SlaveMPLS].currValues.bReportTECError || !m_bHasSlave )
        {
            rfcPkt.solStatus |= ( IntToByte( m_tecParams[eMU_SlaveMPLS].activeSol ) << 4 );

            rfcPkt.isolOutStatus |= m_tecParams[eMU_SlaveMPLS].bIsolOutState[0] ? 0x10 : 00;
            rfcPkt.isolOutStatus |= m_tecParams[eMU_SlaveMPLS].bIsolOutState[1] ? 0x20 : 00;
            rfcPkt.isolOutStatus |= m_tecParams[eMU_SlaveMPLS].bIsolOutState[2] ? 0x40 : 00;
            rfcPkt.isolOutStatus |= m_tecParams[eMU_SlaveMPLS].bIsolOutState[3] ? 0x80 : 00;
        }

        // Lastly build system status word
        rfcPkt.sysStatusBits = m_tecParams[eMU_MasterMPLS].currValues.iResetReason & 0x0003;

        // For now, report both battery types. Could report an 'unknown' enum for slave if not communicating
        rfcPkt.sysStatusBits |= ( ( m_tecParams[eMU_MasterMPLS].currValues.iBattType << 2 ) & 0x000C );
        rfcPkt.sysStatusBits |= ( ( m_tecParams[eMU_SlaveMPLS ].currValues.iBattType << 4 ) & 0x0030 );

        rfcPkt.sysStatusBits |= m_tecParams[eMU_SlaveMPLS ].currValues.bReportTECError ? 0x0400 : 0x0000;
        rfcPkt.sysStatusBits |= m_tecParams[eMU_SlaveMPLS ].currValues.bReportPIBError ? 0x0800 : 0x0000;
        rfcPkt.sysStatusBits |= m_tecParams[eMU_MasterMPLS].currValues.bReportPIBError ? 0x1000 : 0x0000;
        rfcPkt.sysStatusBits |= m_detectorData.haveCommError                           ? 0x2000 : 0x0000;
        rfcPkt.sysStatusBits |= m_tecParams[eMU_MasterMPLS].currValues.bFlagTripped    ? 0x4000 : 0x0000;

        // If we don't have a slave, force errors on its bits
        if( !m_bHasSlave )
        {
            rfcPkt.sysStatusBits |= 0x0400;
            rfcPkt.sysStatusBits |= 0x0800;
        }

        // Send the packet now
        bRFCSent = SendMPLEvent( WTTTS_RESP_MPLS2_RFC, sizeof( MPLS2_RFC_PKT ), &rfcPkt );

        if( bRFCSent )
            PostMessageToLog( "RFC sent" );
        else
            PostMessageToLog( "RFC send failed" );

        m_lastRFCTime = GetTickCount();
    }

    return bRFCSent;
}


void TMPLSSimObject::GetCfgData( const WTTTS_CFG_ITEM& cfgReq )
{
    // Send our cached data. Client will update this as required
    // when they update the visuals
    eMPLSUnit eUnit;
    String    sUnit;

    if( ( cfgReq.pageNbr & 0x80 ) == 0 )
    {
        eUnit = eMU_MasterMPLS;
        sUnit = "Master TEC";
    }
    else
    {
        eUnit = eMU_SlaveMPLS;
        sUnit = "Slave TEC";
    }

    // Build the response. First get the page number. This must range from 0 to 31
    WTTTS_CFG_DATA cfgData = { 0 };
    String sResultMsg = "Get Cfg Data command received, ";

    int iPage = cfgReq.pageNbr & 0x7F;

    if( iPage <= 31 )
    {
        cfgData.pageNbr = cfgReq.pageNbr;
        cfgData.result  = WTTTS_PG_RESULT_SUCCESS;

        memcpy( cfgData.pageData, &(m_tecParams[eUnit].cfgData[ iPage * NBR_BYTES_PER_WTTTS_PAGE ]), NBR_BYTES_PER_WTTTS_PAGE );

        sResultMsg = sResultMsg + "sending data for " + sUnit + " page " + IntToStr( iPage );
    }
    else
    {
        // Invalid page number
        cfgData.pageNbr = cfgReq.pageNbr;
        cfgData.result  = WTTTS_PG_RESULT_BAD_PG;

        memset( cfgData.pageData, 0xFF, NBR_BYTES_PER_WTTTS_PAGE );

        sResultMsg = sResultMsg + "invalid page number for " + sUnit + " page " + IntToStr( iPage );
    }

    if( SendMPLEvent( WTTTS_RESP_MPLS2_CFG_DATA, sizeof( WTTTS_CFG_DATA ), &cfgData ) )
        sResultMsg = sResultMsg + ", response sent OK";
    else
        sResultMsg = sResultMsg + ", response send failed";

    PostMessageToLog( sResultMsg );
}


void TMPLSSimObject::SetCfgData( const WTTTS_CFG_DATA& cfgData )
{
    // If the config packet looks legit, then the response is an echo
    // of the data.
    eMPLSUnit eUnit;
    String    sUnit;

    if( ( cfgData.pageNbr & 0x80 ) == 0 )
    {
        eUnit = eMU_MasterMPLS;
        sUnit = "Master TEC";
    }
    else
    {
        eUnit = eMU_SlaveMPLS;
        sUnit = "Slave TEC";
    }

    // Get the page number. This must range from 0 to 31
    String sResultMsg = "Set Cfg Data command received, ";

    int iPage = cfgData.pageNbr & 0x7F;

    // The firmware will disallow the writing of data to read-only pages.
    // Do the same here
    if( ( iPage >= FIRST_RDWR_CFG_PAGE ) && ( iPage < NBR_WTTTS_CFG_PAGES ) )
    {
        // Copy data to cache
        memcpy( &(m_tecParams[eUnit].cfgData[ iPage * NBR_BYTES_PER_WTTTS_PAGE ]), cfgData.pageData, NBR_BYTES_PER_WTTTS_PAGE );

        PostMessageToLog( "Set Cfg Data for " + sUnit + " page " + IntToStr( cfgData.pageNbr ) + " successful" );

        // Make a call to GetCfgData() to echo the result
        WTTTS_CFG_ITEM cfgReq = { 0 };

        cfgReq.pageNbr = cfgData.pageNbr;

        GetCfgData( cfgReq );
    }
    else
    {
        // Set data failed!
        WTTTS_CFG_DATA failedData;

        failedData.pageNbr = cfgData.pageNbr;
        failedData.result  = WTTTS_PG_RESULT_BAD_PG;

        memset( failedData.pageData, 0xFF, NBR_BYTES_PER_WTTTS_PAGE );

        SendMPLEvent( WTTTS_RESP_MPLS2_CFG_DATA, sizeof( WTTTS_CFG_DATA ), &failedData );

        PostMessageToLog( "Set Cfg Data for " + sUnit + " page " + IntToStr( cfgData.pageNbr ) + " failed" );
    }
}


void TMPLSSimObject::SetRates( const MPLS2_RATES_PKT& ratePkt )
{
    m_tecRates = ratePkt;

    PostMessageToLog( "Set Rates Command received" );

    SendACK( WTTTS_CMD_SET_RATE, ACK_PKT_SUCCESS );

    if( m_onTECChanged )
        m_onTECChanged( this, eMU_MasterMPLS );
}


void TMPLSSimObject::SendVersionPkt( const MPLS2_QUERY_VER_PKT& queryVerPkt )
{
    // Give the client a chance to update our cached values
    if( m_onGetDevIDs )
        m_onGetDevIDs( this );

    MPLS2_VERSION_PKT verPkt = { 0 };

    // Only report dev IDs based on our master / slave config
    if( queryVerPkt.byUnitID == 1 )
    {
        verPkt.byUnitID = 1;

        StringToSNArray( m_didMasterTEC.sSerialNbr, verPkt.tecSN );

        verPkt.tecHWRev = m_didMasterTEC.dwHWRev;
        verPkt.tecFWRev = m_didMasterTEC.dwFWRev;

        verPkt.pibHWRev = m_didMasterPIB.dwHWRev;
        verPkt.pibFWRev = m_didMasterPIB.dwFWRev;

        StringToSNArray( m_didPlugDet.sSerialNbr, verPkt.plugDetSN );

        verPkt.plugDetFWRev = m_didPlugDet.dwFWRev;
        verPkt.plugDetHWRev = m_didPlugDet.dwHWRev;

        // Send it
        if( SendMPLEvent( WTTTS_RESP_MPLS2_VER_PKT, sizeof( MPLS2_VERSION_PKT ), &verPkt ) )
            PostMessageToLog( "Version packet requested and sent" );
        else
            PostMessageToLog( "Version packet requested, send failed" );
    }
    else if( queryVerPkt.byUnitID == 2 )
    {
        verPkt.byUnitID = 2;

        if( m_bHasSlave )
        {
            StringToSNArray( m_didSlaveTEC.sSerialNbr, verPkt.tecSN );

            verPkt.tecHWRev = m_didSlaveTEC.dwHWRev;
            verPkt.tecFWRev = m_didSlaveTEC.dwFWRev;

            verPkt.pibHWRev = m_didSlavePIB.dwHWRev;
            verPkt.pibFWRev = m_didSlavePIB.dwFWRev;
        }
        else
        {
            String sEmptySN;

            StringToSNArray( sEmptySN, verPkt.tecSN );

            verPkt.tecHWRev = 0;
            verPkt.tecFWRev = 0;

            verPkt.pibHWRev = 0;
            verPkt.pibFWRev = 0;
        }

        StringToSNArray( m_didPlugDet.sSerialNbr, verPkt.plugDetSN );

        verPkt.plugDetFWRev = m_didPlugDet.dwFWRev;
        verPkt.plugDetHWRev = m_didPlugDet.dwHWRev;

        // Send it
        if( SendMPLEvent( WTTTS_RESP_MPLS2_VER_PKT, sizeof( MPLS2_VERSION_PKT ), &verPkt ) )
            PostMessageToLog( "Version packet requested and sent" );
        else
            PostMessageToLog( "Version packet requested, send failed" );
    }
    else
    {
        SendACK( WTTTS_CMD_MPLS2_QUERY_VER, ACK_PKT_PARAM_ERROR );

        PostMessageToLog( "Version packet requested, send failed" );
    }
}


void TMPLSSimObject::SetRFChan( const WTTTS_SET_CHAN_PKT& chanPkt )
{
/* move to sim object
    m_pLogger->AddLogMessage( "Set RF Chan cmd received, new chan = " + IntToStr( chanPkt.chanNbr ) );
*/
}


void TMPLSSimObject::SetRFPower( const WTTTS_SET_RF_PWR_PKT& pwrPkt )
{
/* move to sim object
    m_pLogger->AddLogMessage( "Set RF Pwr cmd received, new power = " + IntToStr( pwrPkt.powerLevel ) );
*/
}


void TMPLSSimObject::MarkChanInUse( void )
{
/* move to sim object
    LoggingForm->AddLogMessage( "Mark Channel in Use cmd received - ignoring" );
*/
}


void TMPLSSimObject::SetSolenoids( const MPLS2_PIB_OUTS_PKT& solPkt )
{
    eMPLSUnit eUnit;
    String    sUnit;

    if( solPkt.targetPIB == 1 )
    {
        eUnit = eMU_MasterMPLS;
        sUnit = "Master PIB";
    }
    else if( solPkt.targetPIB == 2 )
    {
        eUnit = eMU_SlaveMPLS;
        sUnit = "Slave PIB";
    }
    else
    {
        SendACK( WTTTS_CMD_MPLS2_SET_PIB_OUT, ACK_PKT_PARAM_ERROR );

        PostMessageToLog( "Set PIB Outs command received, bad target PIB " + UIntToStr( (UINT)solPkt.targetPIB ) );

        return;
    }

    // Fail on command for slave if we have no slave
    if( ( eUnit == eMU_SlaveMPLS ) && !m_bHasSlave )
    {
        SendACK( WTTTS_CMD_MPLS2_SET_PIB_OUT, ACK_PKT_PARAM_ERROR );

        PostMessageToLog( "Set PIB Outs command received for Slave PIB, but no slave in system" );

        return;
    }

    // Fail on solenoid activation with zero time
    if( ( solPkt.activeSol >= 1 ) && ( solPkt.activeSol <= 5 ) && ( solPkt.solTimeout == 0 ) )
    {
        SendACK( WTTTS_CMD_MPLS2_SET_PIB_OUT, ACK_PKT_PARAM_ERROR );

        PostMessageToLog( "Set PIB Outs command received, bad solenoid timeout of zero" );

        return;
    }

    // Solenoid number is zero to turn off all solenoids, 1 to 5 to turn on
    // a solenoid, or 0xF to not change solenoids (just changing isol outs)
    if( solPkt.activeSol <= 5 )
    {
        // Update solenoid and isol outs in this case
        m_tecParams[eUnit].activeSol = solPkt.activeSol;

        // Note the time a relay is turned on (if turning on)
        if( solPkt.activeSol != 0 )
        {
            m_tecParams[eUnit].dwSolOnTime   = GetTickCount();
            m_tecParams[eUnit].dwSolDuration = solPkt.solTimeout * 1000;
        }

        // Update isoldated outs
        m_tecParams[eUnit].bIsolOutState[0] = ( solPkt.isolOuts & 0x01 ) ? true : false;
        m_tecParams[eUnit].bIsolOutState[1] = ( solPkt.isolOuts & 0x02 ) ? true : false;
        m_tecParams[eUnit].bIsolOutState[2] = ( solPkt.isolOuts & 0x04 ) ? true : false;
        m_tecParams[eUnit].bIsolOutState[3] = ( solPkt.isolOuts & 0x08 ) ? true : false;

        SendACK( WTTTS_CMD_MPLS2_SET_PIB_OUT, ACK_PKT_SUCCESS );

        PostMessageToLog( "Set PIB Outs command received, updated solenoid and isol outs for " + sUnit );
    }
    else if( solPkt.activeSol == 0x0F )
    {
        // Update isoldated outs only here
        m_tecParams[eUnit].bIsolOutState[0] = ( solPkt.isolOuts & 0x01 ) ? true : false;
        m_tecParams[eUnit].bIsolOutState[1] = ( solPkt.isolOuts & 0x02 ) ? true : false;
        m_tecParams[eUnit].bIsolOutState[2] = ( solPkt.isolOuts & 0x04 ) ? true : false;
        m_tecParams[eUnit].bIsolOutState[3] = ( solPkt.isolOuts & 0x08 ) ? true : false;

        SendACK( WTTTS_CMD_MPLS2_SET_PIB_OUT, ACK_PKT_SUCCESS );

        PostMessageToLog( "Set PIB Outs command received, updated isol outs for " + sUnit );
    }
    else
    {
        SendACK( WTTTS_CMD_MPLS2_SET_PIB_OUT, ACK_PKT_PARAM_ERROR );

        PostMessageToLog( "Set PIB Outs command received, bad sol number " + UIntToStr( (UINT)solPkt.activeSol ) );
    }

    // Let our clients know things have changed
    if( m_onTECChanged )
        m_onTECChanged( this, eUnit );
}


void TMPLSSimObject::SetTECParams( const MPLS2_TEC_PARAMS_PKT& tecParamsPkt )
{
    eMPLSUnit eUnit;
    String    sUnit;

    if( tecParamsPkt.devAddr & 0x80 )
    {
        eUnit = eMU_MasterMPLS;
        sUnit = "Master PIB";
    }
    else if( tecParamsPkt.devAddr & 0x40 )
    {
        eUnit = eMU_SlaveMPLS;
        sUnit = "Slave PIB";
    }
    else
    {
        SendACK( WTTTS_CMD_MPLS2_SET_TEC_PAR, ACK_PKT_PARAM_ERROR );

        PostMessageToLog( "Set TEC Params command received, bad target TEC 0x" + IntToHex( (int)tecParamsPkt.devAddr, 2 ) );

        return;
    }

    // Fail on command for slave if we have no slave
    if( ( eUnit == eMU_SlaveMPLS ) && !m_bHasSlave )
    {
        SendACK( WTTTS_CMD_MPLS2_SET_TEC_PAR, ACK_PKT_PARAM_ERROR );

        PostMessageToLog( "Set TEC Params command received for Slave TEC, but no slave in system" );

        return;
    }

    // Save params and advise clients of change
    int iADNbr = tecParamsPkt.devAddr & 0x2F;

    if( iADNbr == 0 )
    {
        // Update all ADs on target
        for( int iAD = 0; iAD < NBR_AD_INPUTS_PER_TEC; iAD++ )
        {
            m_tecParams[eUnit].adParam[iAD].iBiasEnum = tecParamsPkt.setting;
            m_tecParams[eUnit].adParam[iAD].iAvging   = tecParamsPkt.readsPerRFC;
        }

        SendACK( WTTTS_CMD_MPLS2_SET_TEC_PAR, ACK_PKT_SUCCESS );

        PostMessageToLog( "Set TEC Params command received for " + sUnit + ", all ADs updated" );
    }
    else if( iADNbr <= 6 )
    {
        // Update specific AD on target. AD number is 1-based
        m_tecParams[eUnit].adParam[iADNbr-1].iBiasEnum = tecParamsPkt.setting;
        m_tecParams[eUnit].adParam[iADNbr-1].iAvging   = tecParamsPkt.readsPerRFC;

        SendACK( WTTTS_CMD_MPLS2_SET_TEC_PAR, ACK_PKT_SUCCESS );

        PostMessageToLog( "Set TEC Params command received for " + sUnit + ", AD " + IntToStr( iADNbr ) + " updated" );
    }
    else
    {
        // Invalid target AD
        SendACK( WTTTS_CMD_MPLS2_SET_TEC_PAR, ACK_PKT_PARAM_ERROR );

        PostMessageToLog( "Set PIB Outs command received, bad Dev Addr field 0x" + IntToHex( tecParamsPkt.devAddr, 2 ) );
    }

    // Let our clients know things have changed
    if( m_onTECChanged )
        m_onTECChanged( this, eUnit );
}


void TMPLSSimObject::SetDetParams( const MPLS2_DET_PARAMS_PKT& detParamsPkt )
{
    m_detParams.iRate   = detParamsPkt.sampleRate;
    m_detParams.iAvging = detParamsPkt.sampleAvging;

    SendACK( WTTTS_CMD_MPLS2_SET_DET_PAR, ACK_PKT_SUCCESS );

    PostMessageToLog( "Set Plug Det Params command received" );

    // Let our clients know things have changed
    if( m_onDetectorChanged )
        m_onDetectorChanged( this );
}

