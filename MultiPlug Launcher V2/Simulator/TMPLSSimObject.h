#ifndef TMPLSSimObjectH
#define TMPLSSimObjectH

#include "MPLSDefs.h"


typedef void __fastcall (__closure *TTECEvent       )( TObject* pSender, eMPLSUnit eUnit );
typedef void __fastcall (__closure *TLogMsgEvent    )( TObject* pSender, const String& sLogMsg );
typedef WORD __fastcall (__closure *TGetDetDataEvent)( TObject* pSender, WORD* pReadings, WORD maxNbrReadings, bool& bHaveError );
typedef bool __fastcall (__closure *TSendDataEvent  )( TObject* pSender, const BYTE* pData, int nbrBytes );


#define NBR_AD_INPUTS_PER_TEC    6


class TMPLSSimObject : public TObject
{
  public:

    typedef struct {
        String sSerialNbr;
        DWORD  dwFWRev;
        DWORD  dwHWRev;
    } DEVICE_ID;

    typedef struct {
        int iBiasEnum;
        int iAvging;
    } AD_PARAMS;

    typedef struct {
        int  iRPM;
        int  iBattVolts;
        int  iSolCurr;
        int  iTemp;
        int  iTankPress;
        int  iRegPress;
        int  iResetReason;
        int  iBattType;
        bool bReportTECError;
        bool bReportPIBError;
        bool bFlagTripped;
    } TEC_READINGS;


  private:

    // Overall simulation control - main window takes care of sending RFCs
    DWORD     m_lastRFCTime;
    BYTE      m_seqNbr;

    // Device IDs of all possible attached devices
    DEVICE_ID m_didMasterTEC;
    DEVICE_ID m_didMasterPIB;
    DEVICE_ID m_didSlaveTEC;
    DEVICE_ID m_didSlavePIB;
    DEVICE_ID m_didPlugDet;

    struct {
        bool         bIsolOutState[NBR_ISOL_OUTS_PER_TEC];   // Current state of isolated outputs
        int          activeSol;                              // The solenoid the is currently active, 0 = no solenoid
        DWORD        dwSolOnTime;                            // Time a solenoid was commanded on
        DWORD        dwSolDuration;                          // Duration in msecs sol to stay on
        AD_PARAMS    adParam[NBR_AD_INPUTS_PER_TEC];
        TEC_READINGS currValues;
        BYTE         cfgData[NBR_WTTTS_CFG_PAGES * NBR_BYTES_PER_WTTTS_PAGE];
    } m_tecParams[eMU_NbrMPLSUnits];

    struct {
        WORD wReadings[MAX_PLUG_DET_READINGS];
        int  nbrReadings;
        bool haveCommError;
    } m_detectorData;

    struct {
        int iRate;
        int iAvging;
    } m_detParams;

    MPLS2_RATES_PKT m_tecRates;

    bool m_bHasSlave;

    void SetHasSlave( bool bHasSlave );

    bool GetIsolOutStatus( eMPLSUnit whichUnit, int iIsolOut );
    int  GetActiveSol( eMPLSUnit whichUnit );

    int  GetRFCRate    ( void )  { return m_tecRates.rfcRate; }
    int  GetRFCTimeout ( void )  { return m_tecRates.rfcTimeout; }
    int  GetPairTimeout( void )  { return m_tecRates.pairTimeout; }

    void UpdateSolenoids( void );

    void PostMessageToLog( const String& sMsg );

    TLogMsgEvent     m_onWriteMsgToLog;
    TTECEvent        m_onTECChanged;
    TTECEvent        m_onGetTECData;
    TNotifyEvent     m_onDetectorChanged;
    TGetDetDataEvent m_onGetDetectorData;
    TNotifyEvent     m_onGetDevIDs;
    TSendDataEvent   m_onSendData;

    // Command handlers
    void GetCfgData    ( const WTTTS_CFG_ITEM& cfgReq );
    void SetCfgData    ( const WTTTS_CFG_DATA& cfgData );
    void SetRates      ( const MPLS2_RATES_PKT& ratePkt );
    void SendVersionPkt( const MPLS2_QUERY_VER_PKT& queryVerPkt );
    void SetRFChan     ( const WTTTS_SET_CHAN_PKT& chanPkt );
    void SetRFPower    ( const WTTTS_SET_RF_PWR_PKT& pwrPkt );
    void MarkChanInUse ( void );
    void SetSolenoids  ( const MPLS2_PIB_OUTS_PKT& solPkt );
    void SetTECParams  ( const MPLS2_TEC_PARAMS_PKT& tecParamsPkt );
    void SetDetParams  ( const MPLS2_DET_PARAMS_PKT& detParamsPkt );

    bool SendACK( BYTE pktType, BYTE ackResp );
    bool SendRequestForCmd( bool bSendNow );
    bool SendMPLEvent( BYTE pktType, WORD payloadLen, const void* pPayload );

  protected:
  public:

    virtual __fastcall TMPLSSimObject();
    virtual __fastcall ~TMPLSSimObject();

    void ResetToDefaults( void );
        // Resets the object to default values.

    typedef enum {
       eRT_Temp,
       eRT_RPM,
       eRT_VBat,
       eRT_ISol,
       eRT_PTank,
       eRT_PReg,
       eRT_NbrReadingTypes
    } eReadingType;
        // Enum that defines the types of analog inputs each TEC
        // board supports

    bool ProcessReceivedData( BYTE* pRxBuffer, DWORD& dwBuffLen );
        // Call with data in the buffer. Simulator object will process
        // and act on any packets. Returns true if one or more packets
        // were in the buffer, and in that case dwBuffLen will be
        // adjusted to show the number of unused bytes left in the buffer.
        //
        // This function acts as a 'heartbeat' for the object, and
        // therefore should be called frequently and at regular
        // intervals.

    void SetTECReadings( eMPLSUnit whichUnit, const TEC_READINGS& newReadings );
        // Call to set the simulators internal cache of data for a given
        // TEC to the passed values. These are typically values reported
        // in the RFC packet.

    bool GetADParams( eMPLSUnit whichUnit, int iADNbr, AD_PARAMS& adParams );
        // Returns the params for a given A/D input. iADNbr is zero-based

    void GetDetParams( int& iRateEnum, int& iAvging );
        // Returns the params for the plug detector

    void SetDeviceIDs( const DEVICE_ID& masterTEC, const DEVICE_ID& masterPIB,
                       const DEVICE_ID& slaveTEC,  const DEVICE_ID& slavePIB,
                       const DEVICE_ID& plugDet );
        // Sets the device IDs to report for each possible attached device

    void GetDeviceIDs( DEVICE_ID& masterTEC, DEVICE_ID& masterPIB,
                       DEVICE_ID& slaveTEC,  DEVICE_ID& slavePIB,
                       DEVICE_ID& plugDet );
        // Reports the device IDs to report for each possible attached device

    bool GetConfigData( eMPLSUnit whichUnit, BYTE whichPage, BYTE* pPageData, int cbMaxBytes );
    bool SetConfigData( eMPLSUnit whichUnit, BYTE whichPage, BYTE* pPageData, int cbBytes );
        // Handlers to get / set config data for a given TEC. Note that at most
        // one page worth of data can get retrieved / set with each call.

    __property bool HasSlave = { read = m_bHasSlave, write = SetHasSlave };
        // Set to true to indicate an MPLS has both a master and a
        // slave unit.

    __property bool IsolOutStatus[eMPLSUnit][int] = { read = GetIsolOutStatus };
    __property int  ActiveSolenoid[eMPLSUnit]     = { read = GetActiveSol };

    __property int  RFCRate     = { read = GetRFCRate };
    __property int  RFCTimeout  = { read = GetRFCTimeout };
    __property int  PairTimeout = { read = GetPairTimeout };

    __property TLogMsgEvent OnWriteMsgToLog = { read = m_onWriteMsgToLog, write = m_onWriteMsgToLog };
        // Event handler fired when simulator has a message to be written
        // to the system log

    __property TSendDataEvent OnSendData = { read = m_onSendData, write = m_onSendData };
        // Event fired when the MPLS object wants to send data

    __property TTECEvent    OnTECChanged      = { read = m_onTECChanged,      write = m_onTECChanged };
    __property TNotifyEvent OnDetectorChanged = { read = m_onDetectorChanged, write = m_onDetectorChanged };
        // Event handlers that fire when the host has updated configuration data
        // for a system module, indicating that the frame should update its display.
        // These events will fire during the call to ProcessReceivedData(). Event
        // also fired if no commands received but a device state has changed on its
        // own (eg, solenoid timing out ).

    __property TTECEvent OnGetTECData = { read = m_onGetTECData, write = m_onGetTECData };
        // Event handler fired when the MPLS object needs current data
        // for a unit. Caller must call the SetTECReadings() method
        // from within this event handler

    __property TGetDetDataEvent OnGetDetectorData = { read = m_onGetDetectorData, write = m_onGetDetectorData };
        // Simulator needs current plug detector data. Event handler must fill the
        // pass buffer with data.

    __property TNotifyEvent OnGetDeviceIDs = { read = m_onGetDevIDs, write = m_onGetDevIDs };
        // Simulator needs device IDs for all units. Call SetDeviceIDs() from within
        // this event handler

};

#endif
