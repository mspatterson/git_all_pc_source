#include <vcl.h>
#pragma hdrstop

#include "TTECFrame.h"
#include "TSliderFrame.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TTECFrame *TECFrame;


__fastcall TTECFrame::TTECFrame(TComponent* pOwner, TWinControl* pParent, const String& sCompName, bool bIsMasterFrame )
                    : TFrame(pOwner)
{
    // Set basic properties first
    Parent = pParent;
    Name   = sCompName;

    m_bIsMasterFrame = bIsMasterFrame;

    // Master TEC can't report comm errors
    if( bIsMasterFrame )
    {
        TECErrorCB->Checked = false;
        TECErrorCB->Enabled = false;
    }

    // Slave TEC doesn't report a flag
    if( !bIsMasterFrame )
    {
        FlagStatusCB->Checked = false;
        FlagStatusCB->Visible = false;
    }

    // Create slider frames
    m_pSliders[TMPLSSimObject::eRT_VBat]  = new TSliderFrame( this, this, "VBatSlider",  "VBat"  );
    m_pSliders[TMPLSSimObject::eRT_ISol]  = new TSliderFrame( this, this, "ISolSlider",  "ISol"  );
    m_pSliders[TMPLSSimObject::eRT_Temp]  = new TSliderFrame( this, this, "TempSlider",  "Temp"  );
    m_pSliders[TMPLSSimObject::eRT_RPM]   = new TSliderFrame( this, this, "RPMSlider",   "RPM"   );
    m_pSliders[TMPLSSimObject::eRT_PTank] = new TSliderFrame( this, this, "PTankSlider", "PTank" );
    m_pSliders[TMPLSSimObject::eRT_PReg]  = new TSliderFrame( this, this, "PRegSlider",  "PReg"  );

    // Set slider ranges and initial values
    m_pSliders[TMPLSSimObject::eRT_VBat ]->SetRange(   0, 5000 );   // mV
    m_pSliders[TMPLSSimObject::eRT_ISol ]->SetRange(   0, 255  );   // mA
    m_pSliders[TMPLSSimObject::eRT_Temp ]->SetRange( -63, 64   );   // deg C
    m_pSliders[TMPLSSimObject::eRT_RPM  ]->SetRange(   0, 255  );   // RPM
    m_pSliders[TMPLSSimObject::eRT_PTank]->SetRange(   0, 3000 );   // PSI
    m_pSliders[TMPLSSimObject::eRT_PReg ]->SetRange(   0, 300  );   // PSI

    // Set default dev IDs
    if( bIsMasterFrame )
    {
        DevIDEdit->Text = "1001";
        HWRevEdit->Text = "1002";
        FWRevEdit->Text = "1003";
    }
    else
    {
        DevIDEdit->Text = "3001";
        HWRevEdit->Text = "3002";
        FWRevEdit->Text = "3003";
    }
}


void TTECFrame::DoOnShow( void )
{
    // Postion sliders. All sliders have the same height
    // and are anchored to the top and bottom.
    int iSliderHeight = DevSettingsGB->Top - 8 /*margin*/ - 8 /*slider top*/;

    TAnchors sliderAnchor = TAnchors() << akLeft << akTop << akBottom;

    for( int iSlider = 0; iSlider < TMPLSSimObject::eRT_NbrReadingTypes; iSlider++ )
    {
        // First slider is at a slight X offset
        if( iSlider == 0 )
            m_pSliders[iSlider]->Left = 8;
        else
            m_pSliders[iSlider]->Left = 10 + iSlider * 50;

        // All sliders have the same Y pos
        m_pSliders[iSlider]->Top = 8;

        // All sliders have the same height
        m_pSliders[iSlider]->Height = iSliderHeight;

        // Anchor the slider
        m_pSliders[iSlider]->Anchors = sliderAnchor;
    }
}


void __fastcall TTECFrame::CfgDataBtnClick(TObject *Sender)
{
    // Event handler called when the "Configuration Data" button is pressed.
    // Call back the event handler - it is up to that handler to display
    // any dialogs or do any update work required.
    if( m_onShowConfigData )
        m_onShowConfigData( this );
}


// Get value from slider bar
int TTECFrame::GetReading( TMPLSSimObject::eReadingType eType )
{
    int iSliderVal = 0;

    switch( eType )
    {
        case TMPLSSimObject::eRT_VBat:
        case TMPLSSimObject::eRT_ISol:
        case TMPLSSimObject::eRT_Temp:
        case TMPLSSimObject::eRT_RPM:
        case TMPLSSimObject::eRT_PTank:
        case TMPLSSimObject::eRT_PReg:
            iSliderVal = m_pSliders[eType]->Value;
            break;
    }

    return iSliderVal;
}


// Set value for slider bar
void TTECFrame::SetReading( TMPLSSimObject::eReadingType eType, int iValue )
{
    switch( eType )
    {
        case TMPLSSimObject::eRT_VBat:
        case TMPLSSimObject::eRT_ISol:
        case TMPLSSimObject::eRT_Temp:
        case TMPLSSimObject::eRT_RPM:
        case TMPLSSimObject::eRT_PTank:
        case TMPLSSimObject::eRT_PReg:
            m_pSliders[eType]->Value = iValue;
            break;
    }
}


// Set Minimum and Maximum value for slider bar
bool TTECFrame::SetReadingRange( TMPLSSimObject::eReadingType eType, int iMinValue, int iMaxValue )
{
    switch( eType )
    {
        case TMPLSSimObject::eRT_VBat:
        case TMPLSSimObject::eRT_ISol:
        case TMPLSSimObject::eRT_Temp:
        case TMPLSSimObject::eRT_RPM:
        case TMPLSSimObject::eRT_PTank:
        case TMPLSSimObject::eRT_PReg:

            if( m_pSliders[eType]->SetRange( iMinValue, iMaxValue ) )
                return true;

            // Fall through means an error happened
            MessageDlg( "Max value must be greater or equal to Min value" , mtError, TMsgDlgButtons() << mbOK, 0 );

            break;
    }

    // Fall through means an error happened
    return false;
}


void TTECFrame::SetTimingParams( const TEC_TIMING& tecTiming )
{
    // Add timing params to status grid, but only if we're the master
    if( !m_bIsMasterFrame )
        return;

    UpdateStatusGrid( TECParamsVLE, "RFC Rate",     IntToStr( tecTiming.iRFCRate     ) );
    UpdateStatusGrid( TECParamsVLE, "RFC Timeout",  IntToStr( tecTiming.iRFCTimeout  ) );
    UpdateStatusGrid( TECParamsVLE, "Pair Timeout", IntToStr( tecTiming.iPairTimeout ) );
}


// Set on/off for Solenoids labels
void TTECFrame::SetSolenoidStatus( bool bSol1On, bool bSol2On, bool bSol3On, bool bSol4On, bool bSol5On )
{
    Solenoid1Label->Caption = GetOnOffString( "", 1, bSol1On );
    Solenoid2Label->Caption = GetOnOffString( "", 2, bSol2On );
    Solenoid3Label->Caption = GetOnOffString( "", 3, bSol3On );
    Solenoid4Label->Caption = GetOnOffString( "", 4, bSol4On );
    Solenoid5Label->Caption = GetOnOffString( "", 5, bSol5On );

    // Additional check: if solenoid 5 is on and this is the master, force
    // the 'flag reset' cb to be unchecked
    if( m_bIsMasterFrame )
    {
        if( bSol5On )
            FlagStatusCB->Checked = false;
    }
}


// Set on/off for contacts labels
void TTECFrame::SetDigIOStatus( bool bDig1On, bool bDig2On, bool bDig3On, bool bDig4On )
{
    Contact1Label->Caption = GetOnOffString( "", 1, bDig1On );
    Contact2Label->Caption = GetOnOffString( "", 2, bDig2On );
    Contact3Label->Caption = GetOnOffString( "", 3, bDig3On );
    Contact4Label->Caption = GetOnOffString( "", 4, bDig4On );
}


void TTECFrame::SetTECParams( const TEC_PARAMS tecParams )
{
    const String ADBiasTitle  ("AD %d Bias" );
    const String ADAvgingTitle("AD %d Avging" );

    // For now, the grid just shows the A/D params
    for( int iAD = 0; iAD < NBR_AD_INPUTS_PER_TEC; iAD++ )
    {
        String sADBiasTitle;
        sADBiasTitle.printf( ADBiasTitle.w_str(), iAD );

        String sADAvgTitle;
        sADAvgTitle.printf( ADAvgingTitle.w_str(), iAD );

        UpdateStatusGrid( TECParamsVLE, sADBiasTitle, IntToStr( tecParams.adParama[iAD].iBiasEnum ) );
        UpdateStatusGrid( TECParamsVLE, sADAvgTitle,  IntToStr( tecParams.adParama[iAD].iAvging   ) );
    }
}


void TTECFrame::GetDeviceIDs( TMPLSSimObject::DEVICE_ID& devID, TMPLSSimObject::DEVICE_ID& pibDevID )
{
    devID.sSerialNbr = Trim( DevIDEdit->Text );
    devID.dwHWRev    = HWRevEdit->Text.ToIntDef( 0 );
    devID.dwFWRev    = FWRevEdit->Text.ToIntDef( 0 );

    // For now...
    pibDevID.sSerialNbr = "";   // PIB's do not report a SN
    pibDevID.dwHWRev    = devID.dwHWRev + 1000;
    pibDevID.dwFWRev    = devID.dwFWRev + 1000;
}


void TTECFrame::SetDeviceIDs( const TMPLSSimObject::DEVICE_ID& devID, const TMPLSSimObject::DEVICE_ID& pibDevID )
{
    DevIDEdit->Text = devID.sSerialNbr;
    HWRevEdit->Text = UIntToStr( (UINT)devID.dwHWRev );
    FWRevEdit->Text = UIntToStr( (UINT)devID.dwFWRev );
}


String TTECFrame::GetOnOffString( const String& sPrefix, int itemNbr, bool bIsClosed )
{
    String sResult = sPrefix + IntToStr( itemNbr ) + ": ";

    if( bIsClosed )
        return sResult + "On";
    else
        return sResult + "Off";
}


void TTECFrame::GetTECData( TMPLSSimObject::TEC_READINGS& newReadings )
{
    // Returns the current TEC data
    newReadings.iBattVolts      = m_pSliders[TMPLSSimObject::eRT_VBat ]->Value;
    newReadings.iSolCurr        = m_pSliders[TMPLSSimObject::eRT_ISol ]->Value;
    newReadings.iTemp           = m_pSliders[TMPLSSimObject::eRT_Temp ]->Value;
    newReadings.iRPM            = m_pSliders[TMPLSSimObject::eRT_RPM  ]->Value;
    newReadings.iTankPress      = m_pSliders[TMPLSSimObject::eRT_PTank]->Value;
    newReadings.iRegPress       = m_pSliders[TMPLSSimObject::eRT_PReg ]->Value;
    newReadings.iResetReason    = ResetReasonEdit->Text.ToIntDef( 0 );
    newReadings.iBattType       = BattTypeEdit->Text.ToIntDef( 0 );
    newReadings.bReportTECError = m_bIsMasterFrame ? false : TECErrorCB->Checked;
    newReadings.bReportPIBError = PIBErrorCB->Checked;
    newReadings.bFlagTripped    = FlagStatusCB->Checked;
}


void TTECFrame::UpdateStatusGrid( TValueListEditor* aGrid, const String& key, const String& text )
{
    // Look for the key first. If not found, add it
    if( aGrid == NULL )
        return;

    bool bKeyExists = false;

    for( int iItem = 1; iItem < aGrid->RowCount; iItem++ )
    {
        if( aGrid->Keys[iItem] == key )
        {
            bKeyExists = true;
            break;
        }
    }

    if( bKeyExists )
        aGrid->Values[ key ] = text;
    else
        aGrid->InsertRow( key, text, true );
}


void TTECFrame::ClearStatusGrid( TValueListEditor* aGrid )
{
    // Clear all but the title row
    if( aGrid == NULL )
        return;

    for( int iRow = 1; iRow < aGrid->RowCount; iRow++ )
        aGrid->Values[ aGrid->Keys[iRow] ] = "";
}



