object TECFrame: TTECFrame
  Left = 0
  Top = 0
  Width = 580
  Height = 502
  TabOrder = 0
  DesignSize = (
    580
    502)
  object TECParamsVLE: TValueListEditor
    Left = 349
    Top = 4
    Width = 227
    Height = 493
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    TitleCaptions.Strings = (
      'Property'
      'Value')
    ColWidths = (
      97
      124)
  end
  object DevSettingsGB: TGroupBox
    Left = 5
    Top = 245
    Width = 338
    Height = 165
    Anchors = [akLeft, akBottom]
    Caption = ' Device Settings '
    TabOrder = 1
    object Label13: TLabel
      Left = 16
      Top = 93
      Width = 46
      Height = 13
      Caption = 'Serial Nbr'
    end
    object Label11: TLabel
      Left = 16
      Top = 30
      Width = 39
      Height = 13
      Caption = 'HW Rev'
    end
    object Label17: TLabel
      Left = 16
      Top = 62
      Width = 38
      Height = 13
      Caption = 'FW Rev'
    end
    object Label1: TLabel
      Left = 177
      Top = 30
      Width = 67
      Height = 13
      Caption = 'Reset Reason'
    end
    object Label2: TLabel
      Left = 177
      Top = 62
      Width = 47
      Height = 13
      Caption = 'Batt Type'
    end
    object CfgDataBtn: TButton
      Left = 28
      Top = 126
      Width = 113
      Height = 25
      Caption = 'Configuration Data'
      TabOrder = 8
      OnClick = CfgDataBtnClick
    end
    object HWRevEdit: TEdit
      Left = 70
      Top = 27
      Width = 89
      Height = 21
      TabOrder = 0
      Text = '1'
    end
    object FWRevEdit: TEdit
      Left = 70
      Top = 59
      Width = 89
      Height = 21
      TabOrder = 1
      Text = '2'
    end
    object DevIDEdit: TEdit
      Left = 70
      Top = 90
      Width = 89
      Height = 21
      TabOrder = 2
      Text = '123456'
    end
    object ResetReasonEdit: TEdit
      Left = 253
      Top = 27
      Width = 75
      Height = 21
      TabOrder = 3
      Text = '1'
    end
    object BattTypeEdit: TEdit
      Left = 253
      Top = 59
      Width = 75
      Height = 21
      TabOrder = 4
      Text = '1'
    end
    object TECErrorCB: TCheckBox
      Left = 177
      Top = 92
      Width = 128
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Indicate TEC Error'
      TabOrder = 5
    end
    object PIBErrorCB: TCheckBox
      Left = 177
      Top = 115
      Width = 128
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Indicate PIB Error'
      TabOrder = 6
    end
    object FlagStatusCB: TCheckBox
      Left = 177
      Top = 138
      Width = 128
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Flag Tripped'
      TabOrder = 7
    end
  end
  object SolenoidsGB: TGroupBox
    Left = 0
    Top = 412
    Width = 343
    Height = 41
    Anchors = [akLeft, akBottom]
    Caption = ' Solenoids '
    TabOrder = 2
    object Solenoid1Label: TLabel
      Left = 16
      Top = 20
      Width = 29
      Height = 13
      Caption = '1: Off'
    end
    object Solenoid2Label: TLabel
      Left = 75
      Top = 20
      Width = 29
      Height = 13
      Caption = '2: Off'
    end
    object Solenoid3Label: TLabel
      Left = 130
      Top = 20
      Width = 29
      Height = 13
      Caption = '3: Off'
    end
    object Solenoid4Label: TLabel
      Left = 185
      Top = 20
      Width = 29
      Height = 13
      Caption = '4: Off'
    end
    object Solenoid5Label: TLabel
      Left = 240
      Top = 20
      Width = 29
      Height = 13
      Caption = '5: Off'
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 455
    Width = 343
    Height = 41
    Anchors = [akLeft, akBottom]
    Caption = ' Contacts '
    TabOrder = 3
    object Contact1Label: TLabel
      Left = 16
      Top = 20
      Width = 29
      Height = 13
      Caption = '1: Off'
    end
    object Contact2Label: TLabel
      Left = 75
      Top = 20
      Width = 29
      Height = 13
      Caption = '2: Off'
    end
    object Contact3Label: TLabel
      Left = 130
      Top = 20
      Width = 29
      Height = 13
      Caption = '3: Off'
    end
    object Contact4Label: TLabel
      Left = 185
      Top = 20
      Width = 29
      Height = 13
      Caption = '4: Off'
    end
  end
end
