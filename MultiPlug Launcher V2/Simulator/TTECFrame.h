//---------------------------------------------------------------------------
#ifndef TTECFrameH
#define TTECFrameH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include "TSliderFrame.h"
#include "TMPLSSimObject.h"
//---------------------------------------------------------------------------
class TTECFrame : public TFrame
{
__published:	// IDE-managed Components
    TValueListEditor *TECParamsVLE;
    TGroupBox *DevSettingsGB;
    TGroupBox *SolenoidsGB;
    TLabel *Solenoid1Label;
    TLabel *Solenoid2Label;
    TLabel *Solenoid3Label;
    TLabel *Solenoid4Label;
    TLabel *Solenoid5Label;
    TButton *CfgDataBtn;
    TLabel *Label13;
    TLabel *Label11;
    TEdit *HWRevEdit;
    TEdit *FWRevEdit;
    TEdit *DevIDEdit;
    TGroupBox *GroupBox1;
    TLabel *Contact1Label;
    TLabel *Contact2Label;
    TLabel *Contact3Label;
    TLabel *Contact4Label;
    TLabel *Label17;
    TLabel *Label1;
    TEdit *ResetReasonEdit;
    TLabel *Label2;
    TEdit *BattTypeEdit;
    TCheckBox *TECErrorCB;
    TCheckBox *PIBErrorCB;
    TCheckBox *FlagStatusCB;
    void __fastcall CfgDataBtnClick(TObject *Sender);


private:

    bool m_bIsMasterFrame;

    TSliderFrame* m_pSliders[TMPLSSimObject::eRT_NbrReadingTypes];

    TNotifyEvent m_onShowConfigData;

    int  GetReading( TMPLSSimObject::eReadingType eType );
    void SetReading( TMPLSSimObject::eReadingType eType, int iValue );

    String GetOnOffString( const String& sPrefix, int itemNbr, bool bIsClosed );

public:
    __fastcall TTECFrame( TComponent* pOwner, TWinControl* pParent, const String& sCompName, bool bIsMasterFrame );

    void DoOnShow( void );
        // Called by parent when form becomes visible to force alignment
        // of child frames

    __property bool IsMasterFrame = { read = m_bIsMasterFrame };
        // Indicates whether this is the master frame or not

    __property TNotifyEvent OnShowConfigData = { read = m_onShowConfigData, write = m_onShowConfigData };
        // Event handler called when the "Configuration Data" button is pressed.
        // Owning object must show the Config Data dialog using current config
        // data from the sim object, then update the sim object if the user has
        // changed any config data.

    __property int Reading[TMPLSSimObject::eReadingType] = { read = GetReading, write = SetReading };
        // Gets / sets an individual TEC reading

    bool SetReadingRange( TMPLSSimObject::eReadingType eType, int iMinValue, int iMaxValue );
        // Sets the allowable range for a given reading type

    typedef struct {
        int iRFCRate;
        int iRFCTimeout;
        int iPairTimeout;
    } TEC_TIMING;

    void SetTimingParams( const TEC_TIMING& tecTiming );
        // sets the device configuration display

    void SetSolenoidStatus( bool bSol1On, bool bSol2On, bool bSol3On, bool bSol4On, bool bSol5On );
    void SetDigIOStatus   ( bool bDig1On, bool bDig2On, bool bDig3On, bool bDig4On );
        // Updates the status of the solenoids and digital I/Os.

    void GetDeviceIDs(       TMPLSSimObject::DEVICE_ID& tecDevID,       TMPLSSimObject::DEVICE_ID& pibDevID );
    void SetDeviceIDs( const TMPLSSimObject::DEVICE_ID& tecDevID, const TMPLSSimObject::DEVICE_ID& pibDevID );
        // Gets sets the device ident string, and HW and FW revs

    typedef struct {
        TMPLSSimObject::AD_PARAMS adParama[NBR_AD_INPUTS_PER_TEC];
    } TEC_PARAMS;

    void SetTECParams( const TEC_PARAMS tecParams );
        // Sets the TEC params as passed and refreshes the display

    void GetTECData( TMPLSSimObject::TEC_READINGS& newReadings );
        // Returns the current TEC data

    // VLE Helper functions (here because they needed a home somewhere)
    static void UpdateStatusGrid( TValueListEditor* aGrid, const String& key, const String& text );
    static void ClearStatusGrid( TValueListEditor* aGrid );

};
//---------------------------------------------------------------------------
extern PACKAGE TTECFrame *TECFrame;
//---------------------------------------------------------------------------
#endif
