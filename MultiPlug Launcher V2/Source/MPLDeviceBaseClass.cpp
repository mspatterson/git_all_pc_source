//************************************************************************
//
//  MPLDeviceBaseClass.h: base class for all MPL-like devices. This is
//       an abstract class. Therefore, descendent classes will need to
//       implemement a number of abstract functions.
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include <math.hpp>

#include "MPLDeviceBaseClass.h"
#include "TypeDefs.h"

#pragma package(smart_init)


//
// Private Declarations
//

const String TMPLDevice::CalFactorCaptions[TMPLDevice::NBR_CALIBRATION_ITEMS] = {
    "Mfg Date",
    "Mfg Info",
    "Cal Version",
    "Cal Revision",
    "Calibration Date",
    "Tank Press Offset",
    "Tank Press Span",
    "Reg Press Offset",
    "Reg Press Span",
};


const BYTE TMPLDevice::CurrCalVersion  = 2;
const BYTE TMPLDevice::CurrCalRevision = 0;


const String TMPLDevice::RFCFieldCaptions[NBR_RFC_FIELD_TYPES] = {
    "Link State",                 // RFT_DEV_STATE,
    "Last RFC",                   // RFT_LAST_RFC_TIME,
    "Status Bits",                // RFT_SYS_STATUS_BITS,
    "Comm Errors",                // RFT_COMM_ERRORS
    "Last Reset",                 // RFT_LAST_RESET,
    "Temperature",                // RFT_TEMPERATURE,
    "RPM",                        // RFT_RPM,
    "Flag State",                 // RFT_FLAG_DEPLOYED,
    "RFC Rate",                   // RFT_RFC_RATE,
    "RFC Timeout",                // RFT_RFC_TIMEOUT,
    "Pairing Timeout",            // RFT_PAIR_TIMEOUT,
    "Sol On - Launch",            // RFT_LAUNCH_TIME,
    "Sol On - Clean",             // RFT_CLEAN_TIME,
    "Sol On - Flag Reset",        // RFT_FLAG_RST_TIME,

    "Master Batt Volts",          // RFT_MASTER_BATT_VOLTS,
    "Master Batt Type",           // RFT_MASTER_BATT_TYPE,
    "Master Tank Press",          // RFT_MASTER_TANK_PRESS,
    "Master Reg Press",           // RFT_MASTER_REG_PRESS,
    "Master Sol State",           // RFT_MASTER_SOL_STATE,
    "Master Sol Current",         // RFT_MASTER_SOL_CURRENT,
    "Master Isol Outs",           // RFT_MASTER_ISOL_OUTS_STATE,
    "Master TEC SN",              // RFT_MASTER_TEC_SN,
    "Master TEC FW",              // RFT_MASTER_TEC_FW,
    "Master TEC HW",              // RFT_MASTER_TEC_HW,
    "Master PIB SN",              // RFT_MASTER_PIB_SN,
    "Master PIB FW",              // RFT_MASTER_PIB_FW,
    "Master PIB HW",              // RFT_MASTER_PIB_HW,

    "Slave Batt Volts",           // RFT_SLAVE_BATT_VOLTS,
    "Slave Batt Type",            // RFT_SLAVE_BATT_TYPE,
    "Slave Tank Press",           // RFT_SLAVE_TANK_PRESS,
    "Slave Reg Press",            // RFT_SLAVE_REG_PRESS,
    "Slave Sol State",            // RFT_SLAVE_SOL_STATE,
    "Slave Sol Current",          // RFT_SLAVE_SOL_CURRENT,
    "Slave Isol Outs",            // RFT_SLAVE_ISOL_OUTS_STATE,
    "Slave TEC SN",               // RFT_SLAVE_TEC_SN,
    "Slave TEC FW",               // RFT_SLAVE_TEC_FW,
    "Slave TEC HW",               // RFT_SLAVE_TEC_HW,
    "Slave PIB SN",               // RFT_SLAVE_PIB_SN,
    "Slave PIB FW",               // RFT_SLAVE_PIB_FW,
    "Slave PIB HW",               // RFT_SLAVE_PIB_HW,

    "Plug Det Rate",              // RFT_DET_RATE,
    "Plug Det Avg'ing",           // RFT_DET_AVGING,
    "Plug Det SN",                // RFT_DET_SN
    "Plug Det FW",                // RFT_DET_FW
    "Plug Det HW",                // RFT_DET_HW

    "Log File Name",              // RFT_LOG_FILE_NAME

    "Bytes Recv'd",               // RFT_BYTES_RXD
    "Good Pkts Recv'd",           // RFT_GOOD_PKTS
    "Pkt Flushes",                // RFT_RXD_FLUSHES
    "Pkt Errors",                 // RFT_RXD_PKT_ERRS
};


// Default times, in msecs unless otherwise noted
#define DEFAULT_LAUNCH_TIME       10000
#define DEFAULT_CLEAN_TIME        10000
#define DEFAULT_RESET_FLAG_TIME   10000


static TAverage* CreateNewAverage( const AVERAGING_PARAMS& avgParams )
{
    TAverage* newAvg;

    switch( avgParams.avgMethod )
    {
        case AM_BOXCAR:      newAvg = new TMovingAverage( (int)( avgParams.param + 0.5 ), avgParams.absVariance );  break;
        case AM_EXPONENTIAL: newAvg = new TExpAverage( avgParams.param, avgParams.absVariance );                    break;
        default:             newAvg = new TNoAverage();  break;
    }

    return newAvg;
}


//
// Public Constants
//

const String MPLTypeText[MT_NBR_DEVICES] = {
    L"Null Device",
    L"MPLS Device",
};


//
// Class Implementation
//

__fastcall TMPLDevice::TMPLDevice( MPL_TYPE mplType )
{
    // Save our type
    m_mplType     = mplType;
    m_mplTypeText = MPLTypeText[mplType];

    // Reset comms
    ClearCommStats();

    m_port.portType       = CT_UNUSED;
    m_port.connResult     = CCommObj::ERR_NONE;
    m_port.connResultText = "(closed)";

    m_portLost = false;

    // Set default averaging params
    m_avgRPM = new TNoAverage();

    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        m_tecInfo[iUnit].tankAvgPress = new TNoAverage();
        m_tecInfo[iUnit].regAvgPress  = new TNoAverage();
    }

    ResetDeviceParams();
}


void TMPLDevice::ResetDeviceParams( void )
{
    // Set time param defaults
    GetWirelessTimingParams( m_timeParams );
    GetWirelessTimingParams( m_pendingTimeParams );

    // Set the default packet timeout
    m_tPacketTimeout = DEFAULT_PACKET_TIMEOUT;

    // Clear TEC unit params
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        m_tecInfo[iUnit].sDevID     = "";
        m_tecInfo[iUnit].battLevel  = eBL_Unknown;
        m_tecInfo[iUnit].battType   = BT_UNKNOWN;
        m_tecInfo[iUnit].activeSol  = 0;
        m_tecInfo[iUnit].pendingSol = -1;
        m_tecInfo[iUnit].bcSolCmd   = eBC_NoCmd;
        m_tecInfo[iUnit].maxSolCurr = 0;

        for( int iIsolOut = 0; iIsolOut < NBR_ISOL_OUTS_PER_TEC; iIsolOut++ )
        {
            m_tecInfo[iUnit].pibIsolOutState[iIsolOut] = eBS_Unknown;
            m_tecInfo[iUnit].pibIsolOutCmd  [iIsolOut] = eBC_NoCmd;
        }
    }

    // No plug is detected
    m_ePlugDetState = ePDS_NoPlug;

    // Set solenoid control params. Even though there can be two TEC
    // units per system, there is only a single command that controls
    // the state of both solenoids.
    m_solOnTimeLaunch       = DEFAULT_LAUNCH_TIME;
    m_solOnTimeClean        = DEFAULT_CLEAN_TIME;
    m_solOnTimeReset        = DEFAULT_RESET_FLAG_TIME;
}


void TMPLDevice::ResetAveraging( void )
{
    m_avgRPM->Reset();

    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        m_tecInfo[iUnit].tankAvgPress->Reset();
        m_tecInfo[iUnit].regAvgPress->Reset();
    }
}


void TMPLDevice::ClearHardwareInfo( void )
{
    for( int iComp = 0; iComp < eMC_NbrMPLSComps; iComp++ )
    {
        m_deviceInfo[iComp].sSN     = "";
        m_deviceInfo[iComp].dwHWRev = 0;
        m_deviceInfo[iComp].dwFWRev = 0;

        m_deviceErrCounts[iComp] = 0;
    }
}


bool TMPLDevice::GetDeviceHWInfo( eMPLSComponent whichComp, bool& bPresent, MPLS_COMP_ID& devInfo )
{
    // Init result struct in case of early return
    devInfo.sSN     = "";
    devInfo.dwFWRev = 0;
    devInfo.dwHWRev = 0;

    switch( whichComp )
    {
        case eMC_MasterTEC:
            bPresent = true;   // This must always be the case
            break;

        case eMC_MasterPIB:
            bPresent = GetHaveMasterPIB();
            break;

        case eMC_SlaveTEC:
            bPresent = GetHaveSlaveTEC();
            break;

        case eMC_SlavePIB:
            bPresent = GetHaveSlavePIB();
            break;

        case eMC_PlugDet:
            bPresent = GetHavePlugDet();
            break;

        default:
            bPresent = false;
            return false;
    }

    // Fall through implies we had a good enum passed
    devInfo.sSN     = m_deviceInfo[whichComp].sSN;
    devInfo.dwHWRev = m_deviceInfo[whichComp].dwHWRev;
    devInfo.dwFWRev = m_deviceInfo[whichComp].dwFWRev;

    return true;
}


String TMPLDevice::DevSNToString( const BYTE bySNBytes[] )
{
    // An empty array of bytes means the device is not present.
    // 0xFF means SN not programmed
    String sResult;

    if( bySNBytes[0] == 0x00 )
    {
        sResult = "(no device)";
    }
    else if( bySNBytes[0] == 0xFF )
    {
        sResult = "(not programmed)";
    }
    else
    {
        int iByteIndex = 0;

        while( bySNBytes[iByteIndex] != 0xFF )
        {
           if( bySNBytes[iByteIndex] == 0x00 )
               break;

           // Make sure this is a 'printable' char
           if( ( bySNBytes[iByteIndex] >= 0x20 ) && ( bySNBytes[iByteIndex] <= 0x7E ) )
               sResult = sResult + (char)( bySNBytes[iByteIndex] );
           else
               sResult = sResult + "?";

           iByteIndex++;

           if( iByteIndex >= 32 )
               break;
        }
    }

    return sResult;
}


bool TMPLDevice::GetHaveMasterPIB( void )
{
    return( m_deviceInfo[eMC_MasterPIB].dwFWRev != 0 );
}


bool TMPLDevice::GetHaveSlaveTEC( void )
{
    return( m_deviceInfo[eMC_SlaveTEC].dwFWRev != 0 );
}


bool TMPLDevice::GetHaveSlavePIB( void )
{
    return( m_deviceInfo[eMC_SlavePIB].dwFWRev != 0 );
}


bool TMPLDevice::GetHavePlugDet( void )
{
    return( m_deviceInfo[eMC_PlugDet].dwFWRev != 0 );
}


bool TMPLDevice::GetDevIsRecving( void )
{
    if( time( NULL ) > m_port.stats.tLastPkt + m_tPacketTimeout )
        return false;

    return true;
}


bool TMPLDevice::GetIsConnected( void )
{
    // Legacy sub only uses the data port
    if( m_port.portObj == NULL )
        return false;

    return m_port.portObj->isConnected;
}


bool TMPLDevice::GetPlugDetected( void )
{
    // Return true only if a plug has been detected but not ack'd
    return( m_ePlugDetState == ePDS_PlugDetected );
}


void TMPLDevice::SetPlugDetected( void )
{
    // Protected method that derived classes can use to indicate
    // a plug is present. It is safe to repeatedly call this method
    // while a plug is being detected - it will only fire its callback
    // the first time.
    if( m_ePlugDetState == ePDS_NoPlug )
    {
        m_ePlugDetState = ePDS_PlugDetected;
        DoOnPlugDetected();
    }
}


BATTERY_TYPE TMPLDevice::GetBattTypeFromStatus( eMPLSUnit eUnit, WORD statusBits )
{
    BATTERY_TYPE btType = BT_UNKNOWN;

    WORD wBattBits;

    if( eUnit == eMU_MasterMPLS )
        wBattBits = ( statusBits >> 2 ) & 0x0003;
    else
        wBattBits = ( statusBits >> 4 ) & 0x0003;

    if( wBattBits < NBR_BATTERY_TYPES )
        btType = (BATTERY_TYPE)wBattBits;

    return btType;
}


void TMPLDevice::RefreshSettings( void )
{
    // Called to refresh any registry-based settings. Causes those settings
    // to be re-read from the registry.
    SolenoidOnTimeLaunch = GetSolenoidOnTime( SOTT_LAUNCH );
    SolenoidOnTimeClean  = GetSolenoidOnTime( SOTT_CLEAN  );
    SolenoidOnTimeReset  = GetSolenoidOnTime( SOTT_RESET  );

    GetWirelessTimingParams( m_pendingTimeParams );

    String sLogName;

    if( GetDataLogging( DLT_RAW_DATA, sLogName ) )
    {
        // Turn on logging
        EnableRFCLogging( sLogName );
    }
    else
    {
        // Turn off logging
        EnableRFCLogging( "" );
    }

    // Refresh averaging
    AVERAGING_PARAMS avgParams;
    GetAveragingSettings( AT_RPM, avgParams );
    SetRPMAveraging( avgParams );

    GetAveragingSettings( AT_PRESS, avgParams );
    SetPressAveraging( avgParams );
}


void TMPLDevice::AckPlugDetected( void )
{
    // Called by the client when it has ack'd the presence of a plug.
    // Will cause PlugDetected to go false, and PlugDetected won't go
    // true again until a new plug is detected
    if( m_ePlugDetState == ePDS_PlugDetected )
        m_ePlugDetState = ePDS_DetectAckd;
}


void TMPLDevice::SetRFCRate( WORD wNewRate )
{
    m_timeParams.rfcRate = wNewRate;
}


void TMPLDevice::SetRFCTimeout( WORD wNewRate )
{
    m_timeParams.rfcTimeout = wNewRate;
}


void TMPLDevice::SetPairingTimeout( WORD wNewRate )
{
    m_timeParams.pairingTimeout = wNewRate;
}


int TMPLDevice::GetActiveSol( eMPLSUnit eUnit )
{
    // Method support public property, so validate eUnit param
    if( eUnit == eMU_MasterMPLS )
        return m_tecInfo[eMU_MasterMPLS].activeSol;
    else if( eUnit == eMU_SlaveMPLS )
        return m_tecInfo[eMU_SlaveMPLS].activeSol;
    else
        return 0;
}


int TMPLDevice::GetPendingSol( eMPLSUnit eUnit )
{
    // Method support public property, so validate eUnit param
    if( eUnit == eMU_MasterMPLS )
        return m_tecInfo[eMU_MasterMPLS].pendingSol;
    else if( eUnit == eMU_SlaveMPLS )
        return m_tecInfo[eMU_SlaveMPLS].pendingSol;
    else
        return -1;
}


float TMPLDevice::GetAvgRPM( void )
{
    if( m_avgRPM == NULL )
        return NaN;

    if( m_avgRPM->IsValid )
        return m_avgRPM->AverageValue;
    else
        return NaN;
}


float TMPLDevice::GetTankPress( eMPLSUnit eUnit )
{
    TAverage* pAvg;

    switch( eUnit )
    {
        case eMU_MasterMPLS:
        case eMU_SlaveMPLS:
            pAvg = m_tecInfo[eUnit].tankAvgPress;
            break;

        default:
            return NaN;
    }

    if( pAvg->IsValid )
        return pAvg->AverageValue;
    else
        return NaN;
}


float TMPLDevice::GetRegPress ( eMPLSUnit eUnit )
{
    TAverage* pAvg;

    switch( eUnit )
    {
        case eMU_MasterMPLS:
        case eMU_SlaveMPLS:
            pAvg = m_tecInfo[eUnit].regAvgPress;
            break;

        default:
            return NaN;
    }

    if( pAvg->IsValid )
        return pAvg->AverageValue;
    else
        return NaN;
}


bool TMPLDevice::Connect( const COMMS_CFG& portCfg )
{
    if( portCfg.portType == CT_UNUSED )
    {
        m_port.connResultText = "port settings not specified";
        return false;
    }

    return CreateCommPort( portCfg, m_port );
}


void TMPLDevice::Disconnect( void )
{
    // Base class only disconnects and releases any created objects
    ReleaseCommPort( m_port );

    // Clear our data structs
    ResetDeviceParams();
}


void TMPLDevice::GetCommStats( PORT_STATS& commStats )
{
    commStats = m_port.stats;
}


void TMPLDevice::ClearCommStats( void )
{
    // Only the count members of the stats gets updated
    ClearPortStats( m_port.stats );
}


void TMPLDevice::SetRPMAveraging( const AVERAGING_PARAMS& avgParams )
{
    if( m_avgRPM != NULL )
        delete m_avgRPM;

    m_avgRPM = CreateNewAverage( avgParams );
}


void TMPLDevice::SetPressAveraging( const AVERAGING_PARAMS& avgParams )
{
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        if( m_tecInfo[iUnit].tankAvgPress != NULL )
            delete m_tecInfo[iUnit].tankAvgPress;

        if( m_tecInfo[iUnit].regAvgPress != NULL )
            delete m_tecInfo[iUnit].regAvgPress;

        m_tecInfo[iUnit].tankAvgPress = CreateNewAverage( avgParams );
        m_tecInfo[iUnit].regAvgPress  = CreateNewAverage( avgParams );
    }
}


bool TMPLDevice::InitPropertyList( TStringList* pCaptions, TStringList* pValues, const String captionList[], int nbrCaptions )
{
    // Helper function that initializes the passed string lists used
    // to populate property editors.
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( !IsConnected )
        return false;

    // Add all of the captions first, with blank
    for( int iItem = 0; iItem < nbrCaptions; iItem++ )
    {
        pCaptions->Add( captionList[iItem] );
        pValues->Add( "" );
    }

    return true;
}


void TMPLDevice::ClearV2RFCPkt( LOGGED_RFC_DATA& rfcPkt )
{
    // For now, just set the entire packet to zero
    memset( &rfcPkt, 0, sizeof( LOGGED_RFC_DATA ) );
}


String TMPLDevice::GetSensorSamplingRateText( BYTE byEnumVal )
{
    switch( byEnumVal )
    {
        case 0:  return "0 - standby mode";
        case 1:  return "1 - 800 Hz (1.25 ms)";
        case 2:  return "2 - 400 Hz (2.5 ms)";
        case 3:  return "3 - 200 Hz (5 ms)";
        case 4:  return "4 - 100 Hz (10 ms)";
        case 5:  return "5 - 50 Hz (20 ms)";
        case 6:  return "6 - 12.5 Hz (80 ms)";
        case 7:  return "7 - 6.25 Hz (160 ms)";
        case 8:  return "8 - 1.5625 Hz (640 ms)";
    }

    // Fall through means unknown enum value
    return "Unknown enum " + IntToStr( byEnumVal );
}


float TMPLDevice::ConvertRawBattVolts( WORD wVBatt )
{
    // Voltage in mV (eg 3.742V would be 3742)
    XFER_BUFFER xferBuff;

    xferBuff.wData[0] = wVBatt;
    xferBuff.wData[1] = 0;

    return (float)xferBuff.iData / 1000.0;
}


float TMPLDevice::ConvertRawTemp( BYTE byRawTemp )
{
    // Byte is a signed value, 0.5C per count with, -64C to +63.5C
    XFER_BUFFER xferBuff = { 0 };

    xferBuff.byData[0] = byRawTemp;

    // Manually sign extend
    if( byRawTemp & 0x80 )
    {
        xferBuff.byData[1] = 0xFF;
        xferBuff.byData[2] = 0xFF;
        xferBuff.byData[3] = 0xFF;
    }

    return (float)( xferBuff.iData * 5 ) / 10.0;
}


float TMPLDevice::ConvertRawRPM( BYTE byRawRPM )
{
    // For now, just report the value as a float
    XFER_BUFFER xferBuff = { 0 };

    xferBuff.byData[0] = byRawRPM;

    return (float)xferBuff.iData;
}


void TMPLDevice::DoOnDeviceConnected( void )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };

        m_onDevEvent( this, eME_DevConnected, xbData );
    }
}


void TMPLDevice::DoOnInitFailed( void )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };

        m_onDevEvent( this, eME_InitFailed, xbData );
    }
}


void TMPLDevice::DoOnLinkStateChange( void )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };

        m_onDevEvent( this, eME_LinkStateChange, xbData );
    }
}


void TMPLDevice::DoOnRFCRecvd( void )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };

        m_onDevEvent( this, eME_RFCRcvd, xbData );
    }
}


void TMPLDevice::DoOnLaunchDone( eCanisterNbr ePlug, int iSolCurrent )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };
        xbData.wData[0] = ePlug;
        xbData.wData[1] = iSolCurrent;

        m_onDevEvent( this, eME_LaunchDone, xbData );
    }
}


void TMPLDevice::DoOnPlugDetected( void )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };

        m_onDevEvent( this, eME_PlugDetected, xbData );
    }
}


void TMPLDevice::DoOnCleaningDone( eCanisterNbr ePlug, int iSolCurrent )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };
        xbData.wData[0] = ePlug;
        xbData.wData[1] = iSolCurrent;

        m_onDevEvent( this, eME_CleaningDone, xbData );
    }
}


void TMPLDevice::DoOnFlagTripped( void )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };

        m_onDevEvent( this, eME_FlagTripped, xbData );
    }
}


void TMPLDevice::DoOnResetFlagDone( int iSolCurrent )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };
        xbData.wData[0] = 0;
        xbData.wData[1] = iSolCurrent;

        m_onDevEvent( this, eME_FlagResetDone, xbData );
    }
}


void TMPLDevice::DoOnCmdFailed( BYTE byCmdNbr, BYTE byReason )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };

        xbData.byData[0] = byCmdNbr;
        xbData.byData[1] = byReason;

        m_onDevEvent( this, eME_CommandFailed, xbData );
    }
}


void TMPLDevice::DoOnHWErrors( BYTE errorBits )
{
    if( m_onDevEvent )
    {
        XFER_BUFFER xbData = { 0 };

        xbData.byData[0] = errorBits;

        m_onDevEvent( this, eME_HWErrors, xbData );
    }
}

