//************************************************************************
//
//  MPLDeviceBaseClass.h: base class for all MPL devices. This is
//       an abstract class. Therefore, descendent classes will need to
//       implemement a number of the abstract functions declared below.
//
//************************************************************************

#ifndef MPLDeviceBaseClassH
#define MPLDeviceBaseClassH

    #include "CommObj.h"
    #include "MPLRegistryInterface.h"
    #include "WTTTSDefs.h"
    #include "WTTTSProtocolUtils.h"
    #include "SerialPortUtils.h"
    #include "MPLSDefs.h"


    typedef enum {
        MT_NULL,
        MT_MPL,
        MT_NBR_DEVICES
    } MPL_TYPE;

    extern const String MPLTypeText[];


    #define MPL_RESET_FLAG_SOLENOID 5


    typedef enum {
        eME_DevConnected,
        eME_InitFailed,
        eME_LinkStateChange,
        eME_RFCRcvd,
        eME_LaunchDone,
        eME_PlugDetected,
        eME_CleaningDone,
        eME_FlagTripped,
        eME_FlagResetDone,
        eME_CommandFailed,
        eME_HWErrors,
        eME_NbrMPLSEvents
    } eMPLSEvent;

    typedef void __fastcall (__closure *TMPLSEvent)(TObject* Sender, eMPLSEvent eEvt, const XFER_BUFFER& sbEvtData );


    class TMPLDevice : public TObject {

      public:

        typedef enum {
            eBL_Unknown,
            eBL_Low,
            eBL_Med,
            eBL_OK,
            eNbrBattLevels
        } BatteryLevelType;

      private:

        MPL_TYPE      m_mplType;
        String        m_mplTypeText;

      protected:

        DEVICE_PORT   m_port;

        bool          m_portLost;

        time_t        m_tPacketTimeout;

        TMPLSEvent    m_onDevEvent;

        // Averaging parameters - we do RPM averaging ourself, but the device
        // does averaging on the sensor readings
        TAverage*     m_avgRPM;

        // Device parameters
        DWORD         m_solOnTimeLaunch;         // How long the solenoid is to be activate for a launch
        DWORD         m_solOnTimeClean;          // How long the solenoid is to be activate for a clean
        DWORD         m_solOnTimeReset;          // How long the solenoid is to be activate when issuing a reset flag

        typedef enum
        {
           ePDS_NoPlug,          // No plug present
           ePDS_PlugDetected,    // Plug detected, not yet ack'd by client
           ePDS_DetectAckd       // Client has ack'd that a plug is present
        } PlugDetectState;

        PlugDetectState m_ePlugDetState;

        // Device info
        MPLS_COMP_ID m_deviceInfo[eMC_NbrMPLSComps];
        int          m_deviceErrCounts[eMC_NbrMPLSComps];

        // Logging
        String m_rfcLogFileName;


                __fastcall  TMPLDevice( MPL_TYPE mplType );
                            // Can only construct descendant classes

                void          ResetDeviceParams( void );
                void          ResetAveraging( void );
                void          ClearHardwareInfo( void );
                String        DevSNToString( const BYTE bySNBytes[] );

        virtual String        GetDevStatus( void ) = 0;
        virtual bool          GetDevIsIdle( void ) = 0;
        virtual bool          GetDevIsRecving( void );
        virtual bool          GetDevIsCalibrated( void ) = 0;
        virtual bool          GetIsConnected( void );

        virtual String        GetPortDesc( void )  { return m_port.stats.portDesc; }

                bool          InitPropertyList( TStringList* pCaptions, TStringList* pValues, const String captionList[], int nbrCaptions );

        virtual bool          GetHaveMasterPIB( void );
        virtual bool          GetHaveSlaveTEC( void );
        virtual bool          GetHaveSlavePIB( void );
        virtual bool          GetHavePlugDet( void );

        WIRELESS_TIME_PARAMS  m_timeParams;         // Wireless timing currently in effect
        WIRELESS_TIME_PARAMS  m_pendingTimeParams;  // Wireless timing pending to be sent to MPLS

        static const BYTE CurrCalVersion;
        static const BYTE CurrCalRevision;

        // Master / slave specific data
        struct {
            String    sDevID;

            BoolState pibIsolOutState[NBR_ISOL_OUTS_PER_TEC];    // Current state of isolated outputs
            BoolCmd   pibIsolOutCmd  [NBR_ISOL_OUTS_PER_TEC];    // Desired state of isolated outputs

            BatteryLevelType battLevel;
            BATTERY_TYPE     battType;

            TAverage* tankAvgPress;
            TAverage* regAvgPress;

            // Even though only one solenoid in an entire MPLS system is supposed to
            // be active at any given time, technically the hardware treats the master
            // and slave PIB boards as separate units.
            //
            // Solenoid state machine: currently solenoids only get commanded on, not off
            // (they automatically turn themselves off). So the following variables are used
            // to control the solenoid turn on state machine:
            //    pendingSol: set to the solenoid to turn on when a request is made by client layer
            //                -1 means no change requested
            //    activeSol:  set to the solenoid that is active after receipt of RFC
            //    bcSolCmd:   set to turn-off or turn-on after receipt of a solenoid operation command
            int      activeSol;     // The solenoid the is currently active, 0 = all off
            int      pendingSol;    // The solenoid we want activated, 0 = all off, -1 = no solenoid
            BoolCmd  bcSolCmd;

            int      maxSolCurr; // Latched while a solenoid is active

        } m_tecInfo[eMU_NbrMPLSUnits];

        virtual BatteryLevelType GetBattLevel( eMPLSUnit eUnit ) { return m_tecInfo[eUnit].battLevel; }
        virtual BATTERY_TYPE     GetBattType ( eMPLSUnit eUnit ) { return m_tecInfo[eUnit].battType;  }
        virtual String           GetDeviceID ( eMPLSUnit eUnit ) { return m_tecInfo[eUnit].sDevID;    }

        BATTERY_TYPE GetBattTypeFromStatus( eMPLSUnit eUnit, WORD statusBits );

        // RFC support
        typedef enum {
            RFT_DEV_STATE,
            RFT_LAST_RFC_TIME,
            RFT_SYS_STATUS_BITS,
            RFT_COMM_ERRORS,
            RFT_LAST_RESET,
            RFT_TEMPERATURE,
            RFT_RPM,
            RFT_FLAG_DEPLOYED,
            RFT_RFC_RATE,
            RFT_RFC_TIMEOUT,
            RFT_PAIR_TIMEOUT,
            RFT_LAUNCH_TIME,
            RFT_CLEAN_TIME,
            RFT_FLAG_RST_TIME,

            RFT_MASTER_BATT_VOLTS,
            RFT_MASTER_BATT_TYPE,
            RFT_MASTER_TANK_PRESS,
            RFT_MASTER_REG_PRESS,
            RFT_MASTER_SOL_STATE,
            RFT_MASTER_SOL_CURRENT,
            RFT_MASTER_ISOL_OUTS_STATE,
            RFT_MASTER_TEC_SN,
            RFT_MASTER_TEC_FW,
            RFT_MASTER_TEC_HW,
            RFT_MASTER_PIB_SN,
            RFT_MASTER_PIB_FW,
            RFT_MASTER_PIB_HW,

            RFT_SLAVE_BATT_VOLTS,
            RFT_SLAVE_BATT_TYPE,
            RFT_SLAVE_TANK_PRESS,
            RFT_SLAVE_REG_PRESS,
            RFT_SLAVE_SOL_STATE,
            RFT_SLAVE_SOL_CURRENT,
            RFT_SLAVE_ISOL_OUTS_STATE,
            RFT_SLAVE_TEC_SN,
            RFT_SLAVE_TEC_FW,
            RFT_SLAVE_TEC_HW,
            RFT_SLAVE_PIB_SN,
            RFT_SLAVE_PIB_FW,
            RFT_SLAVE_PIB_HW,

            RFT_DET_RATE,
            RFT_DET_AVGING,
            RFT_DET_SN,
            RFT_DET_FW,
            RFT_DET_HW,

            RFT_LOG_FILE_NAME,

            RFT_BYTES_RXD,
            RFT_GOOD_PKTS,
            RFT_RXD_FLUSHES,
            RFT_RXD_PKT_ERRS,

            NBR_RFC_FIELD_TYPES
        } RFC_FIELD_TYPE;

        static const String RFCFieldCaptions[];

        void ClearV2RFCPkt( LOGGED_RFC_DATA& rfcPkt );

        String GetSensorSamplingRateText( BYTE byEnumVal );

        // Plug detection
        bool GetPlugDetected( void );
        void SetPlugDetected( void );

        // Flag reporting
        virtual bool GetFlagTripped( void ) { return false; }

        // Timing params
        virtual void SetRFCRate( WORD wNewRate );
        virtual void SetRFCTimeout( WORD wNewRate );
        virtual void SetPairingTimeout( WORD wNewRate );

        // Cycle support
        virtual bool GetCanStartCycle( void ) { return false; }
        virtual int  GetActiveSol( eMPLSUnit eUnit );
        virtual int  GetPendingSol( eMPLSUnit eUnit );

        // Averages
        virtual float GetAvgRPM( void );
        virtual float GetTankPress( eMPLSUnit eUnit );
        virtual float GetRegPress ( eMPLSUnit eUnit );

        // Conversion support
        static float ConvertRawBattVolts( WORD wVBatt );
        static float ConvertRawTemp( BYTE byRawTemp );
        static float ConvertRawRPM( BYTE byRawRPM );

        // Notification support
        void DoOnDeviceConnected( void );
        void DoOnInitFailed( void );
        void DoOnLinkStateChange( void );
        void DoOnRFCRecvd( void );
        void DoOnLaunchDone( eCanisterNbr ePlug, int iSolCurrent );
        void DoOnPlugDetected( void );
        void DoOnCleaningDone( eCanisterNbr eCan, int iSolCurrent );
        void DoOnFlagTripped( void );
        void DoOnResetFlagDone( int iSolCurrent );
        void DoOnCmdFailed( BYTE byCmdNbr, BYTE byReason );
        void DoOnHWErrors( BYTE errorBits );

      public:

        virtual __fastcall ~TMPLDevice() { Disconnect(); }

        __property MPL_TYPE      DeviceType         = { read = m_mplType };
        __property String        DeviceTypeText     = { read = m_mplTypeText };

        __property String        DeviceStatus       = { read = GetDevStatus };
        __property bool          DeviceIsIdle       = { read = GetDevIsIdle };
        __property bool          DeviceIsCalibrated = { read = GetDevIsCalibrated };
        __property bool          DeviceIsRecving    = { read = GetDevIsRecving };

        virtual bool Connect( const COMMS_CFG& portCfg );
        virtual void Disconnect( void );

        __property String           DeviceID    [eMPLSUnit] = { read = GetDeviceID };
        __property BatteryLevelType BatteryLevel[eMPLSUnit] = { read = GetBattLevel };
        __property BATTERY_TYPE     BatteryType [eMPLSUnit] = { read = GetBattType };

        __property bool          IsConnected   = { read = GetIsConnected };
        __property String        ConnectResult = { read = m_port.connResultText };
        __property bool          PortLost      = { read = m_portLost };
        __property String        PortDesc      = { read = GetPortDesc };

        __property bool          HaveMasterPIB = { read = GetHaveMasterPIB };
        __property bool          HaveSlaveTEC  = { read = GetHaveSlaveTEC };
        __property bool          HaveSlavePIB  = { read = GetHaveSlavePIB };
        __property bool          HavePlugDet   = { read = GetHavePlugDet };

        __property bool          CanStartCycle  = { read = GetCanStartCycle };
        __property bool          PlugDetected   = { read = GetPlugDetected };
        __property bool          FlagTripped    = { read = GetFlagTripped };

        __property float         AvgRPM                  = { read = GetAvgRPM };
        __property float         AvgTankPress[eMPLSUnit] = { read = GetTankPress };
        __property float         AvgRegPress[eMPLSUnit]  = { read = GetRegPress };

        __property int           ActiveSol[eMPLSUnit]    = { read = GetActiveSol  };
        __property int           PendingSol[eMPLSUnit]   = { read = GetPendingSol };

        __property WORD          RFCRate        = { read = m_timeParams.rfcRate,        write = m_pendingTimeParams.rfcRate };
        __property WORD          RFCTimeout     = { read = m_timeParams.rfcTimeout,     write = m_pendingTimeParams.rfcTimeout };
        __property WORD          PairingTimeout = { read = m_timeParams.pairingTimeout, write = m_pendingTimeParams.pairingTimeout };

        __property DWORD         SolenoidOnTimeLaunch = { read = m_solOnTimeLaunch, write = m_solOnTimeLaunch };
        __property DWORD         SolenoidOnTimeClean  = { read = m_solOnTimeClean,  write = m_solOnTimeClean };
        __property DWORD         SolenoidOnTimeReset  = { read = m_solOnTimeReset,  write = m_solOnTimeReset};

        __property TMPLSEvent    OnDeviceEvent = { read = m_onDevEvent, write = m_onDevEvent };

        void SetRPMAveraging  ( const AVERAGING_PARAMS& avgParams );
        void SetPressAveraging( const AVERAGING_PARAMS& avgParams );
            // Updates the passed averaging type to use the new averaging method.

        virtual bool Update( void ) = 0;
            // Descendant classes must implement this function. It will get called
            // regularly to allow the class to receive and process information.

        virtual void RefreshSettings( void );
            // Called to refresh any registry-based settings. Causes those settings
            // to be re-read from the registry.

        virtual bool LaunchPlug( eCanisterNbr ePlug ) = 0;
            // Instructs the MPL to activate the launch mechanism for the indicated plug.
            // Plug number must range in value from 1 to NBR_MPL_PLUGS

        virtual void AckPlugDetected( void );
            // Called by the client when it has ack'd the presence of a plug.
            // Will cause PlugDetected to go false, and PlugDetected won't go
            // true again until a new plug is detected

        virtual bool CleanCanister( eCanisterNbr eCan ) = 0;
            // Instructs the MPL to activate the cleaning mechanism for the
            // indicated canister.

        virtual bool ActivateResetFlag( void ) = 0;
            // Activates the reset flag solenoid

        virtual void GetCommStats( PORT_STATS& commStats );
        virtual void ClearCommStats( void );

        // RFC support
        virtual bool ShowLastRFCPkt( TStringList* pCaptions, TStringList* pValues ) = 0;
        virtual bool GetLastRFCPkt ( TDateTime& tPktTime, DWORD& rfcCount, LOGGED_RFC_DATA& rfcPkt ) = 0;

        // Calibration constants and methods
        typedef enum {
            CI_MFG_DATE,
            CI_MFG_INFO,
            CI_CAL_VER,
            CI_CAL_REV,
            CI_CAL_DATE,
            CI_TANK_PRESS_OFFSET,
            CI_TANK_PRESS_SPAN,
            CI_REG_PRESS_OFFSET,
            CI_REG_PRESS_SPAN,
            NBR_CALIBRATION_ITEMS
        } CALIBRATION_ITEM;

        static const String CalFactorCaptions[];

        virtual bool GetCalDataRaw      ( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues ) = 0;
        virtual bool GetCalDataFormatted( eMPLSUnit whichUnit, CALIBRATION_ITEM ciItem, String& sValue       ) = 0;
        virtual bool GetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues ) = 0;
        virtual bool SetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues ) = 0;
            // Calibration support routines. The get 'raw' function returns data as
            // binary bytes. The 'formatted' get/set functions provide access to
            // data as named values.

        virtual bool ReloadCalDataFromDevice( eMPLSUnit whichUnit ) = 0;
            // Causes the device to clear it internal cached cal data and
            // reload the data from the device.

        virtual bool WriteCalDataToDevice( eMPLSUnit whichUnit ) = 0;
            // Writes the calibration data currently cached in the software
            // to the MPLS device.

        virtual bool GetDeviceHWInfo( eMPLSComponent whichComp, bool& bPresent, MPLS_COMP_ID& devInfo );
            // Gets HW info of connected MPLS components

        virtual bool GetDeviceBatteryInfo( eMPLSUnit whichUnit, BATTERY_INFO& battInfo ) = 0;
            // All devices must report back battery information

        virtual void EnableRFCLogging( const String& sLogFileName ) { m_rfcLogFileName = sLogFileName; }
            // Enables auxilliary logging of RFC data. If filename is empty, no
            // logging is performed

    };

#endif

