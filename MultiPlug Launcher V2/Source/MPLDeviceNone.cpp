#include <vcl.h>
#pragma hdrstop

//
// Stub class when no device is loaded
//

#include "MPLDeviceNone.h"
#include "ApplUtils.h"

#pragma package(smart_init)


//
// Class Implementation
//

__fastcall TNullMPLDevice::TNullMPLDevice( void ) : TMPLDevice( MT_NULL )
{
    // Init port object
    m_port.portType = CT_UNUSED;
    m_port.portObj  = NULL;

    m_port.stats.portType  = CT_UNUSED;
    m_port.stats.portDesc  = "N/A";

    ClearPortStats( m_port.stats );
}


__fastcall TNullMPLDevice::~TNullMPLDevice()
{
}


bool TNullMPLDevice::Connect( const COMMS_CFG& portCfg )
{
    // Allow connect to always succeed
    return true;
}


bool TNullMPLDevice::Update( void )
{
    // Set the last packet received to now
    m_port.stats.tLastPkt = time( NULL );

    // Accept any pending timing params
    m_timeParams = m_pendingTimeParams;

    // No update required of Null device
    return true;
}


bool TNullMPLDevice::ShowLastRFCPkt( TStringList* pCaptions, TStringList* pValues )
{
    InitPropertyList( pCaptions, pValues, RFCFieldCaptions, NBR_RFC_FIELD_TYPES );

    return true;
}


bool TNullMPLDevice::GetLastRFCPkt ( TDateTime& tPktTime, DWORD& rfcCount, LOGGED_RFC_DATA& rfcPkt )
{
    tPktTime = Now();
    rfcCount = 0;

    ClearV2RFCPkt( rfcPkt );

    return false;
}


bool TNullMPLDevice::LaunchPlug( eCanisterNbr ePlug )
{
    // Ignored in the NULL device
    return true;
}


bool TNullMPLDevice::CleanCanister( eCanisterNbr eCan )
{
    // Ignored in the NULL device
    return true;
}


bool TNullMPLDevice::ActivateResetFlag( void )
{
    // Ignored in the NULL device
    return true;
}


UnicodeString TNullMPLDevice::GetDevStatus( void )
{
    return "N/A";
}


bool TNullMPLDevice::GetDevIsIdle( void )
{
    // Null device is always idle
    return true;
}


bool TNullMPLDevice::GetDevIsCalibrated( void )
{
    // Null device is never calibrated
    return false;
}


bool TNullMPLDevice::GetCalDataRaw( eMPLSUnit whichUnit, TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    return true;
}


bool TNullMPLDevice::GetCalDataFormatted( eMPLSUnit whichUnit, CALIBRATION_ITEM ciItem, String& sValue )
{
    sValue = "";
    return true;
}


bool TNullMPLDevice::GetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, CalFactorCaptions, NBR_CALIBRATION_ITEMS ) )
        return false;

    return true;
}


bool TNullMPLDevice::SetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions, TStringList* pValues )
{
    // Cannot set cal data for Null device
    return false;
}


bool TNullMPLDevice::GetDeviceBatteryInfo( eMPLSUnit whichUnit, BATTERY_INFO& battInfo )
{
    // Null devices have no battery info
    return false;
}


bool TNullMPLDevice::ReloadCalDataFromDevice( eMPLSUnit whichUnit )
{
    // Null device cannot reload cal data
    return false;
}


bool TNullMPLDevice::WriteCalDataToDevice( eMPLSUnit whichUnit )
{
    // Null device cannot write cal data to device
    return false;
}

