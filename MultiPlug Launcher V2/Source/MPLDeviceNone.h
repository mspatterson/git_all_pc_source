#ifndef MPLDeviceNoneH
#define MPLDeviceNoneH

    //
    // Class Declaration for 'null' MPL device
    //

    #include "MPLDeviceBaseClass.h"

    class TNullMPLDevice: public TMPLDevice {

      protected:

        virtual String GetDevStatus( void );
        virtual bool   GetDevIsIdle( void );
        virtual bool   GetDevIsCalibrated( void );

      public:

        __fastcall  TNullMPLDevice( void );
        __fastcall ~TNullMPLDevice();

        bool Connect( const COMMS_CFG& portCfg );

        bool Update( void );

        virtual bool ShowLastRFCPkt( TStringList* pCaptions, TStringList* pValues );
        virtual bool GetLastRFCPkt ( TDateTime& tPktTime, DWORD& rfcCount, LOGGED_RFC_DATA& rfcPkt );

        virtual bool LaunchPlug( eCanisterNbr ePlug );
        virtual bool CleanCanister( eCanisterNbr eCan );
        virtual bool ActivateResetFlag( void );

        bool GetCalDataRaw      ( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues );
        bool GetCalDataFormatted( eMPLSUnit whichUnit, CALIBRATION_ITEM ciItem, String& sValue       );
        bool GetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues );
        bool SetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues );

        bool ReloadCalDataFromDevice( eMPLSUnit whichUnit );
        bool WriteCalDataToDevice( eMPLSUnit whichUnit );

        virtual bool GetDeviceBatteryInfo( eMPLSUnit whichUnit, BATTERY_INFO& battInfo );

      private:

    };

#endif
