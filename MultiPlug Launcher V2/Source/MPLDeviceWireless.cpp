//
// Device class for MPL module
//

#include <vcl.h>
#pragma hdrstop

#include <math.hpp>

#include "MPLDeviceWireless.h"
#include "CUDPPort.h"
#include "ApplUtils.h"
#include "MPLRegistryInterface.h"

#pragma package(smart_init)


// The current over-the-air protocol and backplane protocol will not work
// well for downloaded MPLS configuration data. So in the current release
// of the software we skip the config download and run with default values.
// Likewise, we skip config uploads as well. If true config upload and
// download is implement in the future, uncomment the following #def
//
// NOTE! NOTE! NOTE! Because this feature was not implemented, #else code
// is in place for reference only. This code can't be tested, and therefore
// is for refernce only. Any implementation / testing / changes to the code
// IS NOT IN THE ORIGINAL SCOPE OF THE PROJECT!!!
//
// #def MPLS_HAS_CONFIG_SUPPORT


//
// Private Functions
//

static const DWORD RxBuffLen = 2048;

static const DWORD MSecsPerPktTick = 10;   // Each count in the pkt header equals this many msecs

static const ConsecutiveHWErrorLimit = 5;  // Max number of consecutive HW errors before an alert is generated


static float DoSpanOffsetCal( int rawValue, int offset, float span )
{
    // The rawValue can never be 0xFFFFFFFF in this implementation

    float fResult = 0.0;

    if( rawValue == (int)0xFFFFFFFF )
    {
        fResult = NaN;
    }
    else
    {
        try
        {
            fResult = (float)(rawValue - offset ) * span;
        }
        catch( ... )
        {
        }
    }

    return fResult;
}


static String IsolOutsToStr( bool bHaveUnit, BoolState bsValues[] )
{
    String sResult;

    if( bHaveUnit )
    {
        for( int iIsol = 0; iIsol < NBR_ISOL_OUTS_PER_TEC; iIsol++ )
        {
            String sState;

            switch( bsValues[iIsol] )
            {
                case eBS_Off: sState = ":Off ";   break;
                case eBS_On:  sState = ":On  ";   break;
                default:      sState = ":??? ";   break;
                ;
            }

            sResult += IntToStr( iIsol + 1 ) + sState;
        }
    }
    else
    {
        sResult = "1:--- 2:--- 3:--- 4:---";
    }

    return sResult;
}


static void DebugLog( const String& sDir, const BYTE* pHdr, const BYTE* pPayLoad )
{
#if 0
    TStringList* pOutStrings = new TStringList();

    try
    {
        const WTTTS_PKT_HDR* pPktHdr = (const WTTTS_PKT_HDR*)pHdr;

        String sPktHdr;

        if( pPayLoad != NULL )
            sPktHdr.printf( L"Header: 0x%02X %hu %hu %hu, Payload (hex): %02X %02X %02X %02X %02X %02X %02X %02X",
                            pPktHdr->pktType, pPktHdr->seqNbr, pPktHdr->dataLen, pPktHdr->timeStamp,
                            pPayLoad[0], pPayLoad[1], pPayLoad[2], pPayLoad[3], pPayLoad[4], pPayLoad[5], pPayLoad[6], pPayLoad[7] );
        else
            sPktHdr.printf( L"Header: 0x%02X %hu %hu %hu",
                            pPktHdr->pktType, pPktHdr->seqNbr, pPktHdr->dataLen, pPktHdr->timeStamp );

        sPktHdr = sDir + " " + UIntToStr( (UINT)GetTickCount() ) + " " + sPktHdr;

        String sLogName = "C:\\Users\\Ken\\Desktop\\PktHdrs.txt";

        if( FileExists( sLogName ) )
            pOutStrings->LoadFromFile( sLogName );

        pOutStrings->Add( sPktHdr );
        pOutStrings->SaveToFile( sLogName );
    }
    catch( ... )
    {
    }

    delete pOutStrings;
#endif
}


//
// Class Implementation
//

__fastcall TWirelessMPLDevice::TWirelessMPLDevice( void ) : TMPLDevice( MT_MPL )
{
    // Init state vars
    SetLinkState( LS_CLOSED );

    // Init rx control vars
    m_rxBuffer    = new BYTE[RxBuffLen];
    m_rxBuffCount = 0;

    // UDP control vars
    m_bUsingUDP   = false;    // True if we're using UDP comms
    m_sMPLDevAddr = "";       // If using UDP, this is the IP addr from which the last event was received
    m_wMPLDevPort = 0;        // If using UDP, this is the port from which the last event was received

    // Reset last RFC control struct
    ResetRFCControlStruct();

    // We don't create the plug hysteresis object until we know the current
    // state of the plug reading
    m_plugState = NULL;

    // Init battery averaging - averaging is hard-coded for now
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        m_batteryInfo[iUnit].fLastBattVolts = 0.0;
        m_batteryInfo[iUnit].iLastBattUsed  = 0;
        m_batteryInfo[iUnit].fValidThresh   = 0.0;   // Will be updated by RefreshSettings() call below
        m_batteryInfo[iUnit].fMinOperThresh = 0.0;   // Will be updated by RefreshSettings() call below
        m_batteryInfo[iUnit].fGoodThresh    = 0.0;   // Will be updated by RefreshSettings() call below
        m_batteryInfo[iUnit].avgBattVolts   = new TExpAverage( 100, 0 );
        m_batteryInfo[iUnit].avgBattUsed    = new TExpAverage( 100, 0 );
    }

    // Init command cache
    for( int iCachedItem = 0; iCachedItem < NBR_MPLS2_CMD_TYPES; iCachedItem++ )
        InitMPLCommandPkt( (MPLS2_CMD_TYPE)iCachedItem );

    // Init command Ack mechanism
    m_cmdRespWait.aCmd  = MCT_NO_COMMAND;
    m_cmdRespWait.state = eAS_Ready;

    // Setup the initialization script. Order is important here! First thing we
    // need is to determine the hardware config of the MPLS we're connected to
    m_ScriptSteps[ 0] = InitReqRevs;
    m_ScriptSteps[ 1] = InitWaitRevsResp;
    m_ScriptSteps[ 2] = InitSetRates;
    m_ScriptSteps[ 3] = InitWaitRatesAck;
    m_ScriptSteps[ 4] = InitStartTECParams;
    m_ScriptSteps[ 5] = InitSetTECParams;
    m_ScriptSteps[ 6] = InitSetDetParams;
    m_ScriptSteps[ 7] = InitWaitDetParamsAck;
    m_ScriptSteps[ 8] = InitReqCalData;
    m_ScriptSteps[ 9] = InitWaitCalData;
    m_ScriptSteps[10] = InitWaitSystemIdle;
    m_ScriptSteps[11] = InitSyncToHW;
    m_ScriptSteps[12] = NULL;

    m_scriptCtrl.pScriptSteps = m_ScriptSteps;
    m_scriptCtrl.eScriptState = eSSR_Done;
    m_scriptCtrl.iScriptIndex = 0;

    // Clear other device info
    ClearConfigurationData( eMU_MasterMPLS );
    ClearConfigurationData( eMU_SlaveMPLS );
    ClearHardwareInfo();

    // Read registry-based data
    RefreshSettings();
}


__fastcall TWirelessMPLDevice::~TWirelessMPLDevice()
{
    Disconnect();

    if( m_rxBuffer != NULL )
        delete [] m_rxBuffer;

    // Free battery averages
    delete m_batteryInfo[eMU_MasterMPLS].avgBattVolts;
    delete m_batteryInfo[eMU_MasterMPLS].avgBattUsed;
    delete m_batteryInfo[eMU_SlaveMPLS ].avgBattVolts;
    delete m_batteryInfo[eMU_SlaveMPLS ].avgBattUsed;
}


bool TWirelessMPLDevice::Connect( const COMMS_CFG& portCfg )
{
    // Reset our state first
    SetLinkState( LS_CLOSED );

    m_cmdRespWait.aCmd  = MCT_NO_COMMAND;
    m_cmdRespWait.state = eAS_Ready;

    // Let base class do its work
    if( !TMPLDevice::Connect( portCfg ) )
        return false;

    // Note if we're using UDP
    if( portCfg.portType == CT_UDP )
        m_bUsingUDP = true;

    // Once the physical connection is established, we go into a hunt
    // mode. In this mode, we are looking for any type of response
    // from the sub. This tells us we have the correct physical link.
    EnterHuntMode();

    return true;
}


bool TWirelessMPLDevice::Update( void )
{
    // Update our state machine here. First, if we have no port, there is
    // nothing to do
    if( m_linkState == LS_CLOSED )
        return false;

    // The nature of the protocol is such that we need to process any requests
    // or events from the device first.
    DWORD bytesRxd;

    if( m_bUsingUDP )
        bytesRxd = ((CUDPPort*)m_port.portObj)->RecvFrom( &(m_rxBuffer[m_rxBuffCount]), RxBuffLen - m_rxBuffCount, m_sMPLDevAddr, m_wMPLDevPort );
    else
        bytesRxd = m_port.portObj->CommRecv( &(m_rxBuffer[m_rxBuffCount]), RxBuffLen - m_rxBuffCount );

    if( bytesRxd > 0 )
    {
        m_lastRxByteTime = GetTickCount();

        IncStat( m_port.stats.bytesRecv, bytesRxd );

        m_rxBuffCount += bytesRxd;
    }
    else if( m_rxBuffCount > 0 )
    {
        // No data received - check if any bytes in the buffer have gone stale
        if( HaveTimeout( m_lastRxByteTime, 250 ) )
        {
            m_rxBuffCount = 0;

            IncStat( m_port.stats.pktFlushes );
        }
    }

    // After checking for received data, confirm the port is still open
    if( m_port.portObj->portLost )
    {
        // This is not good!
        m_portLost = true;
        Disconnect();

        SetLinkState( LS_CLOSED );

        m_cmdRespWait.aCmd  = MCT_NO_COMMAND;
        m_cmdRespWait.state = eAS_Ready;

        ResetAveraging();

        return false;
    }

    // Check if we've stopped receiving data. If so, need to reset our
    // state and go back to hunt mode
    switch( m_linkState )
    {
        case LS_CLOSED:
        case LS_HUNTING:
            // Reception not checked in these states
            break;

        default:
            // Reception checked in these states
            if( !DeviceIsRecving )
                EnterHuntMode();
                
            break;
    }

    // Have the parser check for any responses from the unit.
    // HaveMPLS2RespPkt() will shift good (and bad) bytes out
    // of the rx buffer, so note the number of bytes on entry
    // so that we know if there was a packet error
    bool bRFCPending = false;

    WTTTS_PKT_HDR    pktHdr;
    MPLS2_DATA_UNION pktData;

    DWORD dwPreParseBuffCount = m_rxBuffCount;

    if( !HaveMPLS2RespPkt( m_rxBuffer, m_rxBuffCount, pktHdr, pktData ) )
    {
        // We didn't get a packet. If the parser turfed some bytes,
        // it means there was a packet error (bad header or bad CRC)
        if( m_rxBuffCount != dwPreParseBuffCount )
            IncStat( m_port.stats.pktErrors );
    }
    else
    {
        IncStat( m_port.stats.respRecv );

        DebugLog( "Rx", (BYTE*)&pktHdr, (BYTE*)&pktData );

        m_port.stats.tLastPkt = time( NULL );
        m_lastRxPktTime       = GetTickCount();

        // If we are hunt mode, we want to wait for an RFC. No other
        // packets will be processed until we get an RFC. So check
        // for that first here.
        if( m_linkState == LS_HUNTING )
        {
            // If we're in the hunting state, the only thing that can get
            // us out is an RFC
            if( pktHdr.pktType == WTTTS_RESP_MPLS2_RFC )
            {
                // Start the initialization script
                SetLinkState( LS_INITING );

                // Signal to start the script
                m_scriptCtrl.eScriptState = eSSR_Running;
                m_scriptCtrl.iScriptIndex = 0;
            }
            else
            {
                // Reject this packet by setting its type to a bogus value
                pktHdr.pktType = WTTTS_CMD_NO_CMD;
            }
        }

        // Now process received packets
        if( pktHdr.pktType == WTTTS_RESP_MPLS2_ACK_PKT )
        {
            // We've received an ack. The only state this is valid in is
            // if we were expecting one.
            if( m_cmdRespWait.state == eAS_Waiting )
            {
                // If this is the ack we were waiting for, we are done.
                if( m_cmdRespWait.pktCmdByte == pktData.ackPkt.byCmdBeingAckd )
                {
                    m_cmdRespWait.lastResult = pktData.ackPkt.byCmdResult;
                    m_cmdRespWait.state      = eAS_Rcvd;
                }
            }
        }
        else if( pktHdr.pktType == WTTTS_RESP_MPLS2_VER_PKT )
        {
            // Save the version info
            if( pktData.verPkt.byUnitID == 1 )
            {
                m_bHaveDevInfo[eMU_MasterMPLS] = true;

                m_deviceInfo[eMC_MasterTEC].sSN     = DevSNToString( pktData.verPkt.tecSN );
                m_deviceInfo[eMC_MasterTEC].dwHWRev = pktData.verPkt.tecHWRev;
                m_deviceInfo[eMC_MasterTEC].dwFWRev = pktData.verPkt.tecFWRev;

                m_deviceInfo[eMC_MasterPIB].sSN     = "";   // PIB does not have a SN
                m_deviceInfo[eMC_MasterPIB].dwHWRev = pktData.verPkt.pibHWRev;
                m_deviceInfo[eMC_MasterPIB].dwFWRev = pktData.verPkt.pibFWRev;

                m_deviceInfo[eMC_PlugDet].sSN     = DevSNToString( pktData.verPkt.plugDetSN );
                m_deviceInfo[eMC_PlugDet].dwHWRev = pktData.verPkt.plugDetHWRev;
                m_deviceInfo[eMC_PlugDet].dwFWRev = pktData.verPkt.plugDetFWRev;

                m_tecInfo[eMU_MasterMPLS].sDevID = m_deviceInfo[eMC_MasterTEC].sSN;
            }
            else if( pktData.verPkt.byUnitID == 2 )
            {
                m_bHaveDevInfo[eMU_SlaveMPLS] = true;

                m_deviceInfo[eMC_SlaveTEC].sSN     = DevSNToString( pktData.verPkt.tecSN );
                m_deviceInfo[eMC_SlaveTEC].dwHWRev = pktData.verPkt.tecHWRev;
                m_deviceInfo[eMC_SlaveTEC].dwFWRev = pktData.verPkt.tecFWRev;

                m_deviceInfo[eMC_SlavePIB].sSN     = "";   // PIB does not have a SN
                m_deviceInfo[eMC_SlavePIB].dwHWRev = pktData.verPkt.pibHWRev;
                m_deviceInfo[eMC_SlavePIB].dwFWRev = pktData.verPkt.pibFWRev;

                m_tecInfo[eMU_SlaveMPLS].sDevID = m_deviceInfo[eMC_SlaveTEC].sSN;
            }
        }
        else if( pktHdr.pktType == WTTTS_RESP_MPLS2_CFG_DATA )
        {
            // This pakcet is valid only in certain states. If downloading
            // cfg data, save the data and mark the page as such. If
            // uploading, this is an ack to a previous 'set cfg' command
            // so verify the response.
            if( ( m_linkState == LS_CFG_DOWNLOAD ) || ( m_linkState == LS_INITING ) )
            {
                // Isolate the page number from the MPLS unit flag
                BYTE      byPageNbr =   pktData.cfgData.pageNbr & 0x7F;
                eMPLSUnit eUnit     = ( pktData.cfgData.pageNbr & 0x80 ) == 0 ? eMU_MasterMPLS : eMU_SlaveMPLS;

                if( byPageNbr < NBR_WTTTS_CFG_PAGES )
                {
                    m_cfgPgStatus[eUnit][byPageNbr].havePage = true;

                    int pageOffset = byPageNbr * NBR_BYTES_PER_WTTTS_PAGE;

                    memcpy( &( m_rawCfgData[eUnit][pageOffset] ), pktData.cfgData.pageData, NBR_BYTES_PER_WTTTS_PAGE );
                }
            }
            else if( m_linkState == LS_CFG_UPLOAD )
            {
                // The MPLS will have echoed back in response to a previous
                // 'set' packet command. If the values echoed back match
                // the values in the table, then that item has been
                // successfully uploaded. First, check the response code.
                BYTE      byPageNbr =   pktData.cfgData.pageNbr & 0x7F;
                eMPLSUnit eUnit     = ( pktData.cfgData.pageNbr & 0x80 ) == 0 ? eMU_MasterMPLS : eMU_SlaveMPLS;

                if( pktData.cfgData.result != WTTTS_PG_RESULT_SUCCESS )
                {
                    // MPLS is indicating we cannot download this page.
                    // Clear the download pending bit and move on.
                    m_cfgPgStatus[eUnit][byPageNbr].setPending = false;
                }
                else if( pktData.cfgData.pageNbr < NBR_WTTTS_CFG_PAGES )
                {
                    // MPLS liked the page number. If our cached data matches
                    // the data echoed back, this page has been updated.
                    int   pageOffset = byPageNbr * NBR_BYTES_PER_WTTTS_PAGE;
                    BYTE* pgPointer  = &( m_rawCfgData[eUnit][pageOffset] );

                    if( memcmp( pgPointer, pktData.cfgData.pageData, NBR_BYTES_PER_WTTTS_PAGE ) == 0 )
                        m_cfgPgStatus[eUnit][byPageNbr].setPending = false;
                }
            }
        }
        else if( pktHdr.pktType == WTTTS_RESP_MPLS2_RFC )
        {
            // The MPL has requested a command from us. If we have
            // none pending, send a 'no command' response back. First,
            // it is possible for duplicate RFC packets to be received.
            // Ignore those.
            if( m_lastRFC.seqNbr != pktHdr.seqNbr )
            {
                m_lastRFC.seqNbr    = pktHdr.seqNbr;
                m_lastRFC.dwRFCTick = GetTickCount();
                m_lastRFC.tRFCTime  = Now();

                // Remember the last state of the flag-in bit. We'll raise an
                // event if that changes
                bool bOldFlagActive = m_lastRFC.rfcPkt.flagInActive;

                CreateGenericRFC( m_lastRFC.rfcPkt, pktData.rfcPkt );

                m_lastRFC.rawRFCPkt = pktData.rfcPkt;
                m_lastRFC.rfcCount++;

                LogRFCPkt( m_lastRFC.rfcPkt );

                // Only perform the following updates if the system is in an
                // idle (ready) state
                if( m_linkState == LS_IDLE )
                {
                    UpdateCommsErrors();
                    UpdateAveraging();
                    UpdateBatteryVolts( eMU_MasterMPLS, m_lastRFC.rfcPkt.tecState[eMU_MasterMPLS].solActive != 0 );
                    UpdateBatteryVolts( eMU_SlaveMPLS,  m_lastRFC.rfcPkt.tecState[eMU_SlaveMPLS ].solActive != 0 );
                    CheckForPlug();
                    UpdateSolenoids();
                    UpdateIsolOuts();

                    if( m_lastRFC.rfcPkt.flagInActive && !bOldFlagActive )
                        DoOnFlagTripped();
                }

                DoOnRFCRecvd();
            }

            bRFCPending = true;
        }
    }

    // If a RFC is pending, check our command state. If we were waiting on
    // an ack, we didn't receive it. Need to resend the command.
    if( bRFCPending )
    {
        if( ResendLastCmd() )
            bRFCPending = false;
    }

    // Now update the state machine
    switch( m_linkState )
    {
        case LS_HUNTING:
            // We are currently waiting to receive a command from the sub.
            // Packet reception is captured in receive processing above,
            // so there is nothing to do here.
            break;

        case LS_INITING:
            // We've connected to a device and are configuring it.
            // Execute the config script.
            if( m_scriptCtrl.eScriptState == eSSR_Running )
            {
                // Check if we're done
                if( m_scriptCtrl.pScriptSteps[m_scriptCtrl.iScriptIndex] == NULL )
                {
                    // We're done, and with success, set the script state.
                    // We'll exit the init'ing state on the next pass through
                    m_scriptCtrl.eScriptState = eSSR_Done;
                }
                else
                {
                     // Still executing script steps
                     switch( m_scriptCtrl.pScriptSteps[m_scriptCtrl.iScriptIndex]( bRFCPending, m_scriptCtrl.xbParam ) )
                     {
                         case eSSR_Running:
                             // Stay in this state for now
                             break;

                         case eSSR_Done:
                             // Advance to next script step
                             m_scriptCtrl.iScriptIndex++;
                             break;

                         default:
                             // Script has failed!
                             m_scriptCtrl.eScriptState = eSSR_Failed;
                             break;
                     }
                }
            }
            else if( m_scriptCtrl.eScriptState == eSSR_Done )
            {
                // Script successfully completed.
                SetLinkState( LS_IDLE );
                DoOnDeviceConnected();
            }
            else
            {
                // Script has failed. Re-enter the hunting state, but let the
                // client know so they can decide what to do
                DoOnInitFailed();
                EnterHuntMode();
            }

            break;

        case LS_IDLE:
            // If we have a RFC timeout, could have lost the link. Could also
            // scale back RFC interval based on the last time we got a command
            // from client modules.

            // If a RFC is pending, check that the time params are up to date. If they
            // are not, a command will be sent to update those first.
            if( bRFCPending )
                bRFCPending = TimeParamsGood();

            // Next check if we need to send an activate solenoid command
            if( bRFCPending )
                bRFCPending = SolenoidsUpToDate();

            // Check if any isolated outputs need to be updated
            if( bRFCPending )
                bRFCPending = IsolOutsUpToDate();

            break;

        case LS_CFG_DOWNLOAD:
            // We are downloading the calibration table. Check if we are
            // done - switch to idle if so.
            if( bRFCPending )
            {
                // Check to see if we need more cal data. Passing in true
                // will send a request if necessary.
                bRFCPending = CheckCalTableState( true );

                // If the RFC is pending, it means all pages have been downloaded.
                // Send the 'get version' command, and also put the link into the
                // idle state
                if( bRFCPending )
                {
                    // Calibration data all downloaded. Check the version of the
                    // cal data, and update if necessary.
                    CheckCalDataVersion( eMU_MasterMPLS );
                    CheckCalDataVersion( eMU_SlaveMPLS );

                    // Can now go to idle state
                    SetLinkState( LS_IDLE );
                }
            }
            break;

        case LS_CFG_UPLOAD:
            // We are uploading the calibration table. Check if we are
            // done - switch to idle if so.
            if( bRFCPending )
            {
                // Scan the table for any pending entries. If there is
                // one, request it. Otherwise, switch to the idle state.
                for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
                {
                    // Skip the slave unit if we don't have one
                    if( !HaveSlaveTEC && ( iUnit == eMU_SlaveMPLS ) )
                        continue;

                    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
                    {
                        if( m_cfgPgStatus[iUnit][iPg].setPending )
                        {
                            MPLS2_DATA_UNION pktPayload;

                            pktPayload.cfgData.pageNbr = iPg;
                            pktPayload.cfgData.result  = 0;

                            if( (eMPLSUnit)iUnit == eMU_SlaveMPLS )
                                pktPayload.cfgData.pageNbr |= 0x80;

                            int   pageOffset = iPg * NBR_BYTES_PER_WTTTS_PAGE;
                            BYTE* pgPointer  = &( m_rawCfgData[iUnit][pageOffset] );

                            memcpy( pktPayload.cfgData.pageData, pgPointer, NBR_BYTES_PER_WTTTS_PAGE );

                            SendMPLS2Cmd( MCT_SET_CFG_PAGE, &pktPayload );

                            bRFCPending = false;

                            break;
                        }
                    }

                    // If we've sent a command, break out of the outer loop too
                    if( !bRFCPending )
                        break;
                }

                // If the RFC is pending, it means all pages have been set. Switch
                // to the idle state.
                if( bRFCPending )
                {
                    SetLinkState( LS_IDLE );
                }
            }
            break;
    }

    // If at this point the RFC is still pending, send a 'no command' response
    if( bRFCPending )
        SendMPLS2Cmd( MCT_NO_COMMAND );

    // Finally, data values are only valid in certain states. In any other
    // state the averaged data values must be reset. Don't update the averages
    // here (that happens above with the reception of each packet). Only
    // reset the averaging if required.
    switch( m_linkState )
    {
        case LS_IDLE:
            break;

        default:

            // In any other state, data from the tool is not valid.
            // Reset the data averages
            ResetAveraging();

            for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
            {
                m_batteryInfo[iUnit].avgBattVolts->Reset();
                m_batteryInfo[iUnit].avgBattUsed->Reset();
            }

            break;
    }

    return true;
}


eScriptStepResult TWirelessMPLDevice::InitReqRevs( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // We're waiting to send the get revs command in this state
    if( !bRFCPending )
        return eSSR_Running;

    // Clear the 'have received pkt' flags
    m_bHaveDevInfo[eMU_MasterMPLS] = false;
    m_bHaveDevInfo[eMU_SlaveMPLS ] = false;

    // Request master info first
    MPLS2_DATA_UNION cmdPkt = { 0 };

    // Request master info first
    cmdPkt.queryVerPkt.byUnitID = 1;

    if( SendMPLS2Cmd( MCT_REQUEST_VER_INFO, &cmdPkt ) )
    {
        // On success, clear the RFC pending flag. This script step
        // uses the xbParams as a retry counter
        bRFCPending   = false;
        xbParam.iData = 1;      // Retry counter

        return eSSR_Done;
    }
    else
    {
        return eSSR_Failed;
    }
}


eScriptStepResult TWirelessMPLDevice::InitWaitRevsResp( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // First, check if we've received the version info. If we have,
    // the firmware rev will not be zero
    if( m_bHaveDevInfo[eMU_MasterMPLS] && m_bHaveDevInfo[eMU_SlaveMPLS] )
        return eSSR_Done;

    // Still waiting on a response. Wait for a RFC
    if( !bRFCPending )
        return eSSR_Running;

    // Have RFC. Check for retry failure. Note that this failure count
    // is a cumulative count of 3 attempts for each of the two commands
    if( xbParam.iData > 6 )
        return eSSR_Failed;

    // Okay to resend
    MPLS2_DATA_UNION cmdPkt = { 0 };

    if( !m_bHaveDevInfo[eMU_MasterMPLS] )
        cmdPkt.queryVerPkt.byUnitID = 1;
    else
        cmdPkt.queryVerPkt.byUnitID = 2;

    if( SendMPLS2Cmd( MCT_REQUEST_VER_INFO, &cmdPkt ) )
    {
        // On success, clear the RFC pending flag. This stage uses the
        // dwParam as a retry counter since there is no ack for this
        // command.
        bRFCPending = false;
        xbParam.iData++;
        
        return eSSR_Running;
    }
    else
    {
        return eSSR_Failed;
    }
}


eScriptStepResult TWirelessMPLDevice::InitSetRates( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // We're waiting to send the set rates command to the device.
    if( !bRFCPending )
        return eSSR_Running;

    MPLS2_DATA_UNION pktUnion = { 0 };

    pktUnion.ratePkt.rfcRate     = m_timeParams.rfcRate;
    pktUnion.ratePkt.rfcTimeout  = m_timeParams.rfcTimeout;
    pktUnion.ratePkt.pairTimeout = m_timeParams.pairingTimeout;

    if( SendMPLS2Cmd( MCT_SET_RATE_CMD, &pktUnion ) )
    {
        // On success, clear the RFC pending flag. dwParam is not used
        // by this step, so just clear it too.
        bRFCPending   = false;
        xbParam.iData = 0;
        
        return eSSR_Done;
    }
    else
    {
        return eSSR_Failed;
    }
}


eScriptStepResult TWirelessMPLDevice::InitWaitRatesAck( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // We're waiting for an Ack for the set rates command. Wait for the
    // ack cycle to finish.
    if( m_cmdRespWait.state == eAS_Waiting )
        return eSSR_Running;

    // Ack cycle complete - check result
    if( m_cmdRespWait.state == eAS_Rcvd )
        return eSSR_Done;
    else
        return eSSR_Failed;
}


eScriptStepResult TWirelessMPLDevice::InitStartTECParams( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // This stage involves sending a number of Ack'd commands to the
    // MPLS. dwParam is used to indicate what step we're on. This
    // script step just inits that value
    xbParam.dwData = 0;

    // Both the master and slave TECs boot up with the correct TEC params
    // by default. This script step is a place holder in case we want to
    // change those in the future.
    return eSSR_Done;
}


eScriptStepResult TWirelessMPLDevice::InitSetTECParams( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // Both the master and slave TECs boot up with the correct TEC params
    // by default. This script step is a place holder in case we want to
    // change those in the future.
    return eSSR_Done;
}


eScriptStepResult TWirelessMPLDevice::InitSetDetParams( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // In this release, we don't send any plug detector params.
    // It runs in its power-up default mode. This script step
    // is a place holder in case we need to change that in the
    // future.
    return eSSR_Done;
}


eScriptStepResult TWirelessMPLDevice::InitWaitDetParamsAck( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // In this release, we don't send any plug detector params.
    // It runs in its power-up default mode
    return eSSR_Done;
}

eScriptStepResult TWirelessMPLDevice::InitReqCalData( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // First, clear any existing cal data. This will also cause default
    // cal data to be populated
    ClearConfigurationData( eMU_MasterMPLS );
    ClearConfigurationData( eMU_SlaveMPLS );

    #ifdef MPLS_HAS_CONFIG_SUPPORT

        // Nothing else to do... on next RFC the first request will go out

    #else

        // Fake out that we have all data
        for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
        {
            for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
                m_cfgPgStatus[iUnit][iPg].havePage = true;
        }

    #endif

    return eSSR_Done;
}


eScriptStepResult TWirelessMPLDevice::InitWaitCalData( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // Wait for a RFC to have been received
    if( !bRFCPending )
        return eSSR_Running;

    // Check the cal table state. If it is good (returns true), then we're done
    if( CheckCalTableState( true /* request pg if necessary */ ) )
        return eSSR_Done;

    // Still have data to download and a command will have been sent
    bRFCPending = false;

    return eSSR_Running;
}


eScriptStepResult TWirelessMPLDevice::InitWaitSystemIdle( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // Wait for a RFC
    if( !bRFCPending )
        return eSSR_Running;

    // We've received an RFC. Check that all relays are idle. In future releases,
    // may want to check that isolated outputs are in a known state too.

    // NOTE! If a solenoid is active, we just wait for it to timeout instead of
    // sending a 'turn off' command. This is because shortening a solenoid's
    // on cycle could have a bad impact on what had previously been started.
    if( m_lastRFC.rfcPkt.tecState[eMU_MasterMPLS].solActive != 0 )
        return eSSR_Running;

    if( HaveSlavePIB )
    {
        if( m_lastRFC.rfcPkt.tecState[eMU_SlaveMPLS].solActive != 0 )
            return eSSR_Running;
    }

    // Fall through means no relays are active
    return eSSR_Done;
}


eScriptStepResult TWirelessMPLDevice::InitSyncToHW( bool& bRFCPending, XFER_BUFFER& xbParam )
{
    // Wait for an RFC, then sync the state of the isolated outputs and relays
    // to the hardware's current state
    if( !bRFCPending )
        return eSSR_Running;

    // RFC received - update our state
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        m_tecInfo[iUnit].activeSol  = m_lastRFC.rfcPkt.tecState[iUnit].solActive;
        m_tecInfo[iUnit].pendingSol = -1;
        m_tecInfo[iUnit].bcSolCmd   = eBC_NoCmd;

        for( int iIsolOut = 0; iIsolOut < NBR_ISOL_OUTS_PER_TEC; iIsolOut++ )
        {
            m_tecInfo[iUnit].pibIsolOutState[iIsolOut] = m_lastRFC.rfcPkt.tecState[iUnit].eIsolOutState[iIsolOut];
            m_tecInfo[iUnit].pibIsolOutCmd  [iIsolOut] = eBC_NoCmd;
        }
    }

    return eSSR_Done;
}


void TWirelessMPLDevice::SetLinkState( LINK_STATE newState )
{
    if( m_linkState != newState )
        DoOnLinkStateChange();

    m_linkState = newState;
}


void TWirelessMPLDevice::EnterHuntMode( void )
{
    // Performs init necessary to enter hunt mode
    SetLinkState( LS_HUNTING );

    m_cmdRespWait.aCmd  = MCT_NO_COMMAND;
    m_cmdRespWait.state = eAS_Ready;

    ResetAveraging();

    ClearConfigurationData( eMU_MasterMPLS );
    ClearConfigurationData( eMU_SlaveMPLS );
    ClearHardwareInfo();
    ResetRFCControlStruct();
}


void TWirelessMPLDevice::RefreshSettings( void )
{
    // Called to refresh any registry-based settings. Causes those settings
    // to be re-read from the registry. Do base class refresh first,
    TMPLDevice::RefreshSettings();

    BATT_THRESH_VALS battThresh;
    GetBatteryThresholds( battThresh );

    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        m_batteryInfo[iUnit].fValidThresh   = battThresh.fReadingValidThresh;
        m_batteryInfo[iUnit].fMinOperThresh = battThresh.fMinOperValue;
        m_batteryInfo[iUnit].fGoodThresh    = battThresh.fGoodThresh;
    }
}


bool TWirelessMPLDevice::ShowLastRFCPkt( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, RFCFieldCaptions, NBR_RFC_FIELD_TYPES ) )
        return false;

    // Always show the device state
    pValues->Strings[RFT_DEV_STATE] = GetDevStatus();

    // Always show comms stats
    pValues->Strings[RFT_BYTES_RXD]    = UIntToStr( (UINT)m_port.stats.bytesRecv );
    pValues->Strings[RFT_GOOD_PKTS]    = UIntToStr( (UINT)m_port.stats.respRecv );
    pValues->Strings[RFT_RXD_FLUSHES]  = UIntToStr( (UINT)m_port.stats.pktFlushes );
    pValues->Strings[RFT_RXD_PKT_ERRS] = UIntToStr( (UINT)m_port.stats.pktErrors );

    // Make sure we have a RFC packet
    if( ( m_linkState != LS_IDLE ) || ( m_lastRFC.rfcCount == 0 ) )
        return true;

    // Have a RFC - populate the value string list
    pValues->Strings[RFT_LAST_RFC_TIME]   = m_lastRFC.tRFCTime.TimeString();
    pValues->Strings[RFT_SYS_STATUS_BITS] = "0x" + IntToHex( (int)m_lastRFC.rfcPkt.systemStatusBits, 4 );
    pValues->Strings[RFT_TEMPERATURE]     = FloatToStrF( m_lastRFC.rfcPkt.temp, ffFixed, 7, 1 );
    pValues->Strings[RFT_RPM]             = IntToStr( m_lastRFC.rfcPkt.rpm );
    pValues->Strings[RFT_FLAG_DEPLOYED]   = m_lastRFC.rfcPkt.flagInActive ? String( "Tripped" ) : String( "Reset" );
    pValues->Strings[RFT_RFC_RATE]        = UIntToStr( (UINT)m_timeParams.rfcRate )        + " msecs";
    pValues->Strings[RFT_RFC_TIMEOUT]     = UIntToStr( (UINT)m_timeParams.rfcTimeout )     + " msecs";
    pValues->Strings[RFT_PAIR_TIMEOUT]    = UIntToStr( (UINT)m_timeParams.pairingTimeout ) + " secs";
    pValues->Strings[RFT_LAUNCH_TIME]     = IntToStr( (int)( m_solOnTimeLaunch / 1000 ) );
    pValues->Strings[RFT_CLEAN_TIME]      = IntToStr( (int)( m_solOnTimeClean  / 1000 ) );
    pValues->Strings[RFT_FLAG_RST_TIME]   = IntToStr( (int)( m_solOnTimeReset  / 1000 ) );

    WORD wLastReset = m_lastRFC.rfcPkt.systemStatusBits & SYS_STATUS_FIELD_PWR_UP;

    switch( wLastReset )
    {
        case WDRT_POWER_UP :   pValues->Strings[RFT_LAST_RESET] = "Power-up";   break;
        case WDRT_WATCH_DOG:   pValues->Strings[RFT_LAST_RESET] = "WDT";        break;
        case WDRT_BROWN_OUT:   pValues->Strings[RFT_LAST_RESET] = "Brown out";  break;
        default:               pValues->Strings[RFT_LAST_RESET] = "Type " + IntToStr( (int)wLastReset );  break;
    }

    // Determine if there were any comm errors
    String sCommErrors;

    if( m_lastRFC.rfcPkt.systemStatusBits & SYS_STATUS_FIELD_SLAVE_TEC_ERR )
       sCommErrors = sCommErrors + "STEC ";

    if( m_lastRFC.rfcPkt.systemStatusBits & SYS_STATUS_FIELD_SLAVE_PIB_ERR )
       sCommErrors = sCommErrors + "SPIB ";

    if( m_lastRFC.rfcPkt.systemStatusBits & SYS_STATUS_FIELD_MASTER_PIB_ERR )
       sCommErrors = sCommErrors + "MPIB ";

    if( m_lastRFC.rfcPkt.systemStatusBits & SYS_STATUS_FIELD_SLAVE_PD_ERR )
       sCommErrors = sCommErrors + "PDET ";

    if( sCommErrors.IsEmpty() )
        sCommErrors = "(none)";

    pValues->Strings[RFT_COMM_ERRORS] = sCommErrors;

    // Populate Master TEC data
    pValues->Strings[RFT_MASTER_BATT_VOLTS]      = FloatToStrF( m_lastRFC.rfcPkt.tecState[eMU_MasterMPLS].battVolts, ffFixed, 7, 3 );
    pValues->Strings[RFT_MASTER_BATT_TYPE]       = GetBattTypeText( (BATTERY_TYPE)( m_lastRFC.rfcPkt.tecState[eMU_MasterMPLS].battType ) );
    pValues->Strings[RFT_MASTER_TANK_PRESS]      = FloatToStrF( m_lastRFC.rfcPkt.tecState[eMU_MasterMPLS].tankPress, ffFixed, 7, 0 );
    pValues->Strings[RFT_MASTER_REG_PRESS]       = FloatToStrF( m_lastRFC.rfcPkt.tecState[eMU_MasterMPLS].regPress,  ffFixed, 7, 0 );
    pValues->Strings[RFT_MASTER_SOL_STATE]       = IntToStr( m_lastRFC.rfcPkt.tecState[eMU_MasterMPLS].solActive );
    pValues->Strings[RFT_MASTER_SOL_CURRENT]     = IntToStr( m_lastRFC.rfcPkt.tecState[eMU_MasterMPLS].solCurrent );
    pValues->Strings[RFT_MASTER_ISOL_OUTS_STATE] = IsolOutsToStr( true, m_lastRFC.rfcPkt.tecState[eMU_MasterMPLS].eIsolOutState );

    pValues->Strings[RFT_MASTER_TEC_SN] = m_deviceInfo[eMC_MasterTEC].sSN;
    pValues->Strings[RFT_MASTER_TEC_FW] = "0x" + IntToHex( (int)m_deviceInfo[eMC_MasterTEC].dwFWRev, 8 );
    pValues->Strings[RFT_MASTER_TEC_HW] = "0x" + IntToHex( (int)m_deviceInfo[eMC_MasterTEC].dwHWRev, 8 );

    pValues->Strings[RFT_MASTER_PIB_SN] = "N/A";
    pValues->Strings[RFT_MASTER_PIB_FW] = "0x" + IntToHex( (int)m_deviceInfo[eMC_MasterPIB].dwFWRev, 8 );
    pValues->Strings[RFT_MASTER_PIB_HW] = "0x" + IntToHex( (int)m_deviceInfo[eMC_MasterPIB].dwHWRev, 8 );

    if( HaveSlaveTEC )
    {
        pValues->Strings[RFT_SLAVE_BATT_VOLTS]      = FloatToStrF( m_lastRFC.rfcPkt.tecState[eMU_SlaveMPLS].battVolts, ffFixed, 7, 3 );
        pValues->Strings[RFT_SLAVE_BATT_TYPE]       = GetBattTypeText( (BATTERY_TYPE)( m_lastRFC.rfcPkt.tecState[eMU_SlaveMPLS].battType ) );
        pValues->Strings[RFT_SLAVE_TANK_PRESS]      = FloatToStrF( m_lastRFC.rfcPkt.tecState[eMU_SlaveMPLS].tankPress, ffFixed, 7, 0 );
        pValues->Strings[RFT_SLAVE_REG_PRESS]       = FloatToStrF( m_lastRFC.rfcPkt.tecState[eMU_SlaveMPLS].regPress,  ffFixed, 7, 0 );
        pValues->Strings[RFT_SLAVE_SOL_STATE]       = IntToStr( m_lastRFC.rfcPkt.tecState[eMU_SlaveMPLS].solActive );
        pValues->Strings[RFT_SLAVE_SOL_CURRENT]     = IntToStr( m_lastRFC.rfcPkt.tecState[eMU_SlaveMPLS].solCurrent );
        pValues->Strings[RFT_SLAVE_ISOL_OUTS_STATE] = IsolOutsToStr( true, m_lastRFC.rfcPkt.tecState[eMU_SlaveMPLS].eIsolOutState );
    }
    else
    {
        pValues->Strings[RFT_SLAVE_BATT_VOLTS]      = "";
        pValues->Strings[RFT_SLAVE_BATT_TYPE]       = "";
        pValues->Strings[RFT_SLAVE_TANK_PRESS]      = "";
        pValues->Strings[RFT_SLAVE_REG_PRESS]       = "";
        pValues->Strings[RFT_SLAVE_SOL_STATE]       = "";
        pValues->Strings[RFT_SLAVE_SOL_CURRENT]     = "";
        pValues->Strings[RFT_SLAVE_ISOL_OUTS_STATE] = "";
    }

    pValues->Strings[RFT_SLAVE_TEC_SN] = m_deviceInfo[eMC_SlaveTEC].sSN;
    pValues->Strings[RFT_SLAVE_TEC_FW] = "0x" + IntToHex( (int)m_deviceInfo[eMC_SlaveTEC].dwFWRev, 8 );
    pValues->Strings[RFT_SLAVE_TEC_HW] = "0x" + IntToHex( (int)m_deviceInfo[eMC_SlaveTEC].dwHWRev, 8 );

    pValues->Strings[RFT_SLAVE_PIB_SN] = "N/A";
    pValues->Strings[RFT_SLAVE_PIB_FW] = "0x" + IntToHex( (int)m_deviceInfo[eMC_SlavePIB].dwFWRev, 8 );
    pValues->Strings[RFT_SLAVE_PIB_HW] = "0x" + IntToHex( (int)m_deviceInfo[eMC_SlavePIB].dwHWRev, 8 );

    if( HavePlugDet )
    {
        pValues->Strings[RFT_DET_RATE]   = "Firmware Default";
        pValues->Strings[RFT_DET_AVGING] = "Firmware Default";
    }
    else
    {
        pValues->Strings[RFT_DET_RATE]   = "";
        pValues->Strings[RFT_DET_AVGING] = "";
    }

    pValues->Strings[RFT_DET_SN] = m_deviceInfo[eMC_PlugDet].sSN;
    pValues->Strings[RFT_DET_FW] = "0x" + IntToHex( (int)m_deviceInfo[eMC_PlugDet].dwFWRev, 8 );
    pValues->Strings[RFT_DET_HW] = "0x" + IntToHex( (int)m_deviceInfo[eMC_PlugDet].dwHWRev, 8 );

    if( m_rfcLogFileName.IsEmpty() )
        pValues->Strings[RFT_LOG_FILE_NAME] = "Disabled";
    else
        pValues->Strings[RFT_LOG_FILE_NAME] = m_rfcLogFileName;

    return true;
}


bool TWirelessMPLDevice::GetLastRFCPkt( TDateTime& tPktTime, DWORD& rfcCount, LOGGED_RFC_DATA& rfcPkt )
{
    if( ( m_linkState == LS_IDLE ) && ( m_lastRFC.rfcCount != 0 ) )
    {
        tPktTime = m_lastRFC.tRFCTime;
        rfcCount = m_lastRFC.rfcCount;
        rfcPkt   = m_lastRFC.rfcPkt;

        return true;
    }

    // Fall through means we have no packet
    tPktTime.Val = 0.0;
    rfcCount     = 0;

    memset( &rfcPkt, 0, sizeof( LOGGED_RFC_DATA ) );

    return false;
}


void TWirelessMPLDevice::InitMPLCommandPkt( MPLS2_CMD_TYPE cmdType )
{
    // Clear the memory area first
    memset( &( m_cachedCmds[cmdType] ), 0, sizeof( CACHED_CMD_PKT ) );

    // Determine the packet type byte, and payload length (if any)
    BYTE* byBuff = m_cachedCmds[cmdType].byTxData;

    WTTTS_PKT_HDR* pHdr = (WTTTS_PKT_HDR*)byBuff;

    // Setup header defaults
    pHdr->pktHdr    = MPL_HDR_TX;
    pHdr->pktType   = 0;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = 0;

    // Now initialize specific commands
    bool bExpectAck = false;

    switch( cmdType )
    {
        case MCT_NO_COMMAND:
            pHdr->pktType = WTTTS_CMD_NO_CMD;
            pHdr->dataLen = 0;
            break;

        case MCT_SET_RATE_CMD:
            pHdr->pktType = WTTTS_CMD_SET_RATE;
            pHdr->dataLen = sizeof( MPLS2_RATES_PKT);
            bExpectAck    = true;
            break;

        case MCT_REQUEST_VER_INFO:
            pHdr->pktType = WTTTS_CMD_MPLS2_QUERY_VER;
            pHdr->dataLen = sizeof( MPLS2_QUERY_VER_PKT );
            break;

        case MCT_GET_CFG_PAGE:
            pHdr->pktType = WTTTS_CMD_GET_CFG_DATA;
            pHdr->dataLen = sizeof( WTTTS_CFG_ITEM );
            break;

        case MCT_SET_CFG_PAGE:
            pHdr->pktType = WTTTS_CMD_SET_CFG_DATA;
            pHdr->dataLen = sizeof( WTTTS_CFG_DATA );
            break;

        case MCT_SET_RF_CHANNEL:
            pHdr->pktType = WTTTS_CMD_SET_RF_CHANNEL;
            pHdr->dataLen = sizeof( WTTTS_SET_CHAN_PKT );
            bExpectAck    = true;
            break;

        case MCT_MARK_CHAN_IN_USE:
            pHdr->pktType = WTTTS_CMD_MARK_CHAN_IN_USE;
            pHdr->dataLen = 0;
            break;

        case MCT_SET_PIB_OUTS:
            pHdr->pktType = WTTTS_CMD_MPLS2_SET_PIB_OUT;
            pHdr->dataLen = sizeof( MPLS2_PIB_OUTS_PKT );
            bExpectAck    = true;
            break;

        case MCT_SET_TEC_PARAMS:
            pHdr->pktType = WTTTS_CMD_MPLS2_SET_TEC_PAR;
            pHdr->dataLen = sizeof( MPLS2_TEC_PARAMS_PKT );
            bExpectAck    = true;
            break;

        default:
            // Don't know this command!
            throw( "TWirelessMPLDevice::InitMPLCommandPkt() - invalid MPL_CMD_TYPE enum" );
    }

    // If we don't need a payload, can finish initializing the command.
    // Otherwise, we have to finish the command each time it is sent.
    m_cachedCmds[cmdType].byCmdNbr = pHdr->pktType;
    m_cachedCmds[cmdType].needAck  = bExpectAck;

    if( pHdr->dataLen == 0 )
    {
        m_cachedCmds[cmdType].needsPayload = false;
        m_cachedCmds[cmdType].cmdLen       = MIN_MPL_PKT_LEN;

        BYTE byCRC = CalcWTTTSChecksum( byBuff, sizeof( WTTTS_PKT_HDR ) );

        byBuff[ sizeof( WTTTS_PKT_HDR ) ] = byCRC;
    }
    else
    {
        m_cachedCmds[cmdType].needsPayload = true;
        m_cachedCmds[cmdType].cmdLen       = MIN_MPL_PKT_LEN + pHdr->dataLen;
    }
}


bool TWirelessMPLDevice::SendMPLS2Cmd( MPLS2_CMD_TYPE aCmd, const MPLS2_DATA_UNION* pPayload )
{
    if( !IsConnected )
        return false;

    // If the command needs a payload, add it on
    if( m_cachedCmds[aCmd].needsPayload )
    {
        if( pPayload == NULL )
            return false;

        BYTE* txBuff   = m_cachedCmds[aCmd].byTxData;
        int   crcCount = m_cachedCmds[aCmd].cmdLen - 1;

        MPLS2_DATA_UNION* pData = (MPLS2_DATA_UNION*)&( txBuff[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pData, pPayload, sizeof( MPLS2_DATA_UNION ) );

        txBuff[crcCount] = CalcWTTTSChecksum( txBuff, crcCount );
    }

    BYTE* pBuff       = m_cachedCmds[aCmd].byTxData;
    DWORD bytesToSend = m_cachedCmds[aCmd].cmdLen;

    DWORD bytesSent = 0;

    // When sending the data, if sending on UDP we want to use a 'send to' command
    if( m_bUsingUDP )
        bytesSent = ((CUDPPort*)m_port.portObj)->SendTo( pBuff, bytesToSend, m_sMPLDevAddr, m_wMPLDevPort );
    else
        bytesSent = m_port.portObj->CommSend( pBuff, bytesToSend );

    DebugLog( "Tx", pBuff, (BYTE*)pPayload );

    IncStat( m_port.stats.cmdsSent );
    IncStat( m_port.stats.bytesSent, bytesSent );

    // Init the ack engine, if necessary
    if( m_cachedCmds[aCmd].needAck )
    {
        m_cmdRespWait.state      = eAS_Waiting;
        m_cmdRespWait.aCmd       = aCmd;
        m_cmdRespWait.pktCmdByte = m_cachedCmds[aCmd].byCmdNbr;
        m_cmdRespWait.retryCount = 0;
        m_cmdRespWait.lastResult = 0xFF;

        if( pPayload != NULL )
            m_cmdRespWait.cmdData = *pPayload;
        else
            memset( &m_cmdRespWait.cmdData, 0, sizeof( MPLS2_DATA_UNION ) );
    }

    return( bytesSent == bytesToSend );
}


bool TWirelessMPLDevice::ResendLastCmd( void )
{
    // Tries to resend a command, if we were waiting on an ack for one.
    // Returns true if a command was resent, false otherwise.

    // Make sure we are waiting for an ack
    if( m_cmdRespWait.state != eAS_Waiting )
        return false;

    // If we've failed, stop trying
    if( m_cmdRespWait.retryCount > 3 )
    {
        m_cmdRespWait.state = eAS_Failed;

        DoOnCmdFailed( m_cmdRespWait.pktCmdByte, m_cmdRespWait.lastResult );

        return false;
    }

    // Determine if there was a payload
    MPLS2_DATA_UNION* pPayload = NULL;

    if( m_cachedCmds[m_cmdRespWait.aCmd].needsPayload )
        pPayload = &( m_cmdRespWait.cmdData );

    // Sending a command clears the retry count, so save that count
    // now and then inc it
    int iCurrRetries = m_cmdRespWait.retryCount;

    bool bCmdSent = SendMPLS2Cmd( m_cmdRespWait.aCmd, pPayload );

    // Inc retry counter if command was sent
    if( bCmdSent )
        m_cmdRespWait.retryCount = iCurrRetries + 1;
    else
        m_cmdRespWait.retryCount = iCurrRetries;

    return bCmdSent;
}


String TWirelessMPLDevice::GetDevStatus( void )
{
    switch( m_linkState )
    {
        case LS_CLOSED:        return "Port closed";
        case LS_HUNTING:       return "Hunting";
        case LS_INITING:       return "Initializing";
        case LS_CFG_DOWNLOAD:  return "Config download";
        case LS_CFG_UPLOAD:    return "Config upload";

        case LS_IDLE:
            if( DeviceIsRecving )
                return "Active";
            else
                return "Packet Timeout";
    }

    // Fall through means unknown state!
    return "State " + IntToStr( m_linkState );
}


bool TWirelessMPLDevice::GetDevIsIdle( void )
{
    return( m_linkState == LS_IDLE );
}


bool TWirelessMPLDevice::GetDevIsCalibrated( void )
{
    // Return true if all cal pages have been downloaded
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        if( ( iUnit == eMU_SlaveMPLS ) && !HaveSlaveTEC )
            continue;

        for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
        {
            if( !m_cfgPgStatus[iUnit][iPg].havePage )
                return false;
        }
    }

    // Fall through means we have config data
    return true;
}


bool TWirelessMPLDevice::GetCanStartCycle( void )
{
    // Can't launch or clean if not in idle state
    if( m_linkState != LS_IDLE )
        return false;

    // Can only start a cycle if there is no pending change and
    // the last RFC shows no active relays
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
       // Can't start if we have a pending action
       if( m_tecInfo[iUnit].pendingSol >= 0 )
           return false;

       // Can't start if an action is underway
       if( m_tecInfo[iUnit].bcSolCmd != eBC_NoCmd )
           return false;

       // Can't start if we already have an active solenoid
       if( m_tecInfo[iUnit].activeSol != 0 )
           return false;
    }

    // Fall through means we can start a cycle
    return true;
}


bool TWirelessMPLDevice::GetFlagTripped( void )
{
    // Could really implement this as a BoolState var for more info to caller
    if( m_lastRFC.rfcCount == 0 )
        return false;

    return m_lastRFC.rfcPkt.flagInActive;
}


void TWirelessMPLDevice::UpdateCommsErrors( void )
{
    // Update comm error counts. Post an event if comm errors are detected.
    // To prevent spamming the client, we only set error bits when the count
    // equals the error limt. If an error persists, we'll re-nag the user
    // after the count wraps around back around past zero (essentially
    // not a chance of that happening in this app)

    // The Master PIB and the Plug detector must always be present
    BYTE byErrorBits = 0;

    // First, there will always be a Master PIB
    if( m_lastRFC.rfcPkt.systemStatusBits & SYS_STATUS_FIELD_MASTER_PIB_ERR )
    {
        // Error present - inc the count. Note the error if we've hit the limit
        m_deviceErrCounts[eMC_MasterPIB]++;

        if( m_deviceErrCounts[eMC_MasterPIB] == ConsecutiveHWErrorLimit )
            byErrorBits |= ( 1 << eMC_MasterPIB );
    }
    else
    {
        // No error present - clear count
        m_deviceErrCounts[eMC_MasterPIB] = 0;
    }

    if( m_lastRFC.rfcPkt.systemStatusBits & SYS_STATUS_FIELD_SLAVE_PD_ERR )
    {
        // Error present - inc the count. Note the error if we've hit the limit
        m_deviceErrCounts[eMC_PlugDet]++;

        if( m_deviceErrCounts[eMC_PlugDet] == ConsecutiveHWErrorLimit )
            byErrorBits |= ( 1 << eMC_PlugDet );
    }
    else
    {
        // No error present - clear count
        m_deviceErrCounts[eMC_PlugDet] = 0;
    }

    // Check slave devices only if present in system
    if( HaveSlaveTEC )
    {
        if( m_lastRFC.rfcPkt.systemStatusBits & SYS_STATUS_FIELD_SLAVE_TEC_ERR )
        {
            // Error present - inc the count. Note the error if we've hit the limit
            m_deviceErrCounts[eMC_SlaveTEC]++;

            if( m_deviceErrCounts[eMC_SlaveTEC] == ConsecutiveHWErrorLimit )
                byErrorBits |= ( 1 << eMC_SlaveTEC );
        }
        else
        {
            // No error present - clear count
            m_deviceErrCounts[eMC_SlaveTEC] = 0;
        }

        if( m_lastRFC.rfcPkt.systemStatusBits & SYS_STATUS_FIELD_SLAVE_PIB_ERR )
        {
            // Error present - inc the count. Note the error if we've hit the limit
            m_deviceErrCounts[eMC_SlavePIB]++;

            if( m_deviceErrCounts[eMC_SlavePIB] == ConsecutiveHWErrorLimit )
                byErrorBits |= ( 1 << eMC_SlavePIB );
        }
        else
        {
            // No error present - clear count
            m_deviceErrCounts[eMC_SlavePIB] = 0;
        }
    }

    if( byErrorBits != 0 )
        DoOnHWErrors( byErrorBits );
}


void TWirelessMPLDevice::UpdateAveraging( void )
{
    // Method called to update averages when a RFC packet is received
    m_avgRPM->AddValue( m_lastRFC.rfcPkt.rpm );

    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        m_tecInfo[iUnit].tankAvgPress->AddValue( m_lastRFC.rfcPkt.tecState[iUnit].tankPress );
        m_tecInfo[iUnit].regAvgPress->AddValue ( m_lastRFC.rfcPkt.tecState[iUnit].regPress  );
    }
}


void TWirelessMPLDevice::UpdateSolenoids( void )
{
    // Called after receipt of a RFC and is used to update the solenoid
    // state machine.
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        // Get the currently active solenoid
        int iCurrActiveSol = m_lastRFC.rfcPkt.tecState[iUnit].solActive;

        // If a solenoid is on, latch the max current
        if( iCurrActiveSol > 0 )
        {
             if( m_tecInfo[iUnit].maxSolCurr < m_lastRFC.rfcPkt.tecState[iUnit].solCurrent )
                 m_tecInfo[iUnit].maxSolCurr = m_lastRFC.rfcPkt.tecState[iUnit].solCurrent;
        }

        // Check if a command has completed
        if( m_tecInfo[iUnit].bcSolCmd == eBC_TurnOff )
        {
            if( iCurrActiveSol == 0 )
            {
                m_tecInfo[iUnit].bcSolCmd   = eBC_NoCmd;
                m_tecInfo[iUnit].pendingSol = -1;
            }
        }
        else if( m_tecInfo[iUnit].bcSolCmd == eBC_TurnOn )
        {
            if( iCurrActiveSol == m_tecInfo[iUnit].pendingSol )
            {
                m_tecInfo[iUnit].bcSolCmd   = eBC_NoCmd;
                m_tecInfo[iUnit].pendingSol = -1;
            }
        }

        // If a solenoid has turned off, fire an event. At this point
        // in time, activeSol is actually the previous state because
        // it hasn't been updated yet
        if( ( iCurrActiveSol == 0 ) && ( m_tecInfo[iUnit].activeSol != 0 ) )
        {
            // A relay has opened. Fire the event for it
            switch( m_tecInfo[iUnit].activeSol )
            {
                case 1:  DoOnLaunchDone  ( iUnit == 0 ? eCN_Master1 : eCN_Slave1, m_tecInfo[iUnit].maxSolCurr ); break;
                case 2:  DoOnCleaningDone( iUnit == 0 ? eCN_Master1 : eCN_Slave1, m_tecInfo[iUnit].maxSolCurr ); break;
                case 3:  DoOnLaunchDone  ( iUnit == 0 ? eCN_Master2 : eCN_Slave2, m_tecInfo[iUnit].maxSolCurr ); break;
                case 4:  DoOnCleaningDone( iUnit == 0 ? eCN_Master2 : eCN_Slave2, m_tecInfo[iUnit].maxSolCurr ); break;
                case 5:  DoOnResetFlagDone( m_tecInfo[iUnit].maxSolCurr ); break;
            }
        }

        // Remember currently active solenoid for next pass through
        m_tecInfo[iUnit].activeSol = iCurrActiveSol;

        // Clear the max current if the solenoid is now off
        if( iCurrActiveSol == 0 )
            m_tecInfo[iUnit].maxSolCurr = 0;
    }
}


void TWirelessMPLDevice::UpdateIsolOuts( void )
{
    // Called after receipt of a RFC. Update the state of the isolated
    // outputs.

    // Isolated outputs not supported in this release. For now, just
    // record their current state.
    for( int iIsolOut = 0; iIsolOut < NBR_ISOL_OUTS_PER_TEC; iIsolOut++ )
    {
        m_tecInfo[eMU_MasterMPLS].pibIsolOutState[iIsolOut] = m_lastRFC.rfcPkt.tecState[eMU_MasterMPLS].eIsolOutState[iIsolOut];
        m_tecInfo[eMU_MasterMPLS].pibIsolOutCmd  [iIsolOut] = eBC_NoCmd;

        m_tecInfo[eMU_SlaveMPLS].pibIsolOutState[iIsolOut] = m_lastRFC.rfcPkt.tecState[eMU_SlaveMPLS].eIsolOutState[iIsolOut];
        m_tecInfo[eMU_SlaveMPLS].pibIsolOutCmd  [iIsolOut] = eBC_NoCmd;
    }
}


void TWirelessMPLDevice::CheckForPlug( void )
{
    // Plug is detected using the plug detector board. Current version
    // does not use this feature. Prototype code has been left in place
    // for reference and possible re-use in future versions

#if LEGACY_PLUG_DETECTION_CODE
    // Plug detector works off magnetism, but the magnet may get installed
    // backwards so we don't know if we're looking for a positive going or
    // negative going transition until we get our first reading. Assume
    // that the switch is *not* active when the first reading comes through.
    #define PLUG_DETECT_BD_NBR   0

    if( m_linkState == LS_IDLE )
    {
        if( m_lastRFC.rfcCount != 0 )
        {
            if( m_plugState == NULL )
            {
                if( m_lastRFC.rfcPkt.activeSensors[PLUG_DETECT_BD_NBR] )
                {
                    // Create the object only if we have a non-zero reading
                    float MinValidReading = 1000.0;
                    float fCurrReading    = m_lastRFC.rfcPkt.sensXAvg[PLUG_DETECT_BD_NBR];

                    if( fCurrReading > MinValidReading )
                        m_plugState = new TIntHysteresis( (int)MinValidReading, TIntHysteresis::TT_VALUE_DECREASING );
                    else if( fCurrReading < -MinValidReading )
                        m_plugState = new TIntHysteresis( (int)(-MinValidReading), TIntHysteresis::TT_VALUE_INCREASING );

                    // If we've constructed an object, set its timing params
                    if( m_plugState != NULL )
                    {
                        m_plugState->OffToOnDelay = 0;
                        m_plugState->OnToOffDelay = 10000;
                    }
                }
            }
            else
            {
                if( m_lastRFC.rfcPkt.activeSensors[PLUG_DETECT_BD_NBR] )
                {
                    float fCurrReading = m_lastRFC.rfcPkt.sensXAvg[PLUG_DETECT_BD_NBR];

                    m_plugState->SetCurrentValue( (int)fCurrReading );

                    if( m_plugState->IsActive )
                        SetPlugDetected();
                    else if( m_ePlugDetState == ePDS_DetectAckd )
                        m_ePlugDetState = ePDS_NoPlug;
                }
            }
        }
    }
#endif
}


bool TWirelessMPLDevice::LaunchPlug( eCanisterNbr ePlug )
{
    // Activates the launch mechanism
    if( m_linkState != LS_IDLE )
        return false;

    if( !GetCanStartCycle() )
        return false;

    // Map the canister number to a launching solenoid
    int iSol = 0;
    eMPLSUnit eUnit;

    switch( ePlug )
    {
        case eCN_Master1:

            if( !HaveMasterPIB )
                return false;

            iSol  = 1;
            eUnit = eMU_MasterMPLS;

            break;

        case eCN_Master2:

            if( !HaveMasterPIB )
                return false;

            iSol  = 3;
            eUnit = eMU_MasterMPLS;

            break;

        case eCN_Slave1:

            if( !HaveSlavePIB )
                return false;

            iSol  = 1;
            eUnit = eMU_SlaveMPLS;

            break;
        case eCN_Slave2:

            if( !HaveSlavePIB )
                return false;

            iSol  = 3;
            eUnit = eMU_SlaveMPLS;

            break;

        default:
            return false;
    }

    // Note this as a pending action
    m_tecInfo[eUnit].pendingSol = iSol;

    return true;
}


bool TWirelessMPLDevice::CleanCanister( eCanisterNbr eCan )
{
    // Activates the launch mechanism
    if( m_linkState != LS_IDLE )
        return false;

    if( !GetCanStartCycle() )
        return false;

    // Map the canister number to a launching solenoid
    int iSol = 0;
    eMPLSUnit eUnit;

    switch( eCan )
    {
        case eCN_Master1:

            if( !HaveMasterPIB )
                return false;

            iSol  = 2;
            eUnit = eMU_MasterMPLS;

            break;

        case eCN_Master2:

            if( !HaveMasterPIB )
                return false;

            iSol  = 4;
            eUnit = eMU_MasterMPLS;

            break;

        case eCN_Slave1:

            if( !HaveSlavePIB )
                return false;

            iSol  = 2;
            eUnit = eMU_SlaveMPLS;

            break;
        case eCN_Slave2:

            if( !HaveSlavePIB )
                return false;

            iSol  = 4;
            eUnit = eMU_SlaveMPLS;

            break;

        default:
            return false;
    }

    // Note this as a pending action
    m_tecInfo[eUnit].pendingSol = iSol;

    return true;
}


bool TWirelessMPLDevice::ActivateResetFlag( void )
{
    // Activates the launch mechanism
    if( m_linkState != LS_IDLE )
        return false;

    if( !GetCanStartCycle() )
        return false;

    // Reset flag is solenoid 5 on the master TEC
    int       iSol  = 5;
    eMPLSUnit eUnit = eMU_MasterMPLS;

    // Note this as a pending action
    m_tecInfo[eUnit].pendingSol = iSol;

    return true;
}


void TWirelessMPLDevice::AckPlugDetected( void )
{
    // Let the base class dd its work first
    TMPLDevice::AckPlugDetected();

    // This version currently doesn't do plug detection, so
    // no further work done here.
}


bool TWirelessMPLDevice::TimeParamsGood( void )
{
    // Check that the pending time params match those pending params struct.
    // Only call this function if a RFC has been received and no other command
    // has been sent. Returns true if all timing params match, otherwise
    // return false (in which case a command will have been sent)
    if( ( m_pendingTimeParams.rfcRate == m_timeParams.rfcRate )
          &&
        ( m_pendingTimeParams.rfcTimeout == m_timeParams.rfcTimeout )
          &&
        ( m_pendingTimeParams.pairingTimeout == m_timeParams.pairingTimeout )
      )
    {
        return true;
    }

    // Have a mismatch in params. Send a command
    MPLS2_DATA_UNION pktUnion = { 0 };

    pktUnion.ratePkt.rfcRate     = m_pendingTimeParams.rfcRate;
    pktUnion.ratePkt.rfcTimeout  = m_pendingTimeParams.rfcTimeout;
    pktUnion.ratePkt.pairTimeout = m_pendingTimeParams.pairingTimeout;

    if( !SendMPLS2Cmd( MCT_SET_RATE_CMD, &pktUnion ) )
    {
        // Couldn't send command. Return true for now to force
        // a retry on this
        return true;
    }

    // Command sent - make our time params equal the pending ones
    m_timeParams = m_pendingTimeParams;

    // Return false to indicate time params weren't good and command was sent
    return false;
}


bool TWirelessMPLDevice::IsolOutsUpToDate( void )
{
    // Check if the isolated outputs need to be updated. Return true if they
    // are up to date, or false if they weren't and a set command was sent.

    // TODO - this functionality to be implemented in a later release
    return true;
}


bool TWirelessMPLDevice::SolenoidsUpToDate( void )
{
    // Call this method when there is an RFC pending to check if we need
    // to send an 'activate solenoid' command. Returns true if the solenoids
    // are up to date (eg, no command was sent), or false if a command was
    // sent (and therefore an RFC is no longer pending).
    bool bSolsUpToDate = true;

    eMPLSUnit eTarget;
    int       activeSol;

    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        // Only check if a cmd is not pending for a unit
        if( m_tecInfo[iUnit].bcSolCmd == eBC_NoCmd )
        {
            // Only check if we have a pending request )
            if( m_tecInfo[iUnit].pendingSol >= 0 )
            {
                if( m_tecInfo[iUnit].pendingSol != m_tecInfo[iUnit].activeSol )
                {
                    activeSol = m_tecInfo[iUnit].pendingSol;
                    eTarget   = (eMPLSUnit)iUnit;

                    bSolsUpToDate = false;

                    break;
                }
            }
        }
    }

    if( bSolsUpToDate )
        return true;

    // Solenoids need updating. Only one solenoid can get activated
    // at a time. But for now we don't validate this
    // Build the command now
    MPLS2_DATA_UNION cmdPkt = { 0 };

    cmdPkt.pibOutsPkt.targetPIB = ( eTarget == eMU_MasterMPLS ) ? 1 : 2;
    cmdPkt.pibOutsPkt.activeSol = activeSol;
    cmdPkt.pibOutsPkt.isolOuts  = 0;  // TODO. For now these are not used

    // Set timeout (in seconds). Assume we're opening the relay for now
    switch( m_tecInfo[eTarget].pendingSol )
    {
        case 1:
        case 3:
            cmdPkt.pibOutsPkt.solTimeout = SolenoidOnTimeLaunch / 1000;
            break;

        case 2:
        case 4:
            cmdPkt.pibOutsPkt.solTimeout = SolenoidOnTimeClean / 1000;
            break;

        case 5:
            cmdPkt.pibOutsPkt.solTimeout = SolenoidOnTimeReset / 1000;
            break;

        default:
            cmdPkt.pibOutsPkt.solTimeout = 0;
            break;
    }

    if( !SendMPLS2Cmd( MCT_SET_PIB_OUTS, &cmdPkt ) )
    {
        // Send command failed - return true to force another attempt
        return true;
    }

    // Fall through means success. Update command state
    m_tecInfo[eTarget].bcSolCmd = ( m_tecInfo[eTarget].pendingSol == 0 ) ? eBC_TurnOff : eBC_TurnOn;

    // Return false to indicate command sent
    return false;
}


//
// WTTTS Onboard Configuration Data Support Functions
//

bool TWirelessMPLDevice::GetCalDataRaw( eMPLSUnit whichUnit, TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( !IsConnected )
        return false;

    // Report the raw data as array of pages
    String sMPLS = ( whichUnit == eMU_MasterMPLS ) ? String( "Master " ) : String( "Slave " );

    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        String pageDataStr;

        BYTE* pData = &( m_rawCfgData[whichUnit][ iPg * NBR_BYTES_PER_WTTTS_PAGE ] );

        for( int iByte = 0; iByte < NBR_BYTES_PER_WTTTS_PAGE; iByte++ )
            pageDataStr = pageDataStr + IntToHex( pData[iByte], 2 ) + " ";

        pCaptions->Add( sMPLS + "Page " + IntToStr( iPg ) );
        pValues->Add( pageDataStr );
    }

    return true;
}


static String CharsToStr( const char* szString, int maxLength )
{
    char* szTemp = new char[maxLength];

    memset( szTemp, 0, maxLength );

    for( int iChar = 0; iChar < maxLength - 1; iChar++ )
    {
        if( szString[iChar] == '\0' )
            break;

        szTemp[iChar] = szString[iChar];
    }

    String sResult = szTemp;

    delete [] szTemp;

    return sResult;
}


static String MfgDateToStr( BYTE relYear, BYTE month )
{
    int year = 2000 + (int)relYear;

    String sMonth;

    switch( month )
    {
        case 1:   sMonth = "Jan";  break;
        case 2:   sMonth = "Feb";  break;
        case 3:   sMonth = "Mar";  break;
        case 4:   sMonth = "Apr";  break;
        case 5:   sMonth = "May";  break;
        case 6:   sMonth = "Jun";  break;
        case 7:   sMonth = "Jul";  break;
        case 8:   sMonth = "Aug";  break;
        case 9:   sMonth = "Sep";  break;
        case 10:  sMonth = "Oct";  break;
        case 11:  sMonth = "Nov";  break;
        case 12:  sMonth = "Dec";  break;
        default:  sMonth = "Month " + IntToStr( month );
    }

    String sResult = IntToStr( year ) + " " + sMonth;

    return sResult;
}


bool TWirelessMPLDevice::GetDeviceBatteryInfo( eMPLSUnit whichUnit, BATTERY_INFO& battInfo )
{
    if( ( whichUnit < 0 ) || ( whichUnit >= eMU_NbrMPLSUnits ) )
        return false;

    if( !m_batteryInfo[whichUnit].avgBattVolts->IsValid )
        return false;

    // Extract battery type from last RFC
    BATTERY_TYPE btType = BT_UNKNOWN;

    if( m_lastRFC.rfcCount != 0 )
        btType = GetBattTypeFromStatus( whichUnit, m_lastRFC.rfcPkt.systemStatusBits );

    battInfo.battType        = btType;
    battInfo.battTypeText    = GetBattTypeText( btType );
    battInfo.initialCapacity = 0;           // pCalData->battCapacity;
    battInfo.currVolts       = m_batteryInfo[whichUnit].fLastBattVolts;
    battInfo.currUsage       = m_batteryInfo[whichUnit].iLastBattUsed;
    battInfo.avgVolts        = m_batteryInfo[whichUnit].avgBattVolts->AverageValue;
    battInfo.avgUsage        = (int)( m_batteryInfo[whichUnit].avgBattUsed->AverageValue );

    return true;
}


bool TWirelessMPLDevice::GetCalDataFormatted( eMPLSUnit whichUnit, CALIBRATION_ITEM ciItem, String& sValue )
{
    WTTTS_MFG_DATA_STRUCT*   pMfgData = (WTTTS_MFG_DATA_STRUCT*)     m_rawCfgData[whichUnit];
    MPLS_CAL_DATA_STRUCT_V2* pCalData = (MPLS_CAL_DATA_STRUCT_V2*)&( m_rawCfgData[whichUnit][ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*    pVerData = (WTTTS_CAL_VER_STRUCT*)   &( m_rawCfgData[whichUnit][ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    sValue = "";

    bool bItemIsValid = true;

    switch( ciItem )
    {
        case CI_CAL_VER:           sValue = IntToStr    ( pVerData->byCalVerNbr );      break;
        case CI_CAL_REV:           sValue = IntToStr    ( pVerData->byCalRevNbr );      break;
        case CI_MFG_INFO:          sValue = CharsToStr  ( pMfgData->szMfgInfo, 8 );     break;
        case CI_MFG_DATE:          sValue = MfgDateToStr( pMfgData->yearOfMfg, pMfgData->monthOfMfg );  break;
        case CI_TANK_PRESS_OFFSET: sValue = IntToStr    ( pCalData->tankPressOffset );  break;
        case CI_TANK_PRESS_SPAN:   sValue = FloatToStrF ( pCalData->tankPressSpan, ffExponent, 7, 2 );  break;
        case CI_REG_PRESS_OFFSET:  sValue = IntToStr    ( pCalData->regPressOffset );   break;
        case CI_REG_PRESS_SPAN:    sValue = FloatToStrF ( pCalData->regPressSpan,  ffExponent, 7, 2 );  break;

        case CI_CAL_DATE:
            if( pVerData->calYear != 0xFF )
            {
                sValue.printf( L"%d/%02d/%02d", (int)pVerData->calYear + 2000,
                                                (int)pVerData->calMonth,
                                                (int)pVerData->calDay );
            }
            else
            {
                sValue = "----/--/--";
            }
            break;

        default:
            bItemIsValid = false;
            break;
    }

    return bItemIsValid;
}


bool TWirelessMPLDevice::GetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, CalFactorCaptions, NBR_CALIBRATION_ITEMS ) )
        return false;

    for( int iItem = 0; iItem < NBR_CALIBRATION_ITEMS; iItem++ )
    {
        String sValue;

        GetCalDataFormatted( whichUnit, (CALIBRATION_ITEM)iItem, sValue );

        pValues->Strings[iItem] = sValue;
    }

    return true;
}


bool TWirelessMPLDevice::SetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions, TStringList* pValues )
{
    // Although the user can try to pass values for all items in configuration
    // memory, we only change the values of items in the host config area.
    // Note that this function only updates the data cached in the host memory
    // area - it does not commit the data to the device.
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    MPLS_CAL_DATA_STRUCT_V2* pCalData = (MPLS_CAL_DATA_STRUCT_V2*)&( m_rawCfgData[whichUnit][ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*    pVerData = (WTTTS_CAL_VER_STRUCT*)   &( m_rawCfgData[whichUnit][ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    try
    {
        for( int iParam = 0; iParam < pCaptions->Count; iParam++ )
        {
            String sKey   = pCaptions->Strings[iParam];
            String sValue = pValues->Strings[iParam];

            for( int paramIndex = 0; paramIndex < NBR_CALIBRATION_ITEMS; paramIndex++ )
            {
                if( sKey == CalFactorCaptions[paramIndex] )
                {
                    switch( paramIndex )
                    {
                        case CI_TANK_PRESS_OFFSET:  pCalData->tankPressOffset = sValue.ToInt();     break;
                        case CI_TANK_PRESS_SPAN:    pCalData->tankPressSpan   = sValue.ToDouble();  break;
                        case CI_REG_PRESS_OFFSET:   pCalData->regPressOffset  = sValue.ToInt();     break;
                        case CI_REG_PRESS_SPAN:     pCalData->regPressSpan    = sValue.ToDouble();  break;
                    }

                    // Can break out of search loop when an item is found
                    break;
                }
            }
        }
    }
    catch( ... )
    {
        // A catch means either the pValues array was insufficiently populated
        // or one of the entries was an invalid value.
        return false;
    }

    // Fall through means success - update the current cal version
    pVerData->byCalVerNbr = CurrCalVersion;
    pVerData->byCalRevNbr = CurrCalRevision;

    return true;
}


bool TWirelessMPLDevice::ReloadCalDataFromDevice( eMPLSUnit whichUnit )
{
    if( !IsConnected )
        return false;

    // Can only start the upload to device if we are currently idle
    if( m_linkState != LS_IDLE )
        return false;

    ClearConfigurationData( eMU_MasterMPLS );
    ClearConfigurationData( eMU_SlaveMPLS );

// In this release, configs are not uploaded to or downloaded from devices.
#ifdef MPLS_HAS_CONFIG_SUPPORT
    SetLinkState( LS_CFG_DOWNLOAD );
#endif

    return true;
}


bool TWirelessMPLDevice::WriteCalDataToDevice( eMPLSUnit whichUnit )
{
    if( !IsConnected )
        return false;

    // Can only start the upload to device if we are currently idle
    if( m_linkState != LS_IDLE )
        return false;

    // Set upload flags (if this feature is implemented)
#ifdef MPLS_HAS_CONFIG_SUPPORT
    for( int iPg = WTTTS_CFG_FIRST_HOST_PAGE; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
        m_cfgPgStatus[whichUnit][iPg].setPending = true;

    SetLinkState( LS_CFG_UPLOAD );
#endif

    return true;
}


void TWirelessMPLDevice::ClearConfigurationData( eMPLSUnit whichUnit )
{
    // Set all pages to the EEPROM 'empty' value first (0xFF)
    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        m_cfgPgStatus[whichUnit][iPg].havePage   = false;
        m_cfgPgStatus[whichUnit][iPg].setPending = false;

        memset( m_rawCfgData[whichUnit], 0xFF, NBR_WTTTS_CFG_PAGES * NBR_BYTES_PER_WTTTS_PAGE );

        // Call CheckCalDataVersion next - this will initialize our local
        // memory to default values while we wait for the config data
        // to be downloaded.
        CheckCalDataVersion( whichUnit );
    }
}


bool TWirelessMPLDevice::CheckCalTableState( bool bRequestNextPg )
{
    // Check to see if the calibration data has all been downloaded.
    // Return true if it has, false if not. If there is more cal data
    // to download and bRequestNextPg is true, then send a request
    // for the required data.
    //
    // Note: this function assumes that the system knows if there
    // is a slave TEC or not!
    bool bHaveAllPages = true;

    // Scan the table for any pending entries. If there is one,
    // send a command to retrieve it if requested by the caller.
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        // Skip the slave unit if we don't have one
        if( ( iUnit == eMU_SlaveMPLS ) && !HaveSlaveTEC )
            continue;

        for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
        {
            if( m_cfgPgStatus[iUnit][iPg].havePage == false )
            {
                // We still need a page
                bHaveAllPages = false;

                // Send a request for this page
                if( bRequestNextPg )
                {
                    MPLS2_DATA_UNION pktPayload;
                    pktPayload.cfgRequest.pageNbr = iPg;

                    if( (eMPLSUnit)iUnit == eMU_SlaveMPLS )
                        pktPayload.cfgRequest.pageNbr |= 0x80;

                    SendMPLS2Cmd( MCT_GET_CFG_PAGE, &pktPayload );
                }

                // Break out of search loop
                break;
            }
        }

        // If we missing pages, break out of loop
        if( !bHaveAllPages )
            break;
    }

    return bHaveAllPages;
}


void TWirelessMPLDevice::CheckCalDataVersion( eMPLSUnit whichUnit )
{
    MPLS_CAL_DATA_STRUCT_V2* pCalData2v0 = (MPLS_CAL_DATA_STRUCT_V2*)&( m_rawCfgData[whichUnit][ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*    pVerData    = (WTTTS_CAL_VER_STRUCT*)   &( m_rawCfgData[whichUnit][ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    // Check for the current version. If we're at it, we are done.
    if( ( pVerData->byCalVerNbr == CurrCalVersion )
          &&
        ( pVerData->byCalRevNbr == CurrCalRevision )
      )
    {
        return;
    }

    // Fall through means we don't know this cal struct. Init it to default values
    memcpy( pCalData2v0, &DefaultMPLSV2CalData, sizeof( MPLS_CAL_DATA_STRUCT_V2 ) );

    pVerData->byCalVerNbr = CurrCalVersion;
    pVerData->byCalRevNbr = CurrCalRevision;
}


void TWirelessMPLDevice::ResetRFCControlStruct( void )
{
    m_lastRFC.rfcCount     = 0;
    m_lastRFC.seqNbr       = 0;
    m_lastRFC.dwRFCTick    = 0;
    m_lastRFC.tRFCTime.Val = 0.0;

    memset( &m_lastRFC.rawRFCPkt, 0, sizeof( WTTTS_MPL_RFC_PKT ) );
    memset( &m_lastRFC.rfcPkt,    0, sizeof( LOGGED_RFC_DATA ) );
}


static int SolStatusBitsToSolNbr( BYTE byBits )
{
    // Look at only the lower nibbler
    byBits &= 0x0F;

    if( ( byBits >= 1 ) && ( byBits <= 5 ) )
        return byBits;
    else
        return 0;
}


void TWirelessMPLDevice::CreateGenericRFC( LOGGED_RFC_DATA& rfcPkt, const MPLS2_RFC_PKT& rawRFCPkt )
{
    // Convert the raw packet to the logged packet format. This method assumes
    // that the packet was just received.
    MPLS_CAL_DATA_STRUCT_V2* pMasterCalData = (MPLS_CAL_DATA_STRUCT_V2*)&( m_rawCfgData[eMU_MasterMPLS][ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    MPLS_CAL_DATA_STRUCT_V2* pSlaveCalData  = (MPLS_CAL_DATA_STRUCT_V2*)&( m_rawCfgData[eMU_SlaveMPLS ][ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    WORD wHr;
    WORD wMin;
    WORD wSec;
    WORD wMSec;
    Now().DecodeTime( &wHr, &wMin, &wSec, &wMSec );

    rfcPkt.tRFC             = time( NULL );
    rfcPkt.wMsecs           = wMSec;
    rfcPkt.systemStatusBits = rawRFCPkt.sysStatusBits;
    rfcPkt.rpm              = rawRFCPkt.rpm;
    rfcPkt.temp             = ConvertRawTemp( rawRFCPkt.temperature );
    rfcPkt.flagInActive     = ( rawRFCPkt.sysStatusBits & SYS_STATUS_FIELD_FLAG_TRIGGERED ) != 0;

    rfcPkt.tecState[eMU_MasterMPLS].tankPress  = DoSpanOffsetCal( TriByteToInt( rawRFCPkt.masterTankPress ), pMasterCalData->tankPressOffset, pMasterCalData->tankPressSpan );
    rfcPkt.tecState[eMU_MasterMPLS].regPress   = DoSpanOffsetCal( TriByteToInt( rawRFCPkt.masterRegPress  ), pMasterCalData->regPressOffset,  pMasterCalData->regPressSpan );
    rfcPkt.tecState[eMU_MasterMPLS].battVolts  = ConvertRawBattVolts( rawRFCPkt.masterBattVolts );
    rfcPkt.tecState[eMU_MasterMPLS].battType   = ( rawRFCPkt.sysStatusBits >> 2 ) & 0x0003;
    rfcPkt.tecState[eMU_MasterMPLS].solActive  = SolStatusBitsToSolNbr( rawRFCPkt.solStatus );
    rfcPkt.tecState[eMU_MasterMPLS].solCurrent = rawRFCPkt.masterSolCurrent;   // in mA
    rfcPkt.tecState[eMU_MasterMPLS].eIsolOutState[0] = ( rawRFCPkt.isolOutStatus & 0x01 ) ? eBS_On : eBS_Off;;
    rfcPkt.tecState[eMU_MasterMPLS].eIsolOutState[1] = ( rawRFCPkt.isolOutStatus & 0x02 ) ? eBS_On : eBS_Off;;
    rfcPkt.tecState[eMU_MasterMPLS].eIsolOutState[2] = ( rawRFCPkt.isolOutStatus & 0x04 ) ? eBS_On : eBS_Off;;
    rfcPkt.tecState[eMU_MasterMPLS].eIsolOutState[3] = ( rawRFCPkt.isolOutStatus & 0x08 ) ? eBS_On : eBS_Off;;

    rfcPkt.tecState[eMU_SlaveMPLS].tankPress   = DoSpanOffsetCal( TriByteToInt( rawRFCPkt.slaveTankPress ), pSlaveCalData->tankPressOffset, pSlaveCalData->tankPressSpan );
    rfcPkt.tecState[eMU_SlaveMPLS].regPress    = DoSpanOffsetCal( TriByteToInt( rawRFCPkt.slaveRegPress ),  pSlaveCalData->regPressOffset,  pSlaveCalData->regPressSpan );
    rfcPkt.tecState[eMU_SlaveMPLS].battVolts   = ConvertRawBattVolts( rawRFCPkt.slaveBattVolts );
    rfcPkt.tecState[eMU_SlaveMPLS].battType    = ( rawRFCPkt.sysStatusBits >> 4 ) & 0x0003;
    rfcPkt.tecState[eMU_SlaveMPLS].solActive   = SolStatusBitsToSolNbr( rawRFCPkt.solStatus >> 4 );
    rfcPkt.tecState[eMU_SlaveMPLS].solCurrent  = rawRFCPkt.slaveSolCurrent;   // in mA
    rfcPkt.tecState[eMU_SlaveMPLS].eIsolOutState[0] = ( rawRFCPkt.isolOutStatus & 0x10 ) ? eBS_On : eBS_Off;;
    rfcPkt.tecState[eMU_SlaveMPLS].eIsolOutState[1] = ( rawRFCPkt.isolOutStatus & 0x20 ) ? eBS_On : eBS_Off;;
    rfcPkt.tecState[eMU_SlaveMPLS].eIsolOutState[2] = ( rawRFCPkt.isolOutStatus & 0x40 ) ? eBS_On : eBS_Off;;
    rfcPkt.tecState[eMU_SlaveMPLS].eIsolOutState[3] = ( rawRFCPkt.isolOutStatus & 0x80 ) ? eBS_On : eBS_Off;;

    // Copy all readings (valid or  not) verbatim from the raw RFC packet
    for( int iReading = 0; iReading < MAX_PLUG_DET_READINGS; iReading++ )
        rfcPkt.plugDetReadings[iReading] = rawRFCPkt.plugData[iReading];

    // Now count the number of valid readings. Readings are valid up to the
    // first 0xFFFF
    rfcPkt.nbrPlugDetReadings = 0;

    for( int iReading = 0; iReading < MAX_PLUG_DET_READINGS; iReading++ )
    {
        if( rfcPkt.plugDetReadings[iReading] == 0xFFFF )
            break;

        rfcPkt.nbrPlugDetReadings++;
    }
}


void TWirelessMPLDevice::ClearBatteryVolts( eMPLSUnit eWhichUnit )
{
    m_batteryInfo[eWhichUnit].avgBattVolts->Reset();
    m_batteryInfo[eWhichUnit].avgBattUsed->Reset();
}


void TWirelessMPLDevice::UpdateBatteryVolts( eMPLSUnit eWhichUnit, bool bSolenoidActive )
{
    // Assumes that an RFC packet was just received. Only update these
    // values if the battery voltage is above the valid reading thresh.
    m_tecInfo[eWhichUnit].battType = GetBattTypeFromStatus( eWhichUnit, m_lastRFC.rfcPkt.systemStatusBits );

    if( m_lastRFC.rfcPkt.tecState[eWhichUnit].battVolts >= m_batteryInfo[eWhichUnit].fValidThresh )
    {
        m_batteryInfo[eWhichUnit].fLastBattVolts = m_lastRFC.rfcPkt.tecState[eWhichUnit].battVolts;
        m_batteryInfo[eWhichUnit].iLastBattUsed  = 0;  // No implemented in this release

        m_batteryInfo[eWhichUnit].avgBattUsed->AddValue( m_batteryInfo[eWhichUnit].iLastBattUsed );

        // The battery voltage will drop while a solenoid is active. If
        // one is currently on, then do not update the average.
        if( !bSolenoidActive )
            m_batteryInfo[eWhichUnit].avgBattVolts->AddValue( m_lastRFC.rfcPkt.tecState[eWhichUnit].battVolts );

        if( m_batteryInfo[eWhichUnit].avgBattVolts->AverageValue < m_batteryInfo[eWhichUnit].fMinOperThresh )
            m_tecInfo[eWhichUnit].battLevel = eBL_Low;
        else if( m_batteryInfo[eWhichUnit].avgBattVolts->AverageValue < m_batteryInfo[eWhichUnit].fGoodThresh )
            m_tecInfo[eWhichUnit].battLevel = eBL_Med;
        else
            m_tecInfo[eWhichUnit].battLevel = eBL_OK;
    }
}


void TWirelessMPLDevice::LogRFCPkt( LOGGED_RFC_DATA& rfcPkt )
{
    // Write the record to file as text, if logging is enabled
    if( m_rfcLogFileName.IsEmpty() )
        return;

    TFileStream* logStream = NULL;

    try
    {
        if( FileExists( m_rfcLogFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( m_rfcLogFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( m_rfcLogFileName, fmCreate );

            AnsiString sHeader = GetRFCRecHdrString() + "\r";

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        AnsiString sLine = ConvertRFCRecToString( rfcPkt ) + "\r";

        logStream->Write( sLine.c_str(), sLine.Length() );
    }
    catch( ... )
    {
        // Stop logging on exception
        m_rfcLogFileName = "";
    }

    if( logStream != NULL )
        delete logStream;
}


