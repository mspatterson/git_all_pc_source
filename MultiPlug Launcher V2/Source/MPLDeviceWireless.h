#ifndef MPLDeviceWirelessH
#define MPLDeviceWirelessH

    //
    // Class Declaration for new WTTTS Sub handler
    //

    #include "MPLDeviceBaseClass.h"
    #include "Hysteresis.h"

    // We use a script engine to perform initialization and config steps
    // when first connecting to a device. All script steps have the same
    // form, defined by the typedef below.
    typedef enum {
        eSSR_Running,
        eSSR_Done,
        eSSR_Failed
    } eScriptStepResult;

    typedef eScriptStepResult (__closure *TScriptStep)( bool& bRFCPending, XFER_BUFFER& xbParam );


    //
    // Class Definition
    //

    class TWirelessMPLDevice : public TMPLDevice {

      protected:

        virtual String GetDevStatus( void );
        virtual bool   GetDevIsIdle( void );
        virtual bool   GetDevIsCalibrated( void );
        virtual bool   GetCanStartCycle( void );
        virtual bool   GetFlagTripped( void );

        void EnterHuntMode( void );
            // Performs init necessary to enter hunt mode

        void UpdateCommsErrors( void );
            // Call this function to update comms error counts

        void UpdateAveraging( void );
            // Call this function to update averages when a RFC packet is received

        void UpdateSolenoids( void );
            // Call this function to update solenoid states when a RFC packet is received

        void UpdateIsolOuts( void );
            // Call this function to update state of the isolated outputs when a RFC packet is received

        // The following structure controls the script engine
        struct {
            const TScriptStep* pScriptSteps;   // Pointer to a null-terminated array of script step functions
            eScriptStepResult  eScriptState;   // Current state of the script
            int                iScriptIndex;   // Script step currently executing
            XFER_BUFFER        xbParam;        // Param value used by script steps for state maintenance
        } m_scriptCtrl;

        // The following is a null terminated array of script
        // steps to be executed.
        #define NBR_INIT_SCRIPT_STEPS ( 12 /*fcns*/ + 1 /*null term*/ )
        TScriptStep m_ScriptSteps[NBR_INIT_SCRIPT_STEPS];

        // Script step methods. If a step sends a command, it clears the bRFCPending
        // flag. The dwParam is used to allow a script state to store state info.
        eScriptStepResult InitReqRevs         ( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitWaitRevsResp    ( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitSetRates        ( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitWaitRatesAck    ( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitStartTECParams  ( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitSetTECParams    ( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitSetDetParams    ( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitWaitDetParamsAck( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitReqCalData      ( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitWaitCalData     ( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitWaitSystemIdle  ( bool& bRFCPending, XFER_BUFFER& xbParam );
        eScriptStepResult InitSyncToHW        ( bool& bRFCPending, XFER_BUFFER& xbParam );

        // When retrieving device revision info, this member tells us
        // which packets have been received
        bool m_bHaveDevInfo[eMU_NbrMPLSUnits];

      public:

        __fastcall  TWirelessMPLDevice( void );
        __fastcall ~TWirelessMPLDevice();

        bool Connect( const COMMS_CFG& portCfg );

        bool CheckPort( void );

        virtual bool Update( void );

        virtual void RefreshSettings( void );
            // Called to refresh any registry-based settings. Causes those settings
            // to be re-read from the registry.

        virtual bool LaunchPlug( eCanisterNbr ePlug );
            // Activates the launch mechanism for the passed plug

        virtual void AckPlugDetected( void );
            // Called by the client when it has ack'd the presence of a plug.
            // Will cause PlugDetected to go false, and PlugDetected won't go
            // true again until a new plug is detected

        virtual bool CleanCanister( eCanisterNbr eCan );
            // Activates the cleaning mechanism for the passed plug

        virtual bool ActivateResetFlag( void );
            // Activates the reset flag solenoid

        virtual bool ShowLastRFCPkt( TStringList* pCaptions, TStringList* pValues );
        virtual bool GetLastRFCPkt ( TDateTime& tPktTime, DWORD& rfcCount, LOGGED_RFC_DATA& rftPkt );

        bool GetCalDataRaw      ( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues );
        bool GetCalDataFormatted( eMPLSUnit whichUnit, CALIBRATION_ITEM ciItem, String& sValue       );
        bool GetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues );
        bool SetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues );
            // Note: users should not rely on the items in the above string lists
            // being in the same order as the calibration enum list above.

        bool ReloadCalDataFromDevice( eMPLSUnit whichUnit );
        bool WriteCalDataToDevice( eMPLSUnit whichUnit );

        virtual bool GetDeviceBatteryInfo( eMPLSUnit whichUnit, BATTERY_INFO& battInfo );

      private:

        // We use different comm object routines for UDP comms - these flags track that info
        bool       m_bUsingUDP;    // True if we're using UDP comms
        AnsiString m_sMPLDevAddr;  // If using UDP, this is the IP addr from which the last event was received
        WORD       m_wMPLDevPort;  // If using UDP, this is the port from which the last event was received

        typedef enum {
            LS_CLOSED,         // Comm port closed
            LS_HUNTING,        // Waiting for first RFC command from sub
            LS_INITING,        // Initializating connected device
            LS_CFG_DOWNLOAD,   // Downloading config data from device
            LS_CFG_UPLOAD,     // Uploading config data to device
            LS_IDLE,           // Idle, responding to sub RFC requests with 'no command'
            NBR_LINK_STATES
        } LINK_STATE;

        LINK_STATE m_linkState;

        void SetLinkState( LINK_STATE newState );

        // Command management
        typedef enum {
            MCT_NO_COMMAND,
            MCT_SET_RATE_CMD,
            MCT_REQUEST_VER_INFO,
            MCT_SET_RF_CHANNEL,
            MCT_MARK_CHAN_IN_USE,
            MCT_GET_CFG_PAGE,
            MCT_SET_CFG_PAGE,
            MCT_SET_PIB_OUTS,
            MCT_SET_TEC_PARAMS,
            NBR_MPLS2_CMD_TYPES
        } MPLS2_CMD_TYPE;

        // Create an array of the sub commands. For no payload commands, the
        // cached data can be sent as is.
        typedef struct {
            BYTE  byCmdNbr;
            bool  needsPayload;
            bool  needAck;
            DWORD cmdLen;
            BYTE  byTxData[MAX_MPLS2_PKT_LEN];
        } CACHED_CMD_PKT;

        CACHED_CMD_PKT m_cachedCmds[NBR_MPLS2_CMD_TYPES];

        void InitMPLCommandPkt( MPLS2_CMD_TYPE cmdType );

        // When issuing certain 'set' commands, we expect the MPLS to
        // respond with an 'Ack'. The following structure controls the
        // Ack wait / retry mechanism.
        typedef enum {
           eAS_Ready,     // Can send an ack'able command
           eAS_Waiting,   // Waiting for an ack
           eAS_Rcvd,      // Ack received
           eAS_Failed     // No ack received, failed on retries
        } eAckState;

        struct {
            MPLS2_CMD_TYPE   aCmd;
            BYTE             pktCmdByte;
            MPLS2_DATA_UNION cmdData;
            eAckState        state;       // State of ack
            int              retryCount;  // Times have retried the command
            BYTE             lastResult;  // Value returned in
        } m_cmdRespWait;

        bool SendMPLS2Cmd( MPLS2_CMD_TYPE aCmd, const MPLS2_DATA_UNION* pPayload = NULL );
        bool ResendLastCmd( void );

        // Receive data vars
        BYTE* m_rxBuffer;              // received data holding buffer
        DWORD m_rxBuffCount;           // number of bytes currently in the buffer
        DWORD m_lastRxByteTime;        // time (tick count) of last byte received in buffer
        DWORD m_lastRxPktTime;         // time last valid packet of any type received from sub

        struct {
            DWORD               rfcCount;
            BYTE                seqNbr;      // Sequence number of last RFC
            DWORD               dwRFCTick;   // time (tick count) of last RFC from device
            TDateTime           tRFCTime;    // time_t of last RFC from device
            MPLS2_RFC_PKT       rawRFCPkt;   // Raw RFC packet
            LOGGED_RFC_DATA     rfcPkt;      // Converted RFC packet
        } m_lastRFC;

        void ResetRFCControlStruct( void );
        void CreateGenericRFC( LOGGED_RFC_DATA& rfcPkt, const MPLS2_RFC_PKT& rawRFCPkt );

        // Battery averaging
        struct {
            float        fLastBattVolts;
            int          iLastBattUsed;
            float        fValidThresh;
            float        fMinOperThresh;
            float        fGoodThresh;
            TExpAverage* avgBattVolts;
            TExpAverage* avgBattUsed;
        } m_batteryInfo[eMU_NbrMPLSUnits];

        void ClearBatteryVolts( eMPLSUnit wWhichUnit );
        void UpdateBatteryVolts( eMPLSUnit wWhichUnit, bool bSolenoidActive );

        // MPL plug detection
        TIntHysteresis* m_plugState;

        void CheckForPlug( void );

        void UpdateSwitches( void );

        bool TimeParamsGood( void );
        bool SolenoidsUpToDate( void );
        bool IsolOutsUpToDate( void );

        // Configuration data support
        BYTE m_rawCfgData[eMU_NbrMPLSUnits][NBR_WTTTS_CFG_PAGES * NBR_BYTES_PER_WTTTS_PAGE];

        typedef struct {
            bool  havePage;
            bool  setPending;
        } CFG_PAGE_STATUS;

        CFG_PAGE_STATUS m_cfgPgStatus[eMU_NbrMPLSUnits][NBR_WTTTS_CFG_PAGES];

        void ClearConfigurationData( eMPLSUnit whichUnit );
        bool CheckCalTableState( bool bRequestNextPg );
        void CheckCalDataVersion( eMPLSUnit whichUnit );

        // Logging
        void LogRFCPkt( LOGGED_RFC_DATA& rfcPkt );
    };

#endif

