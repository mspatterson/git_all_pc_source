#include <vcl.h>
#pragma hdrstop

#include <registry.hpp>

#include "MPLRegistryInterface.h"
#include "ApplUtils.h"

#pragma package(smart_init)


//
// Private Registry Defines
//

static const String m_BootSection( "Boot" );
    static const String m_LastShutdown  ( "LastShutdown" );
    static const String m_ProcPriority  ( "ProcessPriority" );
    static const String m_HiveBuilt     ( "HiveBuilt" );

static const String m_JobsMRUSection( "MostRecentJobs" );
    static const String m_MRUItem       ( "LastJob%d" );

static const String m_RecentNameSection( "RecentNames %d" );
    static const String m_recentNameItem( "Name %d" );

static const String m_SettingsSection( "Settings" );
    static const String m_JobsDir            ( "DataDir" );
    static const String m_DefaultUOM         ( "Default UOM" );
    static const String m_Password           ( "WindowAttributes" );
    static const String m_ForceBackupSave    ( "Force Backup" );
    static const String m_ForceBackupSaveDir ( "Force Backup Dir" );
    static const String m_LaunchClkTimeout   ( "Launch Click Timeout" );
    static const String m_SolenoidOnTime     ( "Solenoid On Time %d" );
    static const String m_PlugDetectTimeout  ( "Plug Detect Timeout" );
    static const String m_BattValidThresh    ( "Battery Valid Threshold" );
    static const String m_BattMinOperV       ( "Battery Min Oper Volts" );
    static const String m_BattGoodV          ( "Battery Good Volts" );
    static const String m_EnableBRAlarmOut   ( "Enable BR Alarm Out" );

static const String m_AvgSection( "Averaging Settings" );
    static const String m_AvgSettingAlpha    ( "%s %s Alpha" );
    static const String m_AvgSettingLevel    ( "Avg %s Param" );
    static const String m_AvgSettingVariance ( "Avg %s Variance" );

static const String m_CommsSection( "Comms Settings" );
    static const String m_portType         ( "Port %d Type" );
    static const String m_portName         ( "Port %d Name" );
    static const String m_portParam        ( "Port %d Param" );
    static const String m_autoAssignPorts  ( "AutoAssignPorts" );
    static const String m_radioChanTimeout ( "Radio Chan Timeout" );
    static const String m_defaultRadioChan ( "Default Radio Chan" );
    static const String m_RFCRate          ( "RFC Rate" );
    static const String m_RFCTimeout       ( "RFC Timeout" );
    static const String m_PairTimeout      ( "Pairing Timeout" );


#define NBR_MRU_JOBS  4


static TRegistryIniFile* CreateRegIni( void );
    // Creates a reg ini file object. Caller is responsible for releasing it


// Default Averaging Level Properties ( Param, Variance )
typedef struct {
    float      param;
    float      absVariance;
} AVG_LEVEL_PROPS;

static const AVG_LEVEL_PROPS DefaultAvgLevels[NBR_AVG_TYPES][NBR_AVG_LEVELS] =
{
    //                AL_OFF       AL_LIGHT     AL_MEDIUM    AL_HEAVY
    /*AT_RPM */   { { 1.00, 0 }, { 0.99, 0 }, { 0.85, 0 }, { 0.70, 0 } },
    /*AT_PRESS*/  { { 1.00, 0 }, { 0.99, 0 }, { 0.85, 0 }, { 0.70, 0 } },
};

static const String AvgTypeName[NBR_AVG_TYPES] = { "RPM", "Pressure" };


// Some properties only persist through the execution of the application.
// Those are stored here.

static bool    m_loggingEnabled[NBR_DATA_LOG_TYPES] = { false, };
static String  m_logFileName[NBR_DATA_LOG_TYPES]    = { "",    };


//
// Public Functions
//

void SetMPLSRegistryDefaults( void )
{
    // Checks for the existence of the MPLS registry hive. If found,
    // does nothing. If not found, creates default entries for all keys.
    // Check if the hive has been built first
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        if( regIni->ReadBool( m_BootSection, m_HiveBuilt, false ) == false )
        {
            // Hive not built. Its a pain to delete subkeys, so just leave
            // any existing keys and subkeys in place and create new ones
            // with default values. Also, we only create default values for
            // the Settings and Comm Settings sub folders.

            // Build the default have by getting default values and saving them
            SetProcessPriority( GetProcessPriority() );
            SetJobDataDir( GetJobDataDir() );
            SetDefaultUnitsOfMeasure( GetDefaultUnitsOfMeasure() );

            for( int iAvgType = 0; iAvgType < NBR_AVG_TYPES; iAvgType++ )
            {
                AVERAGING_PARAMS avgParams;
                GetAveragingSettings ( iAvgType, avgParams );
                SaveAveragingSettings( iAvgType, avgParams );
            }

            for( int iDevCommTypes = 0; iDevCommTypes < NBR_DEV_COMM_TYPES; iDevCommTypes++ )
            {
                COMMS_CFG commCfg;
                GetCommSettings ( iDevCommTypes, commCfg );
                SaveCommSettings( iDevCommTypes, commCfg );
            }

            SaveAutoAssignCommPorts( GetAutoAssignCommPorts() );
            SaveRadioChanWaitTimeout( GetRadioChanWaitTimeout() );
            SaveDefaultRadioChan( GetDefaultRadioChan() );
            SaveLaunchClickTimeout( GetLaunchClickTimeout() );
            SavePlugDetectTimeout( GetPlugDetectTimeout() );

            for( int iSolenoidOnTimeType = 0; iSolenoidOnTimeType < SOTT_NBR_SOLENOID_ON_TIME_TYPES; iSolenoidOnTimeType++ )
                SaveSolenoidOnTime( iSolenoidOnTimeType, GetSolenoidOnTime( iSolenoidOnTimeType ) );

            String sDir;
            bool bForceBackup = GetForceBackup( sDir );
            SaveForceBackup( bForceBackup, sDir );

            BATT_THRESH_VALS battThresh;
            GetBatteryThresholds( battThresh );
            SaveBatteryThresholds( battThresh );

            WIRELESS_TIME_PARAMS wtParams;
            GetWirelessTimingParams( wtParams );
            SaveWirelessTimingParams( wtParams );

            SaveAlarmOutOnBREnabled( GetAlarmOutOnBREnabled() );

            // Note that the hive has been built
            regIni->WriteBool( m_BootSection, m_HiveBuilt, true );
        }

        delete regIni;
    }
}


SHUT_DOWN_TYPE GetLastShutdown( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    SHUT_DOWN_TYPE sdtReturn = (SHUT_DOWN_TYPE)(regIni->ReadInteger( m_BootSection, m_LastShutdown, SDT_NEW ) );

    delete regIni;

    return sdtReturn;
}


void SetProgramInUse( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_LastShutdown, SDT_CRASH );

    delete regIni;
}


void SetProgramShutdownGood( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_LastShutdown, SDT_GOOD );

    delete regIni;
}


DWORD GetProcessPriority( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    DWORD procPriority = regIni->ReadInteger( m_BootSection, m_ProcPriority, NORMAL_PRIORITY_CLASS );

    delete regIni;

    return procPriority;
}


void SetProcessPriority( DWORD newPriority )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_ProcPriority, newPriority );

    delete regIni;
}


void GetMostRecentJobsList( TStrings* slJobList )
{
    if( slJobList == NULL )
        return;

    slJobList->Clear();

    TRegistryIniFile* regIni = CreateRegIni();

    // Add strings from the MRU list until we've either max'd out or
    // we encounter the first empty string.
    for( int iItem = 0; iItem < NBR_MRU_JOBS; iItem++ )
    {
        String sKey;
        sKey.printf( m_MRUItem.w_str(), iItem + 1 );

        String sEntry = regIni->ReadString( m_JobsMRUSection, sKey, L"" );

        if( sEntry.Length() == 0 )
            break;

        if( FileExists( sEntry ) )
            slJobList->Add( sEntry );

    }

    delete regIni;
}


String GetMostRecentJob( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    // Return the most recent entry from the MRU list
    String sKey;
    sKey.printf( m_MRUItem.w_str(), 1 );

    String sEntry = regIni->ReadString( m_JobsMRUSection, sKey, L"" );

    delete regIni;

    return sEntry;
}


void AddToMostRecentJobsList( const String& sNewJob )
{
    TRegistryIniFile* regIni = CreateRegIni();

    // Copy all of the existing strings in order to a string list. It
    // will be easier to work with than the actual registry entries.
    TStringList* mruStrings = new TStringList();

    GetMostRecentJobsList( mruStrings );

    // Check if the entry exists. If it does, make sure it is the topmost
    // entry in the list
    int currIndex = mruStrings->IndexOf( sNewJob );

    if( currIndex > 0 )
    {
        // The string exists, but is not the top-most entry. Delete it
        // from its current position, then add it back at the top of
        // the list.
        mruStrings->Delete( currIndex );
        mruStrings->Insert( 0, sNewJob );
    }
    else if( currIndex < 0 )
    {
        // The string does not exist. Insert it at the top of the
        // list. We don't need to delete items in this case because
        // of how we store the strings back in the registry below.
        mruStrings->Insert( 0, sNewJob );
    }
    else
    {
        // currIndex == 0 in this case, meaning the string exists and
        // is at the top of list. No work needs to be done.
    }

    // mruStrings has been setup. Now re-create the section. First,
    // delete all existing entries.
    regIni->EraseSection( m_JobsMRUSection );

    // Now add all items to the section
    for( int iItem = 0; iItem < mruStrings->Count; iItem++ )
    {
        // Only add up to NBR_MRU_JOBS items
        if( iItem >= NBR_MRU_JOBS )
            break;

        // Now add this job to the top of the list
        String sKey;
        sKey.printf( m_MRUItem.w_str(), iItem + 1 );

        regIni->WriteString( m_JobsMRUSection, sKey, mruStrings->Strings[iItem] );
    }

    delete mruStrings;
    delete regIni;
}


String GetJobDataDir( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    String sReturn = regIni->ReadString( m_SettingsSection, m_JobsDir, L"" );

    delete regIni;

    return sReturn;
}


void SetJobDataDir( String newDir )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteString( m_SettingsSection, m_JobsDir, newDir );

    delete regIni;
}


int GetDefaultUnitsOfMeasure( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    int iResult = regIni->ReadInteger( m_SettingsSection, m_DefaultUOM, 1 /*imperial*/ );

    delete regIni;

    return iResult;
}


void SetDefaultUnitsOfMeasure( int newDefaultUOM )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_SettingsSection, m_DefaultUOM, newDefaultUOM );

    delete regIni;
}


void GetAveragingSettings( AVG_TYPE avgType, AVERAGING_PARAMS& avgParams )
{
    // This should never happen, but just in case...
    if( ( avgType < 0 ) || ( avgType >= NBR_AVG_TYPES ) )
        avgType = AT_RPM;

    // Set default return values first
    avgParams.avgMethod   = AM_EXPONENTIAL;
    avgParams.avgLevel    = AL_LIGHT;
    avgParams.absVariance = DefaultAvgLevels[avgType][AL_LIGHT].absVariance;
    avgParams.param       = DefaultAvgLevels[avgType][AL_LIGHT].param;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sKey;

        sKey.printf( m_AvgSettingLevel.w_str(), AvgTypeName[avgType] );
        avgParams.avgLevel = (AVG_LEVEL) regIni->ReadInteger( m_AvgSection, sKey, avgParams.avgLevel );

        sKey.printf( m_AvgSettingVariance.w_str(), AvgTypeName[avgType] );
        avgParams.absVariance = regIni->ReadInteger( m_AvgSection, sKey, (int)avgParams.absVariance );

        if( ( avgParams.avgLevel >= AL_LIGHT ) && ( avgParams.avgLevel < NBR_AVG_LEVELS ) )
        {
            // Lookup the Param based on the Level & Average Type
            sKey.printf( m_AvgSettingAlpha.w_str(), AvgTypeName[avgType], AvgLevelDesc[avgParams.avgLevel] );

            String sAlpha = regIni->ReadString( m_AvgSection, sKey, FloatToStrF( avgParams.param, ffFixed, 7, 3 ) );

            avgParams.param = StrToFloatDef( sAlpha, DefaultAvgLevels[avgType][avgParams.avgLevel].param );
        }
        else
        {
            // Averaging is off - leave the other values as defaults to ensure no averaging happens
            avgParams.avgLevel = AL_OFF;
        }

        delete regIni;
    }
}


void SaveAveragingSettings( AVG_TYPE avgType, const AVERAGING_PARAMS& avgParams )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        // Note: we only save the averaging level enum and variance.
        // Averaging type is always exponential and alpha for level
        // is a read-only constant under a different key
        String sKey;

        sKey.printf( m_AvgSettingLevel.w_str(), AvgTypeName[avgType] );
        regIni->WriteInteger( m_AvgSection, sKey, avgParams.avgLevel );

        sKey.printf( m_AvgSettingVariance.w_str(), AvgTypeName[avgType] );
        regIni->WriteInteger( m_AvgSection, sKey, (int)avgParams.absVariance );

        delete regIni;
    }
}


String GetSysAdminPassword( void )
{
    // Initialize return result with default password
    String sPassword( "tesco" );

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TMemoryStream* pStream = new TMemoryStream();

        if( pStream != NULL )
        {
            int bytesRead = regIni->ReadBinaryStream( m_SettingsSection, m_Password, pStream );

            if( bytesRead > 0 )
            {
                // Decode the password. Byte values are xor'd with 0x85
                AnsiString sTemp;
                sTemp.SetLength( bytesRead );

                BYTE* pData = (BYTE*)( pStream->Memory );

                for( int iByte = 0; iByte < bytesRead; iByte++ )
                    sTemp[iByte+1] = (char)( pData[iByte] ^ 0x85 );

                sPassword = sTemp;
            }

            delete pStream;

        }

        delete regIni;
    }

    return sPassword;
}


void SaveSysAdminPassword( String newPW )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TMemoryStream* pStream = new TMemoryStream();

        if( pStream != NULL )
        {
            AnsiString sTemp( newPW );

            pStream->SetSize( sTemp.Length() );

            BYTE* pData = (BYTE*)( pStream->Memory );

            for( int iByte = 0; iByte < sTemp.Length(); iByte++ )
                pData[iByte] = 0x85 ^ sTemp[iByte+1];

            pStream->Position = 0;

            regIni->WriteBinaryStream( m_SettingsSection, m_Password, pStream );

            delete pStream;
        }

        delete regIni;
    }
}


void GetCommSettings( DEV_COMM_TYPE devType, COMMS_CFG& commCfg )
{
    // Init defaults
    commCfg.portType  = CT_UNUSED;
    commCfg.portName  = "";
    commCfg.portParam = 0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sPortTypeKey;
        String sPortNameKey;
        String sPortParamKey;

        sPortTypeKey.printf ( m_portType.w_str(),  devType );
        sPortNameKey.printf ( m_portName.w_str(),  devType );
        sPortParamKey.printf( m_portParam.w_str(), devType );

        commCfg.portType  = (COMM_TYPE) regIni->ReadInteger( m_CommsSection, sPortTypeKey,  commCfg.portType );
        commCfg.portName  =             regIni->ReadString ( m_CommsSection, sPortNameKey,  commCfg.portName );
        commCfg.portParam =             regIni->ReadInteger( m_CommsSection, sPortParamKey, commCfg.portParam );

        delete regIni;
    }
}


void SaveCommSettings( DEV_COMM_TYPE devType, const COMMS_CFG& commCfg )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sPortTypeKey;
        String sPortNameKey;
        String sPortParamKey;

        sPortTypeKey.printf ( m_portType.w_str(),  devType );
        sPortNameKey.printf ( m_portName.w_str(),  devType );
        sPortParamKey.printf( m_portParam.w_str(), devType );

        regIni->WriteInteger( m_CommsSection, sPortTypeKey,  commCfg.portType );
        regIni->WriteString ( m_CommsSection, sPortNameKey,  commCfg.portName );
        regIni->WriteInteger( m_CommsSection, sPortParamKey, commCfg.portParam );

        delete regIni;
    }
}


bool GetAutoAssignCommPorts( void )
{
    // Init defaults
    bool bAutoAssignPorts = true;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bAutoAssignPorts = regIni->ReadBool( m_CommsSection, m_autoAssignPorts, bAutoAssignPorts );

        delete regIni;
    }

    return bAutoAssignPorts;
}


void SaveAutoAssignCommPorts( bool bAutoAssignPorts )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool( m_CommsSection, m_autoAssignPorts, bAutoAssignPorts );

        delete regIni;
    }
}


DWORD GetRadioChanWaitTimeout( void )
{
    // Init defaults
    DWORD dwTimeout = 30000;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        dwTimeout = regIni->ReadInteger( m_CommsSection, m_radioChanTimeout, dwTimeout );

        delete regIni;
    }

    return dwTimeout;
}


void SaveRadioChanWaitTimeout( DWORD dwNewTimeout )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_CommsSection, m_radioChanTimeout, dwNewTimeout );

        delete regIni;
    }
}


int GetDefaultRadioChan( void )
{
    // Init defaults
    int iDefaultChan = 12;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        iDefaultChan = regIni->ReadInteger( m_CommsSection, m_defaultRadioChan, iDefaultChan );

        delete regIni;
    }

    return iDefaultChan;
}


void SaveDefaultRadioChan( int iDefaultRadioChan )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_CommsSection, m_defaultRadioChan, iDefaultRadioChan );

        delete regIni;
    }
}


DWORD GetLaunchClickTimeout( void )
{
    // Init defaults
    DWORD dwTimeout = 4000;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        dwTimeout = regIni->ReadInteger( m_SettingsSection, m_LaunchClkTimeout, dwTimeout );

        delete regIni;
    }

    return dwTimeout;
}


void SaveLaunchClickTimeout( DWORD dwNewTimeout )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_SettingsSection, m_LaunchClkTimeout, dwNewTimeout );

        delete regIni;
    }
}


DWORD GetPlugDetectTimeout( void )
{
    // Init defaults
    DWORD dwTimeout = 60000;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        dwTimeout = regIni->ReadInteger( m_SettingsSection, m_PlugDetectTimeout, dwTimeout );

        delete regIni;
    }

    return dwTimeout;
}


void SavePlugDetectTimeout( DWORD dwNewTimeout )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_SettingsSection, m_PlugDetectTimeout, dwNewTimeout );

        delete regIni;
    }
}


DWORD GetMinSolenoidOnTime( void )
{
    // Not really a registry entry, but required here as this unit
    // doesn't know about MPLS defs. This also allows this value
    // to be made a reg entry in the future.
    return 5000;
}


DWORD GetSolenoidOnTime( SOLENOID_ON_TIME_TYPE eType )
{
    // Init defaults
    DWORD dwOnTime = GetMinSolenoidOnTime();

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sKey;
        sKey.printf( m_SolenoidOnTime.w_str(), eType );

        dwOnTime = regIni->ReadInteger( m_SettingsSection, sKey, dwOnTime );

        delete regIni;
    }

    if( dwOnTime < GetMinSolenoidOnTime() )
        return GetMinSolenoidOnTime();
    else
        return dwOnTime;
}


void SaveSolenoidOnTime( SOLENOID_ON_TIME_TYPE eType, DWORD dwNewTime )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sKey;
        sKey.printf( m_SolenoidOnTime.w_str(), eType );

        regIni->WriteInteger( m_SettingsSection, sKey, dwNewTime );

        delete regIni;
    }
}


bool GetDataLogging( DATA_LOG_TYPE logType, String& logFileName )
{
    // Non-persistent property
    logFileName = m_logFileName[logType];

    return m_loggingEnabled[logType];
}


void SaveDataLogging( DATA_LOG_TYPE logType, bool loggingOn, String logFileName )
{
    // Non-persistent property
    m_logFileName[logType]    = logFileName;
    m_loggingEnabled[logType] = loggingOn;
}


void GetRecentNameList( RECENT_NAME_LIST aList, TStringList* nameList )
{
    if( nameList == NULL )
        return;

    nameList->Clear();
    nameList->Sorted = false;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sKey;
        sKey.printf( m_RecentNameSection.w_str(), aList );

        for( int iItem = 0; iItem < 30; iItem++ )
        {
           String sItem;
           sItem.printf( m_recentNameItem.w_str(), iItem );

           String sEntry = regIni->ReadString( sKey, sItem, L"" );

            if( sEntry.Length() == 0 )
                break;

            nameList->Add( sEntry );
        }

        delete regIni;
    }
}


void SaveRecentName( RECENT_NAME_LIST aList, const String& newName )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TStringList* currNames = new TStringList;

        // Get current name list on registry for a section
        GetRecentNameList( aList, currNames );

        // If the newName is in the list, delete it.
        int currIndex = currNames->IndexOf( newName );

        if( currIndex >= 0 )
            currNames->Delete( currIndex );

        // Insert to the top of list
        currNames->Insert( 0, newName );

        // Make sure we have no more thean the max items in the list
        const int MaxNames = 30;

        while( currNames->Count > MaxNames )
            currNames->Delete( currNames->Count - 1 );

        // Write the list to section. Erase any current entries first
        String sKey;
        sKey.printf( m_RecentNameSection.w_str(), aList );

        regIni->EraseSection( sKey );

        for( int iItem = 0; iItem < currNames->Count; iItem++ )
        {
            String sItem;
            sItem.printf( m_recentNameItem.w_str(), iItem );

            regIni->WriteString( sKey, sItem, currNames->Strings[iItem] );
        }

        delete currNames;
        delete regIni;
    }
}


bool GetForceBackup( String& sDir )
{
    bool bResult = false;
    sDir         = "";

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bResult = regIni->ReadBool  ( m_SettingsSection, m_ForceBackupSave,    bResult );
        sDir    = regIni->ReadString( m_SettingsSection, m_ForceBackupSaveDir, sDir    );

        delete regIni;
    }

    return bResult;
}


void SaveForceBackup( bool bNewValue, String sDir )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool  ( m_SettingsSection, m_ForceBackupSave,    bNewValue );
        regIni->WriteString( m_SettingsSection, m_ForceBackupSaveDir, sDir      );

        delete regIni;
    }
}


void GetBatteryThresholds( BATT_THRESH_VALS& battThresh )
{
    // Set defaults
    battThresh.fReadingValidThresh = 2.5;   // Battery readings below this value are ignored, in V
    battThresh.fMinOperValue       = 3.5;   // Operation not allowed below this threshold, in V
    battThresh.fGoodThresh         = 3.9;   // Battery is in a good state at or above this, in V

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        battThresh.fReadingValidThresh = regIni->ReadFloat( m_SettingsSection, m_BattValidThresh, battThresh.fReadingValidThresh );
        battThresh.fMinOperValue       = regIni->ReadFloat( m_SettingsSection, m_BattMinOperV,    battThresh.fMinOperValue       );
        battThresh.fGoodThresh         = regIni->ReadFloat( m_SettingsSection, m_BattGoodV,       battThresh.fGoodThresh         );

        delete regIni;
    }
}


void SaveBatteryThresholds( const BATT_THRESH_VALS& battThresh )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteFloat( m_SettingsSection, m_BattValidThresh, battThresh.fReadingValidThresh );
        regIni->WriteFloat( m_SettingsSection, m_BattMinOperV,    battThresh.fMinOperValue       );
        regIni->WriteFloat( m_SettingsSection, m_BattGoodV,       battThresh.fGoodThresh         );

        delete regIni;
    }
}


void GetWirelessTimingParams( WIRELESS_TIME_PARAMS& wtParams )
{
    // Set defaults
    wtParams.rfcRate        = 1000;
    wtParams.rfcTimeout     = 100;
    wtParams.pairingTimeout = 900;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        wtParams.rfcRate        = (WORD)regIni->ReadInteger( m_CommsSection, m_RFCRate,     wtParams.rfcRate        );
        wtParams.rfcTimeout     = (WORD)regIni->ReadInteger( m_CommsSection, m_RFCTimeout,  wtParams.rfcTimeout     );
        wtParams.pairingTimeout = (WORD)regIni->ReadInteger( m_CommsSection, m_PairTimeout, wtParams.pairingTimeout );

        delete regIni;
    }
}


void SaveWirelessTimingParams( const WIRELESS_TIME_PARAMS& wtParams )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_CommsSection, m_RFCRate,     wtParams.rfcRate        );
        regIni->WriteInteger( m_CommsSection, m_RFCTimeout,  wtParams.rfcTimeout     );
        regIni->WriteInteger( m_CommsSection, m_PairTimeout, wtParams.pairingTimeout );

        delete regIni;
    }
}


bool GetAlarmOutOnBREnabled( void )
{
    bool bIsEnabled = false;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bIsEnabled = regIni->ReadBool( m_SettingsSection, m_EnableBRAlarmOut, bIsEnabled );

        delete regIni;
    }

    return bIsEnabled;
}


void SaveAlarmOutOnBREnabled( bool isEnabled )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool( m_SettingsSection, m_EnableBRAlarmOut, isEnabled );

        delete regIni;
    }
}


//
//  Private Functions
//

TRegistryIniFile* CreateRegIni( void )
{
    // Caller must free the created object
    return new TRegistryIniFile( String( "Software\\CanRig\\MultiPlugLauncher\\V" ) + IntToStr( GetApplMajorVer() ) );
}

