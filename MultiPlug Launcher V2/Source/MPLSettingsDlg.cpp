#include <vcl.h>
#pragma hdrstop

#include <FileCtrl.hpp>
#include <IniFiles.hpp>
#include <DateUtils.hpp>
#include <System.Math.hpp>

#include "MPLSettingsDlg.h"
#include "UnitsOfMeasure.h"
#include "ApplUtils.h"
#include "TesTorkManagerPWGen.h"
#include "NewBattDlg.h"
#include "Messages.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TSystemSettingsForm *SystemSettingsForm;


//
// Private Functions
//

static bool IsValidInt( TEdit* anEdit )
{
    try
    {
        int iVal = anEdit->Text.ToInt();
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


static bool IsValidFloat( TEdit* anEdit )
{
    float fVal = 0.0;

    try
    {
        fVal = anEdit->Text.ToDouble();
    }
    catch( ... )
    {
    }

    return( fVal > 0.0 );
}


// The calibration load from file / save to file functions use the ini file format.
// The following constants define the section names in those files.
static const String calIniDataSection( "Calibration Information" );


// Process Priority Combo settings
typedef struct {
    char*  szDesc;
    DWORD  procEnum;
} PROC_PRIORITY_ENTRY;

#define NBR_PROC_PRIORITIES 4

static const PROC_PRIORITY_ENTRY ProcPriorities[NBR_PROC_PRIORITIES] = {
    { "Normal",        NORMAL_PRIORITY_CLASS       },
    { "Above Normal",  ABOVE_NORMAL_PRIORITY_CLASS },
    { "High Priority", HIGH_PRIORITY_CLASS         },
    { "Real Time",     REALTIME_PRIORITY_CLASS     }
};


//
// Class Implementation
//

__fastcall TSystemSettingsForm::TSystemSettingsForm(TComponent* Owner) : TForm(Owner)
{
    // Init the comms sheet controls structs
    m_MPLCommsCtrls.pTypeCombo    = MPLPortTypeCombo;
    m_MPLCommsCtrls.pPortNbrEdit  = MPLPortEdit;
    m_MPLCommsCtrls.pParamEdit    = MPLParamEdit;

    m_BaseCommsCtrls.pTypeCombo   = BaseRadioPortTypeCombo;
    m_BaseCommsCtrls.pPortNbrEdit = BaseRadioPortEdit;
    m_BaseCommsCtrls.pParamEdit   = BaseRadioParamEdit;

    m_MgmtCommsCtrls.pTypeCombo   = MgmtDevPortTypeCombo;
    m_MgmtCommsCtrls.pPortNbrEdit = MgmtDevPortEdit;
    m_MgmtCommsCtrls.pParamEdit   = MgmtDevParamEdit;

    // Init comm type combos
    MPLPortTypeCombo->Items->Clear();
    BaseRadioPortTypeCombo->Items->Clear();
    MgmtDevPortTypeCombo->Items->Clear();

    for( int iType = 0; iType < NBR_COMM_TYPES; iType++ )
    {
        MPLPortTypeCombo->Items->Add  ( CommTypeText[iType] );
        BaseRadioPortTypeCombo->Items->Add( CommTypeText[iType] );
        MgmtDevPortTypeCombo->Items->Add  ( CommTypeText[iType] );
    }

    MPLPortTypeCombo->ItemIndex       = CT_SERIAL;
    BaseRadioPortTypeCombo->ItemIndex = CT_SERIAL;
    MgmtDevPortTypeCombo->ItemIndex   = CT_SERIAL;

    // Init base radio chan combo
    BRRadNbrComboClick( BRRadNbrCombo );

    // Init process priority combo
    ProcPriorityCombo->Items->Clear();

    for( int iItem = 0; iItem < NBR_PROC_PRIORITIES; iItem++ )
        ProcPriorityCombo->Items->Add( ProcPriorities[iItem].szDesc );

    ProcPriorityCombo->ItemIndex = 0;

    // Init averaging combos
    RPMAvgCombo->Items->Clear();
    PressAvgCombo->Items->Clear();

    for( int iItem = 0; iItem < NBR_AVG_METHODS; iItem++ )
    {
        RPMAvgCombo->Items->Add  ( AvgMethodDesc[iItem] );
        PressAvgCombo->Items->Add( AvgMethodDesc[iItem] );
    }

    RPMAvgCombo->ItemIndex   = 0;
    PressAvgCombo->ItemIndex = 0;
}


DWORD TSystemSettingsForm::ShowDlg( TMPLSManager* pPoller, TBasePlugJob* currJob )
{
    DWORD dwSettingsChanged = 0x0000;

    TSystemSettingsForm* currForm = new TSystemSettingsForm( NULL );

    currForm->PollObject = pPoller;
    currForm->CurrentJob = currJob;

    try
    {
        if( currForm->ShowModal() == mrOk )
            dwSettingsChanged = currForm->SettingsChanged;
    }
    __finally
    {
        delete currForm;
    }

    return dwSettingsChanged;
}


void __fastcall TSystemSettingsForm::FormShow(TObject *Sender)
{
    // Determine units of measure in effect at time of form show.
    // Get from the current job (if we have one) or the default
    // from the registry otherwise
    if( m_currJob == NULL )
        m_uomType = (UNITS_OF_MEASURE) GetDefaultUnitsOfMeasure();
    else
        m_uomType = m_currJob->Units;

    // On show, populate current settings
    LoadCommsPage();
    LoadMiscPage();
    LoadMPLSPage();
    LoadCalPage();

    BRRadNbrComboClick( BRRadNbrCombo );

    // Default settings changed to none
    m_dwSettingsChanged = 0x0000;

    PgCtrl->ActivePage        = CommsSheet;
    CalDataPgCtrl->ActivePage = DevMemorySheet;
    DevConnPgCtrl->ActivePage = MPLSheet;

    RFCPollTimer->Enabled = true;

    // Force a timer tick right away for immediate update
    RFCPollTimerTimer( RFCPollTimer );
}


void __fastcall TSystemSettingsForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Kill the comms object poll timer first
    RFCPollTimer->Enabled = false;

    // On close, if ModalResult == mrOK, then save current settings.
    // In this case, all settings are guaranteed to be valid.
    if( ModalResult == mrOk )
    {
        m_dwSettingsChanged = 0x0000;

        m_dwSettingsChanged |= SaveCommsPage();
        m_dwSettingsChanged |= SaveCalPage();
        m_dwSettingsChanged |= SaveMPLSPage();
        m_dwSettingsChanged |= SaveMiscPage();
    }
}


void __fastcall TSystemSettingsForm::OKBtnClick(TObject *Sender)
{
    // Validate settings
    TWinControl* invalidControl = CheckCommsPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = CommsSheet;
        ActiveControl = invalidControl;

        return;
    }

    invalidControl = CheckCalPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = CalSheet;
        ActiveControl = invalidControl;

        return;
    }

    invalidControl = CheckMPLSPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = MPLSheet;
        ActiveControl = invalidControl;

        return;
    }

    invalidControl = CheckMiscPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = MiscSheet;
        ActiveControl = invalidControl;

        return;
    }

    // Values look good. Can close dialog
    ModalResult = mrOk;
}


void __fastcall TSystemSettingsForm::CommTypeClick(TObject *Sender)
{
    // There are two identical tabs of comm settings tab sheets.
    // Find the parent of the sheet of the Sender, then enable
    // all edits on that sheet whose Tag matches that of the
    // Sender. All other edits are disabled.
    TRadioButton* pRB = dynamic_cast<TRadioButton*>(Sender);

    if( pRB == NULL )
        return;

    TTabSheet* parentSheet = (TTabSheet*)( pRB->Parent );

    for( int iCtrl = 0; iCtrl < parentSheet->ControlCount; iCtrl++ )
    {
        TEdit* pEdit = dynamic_cast<TEdit*>(parentSheet->Controls[iCtrl]);

        if( pEdit == NULL )
            continue;

        if( pEdit->Tag == pRB->Tag )
        {
            pEdit->Enabled = true;
            pEdit->Color   = clWindow;
        }
        else
        {
            pEdit->Enabled = false;
            pEdit->Color   = clBtnFace;
        }
    }
}


void __fastcall TSystemSettingsForm::BrowseLogFileBtnClick(TObject *Sender)
{
    if( SaveLogDlg->Execute() )
        LogFileNameEdit->Text = SaveLogDlg->FileName;
}


void __fastcall TSystemSettingsForm::SavePWBtnClick(TObject *Sender)
{
    // Password is saved with explicit button click, rather than
    // when the dialog is closed.
    String currPW     = GetSysAdminPassword();
    String backdoorPW = GeneratePassword();

    if( ( currPW != CurrPWEdit->Text ) && ( backdoorPW != CurrPWEdit->Text ) )
    {
        MessageDlg( "The current password you have entered is incorrect.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = CurrPWEdit;
        return;
    }

    if( NewPWEdit1->Text.Trim().Length() == 0 )
    {
        MessageDlg( "Your new password cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewPWEdit1;
        return;
    }

    if( NewPWEdit1->Text != NewPWEdit2->Text )
    {
        MessageDlg( "Your new passwords do not match.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewPWEdit1;
        return;
    }

    // Password looks good. Save it and clear the boxes
    SaveSysAdminPassword( NewPWEdit1->Text );

    // Clear the edits of any text
    CurrPWEdit->Text = "";
    NewPWEdit1->Text = "";
    NewPWEdit2->Text = "";

    MessageDlg( "Password successfully changed.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
}


void __fastcall TSystemSettingsForm::BrowseJobDirBtnClick(TObject *Sender)
{
    UnicodeString jobDir = JobDirEdit->Text;

      if( SelectDirectory( "Select Job Directory", "Desktop", jobDir, TSelectDirExtOpts() << sdNewUI << sdNewFolder, NULL ) )
        JobDirEdit->Text = jobDir;
}


void __fastcall TSystemSettingsForm::ListEditorSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect)
{
    // Called by read-only grids
    CanSelect = false;
}


void TSystemSettingsForm::SetAveragingParams( const AVERAGING_PARAMS& avgParams, TComboBox* pCombo, TEdit* pVarEdit )
{
    if( ( avgParams.avgMethod >= 0 ) && ( avgParams.avgLevel < NBR_AVG_LEVELS ) )
        pCombo->ItemIndex = avgParams.avgMethod;
    else
        pCombo->ItemIndex = 0;

    pVarEdit->Text = FloatToStrF( avgParams.absVariance, ffFixed, 7, 0 );
}


bool TSystemSettingsForm::GetAveragingParams( AVERAGING_PARAMS& avgParams, TComboBox* pCombo, TEdit* pVarEdit )
{
    // The averaging level
    if( pCombo->ItemIndex < 0 )
        return false;

    float fVar = StrToFloatDef( pVarEdit->Text, -1.0 );

    if( fVar < 0.0 )
        return false;

    // Just overwrite the level and variance; param and method are fixed
    avgParams.avgLevel    = (AVG_LEVEL)(pCombo->ItemIndex);
    avgParams.absVariance = fVar;

    return true;
}


void TSystemSettingsForm::SetCommsCtrls( const COMMS_CFG& commCfg, const COMMS_SHEET_CTRLS& sheetCtrls )
{
    if( ( commCfg.portType >= 0 ) && ( commCfg.portType < sheetCtrls.pTypeCombo->Items->Count ) )
        sheetCtrls.pTypeCombo->ItemIndex = commCfg.portType;
    else
        sheetCtrls.pTypeCombo->ItemIndex = CT_SERIAL;

    switch( commCfg.portType )
    {
        case CT_SERIAL:
        case CT_UDP:
        case CT_USB:
            sheetCtrls.pPortNbrEdit->Text = commCfg.portName;

            // Force a default value if the param value is 0
            if( commCfg.portParam == 0 )
            {
                if( commCfg.portType == CT_UDP )
                    sheetCtrls.pParamEdit->Text = "10000";
                else
                    sheetCtrls.pParamEdit->Text = "115200";
            }
            else
            {
                sheetCtrls.pParamEdit->Text = IntToStr( (int)commCfg.portParam );
            }
            break;

        default:
            sheetCtrls.pPortNbrEdit->Text    = "1";
            sheetCtrls.pPortNbrEdit->Enabled = false;
            sheetCtrls.pParamEdit->Text      = "115200";
            sheetCtrls.pParamEdit->Enabled   = false;
            break;
    }
}


TWinControl* TSystemSettingsForm::GetCommsCtrls( COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls )
{
    // Assume default for now
    commsCfg.portType  = CT_UNUSED;
    commsCfg.portName  = "";
    commsCfg.portParam = 0;

    int iTestPort;
    int iTestParam;

    switch( sheetCtrls.pTypeCombo->ItemIndex )
    {
        case CT_SERIAL:

            iTestPort = sheetCtrls.pPortNbrEdit->Text.ToIntDef( 0 );

            if( iTestPort <= 0 )
            {
                MessageDlg( "Invalid Comm Port number.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pPortNbrEdit;
            }

            iTestParam = sheetCtrls.pParamEdit->Text.ToIntDef( 0 );

            if( iTestParam <= 0 )
            {
                MessageDlg( "Invalid serial Bit Rate value.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pParamEdit;
            }

            commsCfg.portType  = CT_SERIAL;
            commsCfg.portName  = IntToStr( iTestPort );
            commsCfg.portParam = iTestParam;

            break;

        case CT_UDP:

            // Port number may or may not be blank, depending on the
            // device we're connecting to (or being connected from)
            // However, a port must always be given
            iTestParam = sheetCtrls.pParamEdit->Text.ToIntDef( 0 );

            if( ( iTestParam <= 0 ) || ( iTestParam > 65535 ) )
            {
                MessageDlg( "Invalid UDP port number.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pParamEdit;
            }

            commsCfg.portType  = CT_UDP;
            commsCfg.portName  = sheetCtrls.pPortNbrEdit->Text;
            commsCfg.portParam = iTestParam;

            break;

        case CT_USB:

            if( sheetCtrls.pPortNbrEdit->Text.Trim().Length() == 0 )
            {
                MessageDlg( "USB Port Description cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pPortNbrEdit;
            }

            iTestParam = sheetCtrls.pParamEdit->Text.ToIntDef( 0 );

            if( iTestParam <= 0 )
            {
                MessageDlg( "Invalid USB Bit Rate value.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pParamEdit;
            }

            commsCfg.portType  = CT_USB;
            commsCfg.portName  = sheetCtrls.pPortNbrEdit->Text;
            commsCfg.portParam = iTestParam;

            break;

        case CT_UNUSED:
            break;

        default:
            // Should never occur, but if it does return something to indicate an error
            MessageDlg( "TSystemSettingsForm::GetCommsCtrls() : invalid port type.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return sheetCtrls.pTypeCombo;
    }

    // Fall through means all values are valid
    return NULL;
}


void TSystemSettingsForm::LoadCommsPage( void )
{
    COMMS_CFG mplCommCfg;
    COMMS_CFG baseCommCfg;
    COMMS_CFG mgmtCommCfg;

    GetCommSettings( DCT_MPL,  mplCommCfg );
    GetCommSettings( DCT_BASE, baseCommCfg );
    GetCommSettings( DCT_MGMT, mgmtCommCfg );

    m_OrigCommsSettings.mplCommCfg  = mplCommCfg;
    m_OrigCommsSettings.baseCommCfg = baseCommCfg;
    m_OrigCommsSettings.mgmtCommCfg = mgmtCommCfg;

    SetCommsCtrls( mplCommCfg,   m_MPLCommsCtrls );
    SetCommsCtrls( baseCommCfg,  m_BaseCommsCtrls );
    SetCommsCtrls( mgmtCommCfg,  m_MgmtCommsCtrls );

    bool bAutoAssignPorts = GetAutoAssignCommPorts();

    m_OrigCommsSettings.bAutoAssignPorts = bAutoAssignPorts;

    AutoAssignPortsCB->Checked = bAutoAssignPorts;

    // Data Logging
    String logFileName;

    EnableLoggingCB->Checked = GetDataLogging( DLT_RAW_DATA, logFileName );
    LogFileNameEdit->Text     = logFileName;

    // Radio Parameters - no need to save this to original config struct
    // because a change doesn't require notification to the caller
    RadioChanTimeoutEdit->Text = UIntToStr( (UINT)( GetRadioChanWaitTimeout() / 1000 ) );

    // Wireless Parameters - no need to save these to original config struct
    // because a change doesn't require notification to the caller
    WIRELESS_TIME_PARAMS wirelessTimeParams;
    GetWirelessTimingParams( wirelessTimeParams );

    RFCRateEdit->Text     = UIntToStr( (UINT)wirelessTimeParams.rfcRate );
    RFCTimeoutEdit->Text  = UIntToStr( (UINT)wirelessTimeParams.rfcTimeout );
    PairTimeoutEdit->Text = UIntToStr( (UINT)wirelessTimeParams.pairingTimeout );

    // Show current MPL radio channel number
    BRRadNbrComboClick( BRRadNbrCombo );
}


void TSystemSettingsForm::LoadCalPage( void )
{
    FormattedCalDataEditor->Strings->Clear();
    RawCalDataEditor->Strings->Clear();

    // Load raw data view first
    if( m_poller != NULL )
    {
       eMPLSUnit whichUnit = (eMPLSUnit)( TECUnitCombo->ItemIndex );

       TStringList* pCaptions = new TStringList();
       TStringList* pValues   = new TStringList();

       if( m_poller->GetCalDataRaw( whichUnit, pCaptions, pValues ) )
       {
           for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
               RawCalDataEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
       }

       if( m_poller->GetCalDataFormatted( whichUnit, pCaptions, pValues ) )
       {
           for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
               FormattedCalDataEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
       }

       delete pCaptions;
       delete pValues;
    }

    // Can only change battery if we have a poller and it is a real device.
    NewBattBtn->Enabled = ( ( m_poller != NULL ) && ( m_poller->MPLType == MT_MPL ) );

    // The WriteToDevBtn's tag is used to indicate if the user has
    // uploaded new data. Clear it on load.
    WriteToDevBtn->Tag = 0;
}


void TSystemSettingsForm::LoadMPLSPage( void )
{
    // Launch params. Values in registry are in msecs, but screen values are in secs
    LaunchClkTimeoutEdit->Text = GetLaunchClickTimeout() / 1000;
    LaunchDurEdit->Text        = GetSolenoidOnTime( SOTT_LAUNCH ) / 1000;
    CleanDurEdit->Text         = GetSolenoidOnTime( SOTT_CLEAN  ) / 1000;
    ResetFlagDurEdit->Text     = GetSolenoidOnTime( SOTT_RESET  ) / 1000;

    m_OrigMPLSSettings.dwLaunchClkTimeout = GetLaunchClickTimeout();
    m_OrigMPLSSettings.dwLaunchDuration   = GetSolenoidOnTime( SOTT_LAUNCH );
    m_OrigMPLSSettings.dwCleanDuration    = GetSolenoidOnTime( SOTT_CLEAN  );
    m_OrigMPLSSettings.dwResetDuration    = GetSolenoidOnTime( SOTT_RESET  );

    // Load battery voltage thresholds
    GetBatteryThresholds( m_OrigMPLSSettings.battThresh );

    BattMinValidVoltsEdit->Text = FloatToStrF( m_OrigMPLSSettings.battThresh.fReadingValidThresh, ffFixed, 7, 3 );
    BattMinOperVoltsEdit->Text  = FloatToStrF( m_OrigMPLSSettings.battThresh.fMinOperValue,       ffFixed, 7, 3 );
    BattGoodVoltsEdit->Text     = FloatToStrF( m_OrigMPLSSettings.battThresh.fGoodThresh,         ffFixed, 7, 3 );

    // Load alarm out setting
    m_OrigMPLSSettings.bAlarmOutEn = GetAlarmOutOnBREnabled();
    EnableAlarmOutCB->Checked      = m_OrigMPLSSettings.bAlarmOutEn;

    // Load data averaging form registry. Format of the factor value
    // depends on the averaging type.
    GetAveragingSettings( AT_RPM,   m_OrigMPLSSettings.rpmAvgParams   );
    GetAveragingSettings( AT_PRESS, m_OrigMPLSSettings.pressAvgParams );

    SetAveragingParams( m_OrigMPLSSettings.rpmAvgParams,   RPMAvgCombo,   RPMAvgVarEdit );
    SetAveragingParams( m_OrigMPLSSettings.pressAvgParams, PressAvgCombo, PressAvgVarEdit );
}


void TSystemSettingsForm::LoadMiscPage( void )
{
    // Clear password field edits
    CurrPWEdit->Text = "";
    NewPWEdit1->Text = "";
    NewPWEdit2->Text = "";

    // Load job data dir
    JobDirEdit->Text = GetJobDataDir();

    m_OrigMiscSettings.sJobDir = JobDirEdit->Text;

    SetJobStatusBtns();

    // Get process priority. Force default setting to start, but
    // override that when we find the priority in the list.
    ProcPriorityCombo->ItemIndex = 0;

    DWORD procPriority = GetProcessPriority();

    m_OrigMiscSettings.dwProcPriority = procPriority;

    for( int iItem = 0; iItem < NBR_PROC_PRIORITIES; iItem++ )
    {
        if( ProcPriorities[iItem].procEnum == procPriority )
        {
            ProcPriorityCombo->ItemIndex = iItem;
            break;
        }
    }

    // Get the backup save settings
    String sBackupDir;

    bool bEnableBackup = GetForceBackup( sBackupDir );

    BackupDirEdit->Text   = sBackupDir;
    BackupSaveCB->Checked = bEnableBackup;

    m_OrigMiscSettings.sBackupDir       = sBackupDir;
    m_OrigMiscSettings.bForceBackupSave = bEnableBackup;
}


TWinControl* TSystemSettingsForm::CheckCommsPage( void )
{
    COMMS_CFG temp;

    TWinControl* commsErrCtrl = GetCommsCtrls( temp, m_MPLCommsCtrls );

    if( commsErrCtrl != NULL )
    {
        DevConnPgCtrl->ActivePage = MPLSheet;
        return commsErrCtrl;
    }

    commsErrCtrl = GetCommsCtrls( temp, m_BaseCommsCtrls );

    if( commsErrCtrl != NULL )
    {
        DevConnPgCtrl->ActivePage = BaseRadioSheet;
        return commsErrCtrl;
    }

    if( EnableLoggingCB->Checked )
    {
        if( LogFileNameEdit->Text.Trim().Length() == 0 )
        {
            MessageDlg( "You must specify a file name to enable logging.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return LogFileNameEdit;
        }
    }

    int iTestValue = RadioChanTimeoutEdit->Text.ToIntDef( 0 );

    if( iTestValue <= 0 )
    {
        MessageDlg( "You have entered an invalid Radio Channel Timeout value.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return RadioChanTimeoutEdit;
    }

    // RFC rate is in msecs, must be >= 250 and <= 65535
    iTestValue = RFCRateEdit->Text.ToIntDef( -1 );

    if( ( iTestValue < 250 ) || ( iTestValue > 65535 ) )
    {
        MessageDlg( "You have entered an invalid RFC Rate value.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return RFCRateEdit;
    }

    // RFC timeout is in msecs, must be >= 0 and <= 65535
    iTestValue = RFCTimeoutEdit->Text.ToIntDef( -1 );

    if( ( iTestValue < 0 ) || ( iTestValue > 65535 ) )
    {
        MessageDlg( "You have entered an invalid RFC Timeout value.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return RFCTimeoutEdit;
    }

    // Pairing timeout is in secs, must be >= 0 and <= 65535
    iTestValue = PairTimeoutEdit->Text.ToIntDef( -1 );

    if( ( iTestValue < 0 ) || ( iTestValue > 65535 ) )
    {
        MessageDlg( "You have entered an invalid Pairing Timeout value.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return PairTimeoutEdit;
    }

    // Fall through means all page controls valid
    return NULL;
}


TWinControl* TSystemSettingsForm::CheckCalPage( void )
{
    // If a calibration has been loaded, but not written to the device,
    // warn the user.
    if( WriteToDevBtn->Tag == 1 )
    {
        int mrResult = MessageDlg( "You have loaded calibration data but not written the data to the device. "
                                   "If you exit now, the software will continue to use this calibration data, "
                                   "but the data stored on the device will be different."
                                   "\r  Press OK to continue with different calibrations."
                                   "\r  Press Cancel to reconsider.",
                                   mtWarning, TMsgDlgButtons() << mbOK << mbCancel, 0 );

        if( mrResult != mrOk )
            return FormattedCalDataEditor;
    }

    return NULL;
}


TWinControl* TSystemSettingsForm::CheckMPLSPage( void )
{
    // Launch params must be positive ints
    int iTestValue = LaunchDurEdit->Text.ToIntDef( 0 );

    if( iTestValue <= 0 )
    {
        MessageDlg( "Launch Duration is not valid.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return LaunchDurEdit;
    }

    iTestValue = CleanDurEdit->Text.ToIntDef( 0 );

    if( iTestValue <= 0 )
    {
        MessageDlg( "Cleaning Duration is not valid.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return CleanDurEdit;
    }

    iTestValue = ResetFlagDurEdit->Text.ToIntDef( 0 );

    if( iTestValue <= 0 )
    {
        MessageDlg( "ResetFlag Duration is not valid.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return ResetFlagDurEdit;
    }

    // Check battery thresholds
    float fTestValue = StrToFloatDef( BattMinValidVoltsEdit->Text, 0.0 );

    if( fTestValue <= 0.0 )
    {
        MessageDlg( "You have specified an invalid Minimum Valid Reading for the Battery Threshold.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return BattMinValidVoltsEdit;
    }

    fTestValue = StrToFloatDef( BattMinOperVoltsEdit->Text, 0.0 );

    if( fTestValue <= 0.0 )
    {
        MessageDlg( "You have specified an invalid Minimum Operating Voltage for the Battery Threshold.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return BattMinOperVoltsEdit;
    }

    fTestValue = StrToFloatDef( BattGoodVoltsEdit->Text, 0.0 );

    if( fTestValue <= 0.0 )
    {
        MessageDlg( "You have specified an invalid Good Voltage Threshold for the Battery Threshold.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return BattGoodVoltsEdit;
    }

    // Check averaging param
    AVERAGING_PARAMS dummyParams;

    if( !GetAveragingParams( dummyParams, RPMAvgCombo, RPMAvgVarEdit ) )
    {
        MessageDlg( "You have specified an invalid RPM Averaging parameters.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return RPMAvgVarEdit;
    }

    if( !GetAveragingParams( dummyParams, RPMAvgCombo, RPMAvgVarEdit ) )
    {
        MessageDlg( "You have specified an invalid Pressure Averaging parameters.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return PressAvgVarEdit;
    }

    return NULL;
}


TWinControl* TSystemSettingsForm::CheckMiscPage( void )
{
    // Enforce that the job directory exists
    if( !IsValidJobLogFolder( JobDirEdit->Text, true ) )
    {
        if( JobDirEdit->Text.Length() == 0 )
            MessageDlg( "You must specify a directory to save Jobs in.", mtError, TMsgDlgButtons() << mbOK, 0 );
        else
            MessageDlg( "The Jobs Directory you have selected cannot be used. Please select another directory.", mtError, TMsgDlgButtons() << mbOK, 0 );

        return JobDirEdit;
    }

    return NULL;
}


DWORD TSystemSettingsForm::SaveCommsPage( void )
{
    // All controls guaranteed to be valid with this function is called.
    bool bSettingsChanged = false;

    // Save comms settings
    COMMS_CFG mplCommCfg;
    COMMS_CFG baseCommCfg;
    COMMS_CFG mgmtCommCfg;

    GetCommsCtrls( mplCommCfg,   m_MPLCommsCtrls  );
    GetCommsCtrls( baseCommCfg,  m_BaseCommsCtrls );
    GetCommsCtrls( mgmtCommCfg,  m_MgmtCommsCtrls );

    if( ( mplCommCfg.portType != m_OrigCommsSettings.mplCommCfg.portType ) || ( mplCommCfg.portName != m_OrigCommsSettings.mplCommCfg.portName ) || ( mplCommCfg.portParam != m_OrigCommsSettings.mplCommCfg.portParam ) )
        bSettingsChanged = true;

    if( ( baseCommCfg.portType != m_OrigCommsSettings.baseCommCfg.portType ) || ( baseCommCfg.portName != m_OrigCommsSettings.baseCommCfg.portName ) || ( baseCommCfg.portParam != m_OrigCommsSettings.baseCommCfg.portParam ) )
        bSettingsChanged = true;

    if( ( mgmtCommCfg.portType != m_OrigCommsSettings.mgmtCommCfg.portType ) || ( mgmtCommCfg.portName != m_OrigCommsSettings.mgmtCommCfg.portName ) || ( mgmtCommCfg.portParam != m_OrigCommsSettings.mgmtCommCfg.portParam ) )
        bSettingsChanged = true;

    SaveCommSettings( DCT_MPL,  mplCommCfg );
    SaveCommSettings( DCT_BASE, baseCommCfg );
    SaveCommSettings( DCT_MGMT, mgmtCommCfg );

    if( AutoAssignPortsCB->Checked != m_OrigCommsSettings.bAutoAssignPorts )
        bSettingsChanged = true;

    SaveAutoAssignCommPorts( AutoAssignPortsCB->Checked );

    // Save raw logging settings. Note that change of log file doesn't
    // warrant setting bSettingsChanged true.
    SaveDataLogging( DLT_RAW_DATA, EnableLoggingCB->Checked, LogFileNameEdit->Text );

    // Get radio timeout. A change in its value does not warrant
    // setting bSettingsChanged true.
    SaveRadioChanWaitTimeout( RadioChanTimeoutEdit->Text.ToInt() * 1000 );

    // Wireless rates do not warrant a bSettingsChanged indication
    WIRELESS_TIME_PARAMS wirelessTimeParams;

    wirelessTimeParams.rfcRate        = RFCRateEdit->Text.ToInt();
    wirelessTimeParams.rfcTimeout     = RFCTimeoutEdit->Text.ToInt();
    wirelessTimeParams.pairingTimeout = PairTimeoutEdit->Text.ToInt();

    SaveWirelessTimingParams( wirelessTimeParams );

    if( bSettingsChanged )
        return COMMS_SETTINGS_CHANGED;
    else
        return 0; /* 0 = no settings changed */
}


DWORD TSystemSettingsForm::SaveCalPage( void )
{
    bool bSettingsChanged = false;

    // Nothing to save on this page.
    if( bSettingsChanged )
        return CAL_SETTINGS_CHANGED;

    return 0;
}


DWORD TSystemSettingsForm::SaveMPLSPage( void )
{
    bool bSettingsChanged = false;

    // Launch dur timeout
    DWORD dwValue = LaunchDurEdit->Text.ToInt() * 1000;

    if( dwValue != m_OrigMPLSSettings.dwLaunchDuration )
        bSettingsChanged = true;

    SaveSolenoidOnTime( SOTT_LAUNCH, dwValue );

    // Cleaning dur timeout
    dwValue = CleanDurEdit->Text.ToInt() * 1000;

    if( dwValue != m_OrigMPLSSettings.dwCleanDuration )
        bSettingsChanged = true;

    SaveSolenoidOnTime( SOTT_CLEAN, dwValue );

    // Reset flag dur timeout
    dwValue = ResetFlagDurEdit->Text.ToInt() * 1000;

    if( dwValue != m_OrigMPLSSettings.dwResetDuration )
        bSettingsChanged = true;

    SaveSolenoidOnTime( SOTT_RESET, dwValue );

    // Launch btn click timeout
    dwValue = LaunchClkTimeoutEdit->Text.ToInt() * 1000;

    if( dwValue != m_OrigMPLSSettings.dwLaunchClkTimeout )
        bSettingsChanged = true;

    SaveLaunchClickTimeout( dwValue );

    // Save battery thresholds
    BATT_THRESH_VALS newBattThresh;

    newBattThresh.fReadingValidThresh = BattMinValidVoltsEdit->Text.ToDouble();
    newBattThresh.fMinOperValue       = BattMinOperVoltsEdit->Text.ToDouble();
    newBattThresh.fGoodThresh         = BattGoodVoltsEdit->Text.ToDouble();

    SaveBatteryThresholds( newBattThresh );

    const float MinBattDelta = 0.001;

    if( fabs( m_OrigMPLSSettings.battThresh.fReadingValidThresh - newBattThresh.fReadingValidThresh ) >= MinBattDelta )
        bSettingsChanged = true;
    else if( fabs( m_OrigMPLSSettings.battThresh.fMinOperValue - newBattThresh.fMinOperValue ) >= MinBattDelta )
        bSettingsChanged = true;
    else if( fabs( m_OrigMPLSSettings.battThresh.fGoodThresh - newBattThresh.fGoodThresh ) >= MinBattDelta )
        bSettingsChanged = true;

    // Save alarm out setting
    SaveAlarmOutOnBREnabled( EnableAlarmOutCB->Checked );

    bSettingsChanged = ( EnableAlarmOutCB->Checked != m_OrigMPLSSettings.bAlarmOutEn );

    // Save averaging. Values are guaranteed to be valid. Init the newParams with
    // the current ones read from the registry as only certain members of the
    // new params get overwritten
    AVERAGING_PARAMS newParams = m_OrigMPLSSettings.rpmAvgParams;

    GetAveragingParams( newParams, RPMAvgCombo, RPMAvgVarEdit );
    SaveAveragingSettings( AT_RPM, newParams );

    bSettingsChanged |= ( newParams.avgLevel    != m_OrigMPLSSettings.rpmAvgParams.avgLevel );
    bSettingsChanged |= ( newParams.absVariance != m_OrigMPLSSettings.rpmAvgParams.absVariance );

    newParams = m_OrigMPLSSettings.pressAvgParams;

    GetAveragingParams( newParams, PressAvgCombo, PressAvgVarEdit );
    SaveAveragingSettings( AT_PRESS, newParams );

    bSettingsChanged |= ( newParams.avgLevel    != m_OrigMPLSSettings.pressAvgParams.avgLevel );
    bSettingsChanged |= ( newParams.absVariance != m_OrigMPLSSettings.pressAvgParams.absVariance );

    // Nothing to save on this page.
    if( bSettingsChanged )
        return MPLS_SETTINGS_CHANGED;
    else
        return 0;
}


DWORD TSystemSettingsForm::SaveMiscPage( void )
{
    // All controls guaranteed to be valid with this function is called.
    bool bSettingsChanged = false;

    // Save job data dir, check if it has changed as well
    if( JobDirEdit->Text != m_OrigMiscSettings.sJobDir )
        bSettingsChanged = true;

    SetJobDataDir( JobDirEdit->Text );

    if( ProcPriorities[ ProcPriorityCombo->ItemIndex ].procEnum != m_OrigMiscSettings.dwProcPriority )
        bSettingsChanged = true;

    // Save process setting
    SetProcessPriority( ProcPriorities[ ProcPriorityCombo->ItemIndex ].procEnum );

    if( BackupDirEdit->Text != m_OrigMiscSettings.sBackupDir )
        bSettingsChanged = true;

    if( BackupSaveCB->Checked != m_OrigMiscSettings.bForceBackupSave )
        bSettingsChanged = true;

    SaveForceBackup( BackupSaveCB->Checked, BackupDirEdit->Text );

    // Return the flag for this page if any settings have been changed
    if( bSettingsChanged )
        return MISC_SETTINGS_CHANGED;

    return 0;
}


//
// Local Event Handlers
//

void __fastcall TSystemSettingsForm::RFCPollTimerTimer(TObject *Sender)
{
    // If we have a device, poll and display its last status
    if( m_poller != NULL )
    {
        TStringList* pCaptions = new TStringList();
        TStringList* pValues   = new TStringList();

        if( m_poller->GetLastStatusPktInfo( pCaptions, pValues ) )
        {
            for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
                LastRFCEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
        }

        // If either the Read From Dev or the Write To Dev buttons are
        // disabled and we the poller is idle, assume that a read/write
        // operation has completed. Reload the cal data in that case.
        if( m_poller->MPLIsIdle )
        {
            if( !WriteToDevBtn->Enabled || !ReadFromDevBtn->Enabled )
                LoadCalPage();
        }

        // Update the cal page read/write to device buttons. These are
        // only enabled if the device is idle
        WriteToDevBtn->Enabled  = m_poller->MPLIsIdle;
        ReadFromDevBtn->Enabled = m_poller->MPLIsIdle;

        // Check for base radio status packet.
        if( m_poller->GetLastRadioStatusPktInfo( pCaptions, pValues ) )
        {
            for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
                LastStatusEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
        }

        delete pCaptions;
        delete pValues;
    }
    else
    {
        WriteToDevBtn->Enabled  = false;
        ReadFromDevBtn->Enabled = false;
    }
}


void __fastcall TSystemSettingsForm::SaveCalDatBtnClick(TObject *Sender)
{
    // Save the data as is in the current view.
    if( SaveCalDataDlg->Execute() )
    {
        TIniFile* pIniFile = new TIniFile( SaveCalDataDlg->FileName );

        pIniFile->EraseSection( calIniDataSection );

        // For each key in the editor, look for a corresponding section in the
        // ini file. If the section is not found, skip it.
        for( int iKey = 1; iKey <= FormattedCalDataEditor->Strings->Count; iKey++ )
        {
            UnicodeString sKey   = FormattedCalDataEditor->Keys[iKey];
            UnicodeString sValue = FormattedCalDataEditor->Values[sKey];

            pIniFile->WriteString( calIniDataSection, sKey, sValue );
        }

        pIniFile->UpdateFile();

        delete pIniFile;
    }
}


void __fastcall TSystemSettingsForm::LoadCalDatBtnClick(TObject *Sender)
{
    // Load the calibration data. Can only do this from the formatted view.
    if( CalDataPgCtrl->ActivePage != DevMemorySheet )
    {
        MessageDlg( "You can only load data in the Device Memory view.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    if( OpenCalDataDlg->Execute() )
    {
        TStringList* pCaptions = new TStringList();
        TStringList* pValues   = new TStringList();

        try
        {
            TIniFile* pIniFile = new TIniFile( OpenCalDataDlg->FileName );

            // For each key in the editor, look for a corresponding section in the
            // ini file. If the section is not found, skip it.
            for( int iKey = 1; iKey <= FormattedCalDataEditor->Strings->Count; iKey++ )
            {
                String sKey     = FormattedCalDataEditor->Keys[iKey];
                String newValue = pIniFile->ReadString( calIniDataSection, sKey, "" );

                if( newValue.Length() > 0 )
                {
                    FormattedCalDataEditor->Values[ sKey ] = newValue;
                }
            }

            delete pIniFile;

            // Now write the values to the software cache (not to the device yet though)
            for( int iKey = 1; iKey <= FormattedCalDataEditor->Strings->Count; iKey++ )
            {
                String sKey = FormattedCalDataEditor->Keys[iKey];

                pCaptions->Add( sKey );
                pValues->Add( FormattedCalDataEditor->Values[sKey] );
            }

            eMPLSUnit whichUnit = (eMPLSUnit)( TECUnitCombo->ItemIndex );

            if( !m_poller->SetCalDataFormatted( whichUnit, pCaptions, pValues ) )
                Abort();
        }
        catch( ... )
        {
            MessageDlg( "Load calibration data failed.", mtError, TMsgDlgButtons() << mbOK, 0 );
        }

        delete pCaptions;
        delete pValues;

        // Regardless of the outcome, always refresh the cal data grids with
        // what is in the cache
        LoadCalPage();
    }
}


void __fastcall TSystemSettingsForm::WriteToDevBtnClick(TObject *Sender)
{
/* Current version of MPLS firmware does not support uploads of cal data

    // Write the data to the device. On success, set the 'write pending' indication
    eMPLSUnit whichUnit = (eMPLSUnit)( TECUnitCombo->ItemIndex );

    if( m_poller->WriteCalDataToDevice( whichUnit ) )
        WriteToDevBtn->Tag = 1;
    else
        MessageDlg( "Could not write calibration data to device.", mtError, TMsgDlgButtons() << mbOK, 0 );

    // Call the timer tick function to update the current status
    RFCPollTimerTimer( RFCPollTimer );
*/
}


void __fastcall TSystemSettingsForm::ReadFromDevBtnClick(TObject *Sender)
{
/* Current version of MPLS firmware does not support downloading of cal data

    // On success, clear any 'write pending' indication
    eMPLSUnit whichUnit = (eMPLSUnit)( TECUnitCombo->ItemIndex );

    if( m_poller->ReloadCalDataFromDevice( whichUnit ) )
        WriteToDevBtn->Tag = 0;
    else
        MessageDlg( "Could not read calibration data from device.", mtError, TMsgDlgButtons() << mbOK, 0 );

    // Call the timer tick function to update the current status
    RFCPollTimerTimer( RFCPollTimer );
*/
}


void __fastcall TSystemSettingsForm::TECUnitComboClick(TObject *Sender)
{
    LoadCalPage();
}


void __fastcall TSystemSettingsForm::NewBattBtnClick(TObject *Sender)
{
/* TODO - determine if this functionality required.
          Battery type determined by hardware now
          Not sure if we need battery capacity

    BYTE battType;
    WORD capacity;

    if( TNewBattForm::ShowDlg( battType, capacity ) )
    {
        // We need only pass a list of the cal items that need to be updated to the comms mgr
        TStringList* pCaptions = new TStringList();
        TStringList* pValues   = new TStringList();

        pCaptions->Add( TMPLDevice::CalFactorCaptions[ TMPLDevice::CI_BATTERY_TYPE ] );
        pValues->Add  ( IntToStr( battType ) );

        pCaptions->Add( TMPLDevice::CalFactorCaptions[ TMPLDevice::CI_BATTERY_CAPACITY ] );
        pValues->Add  ( IntToStr( capacity ) );

        if( !m_poller->SetCalDataFormatted( pCaptions, pValues ) )
        {
            MessageDlg( "Could not set new battery information.", mtError, TMsgDlgButtons() << mbOK, 0 );

            delete pCaptions;
            delete pValues;

            return;
        }

        delete pCaptions;
        delete pValues;

        if( !m_poller->WriteCalDataToDevice() )
        {
            MessageDlg( "Could not write calibration data to device.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return;
        }

        // Now wait up to 30 seconds for the cal to upload. Device will return to idle
        // when upload is done.
        NewBattBtn->Enabled = false;
        Screen->Cursor = crHourGlass;

        TDateTime timeoutTime = IncSecond( Now(), 30 );

        while( !m_poller->MPLIsIdle )
        {
            // Let the appl process its messages while in this loop
            Application->ProcessMessages();

            if( Now() > timeoutTime )
                break;
        }

        if( m_poller->MPLIsIdle )
             MessageDlg( "New battery information successfully uploaded.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
        else
             MessageDlg( "Upload of new battery information failed.", mtError, TMsgDlgButtons() << mbOK, 0 );

        Screen->Cursor = crDefault;
        NewBattBtn->Enabled = true;
    }
*/
}


void __fastcall TSystemSettingsForm::CompleteJobBtnClick(TObject *Sender)
{
    // Main form takes care of this request
    Application->MainForm->Perform( WM_COMPLETE_JOB, 0, 0 );
    SetJobStatusBtns();
}


void __fastcall TSystemSettingsForm::ReopenJobBtnClick(TObject *Sender)
{
    // Main form takes care of this request
    Application->MainForm->Perform( WM_REOPEN_JOB, 0, 0 );
    SetJobStatusBtns();
}


void TSystemSettingsForm::SetJobStatusBtns( void )
{
    // We only use the current job to set the state of the job status buttons
    if( ( m_currJob == NULL ) || m_currJob->IsTestJob )
    {
        CompleteJobBtn->Enabled = false;
        ReopenJobBtn->Enabled   = false;
    }
    else if( m_currJob->Completed )
    {
        CompleteJobBtn->Enabled = false;
        ReopenJobBtn->Enabled   = true;
    }
    else
    {
        CompleteJobBtn->Enabled = true;
        ReopenJobBtn->Enabled   = false;
    }
}


void __fastcall TSystemSettingsForm::SendChangeChanBtnClick(TObject *Sender)
{
    if( m_poller != NULL )
    {
        if( BRChanCombo->ItemIndex >= 0 )
        {
            eBaseRadioNbr eSelRadio = (eBaseRadioNbr)( BRRadNbrCombo->ItemIndex );

            switch( m_poller->SetRadioChannel( eSelRadio, BRChanCombo->Items->Strings[ BRChanCombo->ItemIndex ].ToInt() ) )
            {
                case TBaseRadioDevice::SRCR_SUCCESS:
                    break;

                case TBaseRadioDevice::SRCR_NO_RADIO:
                    MessageDlg( "You cannot issue this command if you are not connected to a base radio.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    break;

                case TBaseRadioDevice::SRCR_IN_USE:
                    MessageDlg( "The selected channel is in use by another radio. Please try again with a different channel.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    break;

                default:
                    MessageDlg( "The command failed: there is a problem with the serial port.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    break;
            }
        }
    }
}


void __fastcall TSystemSettingsForm::AutoAssignPortsCBClick(TObject *Sender)
{
    // If we're auto-assigning ports, then disable everything
    if( AutoAssignPortsCB->Checked )
    {
        for( int iCtrl = 0; iCtrl < PortsGB->ControlCount; iCtrl++ )
        {
            TComboBox* pCombo = dynamic_cast<TComboBox*>( PortsGB->Controls[iCtrl] );

            if( pCombo != NULL )
            {
                // Disable the 'onclick' handler here so that it doesn't fire
                // and undo our work if the item index changes
                TNotifyEvent pOnComboClick = pCombo->OnClick;
                pCombo->OnClick = NULL;

                pCombo->ItemIndex = CT_SERIAL;  // serial
                pCombo->Enabled   = false;

                // Okay to restore click handler now
                pCombo->OnClick = pOnComboClick;
                continue;
            }

            TEdit* pEdit = dynamic_cast<TEdit*>( PortsGB->Controls[iCtrl] );

            if( pEdit != NULL )
                pEdit->Enabled   = false;

        }

        // Get the last saved settings
        COMMS_CFG radioCommCfg;
        COMMS_CFG deviceCommCfg;
        COMMS_CFG mgmtCommCfg;

        GetCommSettings( DCT_BASE, radioCommCfg );
        GetCommSettings( DCT_MPL,  deviceCommCfg );
        GetCommSettings( DCT_MGMT, mgmtCommCfg );

        // Set the Port Numbers to that from the registry
        BaseRadioPortEdit->Text = radioCommCfg.portName;
        MPLPortEdit->Text       = deviceCommCfg.portName;
        MgmtDevPortEdit->Text   = mgmtCommCfg.portName;

        // If any of the texts are empty, give them a default value
        if( ( radioCommCfg.portName.IsEmpty() ) || ( deviceCommCfg.portName.IsEmpty() ) || ( mgmtCommCfg.portName.IsEmpty() ) )
        {
            BaseRadioPortEdit->Text = IntToStr( BASESTN_CONTROL_PORT_OFFSET + 1 );
            MPLPortEdit->Text       = IntToStr( BASESTN_TESTORK_RADIO_PORT_OFFSET + 1 );
            MgmtDevPortEdit->Text   = IntToStr( BASESTN_MGMT_PORT_OFFSET );
        }

        // When auto assigning, all ports are at 115200
        BaseRadioParamEdit->Text = "115200";
        MPLParamEdit->Text       = "115200";
        MgmtDevParamEdit->Text   = "115200";
    }
    else
    {
        // Not auto-assigning. Enable all edits
        for( int iCtrl = 0; iCtrl < PortsGB->ControlCount; iCtrl++ )
        {
            TComboBox* pCombo = dynamic_cast<TComboBox*>( PortsGB->Controls[iCtrl] );

            if( pCombo != NULL )
            {
                pCombo->Enabled = true;
                continue;
            }

            TEdit* pEdit = dynamic_cast<TEdit*>( PortsGB->Controls[iCtrl] );

            if( pEdit != NULL )
            {
                pEdit->Enabled = true;
                pEdit->Color   = clWindow;
                continue;
            }
        }
    }
}


void __fastcall TSystemSettingsForm::BrowseBackupDirBtnClick(TObject *Sender)
{
    UnicodeString sBackupDir = JobDirEdit->Text;

    if( SelectDirectory( "Select Backup Directory", "Desktop", sBackupDir, TSelectDirExtOpts() << sdNewUI << sdNewFolder, NULL ) )
        BackupDirEdit->Text = sBackupDir;
}


void __fastcall TSystemSettingsForm::BaseRadioPortTypeComboClick(TObject *Sender)
{
    if( BaseRadioPortTypeCombo->ItemIndex == CT_UNUSED )
    {
        BaseRadioPortEdit->Text     = "";
        BaseRadioParamEdit->Text    = "";
        BaseRadioPortEdit->Enabled  = false;
        BaseRadioParamEdit->Enabled = false;
    }
    else if( MPLPortTypeCombo->ItemIndex == CT_UDP )
    {
        BaseRadioPortEdit->Text     = "";
        BaseRadioParamEdit->Text    = "62220";
        BaseRadioPortEdit->Enabled  = false;
        BaseRadioParamEdit->Enabled = true;
    }
    else
    {
        BaseRadioPortEdit->Enabled  = true;
        BaseRadioParamEdit->Enabled = true;
    }
}


void __fastcall TSystemSettingsForm::MPLPortTypeComboClick(TObject *Sender)
{
    if( MPLPortTypeCombo->ItemIndex == CT_UNUSED )
    {
        MPLPortEdit->Text     = "";
        MPLParamEdit->Text    = "";
        MPLPortEdit->Enabled  = false;
        MPLParamEdit->Enabled = false;
    }
    else if( MPLPortTypeCombo->ItemIndex == CT_UDP )
    {
        MPLPortEdit->Text     = "";
        MPLParamEdit->Text    = "62222";
        MPLPortEdit->Enabled  = false;
        MPLParamEdit->Enabled = true;
    }
    else
    {
        MPLPortEdit->Enabled  = true;
        MPLParamEdit->Enabled = true;
    }
}


void __fastcall TSystemSettingsForm::MgmtDevPortTypeComboClick(TObject *Sender)
{
    if( MgmtDevPortTypeCombo->ItemIndex == CT_UNUSED )
    {
        MgmtDevPortEdit->Text     = "";
        MgmtDevParamEdit->Text    = "";
        MgmtDevPortEdit->Enabled  = false;
        MgmtDevParamEdit->Enabled = false;
    }
    else if( MPLPortTypeCombo->ItemIndex == CT_UDP )
    {
        MgmtDevPortEdit->Text     = "127.0.0.1";
        MgmtDevParamEdit->Text    = "62224";
        MgmtDevPortEdit->Enabled  = true;
        MgmtDevParamEdit->Enabled = true;
    }
    else
    {
        MgmtDevPortEdit->Enabled  = true;
        MgmtDevParamEdit->Enabled = true;
    }
}


bool TSystemSettingsForm::IsValidJobLogFolder( const String& sFolderName, bool bCreateFolder )
{
    // Returns true if the folder exists and can be written to. If the
    // folder does not exist, the method will attempt to create the
    // folder first if bCreateFolder is true. This is a static method.

    // Simple check first
    if( sFolderName.Length() == 0 )
        return false;

    // Check the drive type
    String sDrive = IncludeTrailingBackslash( ExtractFileDrive( sFolderName ) );

    UINT uiDriveType = GetDriveType( sDrive.w_str() );

    // Only check if the drive type could be determined
    if( uiDriveType != DRIVE_UNKNOWN )
    {
        // We only want a fixed drive
        if( uiDriveType != DRIVE_FIXED )
            return false;
    }

    // Check that the folder exists
    if( !DirectoryExists( sFolderName ) )
    {
        // Try to create the folder
        if( !ForceDirectories( sFolderName ) )
            return false;
    }

    // Lastly, check that we can write to it
    String sTestFileName = IncludeTrailingBackslash( sFolderName ) + "MPL" + IntToStr( (int)time( NULL ) ) + ".tmp";

    TStringList* pTestList = new TStringList();
    pTestList->Add( "MPL access test" );

    bool bAccessOK = false;

    try
    {
        pTestList->SaveToFile( sTestFileName );
        bAccessOK = true;
    }
    catch( ... )
    {
    }

    // Clean up, being sure to delete our temp file as well
    delete pTestList;

    DeleteFile( sTestFileName );

    // Return result of access test
    return bAccessOK;
}


void __fastcall TSystemSettingsForm::BRRadNbrComboClick(TObject *Sender)
{
    // Repopulate the channel list when changing antennas.
    eBaseRadioNbr eSelRadio = (eBaseRadioNbr)( BRRadNbrCombo->ItemIndex );

    // If the selected radio is 1, then assume this is for the TesTORK system
    WIRELESS_SYSTEM_INFO wirelessInfo;

    if( eSelRadio == eBRN_1 )
        GetWirelessSystemInfo( eWS_TesTork, wirelessInfo );
    else
        GetWirelessSystemInfo( eWS_MPLS2, wirelessInfo );

    // Repopulate the combo
    BRChanCombo->Items->Clear();

    for( int iItem = 0; iItem < MAX_CHANS_PER_SYSTEM; iItem++ )
    {
        if( wirelessInfo.chanList[iItem] > 0 )
            BRChanCombo->Items->Add( IntToStr( wirelessInfo.chanList[iItem] ) );
    }

    // Now select the channel based on the currently active chan from the radio
    BRChanCombo->ItemIndex = -1;

    if( m_poller != NULL )
    {
        String sCurrChan = IntToStr( m_poller->GetRadioChannel( eSelRadio ) );

        for( int iEntry = 0; iEntry < BRChanCombo->Items->Count; iEntry++ )
        {
            if( BRChanCombo->Items->Strings[iEntry] == sCurrChan )
            {
                BRChanCombo->ItemIndex = iEntry;
                break;
            }
        }
    }
}


