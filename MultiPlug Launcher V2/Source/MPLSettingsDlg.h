#ifndef MPLSettingsDlgH
#define MPLSettingsDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.Menus.hpp>

#include "TBasePlugJob.h"
#include "TMPLSManager.h"
#include "MPLRegistryInterface.h"


class TSystemSettingsForm : public TForm
{
__published:    // IDE-managed Components
    TPageControl *PgCtrl;
    TButton *OKBtn;
    TButton *CancelBtn;
    TTabSheet *CommsSheet;
    TTabSheet *MiscSheet;
    TSaveDialog *SaveLogDlg;
    TGroupBox *JobDirGB;
    TEdit *JobDirEdit;
    TButton *BrowseJobDirBtn;
    TGroupBox *SysAdminPWGB;
    TLabel *Label8;
    TLabel *Label9;
    TLabel *Label10;
    TEdit *CurrPWEdit;
    TEdit *NewPWEdit1;
    TEdit *NewPWEdit2;
    TButton *SavePWBtn;
    TTimer *RFCPollTimer;
    TGroupBox *JobStatusGB;
    TButton *CompleteJobBtn;
    TButton *ReopenJobBtn;
    TPageControl *DevConnPgCtrl;
    TTabSheet *MPLSheet;
    TTabSheet *BaseRadioSheet;
    TGroupBox *DataLoggingGB;
    TCheckBox *EnableLoggingCB;
    TEdit *LogFileNameEdit;
    TButton *BrowseLogFileBtn;
    TGroupBox *LastRFCGB;
    TValueListEditor *LastRFCEditor;
    TGroupBox *BRLastStatusGB;
    TGroupBox *BRCmdGB;
    TButton *SendChangeChanBtn;
    TComboBox *BRChanCombo;
    TLabel *Label39;
    TValueListEditor *LastStatusEditor;
    TComboBox *BRRadNbrCombo;
    TGroupBox *PriorityGB;
    TComboBox *ProcPriorityCombo;
    TTabSheet *PortsSheet;
    TGroupBox *PortsGB;
    TLabel *Label19;
    TCheckBox *AutoAssignPortsCB;
    TLabel *Label21;
    TLabel *Label22;
    TLabel *Label23;
    TComboBox *MPLPortTypeCombo;
    TComboBox *MgmtDevPortTypeCombo;
    TEdit *MPLPortEdit;
    TEdit *MPLParamEdit;
    TEdit *MgmtDevPortEdit;
    TEdit *MgmtDevParamEdit;
    TLabel *Label24;
    TLabel *Label20;
    TComboBox *BaseRadioPortTypeCombo;
    TEdit *BaseRadioPortEdit;
    TEdit *BaseRadioParamEdit;
    TGroupBox *BackupSaveGB;
    TCheckBox *BackupSaveCB;
    TEdit *BackupDirEdit;
    TButton *BrowseBackupDirBtn;
    TTabSheet *CalSheet;
    TPageControl *CalDataPgCtrl;
    TTabSheet *DevMemorySheet;
    TValueListEditor *FormattedCalDataEditor;
    TTabSheet *RawDataSheet;
    TValueListEditor *RawCalDataEditor;
    TButton *LoadCalDatBtn;
    TButton *SaveCalDatBtn;
    TButton *WriteToDevBtn;
    TButton *ReadFromDevBtn;
    TButton *NewBattBtn;
    TSaveDialog *SaveCalDataDlg;
    TOpenDialog *OpenCalDataDlg;
    TGroupBox *RadioConnParamsGB;
    TLabel *Label2;
    TEdit *RadioChanTimeoutEdit;
    TLabel *Label3;
    TComboBox *TECUnitCombo;
    TTabSheet *TabSheet1;
    TGroupBox *AvgingGB;
    TLabel *Label1;
    TComboBox *RPMAvgCombo;
    TGroupBox *LaunchParamsGB;
    TLabel *Label4;
    TLabel *Label13;
    TLabel *Label6;
    TLabel *Label15;
    TEdit *LaunchDurEdit;
    TEdit *LaunchClkTimeoutEdit;
    TEdit *CleanDurEdit;
    TEdit *ResetFlagDurEdit;
    TGroupBox *BattThreshGB;
    TLabel *Label11;
    TLabel *Label12;
    TLabel *Label14;
    TEdit *BattMinValidVoltsEdit;
    TEdit *BattMinOperVoltsEdit;
    TEdit *BattGoodVoltsEdit;
    TGroupBox *AlarmOutGB;
    TLabel *Label18;
    TCheckBox *EnableAlarmOutCB;
    TGroupBox *WirelessParamsGB;
    TLabel *Label7;
    TLabel *Label16;
    TLabel *Label17;
    TEdit *RFCRateEdit;
    TEdit *RFCTimeoutEdit;
    TEdit *PairTimeoutEdit;
    TLabel *Label25;
    TLabel *Label26;
    TLabel *Label27;
    TLabel *Label5;
    TComboBox *PressAvgCombo;
    TEdit *RPMAvgVarEdit;
    TEdit *PressAvgVarEdit;
    TLabel *LevelLB;
    TLabel *Label28;
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall CommTypeClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall BrowseLogFileBtnClick(TObject *Sender);
    void __fastcall SavePWBtnClick(TObject *Sender);
    void __fastcall BrowseJobDirBtnClick(TObject *Sender);
    void __fastcall RFCPollTimerTimer(TObject *Sender);
    void __fastcall ListEditorSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
    void __fastcall SaveCalDatBtnClick(TObject *Sender);
    void __fastcall LoadCalDatBtnClick(TObject *Sender);
    void __fastcall WriteToDevBtnClick(TObject *Sender);
    void __fastcall ReadFromDevBtnClick(TObject *Sender);
    void __fastcall CompleteJobBtnClick(TObject *Sender);
    void __fastcall ReopenJobBtnClick(TObject *Sender);
    void __fastcall SendChangeChanBtnClick(TObject *Sender);
    void __fastcall AutoAssignPortsCBClick(TObject *Sender);
    void __fastcall BrowseBackupDirBtnClick(TObject *Sender);
    void __fastcall BaseRadioPortTypeComboClick(TObject *Sender);
    void __fastcall MPLPortTypeComboClick(TObject *Sender);
    void __fastcall MgmtDevPortTypeComboClick(TObject *Sender);
    void __fastcall NewBattBtnClick(TObject *Sender);
    void __fastcall TECUnitComboClick(TObject *Sender);
    void __fastcall BRRadNbrComboClick(TObject *Sender);

private:
    TMPLSManager* m_poller;
    TBasePlugJob* m_currJob;

    DWORD         m_dwSettingsChanged;

    UNITS_OF_MEASURE m_uomType;

    typedef struct {
        TComboBox* pTypeCombo;
        TEdit*     pPortNbrEdit;
        TEdit*     pParamEdit;
    } COMMS_SHEET_CTRLS;

    COMMS_SHEET_CTRLS m_MPLCommsCtrls;
    COMMS_SHEET_CTRLS m_BaseCommsCtrls;
    COMMS_SHEET_CTRLS m_MgmtCommsCtrls;

    typedef struct {
        COMMS_CFG   mplCommCfg;
        COMMS_CFG   baseCommCfg;
        COMMS_CFG   mgmtCommCfg;
        bool        bAutoAssignPorts;
    } COMMS_PAGE_SETTINGS;

    COMMS_PAGE_SETTINGS m_OrigCommsSettings;

    typedef struct {
        String           sJobDir;
        DWORD            dwProcPriority;
        String           sBackupDir;
        bool             bForceBackupSave;
    } MISC_PAGE_SETTINGS;

    MISC_PAGE_SETTINGS m_OrigMiscSettings;

    typedef struct {
        DWORD            dwLaunchClkTimeout;
        DWORD            dwLaunchDuration;
        DWORD            dwCleanDuration;
        DWORD            dwResetDuration;
        bool             bAlarmOutEn;
        AVERAGING_PARAMS rpmAvgParams;
        AVERAGING_PARAMS pressAvgParams;
        BATT_THRESH_VALS battThresh;
    } MPLS_PAGE_SETTINGS;

    MPLS_PAGE_SETTINGS m_OrigMPLSSettings;

    void SetAveragingParams( const AVERAGING_PARAMS& avgParams, TComboBox* pCombo, TEdit* pVarEdit );
    bool GetAveragingParams(       AVERAGING_PARAMS& avgParams, TComboBox* pCombo, TEdit* pVarEdit );

    void         SetCommsCtrls( const COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls );
    TWinControl* GetCommsCtrls(       COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls );

    void LoadCommsPage( void );
    void LoadCalPage( void );
    void LoadMPLSPage( void );
    void LoadMiscPage( void );

    TWinControl* CheckCommsPage( void );
    TWinControl* CheckCalPage( void );
    TWinControl* CheckMPLSPage( void );
    TWinControl* CheckMiscPage( void );

    // All controls must be valid when calling the following 'Save' functions
    DWORD SaveCommsPage( void );
    DWORD SaveCalPage( void );
    DWORD SaveMPLSPage( void );
    DWORD SaveMiscPage( void );

    // Support functions
    void SetJobStatusBtns( void );

public:
    __fastcall TSystemSettingsForm(TComponent* Owner);

    __property TMPLSManager* PollObject      = { read = m_poller,  write = m_poller  };
    __property TBasePlugJob* CurrentJob      = { read = m_currJob, write = m_currJob };
    __property DWORD         SettingsChanged = { read = m_dwSettingsChanged };

    typedef enum {
        COMMS_SETTINGS_CHANGED = 0x0001,
        MISC_SETTINGS_CHANGED  = 0x0002,
        CAL_SETTINGS_CHANGED   = 0x0004,
        MPLS_SETTINGS_CHANGED  = 0x0008,
    } SETTINGS_CHANGED;

    // This is not an auto-created form. Must call the static ShowDlg() method
    // to construct, show, and then destroy the dialog.
    static DWORD ShowDlg( TMPLSManager* pPoller, TBasePlugJob* currJob );

    static bool IsValidJobLogFolder( const String& sFolderName, bool bCreateFolder );
        // Returns true if the folder exists and can be written to. If the
        // folder does not exist, the method will attempt to create the
        // folder first if bCreateFolder is true.

};

extern PACKAGE TSystemSettingsForm *SystemSettingsForm;

#endif
