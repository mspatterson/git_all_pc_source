#include <vcl.h>
#pragma hdrstop

#include "RptHelpers.h"
#include "Applutils.h"

#pragma package(smart_init)


static const String FieldTypeIdentifiers[NBR_RPT_FIELD_TYPES] = {
    "JobInfo_",
    "Graph_",
    "Event_",
    "Comment_",
};

static const String BadJobString        = "Bad Job Pointer";
static const String BadRecordString     = "Bad Record Nbr";
static const String DevNotPresentString = "(not present)";


static String BoolToYesNo( bool bValue )
{
    return bValue ? String( "Yes" ) : String( "No" );
}


static String CanisterCfgToString( eCanisterConfig eCfg )
{
    switch( eCfg )
    {
        case eCC_OneShort:   return "One short";
        case eCC_TwoShort:   return "Two short";
        case eCC_OneLong:    return "One long";
    }

    return "Not used";
}


//
// Public Functions
//

RPT_FIELD_TYPE RptGetFieldType( const String &VarName )
{
    // Check the prefix of the passed string for a match with one
    // of our known prefixes.
    for( int iType = 0; iType < NBR_RPT_FIELD_TYPES; iType++ )
    {
        if( VarName.Pos( FieldTypeIdentifiers[iType] ) == 1 )
            return (RPT_FIELD_TYPE)iType;
    }

    // Fall-through means we don't know this field type
    return RFT_UNKNOWN;
}


void RptGetJobInfoField( TBasePlugJob* currJob, const PLUG_JOB_INFO& jobInfo, const String &VarName, Variant &Value )
{
    if( currJob == NULL )
    {
        Value = BadJobString;
        return;
    }

    if( VarName == "JobInfo_wellName" )
        Value = currJob->WellName;
    else if( VarName == "JobInfo_stJobDate" )
        Value = currJob->JobDate;
    else if( VarName == "JobInfo_jobCreateTime" )
        Value = currJob->CreatedDate;
    else if( VarName == "JobInfo_timeZoneDisplay" )
        Value = jobInfo.timeZoneDisplay;
    else if( VarName == "JobInfo_isDST" )
        Value = BoolToYesNo( jobInfo.isDST );
    else if( VarName == "JobInfo_bias" )
        Value = IntToStr( jobInfo.bias ) + " Minutes";
    else if( VarName == "JobInfo_standardBias" )
        Value = IntToStr( jobInfo.standardBias ) + " Minutes";
    else if( VarName == "JobInfo_daylightBias" )
        Value = IntToStr( jobInfo.daylightBias ) + " Minutes";
    else if( VarName == "JobInfo_isTestJob" )
        Value = BoolToYesNo( jobInfo.isTestJob );
    else if( VarName == "JobInfo_clientName" )
        Value = jobInfo.clientName;
    else if( VarName == "JobInfo_wellNbr" )
        Value = jobInfo.wellNbr;
    else if( VarName == "JobInfo_rig" )
        Value = jobInfo.rig;
    else if( VarName == "JobInfo_leadHand" )
        Value = jobInfo.leadHand;
    else if( VarName == "JobInfo_cementCo" )
        Value = jobInfo.cementCo;
    else if( VarName == "JobInfo_companyManDay" )
        Value = jobInfo.companyManDay;
    else if( VarName == "JobInfo_companyManNight" )
        Value = jobInfo.companyManNight;
    else if( VarName == "JobInfo_drillerDay" )
        Value = jobInfo.drillerDay;
    else if( VarName == "JobInfo_drillerNight" )
        Value = jobInfo.drillerNight;
    else if( VarName == "JobInfo_rigMgrDay" )
        Value = jobInfo.rigMgrDay;
    else if( VarName == "JobInfo_rigMgrNight" )
        Value = jobInfo.rigMgrNight;
    else if( VarName == "JobInfo_casingSize" )
        Value = jobInfo.casingSize;
    else if( VarName == "JobInfo_casingType" )
        Value = jobInfo.casingType;
    else if( VarName == "JobInfo_casingWeight" )
        Value = jobInfo.casingWeight;
    else if( VarName == "JobInfo_spacerWeight" )
        Value = jobInfo.spacerWeight;
    else if( VarName == "JobInfo_spacerVolume" )
        Value = jobInfo.spacerVolume;
    else if( VarName == "JobInfo_leadWeight" )
        Value = jobInfo.leadWeight;
    else if( VarName == "JobInfo_leadVolume" )
        Value = jobInfo.leadVolume;
    else if( VarName == "JobInfo_tailWeight" )
        Value = jobInfo.tailWeight;
    else if( VarName == "JobInfo_tailVolume" )
        Value = jobInfo.tailVolume;
    else if( VarName == "JobInfo_eLowerCanConfig" )
        Value = CanisterCfgToString( jobInfo.eLowerCanConfig );
    else if( VarName == "JobInfo_eUpperCanConfig" )
        Value = CanisterCfgToString( jobInfo.eUpperCanConfig );
    else if( VarName == "JobInfo_unitsOfMeasure" )
        Value = GetUnitsText( jobInfo.unitsOfMeasure, MT_MEASURE_NAME );
    else if( VarName == "JobInfo_isDone" )
        Value = BoolToYesNo( currJob->Completed );
    else if( VarName == "JobInfo_CanMaster1_State" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanMaster1_tClean" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanMaster1_tLaunch" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanMaster2_State" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanMaster2_tClean" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanMaster2_tLaunch" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanSlave1_State" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanSlave1_tClean" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanSlave1_tLaunch" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanSlave2_State" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanSlave2_tClean" )
        Value = "TODO";
    else if( VarName == "JobInfo_CanSlave2_tLaunch" )
        Value = "TODO";
    else if( VarName == "JobInfo_PreJobRemarks" )
    {
        TStringList* pRemarksString = new TStringList();

        currJob->GetJobRemarks( TBasePlugJob::eJRT_Pre, pRemarksString );

        Value = pRemarksString->Text;

        delete pRemarksString;
    }
    else if( VarName == "JobInfo_PostJobRemarks" )
    {
        TStringList* pRemarksString = new TStringList();

        currJob->GetJobRemarks( TBasePlugJob::eJRT_Post, pRemarksString );

        Value = pRemarksString->Text;

        delete pRemarksString;
    }
    else
    {
        Value = "unknown field " + VarName;
    }
}


void RptGetMPLSInfoRecField( TBasePlugJob* currJob, int iRecNbr, const String &VarName, Variant &Value )
{
    if( currJob == NULL )
    {
        Value = BadJobString;
        return;
    }

    MPLS_INFO_REC infoRec;

    if( !currJob->GetMPLSInfoRec( iRecNbr, infoRec ) )
    {
        Value = BadRecordString;
        return;
    }

    if( VarName == "DevInfo_ConnectTime" )
        Value = TBasePlugJob::JobTimeToLocalString( infoRec.entryTime );
    else if( VarName.Pos( "MasterTEC_" ) > 0 )
    {
        if( VarName == "DevInfo_MasterTEC_Present" )
        {
            Value = BoolToYesNo( infoRec.uirMasterTec.bPresent );
        }
        else if( VarName == "DevInfo_MasterTEC_SN" )
        {
            if( infoRec.uirMasterTec.bPresent )
                Value = infoRec.uirMasterTec.devSN;
            else
                Value = DevNotPresentString;
        }
        else if( infoRec.uirMasterTec.bPresent )
        {
            if( VarName == "DevInfo_MasterTEC_HWRev" )
                Value = infoRec.uirMasterTec.devHWRev;
            else if( VarName == "DevInfo_MasterTEC_FWRev" )
                Value = infoRec.uirMasterTec.devFWRev;
            else if( VarName == "DevInfo_MasterTEC_TankOffset" )
                Value = infoRec.uirMasterTec.tankPressOffset;
            else if( VarName == "DevInfo_MasterTEC_TankSpan" )
                Value = infoRec.uirMasterTec.tankPressSpan;
            else if( VarName == "DevInfo_MasterTEC_RegOffset" )
                Value = infoRec.uirMasterTec.regPressOffset;
            else if( VarName == "DevInfo_MasterTEC_RegSpan" )
                Value = infoRec.uirMasterTec.regPressSpan;
            else if( VarName == "DevInfo_MasterTEC_BattType" )
                Value = infoRec.uirMasterTec.battType;
            else if( VarName == "DevInfo_MasterTEC_CalVer" )
                Value = "Cal Ver/Rev: " + infoRec.uirMasterTec.calVersion + "/" + infoRec.uirMasterTec.calRev;
            else
                Value = "unknown field " + VarName;
        }
        else
        {
            Value = "";
        }
    }
    else if( VarName.Pos( "MasterPIB_" ) > 0 )
    {
        if( VarName == "DevInfo_MasterPIB_Present" )
        {
            Value = BoolToYesNo( infoRec.uirMasterTec.pibPresent );
        }
        else if( infoRec.uirMasterTec.pibPresent )
        {
            if( VarName == "DevInfo_MasterPIB_HWRev" )
                Value = infoRec.uirMasterTec.pibHWRev;
            else if( VarName == "DevInfo_MasterPIB_FWRev" )
                Value = infoRec.uirMasterTec.pibFWRev;
            else
                Value = "unknown field " + VarName;
        }
        else
        {
            Value = "";
        }
    }
    else if( VarName.Pos( "SlaveTEC_" ) > 0 )
    {
        if( VarName == "DevInfo_SlaveTEC_Present" )
        {
            Value = BoolToYesNo( infoRec.uirSlaveTec.bPresent );
        }
        else if( VarName == "DevInfo_SlaveTEC_SN" )
        {
            if( infoRec.uirSlaveTec.bPresent )
                Value = infoRec.uirSlaveTec.devSN;
            else
                Value = DevNotPresentString;
        }
        else if( infoRec.uirSlaveTec.bPresent )
        {
            if( VarName == "DevInfo_SlaveTEC_HWRev" )
                Value = infoRec.uirSlaveTec.devHWRev;
            else if( VarName == "DevInfo_SlaveTEC_FWRev" )
                Value = infoRec.uirSlaveTec.devFWRev;
            else if( VarName == "DevInfo_SlaveTEC_TankOffset" )
                Value = infoRec.uirSlaveTec.tankPressOffset;
            else if( VarName == "DevInfo_SlaveTEC_TankSpan" )
                Value = infoRec.uirSlaveTec.tankPressSpan;
            else if( VarName == "DevInfo_SlaveTEC_RegOffset" )
                Value = infoRec.uirSlaveTec.regPressOffset;
            else if( VarName == "DevInfo_SlaveTEC_RegSpan" )
                Value = infoRec.uirSlaveTec.regPressSpan;
            else if( VarName == "DevInfo_SlaveTEC_BattType" )
                Value = infoRec.uirSlaveTec.battType;
            else if( VarName == "DevInfo_SlaveTEC_CalVer" )
                Value = "Cal Ver/Rev: " + infoRec.uirSlaveTec.calVersion + "/" + infoRec.uirSlaveTec.calRev;
            else
                Value = "unknown field " + VarName;
        }
        else
        {
            Value = "";
        }
    }
    else if( VarName.Pos( "SlavePIB_" ) > 0 )
    {
        if( VarName == "DevInfo_SlavePIB_Present" )
        {
            Value = BoolToYesNo( infoRec.uirSlaveTec.pibPresent );
        }
        else if( infoRec.uirSlaveTec.pibPresent )
        {
            if( VarName == "DevInfo_SlavePIB_HWRev" )
                Value = infoRec.uirSlaveTec.pibHWRev;
            else if( VarName == "DevInfo_SlavePIB_FWRev" )
                Value = infoRec.uirSlaveTec.pibFWRev;
            else
                Value = "unknown field " + VarName;
        }
        else
        {
            Value = "";
        }
    }
    else if( VarName.Pos( "PlugDet_" ) > 0 )
    {
        if( VarName == "DevInfo_PlugDet_Present" )
        {
            Value = BoolToYesNo( infoRec.uirPlugDet.pibPresent );
        }
        else if( infoRec.uirMasterTec.pibPresent )
        {
            if( VarName == "DevInfo_PlugDet_SN" )
                Value = infoRec.uirPlugDet.devSN;
            else if( VarName == "DevInfo_PlugDet_HWRev" )
                Value = infoRec.uirPlugDet.devHWRev;
            else if( VarName == "DevInfo_PlugDet_FWRev" )
                Value = infoRec.uirPlugDet.devFWRev;
            else
                Value = "unknown field " + VarName;
        }
        else
        {
            Value = "";
        }
    }
    else
    {
        Value = "unknown field " + VarName;
    }
}


void RptGetJobCommentField( TBasePlugJob* currJob, int iRecNbr, const String &VarName, Variant &Value )
{
    if( currJob == NULL )
    {
        Value = BadJobString;
        return;
    }

    JOB_COMMENT comment;

    if( currJob->GetJobComment( iRecNbr, comment ) )
    {
        if( VarName == "Comment_Nbr" )
            Value = IntToStr( iRecNbr + 1 );
        else if( VarName == "Comment_DateTime" )
            Value = TBasePlugJob::JobTimeToLocalString( comment.entryTime, true );
        else if( VarName == "Comment_Text" )
            Value = comment.entryText;
        else
            Value = "unknown field " + VarName;
    }
    else
    {
        Value = BadRecordString;
    }
}


bool RptSetMemoText( TBasePlugJob* currJob, TfrxReport* aRpt, String memoName, String text )
{
    if( currJob == NULL )
        return false;

    if( aRpt == NULL )
        return false;

    TfrxMemoView* aMemo = dynamic_cast <TfrxMemoView *>( aRpt->FindObject( memoName ) );

    if( aMemo == NULL )
        return false;

    // Clear the memo, then add the text
    aMemo->Lines->Clear();
    aMemo->Memo->Add( text );

    return true;
}


bool RptSetMemoText( TBasePlugJob* currJob, TfrxReport* aRpt, String memoName, TStringList* slMemoText )
{
    if( currJob == NULL )
        return false;

    if( aRpt == NULL )
        return false;

    if( slMemoText == NULL )
        return false;

    TfrxMemoView* aMemo = dynamic_cast <TfrxMemoView *>( aRpt->FindObject( memoName ) );

    if( aMemo == NULL )
        return false;

    // Clear the memo, then add the text
    aMemo->Lines->Clear();
    aMemo->Lines->Assign( slMemoText );

    return true;
}

