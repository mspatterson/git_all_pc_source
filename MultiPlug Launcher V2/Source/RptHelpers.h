#ifndef RptHelpersH
#define RptHelpersH

#include <VCLTee.Chart.hpp>

#include "frxClass.hpp"

#include "TBasePlugJob.h"

    typedef enum
    {
       RFT_UNKNOWN = -1,
       RFT_JOB_INFO = 0,
       RFT_GRAPH,
       RFT_EVENTS,
       RFT_COMMENTS,
       NBR_RPT_FIELD_TYPES
    } RPT_FIELD_TYPE;

    RPT_FIELD_TYPE RptGetFieldType( const String &VarName );

    void RptGetJobInfoField    ( TBasePlugJob* currJob, const PLUG_JOB_INFO& jobInfo, const String &VarName, Variant &Value );
    void RptGetMPLSInfoRecField( TBasePlugJob* currJob, int iRecNbr, const String &VarName, Variant &Value );
    void RptGetJobCommentField ( TBasePlugJob* currJob, int iRecNbr, const String &VarName, Variant &Value );
        // Helpers that return fields from specific areas of a job.

    bool RptSetMemoText( TBasePlugJob* currJob, TfrxReport* aRpt, String memoName, String text );
    bool RptSetMemoText( TBasePlugJob* currJob, TfrxReport* aRpt, String memoName, TStringList* slMemoText );
        // Sets the value of a TfrxMemoView to the text passed. Returns false if NULL
        // passed for any of the pointers or if the named memo cannot be found.

#endif
