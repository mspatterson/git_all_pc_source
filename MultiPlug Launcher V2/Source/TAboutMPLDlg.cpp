#include <vcl.h>
#pragma hdrstop

#include "TAboutMPLDlg.h"
#include "ApplUtils.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TAboutMPLForm *AboutMPLForm;


__fastcall TAboutMPLForm::TAboutMPLForm(TComponent* Owner) : TForm(Owner)
{
    MPLVerLabel->Caption = "Version " + GetApplVerAsString();
}

