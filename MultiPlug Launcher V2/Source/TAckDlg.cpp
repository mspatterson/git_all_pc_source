//
// This form will display event messages that must be acknowledged by
// the user. Multiple events can be queued up for display and in this
// case the form will display individually for each event. On ack by
// the user, a message is posted to the main form allowing it to
// update states based on this acknowledgement.
//
// Because this form maintains a queue of activity, a single instance
// of it must be constructed and used throughout program execution.
//
#include <vcl.h>
#pragma hdrstop

#include "TAckDlg.h"
#include "TescoShared.h"

#pragma package(smart_init)
#pragma link "AdvSmoothToggleButton"
#pragma link "AdvSmoothLabel"
#pragma resource "*.dfm"


TAckForm *AckForm;


#define ET_ALARM_BIT  0x80000000


__fastcall TAckForm::TAckForm(TComponent* Owner) : TForm(Owner)
{
    m_pAckQueue = new TList();

    m_hwndMsg = NULL;
    m_msgNbr  = 0;

    AckBtn->ColorDisabled    = (TColor)0x404040;
    AckBtn->BorderColor      = clTESCOBlue;
    AckBtn->BorderInnerColor = clTESCOBlue;

    EventMsgPanel->BorderColor        = clTESCOBlue;
    EventMsgPanel->BorderInnerColor   = clTESCOBlue;
    EventMsgPanel->BevelColorDisabled = clTESCOBlue;
}


void __fastcall TAckForm::FormDestroy(TObject *Sender)
{
    delete m_pAckQueue;
}


void TAckForm::SetAckEventMessage( HWND hDestWindow, int uMsgNbr )
{
    // Call to set the message number of send to a window
    // when an event is ack'd. WParam will be an AckEventType
    // enum, LParam is not used.
    m_hwndMsg = hDestWindow;
    m_msgNbr  = uMsgNbr;
}


void TAckForm::DisplayEvent( AckEventType aetType, bool bIsAlarm )
{
    // Queues the passed event for display and ack by the user.
    // The dialog appears immediately if there are no pending
    // events waiting ack. Otherwise, the event is queued for
    // display after all prior events have been ack'd

    // Just add the event to the queue. The timer tick will
    // pick this up and display the dialog. The alarm indication
    // will set the higher order bit - a hack indeed.
    DWORD dwItem = aetType;

    if( bIsAlarm )
        dwItem |= ET_ALARM_BIT;

    // If this exact event is already in the queue, don't add
    // it again as the user will get nagged multiple times.
    for( int iIndex = 0; iIndex < m_pAckQueue->Count; iIndex++ )
    {
        DWORD dwQueuedItem = (DWORD)( m_pAckQueue->Items[iIndex] );

        if( dwItem == dwQueuedItem )
            return;
    }

    // Okay to add event
    m_pAckQueue->Add( (void*)dwItem );
}


bool TAckForm::HaveEvents( void )
{
    // Returns true if one or more events are queued for ack'ing
    return( m_pAckQueue->Count > 0 );
}


void TAckForm::Flush( void )
{
    // Call to flush the event queue of any pending events
    // and close the form if visible.
    m_pAckQueue->Clear();

    // Be sure we close the form on flush as well
    ModalResult = mrCancel;
}


void __fastcall TAckForm::DisplayTimerTimer(TObject *Sender)
{
    // If we have no events, can return now
    if( m_pAckQueue->Count == 0 )
        return;

    // We have at least one event to show. Kill the timer
    // while we go modal.
    if( m_pAckQueue->Count > 0 )
    {
        DisplayTimer->Enabled = false;

        // Extract the next item. It will consist of an event type enum and
        // an alarm flag.
        DWORD dwCurrItem = (DWORD)( m_pAckQueue->Items[0] );

        AckEventType eCurrEvent = (AckEventType)( dwCurrItem & ~ET_ALARM_BIT );
        bool         bIsAlarm   = ( dwCurrItem & ET_ALARM_BIT ) ? true : false;

        String sEventText;

        switch( eCurrEvent )
        {
            case eAET_StateMismatch:  sEventText = "The state of one of the canisters is different from its last recorded state - check job log.";  break;
            case eAET_ConfigMismatch: sEventText = "The MPLS hardware configuration is different from that stored in the job - check job log.";     break;
            case eAET_ConfigChange:   sEventText = "The MPLS hardware has changed from that stored in the job - check job log.";                    break;
            case eAET_BadLaunchPlug:  sEventText = "The MPLS hardware reported a plug being launched for a plug not in the job setup - check job log.";         break;
            case eAET_BadCleanCan:    sEventText = "The MPLS hardware reported a canister being cleaned for a canister not in the job setup - check job log.";  break;
            case eAET_LowBattery:     sEventText = "The battery voltage is too low. Please replace the battery immediately.";                    break;
            case eAET_HardwareErrors: sEventText = "The MPLS hardware is reporting errors. Check the job log for more information.";             break;
            case eAET_FileSaveError:  sEventText = "The job log could not be saved. No more of these errors will be reporte for this job.";      break;
            case eAET_CommsLost:      sEventText = "Lost communications with MPLS system.";                                                      break;
            case eAET_LowBattOnCycle: sEventText = "The battery voltage is low. Monitor solenoid current to confirm launch or clean occurred.";  break;
            case eAET_InitFailed:     sEventText = "The MPLS hardware could not be initialized. You may need to power cycle the unit.";          break;
            default:                  sEventText = "Unknown event " + IntToStr( eCurrEvent ) + " reported";                                      break;
        }

        EventMsgPanel->Caption = sEventText;

        if( bIsAlarm )
        {
            Caption = "Acknowledge MPLS Alarm";

            EventMsgPanel->BorderColor        = clTESCORed;
            EventMsgPanel->BorderInnerColor   = clTESCORed;
            EventMsgPanel->BevelColorDisabled = clTESCORed;
        }
        else
        {
            Caption = "Acknowledge MPLS Event";

            EventMsgPanel->BorderColor        = clTESCOBlue;
            EventMsgPanel->BorderInnerColor   = clTESCOBlue;
            EventMsgPanel->BevelColorDisabled = clTESCOBlue;
        }

        // Show the dialog. If the user presses OK, then we can delete
        // the event from the queue. Watch for the special case where
        // we've been forced closed, which will have flushed the queue.
        if( ShowModal() == mrOk )
        {
            if( m_pAckQueue->Count > 0 )
                m_pAckQueue->Delete( 0 );
        }

        // Okay to re-enable time
        DisplayTimer->Enabled = true;
    }
}


void __fastcall TAckForm::AckBtnClick(TObject *Sender)
{
    // On Ack, call back the event window. Event being displayed is at the
    // top of the list. Remember to strip off the alarm bit.
    if( m_pAckQueue->Count > 0 )
    {
        DWORD dwCurrItem = (DWORD)( m_pAckQueue->Items[0] );

        if( ( m_hwndMsg != NULL ) && ( m_msgNbr != 0 ) )
            ::PostMessage( m_hwndMsg, m_msgNbr, dwCurrItem & ~ET_ALARM_BIT, 0 );
    }

    ModalResult = mrOk;
}
