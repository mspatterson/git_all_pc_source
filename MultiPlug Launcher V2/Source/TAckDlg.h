//---------------------------------------------------------------------------
#ifndef TAckDlgH
#define TAckDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "AdvSmoothToggleButton.hpp"
#include "AdvSmoothLabel.hpp"
//---------------------------------------------------------------------------
typedef enum {
   eAET_StateMismatch,
   eAET_LowBattery,
   eAET_ConfigMismatch,
   eAET_ConfigChange,
   eAET_BadLaunchPlug,
   eAET_BadCleanCan,
   eAET_HardwareErrors,
   eAET_FileSaveError,
   eAET_CommsLost,
   eAET_LowBattOnCycle,
   eAET_InitFailed,
   eAET_NbrAckEventTypes
} AckEventType;

class TAckForm : public TForm
{
__published:	// IDE-managed Components
    TTimer *DisplayTimer;
    TAdvSmoothToggleButton *AckBtn;
    TAdvSmoothToggleButton *EventMsgPanel;
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall DisplayTimerTimer(TObject *Sender);
    void __fastcall AckBtnClick(TObject *Sender);

private:
    TList* m_pAckQueue;
    HWND   m_hwndMsg;
    int    m_msgNbr;

public:
    __fastcall TAckForm(TComponent* Owner);

    void SetAckEventMessage( HWND hDestWindow, int uMsgNbr );
        // Call to set the message number of send to a window
        // when an event is ack'd. WParam will be an AckEventType
        // enum, LParam is not used.

    void DisplayEvent( AckEventType aetType, bool bIsAlarm = false );
        // Queues the passed event for display and ack by the user.
        // The dialog appears immediately if there are no pending
        // events waiting ack. Otherwise, the event is queued for
        // display after all prior events have been ack'd

    bool HaveEvents( void );
        // Returns true if one or more events are queued for ack'ing

    void Flush( void );
        // Call to flush the event queue of any pending events
        // and close the form if visible.
};
//---------------------------------------------------------------------------
extern PACKAGE TAckForm *AckForm;
//---------------------------------------------------------------------------
#endif
