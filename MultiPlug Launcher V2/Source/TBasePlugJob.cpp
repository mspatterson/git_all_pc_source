#include <vcl.h>
#pragma hdrstop

#include <msxmldom.hpp>
#include <XMLDoc.hpp>
#include <xmldom.hpp>
#include <DateUtils.hpp>

#include "TBasePlugJob.h"
#include "XMLHelpers.h"
#include "MPLRegistryInterface.h"

#pragma package(smart_init)


//
// Abstract base class for plug jobs
//


//
// Public Consts
//

const String MPLSJobFileExt ( ".pjb" );
const String MPLSDataFileExt( ".csv" );


//
// Private Declarations
//

#define CURR_JOB_FILE_VER  2000


//
// Root-level XML doc keys and subkeys.
//
static const String sRootNode   = "CanRigPlugJobFile";
static const String rnFileVer   = "FileVer";

static const String sJobInfoNode = "JobInfo";
    static const String jiWellName      = "WellName";
    static const String jiJobDate       = "JobDate";
    static const String jiJobCreateTime = "JobCreateTime";
    static const String jiTZDisplay     = "TimeZoneDisplay";
    static const String jiIsDST         = "IsDST";
    static const String jiBias          = "Bias";
    static const String jiStandardBias  = "StdBias";
    static const String jiDaylightBias  = "DaylightBias";    // As reported by TIME_ZONE_INFORMATION struct
    static const String jiIsTestJob     = "IsTestJob";
    static const String jiClientName    = "ClientName";
    static const String jiRig           = "Rig";
    static const String jiWellNbr       = "WellNbr";
    static const String jiLeadHand      = "LeadHand";
    static const String jiCementCo      = "CementCo";
    static const String jiCompanyManD   = "CompanyManDay";
    static const String jiCompanyManN   = "CompanyManNight";
    static const String jiDrillerD      = "DrillerDay";
    static const String jiDrillerN      = "DrillerNight";
    static const String jiRigMgrD       = "RigMgrDay";
    static const String jiRigMgrN       = "RigMgrNight";
    static const String jiCasingSize    = "CasingSize";
    static const String jiCasingType    = "CasingType";
    static const String jiCasingWeight  = "CasingWeight";
    static const String jiSpacerVolume  = "SpacerVolume";
    static const String jiSpacerWeight  = "SpacerWeight";
    static const String jiLeadVolume    = "LeadVolume";
    static const String jiLeadWeight    = "LeadWeight";
    static const String jiTailVolume    = "TailVolume";
    static const String jiTailWeight    = "TailWeight";
    static const String jiLowerCanCfg   = "LowerCanConfig";
    static const String jiUpperCanCfg   = "UpperCanConfig";
    static const String jiUOM           = "Units";

static const String sPreJobRemsNode  = "PreJobRemarks";
static const String sPostJobRemsNode = "PostJobRemarks";

static const String sJobParamsNode = "JobParams";
    static const String jpStatus       = "Done";
    static const String jpCreateNbr    = "CreateNbr";
    static const String jpCompPresent  = "Comp%dPresent";
    static const String jpCompSN       = "Comp%dSN";
    static const String jpCompFWRev    = "Comp%dFWRev";
    static const String jpCompHWRev    = "Comp%dHWRev";
    static const String jpSlaveTECSN   = "SlaveTECSN";
    static const String jpPlugState    = "Plug%dState";
    static const String jpPlugLaunch   = "Plug%dTLaunch";
    static const String jpCleanTime    = "Plug%dTClean";

static const String sLogNode = "JobLog";
    static const String jlCreateNbr  = "CreateNbr";
    static const String jlEntryTime  = "EntryTime";
    static const String jlEntryText  = "EntryText";
    static const String jiEventEnum  = "EventEnum";
    static const String jiEventInfo  = "EventInfo";

static const String sMPLSInfoRecs  = "MPLSInfoRecs";
    static const String mirCreateNbr        = "CreateNbr";
    static const String mirEntryTime        = "EntryTime";
    static const String mirDevicePresent    = "Present";
    static const String mirDeviceSN         = "SN";
    static const String mirDeviceFWRev      = "FWRev";
    static const String mirDeviceHWRev      = "HWRev";
    static const String mirPIBPresent       = "PIBPresent";
    static const String mirPIBFWRev         = "PIBFWRev";
    static const String mirPIBHWRev         = "PIBHWRev";
    static const String mirMfgInfo          = "MfgInfo";
    static const String mirMfgDate          = "MfgDate";
    static const String mirLastCalDate      = "LastCalDate";
    static const String mirCalVer           = "CalVersion";
    static const String mirCalRev           = "CalRev";
    static const String mirRegPressOffset   = "RegPressOffset";
    static const String mirRegPressSpan     = "RegPressSpan";
    static const String mirTankPressOffset  = "TankPressOffset";
    static const String mirTankPressSpan    = "TankPressSpan";
    static const String mirBattType         = "BattType";


// Record handlers. These handlers return true if keyName was found in aNode,
// false otherwise (for both get...() and set...() functions). The functions will
// also return false if the underlying XML doc is not active or an exception occurs
// during the get...() or set... () operation.
//
// All record handlers for the same type of operation have the same function name
// and are overloaded by the param list. This allows the functions to be called
// from within the template class implementation.

static bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName,       PLUG_JOB_INFO& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, const String& keyName, const PLUG_JOB_INFO& entry );

static bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName,       JOB_PARAMS& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, const String& keyName, const JOB_PARAMS& entry );

static bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName,       JOB_COMMENT& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, const String& keyName, const JOB_COMMENT& entry );

static bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName,       MPLS_INFO_REC& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, const String& keyName, const MPLS_INFO_REC& entry );


//
// Private Methods
//

static void SetXMLDocOptions( _di_IXMLDocument& xmlDoc, WORD objFileVer, const PLUG_JOB_INFO& jobInfo )
{
    // This method sets up the initial XML file structure for a job log

    // Set the basic structure for an empty xml doc
    xmlDoc->NodeIndentStr = "  ";
    xmlDoc->Options  = TXMLDocOptions() << doNodeAutoIndent << doAttrNull << doAutoPrefix << doNamespaceDecl;
    xmlDoc->Encoding = "ISO-8859-1";
    xmlDoc->Version  = "1.0";

    // Add the root node
    _di_IXMLNode rootNode = xmlDoc->AddChild( sRootNode );

    // Add the file version
    _di_IXMLNode verNode = rootNode->AddChild( rnFileVer );
    verNode->Text = IntToStr( objFileVer );

    // Add initial job params
    JOB_PARAMS initialParams;

    initialParams.isDone = false;
    initialParams.lastCreationNbr = 0;

    for( int iComp = 0; iComp < eMC_NbrMPLSComps; iComp++ )
    {
        initialParams.components[iComp].bPresent       = false;
        initialParams.components[iComp].compID.sSN     = "";
        initialParams.components[iComp].compID.dwHWRev = 0;
        initialParams.components[iComp].compID.dwFWRev = 0;
    }

    for( int iPlug = 0; iPlug < eCN_NbrCanisters; iPlug++ )
    {
        initialParams.plugInfo[iPlug].eState  = ePS_NotLoaded;
        initialParams.plugInfo[iPlug].tClean  = 0;
        initialParams.plugInfo[iPlug].tLaunch = 0;
    }

    // Update plug info for loaded plugs
    switch( jobInfo.eLowerCanConfig )
    {
        case eCC_OneLong:
        case eCC_OneShort:
            initialParams.plugInfo[eCN_Master1].eState = ePS_Loaded;
            break;

        case eCC_TwoShort:
            initialParams.plugInfo[eCN_Master1].eState = ePS_Loaded;
            initialParams.plugInfo[eCN_Master2].eState = ePS_Loaded;
            break;
    }

    switch( jobInfo.eUpperCanConfig )
    {
        case eCC_OneLong:
        case eCC_OneShort:
            initialParams.plugInfo[eCN_Slave1].eState = ePS_Loaded;
            break;

        case eCC_TwoShort:
            initialParams.plugInfo[eCN_Slave1].eState = ePS_Loaded;
            initialParams.plugInfo[eCN_Slave2].eState = ePS_Loaded;
            break;
    }

    // Add the job params
    SaveEntryToNode( rootNode, sJobParamsNode, initialParams );

    // Add the job info
    SaveEntryToNode( rootNode, sJobInfoNode, jobInfo );
}


static bool GetXMLDocOptions( _di_IXMLDocument& xmlDoc, WORD& objFileVer, PLUG_JOB_INFO& jobInfo )
{
    // Get the root node.
    _di_IXMLNode rootNode = xmlDoc->DocumentElement;

    if( rootNode == NULL )
        return false;

    // Return the file version
    objFileVer = (WORD)GetValueFromNode( rootNode, rnFileVer, 0 );

    if( !GetEntryFromNode( rootNode, sJobInfoNode, jobInfo ) )
        return false;

     return true;
}


static String ItemKeyName( int iItem )
{
    return String( L"Item" + IntToStr( iItem ) );
}


//
// GetEntry...() / SaveEntry...() functions are overloaded implementations
// for getting and saving structure types
//

bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName, PLUG_JOB_INFO& entry )
{
    // Initialize to default values
    SYSTEMTIME defaultST = { 0 };

    entry.wellName        = "";
    entry.jobCreateTime   = 0;
    entry.stJobDate       = defaultST;
    entry.timeZoneDisplay = "";
    entry.isDST           = false;
    entry.bias            = 0;
    entry.standardBias    = 0;
    entry.daylightBias    = 0;
    entry.isTestJob       = false;

    entry.clientName      = "";
    entry.wellNbr         = "";
    entry.rig             = "";
    entry.leadHand        = "";
    entry.cementCo        = "";
    entry.companyManDay   = "";
    entry.companyManNight = "";
    entry.drillerDay      = "";
    entry.drillerNight    = "";
    entry.rigMgrDay       = "";
    entry.rigMgrNight     = "";
    entry.casingSize      = "";
    entry.casingType      = "";
    entry.casingWeight    = "";
    entry.spacerVolume    = "";
    entry.spacerWeight    = "";
    entry.tailVolume      = "";
    entry.tailWeight      = "";
    entry.leadVolume      = "";
    entry.leadWeight      = "";

    entry.eLowerCanConfig = eCC_NoPlug;
    entry.eUpperCanConfig = eCC_NoPlug;
    entry.unitsOfMeasure  = UOM_IMPERIAL;

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.wellName        = GetValueFromNode( infoNode, jiWellName,       entry.wellName        );
        entry.jobCreateTime   = GetValueFromNode( infoNode, jiJobCreateTime,  entry.jobCreateTime   );
        entry.timeZoneDisplay = GetValueFromNode( infoNode, jiTZDisplay,      entry.timeZoneDisplay );
        entry.isDST           = GetValueFromNode( infoNode, jiIsDST,          entry.isDST           );
        entry.bias            = GetValueFromNode( infoNode, jiBias,           entry.bias            );
        entry.standardBias    = GetValueFromNode( infoNode, jiStandardBias,   entry.standardBias    );
        entry.daylightBias    = GetValueFromNode( infoNode, jiDaylightBias,   entry.daylightBias    );
        entry.isTestJob       = GetValueFromNode( infoNode, jiIsTestJob,      entry.isTestJob       );

        entry.clientName      = GetValueFromNode( infoNode, jiClientName,     entry.clientName      );
        entry.wellNbr         = GetValueFromNode( infoNode, jiWellNbr,        entry.wellNbr         );
        entry.rig             = GetValueFromNode( infoNode, jiRig,            entry.rig             );
        entry.leadHand        = GetValueFromNode( infoNode, jiLeadHand,       entry.leadHand        );
        entry.cementCo        = GetValueFromNode( infoNode, jiCementCo,       entry.cementCo        );
        entry.companyManDay   = GetValueFromNode( infoNode, jiCompanyManD,    entry.companyManDay   );
        entry.companyManNight = GetValueFromNode( infoNode, jiCompanyManN,    entry.companyManNight );
        entry.drillerDay      = GetValueFromNode( infoNode, jiDrillerD,       entry.drillerDay      );
        entry.drillerNight    = GetValueFromNode( infoNode, jiDrillerN,       entry.drillerNight    );
        entry.rigMgrDay       = GetValueFromNode( infoNode, jiRigMgrD,        entry.rigMgrDay       );
        entry.rigMgrNight     = GetValueFromNode( infoNode, jiRigMgrN,        entry.rigMgrNight     );
        entry.casingSize      = GetValueFromNode( infoNode, jiCasingSize,     entry.casingSize      );
        entry.casingType      = GetValueFromNode( infoNode, jiCasingType,     entry.casingType      );
        entry.casingWeight    = GetValueFromNode( infoNode, jiCasingWeight,   entry.casingWeight    );
        entry.spacerVolume    = GetValueFromNode( infoNode, jiSpacerVolume,   entry.spacerVolume    );
        entry.spacerWeight    = GetValueFromNode( infoNode, jiSpacerWeight,   entry.spacerWeight    );
        entry.tailVolume      = GetValueFromNode( infoNode, jiTailVolume,     entry.tailVolume      );
        entry.tailWeight      = GetValueFromNode( infoNode, jiTailWeight,     entry.tailWeight      );
        entry.leadVolume      = GetValueFromNode( infoNode, jiLeadVolume,     entry.leadVolume      );
        entry.leadWeight      = GetValueFromNode( infoNode, jiLeadWeight,     entry.leadWeight      );

        entry.eLowerCanConfig = (eCanisterConfig) GetValueFromNode( infoNode, jiLowerCanCfg,  (int)entry.eLowerCanConfig );
        entry.eUpperCanConfig = (eCanisterConfig) GetValueFromNode( infoNode, jiUpperCanCfg,  (int)entry.eUpperCanConfig );
        entry.unitsOfMeasure  = (UNITS_OF_MEASURE)GetValueFromNode( infoNode, jiUOM,          (int)entry.unitsOfMeasure  );

        // The SYSTEMTIME struct is stored CSV string
        String sSysTime = GetValueFromNode( infoNode, jiJobDate, String( "" ) );

        TStringList* pCSVStrings = new TStringList();

        try
        {
            pCSVStrings->CommaText = sSysTime;

            if( pCSVStrings->Count == 8 )
            {
                entry.stJobDate.wYear         = pCSVStrings->Strings[0].ToIntDef( 0 );
                entry.stJobDate.wMonth        = pCSVStrings->Strings[1].ToIntDef( 0 );
                entry.stJobDate.wDayOfWeek    = pCSVStrings->Strings[2].ToIntDef( 0 );
                entry.stJobDate.wDay          = pCSVStrings->Strings[3].ToIntDef( 0 );
                entry.stJobDate.wHour         = pCSVStrings->Strings[4].ToIntDef( 0 );
                entry.stJobDate.wMinute       = pCSVStrings->Strings[5].ToIntDef( 0 );
                entry.stJobDate.wSecond       = pCSVStrings->Strings[6].ToIntDef( 0 );
                entry.stJobDate.wMilliseconds = pCSVStrings->Strings[7].ToIntDef( 0 );
            }
        }
        catch( ... )
        {
        }

        delete pCSVStrings;
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode( _di_IXMLNode aNode, const String& keyName, const PLUG_JOB_INFO& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, jiWellName,       entry.wellName        );
        SaveValueToNode( infoNode, jiJobCreateTime,  entry.jobCreateTime   );
        SaveValueToNode( infoNode, jiTZDisplay,      entry.timeZoneDisplay );
        SaveValueToNode( infoNode, jiIsDST,          entry.isDST           );
        SaveValueToNode( infoNode, jiBias,           entry.bias            );
        SaveValueToNode( infoNode, jiStandardBias,   entry.standardBias    );
        SaveValueToNode( infoNode, jiDaylightBias,   entry.daylightBias    );
        SaveValueToNode( infoNode, jiIsTestJob,      entry.isTestJob       );

        SaveValueToNode( infoNode, jiClientName,     entry.clientName      );
        SaveValueToNode( infoNode, jiWellNbr,        entry.wellNbr         );
        SaveValueToNode( infoNode, jiRig,            entry.rig             );
        SaveValueToNode( infoNode, jiLeadHand,       entry.leadHand        );
        SaveValueToNode( infoNode, jiCementCo,       entry.cementCo        );
        SaveValueToNode( infoNode, jiCompanyManD,    entry.companyManDay   );
        SaveValueToNode( infoNode, jiCompanyManN,    entry.companyManNight );
        SaveValueToNode( infoNode, jiDrillerD,       entry.drillerDay      );
        SaveValueToNode( infoNode, jiDrillerN,       entry.drillerNight    );
        SaveValueToNode( infoNode, jiRigMgrD,        entry.rigMgrDay       );
        SaveValueToNode( infoNode, jiRigMgrN,        entry.rigMgrNight     );
        SaveValueToNode( infoNode, jiCasingSize,     entry.casingSize      );
        SaveValueToNode( infoNode, jiCasingType,     entry.casingType      );
        SaveValueToNode( infoNode, jiCasingWeight,   entry.casingWeight    );
        SaveValueToNode( infoNode, jiSpacerVolume,   entry.spacerVolume    );
        SaveValueToNode( infoNode, jiSpacerWeight,   entry.spacerWeight    );
        SaveValueToNode( infoNode, jiTailVolume,     entry.tailVolume      );
        SaveValueToNode( infoNode, jiTailWeight,     entry.tailWeight      );
        SaveValueToNode( infoNode, jiLeadVolume,     entry.leadVolume      );
        SaveValueToNode( infoNode, jiLeadWeight,     entry.leadWeight      );

        SaveValueToNode( infoNode, jiLowerCanCfg,    (int)entry.eLowerCanConfig );
        SaveValueToNode( infoNode, jiUpperCanCfg,    (int)entry.eUpperCanConfig );
        SaveValueToNode( infoNode, jiUOM,            (int)entry.unitsOfMeasure  );

        // SYSTEMTIME is stored as a string of values
        String sSystemTime;
        sSystemTime.printf( L"%d,%d,%d,%d,%d,%d,%d,%d", entry.stJobDate.wYear, entry.stJobDate.wMonth,  entry.stJobDate.wDayOfWeek, entry.stJobDate.wDay,
                                                        entry.stJobDate.wHour, entry.stJobDate.wMinute, entry.stJobDate.wSecond,    entry.stJobDate.wMilliseconds );

        SaveValueToNode( infoNode, jiJobDate, sSystemTime );

    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName, JOB_PARAMS& entry )
{
    // Initialize to default values
    entry.isDone          = false;
    entry.lastCreationNbr = 0;

    for( int iComp = 0; iComp < eMC_NbrMPLSComps; iComp++ )
    {
        entry.components[iComp].bPresent       = false;
        entry.components[iComp].compID.sSN     = "";
        entry.components[iComp].compID.dwHWRev = 0;
        entry.components[iComp].compID.dwFWRev = 0;
    }

    for( int iPlug = 0; iPlug < eCN_NbrCanisters; iPlug++ )
    {
        entry.plugInfo[iPlug].eState  = ePS_NotLoaded;
        entry.plugInfo[iPlug].tClean  = 0;
        entry.plugInfo[iPlug].tLaunch = 0;
    }

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.isDone          = GetValueFromNode( infoNode, jpStatus,    entry.isDone          );
        entry.lastCreationNbr = GetValueFromNode( infoNode, jpCreateNbr, entry.lastCreationNbr );

        // Must iterate through a loop for components IDs
        for( int iComp = 0; iComp < eMC_NbrMPLSComps; iComp++ )
        {
            String sCompPresentKey;
            String sCompSNKey;
            String sCompHWRevKey;
            String sCompFWRevKey;

            sCompPresentKey.printf( jpCompPresent.w_str(), iComp );
            sCompSNKey.printf     ( jpCompSN.w_str(),      iComp );
            sCompHWRevKey.printf  ( jpCompFWRev.w_str(),   iComp );
            sCompFWRevKey.printf  ( jpCompHWRev.w_str(),   iComp );

            entry.components[iComp].bPresent       = GetValueFromNode( infoNode, sCompPresentKey,      entry.components[iComp].bPresent );
            entry.components[iComp].compID.sSN     = GetValueFromNode( infoNode, sCompSNKey,           entry.components[iComp].compID.sSN );
            entry.components[iComp].compID.dwHWRev = GetValueFromNode( infoNode, sCompHWRevKey,   (int)entry.components[iComp].compID.dwHWRev );
            entry.components[iComp].compID.dwFWRev = GetValueFromNode( infoNode, sCompFWRevKey,   (int)entry.components[iComp].compID.dwFWRev );
        }

        // Must iterate through a loop for plug state info
        for( int iPlug = 0; iPlug < eCN_NbrCanisters; iPlug++ )
        {
            String sStateKey;
            String sTLaunchKey;
            String sTCleanKey;

            sStateKey.printf  ( jpPlugState.w_str(),  iPlug );
            sTLaunchKey.printf( jpPlugLaunch.w_str(), iPlug );
            sTCleanKey.printf ( jpCleanTime.w_str(),  iPlug );

            entry.plugInfo[iPlug].eState  = GetValueFromNode( infoNode, sStateKey,   (int)entry.plugInfo[iPlug].eState );
            entry.plugInfo[iPlug].tLaunch = GetValueFromNode( infoNode, sTLaunchKey, entry.plugInfo[iPlug].tLaunch );
            entry.plugInfo[iPlug].tClean  = GetValueFromNode( infoNode, sTCleanKey,  entry.plugInfo[iPlug].tClean );
        }
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode( _di_IXMLNode aNode, const String& keyName, const JOB_PARAMS& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, jpStatus,    entry.isDone          );
        SaveValueToNode( infoNode, jpCreateNbr, entry.lastCreationNbr );

        // Must iterate through a loop for components IDs
        for( int iComp = 0; iComp < eMC_NbrMPLSComps; iComp++ )
        {
            String sCompPresentKey;
            String sCompSNKey;
            String sCompHWRevKey;
            String sCompFWRevKey;

            sCompPresentKey.printf( jpCompPresent.w_str(), iComp );
            sCompSNKey.printf     ( jpCompSN.w_str(),      iComp );
            sCompHWRevKey.printf  ( jpCompFWRev.w_str(),   iComp );
            sCompFWRevKey.printf  ( jpCompHWRev.w_str(),   iComp );

            SaveValueToNode( infoNode, sCompPresentKey,      entry.components[iComp].bPresent );
            SaveValueToNode( infoNode, sCompSNKey,           entry.components[iComp].compID.sSN );
            SaveValueToNode( infoNode, sCompHWRevKey,   (int)entry.components[iComp].compID.dwHWRev );
            SaveValueToNode( infoNode, sCompFWRevKey,   (int)entry.components[iComp].compID.dwFWRev );
        }

        // Must iterate through a loop for plug state info
        for( int iPlug = 0; iPlug < eCN_NbrCanisters; iPlug++ )
        {
            String sStateKey;
            String sTLaunchKey;
            String sTCleanKey;

            sStateKey.printf  ( jpPlugState.w_str(),  iPlug );
            sTLaunchKey.printf( jpPlugLaunch.w_str(), iPlug );
            sTCleanKey.printf ( jpCleanTime.w_str(),  iPlug );

            SaveValueToNode( infoNode, sStateKey,   (int)entry.plugInfo[iPlug].eState );
            SaveValueToNode( infoNode, sTLaunchKey, entry.plugInfo[iPlug].tLaunch );
            SaveValueToNode( infoNode, sTCleanKey,  entry.plugInfo[iPlug].tClean  );
        }
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


static void GetUnitInfoRec( _di_IXMLNode aNode, const String& keyPrefix, UNIT_INFO_REC& infoRec, bool bOnlyGetIDFields )
{
    // Helper method that assumes the node exists. Also assumes the infoRec has
    // already been cleared.
    infoRec.bPresent        = GetValueFromNode( aNode, keyPrefix + mirDevicePresent,   infoRec.bPresent        );
    infoRec.devSN           = GetValueFromNode( aNode, keyPrefix + mirDeviceSN,        infoRec.devSN           );
    infoRec.devFWRev        = GetValueFromNode( aNode, keyPrefix + mirDeviceFWRev,     infoRec.devFWRev        );
    infoRec.devHWRev        = GetValueFromNode( aNode, keyPrefix + mirDeviceHWRev,     infoRec.devHWRev        );

    if( bOnlyGetIDFields )
        return;

    infoRec.pibPresent      = GetValueFromNode( aNode, keyPrefix + mirPIBPresent,      infoRec.pibPresent      );
    infoRec.pibFWRev        = GetValueFromNode( aNode, keyPrefix + mirPIBFWRev,        infoRec.pibFWRev        );
    infoRec.pibHWRev        = GetValueFromNode( aNode, keyPrefix + mirPIBHWRev,        infoRec.pibHWRev        );
    infoRec.mfgInfo         = GetValueFromNode( aNode, keyPrefix + mirMfgInfo,         infoRec.mfgInfo         );
    infoRec.mfgDate         = GetValueFromNode( aNode, keyPrefix + mirMfgDate,         infoRec.mfgDate         );
    infoRec.calVersion      = GetValueFromNode( aNode, keyPrefix + mirLastCalDate,     infoRec.calVersion      );
    infoRec.calRev          = GetValueFromNode( aNode, keyPrefix + mirCalVer,          infoRec.calRev          );
    infoRec.lastCalDate     = GetValueFromNode( aNode, keyPrefix + mirCalRev,          infoRec.lastCalDate     );
    infoRec.tankPressOffset = GetValueFromNode( aNode, keyPrefix + mirRegPressOffset,  infoRec.tankPressOffset );
    infoRec.tankPressSpan   = GetValueFromNode( aNode, keyPrefix + mirRegPressSpan,    infoRec.tankPressSpan   );
    infoRec.regPressOffset  = GetValueFromNode( aNode, keyPrefix + mirTankPressOffset, infoRec.regPressOffset  );
    infoRec.regPressSpan    = GetValueFromNode( aNode, keyPrefix + mirTankPressSpan,   infoRec.regPressSpan    );
    infoRec.battType        = GetValueFromNode( aNode, keyPrefix + mirBattType,        infoRec.battType        );
}


bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName, MPLS_INFO_REC& entry )
{
    // Initialize to default values
    entry.creationNbr = 0;
    entry.entryTime   = 0;
    entry.uirMasterTec.Clear();
    entry.uirSlaveTec.Clear();
    entry.uirPlugDet.Clear();

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.creationNbr = GetValueFromNode( infoNode, mirCreateNbr, entry.creationNbr );
        entry.entryTime   = GetValueFromNode( infoNode, mirEntryTime, entry.entryTime   );

        GetUnitInfoRec( infoNode, "Master_",  entry.uirMasterTec, false );
        GetUnitInfoRec( infoNode, "Slave_",   entry.uirSlaveTec,  false );
        GetUnitInfoRec( infoNode, "PlugDet_", entry.uirPlugDet,   true  );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


static void SaveUnitInfoRec( _di_IXMLNode aNode, const String& keyPrefix, const UNIT_INFO_REC& infoRec, bool bOnlySaveIDFields )
{
    // Helper method that assumes the node exists.
    SaveValueToNode( aNode, keyPrefix + mirDevicePresent,   infoRec.bPresent        );
    SaveValueToNode( aNode, keyPrefix + mirDeviceSN,        infoRec.devSN           );
    SaveValueToNode( aNode, keyPrefix + mirDeviceFWRev,     infoRec.devFWRev        );
    SaveValueToNode( aNode, keyPrefix + mirDeviceHWRev,     infoRec.devHWRev        );

    if( bOnlySaveIDFields )
        return;

    SaveValueToNode( aNode, keyPrefix + mirPIBPresent,      infoRec.pibPresent      );
    SaveValueToNode( aNode, keyPrefix + mirPIBFWRev,        infoRec.pibFWRev        );
    SaveValueToNode( aNode, keyPrefix + mirPIBHWRev,        infoRec.pibHWRev        );
    SaveValueToNode( aNode, keyPrefix + mirMfgInfo,         infoRec.mfgInfo         );
    SaveValueToNode( aNode, keyPrefix + mirMfgDate,         infoRec.mfgDate         );
    SaveValueToNode( aNode, keyPrefix + mirLastCalDate,     infoRec.calVersion      );
    SaveValueToNode( aNode, keyPrefix + mirCalVer,          infoRec.calRev          );
    SaveValueToNode( aNode, keyPrefix + mirCalRev,          infoRec.lastCalDate     );
    SaveValueToNode( aNode, keyPrefix + mirRegPressOffset,  infoRec.tankPressOffset );
    SaveValueToNode( aNode, keyPrefix + mirRegPressSpan,    infoRec.tankPressSpan   );
    SaveValueToNode( aNode, keyPrefix + mirTankPressOffset, infoRec.regPressOffset  );
    SaveValueToNode( aNode, keyPrefix + mirTankPressSpan,   infoRec.regPressSpan    );
    SaveValueToNode( aNode, keyPrefix + mirBattType,        infoRec.battType        );
}


bool SaveEntryToNode( _di_IXMLNode aNode, const String& keyName, const MPLS_INFO_REC& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, mirCreateNbr, entry.creationNbr );
        SaveValueToNode( infoNode, mirEntryTime, entry.entryTime   );

        SaveUnitInfoRec( infoNode, "Master_",  entry.uirMasterTec, false );
        SaveUnitInfoRec( infoNode, "Slave_",   entry.uirSlaveTec,  false );
        SaveUnitInfoRec( infoNode, "PlugDet_", entry.uirPlugDet,   true  );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool GetEntryFromNode( _di_IXMLNode aNode, const String& keyName, JOB_COMMENT& entry )
{
    // Initialize to default values
    entry.creationNbr = 0;
    entry.entryTime   = 0;
    entry.entryText   = L"";
    entry.eEvent      = eSE_None;
    entry.iInfo       = 0;

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.creationNbr = GetValueFromNode( infoNode, jlCreateNbr, entry.creationNbr );
        entry.entryTime   = GetValueFromNode( infoNode, jlEntryTime, entry.entryTime   );
        entry.entryText   = GetValueFromNode( infoNode, jlEntryText, entry.entryText   );
        entry.eEvent      = GetValueFromNode( infoNode, jiEventEnum, entry.eEvent      );
        entry.iInfo       = GetValueFromNode( infoNode, jiEventInfo, entry.iInfo       );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode( _di_IXMLNode aNode, const String& keyName, const JOB_COMMENT& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, jlCreateNbr, entry.creationNbr );
        SaveValueToNode( infoNode, jlEntryTime, entry.entryTime   );
        SaveValueToNode( infoNode, jlEntryText, entry.entryText   );
        SaveValueToNode( infoNode, jiEventEnum, entry.eEvent      );
        SaveValueToNode( infoNode, jiEventInfo, entry.iInfo       );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


//
// TStructArray
//

// We have to do a trick here to force the compiler to instantiate the
// template class code. If you get an 'unresolved external', try adding
// that function to this initializer.

static bool DoInit( void )
{
    TStructArray<JOB_COMMENT> initRecs;

    JOB_COMMENT temp;

    initRecs.Append( temp );
    initRecs.Modify( 0, temp );

    return true;
}

static bool initDone = DoInit();


//
// NOTE! NOTE! NOTE! Do not use memmove(), memcpy(), etc, where
// copying or moving members of the templated class. If the
// class contains Unicode strings, using mem...() will make bad
// things happen. You have to do an item by item copy instead.
//

template <class T> TStructArray<T>::TStructArray()
{
   m_allocElements = 16;
   m_allocIncrease = 16;
   m_nbrElements   = 0;

   m_pData = new T[m_allocElements];
}


template <class T> TStructArray<T>::~TStructArray()
{
  if( m_allocElements )
      delete [] m_pData;
}


template <class T> void TStructArray<T>::CheckAndAllocMem( int newNbrElements )
{
    // Checks to see if the object contains the number of records passed.
    // If not, allocates new memory and copies existing data over.
    if( newNbrElements >= m_allocElements )
    {
        // More memory required. Over-allocate it
        T *pTmp = new T[ newNbrElements + m_allocIncrease ];

        // Copy any existing elements over. Cannot use memcpy in case
        // element members are classes (eg, String)
        if( m_nbrElements )
        {
            for( int iEl = 0; iEl < m_nbrElements; iEl++ )
                pTmp[iEl] = m_pData[iEl];

            delete [] m_pData;
        }

        m_pData = pTmp;
        m_allocElements = newNbrElements + m_allocIncrease;
    }
}


template <class T> TStructArray<T>& TStructArray<T>::operator =(const TStructArray &equ)
{
    try
    {
        if( this == &equ )
            return *this;

        CheckAndAllocMem( equ.m_nbrElements );

        // Copy data element by element. Cannot use memcpy() in case an
        // element member is a class
        for( int iEl = 0; iEl < equ.m_nbrElements; iEl++ )
            m_pData[iEl] = equ.m_pData[iEl];

        m_nbrElements = equ.m_nbrElements;

        return *this;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::operator =" );
        return *this;
    }
}


template <class T> TStructArray<T>& TStructArray<T>::operator += (const TStructArray &plus)
{
    try
    {
        CheckAndAllocMem( m_nbrElements + plus.m_nbrElements );

        // Copy data element by element. Cannot use memcpy() in case an
        // element member is a class
        for( int iEl = 0; iEl < plus.m_nbrElements; iEl++ )
            m_pData[m_nbrElements+iEl] = plus.m_pData[iEl];

        m_nbrElements += plus.m_nbrElements;

        return *this;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::operator +=" );
        return *this;
    }
}


template <class T> void TStructArray<T>::Append( const T& add )
{
    try
    {
        CheckAndAllocMem( m_nbrElements + 1 );

        m_pData[m_nbrElements++] = add;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::Append()" );
    }
}


template <class T> void TStructArray<T>::Insert( int index, const T& insert )
{
    // Inserts an item at the given index
    try
    {
        CheckAndAllocMem( m_nbrElements + 1 );

        for( int i = m_nbrElements; i > index; i-- )
            m_pData[i] = m_pData[i-1];

        m_pData[index] = insert;

        m_nbrElements++;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::Insert()" );
    }
}


template <class T> void TStructArray<T>::Modify( int index, const T& modify )
{
    if( ( index >= 0 ) && ( index < m_nbrElements ) )
    {
        m_pData[index] = modify;
    }
}


template <class T> void TStructArray<T>::Del( int index )
{
    if( ( index >= 0 ) && ( index < m_nbrElements ) )
    {
        for( int i = index; i < m_nbrElements - 1; i++ )
            m_pData[i] = m_pData[i+1];

        m_nbrElements--;
    }
}


template <class T> void TStructArray<T>::LoadFromNode( _di_IXMLNode aNode )
{
    // Assumes that each element T is stored under aNode under 0-based
    // key values (eg, "0" to "n-1" where n is the number of elements
    // in the array.

    // Clear all elements from array first
    Clear();

    if( aNode != NULL )
    {
        // The ChildNodes->Count property gives bogus values at times for some
        // reason. So don't use it. Instead, loop here until the next item
        // number is not found.
        int itemNbr = 0;

        while( itemNbr >= 0 )
        {
            T tempData;

            if( !GetEntryFromNode( aNode, ItemKeyName( itemNbr ), tempData ) )
                break;

            Append( tempData );

            itemNbr++;
        }
    }
}


template <class T> void TStructArray<T>::SaveToNode( _di_IXMLNode aNode )
{
    // Stores each element T under aNode using 0-based key values
    // (eg, "0" to "n-1" where n is the number of elements in the array).

    if( aNode != NULL )
    {
        // Delete any existing children (and presumably leaf nodes)
        aNode->ChildNodes->Clear();

        for( int iItem = 0; iItem < m_nbrElements; iItem++ )
            SaveEntryToNode( aNode, ItemKeyName( iItem ), m_pData[iItem] );
    }
}


//
// TPlugJog
//

__fastcall TBasePlugJob::TBasePlugJob( const String& sJobFileName )
{
    // Create required objects
    m_preJobRemStrings  = new TStringList();
    m_postJobRemStrings = new TStringList();

    // Init default values
    InitJobInfo();

    // Try to load the job
    LoadJob( sJobFileName );
}


__fastcall TBasePlugJob::~TBasePlugJob()
{
    // Delete any created objects
    delete m_preJobRemStrings;
    delete m_postJobRemStrings;
}


bool TBasePlugJob::CreateJobFile( const PLUG_JOB_INFO& jobInfo, const String& sJobFileName )
{
    // Creates the basic files required for a job.
    bool bSuccess = false;

    _di_IXMLDocument xmlDoc = NewXMLDocument();

    try
    {
        // Initialize new doc
        SetXMLDocOptions( xmlDoc, CURR_JOB_FILE_VER, jobInfo );

        _di_IXMLNode rootNode = xmlDoc->DocumentElement;

        if( rootNode == NULL )
            Abort();

        // Get the job params just created
        JOB_PARAMS jpInitial;

        if( !GetEntryFromNode( rootNode, sJobParamsNode, jpInitial ) )
            Abort();

        // Add initial job log entries
        TStructArray<JOB_COMMENT> jcInitialComments;

        if( jobInfo.isTestJob )
        {
            jpInitial.lastCreationNbr++;

            JOB_COMMENT newComment;

            newComment.creationNbr = jpInitial.lastCreationNbr;
            newComment.entryTime   = time( NULL );
            newComment.entryText   = "Test job created for " + jobInfo.wellName;
            newComment.eEvent      = eSE_None;
            newComment.iInfo       = 0;

            jcInitialComments.Append( newComment );
        }
        else
        {
            jpInitial.lastCreationNbr++;

            JOB_COMMENT newComment;

            newComment.creationNbr = jpInitial.lastCreationNbr;
            newComment.entryTime   = time( NULL );
            newComment.entryText   = "New job created: " + jobInfo.wellName + " on " + JobDateToString( jobInfo.stJobDate );
            newComment.eEvent      = eSE_None;
            newComment.iInfo       = 0;

            jcInitialComments.Append( newComment );

            String sConfig = "Lower canister: ";

            switch( jobInfo.eLowerCanConfig )
            {
                case eCC_OneLong:   sConfig = sConfig + "one long";    break;
                case eCC_OneShort:  sConfig = sConfig + "one short";   break;
                case eCC_TwoShort:  sConfig = sConfig + "two short";   break;
                default:            sConfig = sConfig + IntToStr( (int)jobInfo.eLowerCanConfig ); break;
            }

            sConfig = sConfig + ", Upper canister: ";

            switch( jobInfo.eUpperCanConfig )
            {
                case eCC_OneLong:   sConfig = sConfig + "one long";    break;
                case eCC_OneShort:  sConfig = sConfig + "one short";   break;
                case eCC_TwoShort:  sConfig = sConfig + "two short";   break;
                case eCC_NoPlug:    sConfig = sConfig + "not used";    break;
                default:            sConfig = sConfig + IntToStr( (int)jobInfo.eUpperCanConfig ); break;
            }

            jpInitial.lastCreationNbr++;

            newComment.creationNbr = jpInitial.lastCreationNbr;
            newComment.entryText   = sConfig;

            jcInitialComments.Append( newComment );
        }

        // Update the job params
        if( !SaveEntryToNode( rootNode, sJobParamsNode, jpInitial ) )
            Abort();

        // Add comments node - it won't exist on initial create, but it
        // won't hurt doing the delete anyway
        rootNode->ChildNodes->Delete( sLogNode );

        _di_IXMLNode jobLogNode = FindOrCreateNode( rootNode, sLogNode );
        jcInitialComments.SaveToNode( jobLogNode );

        // Done adding elements - save the file now
        xmlDoc->SaveToFile( sJobFileName );

        // Fall through mean succcess
        bSuccess = true;
    }
    catch( ... )
    {
    }

    xmlDoc.Release();

    return bSuccess;
}


void TBasePlugJob::InitJobInfo( void )
{
    m_jobLogFileName  = "";
    m_jobDataFileName = "";
    m_jobLoaded       = true;
    m_bInSimMode      = false;
    m_fileSaveErrors  = 0;

    // Solenoid times are in msecs. Convert to seconds for our usage.
    // Note that users can only enter values in seconds, so there won't
    // be any rounding issues on divide.
    m_solOnTimeLaunch = (time_t)GetSolenoidOnTime( SOTT_LAUNCH ) / 1000;
    m_solOnTimeClean  = (time_t)GetSolenoidOnTime( SOTT_CLEAN )  / 1000;
    m_solOnTimeReset  = (time_t)GetSolenoidOnTime( SOTT_RESET )  / 1000;

    // Clear job info
    m_jobInfo.Clear();

    m_preJobRemStrings->Clear();
    m_postJobRemStrings->Clear();

    // Clear job params
    m_jobParams.isDone = false;
    m_jobParams.lastCreationNbr = 0;

    for( int iPlug = 0; iPlug < eCN_NbrCanisters; iPlug++ )
    {
        m_jobParams.plugInfo[iPlug].eState  = ePS_NotLoaded;
        m_jobParams.plugInfo[iPlug].tLaunch = 0;
        m_jobParams.plugInfo[iPlug].tClean  = 0;
    }

    // Clear log
    m_jobComments.Clear();

    // Clear sys events
    m_sysEvents.Clear();

    // Clear info recs
    m_mplsInfoRecs.Clear();

    // Clear RFC data
    m_rfcRecs.Clear();
}


void TBasePlugJob::LoadJob( const String& sJobFileName )
{
    // Helper function used to initialize class. Called by both
    // forms of the class constructor. Will set m_jobLoaded to
    // true on success.

    // Init to defaults first
    InitJobInfo();

    // Now try to load the job from file
    // Get the basic job info. This also validates the job
    if( !GetJobInfo( sJobFileName, m_jobInfo ) )
        return;

    // Load the remaining elements of the job
    _di_IXMLDocument xmlDoc = NULL;

    try
    {
        xmlDoc = LoadXMLDocument( sJobFileName );

        // Get the root node.
        _di_IXMLNode rootNode = xmlDoc->DocumentElement;

        if( rootNode != NULL )
        {
            GetEntryFromNode( rootNode, sJobInfoNode,     m_jobInfo );
            GetEntryFromNode( rootNode, sJobParamsNode,   m_jobParams );
            GetValueFromNode( rootNode, sPreJobRemsNode,  m_preJobRemStrings );
            GetValueFromNode( rootNode, sPostJobRemsNode, m_postJobRemStrings );

            _di_IXMLNode logRecsNode = FindOrCreateNode( rootNode, sLogNode );
            m_jobComments.LoadFromNode( logRecsNode );

            // Get info recs
            _di_IXMLNode infoRecsNode = FindOrCreateNode( rootNode, sMPLSInfoRecs );
            m_mplsInfoRecs.LoadFromNode( infoRecsNode );

            m_jobLoaded = true;
        }
    }
    catch( ... )
    {
        // Don't do anything - the caller will see m_jobLoaded is not
        // set when the check how the construction went
    }

    if( xmlDoc != NULL )
        xmlDoc.Release();

    if( !m_jobLoaded )
        return;

    m_jobLogFileName  = sJobFileName;
    m_jobDataFileName = ChangeFileExt( sJobFileName, MPLSDataFileExt );

    // Solenoid on times come from the registry, not the job file
    m_solOnTimeLaunch = GetSolenoidOnTime( SOTT_LAUNCH );
    m_solOnTimeClean  = GetSolenoidOnTime( SOTT_CLEAN );
    m_solOnTimeReset  = GetSolenoidOnTime( SOTT_RESET );

    // Update the plug number look-up array now. Canister 1 is always
    // plug 1 in a configuration.
    for( int iEnum = 0; iEnum < eCN_NbrCanisters; iEnum++ )
        m_iPlugNbr[iEnum] = 0;

    m_iPlugNbr[eCN_Master1] = 1;

    // Next plug will be number 2
    int iPlugNbr = 2;

    if( m_jobInfo.eLowerCanConfig == eCC_TwoShort )
    {
        m_iPlugNbr[eCN_Master2] = iPlugNbr;
        iPlugNbr++;
    }

    // If the upper canister has any config, the first plug there is next
    if( m_jobInfo.eUpperCanConfig != eCC_NoPlug )
    {
        m_iPlugNbr[eCN_Slave1] = iPlugNbr;
        iPlugNbr++;
    }

    // Last plug number goes to the upper config if it has two plugs
    if( m_jobInfo.eUpperCanConfig == eCC_TwoShort )
        m_iPlugNbr[eCN_Slave2] = iPlugNbr;

    // With plug nbr array populated, can now build the sys event array
    BuildSysEventArray();

    // Now try to load RFC recs from file
    if( FileExists( m_jobDataFileName ) )
    {
        TStringList* pRFCStrings = new TStringList();

        try
        {
            pRFCStrings->LoadFromFile( m_jobDataFileName );

            for( int iRec = 0; iRec < pRFCStrings->Count; iRec++ )
            {
                LOGGED_RFC_DATA rfcRec;

                if( ConvertStringToRFCRec( rfcRec, pRFCStrings->Strings[iRec] ) )
                    m_rfcRecs.Append( rfcRec );
            }
        }
        catch( ... )
        {
        }

        delete pRFCStrings;
    }
}


bool TBasePlugJob::Save( void )
{
    // Internal Save() handler. Can be called even if in sim mode,
    // and in that case no changes are made to the file.
    if( m_bInSimMode )
        return true;

    if( !m_jobLoaded )
        return false;

    // Save job as XML file now
    bool bSuccess = false;

    _di_IXMLDocument xmlDoc = NULL;

    try
    {
        xmlDoc = LoadXMLDocument( m_jobLogFileName );

        if( xmlDoc == NULL )
            Abort();

        xmlDoc->NodeIndentStr = "  ";
        xmlDoc->Options  = TXMLDocOptions() << doNodeAutoIndent << doAttrNull << doAutoPrefix << doNamespaceDecl;

        _di_IXMLNode rootNode = xmlDoc->DocumentElement;

        if( rootNode == NULL )
            Abort();

        // Update job params
        if( !SaveEntryToNode( rootNode, sJobParamsNode, m_jobParams ) )
            Abort();

        // Update job info
        if( !SaveEntryToNode( rootNode, sJobInfoNode, m_jobInfo ) )
            Abort();

        // Update remarks
        SaveValueToNode( rootNode, sPreJobRemsNode,  m_preJobRemStrings  );
        SaveValueToNode( rootNode, sPostJobRemsNode, m_postJobRemStrings );

        // Refresh the comments node. Nodes that are arrays of items
        // must be deleted first
        rootNode->ChildNodes->Delete( sLogNode );

        _di_IXMLNode jobLogNode = FindOrCreateNode( rootNode, sLogNode );
        m_jobComments.SaveToNode( jobLogNode );

        // Refresh info records
        rootNode->ChildNodes->Delete( sMPLSInfoRecs );

        _di_IXMLNode infoRecsNode = FindOrCreateNode( rootNode, sMPLSInfoRecs);
        m_mplsInfoRecs.SaveToNode( infoRecsNode );

        // Done adding elements - save the file now
        xmlDoc->SaveToFile( m_jobLogFileName );

        // Fall through mean succcess
        bSuccess = true;
    }
    catch( ... )
    {
    }

    if( xmlDoc != NULL )
        xmlDoc.Release();

    if( !bSuccess )
        m_fileSaveErrors++;

    return bSuccess;
}


int TBasePlugJob::GetNextCreationNbr( void )
{
    // Returns a unique sequence number for a section rec, connection
    // rec, or main comment
    m_jobParams.lastCreationNbr++;

    return m_jobParams.lastCreationNbr;
}


void TBasePlugJob::GetJobInfo( PLUG_JOB_INFO& jobInfo )
{
    jobInfo = m_jobInfo;
}


bool TBasePlugJob::SaveJobInfo( const PLUG_JOB_INFO& jobInfo )
{
    // We only allow certain job fields to be changed once the job
    // has been created.
    if( m_jobParams.isDone )
        return true;

    m_jobInfo.clientName      = jobInfo.clientName;
    m_jobInfo.wellNbr         = jobInfo.wellNbr;
    m_jobInfo.rig             = jobInfo.rig;
    m_jobInfo.leadHand        = jobInfo.leadHand;
    m_jobInfo.cementCo        = jobInfo.cementCo;
    m_jobInfo.companyManDay   = jobInfo.companyManDay;
    m_jobInfo.companyManNight = jobInfo.companyManNight;
    m_jobInfo.drillerDay      = jobInfo.drillerDay;
    m_jobInfo.drillerNight    = jobInfo.drillerNight;
    m_jobInfo.rigMgrDay       = jobInfo.rigMgrDay;
    m_jobInfo.rigMgrNight     = jobInfo.rigMgrNight;
    m_jobInfo.casingSize      = jobInfo.casingSize;
    m_jobInfo.casingType      = jobInfo.casingType;
    m_jobInfo.casingWeight    = jobInfo.casingWeight;
    m_jobInfo.spacerVolume    = jobInfo.spacerVolume;
    m_jobInfo.spacerWeight    = jobInfo.spacerWeight;
    m_jobInfo.tailVolume      = jobInfo.tailVolume;
    m_jobInfo.tailWeight      = jobInfo.tailWeight;
    m_jobInfo.leadVolume      = jobInfo.leadVolume;
    m_jobInfo.leadWeight      = jobInfo.leadWeight;

    return Save();
}


bool TBasePlugJob::GetJobComment( int whichComment, JOB_COMMENT& comment )
{
    if( ( whichComment >= 0 ) && ( whichComment < m_jobComments.Count ) )
    {
        comment = m_jobComments[whichComment];
        return true;
    }

    return false;
}


bool TBasePlugJob::AddJobComment( const String& commentText, time_t tWhen, eSystemEvent eEvtType, int iEvtInfo, bool bSaveImmediately )
{
    if( m_jobParams.isDone )
        return true;

    JOB_COMMENT newComment;

    newComment.creationNbr = GetNextCreationNbr();
    newComment.entryTime   = ( tWhen == 0 ) ? time( NULL ) : tWhen;
    newComment.entryText   = commentText;
    newComment.eEvent      = eEvtType;
    newComment.iInfo       = iEvtInfo;

    m_jobComments.Append( newComment );

    if( eEvtType != eSE_None )
        AddSystemEvent( newComment, m_jobComments.Count - 1 );

    bool bSavedOK;

    if( bSaveImmediately )
        bSavedOK = Save();
    else
        bSavedOK = true;

    if( m_onCommentAdded )
        m_onCommentAdded( this );

    return bSavedOK;
}


bool TBasePlugJob::GetJobRemarks( eJobRemarkType eType, TStringList* pRemarks )
{
    if( pRemarks == NULL )
        return false;

    pRemarks->Clear();

    TStringList* pSrc;

    if( eType == eJRT_Pre )
        pSrc = m_preJobRemStrings;
    else if( eType == eJRT_Post )
        pSrc = m_postJobRemStrings;
    else
        return false;

    pRemarks->Assign( pSrc );

    return true;
}


bool TBasePlugJob::SaveJobRemarks( eJobRemarkType eType, TStringList* pRemarks )
{
    if( m_jobParams.isDone )
        return true;

    if( pRemarks == NULL )
        return false;

    TStringList* pDest;

    if( eType == eJRT_Pre )
        pDest = m_preJobRemStrings;
    else if( eType == eJRT_Post )
        pDest = m_postJobRemStrings;
    else
        return false;

    pDest->Assign( pRemarks );

    return Save();
}


bool TBasePlugJob::GetSystemEvent( int whichEvent, SYSTEM_EVENT& sysEvent )
{
    if( ( whichEvent >= 0 ) && ( whichEvent < m_sysEvents.Count ) )
    {
        sysEvent = m_sysEvents[whichEvent];
        return true;
    }

    return false;
}


bool TBasePlugJob::InfoRecsEqual( const UNIT_INFO_REC& infoRec1, const UNIT_INFO_REC& infoRec2 )
{
    if( ( infoRec1.bPresent        == infoRec2.bPresent        ) &&
        ( infoRec1.devSN           == infoRec2.devSN           ) &&
        ( infoRec1.devFWRev        == infoRec2.devFWRev        ) &&
        ( infoRec1.devHWRev        == infoRec2.devHWRev        ) &&
        ( infoRec1.pibPresent      == infoRec2.pibPresent      ) &&
        ( infoRec1.pibFWRev        == infoRec2.pibFWRev        ) &&
        ( infoRec1.pibHWRev        == infoRec2.pibHWRev        ) &&
        ( infoRec1.mfgInfo         == infoRec2.mfgInfo         ) &&
        ( infoRec1.mfgDate         == infoRec2.mfgDate         ) &&
        ( infoRec1.lastCalDate     == infoRec2.lastCalDate     ) &&
        ( infoRec1.calVersion      == infoRec2.calVersion      ) &&
        ( infoRec1.calRev          == infoRec2.calRev          ) &&
        ( infoRec1.tankPressOffset == infoRec2.tankPressOffset ) &&
        ( infoRec1.tankPressSpan   == infoRec2.tankPressSpan   ) &&
        ( infoRec1.regPressOffset  == infoRec2.regPressOffset  ) &&
        ( infoRec1.regPressSpan    == infoRec2.regPressSpan    ) &&
        ( infoRec1.battType        == infoRec2.battType        )
      )
    {
        return true;
    }

    return false;
}


bool TBasePlugJob::GetMPLSInfoRec( int whichInfoRec, MPLS_INFO_REC& infoRec )
{
    if( ( whichInfoRec >= 0 ) && ( whichInfoRec < m_mplsInfoRecs.Count ) )
    {
        infoRec = m_mplsInfoRecs[whichInfoRec];
        return true;
    }

    return false;
}


void TBasePlugJob::AddMPLSInfoRec( MPLS_INFO_REC& newInfoRec )
{
    if( m_jobParams.isDone )
        return;

    // Add an info record only if
    // - There are currently no records or
    // - This new record differs from the last one

    bool bAddRec = false;

    if( m_mplsInfoRecs.Count == 0 )
    {
        bAddRec = true;
    }
    else
    {
        MPLS_INFO_REC lastRec = m_mplsInfoRecs[ m_mplsInfoRecs.Count - 1 ];

        if( !InfoRecsEqual( newInfoRec.uirMasterTec, lastRec.uirMasterTec )
            ||
            !InfoRecsEqual( newInfoRec.uirSlaveTec,  lastRec.uirSlaveTec )
            ||
            !InfoRecsEqual( newInfoRec.uirPlugDet,   lastRec.uirPlugDet )
          )
        {
            bAddRec = true;
        }
    }

    if( bAddRec )
    {
        newInfoRec.creationNbr = GetNextCreationNbr();

        m_mplsInfoRecs.Append( newInfoRec );

        Save();
    }
}


void TBasePlugJob::SetCompleted( bool bIsCompleted )
{
    // Need to be careful with this logic - have to add the comment first
    // since setting the job idDone property true prevents any data being
    // added to the job log
    if( bIsCompleted && !m_jobParams.isDone )
    {
        AddJobComment( "Job completed." );

        m_jobParams.isDone = true;
        Save();
    }
    else if( !bIsCompleted && m_jobParams.isDone )
    {
        AddJobComment( "Job re-opened." );

        m_jobParams.isDone = false;
        Save();
    }
}


void TBasePlugJob::SetInSimMode( bool bInSimMode )
{
    bool bWasInSimMode = m_bInSimMode;

    m_bInSimMode = bInSimMode;

    // If we are now in sim mode, add a job comment
    if( !bWasInSimMode && m_bInSimMode )
        AddJobComment( "In simulation mode." );
}


void TBasePlugJob::BuildSysEventArray( void )
{
    // For each comment with an actual event, build a sys_event record
    for( int iComment = 0; iComment < m_jobComments.Count; iComment++ )
    {
        JOB_COMMENT currComment = m_jobComments[iComment];

        if( currComment.eEvent != eSE_None )
            AddSystemEvent( currComment, iComment );
    }
}


void TBasePlugJob::AddSystemEvent( const JOB_COMMENT& parentComment, int iCommentIndex )
{
    // Only add a system event if we have an event type
    if( parentComment.eEvent == eSE_None )
        return;

    SYSTEM_EVENT seNewEvent;

    seNewEvent.iCommentIndex = iCommentIndex;
    seNewEvent.entryTime     = parentComment.entryTime;
    seNewEvent.eEvent        = parentComment.eEvent;

    String sLongText;
    String sShortText;

    // Re-cast iInfo to a canister enum for more convenient use.
    // Note: iPlugNbr will be > 0 for any canisters that are valid
    // in the current system config.
    eCanisterNbr eCan      = (eCanisterNbr)( parentComment.iInfo );
    int          iPlugNbr  = CanisterToPlugNbr( eCan );

    switch( parentComment.eEvent )
    {
        case eSE_Connected:
            sLongText  = "Connected to MPLS, see job log for details";
            sShortText = "Connected";
            break;

        case eSE_ConfigMismatch:
            sLongText  = "MPLS configuration mismatch, see job log for details";
            sShortText = "Config mismatch";
            break;

        case eSE_StateMismatch:
            sLongText  = "MPLS state mismatch, see job log for details";
            sShortText = "State mismatch";
            break;

        case eSE_Launching:

            if( iPlugNbr > 0 )
            {
                sLongText  = "Launching Plug " + IntToStr( iPlugNbr );
                sShortText = "Launching " + IntToStr( iPlugNbr );
            }
            else
            {
                sLongText  = "Invalid Canister " + IntToStr( parentComment.iInfo ) + " reported as launched";
                sShortText = "Invalid Launch";
            }

            break;

        case eSE_LaunchDone:

            if( iPlugNbr > 0 )
            {
                sLongText  = "Launch completed for Plug " + IntToStr( iPlugNbr );
                sShortText = "Launch " + IntToStr( iPlugNbr ) + " done";
            }
            else
            {
                sLongText  = "Invalid Canister " + IntToStr( parentComment.iInfo ) + " reported as launch done";
                sShortText = "Invalid Launch";
            }

            break;

        case eSE_Cleaning:

            if( iPlugNbr > 0 )
            {
                sLongText  = "Cleaning started for Canister " + IntToStr( iPlugNbr );
                sShortText = "Cleaning " + IntToStr( iPlugNbr );
            }
            else
            {
                sLongText  = "Cleaning reported started for invalid Canister " + IntToStr( parentComment.iInfo );
                sShortText = "Invalid Clean";
            }

            break;

        case eSE_CleanDone:

            if( iPlugNbr > 0 )
            {
                sLongText  = "Cleaning completed for Canister " + IntToStr( iPlugNbr );
                sShortText = "Clean " + IntToStr( iPlugNbr ) + " done";
            }
            else
            {
                sLongText  = "Cleaning reported completed for invalid Canister " + IntToStr( parentComment.iInfo );
                sShortText = "Invalid Clean";
            }

            break;

        case eSE_FlagTriggered:
            sLongText  = "Flag triggered";
            sShortText = "Flag triggered";
            break;

        case eSE_FlagClearing:
            sLongText  = "Resetting flag";
            sShortText = "Resetting flag";
            break;

        case eSE_FlagCleared:
            sLongText  = "Flag reset complete";
            sShortText = "Flag reset done";
            break;

        case eSE_HWError:
            sLongText  = "Hardware reporting errors: 0x" + IntToHex( parentComment.iInfo, 8 );
            sShortText = "Hardware error";
            break;

        case eSE_LowBattery:
            sLongText  = "Low battery alarm";
            sShortText = "Low battery alarm";
            break;

        case eSE_OtherAlarm:
            sLongText  = "Alarm type " + IntToStr( parentComment.iInfo ) + " reported";
            sShortText = "Alarm type " + IntToStr( parentComment.iInfo );
            break;

        default:
            sLongText  = "Unknown event type " + IntToStr( (int)parentComment.eEvent ) + " reported";
            sShortText = "Type " + IntToStr( (int)parentComment.eEvent );
            break;
    }

    seNewEvent.longText  = sLongText;
    seNewEvent.shortText = sShortText;

    // Add the event to the colllection but note we don't have to
    // save the job.
    m_sysEvents.Append( seNewEvent );
}


ePlugState TBasePlugJob::GetPlugState( eCanisterNbr eWhichCan )
{
    if( ( eWhichCan < 0 ) || ( eWhichCan >= eCN_NbrCanisters ) )
        return ePS_NotLoaded;

    return m_jobParams.plugInfo[eWhichCan].eState;
}


time_t TBasePlugJob::GetLaunchTime( eCanisterNbr eWhichCan )
{
    if( ( eWhichCan < 0 ) || ( eWhichCan >= eCN_NbrCanisters ) )
        return ePS_NotLoaded;

    return m_jobParams.plugInfo[eWhichCan].tLaunch;
}


time_t TBasePlugJob::GetCleanTime( eCanisterNbr eWhichCan )
{
    if( ( eWhichCan < 0 ) || ( eWhichCan >= eCN_NbrCanisters ) )
        return false;

    return m_jobParams.plugInfo[eWhichCan].tClean;
}


bool TBasePlugJob::AddRFCRecord( const LOGGED_RFC_DATA& loggedData )
{
    // RFC records are only added if the job is not complete
    if( Completed )
        return true;

    // First add the rec to our internal array
    m_rfcRecs.Append( loggedData );

    // Now save to the log file
    TFileStream* logStream = NULL;
    bool bLoggedOK = false;

    try
    {
        if( FileExists( m_jobDataFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( m_jobDataFileName , fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( m_jobDataFileName, fmCreate );

            AnsiString sHeader = GetRFCRecHdrString() + "\r";

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        AnsiString sLine = ConvertRFCRecToString( loggedData ) + "\r";

        logStream->Write( sLine.c_str(), sLine.Length() );

        bLoggedOK = true;
    }
    catch( ... )
    {
    }

    if( logStream != NULL )
        delete logStream;

    return bLoggedOK;
}


bool TBasePlugJob::GetRFCRecord( int iWhichRec, LOGGED_RFC_DATA& loggedData )
{
    if( ( iWhichRec >= 0 ) && ( iWhichRec < m_rfcRecs.Count ) )
    {
        loggedData = m_rfcRecs[iWhichRec];
        return true;
    }

    return false;
}


bool TBasePlugJob::GetCanLaunch( eCanisterNbr eWhichCan )
{
    // Do some validation first
    if( ( eWhichCan < 0 ) || ( eWhichCan >= eCN_NbrCanisters ) )
        return false;

    // Opeation may be allowed - double check against configuration
    switch( eWhichCan )
    {
        case eCN_Master1:
            // Can always launch this based on config
            break;

        case eCN_Master2:
            // Can only launch this if lower config is two plugs
            if( m_jobInfo.eLowerCanConfig != eCC_TwoShort )
                return false;
            break;

        case eCN_Slave1:
            // Can only launch this if we have an upper config
            if( m_jobInfo.eUpperCanConfig == eCC_NoPlug )
                return false;
            break;

        case eCN_Slave2:
            // Can only launch this if we have two plugs
            if( m_jobInfo.eUpperCanConfig != eCC_TwoShort )
                return false;
            break;

        default:
            // Don't know this canister number
            return false;
    }

    // Double check here that the plug has been loaded
    if( m_jobParams.plugInfo[eWhichCan].eState == ePS_NotLoaded )
        return false;

    // Can launch a plug that's already been launched
    if( m_jobParams.plugInfo[eWhichCan].eState == ePS_Launched )
        return true;

    // Fall through means we're neither cleaning nor have been
    // previously launched. Look 'down' the stack to see if
    // we're either the first plug or the plug prior to us
    // has been launched
    if( eWhichCan == eCN_Master1 )
        return true;

    if( m_jobParams.plugInfo[eWhichCan-1].eState == ePS_Launched )
        return true;

    // Fall through means operation not allowed
    return false;
}


bool TBasePlugJob::GetCanClean( eCanisterNbr eWhichCan )
{
    // Do some validation first
    if( ( eWhichCan < 0 ) || ( eWhichCan >= eCN_NbrCanisters ) )
        return false;

    // Can only clean a canister after its plug has been launched
    if( m_jobParams.plugInfo[eWhichCan].eState == ePS_Launched )
        return true;

    // Fall through means operation not allowed
    return false;
}


DWORD TBasePlugJob::UpdateState( eSystemEvent eWhichEvent, time_t tWhen, int iInfo )
{
    // Updates the state of the job with the passed params. Param iInfo
    // is interpreted in the context of eWhichEvent.

    // First, cannot change the state of a completed job
    if( Completed )
        return US_ERR_JOB_COMPLETE;

    // Should never get a 'no event' here
    if( eWhichEvent == eSE_None )
        return US_ERR_NONE;

    // When updating state, generally three things happen:
    //   - state of the canister gets updated
    //   - a sys event entry is added
    //   - a comment is added to the job log
    DWORD dwErrorBits = 0;

    // Re-cast iInfo to a canister enum for more convenient use.
    // Note: iPlugNbr will be > 0 for any canisters that are valid
    // in the current system config.
    eCanisterNbr eCan      = (eCanisterNbr)iInfo;
    int          iPlugNbr  = CanisterToPlugNbr( eCan );

    String sEventVerb;
    String sComment;

    switch( eWhichEvent )
    {
        case eSE_Launching:

            if( iPlugNbr > 0 )
            {
                m_jobParams.plugInfo[eCan].eState  = ePS_Launched;
                m_jobParams.plugInfo[eCan].tLaunch = tWhen;

                sComment = "Launching Plug " + IntToStr( iPlugNbr );
            }
            else
            {
                sComment = "Invalid Canister " + IntToStr( iInfo ) + " reported as launched";
                dwErrorBits |= US_ERR_ILLEGAL_LAUNCH;
            }

            break;

        case eSE_LaunchDone:

            if( iPlugNbr > 0 )
            {
                if( m_jobParams.plugInfo[eCan].eState != ePS_Launched )
                    sComment = "Launch completed for Plug " + IntToStr( iPlugNbr ) + ", but job log did not show it had been launched yet";
                else
                    sComment = "Launch completed for Plug " + IntToStr( iPlugNbr );

                // Make sure we're now in the launched state
                m_jobParams.plugInfo[eCan].eState = ePS_Launched;
            }
            else
            {
                sComment = "Invalid Canister " + IntToStr( iInfo ) + " reported as launch done";
                dwErrorBits |= US_ERR_ILLEGAL_LAUNCH;
            }

            break;

        case eSE_Cleaning:

            if( iPlugNbr > 0 )
            {
                if( m_jobParams.plugInfo[eCan].eState != ePS_Launched )
                {
                    sComment = "Cleaning reported started for Canister " + IntToStr( iPlugNbr ) + ", but plug not yet launched";
                }
                else
                {
                    m_jobParams.plugInfo[eCan].tClean = tWhen;

                    sComment = "Cleaning started for Canister " + IntToStr( iPlugNbr );
                }
            }
            else
            {
                sComment = "Cleaning reported started for invalid Canister " + IntToStr( iInfo );
                dwErrorBits |= US_ERR_ILLEGAL_CLEAN;
            }

            break;

        case eSE_CleanDone:

            if( iPlugNbr > 0 )
            {
                if( m_jobParams.plugInfo[eCan].eState != ePS_Launched )
                    sComment = "Cleaning reported completed for Canister " + IntToStr( iPlugNbr ) + ", but plug not yet launched";
                else
                    sComment = "Cleaning completed for Canister " + IntToStr( iPlugNbr );
            }
            else
            {
                sComment = "Cleaning reported completed for invalid Canister " + IntToStr( iInfo );
                dwErrorBits |= US_ERR_ILLEGAL_CLEAN;
            }

            break;

        case eSE_FlagTriggered:
            sComment = "Flag triggered";
            break;

        case eSE_FlagClearing:
            sComment = "Resetting flag";
            break;

        case eSE_FlagCleared:
            sComment = "Flag reset complete";
            break;

        case eSE_HWError:
            sComment = "Hardware reporting errors: 0x" + IntToHex( iInfo, 8 );
            dwErrorBits |= US_ERR_HARDWARE_FAULT;
            break;

        case eSE_LowBattery:
            sComment = "Low battery alarm reported.";
            break;

        case eSE_OtherAlarm:
            sComment = "Alarm type " + IntToStr( iInfo ) + " reported.";
            break;

        default:
           // Illegal event for this method or unknown event type - return error indication
           return false;
    }

    // Add our comment, this will also generate a system event
    if( sComment.Length() > 0 )
        AddJobComment( sComment, tWhen, eWhichEvent, iInfo, false /*dont immediately save*/ );

    // Now save all changes to the log
    if( !Save() )
        dwErrorBits |= STH_ERR_SAVE_FAILED;

    return dwErrorBits;
}


DWORD TBasePlugJob::SyncToHardware( const MPLS_SYSTEM_CONFIGURATION& sysConfig, time_t tWhen )
{
    // Called each time we connect to a MPLS to record (and validate)
    // the system configuration.

    // First, cannot change the state of a completed job
    if( Completed )
        return STH_ERR_JOB_COMPLETE;

    // Now verify the configuration
    DWORD dwErrorBits = 0;

    // First, we always assume a master TEC is present. If there is no
    // master TEC, there are no comms. But there has to be a master
    // PIB present.
    if( !sysConfig.compInfo[eMC_MasterPIB].bPresent )
        dwErrorBits |= STH_ERR_MASTER_PIB_MISSING;

    // Check for slave hardware
    if( m_jobInfo.eUpperCanConfig == eCC_NoPlug )
    {
        // Expecting no slave in this case
        if( sysConfig.compInfo[eMC_SlaveTEC].bPresent )
            dwErrorBits |= STH_ERR_SLAVE_PRESENT;
    }
    else
    {
        // Expecting no slave. Report an error if it (or its PIB) are present
        if( !sysConfig.compInfo[eMC_SlaveTEC].bPresent )
            dwErrorBits |= STH_ERR_SLAVE_TEC_MISSING;

        if( !sysConfig.compInfo[eMC_SlavePIB].bPresent )
            dwErrorBits |= STH_ERR_SLAVE_PIB_MISSING;
    }

    // Plug detector must always be present
    if( !sysConfig.compInfo[eMC_PlugDet].bPresent )
        dwErrorBits |= STH_ERR_PLUG_DET_MISSING;

    // Now check for activity. An 'unexepected' activity is a legal one
    // given the current state. An 'illegal' activity is an activity
    // either not valid for the current state or the configuration.
    // We want to be sure our 'connect' event and any associated
    // comments appear in the log first, and then any launch or clean
    // events if one happens to be in progress. So this chunk of logic
    // just detects any issues - well log and update later in the code.
    eCanisterNbr eActiveCan    = eCN_NotApplicable;
    bool         bNeedToLaunch = false;
    bool         bNeedToClean  = false;

    switch( sysConfig.iActiveSol[eMU_MasterMPLS] )
    {
        case 1:  eActiveCan = eCN_Master1;   bNeedToLaunch = true;   break;
        case 2:  eActiveCan = eCN_Master1;   bNeedToClean  = false;  break;
        case 3:  eActiveCan = eCN_Master2;   bNeedToLaunch = true;   break;
        case 4:  eActiveCan = eCN_Master2;   bNeedToClean  = false;  break;
    }

    switch( sysConfig.iActiveSol[eMU_SlaveMPLS] )
    {
        case 1:  eActiveCan = eCN_Slave1;   bNeedToLaunch = true;   break;
        case 2:  eActiveCan = eCN_Slave1;   bNeedToClean  = false;  break;
        case 3:  eActiveCan = eCN_Slave2;   bNeedToLaunch = true;   break;
        case 4:  eActiveCan = eCN_Slave2;   bNeedToClean  = false;  break;
    }

    if( eActiveCan != eCN_NotApplicable )
    {
        if( bNeedToLaunch )
        {
            if( GetCanLaunch( eActiveCan ) )
                dwErrorBits |= STH_ERR_UNEXP_LAUNCH_ACTIVE;
            else
                dwErrorBits |= STH_ERR_ILLEGAL_LAUNCH_ACTIVE;
        }
        else if( bNeedToClean )
        {
            if( GetCanClean( eActiveCan ) )
                dwErrorBits |= STH_ERR_UNEXP_CLEAN_ACTIVE;
            else
                dwErrorBits |= STH_ERR_ILLEGAL_CLEAN_ACTIVE;
        }
    }

    // Now check if we have a TEC that's changed
    if( m_jobParams.components[eMC_MasterTEC].compID.sSN.Length() > 0 )
    {
        if( m_jobParams.components[eMC_MasterTEC].compID.sSN != sysConfig.compInfo[eMC_MasterTEC].compID.sSN )
            dwErrorBits |= STH_WARN_MASTER_TEC_CHANGED;
    }

    if( m_jobInfo.eUpperCanConfig != eCC_NoPlug )
    {
        if( m_jobParams.components[eMC_SlaveTEC].compID.sSN.Length() > 0 )
        {
            if( m_jobParams.components[eMC_SlaveTEC].compID.sSN != sysConfig.compInfo[eMC_SlaveTEC].compID.sSN )
                dwErrorBits |= STH_WARN_SLAVE_TEC_CHANGED;
        }
    }

    // Add connection comment
    String sConnectComment;

    if( m_jobParams.components[eMC_MasterTEC].compID.sSN.IsEmpty() )
        sConnectComment = "Connected to MPLS SN " + sysConfig.compInfo[eMC_MasterTEC].compID.sSN;
    else if( dwErrorBits & STH_WARN_MASTER_TEC_CHANGED )
        sConnectComment = "Connected to new MPLS SN " + sysConfig.compInfo[eMC_MasterTEC].compID.sSN;
    else
        sConnectComment = "Reconnected to MPLS SN " + sysConfig.compInfo[eMC_MasterTEC].compID.sSN;

    AddJobComment( sConnectComment, tWhen, eSE_Connected, 0, false );

    // Okay to update job params with new SNs now
    for( int iComp = 0; iComp < eMC_NbrMPLSComps; iComp++ )
    {
       m_jobParams.components[iComp].bPresent = sysConfig.compInfo[iComp].bPresent;
       m_jobParams.components[iComp].compID   = sysConfig.compInfo[iComp].compID;
    }

    // If we have a config mismatch, create an event and log it
    TStringList* pErrorStrings = new TStringList();

    if( dwErrorBits & STH_ERR_MASTER_PIB_MISSING )
        pErrorStrings->Add( " Master PIB missing" );

    if( dwErrorBits & STH_ERR_SLAVE_PRESENT )
        pErrorStrings->Add( " unexpected Slave TEC present" );

    if( dwErrorBits & STH_ERR_SLAVE_TEC_MISSING )
        pErrorStrings->Add( " Slave TEC not found" );

    if( dwErrorBits & STH_ERR_SLAVE_PIB_MISSING )
        pErrorStrings->Add( " Slave PIB missing" );

    if( dwErrorBits & STH_ERR_PLUG_DET_MISSING )
        pErrorStrings->Add( " Plug Detector missing" );

    if( pErrorStrings->Count > 0 )
        AddJobComment( "Configuration mismatch: " + pErrorStrings->CommaText, tWhen, eSE_ConfigMismatch, 0, false );

    // Now check for a state mismatch
    pErrorStrings->Clear();

    if( dwErrorBits & STH_ERR_UNEXP_CLEAN_ACTIVE )
        pErrorStrings->Add( " Canister " + IntToStr( (int)eActiveCan ) + " being cleaned on connect" );

    if( dwErrorBits & STH_ERR_UNEXP_LAUNCH_ACTIVE )
        pErrorStrings->Add( " Canister " + IntToStr( (int)eActiveCan ) + " being launched on connect" );

    if( dwErrorBits & STH_ERR_ILLEGAL_CLEAN_ACTIVE )
        pErrorStrings->Add( " Canister " + IntToStr( (int)eActiveCan ) + " not in job configuration but being cleaned on connect" );

    if( dwErrorBits & STH_ERR_ILLEGAL_LAUNCH_ACTIVE )
        pErrorStrings->Add( " Canister " + IntToStr( (int)eActiveCan ) + " not in job configuration but being launched on connect" );

    if( pErrorStrings->Count > 0 )
        AddJobComment( "State mismatch: " + pErrorStrings->CommaText, tWhen, eSE_StateMismatch, 0, false );

     // Finally, sync any legal active activity
     if( bNeedToLaunch )
         UpdateState( eSE_Launching, tWhen, eActiveCan );
     else if( bNeedToClean )
         UpdateState( eSE_Cleaning, tWhen, eActiveCan );

    // Now try to save the job
    if( !Save() )
        dwErrorBits |= STH_ERR_SAVE_FAILED;

    return dwErrorBits;
}


int TBasePlugJob::CanisterToPlugNbr( eCanisterNbr eWhichCanister )
{
    // Converts a canister enum to a 1-based plug number based.
    // Returns 0 if the enum is not valid for the given configuration.
    // Plugs are numbered starting at 1 for the master unit.
    if( ( eWhichCanister < 0 ) || ( eWhichCanister >= eCN_NbrCanisters ) )
        return 0;

    return m_iPlugNbr[eWhichCanister];
}


String TBasePlugJob::GetJobDate( void )
{
    return JobDateToString( m_jobInfo.stJobDate );
}


String TBasePlugJob::GetJobCreatedDate( void )
{
    return JobTimeToLocalString( m_jobInfo.jobCreateTime, true /*bIncludeDate*/ );
}


String TBasePlugJob::JobDateToString( const SYSTEMTIME& stJobDate )
{
    String sJobDate;
    sJobDate.printf( L"%04d-%02d-%02d", stJobDate.wYear, stJobDate.wMonth, stJobDate.wDay );

    return sJobDate;
}


String TBasePlugJob::JobTimeToGMTString( time_t tTime, bool bIncludeDate )
{
    struct tm *tmTime = gmtime( &tTime );

    if( tmTime == NULL )
        return "Bad time_t";

    String sJobDate;
    sJobDate.printf( L"%04d-%02d-%02d", tmTime->tm_year + 1900, tmTime->tm_mon + 1, tmTime->tm_mday );

    String sJobTime;
    sJobTime.printf( L"%02d:%02d:%02d", tmTime->tm_hour, tmTime->tm_min, tmTime->tm_sec );

    if( bIncludeDate )
        return sJobDate + " " + sJobTime;
    else
        return sJobTime;
}


String TBasePlugJob::JobTimeToLocalString( time_t tTime, bool bIncludeDate )
{
    struct tm *tmTime = localtime( &tTime );

    if( tmTime == NULL )
        return "Bad time_t";

    String sJobDate;
    sJobDate.printf( L"%04d-%02d-%02d", tmTime->tm_year + 1900, tmTime->tm_mon + 1, tmTime->tm_mday );

    String sJobTime;
    sJobTime.printf( L"%02d:%02d:%02d", tmTime->tm_hour, tmTime->tm_min, tmTime->tm_sec );

    if( bIncludeDate )
        return sJobDate + " " + sJobTime;
    else
        return sJobTime;
}


bool TBasePlugJob::GetJobInfo( const String& jobFileName, PLUG_JOB_INFO& jobInfo )
{
    // Returns true if the job name exists and populates the jobInfo struct.
    // Returns false if the job is not found.
    if( !FileExists( jobFileName ) )
        return false;

    bool bSuccess = false;

    _di_IXMLDocument xmlDoc = NULL;

    try
    {
        _di_IXMLDocument xmlDoc = LoadXMLDocument( jobFileName );

        /* This is where we'd validate the MD5 checksum */

        // Validate the root node contents
        WORD jobFileVer;

        if( !GetXMLDocOptions( xmlDoc, jobFileVer, jobInfo ) )
            Abort();

        if( jobFileVer != CURR_JOB_FILE_VER )
            Abort();

        // Fall through means success
        bSuccess = true;
    }
    catch( ... )
    {
    }

    if( xmlDoc != NULL )
        xmlDoc.Release();

    return bSuccess;
}

