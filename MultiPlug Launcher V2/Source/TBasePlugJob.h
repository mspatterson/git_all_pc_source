#ifndef TBasePlugJobH
#define TBasePlugJobH

//******************************************************************************
//
//  TBasePlugJob.h: abstract base class for all plug job classes
//
//******************************************************************************

#include "TStructArray.h"
#include "UnitsOfMeasure.h"
#include "MPLSDefs.h"


extern const String MPLSJobFileExt;
extern const String MPLSDataFileExt;


// The system config structure contains information about the MPLS
// we are currently connected to.
typedef struct {

    struct {
        bool         bPresent;
        MPLS_COMP_ID compID;
    } compInfo[eMC_NbrMPLSComps];

    int iActiveSol[eMU_NbrMPLSUnits];

} MPLS_SYSTEM_CONFIGURATION;


// The JOB_INFO contains 'unchangeable' information about a job.
typedef struct _PLUG_JOB_INFO {
    String           wellName;
    SYSTEMTIME       stJobDate;       // As entered by the user
    time_t           jobCreateTime;   // Actual time the job was created
    String           timeZoneDisplay; // Display entry under a given Time Zone key
    bool             isDST;           // True if DST was in effect at the start of the job
    int              bias;            // As reported by TIME_ZONE_INFORMATION struct
    int              standardBias;    // As reported by TIME_ZONE_INFORMATION struct
    int              daylightBias;    // As reported by TIME_ZONE_INFORMATION struct
    bool             isTestJob;
    String           clientName;
    String           wellNbr;
    String           rig;
    String           leadHand;
    String           cementCo;
    String           companyManDay;
    String           companyManNight;
    String           drillerDay;
    String           drillerNight;
    String           rigMgrDay;
    String           rigMgrNight;
    String           casingSize;
    String           casingType;
    String           casingWeight;
    String           spacerWeight;
    String           spacerVolume;
    String           leadWeight;
    String           leadVolume;
    String           tailWeight;
    String           tailVolume;
    eCanisterConfig  eLowerCanConfig;
    eCanisterConfig  eUpperCanConfig;
    UNITS_OF_MEASURE unitsOfMeasure;

    // Define a constructor
    _PLUG_JOB_INFO()
    {
        Clear();
    }

    void Clear( void )
    {
        SYSTEMTIME stDefault = { 0 };

        wellName        = "";
        isTestJob       = false;
        jobCreateTime   = 0;
        stJobDate       = stDefault;
        timeZoneDisplay = "";
        isDST           = false;
        bias            = 0;
        standardBias    = 0;
        daylightBias    = 0;

        clientName      = "";
        wellNbr         = "";
        rig             = "";
        leadHand        = "";
        cementCo        = "";
        companyManDay   = "";
        companyManNight = "";
        drillerDay      = "";
        drillerNight    = "";
        rigMgrDay       = "";
        rigMgrNight     = "";
        casingSize      = "";
        casingType      = "";
        casingWeight    = "";
        spacerVolume    = "";
        spacerWeight    = "";
        tailVolume      = "";
        tailWeight      = "";
        leadVolume      = "";
        leadWeight      = "";

        eLowerCanConfig = eCC_NoPlug;
        eUpperCanConfig = eCC_NoPlug;
        unitsOfMeasure  = UOM_IMPERIAL;
    }

} PLUG_JOB_INFO;


// JOB_PARAMS contains information about a job that can change
// through the job
typedef struct {
    bool       isDone;
    int        lastCreationNbr;

    struct {
        bool         bPresent;
        MPLS_COMP_ID compID;
    } components[eMC_NbrMPLSComps];

    struct {
        ePlugState eState;
        time_t     tClean;
        time_t     tLaunch;
    } plugInfo[eCN_NbrCanisters];

} JOB_PARAMS;


// A JOB_COMMENT struct is created for each comment or event
// logged throughout the course of a job
typedef struct {
    int          creationNbr;
    time_t       entryTime;
    String       entryText;
    eSystemEvent eEvent;
    int          iInfo;       // Interpret in context of eEvent
} JOB_COMMENT;


// A SYSTEM_EVENT struct is synthesized from comment records.
// They are not saved to the job database.
typedef struct {
    int          iCommentIndex;
    time_t       entryTime;
    eSystemEvent eEvent;
    String       longText;
    String       shortText;
} SYSTEM_EVENT;


// A MPLS_INFO_REC contains information on the specifics of the
// MPLS device we've connected to.
typedef struct _UNIT_INFO_REC {
    bool      bPresent;
    String    devSN;
    String    devFWRev;
    String    devHWRev;
    bool      pibPresent;
    String    pibFWRev;
    String    pibHWRev;
    String    mfgInfo;
    String    mfgDate;
    String    lastCalDate;
    String    calVersion;
    String    calRev;
    String    tankPressOffset;
    String    tankPressSpan;
    String    regPressOffset;
    String    regPressSpan;
    String    battType;

    // Define a constructor
    _UNIT_INFO_REC()
    {
        Clear();
    }

    void Clear( void )
    {
        bPresent        = false;
        devSN           = "";
        devFWRev        = "";
        devHWRev        = "";
        pibPresent      = false;
        pibFWRev        = "";
        pibHWRev        = "";
        mfgInfo         = "";
        mfgDate         = "----/--/--";;
        lastCalDate     = "----/--/--";;
        calVersion      = "";
        calRev          = "";
        tankPressOffset = "";
        tankPressSpan   = "";
        regPressOffset  = "";
        regPressSpan    = "";
        battType        = "";
    }

} UNIT_INFO_REC;


typedef struct _MPLS_INFO_REC {
    int           creationNbr;
    time_t        entryTime;
    UNIT_INFO_REC uirMasterTec;
    UNIT_INFO_REC uirSlaveTec;
    UNIT_INFO_REC uirPlugDet;

    // Define a constructor
    _MPLS_INFO_REC()
    {
        Clear();
    }

    void Clear( void )
    {
        creationNbr = 0;
        entryTime   = 0;
        uirMasterTec.Clear();
        uirSlaveTec.Clear();
        uirPlugDet.Clear();
    }

} MPLS_INFO_REC;


class TBasePlugJob : public TObject
{
  private:

  protected:

    // General job variables
    String           m_jobLogFileName;
    String           m_jobDataFileName;
    bool             m_jobLoaded;
    PLUG_JOB_INFO    m_jobInfo;
    JOB_PARAMS       m_jobParams;

    time_t           m_solOnTimeLaunch;
    time_t           m_solOnTimeClean;
    time_t           m_solOnTimeReset;

    int              m_fileSaveErrors;

    TStringList*     m_preJobRemStrings;
    TStringList*     m_postJobRemStrings;

    // System control vars
    bool             m_bInSimMode;

    virtual void InitJobInfo( void );
        // Initializes all class variables

    virtual void LoadJob( const String& sJobFileName );
        // Helper function use to initialize class from a job file.
        // Will set m_jobLoaded to true on success. Can be overriden
        // by derived classes.

    virtual bool Save( void );
        // Internal Save() handler. Can be called even if in sim mode,
        // and in that case no changes are made to the file. Derived
        // classes may override this method

    int GetNextCreationNbr( void );
        // Returns a unique sequence number for a section rec, connection
        // rec, or main comment

    TStructArray<JOB_COMMENT> m_jobComments;
    int GetNbrJobComments( void ) { return m_jobComments.Count; }

    TStructArray<SYSTEM_EVENT> m_sysEvents;
    int GetNbrSysEvents( void ) { return m_sysEvents.Count; }

    TStructArray<MPLS_INFO_REC> m_mplsInfoRecs;
    int GetNbrMPLSInfoRecs( void ) { return m_mplsInfoRecs.Count; }

    TStructArray<LOGGED_RFC_DATA> m_rfcRecs;
    int GetNbrRFCRecs( void ) { return m_rfcRecs.Count; }

    int m_iPlugNbr[eCN_NbrCanisters];
        // Maps a canister enum to a 1-based plug number

    // Helpers
    void SetCompleted( bool bIsCompleted );
    void SetInSimMode( bool bInSimMode );

    void BuildSysEventArray( void );
    void AddSystemEvent( const JOB_COMMENT& parentComment, int iCommentIndex );

    bool InfoRecsEqual( const UNIT_INFO_REC& infoRec1, const UNIT_INFO_REC& InfoRec2 );

    // Property handlers
    ePlugState GetPlugState ( eCanisterNbr eWhichCan );
    time_t     GetLaunchTime( eCanisterNbr eWhichCan );
    time_t     GetCleanTime ( eCanisterNbr eWhichCan );
    bool       GetCanLaunch ( eCanisterNbr eWhichCan );
    bool       GetCanClean  ( eCanisterNbr eWhichCan );

    String     GetJobDate( void );
    String     GetJobCreatedDate( void );

    // Event handlers
    TNotifyEvent m_onCommentAdded;

  public:

    virtual __fastcall TBasePlugJob( const String& sJobFileName );
    virtual __fastcall ~TBasePlugJob();
        // Constructor for a plug job. Plug job file must already exist.

    __property String JobFileName  = { read = m_jobLogFileName };
    __property String DataFileName = { read = m_jobDataFileName };
        // Return the names of the main job file or the data (csv) log file

    __property UNITS_OF_MEASURE Units = { read = m_jobInfo.unitsOfMeasure };
        // Returns the units of measure in use for this job

    __property bool Loaded = { read = m_jobLoaded };
        // Returns true if the job was successfully loaded

    __property bool Completed = { read = m_jobParams.isDone, write = SetCompleted };
        // Gets / sets completion status. No changes are allowed to completed jobs

    // Generic read-only properties
    __property String          WellName       = { read = m_jobInfo.wellName };
    __property String          JobDate        = { read = GetJobDate };
    __property String          CreatedDate    = { read = GetJobCreatedDate };
    __property bool            JobLoaded      = { read = m_jobLoaded };
    __property bool            InSimMode      = { read = m_bInSimMode, write = SetInSimMode };
    __property bool            IsTestJob      = { read = m_jobInfo.isTestJob };
    __property eCanisterConfig UpperCanister  = { read = m_jobInfo.eUpperCanConfig };
    __property eCanisterConfig LowerCanister  = { read = m_jobInfo.eLowerCanConfig };

    __property int             FileSaveErrors = { read = m_fileSaveErrors };

    // System state properties
    __property bool       CanLaunch[eCanisterNbr] = { read = GetCanLaunch };
    __property bool       CanClean [eCanisterNbr] = { read = GetCanClean  };

    // Plugs
    __property ePlugState PlugState [eCanisterNbr] = { read = GetPlugState };
    __property time_t     LaunchTime[eCanisterNbr] = { read = GetLaunchTime };
    __property time_t     CleanTime [eCanisterNbr] = { read = GetCleanTime };

    DWORD UpdateState( eSystemEvent eWhichEvent, time_t tWhen, int iInfo );
        // Updates the state of the job with the passed params. Param iInfo
        // is interpreted in the context of eWhichEvent. If the job is complete
        // this method does nothing. Return value will be bit flags indicating
        // one or more problems.
        #define US_ERR_NONE                    0x00000000    /* Expected outcome */
        #define US_ERR_JOB_COMPLETE            0x00000001    /* Job is marked as complete - cannot make changes */
        #define US_ERR_SAVE_FAILED             0x00000002    /* Could not save changes to file */
        #define US_ERR_ILLEGAL_CLEAN           0x00000004    /* Solenoid not valid is this config reported as cleaning */
        #define US_ERR_ILLEGAL_LAUNCH          0x00000008    /* Solenoid not valid is this config reported as launching */
        #define US_ERR_HARDWARE_FAULT          0x00000010    /* Hardware reporting problems */

    DWORD SyncToHardware( const MPLS_SYSTEM_CONFIGURATION& sysConfig, time_t tWhen );
        // Called each time we connect to a MPLS to record (and validate) the
        // system configuration. If the job is complete this method does
        // nothing. Returns bit flags indicating one or more problems
        #define STH_ERR_NONE                   0x00000000    /* Expected outcome */
        #define STH_ERR_JOB_COMPLETE           0x00000001    /* Job is marked as complete - cannot make changes */
        #define STH_ERR_SAVE_FAILED            0x00000002    /* Could not save changes to file */
        #define STH_ERR_MASTER_PIB_MISSING     0x00000004    /* Master PIB must always be present */
        #define STH_ERR_SLAVE_PRESENT          0x00000008    /* Slave present when it shouldn't be */
        #define STH_ERR_SLAVE_TEC_MISSING      0x00000010    /* Slave TEC required but missing */
        #define STH_ERR_SLAVE_PIB_MISSING      0x00000020    /* Slave PIB required but missing */
        #define STH_ERR_PLUG_DET_MISSING       0x00000040    /* Plug Detector required but missing */
        #define STH_ERR_UNEXP_CLEAN_ACTIVE     0x00000100    /* Cleaning solenoid active at job launch */
        #define STH_ERR_UNEXP_LAUNCH_ACTIVE    0x00000200    /* Launch solenoid active at job launch */
        #define STH_ERR_ILLEGAL_CLEAN_ACTIVE   0x00000400    /* Solenoid not valid is this config cleaning at job launch */
        #define STH_ERR_ILLEGAL_LAUNCH_ACTIVE  0x00000800    /* Solenoid not valid is this config launching at job launch */
        #define STH_WARN_MASTER_TEC_CHANGED    0x00001000    /* Master TEC SN does not previous SN */
        #define STH_WARN_SLAVE_TEC_CHANGED     0x00002000    /* Slave TEC SN does not previous SN */

    int CanisterToPlugNbr( eCanisterNbr eWhichCanister );
        // Converts a canister enum to a 1-based plug number based.
        // Returns 0 if the enum is not valid for the given configuration.

    // Job Info
    void GetJobInfo (       PLUG_JOB_INFO& jobInfo );
    bool SaveJobInfo( const PLUG_JOB_INFO& jobInfo );

    // Main Comments
    __property int NbrJobComments  = { read = GetNbrJobComments };
    bool           GetJobComment( int whichComment, JOB_COMMENT& comment );
    bool           AddJobComment( const String& commentText, time_t tWhen = 0, eSystemEvent eEvtType = eSE_None, int iEvtInfo = 0, bool bSaveImmediately = true );

    __property TNotifyEvent OnCommentAdded = { read = m_onCommentAdded, write = m_onCommentAdded };

    // Job remarks
    typedef enum {
        eJRT_Pre,
        eJRT_Post,
        eJRT_NbrJobRemarkTypes
    } eJobRemarkType;

    bool GetJobRemarks ( eJobRemarkType eType, TStringList* pRemarks );
    bool SaveJobRemarks( eJobRemarkType eType, TStringList* pRemarks );

    // System Events. Can't add system events directly. They are added
    // when calls to UpdateState() are made
    __property int NbrSysEvents = { read = GetNbrSysEvents };
    bool           GetSystemEvent( int whichEvent, SYSTEM_EVENT& sysEvent );

    // Logged RFC Data
    __property int NbrRFCRecords = { read = GetNbrRFCRecs };

    bool GetRFCRecord( int iWhichRec, LOGGED_RFC_DATA& loggedData );
    bool AddRFCRecord( const LOGGED_RFC_DATA& loggedData );

    // Information Records
    __property int NbrMPLSInfoRecs = { read = GetNbrMPLSInfoRecs };

    bool           GetMPLSInfoRec( int whichInfoRec, MPLS_INFO_REC& infoRec );
    void           AddMPLSInfoRec( MPLS_INFO_REC& newInfoRec );

    // Backup
    virtual void   SaveBackup( void ) = 0;
        // Derived classes must implement this method.

    // Helpers
    static String  JobDateToString( const SYSTEMTIME& stJobDate );
    static String  JobTimeToGMTString  ( time_t tTime, bool bIncludeDate = true );
    static String  JobTimeToLocalString( time_t tTime, bool bIncludeDate = true );

    static bool CreateJobFile( const PLUG_JOB_INFO& jobInfo, const String& sJobFileName );
        // Creates the basic structure for a job.

    static bool    GetJobInfo( const String& jobFileName, PLUG_JOB_INFO& jobInfo );
        // Returns true if the job name exists and populates the jobInfo struct.
        // Returns false if the job is not found.
};


#endif
