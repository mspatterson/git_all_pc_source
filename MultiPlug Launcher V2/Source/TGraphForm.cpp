#include <vcl.h>
#pragma hdrstop

#include "TGraphForm.h"
#include "ApplUtils.h"
#include "TViewWidthCombo.h"

#pragma package(smart_init)
#pragma link "VCLTee.TeeScroB"
#pragma link "VCLTee.TeeEdit"
#pragma link "VCLTee.TeeTools"
#pragma resource "*.dfm"


TGraphForm *GraphForm;


//
// Notes about the scrollbar: IMHO the VCL does not correctly implement
// validation for page size with respect to max. If your scrollbar
// min/max range is 0 to 99, you're displaying 100 elements and therefore
// should be able to set page size to 100. However the VCL will not let
// that happen (page size must be <= max). Therefore, we have to set
// min/max to be 1 to 100 to be able to set page size to 100. (eg, the
// scrollbar values have to be 1-based)
//
// A second note is that property of the scrollbar is used to indicate
// the first element being displayed, with page size number of elements
// being displayed. If we want to stop scrolling when the last element
// just becomes visible we have to programmatically limit the scrollbar
// pos so that never exceeds max - page size.
//
// Third, the SmallChange and LargeChange properties are only used
// internally by the VCL - these values are neither validated nor
// propogated down to the OS. Therefore, you can change these values
// relatively freely without worrying about refresh or validation issues.
//
// Lastly, take caution in indiscriminately changing the page size
// value to work around the VCL validation. Changing the page size prop
// causes the scroll bar to redraw and therefore you can see flickering
// in the bar if you do this. Also, use the SetParams() call if you need
// to change max and pos concurrently, as that also prevents a double
// refresh/flicker of the scrollbar.
static const int ScrollBarMin = 1;

// Define a min view width for the case where we're displaying the
// entire job but don't have much data yet (5 minutes)
static const time_t MinEntireJobViewWidth = 5 * 60;


__fastcall TGraphForm::TGraphForm(TComponent* Owner) : TForm(Owner)
{
    // Hook label draw handler so we can draw our own labels
    DataChart->Axes->Bottom->OnDrawLabel = OnDrawBottomAxisLabel;

    // Make sure time axis format is valid
    DataChart->Axes->Bottom->AxisValuesFormat = "#";
    DataChart->Axes->Bottom->LabelsSeparation = 0;
    DataChart->Axes->Bottom->LabelStyle       = talValue;

    // Flip position of Custom Axis 0 (temperature)... can't seem to
    // do that at design time
    DataChart->CustomAxes->Items[0]->OtherSide = true;

    // Panning is handled manually, so disable it for now. As well,
    // zoom is not allowed
    DataChart->AllowPanning = Vcltee::Teeprocs::pmNone;
    DataChart->AllowZoom    = false;

    // Make sure the graph is using its internal print buffer
    DataChart->BufferedDisplay = true;

    // Construct our view width combo
    m_viewWidthCombo = new TViewWidthCombo( this, ControlPanel );

    // Position determined by experimentation as setting by
    // using constants didn't work out. Leave height at default
    // as the combo wasn't responding to that changing.
    m_viewWidthCombo->Top   = 8;
    m_viewWidthCombo->Left  = 8;
    m_viewWidthCombo->Width = GraphPanBtn->Left - 11;

    m_viewWidthCombo->OnViewWidthChange = DoOnViewWidthChange;

    // Init 'doing scrolling' flag just once
    m_bScrolling = false;

    // Mouse won't be over a series at this moment
    m_activeSeries = NULL;

    // Allow RFC updates by default
    m_bUpdateSuspended = false;
}


void TGraphForm::LoadNewJob( TBasePlugJob* newJob )
{
    // Call to load a new job to display. If newJob is NULL, no data is displayed
    if( newJob == NULL )
        Caption = "MPLS Graph Form";
    else
        Caption = newJob->WellName + ", " + newJob->JobDate;

    m_currJob = newJob;

    // When loading a new job, allow RFC updates again
    m_bUpdateSuspended = false;

    // Set default display on loading of new job
    m_iLastRFCCount    = 0;
    m_tLastRFC         = 0;
    m_tFirstRFC        = 0;

    if( ( m_currJob == NULL ) || ( m_currJob->UpperCanister == eCC_NoPlug ) )
    {
        SlvTankPressSeries->Visible = false;
        SlvRegPressSeries->Visible  = false;
        SlvBattVoltsSeries->Visible = false;

        SlvTankPressSeries->ShowInLegend = false;
        SlvRegPressSeries->ShowInLegend  = false;
        SlvBattVoltsSeries->ShowInLegend = false;
    }
    else
    {
        SlvTankPressSeries->Visible = true;
        SlvRegPressSeries->Visible  = true;
        SlvBattVoltsSeries->Visible = true;

        SlvTankPressSeries->ShowInLegend = true;
        SlvRegPressSeries->ShowInLegend  = true;
        SlvBattVoltsSeries->ShowInLegend = true;
    }

    // Force the legend values to be justified (otherwise the graph
    // sometimes screws up when clicking on a legend item)
    DataChart->Legend->HorizJustify = ljYes;

    // Set the value formats for our series. Text is included in the
    // value format so that we have a "poor man's" way to determine
    // what series hint is being displayed by the tooltip tool (for
    // some inane reason, Steema didn't think a user would have to
    // know what series a hint was being displayed for). For the
    // EventSeries, we don't actually display a number but instead
    // display a hint we format based on the closest point on that
    // series. Note that units of measure are included here - we may
    // need to dynamically set those in the future if different
    // units of measure need to be supported
    RPMSeries->ValueFormat           = "#0.0 RPM";
    TempSeries->ValueFormat          = "#0.0 C";
    MastTankPressSeries->ValueFormat = "#,##0 PSI";
    MastRegPressSeries->ValueFormat  = "#,##0 PSI";
    SlvRegPressSeries->ValueFormat   = "#,##0 PSI";
    SlvTankPressSeries->ValueFormat  = "#,##0 PSI";
    MastBattVoltsSeries->ValueFormat = "#0.000 VBat Master";
    SlvBattVoltsSeries->ValueFormat  = "#0.000 VBat Slave";
    PlugDetSeries->ValueFormat       = "#0";
    EventSeries->ValueFormat         = "Event";   // No point displaying Y value, as it is fixed for this series

    // Force new graph to show last data from job. Bracket
    // this in a beginupdate / endupdate call to speed up
    // initial loading
    for( int iSeries = 0; iSeries < DataChart->SeriesCount(); iSeries++ )
        DataChart->Series[iSeries]->BeginUpdate();

    try
    {
        RefreshGraph( true /*reload all data*/ );
    }
    catch( ... )
    {
    }

    for( int iSeries = 0; iSeries < DataChart->SeriesCount(); iSeries++ )
        DataChart->Series[iSeries]->EndUpdate();

    // Sync to current width selection
    DoOnViewWidthChange( NULL );
}


void TGraphForm::LoadNewData( void )
{
    // Called when new data has been logged. Graph will display the new data.
    if( m_currJob == NULL )
        return;

    RefreshGraph( false );
}


void TGraphForm::RefreshGraph( bool bReloadData )
{
    if( m_bUpdateSuspended )
        return;

    if( bReloadData )
    {
        // First clear all the data
        for( int iSeries = 0; iSeries < DataChart->SeriesCount(); iSeries++ )
            DataChart->Series[iSeries]->Clear();

        m_iLastRFCCount = 0;
        m_tFirstRFC     = 0;
        m_tLastRFC      = 0;

        m_iLastEventCount = 0;

        // Set scrollbar defaults
        DisableScrollBar();

        // When re-loading data, disable all series update since this process
        // can take a long time
        for( int iSeries = 0; iSeries < DataChart->SeriesCount(); iSeries++ )
            DataChart->SeriesList->Items[iSeries]->BeginUpdate();
    }

    try
    {
        // Add new data
        if( m_currJob != NULL )
        {
            // Add RFC's
            while( m_iLastRFCCount < m_currJob->NbrRFCRecords )
            {
                LOGGED_RFC_DATA loggedData;

                if( m_currJob->GetRFCRecord( m_iLastRFCCount, loggedData ) )
                {
                    // If this is the first record, the note its timestamp.
                    // Otherwise, fill in missing seconds with nulls.
                    if( m_tFirstRFC == 0 )
                        m_tFirstRFC = loggedData.tRFC;

                    if( m_iLastRFCCount > 0 )
                    {
                        // Null fill any gaps in time. Look for gaps greater
                        // then 2 seconds because it is possible for a one
                        // second gap to occur due to variations in the
                        // clock in each MPLS unit vs the PC's clock
                        while( m_tLastRFC + 2 < loggedData.tRFC )
                        {
                            m_tLastRFC++;

                            RPMSeries->AddNullXY ( m_tLastRFC, 0 );
                            TempSeries->AddNullXY( m_tLastRFC, 0 );

                            MastTankPressSeries->AddNullXY( m_tLastRFC, 0 );
                            MastRegPressSeries->AddNullXY ( m_tLastRFC, 0 );
                            MastBattVoltsSeries->AddNullXY( m_tLastRFC, 0 );

                            PlugDetSeries->AddNullXY( m_tLastRFC, 0 );

                            if( m_currJob->UpperCanister != eCC_NoPlug )
                            {
                                SlvTankPressSeries->AddNullXY( m_tLastRFC, 0 );
                                SlvRegPressSeries->AddNullXY ( m_tLastRFC, 0 );
                                SlvBattVoltsSeries->AddNullXY( m_tLastRFC, 0 );
                            }
                        }
                    }

                    // Technically the timestamp should never go backwards, but if
                    // it does we'll have to skip over this record.
                    bool bHaveGoodTimestamp = true;

                    if( m_tLastRFC > 0 )
                    {
                         if( loggedData.tRFC < m_tLastRFC )
                             bHaveGoodTimestamp = false;
                    }

                    if( bHaveGoodTimestamp )
                    {
                        m_tLastRFC = loggedData.tRFC;

                        double dtRFC = (double)loggedData.tRFC + (double)( loggedData.wMsecs % 1000 ) / 1000.0;

                        AddSeriesXY( RPMSeries,  dtRFC, loggedData.rpm );
                        AddSeriesXY( TempSeries, dtRFC, loggedData.temp );

                        AddSeriesXY( MastTankPressSeries, dtRFC, loggedData.tecState[eMU_MasterMPLS].tankPress );
                        AddSeriesXY( MastRegPressSeries,  dtRFC, loggedData.tecState[eMU_MasterMPLS].regPress );
                        AddSeriesXY( MastBattVoltsSeries, dtRFC, loggedData.tecState[eMU_MasterMPLS].battVolts * 10 );

                        if( m_currJob->UpperCanister != eCC_NoPlug )
                        {
                            AddSeriesXY( SlvTankPressSeries, dtRFC, loggedData.tecState[eMU_SlaveMPLS].tankPress );
                            AddSeriesXY( SlvRegPressSeries,  dtRFC, loggedData.tecState[eMU_SlaveMPLS].regPress );
                            AddSeriesXY( SlvBattVoltsSeries, dtRFC, loggedData.tecState[eMU_SlaveMPLS].battVolts * 10 );
                        }

                        // Spread plug detector data out evenly across the time interval.
                        // First count the number of points to plot
                        int iNbrPts = 0;

                        for( int iPt = 0; iPt < MAX_PLUG_DET_READINGS; iPt++ )
                        {
                            if( loggedData.plugDetReadings[iPt] == 0xFFFF )
                                break;

                            iNbrPts++;
                        }

                        // If this is the first RFC, skip plotting it so we don't
                        // get a wierd intro segment on this seris
                        if( m_iLastRFCCount == 0 )
                            iNbrPts = 0;

                        if( iNbrPts > 0 )
                        {
                            // Have some readings to plot. The readings actually are coming
                            // from the previous second, so plot from the previous second
                            // up to the current second
                            double dBaseTime  = dtRFC;
                            double dPtsToPlot = (double)iNbrPts;

                            for( int iPt = 0; iPt < iNbrPts; iPt++ )
                            {
                                 double dXVal = dBaseTime - (double)( iNbrPts - 1 - iPt  )/ dPtsToPlot;

                                // The detector value is actually a signed short
                                XFER_BUFFER xferBuff;
                                xferBuff.wData[0] = loggedData.plugDetReadings[iPt];

                                if( loggedData.plugDetReadings[iPt] & 0x8000 )
                                    xferBuff.wData[1] = 0xFFFF;
                                else
                                    xferBuff.wData[1] = 0;

                                PlugDetSeries->AddXY( dXVal, xferBuff.iData );
                            }
                        }
                    }
                    else
                    {
                        // Have bogus timestamp. Consider alerting the user.
                        // TODO - post alert to main form
                        OutputDebugString( L"Bad tRFC" );
                    }

                }

                m_iLastRFCCount++;
            }

            // Add new events
            while( m_iLastEventCount < m_currJob->NbrSysEvents )
            {
                SYSTEM_EVENT sysEvent;

                if( m_currJob->GetSystemEvent( m_iLastEventCount, sysEvent ) )
                {
                    // System events are plotted against the plug detector axis (custom 1)
                    // and always just above the horizontal axis
                    EventSeries->AddXY( sysEvent.entryTime, DataChart->CustomAxes->Items[1]->Minimum * 0.97 );
                }

                m_iLastEventCount++;
            }
        }
    }
    catch( ... )
    {
    }

    // Allow updating again
    if( bReloadData )
    {
        for( int iSeries = 0; iSeries < DataChart->SeriesCount(); iSeries++ )
            DataChart->SeriesList->Items[iSeries]->EndUpdate();
    }

    // Update the axis range, but only if the user isn't scrolling
    if( !m_bScrolling && ( DataChart->AllowPanning == Vcltee::Teeprocs::pmNone ) )
    {
        // Capture first if we were showing the last RFC
        bool bWasShowingLastRFC = ShowingLastRFC();

        SetScrollBarParams( bWasShowingLastRFC );

        // If we are viewing the entire job, then fake out a ViewWidth click
        // to so that we keep the entire job in view
        if( m_viewWidthCombo->ViewWidth <= 0 )
            DoOnViewWidthChange( NULL );
    }
}


void TGraphForm::GetGraphState( GRAPH_STATE& graphState )
{
    graphState.tStart     = GetChartLeftTime();
    graphState.tViewWidth = m_viewWidthCombo->ViewWidth;

    graphState.formWidth  = Width;
    graphState.formHeight = Height;

    graphState.bRPMSeriesVisible           = RPMSeries->Visible;
    graphState.bTempSeriesVisible          = TempSeries->Visible;
    graphState.bMastTankPressSeriesVisible = MastTankPressSeries->Visible;
    graphState.bMastRegPressSeriesVisible  = MastRegPressSeries->Visible;
    graphState.bSlvRegPressSeriesVisible   = SlvRegPressSeries->Visible;
    graphState.bSlvTankPressSeriesVisible  = SlvTankPressSeries->Visible;
    graphState.bMastBattVoltsSeriesVisible = MastBattVoltsSeries->Visible;
    graphState.bSlvBattVoltsSeriesVisible  = SlvBattVoltsSeries->Visible;
    graphState.bPlugDetSeriesVisible       = PlugDetSeries->Visible;
    graphState.bEventSeriesVisible         = EventSeries->Visible;
}


void TGraphForm::SetGraphState( const GRAPH_STATE& graphState )
{
    Width  = graphState.formWidth;
    Height = graphState.formHeight;

    RPMSeries->Visible           = graphState.bRPMSeriesVisible;
    TempSeries->Visible          = graphState.bTempSeriesVisible;
    MastTankPressSeries->Visible = graphState.bMastTankPressSeriesVisible;
    MastRegPressSeries->Visible  = graphState.bMastRegPressSeriesVisible;
    SlvRegPressSeries->Visible   = graphState.bSlvRegPressSeriesVisible;
    SlvTankPressSeries->Visible  = graphState.bSlvTankPressSeriesVisible;
    MastBattVoltsSeries->Visible = graphState.bMastBattVoltsSeriesVisible;
    SlvBattVoltsSeries->Visible  = graphState.bSlvBattVoltsSeriesVisible;
    PlugDetSeries->Visible       = graphState.bPlugDetSeriesVisible;
    EventSeries->Visible         = graphState.bEventSeriesVisible;

    // If the requested view width is different from the current one, then
    // change the view width
    if( graphState.tViewWidth <= 0 )
    {
        if( m_viewWidthCombo->ViewWidth != TViewWidthCombo::ViewEnterJobValue )
        {
            // This will also cause a DoOnViewWidthChange event to occur
            m_viewWidthCombo->ViewWidth = TViewWidthCombo::ViewEnterJobValue;
        }
    }
    else if( m_viewWidthCombo->ViewWidth != graphState.tViewWidth )
    {
        // This will also cause a DoOnViewWidthChange event to occur
        m_viewWidthCombo->ViewWidth = graphState.tViewWidth;
    }

    // Finally, if we're not viewing the entire job, scroll to the start time
    // requested by the caller.
    if( m_viewWidthCombo->ViewWidth != TViewWidthCombo::ViewEnterJobValue )
    {
        int iNewPos = graphState.tStart - m_tFirstRFC + ScrollBarMin;

        ChartScrollBarScroll( ChartScrollBar, scEndScroll, iNewPos );

        ChartScrollBar->Position = iNewPos;
    }

//    DataChart->Repaint();
}


bool TGraphForm::CopyGraphToClipboard( void )
{
    bool bCopiedOk = false;

    DataChart->BufferedDisplay = false;

    try
    {
        DataChart->CopyToClipboardBitmap();
        bCopiedOk = true;
    }
    catch( ... )
    {
    }

    DataChart->BufferedDisplay = true;

    return bCopiedOk;
}


void TGraphForm::AddSeriesXY( TLineSeries* pSeries, double tWhen, float fValue )
{
    // Adds an XY point to the series. If fValue is NaN adds the point
    // as a NULL value
    if( IsNan( fValue ) )
        pSeries->AddNullXY( tWhen, 0 );
    else
        pSeries->AddXY( tWhen, fValue );
}


void __fastcall TGraphForm::OnDrawBottomAxisLabel( TChartAxis* Sender, int &X, int &Y, int &Z, String &Text, bool &DrawLabel )
{
    if( Sender != DataChart->Axes->Bottom )
        return;

    int iTime = Text.ToIntDef( -1 );

    if( iTime > 0 )
    {
        time_t tPt = (time_t)iTime;

        struct tm* pLocal = localtime( &tPt );

        if( pLocal != NULL )
            Text.printf( L"%02d:%02d:%02d", pLocal->tm_hour, pLocal->tm_min, pLocal->tm_sec );
        else
            Text = UIntToStr( (UINT)tPt );

        DrawLabel = true;
    }
    else
    {
        Text = "";
        DrawLabel = false;
    }
}


void __fastcall TGraphForm::DataChartGetNextAxisLabel(TChartAxis *Sender, int LabelIndex, double &LabelValue, bool &Stop)
{
    if( Sender != DataChart->Axes->Bottom )
        return;

    // Make labels a modulus of the display width
    time_t tLeft  = Ceil( DataChart->BottomAxis->Minimum );
    time_t tRight = Ceil( DataChart->BottomAxis->Maximum );

    // Put labels at evenly spaced intervals
    time_t tWidth   = GetViewWidth();
    time_t tModulus = GetTimeModulus( tWidth );

    // Round the start time to a multiple of the modulus
    time_t tStart = ( tLeft / tModulus ) * tModulus;

    if( tStart < tLeft )
        tStart += tModulus;

    // Now get the label time
    time_t tLabel = tStart + LabelIndex * tModulus;

    LabelValue = tLabel;

    Stop = ( tLabel >= tRight ) ? true : false;
}


time_t TGraphForm::GetChartLeftTime( void )
{
    if( IsNan( DataChart->Axes->Bottom->Minimum ) )
        return 0;

    time_t tLeft = (int)( DataChart->Axes->Bottom->Minimum + 0.1 );

    return tLeft;
}


time_t TGraphForm::GetChartRightTime( void )
{
    if( IsNan( DataChart->Axes->Bottom->Minimum ) )
        return 0;

    time_t tRight = (int)( DataChart->Axes->Bottom->Maximum + 0.1 );

    return tRight;
}


time_t TGraphForm::GetTimeModulus( time_t tDisplayWidth, int* pSecsPerTic )
{
    int iModulus;
    int iSecsPerTic;

    if( tDisplayWidth <= 10 )
    {
        // Up to 10 seconds
        iModulus    = 1;
        iSecsPerTic = 1;
    }
    else if( tDisplayWidth <= 20 )
    {
        // Up to 20 seconds
        iModulus    = 2;
        iSecsPerTic = 1;
    }
    else if( tDisplayWidth <= 60 )
    {
        // Up to one minute
        iModulus    = 5;
        iSecsPerTic = 1;
    }
    else if( tDisplayWidth <= 2 * 60 )
    {
        // Up to two minutes, labels every 15 secs, tic every 5
        iModulus    = 15;
        iSecsPerTic = 5;
    }
    else if( tDisplayWidth <= 5 * 60 )
    {
        // Up to five minutes, labels every 30 secs, tic every 10
        iModulus    = 30;
        iSecsPerTic = 10;
    }
    else if( tDisplayWidth <= 15 * 60 )
    {
        // Up to 15 minutes, labels every min, tic every 15 secs
        iModulus    = 60;
        iSecsPerTic = 15;
    }
    else if( tDisplayWidth <= 30 * 60 )
    {
        // Up to 30 minutes, labels every 2 min, tic every 30 secs
        iModulus    = 2 * 60;
        iSecsPerTic = 30;
    }
    else if( tDisplayWidth <= 60 * 60 )
    {
        // Up to 60 minutes, labels every 5 min, tic every minute
        iModulus    = 5 * 60;
        iSecsPerTic = 60;
    }
    else if( tDisplayWidth <= 2 * 60 * 60 )
    {
        // Up to two hours, labels every 15 min, tic every 5 minutes
        iModulus    = 15 * 60;
        iSecsPerTic = 5 * 60;
    }
    else
    {
        // Anything over 2 hours, labels very 30 min, ticks every 10
        iModulus    = 30 * 60;
        iSecsPerTic = 10 * 60;
    }

    if( pSecsPerTic != NULL )
        *pSecsPerTic = iSecsPerTic;

    return iModulus;
}


void __fastcall TGraphForm::DataChartClickSeries(TCustomChart *Sender, TChartSeries *Series,
          int ValueIndex, TMouseButton Button, TShiftState Shift, int X, int Y)
{
    TLineSeries* pSelSeries = dynamic_cast<TLineSeries*>( Series );

    if( pSelSeries == NULL )
        return;

    for( int iSeries = 0; iSeries < DataChart->SeriesCount(); iSeries++ )
    {
        TLineSeries* pLineSeries = (TLineSeries*)( DataChart->Series[iSeries] );

        // Toggle the line width
        if( pLineSeries == pSelSeries )
        {
            if( pLineSeries->LinePen->Width == 1 )
                pLineSeries->LinePen->Width = 3;
            else
                pLineSeries->LinePen->Width = 1;
        }
    }

    for( int iLegendItem = 0; iLegendItem < DataChart->Legend->Items->Count; iLegendItem++ )
    {
        DataChart->Legend->Items->Custom = true;

        TLineSeries* pLineSeries = (TLineSeries*)( DataChart->Series[iLegendItem] );

        if( pLineSeries->LinePen->Width == 3 )
            DataChart->Legend->Items->Items[iLegendItem]->Font->Style = TFontStyles() << fsBold;
        else
            DataChart->Legend->Items->Items[iLegendItem]->Font->Style = TFontStyles();

        // After setting Custom to true, must restore the legend colors
        DataChart->Legend->Items->Items[iLegendItem]->Font->Color = DataChart->Series[iLegendItem]->Color;
    }
}


void __fastcall TGraphForm::EditChart1Click(TObject *Sender)
{
    ChartEditor->Execute();
}


void TGraphForm::DisableScrollBar( void )
{
    // Disables the scroll bar and sets its props so that Windows
    // doesn't show the thumb. Note: the VCL enables the scrollbar
    // after a call to SetParams(), so we need to disable it after
    // that call (not before).
    ChartScrollBar->PageSize = 0;
    ChartScrollBar->SetParams( ScrollBarMin, ScrollBarMin, 100 );
    ChartScrollBar->Enabled = false;
}


void TGraphForm::SetScrollBarParams( bool bStayAtMax )
{
    // Sets the scroll bar pos and max based on current view. If bStayAtMax
    // is true, forces the scroll bar to stay in at the max value. This
    // method does not change the page size
    if( ( m_currJob == NULL ) || ( m_iLastRFCCount < 2 ) )
    {
        DisableScrollBar();
        return;
    }

    // Make sure we have a time span to work with
    if( m_tFirstRFC >= m_tLastRFC )
    {
        DisableScrollBar();
        return;
    }

    // Get the desired view width
    int iViewWidth = GetViewWidth();

    // Don't change the scrollbar pos. It should be at a legal value
    // as m_tLastRFC can never go lower in value. Also remember that
    // m_tLastRFC and m_tFirstRFC are both values to be displayed
    // (eg, they represent an inclusive time range)
    if( m_tLastRFC - m_tFirstRFC >= iViewWidth )
    {
        int iMax = m_tLastRFC - m_tFirstRFC + 1 /*max represents an included value*/;

        if( bStayAtMax )
        {
            // Change the pos and max props at the same time in this case.
            // Note that pos can never be > Max - PageSize for the scrollbar
            // to work right for us
            ChartScrollBar->SetParams( iMax - iViewWidth + ScrollBarMin, ScrollBarMin, iMax );
        }
        else
        {
            // Just changing max
            ChartScrollBar->Max = iMax;
        }

        // Call SetScrollBarPageSize() to be sure our page size is now correct.
        SetScrollBarPageSize();
    }
    else
    {
        DisableScrollBar();
    }

    SyncGraphToScrollbar();

//    String sMsg = "SetScrollBarParms: min / max / pg size: " + IntToStr( ChartScrollBar->Min ) + " " + IntToStr( ChartScrollBar->Max ) + " " + IntToStr( ChartScrollBar->PageSize );
//    OutputDebugString( sMsg.c_str() );
}


void TGraphForm::SetScrollBarPageSize( void )
{
    // Get our view range and small step size first
    int iViewWidth = GetViewWidth();

    int iSecsPerTic;
    GetTimeModulus( iViewWidth, &iSecsPerTic );

    // Can set the small change and large change props at any time as
    // they don't cause a refresh or change of state in the scrollbar
    ChartScrollBar->SmallChange = iSecsPerTic;
    ChartScrollBar->LargeChange = iViewWidth;

    // If we don't have data to work with, just disable the scroll bar
    if( ( m_currJob == NULL ) || ( m_iLastRFCCount < 2 ) || ( m_tFirstRFC >= m_tLastRFC ) )
    {
        DisableScrollBar();
        return;
    }

    // If the page size is greater than the amount of data available,
    // disable the scrollbar. Remember that tFirst and tLast are
    // inclusive values (hence the + 1)
    if( m_tLastRFC - m_tFirstRFC + 1 <= iViewWidth )
    {
        DisableScrollBar();
        return;
    }

    // Enable the scroll bar and set page size
    ChartScrollBar->Enabled  = true;
    ChartScrollBar->PageSize = iViewWidth;

    // Make sure our scroll position is legit now
    if( ChartScrollBar->Position > ChartScrollBar->Max - iViewWidth + 1 )
    {
        // Pos not legit. Scroll to bottom
        int iNewPos = ChartScrollBar->Max - iViewWidth + 1;

        ChartScrollBarScroll( ChartScrollBar, scBottom, iNewPos );

        ChartScrollBar->Position = iNewPos;
    }

//    String sMsg = "SetScrollBarPageSize: min / max / pg size: " + IntToStr( ChartScrollBar->Min ) + " " + IntToStr( ChartScrollBar->Max ) + " " + IntToStr( ChartScrollBar->PageSize );
//    OutputDebugString( sMsg.c_str() );
}


void TGraphForm::SyncGraphToScrollbar( void )
{
    // Sets the graph bottom axis min/max values to match the scrollbar pos
    // and current view width. Remember that the scroll bar min is 1 so we
    // have to take that into account
    time_t tStart;

    if( ChartScrollBar->Enabled )
        tStart = m_tFirstRFC + ChartScrollBar->Position - ChartScrollBar->Min;
    else
        tStart = m_tFirstRFC;

    // Watch for the case where we don't have data yet
    if( tStart == 0 )
        tStart = time( NULL );

    time_t tEnd = tStart + GetViewWidth();

    DataChart->BottomAxis->Automatic = false;
    DataChart->BottomAxis->SetMinMax( tStart, tEnd );
}


bool TGraphForm::ShowingLastRFC( void )
{
    // Returns true if the last RFC in the job is visible (eg, we've
    // scrolled to the end of the data)

    // If the scroll bar is disabled, we have to be showing the last RFC
    if( !ChartScrollBar->Enabled )
        return true;

    // If the scroll bar pos is max'd out, then we have to be showing
    // the last RFC. Max is an inclusive value, hence the + 1.
    if( ChartScrollBar->Position >= ChartScrollBar->Max - ChartScrollBar->PageSize + 1 )
        return true;

    // Fall through means we're not showing the last RFC
    return false;
}


void TGraphForm::SetTimeAxisIntervals( void )
{
    // Set time increment based on view size.
    int iSecsPerTic;

    int iTimeIncrement = GetTimeModulus( GetViewWidth(), &iSecsPerTic );

    // Just in case, make sure our increment is +ve
    if( iTimeIncrement <= 0 )
    {
        iTimeIncrement = 1;
        iSecsPerTic    = 1;
    }

    // Set minor tick count. Note that iSecsPerTick will always be
    // a multiple of the time increment.
    int iMinorTicks = iTimeIncrement / iSecsPerTic - 1;

    if( iMinorTicks < 0 )
        iMinorTicks = 0;

    // Now update the chart
    DataChart->BottomAxis->Increment      = iTimeIncrement;
    DataChart->BottomAxis->MinorTickCount = iMinorTicks;
}


void __fastcall TGraphForm::DoOnViewWidthChange( TObject* Sender )
{
    SetScrollBarParams();
    SetTimeAxisIntervals();
}


time_t TGraphForm::GetViewWidth( void )
{
    time_t tViewWidth = m_viewWidthCombo->ViewWidth;

    if( tViewWidth <= 0 )
    {
        // Viewing entire job. Set view width to RFC range, but add one
        // because the view width includes both end points.
        tViewWidth = m_tLastRFC - m_tFirstRFC + 1;

        if( tViewWidth < MinEntireJobViewWidth )
            tViewWidth = MinEntireJobViewWidth;
    }

    return tViewWidth;
}


void __fastcall TGraphForm::DataChartMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
    SetChartCursor( Shift, X, Y );
}


void __fastcall TGraphForm::DataChartMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
    SetChartCursor( Shift, X, Y );

    // On mouse down, enable panning if the speed button is down and the scroll bar is enabled
    if( GraphPanBtn->Down && ChartScrollBar->Enabled )
    {
        if( Shift.Contains( ssLeft ) )
        {
            DataChart->AllowPanning = Vcltee::Teeprocs::pmHorizontal;
            DataChart->AllowZoom    = false;
        }
    }
}


void __fastcall TGraphForm::DataChartMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
    SetChartCursor( Shift, X, Y );

    // On mouse up, make sure we have panning disabled
    if( !Shift.Contains( ssLeft ) )
    {
        DataChart->AllowPanning = Vcltee::Teeprocs::pmNone;
        DataChart->AllowZoom    = false;
    }
}


void TGraphForm::SetChartCursor( TShiftState Shift, int X, int Y )
{
    // Implementation note: the chart ignores cursor changes in certain
    // circumstances. It might have to do with the chart capture mouse
    // input when dragging the mouse over the chart area. Regardless,
    // the chart ignores the change of cursor when the mouse button
    // is down. We'll leave the code in place though in case a future
    // release of TeeChart fixes the problem.

    // Check if we need to update the cursor
    TCursor newCursor = crDefault;

    // Change cursor if the button is down and the scroll bar will allow scrolling
    if( GraphPanBtn->Down && ChartScrollBar->Enabled )
    {
        // We're in pan mode. Pick our cursor based on where we are (must be
        // in chart rect) and if the mouse button is down.
        TRect  chartRect = DataChart->ChartRect;
        TPoint mousePt   = TPoint( X, Y );

        if( chartRect.Contains( mousePt ) )
        {
            if( Shift.Contains( ssLeft ) )
                newCursor = CC_HANDGRAB;
            else
                newCursor = CC_HANDFLAT;
        }
    }

    if( DataChart->Cursor != newCursor )
    {
        // The chart has a confusing way of using the OriginalCursor property.
        // Force it to be the same as the new cursor so it doesn't mess us up.
        DataChart->Cursor         = newCursor;
        DataChart->OriginalCursor = newCursor;
    }
}


void __fastcall TGraphForm::ChartScrollBarScroll(TObject *Sender, TScrollCode ScrollCode, int &ScrollPos)
{
    // User is scrolling using the scroll bar. For the page size to work
    // correctly for us, we have the scrollbar min set to 1 and the scroll
    // pos can never be > scrollbar max - page size.
    //
    // The VCL will have automatically given us a suggested new ScrollPos
    // based on the scroll code so we just need to validate here.

//    String sMsg = "ChartScrollBarScroll: " + IntToStr( (int)ScrollCode ) + ", " + IntToStr( (int)ScrollPos );
//    OutputDebugString( sMsg.c_str() );

    int iMaxScrollPos = ChartScrollBar->Max - ChartScrollBar->PageSize + 1;

    switch( ScrollCode )
    {
        case scLineUp:
        case scLineDown:
        case scPageUp:
        case scPageDown:
            // Nothing need be done here
            break;

        case scTop:
            ScrollPos = ScrollBarMin;
            break;

        case scBottom:
            ScrollPos = iMaxScrollPos;
            break;

        case scPosition:
        case scTrack:
            // These events occur as the scroll bar thumb is being dragged
            // and then finally released. Set scrolling flag in this case
            m_bScrolling = true;
            break;

        case scEndScroll:
            // Scroll operation complete
            m_bScrolling = false;
            break;
    }

    // Validate new pos
    if( ScrollPos > iMaxScrollPos )
        ScrollPos = iMaxScrollPos;
    else if( ScrollPos < ScrollBarMin )
        ScrollPos = ScrollBarMin;

    if( ChartScrollBar->Position != ScrollPos )
    {
        time_t tNewFirstTime = m_tFirstRFC + ScrollPos - ScrollBarMin;
        time_t tNewEndTime   = tNewFirstTime + GetViewWidth();

        DataChart->BottomAxis->SetMinMax( tNewFirstTime, tNewEndTime );
    }
}


void __fastcall TGraphForm::DataChartAllowScroll(TChartAxis *Sender, double &AMin, double &AMax, bool &AllowScroll)
{
    // First, cannot scroll if the scroll bar is disabled
    if( !ChartScrollBar->Enabled )
    {
        AllowScroll = false;
        return;
    }

    // All times must be positive (or at least they'd better be!)
    time_t tMin = (time_t)Floor( AMin );
    time_t tMax = (time_t)Floor( AMax );

    // The assumption here is that tMin < tMax
    if( tMin < m_tFirstRFC )
        AllowScroll = false;
    else if( tMax > m_tLastRFC )
        AllowScroll = false;
    else
        AllowScroll = true;

    if( AllowScroll )
    {
        // To scroll, need to fake out a scrollbar event(s). Note that by
        // setting AMin and AMax, we are setting the scroll of the graph
        if( tMin <= m_tFirstRFC )
        {
            AMin = (double)( m_tFirstRFC );
            AMax = (double)( m_tFirstRFC + GetViewWidth() );

            ChartScrollBar->Position = ChartScrollBar->Min;
        }
        else if( tMax >= m_tLastRFC )
        {
            int iMaxScrollPos = ChartScrollBar->Max - ChartScrollBar->PageSize + 1;

            ChartScrollBar->Position = iMaxScrollPos;

            AMin = (double)( iMaxScrollPos - ScrollBarMin + m_tFirstRFC );
            AMax = (double)( iMaxScrollPos - ScrollBarMin + m_tFirstRFC + GetViewWidth() );
        }
        else
        {
            // Manually set the position
            int iPos = ChartScrollBar->Min + tMin - m_tFirstRFC;

            ChartScrollBar->Position = iPos;

            // Note that we could change AMin and AMax to match the floor'd
            // tMin and tMax values, but doing that causes the scrolling to mess
            // up when a user is slowly panning left or right. So leave AMin and
            // AMax as they were passed in (eg, potentially non-integer values).
            // Eventually another event will occur that will cause the graph
            // bottom axis min and max values to snap to an integer value.
        }

//        String sMsg = "AllowScroll: Pos " + IntToStr( ChartScrollBar->Position ) + ", min/max " + FloatToStrF( AMin, ffFixed, 15, 2 ) + ", " + FloatToStrF( AMax, ffFixed, 15, 2 );
//        OutputDebugString( sMsg.c_str() );
    }
}


void __fastcall TGraphForm::ChartMarksToolGetText(TMarksTipTool *Sender, UnicodeString &Text)
{
    // We want to format the tooltip text for events and battery voltage. The
    // events series format includes the text "Event" in the string. Battery
    // series includes "VBat". Look for those here.
    bool bIsVBatHint  = ( Text.Pos( "VBat"  ) > 0 );
    bool bIsEventHint = ( Text.Pos( "Event" ) > 0 );

    if( bIsEventHint )
    {
         // For now clear the text
         Text = "";

         // Find the nearest event point
         TPoint ptMouse = DataChart->GetCursorPos();

         int iIndex = ChartNearestPtTool->GetNearestPoint( EventSeries, ptMouse.X, ptMouse.Y, 10 );

         if( ( iIndex >= 0 ) && ( iIndex < EventSeries->Count() ) )
         {
             time_t tEvent = (time_t)( EventSeries->XValues->Items[iIndex] + 0.01 );

             // Do a brute force linear search. There aren't a lot of event recs,
             // so this should be fast
             for( int iEvent = 0; iEvent < m_currJob->NbrSysEvents; iEvent++ )
             {
                 SYSTEM_EVENT sysEvent;

                 if( m_currJob->GetSystemEvent( iEvent, sysEvent ) )
                 {
                     if( sysEvent.entryTime == tEvent )
                     {
                         Text = sysEvent.shortText;
                         break;
                     }
                 }
             }
         }
    }
    else if( bIsVBatHint )
    {
         // Find the nearest event point. Determine what series we're looking for
         TChartSeries* pBattSeries;

         if( Text.Pos( "Master" ) > 0 )
             pBattSeries = MastBattVoltsSeries;
         else
             pBattSeries = SlvBattVoltsSeries;

         // Clear the text
         Text = "";

         // Now find the nearest point
         TPoint ptMouse = DataChart->GetCursorPos();

         int iIndex = ChartNearestPtTool->GetNearestPoint( pBattSeries, ptMouse.X, ptMouse.Y, 10 );

         if( ( iIndex >= 0 ) && ( iIndex < pBattSeries->Count() ) && !pBattSeries->IsNull( iIndex ) )
         {
             double dVolts = pBattSeries->YValues->Items[iIndex];

             if( !IsNan( dVolts ) )
             {
                 // Voltage is actually plotted 10x actual value so that it can
                 // use the RPM vertical scale. So divide by 10 to display the
                 // actual voltage
                 Text = FloatToStrF( dVolts / 10.0, ffFixed, 7, 1 ) + " Volts";
             }
         }
    }
}


void __fastcall TGraphForm::SeriesMouseEnter(TObject *Sender)
{
    // Capture the series that the mouse has just moved over.
    // May need this in the future if something messes up
    // with the hint tooltip in ChartMarksToolGetText().
    m_activeSeries = dynamic_cast<TCustomSeries*>( Sender );
}


void __fastcall TGraphForm::SeriesMouseLeave(TObject *Sender)
{
    m_activeSeries = NULL;
}

