object GraphForm: TGraphForm
  Left = 0
  Top = 0
  Caption = 'GraphForm'
  ClientHeight = 448
  ClientWidth = 678
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object DataChart: TChart
    Left = 0
    Top = 0
    Width = 678
    Height = 431
    BackWall.Brush.Gradient.Direction = gdBottomTop
    BackWall.Brush.Gradient.EndColor = clWhite
    BackWall.Brush.Gradient.StartColor = 15395562
    BackWall.Brush.Gradient.Visible = True
    BackWall.Transparent = False
    Foot.Font.Color = clBlue
    Foot.Font.Name = 'Verdana'
    Gradient.Direction = gdBottomTop
    Gradient.EndColor = clWhite
    Gradient.MidColor = 15395562
    Gradient.StartColor = 15395562
    Gradient.Visible = True
    LeftWall.Color = 14745599
    Legend.Alignment = laBottom
    Legend.CheckBoxes = True
    Legend.Font.Name = 'Verdana'
    Legend.FontSeriesColor = True
    Legend.HorizMargin = 11
    Legend.Shadow.Transparency = 0
    Legend.Shadow.Visible = False
    Legend.Symbol.Visible = False
    Legend.TopPos = 1
    Legend.VertMargin = 1
    MarginBottom = 10
    MarginLeft = 10
    MarginRight = 20
    MarginTop = 12
    MarginUnits = muPixels
    RightWall.Color = 14745599
    ScrollMouseButton = mbLeft
    Title.Font.Name = 'Verdana'
    Title.Text.Strings = (
      'TChart')
    Title.Visible = False
    OnAllowScroll = DataChartAllowScroll
    OnClickSeries = DataChartClickSeries
    BottomAxis.Axis.Color = 4210752
    BottomAxis.AxisValuesFormat = '#'
    BottomAxis.Grid.Color = 11119017
    BottomAxis.LabelsAngle = 45
    BottomAxis.LabelsFormat.Font.Name = 'Verdana'
    BottomAxis.LabelsSeparation = 0
    BottomAxis.LabelStyle = talValue
    BottomAxis.TicksInner.Color = 11119017
    BottomAxis.Title.Font.Name = 'Verdana'
    DepthAxis.Axis.Color = 4210752
    DepthAxis.Grid.Color = 11119017
    DepthAxis.LabelsFormat.Font.Name = 'Verdana'
    DepthAxis.TicksInner.Color = 11119017
    DepthAxis.Title.Font.Name = 'Verdana'
    DepthTopAxis.Axis.Color = 4210752
    DepthTopAxis.Grid.Color = 11119017
    DepthTopAxis.LabelsFormat.Font.Name = 'Verdana'
    DepthTopAxis.TicksInner.Color = 11119017
    DepthTopAxis.Title.Font.Name = 'Verdana'
    CustomAxes = <
      item
        Automatic = False
        AutomaticMaximum = False
        AutomaticMinimum = False
        Axis.Visible = False
        Increment = 10.000000000000000000
        Horizontal = False
        OtherSide = False
        Maximum = 60.000000000000000000
        Minimum = -40.000000000000000000
        PositionPercent = 100.000000000000000000
        Title.Caption = 'Temperature'
        Title.Visible = False
        Title.Shadow.Visible = False
        Items = {
          0B000000010556616C75650500000000000000F0044000010454657874060235
          300556616C75650500000000000000C804400001045465787406023430055661
          6C75650500000000000000A0044000010454657874060233300556616C756505
          00000000000000F0034000010454657874060232300556616C75650500000000
          000000A0034000010454657874060231300556616C75650500000000000000A0
          0240000104546578740601300001045465787406032D31300556616C75650500
          000000000000A002C00001045465787406032D32300556616C75650500000000
          000000A003C00001045465787406032D33300556616C75650500000000000000
          F003C000010556616C75650500000000000000A004C000}
      end
      item
        Automatic = False
        AutomaticMaximum = False
        AutomaticMinimum = False
        Increment = 2000.000000000000000000
        Horizontal = False
        OtherSide = False
        Maximum = 32000.000000000000000000
        Minimum = -32000.000000000000000000
        Title.Caption = 'Plug Detector'
        Visible = False
      end>
    Hover.Visible = False
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Axis.Color = 4210752
    LeftAxis.Grid.Color = 11119017
    LeftAxis.Grid.Style = psDash
    LeftAxis.Increment = 500.000000000000000000
    LeftAxis.LabelsFormat.Font.Name = 'Verdana'
    LeftAxis.LabelsFormat.Margins.Top = 4
    LeftAxis.Maximum = 3000.000000000000000000
    LeftAxis.TicksInner.Color = 11119017
    LeftAxis.Title.Caption = 'Pressure and Temperature'
    LeftAxis.Title.Font.Name = 'Verdana'
    LeftAxis.Title.Shadow.Visible = False
    Panning.MouseWheel = pmwNone
    RightAxis.Automatic = False
    RightAxis.AutomaticMaximum = False
    RightAxis.AutomaticMinimum = False
    RightAxis.Axis.Color = 4210752
    RightAxis.Grid.Color = 11119017
    RightAxis.Increment = 5.000000000000000000
    RightAxis.LabelsFormat.Font.Name = 'Verdana'
    RightAxis.Maximum = 50.000000000000000000
    RightAxis.TicksInner.Color = 11119017
    RightAxis.Title.Angle = 90
    RightAxis.Title.Caption = 'RPM and Volts (x10)'
    RightAxis.Title.Font.Name = 'Verdana'
    TopAxis.Axis.Color = 4210752
    TopAxis.Grid.Color = 11119017
    TopAxis.LabelsFormat.Font.Name = 'Verdana'
    TopAxis.TicksInner.Color = 11119017
    TopAxis.Title.Font.Name = 'Verdana'
    View3D = False
    Zoom.Allow = False
    Zoom.Direction = tzdHorizontal
    Zoom.History = True
    OnGetNextAxisLabel = DataChartGetNextAxisLabel
    Align = alClient
    TabOrder = 0
    OnMouseDown = DataChartMouseDown
    OnMouseMove = DataChartMouseMove
    OnMouseUp = DataChartMouseUp
    DesignSize = (
      678
      431)
    DefaultCanvas = 'TGDIPlusCanvas'
    ColorPaletteIndex = 13
    object ControlPanel: TPanel
      Left = 448
      Top = 18
      Width = 161
      Height = 41
      Anchors = [akTop, akRight]
      BevelInner = bvLowered
      BevelKind = bkTile
      BevelWidth = 2
      ShowCaption = False
      TabOrder = 0
      object GraphPanBtn: TSpeedButton
        Left = 98
        Top = 6
        Width = 26
        Height = 26
        Hint = 'Enable Panning'
        AllowAllUp = True
        GroupIndex = 3
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
          3333333337773F333333333330FFF03333333333733373F3333333330FFFFF03
          33333337F33337F3333333330FFFFF0333333337F33337F3333333330FFFFF03
          33333337FFFFF7F3333333330777770333333337777777F3333333330FF7FF03
          33333337F37F37F3333333330FF7FF03333333373F7FF7333333333330000033
          33333333777773FFF33333333330330007333333337F37777F33333333303033
          307333333373733377F33333333303333303333333F7F33337F3333333330733
          330333333F777F3337F333333370307330733333F77377FF7733333337033300
          0733333F77333777733333337033333333333337733333333333}
        Margin = 1
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
      end
      object GraphSettingsBtn: TSpeedButton
        Left = 126
        Top = 6
        Width = 26
        Height = 26
        Hint = 'Graph Settings'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555550FF0559
          1950555FF75F7557F7F757000FF055591903557775F75557F77570FFFF055559
          1933575FF57F5557F7FF0F00FF05555919337F775F7F5557F7F700550F055559
          193577557F7F55F7577F07550F0555999995755575755F7FFF7F5570F0755011
          11155557F755F777777555000755033305555577755F75F77F55555555503335
          0555555FF5F75F757F5555005503335505555577FF75F7557F55505050333555
          05555757F75F75557F5505000333555505557F777FF755557F55000000355557
          07557777777F55557F5555000005555707555577777FF5557F55553000075557
          0755557F7777FFF5755555335000005555555577577777555555}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = EditChart1Click
      end
    end
    object RPMSeries: TLineSeries
      Selected.Hover.Visible = False
      SeriesColor = 16711808
      Title = 'RPM'
      ValueFormat = '0.0 RPM'
      VertAxis = aRightAxis
      OnMouseEnter = SeriesMouseEnter
      OnMouseLeave = SeriesMouseLeave
      Brush.BackColor = clDefault
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object TempSeries: TLineSeries
      SeriesColor = clFuchsia
      Title = 'Temperature'
      ValueFormat = '#0.0 C'
      VertAxis = aCustomVertAxis
      OnMouseEnter = SeriesMouseEnter
      OnMouseLeave = SeriesMouseLeave
      Brush.BackColor = clDefault
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      CustomVertAxis = 0
    end
    object MastBattVoltsSeries: TLineSeries
      SeriesColor = 5283584
      Title = 'Master Battery Voltage'
      ValueFormat = '0.000 VBat Master'
      VertAxis = aRightAxis
      OnMouseEnter = SeriesMouseEnter
      OnMouseLeave = SeriesMouseLeave
      Brush.BackColor = clDefault
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object MastTankPressSeries: TLineSeries
      SeriesColor = 374763
      Title = 'Master Tank Pressure'
      ValueFormat = '#,##0 PSI'
      OnMouseEnter = SeriesMouseEnter
      OnMouseLeave = SeriesMouseLeave
      Brush.BackColor = clDefault
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object MastRegPressSeries: TLineSeries
      SeriesColor = 20724
      Title = 'Master Reg Pressure'
      ValueFormat = '#,##0 PSI'
      OnMouseEnter = SeriesMouseEnter
      OnMouseLeave = SeriesMouseLeave
      Brush.BackColor = clDefault
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object SlvBattVoltsSeries: TLineSeries
      SeriesColor = 170
      Title = 'Slave Battery Voltage'
      ValueFormat = '0.000 V VBat Slave'
      VertAxis = aRightAxis
      OnMouseEnter = SeriesMouseEnter
      OnMouseLeave = SeriesMouseLeave
      Brush.BackColor = clDefault
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object SlvTankPressSeries: TLineSeries
      SeriesColor = 14793595
      Title = 'Slave Tank Pressure'
      ValueFormat = '#,##0 PSI'
      OnMouseEnter = SeriesMouseEnter
      OnMouseLeave = SeriesMouseLeave
      Brush.BackColor = clDefault
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object SlvRegPressSeries: TLineSeries
      PercentFormat = '#,###'
      SeriesColor = 9196350
      Title = 'Slave Reg Pressure'
      ValueFormat = '#,##0 PSI'
      OnMouseEnter = SeriesMouseEnter
      OnMouseLeave = SeriesMouseLeave
      Brush.BackColor = clDefault
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object PlugDetSeries: TLineSeries
      SeriesColor = clBlack
      Title = 'Plug Detector'
      ValueFormat = '0'
      VertAxis = aCustomVertAxis
      OnMouseEnter = SeriesMouseEnter
      OnMouseLeave = SeriesMouseLeave
      Brush.BackColor = clDefault
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      CustomVertAxis = 1
    end
    object EventSeries: TPointSeries
      Legend.Visible = False
      ShowInLegend = False
      Title = 'Events'
      ValueFormat = 'Event'
      VertAxis = aCustomVertAxis
      OnMouseEnter = SeriesMouseEnter
      OnMouseLeave = SeriesMouseLeave
      ClickableLine = False
      Pointer.Brush.Color = 16744448
      Pointer.InflateMargins = True
      Pointer.Style = psTriangle
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
      CustomVertAxis = 1
    end
    object ChartMarksTool: TMarksTipTool
      ShowInEditor = False
      Format.CustomPosition = True
      Format.Left = 0
      Format.TextAlignment = taCenter
      Format.Top = 0
      Format.Visible = False
      MouseDelay = 0
      SystemHints = False
      OnGetText = ChartMarksToolGetText
    end
    object ChartNearestPtTool: TNearestTool
      ShowInEditor = False
      Brush.Style = bsClear
      DrawLine = False
      FullRepaint = False
      LinePen.Visible = False
      Pen.Color = clWhite
      Pen.Style = psDot
      Series = EventSeries
      Style = hsNone
    end
  end
  object ChartScrollBar: TScrollBar
    Left = 0
    Top = 431
    Width = 678
    Height = 17
    Align = alBottom
    LargeChange = 3000
    PageSize = 0
    Position = 100
    SmallChange = 300
    TabOrder = 1
    OnScroll = ChartScrollBarScroll
  end
  object ChartEditor: TChartEditor
    Chart = DataChart
    GalleryHeight = 0
    GalleryWidth = 0
    Height = 0
    Width = 0
    Left = 616
    Top = 376
  end
end
