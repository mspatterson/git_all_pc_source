//---------------------------------------------------------------------------
#ifndef TGraphFormH
#define TGraphFormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Buttons.hpp>
#include <VCLTee.Chart.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.Series.hpp>
#include "VCLTee.TeeScroB.hpp"
#include "VCLTee.TeeEdit.hpp"
#include "VCLTee.TeeTools.hpp"
#include "TBasePlugJob.h"
#include "TViewWidthCombo.h"
//---------------------------------------------------------------------------
class TGraphForm : public TForm
{
__published:	// IDE-managed Components
    TChart *DataChart;
    TLineSeries *RPMSeries;
    TLineSeries *MastTankPressSeries;
    TLineSeries *MastRegPressSeries;
    TLineSeries *MastBattVoltsSeries;
    TLineSeries *TempSeries;
    TLineSeries *SlvRegPressSeries;
    TLineSeries *SlvBattVoltsSeries;
    TLineSeries *SlvTankPressSeries;
    TLineSeries *PlugDetSeries;
    TChartEditor *ChartEditor;
    TPanel *ControlPanel;
    TSpeedButton *GraphPanBtn;
    TSpeedButton *GraphSettingsBtn;
    TScrollBar *ChartScrollBar;
    TPointSeries *EventSeries;
    TMarksTipTool *ChartMarksTool;
    TNearestTool *ChartNearestPtTool;
    void __fastcall DataChartGetNextAxisLabel(TChartAxis *Sender, int LabelIndex, double &LabelValue, bool &Stop);
    void __fastcall DataChartClickSeries(TCustomChart *Sender, TChartSeries *Series, int ValueIndex, TMouseButton Button, TShiftState Shift, int X, int Y);
    void __fastcall EditChart1Click(TObject *Sender);
    void __fastcall DataChartMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
    void __fastcall DataChartMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
    void __fastcall DataChartMouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y);
    void __fastcall ChartScrollBarScroll(TObject *Sender, TScrollCode ScrollCode, int &ScrollPos);
    void __fastcall DataChartAllowScroll(TChartAxis *Sender, double &AMin, double &AMax, bool &AllowScroll);
    void __fastcall ChartMarksToolGetText(TMarksTipTool *Sender, UnicodeString &Text);
    void __fastcall SeriesMouseEnter(TObject *Sender);
    void __fastcall SeriesMouseLeave(TObject *Sender);

private:
    TBasePlugJob* m_currJob;

    int    m_iLastRFCCount;    // Count of RFC recs in job at time of last refresh
    time_t m_tFirstRFC;        // Timestamp of first RFC
    time_t m_tLastRFC;         // Timestamp of last RFC
    int    m_iLastEventCount;  // Count of events plotted since last refresh
    bool   m_bScrolling;       // True when the user is scrolling with the scroll bar
    bool   m_bUpdateSuspended; // Used when drawing report graphs to prevent new RFCs from affecting view

    TViewWidthCombo* m_viewWidthCombo;

    TCustomSeries*   m_activeSeries;   // Series that the mouse is currently over

    void __fastcall DoOnViewWidthChange( TObject* Sender );
        // Called with the view width tool changes

    time_t GetViewWidth( void );
        // Returns the number of seconds we should be showing, based
        // on the view width combo selection and data available

    bool ShowingLastRFC( void );
        // Returns true if the last RFC in the job is visible (eg, we've
        // scrolled to the end of the data)

    void RefreshGraph( bool bReloadData );
        // Internal method that redraws the graph based on current visible curves
        // and zoom settings. Set bReloadData to true to clear the data from all
        // series and reload it from the job

    void AddSeriesXY( TLineSeries* pSeries, double tWhen, float fValue );
        // Adds an XY point to the series. If fValue is NaN adds the point
        // as a NULL value

    void __fastcall OnDrawBottomAxisLabel( TChartAxis* Sender, int &X, int &Y, int &Z, String &Text, bool &DrawLabel );

    time_t GetChartLeftTime( void );
    time_t GetChartRightTime( void );
        // Helper functions to get first and last displayed times

    time_t GetTimeModulus( time_t tDisplayWidth, int* pSecsPerTic = NULL );
        // Determines major and minor tick spacing. Minor tick spacing only
        // returned if param not NULL. Major tick count will always be a
        // multiple of minor tick count.

    void SetTimeAxisIntervals( void );
        // Sets the properties of the time axis based on the current view width.

    void DisableScrollBar( void );
        // Disables the scroll bar

    void SetScrollBarParams( bool bStayAtMax = false );
        // Sets the scroll bar pos and max based on current view. If bStayAtMax
        // is true, forces the scroll bar to stay in at the max value. This
        // method does not change the page size

    void SetScrollBarPageSize( void );
        // Sets the scroll page size and potentially changes pos to keep
        // the displayed range of data correct

    void SyncGraphToScrollbar( void );
        // Sets the graph bottom axis min/max values to match
        // the scrollbar pos and page size

    void SetChartCursor( TShiftState Shift, int X, int Y );
        // Sets the chart cursor based on the current mouse state.

public:		// User declarations
    __fastcall TGraphForm(TComponent* Owner);

    void LoadNewJob( TBasePlugJob* newJob );
        // Call to load a new job to display. If newJob is NULL, no data is displayed

    void LoadNewData( void );
        // Called when new data has been logged. Graph will display the new data.

    typedef struct {
        time_t tStart;
        time_t tViewWidth;
        int    formWidth;
        int    formHeight;
        bool   bRPMSeriesVisible;
        bool   bTempSeriesVisible;
        bool   bMastTankPressSeriesVisible;
        bool   bMastRegPressSeriesVisible;
        bool   bSlvRegPressSeriesVisible;
        bool   bSlvTankPressSeriesVisible;
        bool   bMastBattVoltsSeriesVisible;
        bool   bSlvBattVoltsSeriesVisible;
        bool   bPlugDetSeriesVisible;
        bool   bEventSeriesVisible;
        // TODO - add param for selected series
    } GRAPH_STATE;

    void GetGraphState(       GRAPH_STATE& graphState );
    void SetGraphState( const GRAPH_STATE& graphState );

    void SuspendUpdates( void )  { m_bUpdateSuspended = true;  }
    void ResumeUpdates( void )   { m_bUpdateSuspended = false; }

    bool CopyGraphToClipboard( void );
};
//---------------------------------------------------------------------------
extern PACKAGE TGraphForm *GraphForm;
//---------------------------------------------------------------------------
#endif
