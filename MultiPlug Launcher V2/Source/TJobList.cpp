//
// This unit implements support routines for creating, enumerating,
// and updating job files.
//

#include <vcl.h>
#pragma hdrstop

#include <IOUtils.hpp>
#include <DateUtils.hpp>

#include "TJobList.h"
#include "TPlugJob.h"
#include "MPLRegistryInterface.h"

#pragma package(smart_init)


//
// TJobList Implementation
//

__fastcall TJobList::TJobList( const String& dataDir )
{
    // This form of the constructor enumerates all valid job
    // files in the passed folder and any subfolders.
    m_pJobList = new TList();

    TStringDynArray list;
    TSearchOption searchOption = TSearchOption::soAllDirectories;

    try
    {
        list = TDirectory::GetFiles( dataDir, L"*" + MPLSJobFileExt, searchOption );
    }
    catch( ... )
    {
    }

    for( int iItem = 0; iItem < list.Length; iItem++ )
    {
        // Only list valid jobs
        PLUG_JOB_INFO jobInfo;

        if( TPlugJob::GetJobInfo( list[iItem], jobInfo ) )
        {
             JOB_LIST_ITEM* pJobItem = new JOB_LIST_ITEM;

             // Assign the items to be stored in the struct. Force the
             // job file name string to be unique.
             pJobItem->jobInfo     = jobInfo;
             pJobItem->jobFileName = list[iItem];
             pJobItem->jobFileName.Unique();

             m_pJobList->Add( pJobItem );
        }
    }
}


__fastcall TJobList::TJobList( TStrings* fileNameList )
{
    // This form of the constructor enumerates the specific
    // list of job files passed.
    m_pJobList = new TList();

    if( fileNameList == NULL )
        return;

    for( int iItem = 0; iItem < fileNameList->Count; iItem++ )
    {
        // Only list valid jobs
        PLUG_JOB_INFO jobInfo;

        if( TPlugJob::GetJobInfo( fileNameList->Strings[iItem], jobInfo ) )
        {
             JOB_LIST_ITEM* pJobItem = new JOB_LIST_ITEM;

             // Assign the items to be stored in the struct. Force the
             // job file name string to be unique.
             pJobItem->jobInfo     = jobInfo;
             pJobItem->jobFileName = fileNameList->Strings[iItem];
             pJobItem->jobFileName.Unique();

             m_pJobList->Add( pJobItem );
        }
    }
}


__fastcall TJobList::~TJobList( void )
{
    while( m_pJobList->Count > 0 )
    {
        JOB_LIST_ITEM* pJobItem = (JOB_LIST_ITEM*)( m_pJobList->Items[0] );

        delete pJobItem;

        m_pJobList->Delete( 0 );
    }

    delete m_pJobList;
}


bool TJobList::GetItem( int index, JOB_LIST_ITEM& jobItem )
{
    if( ( index < 0 ) || ( index >= m_pJobList->Count ) )
        return false;

    JOB_LIST_ITEM* pJobItem = (JOB_LIST_ITEM*)( m_pJobList->Items[index] );

    jobItem = *pJobItem;

    return true;
}


String TJobList::CreateNewJob( const PLUG_JOB_INFO& pjiInfo, bool bIsTestJob )
{
    // Creates a job with the passed plug info. Jobs are always
    // created under the Job Data folder. Assume the folder is
    // valid for now. If we can't write to the folder, then we'll
    // report an error. This is a static method.
    String sMPLSFolder = ExcludeTrailingBackslash( GetJobDataDir() );

    // Make sure the Well Name can be used for a folder or file name.
    // Only allow characters and numbers in the name. Replace all
    // other chars with an underscore.
    String sWellName = Trim( pjiInfo.wellName );

    // Validate well name - it cant be blank
    if( sWellName.Length() == 0 )
        return "";

    for( int iChar = 1; iChar <= sWellName.Length(); iChar++ )
    {
        wchar_t wChar = sWellName[iChar];

        if( IsCharAlphaNumeric( wChar ) )
            continue;

        sWellName[iChar] = '_';
    }

    // Now create the components of the job file name
    String sJobFolder;
    String sJobName;

    if( bIsTestJob )
    {
        sJobFolder = sMPLSFolder + "\\Test_Jobs";

        if( !DirectoryExists( sJobFolder ) )
        {
            if( !CreateDir( sJobFolder ) )
            {
                MessageDlg( "The Test Jobs could not be created. Please check that you have read and write access to the MPLS Job folder.",
                            mtError, TMsgDlgButtons() << mbOK, 0 );
                return "";
            }
        }

        // Job name is just the well name and local date and time all concatenated together
        sJobName = sWellName + "_" + Now().FormatString( "yyyy_mm_dd_hh_nn_ss" );
    }
    else
    {
        // For regular jobs, need to append the date to the well name
        String sJobDate;
        sJobDate.printf( L"%04d_%02d_%02d", pjiInfo.stJobDate.wYear, pjiInfo.stJobDate.wMonth, pjiInfo.stJobDate.wDay );

        // Job folder depends on whether this is a test job or not
        sJobFolder = sMPLSFolder + "\\" + sWellName + "_" + sJobDate;

        if( !DirectoryExists( sJobFolder ) )
        {
            if( !CreateDir( sJobFolder ) )
            {
                MessageDlg( "A folder for this Job could not be created. Please check that you have read and write access to the MPLS Job folder.",
                            mtError, TMsgDlgButtons() << mbOK, 0 );
                return "";
            }
        }

        // The 32-bit version of time_t will wrap in 2038. Just in case this
        // software is still being used then, check for that. Replace the
        // minus sign with the digit 1 in that case.
        sJobName = IntToStr( (int)pjiInfo.jobCreateTime );

        if( sJobName[1] == '-' )
           sJobName[1] = '1';
    }

    // Now construct the full file name
    String sFullFileName;

    // Sequence the job name, just in case there is a duplicate
    int iSeq = 0;

    while( iSeq <= 9999 )
    {
        String sSeq;
        sSeq.printf( L"_%04d", iSeq );

        sFullFileName = sJobFolder + "\\" + sJobName + sSeq + MPLSJobFileExt;

        if( !FileExists( sFullFileName ) )
            break;

        iSeq++;
    }

    if( iSeq >= 10000 )
    {
        MessageDlg( "The Test Job could not be created. Please try again in a couple of seconds.",
                    mtError, TMsgDlgButtons() << mbOK, 0 );
        return "";
    }

    // Create the job
    if( !TBasePlugJob::CreateJobFile( pjiInfo, sFullFileName ) )
    {
        // Job creation failed
        sFullFileName = "";
    }

    return sFullFileName;
}


int TJobList::JobsExistFor( const String& dataDir, const String& wellName, const SYSTEMTIME& stJobDate )
{
    // Returns how many jobs exists for the passed well name and job date pair
    // in the dataDir. If none exist, return zero.  This is a static method.
    TStringDynArray list;
    TSearchOption searchOption = TSearchOption::soTopDirectoryOnly;

    try
    {
        list = TDirectory::GetFiles( dataDir, L"*" + MPLSJobFileExt, searchOption );
    }
    catch( ... )
    {
    }

    int nbrJobs = 0;

    for( int iItem = 0; iItem < list.Length; iItem++ )
    {
        // Only list valid jobs
        PLUG_JOB_INFO jobInfo;

        if( TPlugJob::GetJobInfo( list[iItem], jobInfo ) )
        {
            if( jobInfo.wellName.CompareIC( wellName ) == 0 )
            {
                // Well name matches - now check for a match on the year,
                // month, and day components of the SYSTEMTIME
                if( ( jobInfo.stJobDate.wYear  ) == ( stJobDate.wYear )
                      &&
                    ( jobInfo.stJobDate.wMonth ) == ( stJobDate.wMonth )
                      &&
                    ( jobInfo.stJobDate.wDay   ) == ( stJobDate.wDay )
                  )
                {
                    nbrJobs++;
                }
            }
        }
    }

    return nbrJobs;
}


