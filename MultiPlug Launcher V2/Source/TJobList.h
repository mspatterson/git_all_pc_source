#ifndef TJobListH
#define TJobListH

    //
    // This file implements support routines for creating, enumerating,
    // and updating plug job files.
    //

    #include <XMLIntf.hpp>

    #include "TBasePlugJob.h"

    // The JOB_LIST_ITEM is used by TJobList to describe each job in the list
    typedef struct {
        PLUG_JOB_INFO jobInfo;
        String        jobFileName;
    } JOB_LIST_ITEM;


    // TJobList manages a list of jobs. The object can be constructed by
    // passing a directory that contains a list of jobs, or passing a
    // list of file names of jobs.
    class TJobList : public TObject
    {
        private:

        protected:
            TList* m_pJobList;

            __fastcall int GetJobCount( void ) { return m_pJobList->Count; }

        public:

            virtual __fastcall TJobList( const String& dataDir );
            virtual __fastcall TJobList( TStrings* fileNameList );
                // Constructors for job lists. First form enumerates all jobs
                // in the dataDir and puts them in the list. Second form creates
                // a list of the jobs whose filename is passed in the string list.

            virtual __fastcall ~TJobList( void );

            __property int Count = { read = GetJobCount };

            bool GetItem( int index, JOB_LIST_ITEM& jobItem );

            static String CreateNewJob( const PLUG_JOB_INFO& pjiInfo, bool bIsTestJob );
                // Creates a job with the passed plug info. Returns the file
                // name for the job on success, an empty string on failure.

            static int JobsExistFor( const String& dataDir, const String& wellName, const SYSTEMTIME& stJobDate );
                // Returns how many jobs exists for the passed well name and job date pair
                // in the dataDir. If none exist, return zero.
    };


#endif
