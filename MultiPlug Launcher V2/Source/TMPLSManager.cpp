//******************************************************************************
//
//  TMPLSManager.cpp: This module provides a wrapper around the lower
//        level hardware classes, implements a thread to keep the comms
//        comms constantly updated, and implements the business rules
//        for the operation of a MPLS system
//
//******************************************************************************
#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>
#include <math.hpp>

#include "TMPLSManager.h"
#include "MPLDeviceNone.h"
#include "MPLDeviceWireless.h"
#include "CUDPPort.h"
#include "USBPortDiscovery.h"
#include "MPLRegistryInterface.h"
#include "UnitsOfMeasure.h"

#pragma package(smart_init)


#define PLUG_DETECTED_ALERT_SW   MPL_ALERT_OUTPUT_SWITCH_NBR

const String TMPLSManager::ConnResultStrings[TMPLSManager::eCR_NumConnectResult] =
{
    "Initializing",
    "Bad Base Radio port",
    "Bad MPLS port",
    "Bad Management port",
    "Connected",
    "Scanning for Radio",
    "Scanning for MPLS",
    "Winsock() initialization error",
    "Unhandled comms thread exception",
};


static bool GetMutex( HANDLE aMutex, DWORD maxWait )
{
    // Return true if the mutex is acquired
    if( aMutex == NULL )
        return false;

    switch( WaitForSingleObject( aMutex, maxWait ) )
    {
        case WAIT_ABANDONED:
        case WAIT_OBJECT_0:
            // Have access to the mutex now
            return true;

        case WAIT_FAILED:
            // Not a good thing! Call failed.
            break;

        case WAIT_TIMEOUT:
            // Timeout expired!
            break;
    }

    // Fall through means mutex not acquired
    return false;
}


//
// Poll Thread Class Implementation
//

__fastcall TMPLSManager::TMPLSManager( HWND hwndEvent, DWORD dwEventMsg ) : TThread( false )
{
    // Save the event handle for later use
    m_hwndEvent  = hwndEvent;
    m_dwEventMsg = dwEventMsg;

    // Init state vars. Always set m_connectResult first before
    // setting the poller state
    SetPollerState( PS_INITIALIZING, eCR_Initializing );
    PostEventToClient( eMME_Initializing );

    // Initialize device variables
    m_device = NULL;
    m_radio  = NULL;

    m_mplType = MT_NULL;

    m_dwRadioChanWaitTime = GetRadioChanWaitTimeout();

    GetWirelessSystemInfo( eWS_MPLS2, m_wirelessInfo );

    // Caller must delete this object on terminate
    FreeOnTerminate = false;

    // Init the thread-safe packet transfer. A critical section
    // is used for this.
    InitializeCriticalSection( &m_criticalSection );

    FlushPendingPktQueue();
}


__fastcall TMPLSManager::~TMPLSManager()
{
    Terminate();

    switch( WaitForSingleObject( (HANDLE)Handle, 1000 /*msecs*/ ) )
    {
        case WAIT_FAILED:
            // Not a good thing - don't try to delete the object.
            break;

        case WAIT_ABANDONED:
        case WAIT_OBJECT_0:
            // Safe to delete the object in both these cases
            break;

        case WAIT_TIMEOUT:
            // Timeout expired! Don't delete the object in this case
            break;
    }

    // Release the critical section
    DeleteCriticalSection( &m_criticalSection );
}


void __fastcall TMPLSManager::Execute()
{
    // Initialize and create objects
    // Winsock is required for UDP ports
    NameThreadForDebugging( AnsiString( "MPLThread" ) );

    if( !InitWinsock() )
    {
        SetPollerState( PS_COMMS_FAILED, eCR_WinsockError );
        return;
    }

    // Initialize alarm structures
    ResetAlarms();

    // Init general vars
    m_currUnits = m_newUnits = (UNITS_OF_MEASURE) GetDefaultUnitsOfMeasure();

    m_bAssertAlarmOut = GetAlarmOutOnBREnabled();

    // Set the Poller to scanning
    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );

    // Loop here now executing the thread
    try
    {
        while( !Terminated )
        {
            if( GetMPLPortLost() || ( PollerState == PS_BR_PORT_SCAN ) || ( PollerState == PS_MPL_PORT_SCAN ) )
            {
                // Check which Port Opening method to use. Read this from the
                // registry each time in case it changes
                if( GetAutoAssignCommPorts() )
                {
                    while( !OpenAutomaticPorts() )
                    {
                        if( Terminated )
                            break;

                        Sleep( 100 );
                    }
                }
                else
                {
                    while( !OpenAssignedPorts() )
                    {
                        if( Terminated )
                            break;

                        Sleep( 100 );
                    }
                }
            }

            if( Terminated )
                break;

            // If we are hunting for the MPL, loop here until it is found
            DWORD dwRFCWaitStart = GetTickCount();

            while( PollerState == PS_MPL_CHAN_SCAN )
            {
                m_device->Update();
                m_radio->Update();

                if( Terminated )
                    break;

                if( !m_radio->IsReceiving )
                {
                    // If the base radio is not receiving, we are in big trouble.
                    // Need to rescan for all devices
                    DeleteDevices();
                    PostEventToClient( eMME_CommsLost );
                    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
                }
                else if( m_device->DeviceIsRecving )
                {
                    SetPollerState( PS_RUNNING, eCR_Connected );
                    PostEventToClient( eMME_InitComplete );

                    break;
                }
                else if( HaveTimeout( dwRFCWaitStart, m_dwRadioChanWaitTime ) )
                {
                    // Time to switch channels - the base radio can figure out
                    // the next channel
                    m_radio->GoToNextRadioChannel( eBRN_2, m_wirelessInfo.chanList, MAX_CHANS_PER_SYSTEM );

                    dwRFCWaitStart = GetTickCount();
                }
            }

            if( Terminated )
                break;

            // Do are work if we are in the running state
            if( PollerState == PS_RUNNING )
            {
                m_device->Update();
                m_radio->Update();

                if( !m_radio->IsReceiving )
                {
                    // If the base radio is not receiving, we are in big trouble.
                    // Need to rescan for all devices
                    DeleteDevices();
                    PostEventToClient( eMME_CommsLost );
                    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
                }
                else if( !m_device->DeviceIsRecving )
                {
                    // Note we don't delete devices in this case - first try different channels
                    PostEventToClient( eMME_CommsLost );
                    SetPollerState( PS_MPL_CHAN_SCAN, eCR_HuntingForMPL );
                }
                else
                {
                    // Update state machine
                    // TODO - this might not be required anymore

                    // Check if a refresh of settings has been requested
                    if( m_refreshSettings )
                    {
                        // Refresh requested - get new values
                        m_currUnits = m_newUnits;

                        m_bAssertAlarmOut = GetAlarmOutOnBREnabled();

                        m_dwRadioChanWaitTime = GetRadioChanWaitTimeout();
                        m_dwPlugDetectTimeout = GetPlugDetectTimeout();

                        m_device->RefreshSettings();

                        m_refreshSettings = false;
                    }
                }
            }

            // Work done for this iteration
            Sleep( 5 );
        }
    }
    catch( ... )
    {
        SetPollerState( PS_EXCEPTION, eCR_ThreadException );
        PostEventToClient( eMME_ThreadFailed );
    }

    // Release all comm mgr resources
    DeleteDevices();

    ShutdownWinsock();
}


bool TMPLSManager::OpenAssignedPorts( void )
{
    while( true )
    {
        // Delete all devices safely
        DeleteDevices();

        // Create the devices
        CreateDevices();

        // Set the result to Scanning
        SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
        PostEventToClient( eMME_Initializing );

        COMMS_CFG mgmtCommCfg;
        COMMS_CFG baseCommCfg;
        COMMS_CFG deviceCommCfg;

        GetCommSettings( DCT_MGMT, mgmtCommCfg );
        GetCommSettings( DCT_BASE, baseCommCfg );
        GetCommSettings( DCT_MPL,  deviceCommCfg );

        // Attempt to connect our devices
        if( ( m_device->Connect( deviceCommCfg ) ) && ( m_radio->Connect( baseCommCfg ) ) && ( m_mgmtPort->Connect( mgmtCommCfg ) ) )
        {
            // Check if we have a Null-Radio
            if( m_radio->ClassNameIs( "TNullBaseRadio" ) )
            {
                SetPollerState( PS_MPL_CHAN_SCAN, eCR_HuntingForMPL );
                PostEventToClient( eMME_Initializing );

                return true;
            }

            // The port could be opened, wait here for 2 seconds for a
            // a message to appear from the base radio
            DWORD dwStartPktCount = m_radio->StatusPktCount;

            DWORD dwEndWait = GetTickCount() + 2500;

            while( GetTickCount() < dwEndWait )
            {
                m_radio->Update();

                if( m_radio->StatusPktCount > dwStartPktCount + 5 )
                {
                    // Found the base radio. Set the MPL radio port onto its
                    // default channel and then move on to the MPL scan.
                    m_radio->SetRadioChannel( eBRN_2, GetDefaultRadioChan() );

                    AfterCommConnect();

                    SetPollerState( PS_MPL_CHAN_SCAN, eCR_HuntingForMPL );
                    PostEventToClient( eMME_Initializing );

                    return true;
                }

                if( Terminated )
                    return false;

                Sleep( 50 );
            }

            m_radio->Disconnect();
        }

        // Could not connect to devices, sleep for one second, then try
        // again on the same ports. Break the sleep into 100ms intervals
        // so we can respond to a terminate request quickly.
        for( int iSleepIndex = 0; iSleepIndex < 10; iSleepIndex++ )
        {
            Sleep( 100 );

            if( Terminated )
                return false;
        }
    }
}


bool TMPLSManager::OpenAutomaticPorts( void )
{
    while( true )
    {
        // Delete all devices safely
        DeleteDevices();

        // Create the devices
        CreateDevices();

        // Set the result to Scanning
        SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
        PostEventToClient( eMME_Initializing );

        COMMS_CFG baseCommCfg;
        COMMS_CFG deviceCommCfg;
        COMMS_CFG mgmtCommCfg;

        GetCommSettings( DCT_BASE, baseCommCfg );
        GetCommSettings( DCT_MPL,  deviceCommCfg );
        GetCommSettings( DCT_MGMT, mgmtCommCfg );

        // For automatic ports, all port types must be serial
        baseCommCfg.portType   = CT_SERIAL;
        deviceCommCfg.portType = CT_SERIAL;
        mgmtCommCfg.portType   = CT_SERIAL;

        baseCommCfg.portParam   = 115200;
        deviceCommCfg.portParam = 115200;
        mgmtCommCfg.portParam   = 115200;

        // Init search loop
        int iPortNbr   = baseCommCfg.portName.ToIntDef( 1 );
        int iPortStart = iPortNbr;

        while( true )
        {
            // Set the Port Number for the radio
            baseCommCfg.portName = IntToStr( iPortNbr );

            // Attempt to connect our Base Radio
            if( m_radio->Connect( baseCommCfg ) )
            {
                // The port could be opened, wait here for 2 seconds for a
                // a message to appear from the base radio. The base radio
                // sends periodic status messages even if no command is
                // sent to it
                DWORD dwStartPktCount = m_radio->StatusPktCount;

                DWORD dwEndWait = GetTickCount() + 2500;

                while( GetTickCount() < dwEndWait )
                {
                    m_radio->Update();

                    if( m_radio->StatusPktCount > dwStartPktCount + 5 )
                    {
                        // Found the base radio. Set the MPL radio port onto its
                        // default channel now
                        m_radio->SetRadioChannel( eBRN_2, GetDefaultRadioChan() );

                        SetPollerState( PS_MPL_PORT_SCAN, eCR_HuntingForMPL );
                        PostEventToClient( eMME_Initializing );

                        // If we can't find the ports, return false
                        if( !FindDevPorts( baseCommCfg, deviceCommCfg, mgmtCommCfg ) )
                            return false;

                        // If the ports fail to connect, return false
                        if( ( !m_device->Connect( deviceCommCfg ) ) || ( !m_mgmtPort->Connect( mgmtCommCfg ) ) )
                            return false;

                        SaveCommSettings( DCT_BASE, baseCommCfg );
                        SaveCommSettings( DCT_MPL,  deviceCommCfg );
                        SaveCommSettings( DCT_MGMT, mgmtCommCfg );

                        AfterCommConnect();

                        SetPollerState( PS_MPL_CHAN_SCAN, eCR_HuntingForMPL );
                        PostEventToClient( eMME_Initializing );

                        return true;
                    }

                    if( Terminated )
                        return false;

                    Sleep( 50 );
                }

                m_radio->Disconnect();
            }

            if( Terminated )
                return false;

            // Autoscan enabled, try next port
            iPortNbr++;

            if( iPortStart == iPortNbr )
                break;

            if( iPortNbr > 999 )
                iPortNbr = 1;

            Sleep( 10 );
        }

        Sleep( 100 );
    }
}


bool TMPLSManager::FindDevPorts( const COMMS_CFG& baseCommCfg, COMMS_CFG& deviceCommCfg, COMMS_CFG& mgmtCommCfg )
{
    // Set up the CommPort and Connection Params
    CCommPort* pCommPort = new CCommPort();

    CCommPort::CONNECT_PARAMS CommPortParams;

    CommPortParams.dwLineBitRate = 9600;

    int iPortNum = 0;
    int iR0Port  = -1;
    int iR1Port  = -1;

    // Iterate through 1000 port numbers, attempt to connect to each
    while( iPortNum <= 999 )
    {
        CommPortParams.iPortNumber = iPortNum;

        // Check if we can connect to the port, if so, wiggle it's signals
        if( pCommPort->Connect( &CommPortParams ) == CCommObj::ERR_NONE )
        {
            // Get the Port Type of the current CommPort
            PORT_TYPE PortType = GetPortType( pCommPort );

            // Check if the Port Type returned was one we care about
            if( PortType == eRadio0 )
                iR0Port = iPortNum;
            else if( PortType == eRadio1 )
                iR1Port = iPortNum;
        }

        // Always disconnect to be safe
        pCommPort->Disconnect();

        // If we've found both ports in this iteration, try to find Mgmt
        if( ( iR0Port >= 0 ) && ( iR1Port >= 0 ) )
        {
            TList* pPorts = new TList();

            int iRadioPort = baseCommCfg.portName.ToIntDef( -1 );

            if( !GetAssociatedPorts( iRadioPort, pPorts ) )
            {
                delete pPorts;
                break;
            }

            pPorts->Remove( (TObject*)( iRadioPort ) );
            pPorts->Remove( (TObject*)( iR0Port    ) );
            pPorts->Remove( (TObject*)( iR1Port    ) );

            if( pPorts->Count != 1 )
            {
                delete pPorts;
                break;
            }

            // The MPL device uses radio port 1 (TT Mgr uses port 0)
            deviceCommCfg.portName = IntToStr( iR1Port );
            mgmtCommCfg.portName   = IntToStr( (int)( pPorts->List[0] ) );

            delete pPorts;
            delete pCommPort;

            return true;
        }

        // Be safe and check for termination
        if( Terminated )
            break;

        Sleep( 10 );

        iPortNum++;
    }

    // Clean-up and return
    delete pCommPort;

    return false;
}


TMPLSManager::PORT_TYPE TMPLSManager::GetPortType( CCommPort* pPort )
{
    // Init our Ctrl Status variables
    BYTE bInitCtrlStatus = 0;
    BYTE bHighCtrlStatus = 0;
    BYTE bLastCtrlStatus = 0;

    if( !WigglePort( pPort, bInitCtrlStatus, bHighCtrlStatus, bLastCtrlStatus ) )
        return eUnknown;

    // WigglePort() will raise RTS, drop it, and then raise it again. The base
    // radio reports the inverse state of the signal, so look in the control
    // signal status byte for a low-high-low sequence.
    if( !( bInitCtrlStatus & 0x01 ) && ( bHighCtrlStatus & 0x01 ) && !( bLastCtrlStatus & 0x01 ) )
        return eRadio0;
    else if( !( bInitCtrlStatus & 0x02 ) && ( bHighCtrlStatus & 0x02 ) && !( bLastCtrlStatus & 0x02 ) )
        return eRadio1;

    return eUnknown;
}


bool TMPLSManager::WigglePort( CCommPort* pPort, BYTE& bInitCtrlStatus, BYTE& bHighCtrlStatus, BYTE& bLastCtrlStatus )
{
    // Init our Base Radio Packet variables
    BASE_STATUS_STATUS_PKT RadioStatusPkt;
    time_t tLastPkt;

    // Set RTS bit to low
    pPort->SetRTS( true );

    DWORD dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the Initial CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bInitCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    // Set RTS bit to high
    pPort->SetRTS( false );

    dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the High CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bHighCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    // Set RTS bit to low
    pPort->SetRTS( true );

    dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the Last CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bLastCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    return true;
}


void TMPLSManager::AfterCommConnect( void )
{
    m_currUnits = GetDefaultUnitsOfMeasure();
    m_newUnits  = GetDefaultUnitsOfMeasure();

    m_refreshSettings  = true;
}


void TMPLSManager::CreateDevices( void )
{
    // If we are auto-assigning ports, then real objects are required.
    // Otherwise, construct the object type based on the registry setting.
    if( GetAutoAssignCommPorts() )
    {
        m_mgmtPort = new TRealMgmtPort();
        m_radio    = new TRealBaseRadio();
        m_device   = new TWirelessMPLDevice();
    }
    else
    {
        // Create devices based on the registry setting
        COMMS_CFG mgmtCommCfg;
        GetCommSettings( DCT_MGMT, mgmtCommCfg );

        COMMS_CFG baseCommCfg;
        GetCommSettings( DCT_BASE, baseCommCfg );

        COMMS_CFG mplCommCfg;
        GetCommSettings( DCT_MPL, mplCommCfg );

        if( mgmtCommCfg.portType == CT_UNUSED )
            m_mgmtPort = new TNullMgmtPort();
        else
            m_mgmtPort = new TRealMgmtPort();

        if( baseCommCfg.portType == CT_UNUSED )
            m_radio = new TNullBaseRadio();
        else
            m_radio = new TRealBaseRadio();

        if( mplCommCfg.portType == CT_UNUSED )
        {
            m_mplType = MT_NULL;
            m_device  = new TNullMPLDevice();
        }
        else
        {
            m_mplType = MT_MPL;
            m_device  = new TWirelessMPLDevice();
        }
    }

    // For the MPL, we use the 'auto switch control' feature
    m_radio->AutoSwitchControl = true;

    // Hook the MPL device event handlers
    m_device->OnDeviceEvent = OnMPLSEvent;
}


void TMPLSManager::DeleteDevices( void )
{
    // Delete the radio if it exists
    if( m_radio != NULL )
    {
        delete m_radio;
        m_radio = NULL;
    }

    // Delete the device if it exists
    if( m_device != NULL )
    {
        delete m_device;
        m_device = NULL;
    }

    // Delete the Mgmt Port if it exists
    if( m_mgmtPort != NULL )
    {
        delete m_mgmtPort;
        m_mgmtPort = NULL;
    }
}


bool TMPLSManager::LaunchPlug( eCanisterNbr ePlug )
{
    // Instructs the MPL to activate the launch mechanism
    if( m_device == NULL )
        return false;

    if( !GetCanLaunch( ePlug ) )
        return false;

    if( !BatteryGoodForCycle( ePlug, true ) )
        return false;

    if( !m_device->LaunchPlug( ePlug ) )
        return false;

    return true;
}


bool TMPLSManager::CleanCanister( eCanisterNbr eCan )
{
    // Instructs the MPL to activate the cleaning mechanism
    if( m_device == NULL )
        return false;

    if( !GetCanClean( eCan ) )
        return false;

    if( !BatteryGoodForCycle( eCan, true ) )
        return false;

    if( !m_device->CleanCanister( eCan ) )
        return false;

    return true;
}


bool TMPLSManager::DoResetFlag( void )
{
    // Instructs the MPL to activate its 'reset flag' solenoid.
    // Returns true on success.
    if( m_device == NULL )
        return false;

    if( !m_device->CanStartCycle )
        return false;

    // Fake out a canister number for this check - need to
    // check the master TEC's battery
    if( !BatteryGoodForCycle( eCN_Master1, true ) )
        return false;

    return m_device->ActivateResetFlag();
}


void TMPLSManager::ClearPlugDetected( void )
{
    if( m_device != NULL )
        m_device->AckPlugDetected();
}


bool TMPLSManager::GetCanLaunch( eCanisterNbr eWhichCan )
{
    // Do some validation first
    if( ( eWhichCan < 0 ) || ( eWhichCan >= eCN_NbrCanisters ) )
        return false;

    if( m_device == NULL )
        return false;

    if( !m_device->CanStartCycle )
        return false;

    if( !BatteryGoodForCycle( eWhichCan, false ) )
        return false;

    // Fall through means operation allowed
    return true;
}


bool TMPLSManager::GetCanClean( eCanisterNbr eWhichCan )
{
    // Do some validation first
    if( ( eWhichCan < 0 ) || ( eWhichCan >= eCN_NbrCanisters ) )
        return false;

    if( m_device == NULL )
        return false;

    if( !m_device->CanStartCycle )
        return false;

    if( !BatteryGoodForCycle( eWhichCan, false ) )
        return false;

    // Fall through means operation allowed
    return true;
}


int TMPLSManager::GetActiveOrPendingSolByUnit( eMPLSUnit whichUnit )
{
    // The complication here is that we may have just issued a launch
    // command but the MPLS has yet to respond with an RFC. Therefore
    // its active solenoid won't have been updated yet.
    if( ( whichUnit < 0 ) || ( whichUnit >= eMU_NbrMPLSUnits ) )
        return 0;

    // Check first if we have an active solenoid
    int iActiveSol = m_device->ActiveSol[whichUnit];

    // If we have an active solenoid, report it
    if( iActiveSol > 0 )
        return iActiveSol;

    // We have no active solenoid, check for a pending one
    int iPendingSol = m_device->PendingSol[whichUnit];

    // Pending can be -1 if there is no pending command. So watch
    // for that here.
    if( iPendingSol > 0 )
        return iPendingSol;
    else
        return 0;
}


int TMPLSManager::GetActiveOrPendingSolByCan( eCanisterNbr eWhichCan )
{
    // The complication here is that we may have just issued a launch
    // command but the MPLS has yet to respond with an RFC. Therefore
    // its active solenoid won't have been updated yet.

    // Check first if we have an active solenoid
    int iActiveSol;

    switch( eWhichCan )
    {
        case eCN_Master1:
        case eCN_Master2:
            iActiveSol = m_device->ActiveSol[eMU_MasterMPLS];
            break;

        case eCN_Slave1:
        case eCN_Slave2:
            iActiveSol = m_device->ActiveSol[eMU_SlaveMPLS];
            break;

        default:
           return false;
    }

    // If we have an active solenoid, report it
    if( iActiveSol > 0 )
        return iActiveSol;

    // We have no active solenoid, check for a pending one
    int iPendingSol;

    switch( eWhichCan )
    {
        case eCN_Master1:
        case eCN_Master2:
            iPendingSol = m_device->PendingSol[eMU_MasterMPLS];
            break;

        case eCN_Slave1:
        case eCN_Slave2:
            iPendingSol = m_device->PendingSol[eMU_SlaveMPLS];
            break;

        default:
           return false;
    }

    if( iPendingSol > 0 )
        return iPendingSol;
    else
        return 0;
}


bool TMPLSManager::GetIsLaunching( eCanisterNbr eWhichCan )
{
    // Private method, so no param validation.
    if( m_device == NULL )
        return false;

    int iActiveSol = GetActiveOrPendingSolByCan( eWhichCan );

    switch( eWhichCan )
    {
        case eCN_Master1:   return( iActiveSol == 1 );
        case eCN_Master2:   return( iActiveSol == 3 );
        case eCN_Slave1:    return( iActiveSol == 1 );
        case eCN_Slave2:    return( iActiveSol == 3 );
    }

    // Fall through means bogus enum
    return false;
}


bool TMPLSManager::GetIsCleaning( eCanisterNbr eWhichCan )
{
    // Private method, so no param validation.
    if( m_device == NULL )
        return false;

    int iActiveSol = GetActiveOrPendingSolByCan( eWhichCan );

    switch( eWhichCan )
    {
        case eCN_Master1:   return( iActiveSol == 2 );
        case eCN_Master2:   return( iActiveSol == 4 );
        case eCN_Slave1:    return( iActiveSol == 2 );
        case eCN_Slave2:    return( iActiveSol == 4 );
    }

    // Fall through means bogus enum
    return false;
}


bool TMPLSManager::AssertAlertOutput( void )
{
    if( m_radio == NULL )
        return false;

    if( m_bAssertAlarmOut )
        return m_radio->SetOutputSwitch( MPL_ALERT_OUTPUT_SWITCH_NBR, eBC_TurnOn );
    else
        return true;
}


void TMPLSManager::ClearAlertOutput( void )
{
    if( m_radio == NULL )
        return;

    if( m_bAssertAlarmOut )
        m_radio->SetOutputSwitch( MPL_ALERT_OUTPUT_SWITCH_NBR, eBC_TurnOff );
}


bool TMPLSManager::CommsGetStats( PORT_STATS& portStats )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_device != NULL )
    {
        m_device->GetCommStats( portStats );
        return true;
    }

    return false;
}


void TMPLSManager::CommsClearStats( void )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_device != NULL )
        m_device->ClearCommStats();
}


bool TMPLSManager::GetRadioStats( PORT_STATS& radioStats )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_radio != NULL )
        return m_radio->GetCommStats( radioStats );

    return false;
}


void TMPLSManager::ClearRadioStats( void )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_radio != NULL )
        m_radio->ClearCommStats();
}


bool TMPLSManager::MgmtGetStats( PORT_STATS& mgmtStats )
{
    // We only report the connection type for the management port
    if( m_mgmtPort == NULL )
        return false;

    if( !m_mgmtPort->GetCommStats( mgmtStats ) )
        return false;

    return true;
}


void TMPLSManager::ClearMgmtStats( void )
{
    // Currently this does nothing, as we don't track any data traffic
    // on the management port
}


UnicodeString TMPLSManager::GetMPLTypeText( void )
{
    // No need to access thread lock for this function
    return ::MPLTypeText[m_mplType];
}


String TMPLSManager::GetMPLPortDesc( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return "";

    // If we aren't connected, overwrite the dev status with comm status
    if( PollerState != PS_RUNNING )
        return m_connectResult;

    return m_device->DeviceStatus;
}


String TMPLSManager::GetMPLStatusText( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return "";

    // If we aren't connected, overwrite the dev status with comm status
    if( PollerState != PS_RUNNING )
        return m_connectResult;

    return m_device->DeviceStatus;
}


bool TMPLSManager::GetMPLIsIdle( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    // Rely on the 'can start data collection' property for this
    if( !m_device->IsConnected )
        return false;

    return m_device->DeviceIsIdle;
}


bool TMPLSManager::GetMPLIsCalibrated( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->DeviceIsCalibrated;
}


bool TMPLSManager::GetMPLIsRecving( void )
{
    if( m_device == NULL )
        return false;

    return m_device->DeviceIsRecving;
}


bool TMPLSManager::GetMPLIsConn( void )
{
    if( m_radio == NULL )
        return false;

    return m_device->IsConnected;
}


bool TMPLSManager::GetUsingMPLSim( void )
{
    COMMS_CFG wtttsCfg;
    GetCommSettings( DCT_MPL, wtttsCfg );

    if( wtttsCfg.portType == CT_UDP )
        return true;
    else
        return false;
}


bool TMPLSManager::GetCanStartCycle( void )
{
    if( m_device == NULL )
        return false;

    return m_device->CanStartCycle;
}


bool TMPLSManager::GetPlugDetected( void )
{
    if( m_device == NULL )
        return false;

    return m_device->PlugDetected;
}


bool TMPLSManager::GetFlagDeployed( void )
{
    if( m_device == NULL )
        return false;

    return m_device->FlagTripped;
}


bool TMPLSManager::GetMPLPortLost( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->PortLost;
}


int TMPLSManager::GetMPLRSSI( void )
{
    if( ( m_device == NULL ) || !m_device->IsConnected )
        return 0;

    // For now, we assume the TesTORK is always connected
    // to radio port 0, so return its RSSI
    if( m_radio == NULL )
        return 0;

    DWORD dwRSSI = m_radio->RSSI[eBRN_2];

    // Map the value of 0 to 255 to percentage
    return 100 * (int)dwRSSI / 255;
}


bool TMPLSManager::GetMPLMasterPIBPresent( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->HaveMasterPIB;
}


bool TMPLSManager::GetMPLSlaveTECPresent( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->HaveSlaveTEC;
}


bool TMPLSManager::GetMPLSlavePIBPresent( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->HaveSlavePIB;
}


bool TMPLSManager::GetMPLPlugDetPresent( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->HavePlugDet;
}


TMPLDevice::BatteryLevelType TMPLSManager::GetMPLBattLevel( eMPLSUnit whichUnit )
{
    if( m_device == NULL )
        return TMPLDevice::eBL_Unknown;

    return m_device->BatteryLevel[whichUnit];
}


BATTERY_TYPE TMPLSManager::GetMPLBattType ( eMPLSUnit whichUnit )
{
    if( m_device == NULL )
        return BT_UNKNOWN;

    return m_device->BatteryType[whichUnit];
}


bool TMPLSManager::BatteryGoodForCycle( eCanisterNbr eWhichCan, bool bPostAlert )
{
    // Returns true if a cycle can occur based on battery voltage.
    // Will post an alert if bPostAlert is true, but only if the
    // build option allowing a cycle start on low battery is defined.
    bool bBattGood = false;

    switch( eWhichCan )
    {
        case eCanisterNbr::eCN_Master1:
        case eCanisterNbr::eCN_Master2:
           bBattGood = BattLevelGood( eMU_MasterMPLS );
           break;

        case eCanisterNbr::eCN_Slave1:
        case eCanisterNbr::eCN_Slave2:
           bBattGood = BattLevelGood( eMU_SlaveMPLS );
           break;
    }

    // Exit now if battery is good
    if( bBattGood )
        return true;

    // Fall through to here means batt not good. Processing depends
    // on build option
#ifdef ALLOW_CYCLE_ON_LOW_BATTERY

    // Allow the operation, but popup a warning to the user
    if( bPostAlert )
    {
        PostEventToClient( eMME_LowBattOnCycle );

        // Check if we'd already nagged a user about a low battery
        switch( m_alarmStatus[eAT_LowBattery].eState )
        {
            case eAS_NewAlarm:
            case eAS_AckdAlarm:
                // There was already a low batt alarm in progress.
                // Reset its timer
                m_alarmStatus[eAT_LowBattery].tLastAck = Now();
                break;
        }
    }

    return true;

#else

    // Cycle not allowed on low batt
    return false;

#endif
}


bool TMPLSManager::BattLevelGood( eMPLSUnit whichUnit )
{
    TMPLDevice::BatteryLevelType battLevel = m_device->BatteryLevel[whichUnit];

    switch( battLevel )
    {
        case TMPLDevice::eBL_Med:
        case TMPLDevice::eBL_OK:
            // Can launch or clean at these levels
            return true;
    }

    // Fall through means battery level too low or unknown
    return false;
}


String TMPLSManager::GetBaseRadioStatus( void )
{
    if( m_radio == NULL )
        return "";

    return m_radio->DeviceStatus;
}


bool TMPLSManager::GetBaseRadioPortLost( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->PortLost;
}


bool TMPLSManager::GetBaseRadioIsConn( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->IsConnected;
}


bool TMPLSManager::GetBaseRadioIsRecving( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->IsReceiving;
}


void TMPLSManager::SetPollerState( POLLER_STATE epsNew, CONNECT_RESULT eCRMsgEnum )
{
    m_pollerState   = epsNew;
    m_connectResult = ConnResultStrings[eCRMsgEnum];
}


void TMPLSManager::PostEventToClient( MPLSMgrEvent eEvent, DWORD dwLParam )
{
    ::PostMessage( m_hwndEvent, m_dwEventMsg, eEvent, dwLParam );
}


bool TMPLSManager::GetLastStatusPktInfo( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->ShowLastRFCPkt( pCaptions, pValues );
}


bool TMPLSManager::GetLastStatusPkt( TDateTime& pktTime, DWORD& rfcCount, LOGGED_RFC_DATA& lastPkt )
{
    if( m_device == NULL )
       return false;

    return m_device->GetLastRFCPkt( pktTime, rfcCount, lastPkt );
}


bool TMPLSManager::GetAvgReadings( AVG_READINGS& avgReadings )
{
    // Returns average reading values. Returns false if readings are not
    // available. Returns NaN for a reading if the unit is not in the config.
    if( m_device == NULL )
       return false;

    avgReadings.fRPM = m_device->AvgRPM;

    // Will always have a master TEC
    avgReadings.fTankPress[eMU_MasterMPLS] = m_device->AvgTankPress[eMU_MasterMPLS];
    avgReadings.fRegPress [eMU_MasterMPLS] = m_device->AvgRegPress [eMU_MasterMPLS];

    // May not have a slave TEC though
    if( m_device->HaveSlaveTEC )
    {
        avgReadings.fTankPress[eMU_SlaveMPLS] = m_device->AvgTankPress[eMU_SlaveMPLS];
        avgReadings.fRegPress [eMU_SlaveMPLS] = m_device->AvgRegPress [eMU_SlaveMPLS];
    }
    else
    {
        avgReadings.fTankPress[eMU_SlaveMPLS] = NaN;
        avgReadings.fRegPress [eMU_SlaveMPLS] = NaN;
    }

    return true;
}


bool TMPLSManager::GetRFCData( LOGGED_RFC_DATA& rfcData )
{
    // Gets the next RFC packet from the internal queue. Returns
    // true if a packet was there and returned.
    return GetPktFromQueue( rfcData );
}


void TMPLSManager::ClearRFCData( void )
{
    FlushPendingPktQueue();
}


bool TMPLSManager::GetLastRadioStatusPktInfo( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_radio == NULL )
       return false;

    return m_radio->ShowLastStatusPkt( pCaptions, pValues );
}


int TMPLSManager::GetRadioChannel( eBaseRadioNbr radioNbr )
{
    if( ( m_radio == NULL ) || ( !m_radio->IsReceiving ) )
        return 0;

    time_t tLastPkt;
    BASE_STATUS_STATUS_PKT statusPkt;

    if( !m_radio->GetLastStatusPkt( tLastPkt, statusPkt ) )
        return 0;

    if( ( radioNbr >= 0 ) && ( radioNbr < eBRN_NbrBaseRadios ) )
        return statusPkt.radioInfo[radioNbr].chanNbr;

    // Fall through means invalid radio number passed
    return 0;
}


TBaseRadioDevice::SET_RADIO_CHAN_RESULT TMPLSManager::SetRadioChannel( eBaseRadioNbr radioNbr, int newChanNbr )
{
    if( m_radio == NULL )
       return TBaseRadioDevice::SRCR_NO_RADIO;

     return m_radio->SetRadioChannel( radioNbr, newChanNbr );
}


void TMPLSManager::RefreshSettings( int unitsOfMeasure )
{
    // Record that a refresh has been requested. The actual refreshing
    // of data will happen in the worker thread.
    m_newUnits        = unitsOfMeasure;
    m_refreshSettings = true;
}


bool TMPLSManager::GetCalDataRaw( eMPLSUnit whichUnit, TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->GetCalDataRaw( whichUnit, pCaptions, pValues );
}


bool TMPLSManager::GetCalDataFormatted( eMPLSUnit whichUnit, TMPLDevice::CALIBRATION_ITEM ciItem, String& sValue )
{
    if( m_device == NULL )
       return false;

    return m_device->GetCalDataFormatted( whichUnit, ciItem, sValue );
}


bool TMPLSManager::GetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->GetCalDataFormatted( whichUnit, pCaptions, pValues );
}


bool TMPLSManager::SetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    if( m_device == NULL )
       return false;

    return m_device->SetCalDataFormatted( whichUnit, pCaptions, pValues );
}


bool TMPLSManager::ReloadCalDataFromDevice( eMPLSUnit whichUnit )
{
    if( m_device == NULL )
       return false;

    return m_device->ReloadCalDataFromDevice( whichUnit );
}


bool TMPLSManager::WriteCalDataToDevice( eMPLSUnit whichUnit )
{
    if( m_device == NULL )
       return false;

    return m_device->WriteCalDataToDevice( whichUnit );
}


bool TMPLSManager::GetDeviceHWInfo( eMPLSComponent whichComp, bool& bPresent, MPLS_COMP_ID& devInfo )
{
    if( m_device == NULL )
       return false;

    return m_device->GetDeviceHWInfo( whichComp, bPresent, devInfo );
}


bool TMPLSManager::GetDeviceBatteryInfo( eMPLSUnit whichUnit, BATTERY_INFO& battInfo )
{
    if( m_device == NULL )
       return false;

    if( ( whichUnit == eMU_SlaveMPLS ) && !m_device->HaveSlaveTEC )
       return false;

    return m_device->GetDeviceBatteryInfo( whichUnit, battInfo );
}


void __fastcall TMPLSManager::OnMPLSEvent( TObject* Sender, eMPLSEvent eEvt, const XFER_BUFFER& sbEvtData )
{
    switch( eEvt )
    {
        case eME_DevConnected:
            // Called when comms established with a device (eg, device initialized
            // and all config data downloaded). Let the client know
            PostEventToClient( eMME_Connected );
            break;

        case eME_InitFailed:
            // Called if init script could not complete. Report to client
            PostEventToClient( eMME_MPLSInitFailed );
            break;

        case eME_LinkStateChange:
            PostEventToClient( eMME_LinkStateChange );
            PostEventToClient( eMME_RFCRecvd );
            break;

        case eME_RFCRcvd:
            // On receipt of RFC, add it to the queue
            {
                TDateTime tPktTime;
                DWORD     rfcCount;
                LOGGED_RFC_DATA rfcPkt;

                if( m_device->GetLastRFCPkt( tPktTime, rfcCount, rfcPkt ) )
                {
                    AddPktToPendingQueue( rfcPkt );

                    ProcessAlarms();
                    PostEventToClient( eMME_RFCRecvd );
                }
            }
            break;

        case eME_LaunchDone:
            PostEventToClient( eMME_LaunchDone, sbEvtData.iData );
            break;

        case eME_PlugDetected:
            PostEventToClient( eMME_PlugDetected );
            break;

        case eME_CleaningDone:
            PostEventToClient( eMME_CleaningDone, sbEvtData.iData );
            break;

        case eME_FlagTripped:
            PostEventToClient( eMME_FlagDeployed );
            break;

        case eME_FlagResetDone:
            PostEventToClient( eMME_ResetFlagDone, sbEvtData.iData );
            break;

        case eME_CommandFailed:
            // Called if MPLS failed a command. Report to client
            PostEventToClient( eMME_MPLSCmdFailed );
            break;

        case eME_HWErrors:
            // Called if MPLS is reporting comms errors on wired network
            PostEventToClient( eMME_HWCommsErrors, sbEvtData.dwData );
            break;
    }
}


//
// Queue Handling Functions
//

static int AdvanceIndex( int currIndex )
{
    currIndex++;

    if( currIndex >= MAX_QUEUED_RFCS )
        currIndex = 0;

    return currIndex;
}


void TMPLSManager::FlushPendingPktQueue( void )
{
    // Flushes all pending packets from the queue. This is a thread-safe function.
    EnterCriticalSection( &m_criticalSection );

    try
    {
        m_pktReadIndex  = 0;
        m_pktWriteIndex = 0;
    }
    __finally
    {
        LeaveCriticalSection ( &m_criticalSection );
    }
}


void TMPLSManager::AddPktToPendingQueue( const LOGGED_RFC_DATA& nextRFC )
{
    // Adds a packet received by the serial port into the pending queue. Will
    // overwrite an old packet's data if the queue is full.
    EnterCriticalSection ( &m_criticalSection );

    // Make sure we have room for this packet. If not, toss the
    // oldest packet.
    int newWriteIndex = AdvanceIndex( m_pktWriteIndex );

    if( newWriteIndex == m_pktReadIndex )
        m_pktReadIndex = AdvanceIndex( m_pktReadIndex );

    // Store the sample
    m_rfcQueue[m_pktWriteIndex] = nextRFC;

    // Okay to advance the write index now
    m_pktWriteIndex = newWriteIndex;

    LeaveCriticalSection ( &m_criticalSection );
}


bool TMPLSManager::GetPktFromQueue( LOGGED_RFC_DATA& nextRFC )
{
    // Gets the next packet from the queue. Returns true if a packet was
    // pending (and copied), false otherwise. This function is thread-safe.
    bool havePkt = false;

    EnterCriticalSection ( &m_criticalSection );

    // Check if we have packets to report
    if( m_pktReadIndex != m_pktWriteIndex )
    {
        nextRFC = m_rfcQueue[m_pktReadIndex];
        havePkt = true;

        // Advance our read index now
        m_pktReadIndex = AdvanceIndex( m_pktReadIndex );
    }

    LeaveCriticalSection ( &m_criticalSection );

    return havePkt;
}


//
// Alarm Management
//

void TMPLSManager::ResetAlarms( void )
{
    for( int iAlarm = 0; iAlarm < eAT_NbrAlarmTypes; iAlarm++ )
    {
        m_alarmStatus[iAlarm].eState       = eAS_NoAlarm;
        m_alarmStatus[iAlarm].tLastAck.Val = 0.0;
    }
}


void TMPLSManager::ProcessAlarms( void )
{
    // Look for alarm conditions and report them to the notification window.
    // Alarms are processed as follows:
    //
    //   Device State       eState          Action
    //   -------------------------------------------------------------
    //   No alarm present   eAS_NoAlarm     None
    //   No alarm present   eAS_NewAlarm    Alarm condition has gone away, stay in this state until user ack's it
    //   No alarm present   eAS_AckdAlarm   Alarm condition has gone away, can reset state to eAS_NoAlarm
    //   Alarm present      eAS_NoAlarm     Switch to either new alarm or ack'd alarm state, depending on tLastAck
    //                                      If new alarm, then post event to notification window
    //   Alarm present      eAS_NewAlarm    Alarm condition is still present and not yet ack'd by user
    //                                      Repost notification if ack timeout has elapsed
    //   Alarm present      eAS_AckdAlarm   Alarm condition is still present and has been ack'd by user
    //                                      Repost notification if ack timeout has elapsed

    const __int64 AckMinutesTimeout = 2;

    // Check for pending alarms
    if( m_device->IsConnected )
    {
        for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
        {
            eMPLSUnit eUnit = (eMPLSUnit)iUnit;

            switch( m_device->BatteryLevel[eUnit] )
            {
                case TMPLDevice::eBL_Med:
                case TMPLDevice::eBL_OK:
                    // Can clear an ack'd alarms in this condition
                    if( m_alarmStatus[eAT_LowBattery].eState == eAS_AckdAlarm )
                        m_alarmStatus[eAT_LowBattery].eState = eAS_NoAlarm;
                    break;

                case TMPLDevice::eBL_Low:
                    // This is an alarm condition
                    switch( m_alarmStatus[eAT_LowBattery].eState )
                    {
                        case eAS_NoAlarm:
                            // If the alarm has already been acknowledged, consider
                            // this a continuation of the previous alarm. Otherwise,
                            // it is a new alarm
                            if( Now() > IncMinute( m_alarmStatus[eAT_LowBattery].tLastAck, AckMinutesTimeout ) )
                            {
                                m_alarmStatus[eAT_LowBattery].eState = eAS_NewAlarm;
                                PostEventToClient( eMME_Alarm, eAT_LowBattery );
                            }
                            else
                            {
                                m_alarmStatus[eAT_LowBattery].eState = eAS_AckdAlarm;
                            }
                            break;

                        case eAS_NewAlarm:
                        case eAS_AckdAlarm:
                            // Nothing to do here - will already have notified the user
                            break;
                    }
                    break;

                case TMPLDevice::eBL_Unknown:
                    // Don't process alarms in this state
                    break;
            }
        }

        // Look for any ack'd alarms. These are still pending and a notification reposted
        // if the ack timer has expired.
        for( int iAlarm = 0; iAlarm < eAT_NbrAlarmTypes; iAlarm++ )
        {
            if( m_alarmStatus[iAlarm].eState == eAS_AckdAlarm )
            {
                if( Now() > IncMinute( m_alarmStatus[iAlarm].tLastAck, AckMinutesTimeout ) )
                {
                    // Alarm condition is still present. Time to nag the user again
                    m_alarmStatus[iAlarm].eState = eAS_NewAlarm;
                    PostEventToClient( eMME_Alarm, iAlarm );
                }
            }
        }
    }
}


bool TMPLSManager::GetHaveAlarm( void )
{
    for( int iAlarm = 0; iAlarm < eAT_NbrAlarmTypes; iAlarm++ )
    {
        if( m_alarmStatus[iAlarm].eState != eAS_NoAlarm )
            return true;
    }

    // Fall through means no alarm
    return false;
}


bool TMPLSManager::GetAlarmActive( int eAlarmTypeEnum )
{
    if( ( eAlarmTypeEnum < 0 ) || ( eAlarmTypeEnum >= eAT_NbrAlarmTypes ) )
        return false;

    return ( m_alarmStatus[eAlarmTypeEnum].eState != eAS_NoAlarm );
}


TDateTime TMPLSManager::GetAlarmAckTime( int eAlarmTypeEnum )
{
    if( ( eAlarmTypeEnum < 0 ) || ( eAlarmTypeEnum >= eAT_NbrAlarmTypes ) )
        return 0.0;

    return ( m_alarmStatus[eAlarmTypeEnum].tLastAck );
}


void TMPLSManager::AckAlarm( AlarmType eWhichAlarm )
{
    if( ( eWhichAlarm < 0 ) || ( eWhichAlarm >= eAT_NbrAlarmTypes ) )
        return;

    // Note that this alarm has been ack'd
    m_alarmStatus[eWhichAlarm].tLastAck = Now();

    if( m_alarmStatus[eWhichAlarm].eState == eAS_NewAlarm )
        m_alarmStatus[eWhichAlarm].eState = eAS_AckdAlarm;
}

