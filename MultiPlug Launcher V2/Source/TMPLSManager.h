#ifndef TMPLSManagerH
#define TMPLSManagerH

//******************************************************************************
//
//  TMPLSManager.cpp: This module provides a wrapper around the lower
//        level hardware classes, implements a thread to keep the comms
//        comms constantly updated, and implements the business rules
//        for the operation of a MPLS system
//
//******************************************************************************

#include "MPLSDefs.h"
#include "WTTTSDefs.h"
#include "MPLDeviceBaseClass.h"
#include "BaseRadioDevice.h"
#include "MgmtPortDevice.h"


// The TMPLSManager class does most of its work in a separate thread. It provides
// events notifications through a PostMessage() call back to a client window.
// The following enums are passed as the WParam member in the message. Depending
// on the WParam, an LParam value may also be provided as noted below.
typedef enum {
    eMME_Initializing,          // Event posted while comm ports are being opened; gets posted multiple times
    eMME_InitComplete,          // Ports are open and ready to start mode operating
    eMME_PortLost,              // Event posted when a port has been lost
    eMME_CommsLost,             // Event posted when we've lost comms with either the MPL or base radio
    eMME_ThreadFailed,          // Unhandled exception in poll thread
    eMME_Connected,             // Now connected to a MPLS system
    eMME_LinkStateChange,       // MPLS device link state has changed
    eMME_RFCRecvd,              // RFC received, new status values will be available
    eMME_LaunchDone,            // A plug launch cycle has been completed
    eMME_PlugDetected,          // A plug-detection event has occurred
    eMME_CleaningDone,          // Cleaning cycle is complete
    eMME_FlagDeployed,          // Mechanical flag has been deployed
    eMME_ResetFlagDone,         // A reset flag cycle is complete
    eMME_Alarm,                 // An alarm has occurred.
    eMME_MPLSInitFailed,        // Could not initialize MPLS device
    eMME_MPLSCmdFailed,         // MPLS reported failure for a command
    eMME_LowBattOnCycle,        // MPLS battery voltage low on start of cycle
    eMME_HWCommsErrors,         // MPLS is reporting errors on internal comms
    eMME_NbrMPLSMgrEvents
} MPLSMgrEvent;


class TMPLSManager : public TThread
{
  public:

    typedef enum {
        PS_INITIALIZING,       // Thread is starting
        PS_BR_PORT_SCAN,       // Looking for base radio comm port
        PS_MPL_PORT_SCAN,      // Looking for MPL comm port
        PS_MPL_CHAN_SCAN,      // Looking for MPL radio channel
        PS_RUNNING,            // Ports open and have MPL
        PS_COMMS_FAILED,       // Comm error, see ConnectResult for more info
        PS_EXCEPTION,          // Thread loop failed due to exception being thrown
        NBR_POLLER_ERRORS
    } POLLER_STATE;

    typedef enum {
        eAT_None,              // No alarm
        eAT_LowBattery,        // Low battery alarm
        eAT_NbrAlarmTypes
    } AlarmType;

  private:

    typedef enum {
        eCR_Initializing,
        eCR_BadBaseRadioPort,
        eCR_BadMPLPort,
        eCR_BadMgmtPort,
        eCR_Connected,
        eCR_HuntingForBase,
        eCR_HuntingForMPL,
        eCR_WinsockError,
        eCR_ThreadException,
        eCR_NumConnectResult
    } CONNECT_RESULT;

    static const String ConnResultStrings[eCR_NumConnectResult];

    typedef enum {
        eUnknown,
        eRadio0,
        eRadio1,
        eBase,
        eMgmt
    } PORT_TYPE;

    TMPLDevice*       m_device;
    TBaseRadioDevice* m_radio;
    TMgmtPortDevice*  m_mgmtPort;
    MPL_TYPE          m_mplType;

    WIRELESS_SYSTEM_INFO m_wirelessInfo;

    POLLER_STATE      m_pollerState;
    String            m_connectResult;

    DWORD             m_dwRadioChanWaitTime;

    // Alarm management
    typedef enum {
        eAS_NoAlarm,
        eAS_NewAlarm,
        eAS_AckdAlarm,
        eAS_NbrAlarmStates
    } AlarmState;

    typedef struct {
        AlarmState eState;
        TDateTime  tLastAck;
    } ALARM_STATUS;

    ALARM_STATUS      m_alarmStatus[eAT_NbrAlarmTypes];

    // Event notification window handle
    HWND              m_hwndEvent;
    DWORD             m_dwEventMsg;

    // Operating params
    DWORD m_dwPlugDetectTimeout;

    bool IsThreadRunning( void ) { return( m_device != NULL ); }

    void SetPollerState( POLLER_STATE epsNew, CONNECT_RESULT eCRMsgEnum );
    void PostEventToClient( MPLSMgrEvent eEvent, DWORD dwLParam = 0 );

    bool OpenAssignedPorts( void );
    bool OpenAutomaticPorts( void );

    bool FindDevPorts( const COMMS_CFG& radioCommCfg, COMMS_CFG& deviceCommCfg, COMMS_CFG& mgmtCommCfg );
    PORT_TYPE GetPortType( CCommPort* pPort );
    bool WigglePort( CCommPort* pPort, BYTE& bInitCtrlStatus, BYTE& bHighCtrlStatus, BYTE& bLastCtrlStatus );

    void AfterCommConnect( void );

    void CreateDevices( void );
    void DeleteDevices( void );

    void ProcessAlarms( void );

    // MPLS Event handler
    void __fastcall OnMPLSEvent( TObject* Sender, eMPLSEvent eEvt, const XFER_BUFFER& sbEvtData );

  protected:

    void __fastcall Execute();
        // Worker thread function

    void FlushPendingPktQueue( void );
        // Flushes all pending packets from the queue.

    void AddPktToPendingQueue( const LOGGED_RFC_DATA& nextRFC );
        // Adds a packet received by the port into the pending queue. Will
        // overwrite an old packet's data if the queue is full.

    bool GetPktFromQueue( LOGGED_RFC_DATA& nextReading );
        // Gets the next reading from the queue. Returns true if a reading
        // was returned.

    // The max number of RFCs should be such that data won't be lost
    // during the longest possible delay in the calling process
    #define MAX_QUEUED_RFCS   50
    LOGGED_RFC_DATA m_rfcQueue[MAX_QUEUED_RFCS];

    int m_pktReadIndex;
    int m_pktWriteIndex;

    // General class vars
    int      m_currUnits;
    int      m_newUnits;
    bool     m_bAssertAlarmOut;

    // The following flag is set by the main thread if this thread should
    // reload settings from the registry. Note that comms settings are
    // not reloaded by this process
    bool     m_refreshSettings;

    // A critical section controls the access of shared objects between threads
    CRITICAL_SECTION m_criticalSection;

    // Property getter methods
    String    GetMPLTypeText( void );
    String    GetMPLStatusText( void );
    String    GetMPLPortDesc( void );
    bool      GetMPLIsIdle( void );
    bool      GetMPLIsCalibrated( void );
    bool      GetMPLIsRecving( void );
    bool      GetMPLIsConn( void );
    bool      GetCanStartCycle( void );
    bool      GetPlugDetected( void );
    bool      GetFlagDeployed( void );
    bool      GetUsingMPLSim( void );
    bool      GetMPLPortLost( void );
    int       GetMPLRSSI( void );
    bool      GetMPLMasterPIBPresent( void );
    bool      GetMPLSlaveTECPresent( void );
    bool      GetMPLSlavePIBPresent( void );
    bool      GetMPLPlugDetPresent( void );

    bool      GetCanLaunch    ( eCanisterNbr eWhichCan );
    bool      GetCanClean     ( eCanisterNbr eWhichCan );
    bool      GetIsLaunching  ( eCanisterNbr eWhichCan );
    bool      GetIsCleaning   ( eCanisterNbr eWhichCan );

    int       GetActiveOrPendingSolByUnit( eMPLSUnit whichUnit );
    int       GetActiveOrPendingSolByCan( eCanisterNbr eWhichCan );

    TMPLDevice::BatteryLevelType GetMPLBattLevel( eMPLSUnit whichUnit );
    BATTERY_TYPE                 GetMPLBattType ( eMPLSUnit whichUnit );
    bool      BattLevelGood( eMPLSUnit whichUnit );
    bool      BatteryGoodForCycle( eCanisterNbr eWhichCan, bool bPostAlert );

    String    GetBaseRadioStatus( void );
    bool      GetBaseRadioPortLost( void );
    bool      GetBaseRadioIsConn( void );
    bool      GetBaseRadioIsRecving( void );

    bool      GetHaveAlarm( void );
    bool      GetAlarmActive ( int eAlarmTypeEnum );
    TDateTime GetAlarmAckTime( int eAlarmTypeEnum );

  public:

    __fastcall TMPLSManager( HWND hwndEvent, DWORD dwEventMsg );
    virtual __fastcall ~TMPLSManager();
        // Constructor and destructor. Because the comm poller does most of
        // its work in a thread, direct event callbacks cannot be used. So
        // instead messages are posted to hwndEvent window when events
        // occur

    __property bool          ThreadRunning  = { read = IsThreadRunning };
    __property POLLER_STATE  PollerState    = { read = m_pollerState };
    __property String        ConnectResult  = { read = m_connectResult };

    void RefreshSettings( int unitsOfMeasure );
        // Informs the poller is must reload registry-based settings

    __property bool          HaveAlarm         = { read = GetHaveAlarm };
    __property bool          AlarmActive[int]  = { read = GetAlarmActive };
    __property TDateTime     AlarmAckTime[int] = { read = GetAlarmAckTime };
        // Alarm properties. The [int] index must be an AlarmType enum

    void ResetAlarms( void );
        // Resets all alarms to the 'no alarm' condition

    void AckAlarm( AlarmType eWhichAlarm );
        // Ack'ing an alarm will suppress it for a given amount of time

    //
    // MPL Specific Methods
    //

    __property String        MPLTypeText     = { read = GetMPLTypeText };
    __property MPL_TYPE      MPLType         = { read = m_mplType };
    __property String        MPLPortDesc     = { read = GetMPLPortDesc };
    __property bool          MPLIsConnected  = { read = GetMPLIsConn };
    __property bool          UsingMPLSim     = { read = GetUsingMPLSim   };

    __property String        MPLStatus       = { read = GetMPLStatusText };
    __property bool          MPLIsIdle       = { read = GetMPLIsIdle };
    __property bool          MPLIsCalibrated = { read = GetMPLIsCalibrated };
    __property bool          MPLIsRecving    = { read = GetMPLIsRecving };
    __property bool          MPLPortLost     = { read = GetMPLPortLost };
    __property int           MPLRSSI         = { read = GetMPLRSSI };

    __property bool          MPLMasterPIBPresent = { read = GetMPLMasterPIBPresent };
    __property bool          MPLSlaveTECPresent  = { read = GetMPLSlaveTECPresent  };
    __property bool          MPLSlavePIBPresent  = { read = GetMPLSlavePIBPresent  };
    __property bool          MPLPlugDetPresent   = { read = GetMPLPlugDetPresent   };

    __property TMPLDevice::BatteryLevelType MPLBatteryLevel[eMPLSUnit] = { read = GetMPLBattLevel };
    __property BATTERY_TYPE                 MPLBatteryType [eMPLSUnit] = { read = GetMPLBattType };

    __property bool          CanStartCycle   = { read = GetCanStartCycle };
    __property bool          PlugDetected    = { read = GetPlugDetected };
    __property bool          FlagDeployed    = { read = GetFlagDeployed };

    __property bool          CanLaunch    [eCanisterNbr] = { read = GetCanLaunch     };
    __property bool          CanClean     [eCanisterNbr] = { read = GetCanClean      };
    __property bool          Launching    [eCanisterNbr] = { read = GetIsLaunching   };
    __property bool          Cleaning     [eCanisterNbr] = { read = GetIsCleaning    };

    __property int           ActiveSolenoid[eMPLSUnit] = { read = GetActiveOrPendingSolByUnit };

    bool LaunchPlug( eCanisterNbr ePlug );
        // Instructs the MPL to activate the launch mechanism for the indicated plug.
        // Returns true on success.

    bool CleanCanister( eCanisterNbr eCan );
        // Instructs the MPL to activate the cleaning mechanism for the indicated.
        // canister. Returns true on success.

    bool DoResetFlag( void );
        // Instructs the MPL to activate its 'reset flag' solenoid. Returns true
        // on success.

    void ClearPlugDetected( void );

    bool AssertAlertOutput( void );
    void ClearAlertOutput( void );
        // Methods to assert and clear the alert output on the base radio

    bool CommsGetStats( PORT_STATS& portStats );
    void CommsClearStats( void );

    bool GetLastStatusPktInfo( TStringList* pCaptions, TStringList* pValues );
    bool GetLastStatusPkt( TDateTime& pktTime, DWORD& rfcCount, LOGGED_RFC_DATA& lastPkt );
        // Pass through function that pulls last status packet information
        // out of the underlying device object.

    typedef struct {
        float fRPM;
        float fTankPress[eMU_NbrMPLSUnits];
        float fRegPress[eMU_NbrMPLSUnits];
    } AVG_READINGS;

    bool GetAvgReadings( AVG_READINGS& avgReadings );
        // Returns average reading values. Returns false if readings are not
        // available. Returns NaN for a reading if the unit is not in the config.

    bool GetRFCData( LOGGED_RFC_DATA& rfcData );
    void ClearRFCData( void );
        // Gets the next RFC packet from the internal queue. Returns
        // true if a packet was there and returned. Use ClearRFCData()
        // to flush any pending RFC packets out of the queue.

    bool GetCalDataRaw      ( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues );
    bool GetCalDataFormatted( eMPLSUnit whichUnit, TMPLDevice::CALIBRATION_ITEM ciItem, String& sValue );
    bool GetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues );
    bool SetCalDataFormatted( eMPLSUnit whichUnit, TStringList* pCaptions,  TStringList* pValues );
        // Pass through functions that get / set calibration data. Not
        // all devices support on-board calibration memory.

    bool ReloadCalDataFromDevice( eMPLSUnit whichUnit );
        // Causes the device to clear it internal cached cal data and
        // reload the data from the device.

    bool WriteCalDataToDevice( eMPLSUnit whichUnit );
        // Writes the calibration data currently cached in the software
        // to the WTTTS device.

    bool GetDeviceHWInfo( eMPLSComponent whichComp, bool& bPresent, MPLS_COMP_ID& devInfo );
        // Returns information about the device hardware

    bool GetDeviceBatteryInfo( eMPLSUnit eWhichUnit, BATTERY_INFO& battInfo );
        // Returns current battery information


    //
    // Base Radio Specific Methods
    //

    __property String        BaseRadioStatus       = { read = GetBaseRadioStatus };
    __property bool          BaseRadioIsConnected  = { read = GetBaseRadioIsConn };
    __property bool          BaseRadioIsRecving    = { read = GetBaseRadioIsRecving };
    __property bool          BaseRadioPortLost     = { read = GetBaseRadioPortLost };

    bool GetRadioStats( PORT_STATS& radioStats );
    void ClearRadioStats( void );

    bool GetLastRadioStatusPktInfo( TStringList* pCaptions, TStringList* pValues );
        // Pass through function that pulls last status packet information
        // out of the underlying device object.

    int GetRadioChannel( eBaseRadioNbr radioNbr );
    TBaseRadioDevice::SET_RADIO_CHAN_RESULT SetRadioChannel( eBaseRadioNbr radioNbr, int newChanNbr );
        // Gets / sets a radio to operate on a given channel


    //
    // Management Port Specific Methods
    //

    bool MgmtGetStats( PORT_STATS& mgmtStats );
    void ClearMgmtStats( void );
};

#endif
