#include <vcl.h>
#pragma hdrstop

#include <FileCtrl.hpp>
#include <shlobj.h>

#include "TMPLSManagerMain.h"
#include "TAboutMPLDlg.h"
#include "TPlugJobsDlg.h"
#include "TPlugJobDetailsDlg.h"
#include "TProjectLogDlg.h"
#include "TGraphForm.h"
#include "TReportSetupDlg.h"
#include "MPLSettingsDlg.h"
#include "PasswordInputDlg.h"
#include "TConfirmTimeDlg.h"
#include "TPlugJob.h"
#include "TTestPlugJob.h"
#include "SerialPortUtils.h"
#include "ApplUtils.h"
#include "TescoShared.h"

#pragma package(smart_init)
#pragma link "AdvSmoothExpanderButtonPanel"
#pragma link "AdvSmoothExpanderPanel"
#pragma link "AdvSmoothPanel"
#pragma link "AdvSmoothToggleButton"
#pragma link "AdvSmoothProgressBar"
#pragma link "GDIPPictureContainer"
#pragma resource "*.dfm"


TMPLMainForm *MPLMainForm;


static const String PasswordDlgCaption( "Administrator Password Required" );
static const String BadPWEnteredMsg( "You have entered the incorrect password." );
static const String EmptyCaption( "---" );

static const TColor clPlugGreen   = (TColor)0x0FAF03;
static const TColor clCleanGreen  = (TColor)0x346830;

#define NBR_CLEANING_ANIMATION_STEPS 8
static const TColor clCleaning[NBR_CLEANING_ANIMATION_STEPS] = {
    clPlugGreen, // start at clPlugGreen
    (TColor)0x14AF03,
    (TColor)0x19A509,
    (TColor)0x1E9B10,
    (TColor)0x239116,
    (TColor)0x28871C,
    (TColor)0x2D7D23,
    clCleanGreen // end at clCleanGreen
};


// Battery level resource names.
const String BattLevelResNames[TMPLDevice::eNbrBattLevels] = {
    "BattLevelImg_Unknown",
    "BattLevelImg_Low",
    "BattLevelImg_Medium",
    "BattLevelImg_Full",
};


static void ResetActionBtn( TAdvSmoothToggleButton* pBtn, bool bStopAnimation = true )
{
    pBtn->Enabled          = false;
    pBtn->Color            = clPlugGreen;
    pBtn->ColorDisabled    = clBtnDisabled;
    pBtn->BorderColor      = clTESCOBlue;
    pBtn->BorderInnerColor = clTESCOBlue;

    // If stopping animation, reset the tag otherwise leave it as-is
    if( bStopAnimation )
        pBtn->Tag = -1;      // -1 ==> not animating
}


static String FloatToMPLSString( float fValue, int nbrDigits = 0 )
{
    if( IsNan( fValue ) )
        return EmptyCaption;

    return FloatToStrF( fValue, ffFixed, 7, nbrDigits );
}


//
// Class Implementation
//

__fastcall TMPLMainForm::TMPLMainForm(TComponent* Owner) : TForm(Owner)
{
    // Make sure we have a registry entry setup for us
    SetMPLSRegistryDefaults();

    // Set the process priority
    DWORD currPriority = GetPriorityClass( GetCurrentProcess() );
    DWORD reqdPriority = GetProcessPriority();

    if( currPriority != reqdPriority )
    {
        if( !SetPriorityClass( GetCurrentProcess(), reqdPriority ) )
        {
            DWORD dwError = GetLastError();

            MessageDlg( "Could not set process priority, error 0x" + IntToHex( (int)dwError, 8 ), mtError, TMsgDlgButtons() << mbOK, 0 );
        }
    }

    // Set initial button states
    EnableLaunchBtn->Enabled = false;
    ResetFlagBtn->Enabled    = false;

    // Setup MPLS controls struct
    m_canisterControl[eCN_Master1].launchBtn = MasterLaunch1Btn;
    m_canisterControl[eCN_Master1].cleanBtn  = MasterClean1Btn;

    m_canisterControl[eCN_Master2].launchBtn = MasterLaunch2Btn;
    m_canisterControl[eCN_Master2].cleanBtn  = MasterClean2Btn;

    m_canisterControl[eCN_Slave1].launchBtn  = SlaveLaunch1Btn;
    m_canisterControl[eCN_Slave1].cleanBtn   = SlaveClean1Btn;

    m_canisterControl[eCN_Slave2].launchBtn  = SlaveLaunch2Btn;
    m_canisterControl[eCN_Slave2].cleanBtn   = SlaveClean2Btn;

    m_mplsDisplays[eMU_MasterMPLS].battVoltsPanel = MasterBattLevelStatusPanel;
    m_mplsDisplays[eMU_MasterMPLS].tankPressPanel = MasterTankPressStatusPanel;
    m_mplsDisplays[eMU_MasterMPLS].regPressPanel  = MasterRegPressStatusPanel;
    m_mplsDisplays[eMU_MasterMPLS].plugImage      = MasterImage;

    m_mplsDisplays[eMU_SlaveMPLS].battVoltsPanel  = SlaveBattLevelStatusPanel;
    m_mplsDisplays[eMU_SlaveMPLS].tankPressPanel  = SlaveTankPressStatusPanel;
    m_mplsDisplays[eMU_SlaveMPLS].regPressPanel   = SlaveRegPressStatusPanel;
    m_mplsDisplays[eMU_SlaveMPLS].plugImage       = SlaveImage;

    // Set colour properties of the launch and slave buttons now
    for( int iCan = 0; iCan < eCN_NbrCanisters; iCan++ )
    {
        ResetActionBtn( m_canisterControl[iCan].launchBtn );
        ResetActionBtn( m_canisterControl[iCan].cleanBtn  );
    }

    ResetActionBtn( ResetFlagBtn );

    // Set enable button colours (can't do these at design time)
    EnableLaunchBtn->BorderColor      = clTESCOBlue;
    EnableLaunchBtn->BorderInnerColor = clTESCOBlue;
    EnableLaunchBtn->ColorDisabled    = clBtnDisabled;

    // Set menu item border colours (can't do these at design time)
    for( int iCtrl = 0; iCtrl < MenuPanel->ControlCount; iCtrl++ )
    {
        TAdvSmoothToggleButton *pBtn = dynamic_cast<TAdvSmoothToggleButton*>( MenuPanel->Controls[iCtrl] );

        if( pBtn != NULL )
        {
            pBtn->BorderColor      = clTESCOBlue;
            pBtn->BorderInnerColor = clTESCOBlue;
        }
    }

    // Set marque colors for progress bars
    BaseRadioProgBar->MarqueeColor = clPlugGreen;
    MPLLinkProgBar->MarqueeColor   = clPlugGreen;

    // Load our custom cursors
    Screen->Cursors[CC_HANDGRAB] = LoadCursor( HInstance, L"HANDGRAB_CUR" );
    Screen->Cursors[CC_HANDFLAT] = LoadCursor( HInstance, L"HANDFLAT_CUR" );

    // Set the job state to our loading state
    SetJobState( eJS_Loading );

    // Reduce flickering on the plug controls panel
    ControlPanel->DoubleBuffered = true;

    // Settings are good at this point. Load default units of measure
    m_defaultUOM = (UNITS_OF_MEASURE) GetDefaultUnitsOfMeasure();

    // Init operating params
    m_dwLaunchClickTimeout = GetLaunchClickTimeout();

    m_dwBtnAnimationTimer = 0;

    // Check that we have a valid job directory defined.
    if( !DirectoryExists( GetJobDataDir() ) )
    {
        int iConfRes = MessageDlg( "You must set the Job Data folder before proceeding.\n\nClick Yes if you would like "
                                   "to use the default location.\n\nClick No to specify a custom location. If you "
                                   "specify a custom location you must ensure all users will have read and write access "
                                   "to that location.", mtInformation, TMsgDlgButtons() << mbYes << mbNo << mbCancel, 0 );

        if( iConfRes == mrYes )
        {
            wchar_t wszPath[MAX_PATH];

            // Default to My Pictures. First, get its path.
            if( SHGetFolderPath( NULL, CSIDL_COMMON_APPDATA, NULL, 0, wszPath ) == S_OK )
            {
                bool bSuccess = true;

                String sCanRigFolder = String( wszPath ) + "\\CanRig";

                if( !DirectoryExists( sCanRigFolder ) )
                    bSuccess &= CreateDir( sCanRigFolder );

                String sMPLSFolder = sCanRigFolder + "\\MPLS";

                if( !DirectoryExists( sMPLSFolder ) )
                    bSuccess &= CreateDir( sMPLSFolder );

                String sLogFolder = sMPLSFolder + "\\JobLogs";

                if( !DirectoryExists( sLogFolder ) )
                    bSuccess &= CreateDir( sLogFolder );

                if( bSuccess )
                {
                    SetJobDataDir( sLogFolder );
                }
                else
                {
                    throw Exception( "Program terminating: unable to create Job Data folder." );
                }
            }
            else
            {
                throw Exception( "Program terminating: unable to get default App Data folder." );
            }
        }
        else if( iConfRes == mrNo )
        {
            String newDir;

            if( SelectDirectory( "Select Job Data Folder", "Desktop", newDir, TSelectDirExtOpts() << sdNewUI << sdNewFolder, NULL ) )
            {
                SetJobDataDir( newDir );
            }
            else
            {
                throw Exception( "Program terminating: Job Data folder is not valid." );
            }
        }
        else
        {
            throw Exception( "Program terminating: Job Data folder has not been defined." );
        }
    }

    // Confirm system time before proceeding, unless we have a command
    // line override
    bool bDoTimeCheck = true;
    m_bIgnoreSimMode = false;

    for( int iParam = 1; iParam <= ParamCount(); iParam++ )
    {
        String sParam = ParamStr( iParam );

        if( sParam.CompareIC( "-notimecheck" ) == 0 )
            bDoTimeCheck = false;

        if( sParam.CompareIC( "-ignoresimmode" ) == 0 )
            m_bIgnoreSimMode = true;
    }

    if( bDoTimeCheck )
    {
        if( TConfirmTimeDlg::ShowConfimTimeDlg( this ) != mrYes )
        {
            Application->Terminate();
            return;
        }
    }

    // Next, set the program in use flag so that we can
    // detect if the program crashed on the next launch.
    m_lastShutdownType = GetLastShutdown();
    SetProgramInUse();
}


void __fastcall TMPLMainForm::FormShow(TObject *Sender)
{
    // Check if a job directory has been specified. If not, they have to do so now
    if( !HaveJobLogDir() )
    {
        Application->Terminate();
        return;
    }

    // Tell the AckForm to advise us of acknowledgements
    AckForm->SetAckEventMessage( Handle, WM_ACK_EVENT );

    // Create the MPLS object now
    m_mplsMgr = new TMPLSManager( Handle, WM_POLLER_EVENT );

    // Finally, open a job
    OpenJob();
}


void __fastcall TMPLMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    // On close, check the state of our job if we have one. Ask the user
    // if the job should be closed (if it was open and not a test job).
    CanClose = CloseJob();
}


void __fastcall TMPLMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Be sure to shut down helper forms
    AckForm->Flush();
    ProjectLogForm->CloseJob();

    // Now delete objects. Null them out too, because we can continue to
    // receive Windows messages after this event handler completes
    if( m_mplsMgr != NULL )
    {
        delete m_mplsMgr;
        m_mplsMgr = NULL;
    }

    if( m_currJob != NULL )
    {
        delete m_currJob;
        m_currJob = NULL;
    }
}


void __fastcall TMPLMainForm::AboutBtnClick(TObject *Sender)
{
    AboutMPLForm->ShowModal();
}


void __fastcall TMPLMainForm::GraphsBtnClick(TObject *Sender)
{
    ShowModelessForm( GraphForm );
}


void __fastcall TMPLMainForm::ReportsBtnClick(TObject *Sender)
{
    // Close the graph form, if it was visible because the
    // report module will fiddle with it.
    bool bGraphWasOpen = GraphForm->Visible;

    GraphForm->Close();

    ReportSetupDlg->ShowReportSetupDlg( m_currJob, GraphForm );

    if( bGraphWasOpen )
        ShowModelessForm( GraphForm );
}


void __fastcall TMPLMainForm::ProjSettingsBtnClick(TObject *Sender)
{
    // Get job info from job. If in test mode, just beep.
    if( ( m_currJob == NULL ) || ( m_currJob->IsTestJob ) )
    {
        Beep();
        return;
    }

    PLUG_JOB_INFO jobInfo;

    TStringList* pPreJobRemarks  = new TStringList();
    TStringList* pPostJobRemarks = new TStringList();

    m_currJob->GetJobInfo( jobInfo );

    m_currJob->GetJobRemarks( TBasePlugJob::eJRT_Pre,  pPreJobRemarks  );
    m_currJob->GetJobRemarks( TBasePlugJob::eJRT_Post, pPostJobRemarks );

    if( m_currJob->Completed )
    {
        PlugJobDetailsForm->ShowJobInfo( jobInfo, pPreJobRemarks, pPostJobRemarks );
    }
    else if( PlugJobDetailsForm->EditJobInfo( jobInfo, pPreJobRemarks, pPostJobRemarks ) )
    {
        m_currJob->SaveJobInfo( jobInfo );

        m_currJob->SaveJobRemarks( TBasePlugJob::eJRT_Pre,  pPreJobRemarks  );
        m_currJob->SaveJobRemarks( TBasePlugJob::eJRT_Post, pPostJobRemarks );
    }

    delete pPreJobRemarks;
    delete pPostJobRemarks;
}


void __fastcall TMPLMainForm::ProjLogBtnClick(TObject *Sender)
{
    ProjectLogForm->ViewLog();
}


void __fastcall TMPLMainForm::SystemTimerTimer(TObject *Sender)
{
    // Check the launch state. If we are enabled and the enable timer
    // has timed out, revert to the ready state
    if( ( m_eJobState == eJS_Loading ) || ( m_eJobState == eJS_Disabled ) )
    {
        return;
    }
    else if( m_eJobState == eJS_Enabled )
    {
        if( HaveTimeout( m_dwEnableBtnClickTime, m_dwLaunchClickTimeout ) )
            SetJobState( m_mplsMgr->CanStartCycle ? eJS_Ready : eJS_Busy );
    }
    else if( m_eJobState == eJS_HWWait )
    {
        // Waiting to connect with hardware - don't need to do anything
    }
    else if( m_eJobState == eJS_Busy )
    {
        if( m_mplsMgr->CanStartCycle )
            SetJobState( eJS_Ready );
    }

    // Also update any buttons in an animated state
    if( GetTickCount() >= m_dwBtnAnimationTimer )
    {
        for( int iCan = 0; iCan < eCN_NbrCanisters; iCan++ )
        {
            // The tag property keeps track of the animation color
            // index. -1 means the button is not animated
            if( m_canisterControl[iCan].launchBtn->Tag >= 0 )
                AnimateActionBtn( m_canisterControl[iCan].launchBtn );

            if( m_canisterControl[iCan].cleanBtn->Tag >= 0 )
                AnimateActionBtn( m_canisterControl[iCan].cleanBtn );
        }

        if( ResetFlagBtn->Tag >= 0 )
            AnimateActionBtn( ResetFlagBtn );

        m_dwBtnAnimationTimer = GetTickCount() + 200;
    }

    // If we have no pending events, clear the output alarm indications
    if( !AckForm->HaveEvents() )
        m_mplsMgr->ClearAlertOutput();
}


void __fastcall TMPLMainForm::EnableLaunchBtnClick(TObject *Sender)
{
    SetJobState( eJS_Enabled );
}


void __fastcall TMPLMainForm::OpenJob1Click(TObject *Sender)
{
    OpenJob();
}


bool TMPLMainForm::OpenJob( void )
{
    // Opens a plug job by showing the Plug Job Manager dialog.
    // If a job is opened, initializes the system for that job
    // and returns true.

    // First check on the state of the currently open job
    if( !CloseJob() )
        return false;

    // We now have no job open. Loop here waiting for the user
    // to select a job.
    String preferredJobFileName = GetMostRecentJob();
    String jobFileName;
    bool   bIsNewJob;

    bool bOpeningJob = true;

    while( bOpeningJob )
    {
        if( PlugJobsForm->OpenJob( preferredJobFileName, jobFileName, bIsNewJob ) )
        {
            // Have a job file. Open this job, even if it is the currently open job.
            // Get the plug info to
            PLUG_JOB_INFO jobInfo;

            if( !TPlugJob::GetJobInfo( jobFileName, jobInfo ) )
            {
                int confRes = MessageDlg( "The job could not be opened. Press Yes to select a different job.",
                                          mtError, TMsgDlgButtons() << mbYes << mbCancel, 0 );

                if( confRes != mrYes )
                    bOpeningJob = false;

                continue;
            }

            // Try to create the job object now
            TBasePlugJob* pNewJob = NULL;

            if( jobInfo.isTestJob )
                pNewJob = new TTestPlugJob( jobFileName );
            else
                pNewJob = new TPlugJob( jobFileName );

            if( pNewJob->JobLoaded )
            {
                // We have a legit job. Can exit the input loop
                bOpeningJob = false;

                // Note this as our current job
                m_currJob = pNewJob;

                // Note we're reopening the job, if its not new and not completed
                if( !bIsNewJob && !m_currJob->Completed )
                    AddJobComment( "Job re-opened" );
            }
            else
            {
                // Job could not be loaded
                delete pNewJob;

                int confRes = MessageDlg( "The job could not be opened. Press Yes to select a different job.",
                                          mtError, TMsgDlgButtons() << mbYes << mbCancel, 0 );

                if( confRes != mrYes )
                    bOpeningJob = false;
            }
        }
        else
        {
            // User cancelled out of Open Job dlg. Break out of loop
            bOpeningJob = false;
        }
    }

    // Re-enable alarm-on-file-save flag
    m_bShowFileSaveError = true;

    // When opening a job, clear any RFC data that may be queued up
    m_mplsMgr->ClearRFCData();

    // Set sim mode if required
    EnterSimMode( m_mplsMgr->UsingMPLSim );

    // Set our state
    if( ( m_currJob == NULL ) || !m_currJob->Loaded )
    {
        SetJobState( eJS_Disabled );
    }
    else
    {
        // We have a job loaded. Sync the hardware to the job
        if( m_currJob->Completed )
        {
            SetJobState( eJS_Complete );
        }
        else
        {
            // If the software just booted, it is possible a connection with
            // an MPLS has already occurred. So first go to the hardware wait
            // state, then check if the MPLS is idle and force a sync if
            // we're already connected
            SetJobState( eJS_HWWait );

            if( m_mplsMgr->MPLIsIdle )
            {
                SyncJobToMPLS( time( NULL ) );
                SetJobState( eJS_Ready );
            }
        }
    }

    // Update visuals
    RefreshCaption();
    SetMainFormDisplay();
    SyncDisplayToMPLS();

    ProjectLogForm->SetJob( m_currJob );
    GraphForm->LoadNewJob( m_currJob );

    // Return true if we have a job loaded
    if( m_currJob == NULL )
        return false;
    else
        return m_currJob->Loaded;
}


bool TMPLMainForm::CloseJob( void )
{
    // Check the state of our job if we have one. Ask the user if the job
    // should be closed (if it was open and not a test job).
    if( m_currJob == NULL )
        return true;

    if( m_currJob->Completed || m_currJob->IsTestJob )
    {
        // Mark the job as complete (this will only affect the case of a
        // test job, as trying to close an already closed job does nothing.
        m_currJob->Completed = true;

        delete m_currJob;
        m_currJob = NULL;

        SetJobState( eJS_Disabled );

        AckForm->Flush();
        ProjectLogForm->CloseJob();

        return true;
    }

    // We have a real job that is still open. Ask the user if it should be closed.
    int msgRes = MessageDlg( "The current job is still open. Do you want to mark this job as completed so that no more changes can be made to the job?"
                             "\r  Click Yes to mark this job as complete."
                             "\r  Click No to leave the job as uncomplete."
                             "\r  Click Cancel to return to the job.",
                               mtConfirmation, TMsgDlgButtons() << mbYes << mbNo << mbCancel, 0 );

    if( ( msgRes == mrYes ) || ( msgRes == mrNo ) )
    {
        if( msgRes == mrYes )
            m_currJob->Completed = true;

        delete m_currJob;
        m_currJob = NULL;

        SetJobState( eJS_Disabled );

        AckForm->Flush();
        ProjectLogForm->CloseJob();

        return true;
    }

    // Assume any other result is a cancel - leave the job open
    return false;
}


void TMPLMainForm::SetJobState( eJobState eNewState )
{
    // Try to set the system to the passed state. Order of checking the
    // new state is important because there are early returns in the code.
    // Use caution if changing the order!!!

    // First, if we are loading a job or have no job then all buttons get disabled
    if( ( eNewState == eJS_Loading ) || ( eNewState == eJS_Disabled ) )
    {
        EnableLaunchBtn->Enabled = false;

        ProjSettingsBtn->Enabled = false;
        ProjLogBtn->Enabled      = false;
        GraphsBtn->Enabled       = false;
        ReportsBtn->Enabled      = false;

        DisableActionBtns( true );

        m_eJobState = eNewState;

        return;
    }

    // If the job is complete, set the overall state and buttons but
    // leave the plug controls as they are. Also do this if we're waiting
    // for the hardware to complete an operation
    if( ( eNewState == eJS_Complete ) || ( eNewState == eJS_HWWait ) )
    {
        EnableLaunchBtn->Enabled = false;

        ProjSettingsBtn->Enabled = true;
        ProjLogBtn->Enabled      = true;
        GraphsBtn->Enabled       = true;
        ReportsBtn->Enabled      = true;

        DisableActionBtns( true );

        m_eJobState = eNewState;

        return;
    }

    // If we are starting an action, disable buttons and wait for that
    // action to complete
    if( eNewState == eJS_Busy )
    {
        EnableLaunchBtn->Enabled = false;

        DisableActionBtns( false );

        m_eJobState = eJS_Busy;

        return;
    }

    // Check to see if we are currently in an actionable state
    switch( m_eJobState )
    {
        case eJS_Loading:
        case eJS_Ready:
        case eJS_Enabled:
        case eJS_Busy:
        case eJS_HWWait:
            // These are actionable states
            break;

        case eJS_Disabled:
        case eJS_Complete:
        default:
            // Not actionable states
            return;
    }

    // We are in an actionable state.
    if( eNewState == eJS_Ready )
    {
        // Because the user can launch / re-launch at will regardless of
        // the detected state of a plug, just re-enable launch here.
        EnableLaunchBtn->Enabled = true;

        // Disable the action buttons until the enable launch is clicked
        DisableActionBtns( true );

        // Can reset the flag if it has been deployed
        ResetFlagBtn->Enabled = m_mplsMgr->FlagDeployed;

        m_eJobState = eJS_Ready;
    }
    else if( eNewState == eJS_Enabled )
    {
        // When switching to the enabled state, enable the launch
        // buttons of any loaded plugs and start the timeout timer.
        EnableLaunchBtn->Enabled = false;
        ResetFlagBtn->Enabled    = false;

        for( int iCan = 0; iCan < eCN_NbrCanisters; iCan++ )
        {
            eCanisterNbr eCan = (eCanisterNbr)iCan;

            // Can launch of clean if both the job and hardware allow it
            m_canisterControl[iCan].launchBtn->Enabled = m_currJob->CanLaunch[eCan] && m_mplsMgr->CanLaunch[eCan];
            m_canisterControl[iCan].cleanBtn->Enabled  = m_currJob->CanClean[eCan]  && m_mplsMgr->CanClean[eCan];
        }

        // Start out timeout timer
        m_dwEnableBtnClickTime = GetTickCount();

        m_eJobState = eJS_Enabled;
    }
}


void __fastcall TMPLMainForm::LaunchPlugBtnClick(TObject *Sender)
{
    // Can only occur if the button was enabled, and therefore if
    // we have a current job
    TAdvSmoothToggleButton* pBtn = dynamic_cast<TAdvSmoothToggleButton*>( Sender );

    // First find the button in the controls array
    if( pBtn == NULL )
       return;

    for( int iCan = 0; iCan < eCN_NbrCanisters; iCan++ )
    {
        if( m_canisterControl[iCan].launchBtn == pBtn )
        {
            eCanisterNbr eCan = (eCanisterNbr)iCan;

            // Make sure we can launch this plug
            if( m_currJob->CanLaunch[eCan] && m_mplsMgr->CanLaunch[eCan] )
            {
                if( m_mplsMgr->LaunchPlug( eCan ) )
                {
                    // After the click, set the system state to launching
                    SetJobState( eJS_Busy );

                    UpdateJobState( eSE_Launching, time( NULL ), iCan );

                    // Set the button tag to enable animation
                    pBtn->Tag = 0;
                }
                else
                {
                    // Should never happen, but just in case...
                    MessageDlg( "Plug could not be launched.", mtError, TMsgDlgButtons() << mbOK, 0 );

                    AddJobComment( "Plug " + IntToStr( m_currJob->CanisterToPlugNbr( eCan ) ) + " was enabled but could not be launched." );
                }
            }
            else if( m_currJob->CanLaunch[eCan] )
            {
                // Could not launch. Should only happen if the battery level is low.
                TMPLDevice::BatteryLevelType battLevel;

                switch( eCan )
                {
                    case eCanisterNbr::eCN_Master1:
                    case eCanisterNbr::eCN_Master2:
                       battLevel = m_mplsMgr->MPLBatteryLevel[eMU_MasterMPLS];
                       break;

                    default:
                       battLevel = m_mplsMgr->MPLBatteryLevel[eMU_SlaveMPLS];
                       break;
                }

                switch( battLevel )
                {
                    case TMPLDevice::eBL_Med:
                    case TMPLDevice::eBL_OK:
                        // Can launch at these levels. Have another problem.
                        MessageDlg( "You cannot launch this plug at this time.", mtError, TMsgDlgButtons() << mbOK, 0 );
                        break;

                    default:
                        // Battery level bad
                        MessageDlg( "The battery voltage is too low to launch a plug. You must change the battery immediately.",
                                    mtError, TMsgDlgButtons() << mbOK, 0 );
                        break;
                }
            }
            else
            {
                // Fall through to here means the job is not allowing a launch.
                // Should never happen!
                Beep();
            }

            // Can break out of loop after finding button
            break;
        }
    }

    // Refresh display right away
    SyncDisplayToMPLS();
}


void __fastcall TMPLMainForm::CleanPlugBtnClick(TObject *Sender)
{
    // Can only occur if the button was enabled, and therefore if
    // we have a current job
    TAdvSmoothToggleButton* pBtn = dynamic_cast<TAdvSmoothToggleButton*>( Sender );

    // First find the button in the controls array
    if( pBtn == NULL )
       return;

    for( int iCan = 0; iCan < eCN_NbrCanisters; iCan++ )
    {
        if( m_canisterControl[iCan].cleanBtn == pBtn )
        {
            eCanisterNbr eCan = (eCanisterNbr)iCan;

            // Make sure we can launch this plug
            if( m_currJob->CanClean[eCan] && m_mplsMgr->CanClean[eCan] )
            {
                if( m_mplsMgr->CleanCanister( eCan ) )
                {
                    // After the click, set the system state to launching
                    SetJobState( eJS_Busy );

                    UpdateJobState( eSE_Cleaning, time( NULL ), iCan );

                    // Set the button tag to enable animation
                    pBtn->Tag = 0;
                }
                else
                {
                    // Should never happen, but just in case...
                    MessageDlg( "Cleaning cycle could not be started for this canister.", mtError, TMsgDlgButtons() << mbOK, 0 );

                    AddJobComment( "Canister " + IntToStr( m_currJob->CanisterToPlugNbr( eCan ) ) + " was enabled but could not be cleaned." );
                }
            }
            else if( m_currJob->CanClean[eCan] )
            {
                // Could not clean. Should only happen if the battery level is low.
                TMPLDevice::BatteryLevelType battLevel;

                switch( eCan )
                {
                    case eCanisterNbr::eCN_Master1:
                    case eCanisterNbr::eCN_Master2:
                       battLevel = m_mplsMgr->MPLBatteryLevel[eMU_MasterMPLS];
                       break;

                    default:
                       battLevel = m_mplsMgr->MPLBatteryLevel[eMU_SlaveMPLS];
                       break;
                }

                switch( battLevel )
                {
                    case TMPLDevice::eBL_Med:
                    case TMPLDevice::eBL_OK:
                        // Can launch at these levels. Have another problem.
                        MessageDlg( "You cannot clean this canister at this time.", mtError, TMsgDlgButtons() << mbOK, 0 );
                        break;

                    default:
                        // Battery level bad
                        MessageDlg( "The battery voltage is too low to clean the canister. You must change the battery immediately.",
                                    mtError, TMsgDlgButtons() << mbOK, 0 );
                        break;
                }
            }
            else
            {
                // Fall through to here means the job is not allowing a cleaning.
                // Should never happen!
                Beep();
            }

            // Can break out of loop after finding button
            break;
        }
    }

    // Refresh display right away
    SyncDisplayToMPLS();
}


void __fastcall TMPLMainForm::ResetFlagBtnClick(TObject *Sender)
{
    // Can only occur if the button was enabled, and therefore if
    // we have a current job.
    if( m_mplsMgr->DoResetFlag() )
    {
        ResetFlagBtn->Caption = "Resetting";
        ResetFlagBtn->Tag     = 0;

        UpdateJobState( eSE_FlagClearing, time( NULL ), 0 );

        // After the click, set the system state to busy
        SetJobState( eJS_Busy );
    }
    else
    {
        AddJobComment( "Could not start Reset Flag operation" );
    }
}


void TMPLMainForm::RefreshCaption( void )
{
    String mainCaption = "CanRig Multi-Plug Launcher System";

    if( m_mplsMgr != NULL )
    {
        if( m_mplsMgr->UsingMPLSim )
            mainCaption += " (Simulation)";
    }

    if( m_currJob != NULL )
    {
        if( m_currJob->IsTestJob )
        {
            mainCaption = mainCaption + " - Test Mode";
        }
        else
        {
            mainCaption = mainCaption + " - " + m_currJob->WellName + ", " + m_currJob->JobDate;

            if( m_currJob->Completed )
                mainCaption = mainCaption + " (completed)";
        }
    }

    Caption = mainCaption;
}


void TMPLMainForm::SetMainFormDisplay( void )
{
    // Set the visible controls based on the job type. The hard-coded
    // values are based on design-time positioning.
    if( ( m_currJob != NULL ) && ( !m_currJob->IsTestJob ) && ( m_currJob->UpperCanister == eCC_NoPlug ) )
    {
        // In this situation, we only have a master unit
        m_canisterControl[eCN_Master1].launchBtn->Top = 191;
        m_canisterControl[eCN_Master1].cleanBtn->Top  = 191;
        m_canisterControl[eCN_Master2].launchBtn->Top = 107;
        m_canisterControl[eCN_Master2].cleanBtn->Top  = 107;

        m_mplsDisplays[eMU_MasterMPLS].battVoltsPanel->Top = 107;
        m_mplsDisplays[eMU_MasterMPLS].tankPressPanel->Top = 175;
        m_mplsDisplays[eMU_MasterMPLS].regPressPanel->Top  = 243;
        m_mplsDisplays[eMU_MasterMPLS].plugImage->Top      = 107;

        m_canisterControl[eCN_Master1].launchBtn->Visible  = true;
        m_canisterControl[eCN_Master1].cleanBtn->Visible   = true;
        m_canisterControl[eCN_Master2].launchBtn->Visible  = ( m_currJob->LowerCanister == eCC_TwoShort );
        m_canisterControl[eCN_Master2].cleanBtn->Visible   = ( m_currJob->LowerCanister == eCC_TwoShort );

        m_canisterControl[eCN_Slave1].launchBtn->Visible   = false;
        m_canisterControl[eCN_Slave1].cleanBtn->Visible    = false;
        m_canisterControl[eCN_Slave2].launchBtn->Visible   = false;
        m_canisterControl[eCN_Slave2].cleanBtn->Visible    = false;

        m_mplsDisplays[eMU_MasterMPLS].battVoltsPanel->Visible = true;
        m_mplsDisplays[eMU_MasterMPLS].tankPressPanel->Visible = true;
        m_mplsDisplays[eMU_MasterMPLS].regPressPanel->Visible  = true;
        m_mplsDisplays[eMU_MasterMPLS].plugImage->Visible      = true;

        m_mplsDisplays[eMU_SlaveMPLS].battVoltsPanel->Visible  = false;
        m_mplsDisplays[eMU_SlaveMPLS].tankPressPanel->Visible  = false;
        m_mplsDisplays[eMU_SlaveMPLS].regPressPanel->Visible   = false;
        m_mplsDisplays[eMU_SlaveMPLS].plugImage->Visible       = false;
    }
    else
    {
        // In these configurations, there is a master and slave unit.
        // Caution - in this block m_currJob could be NULL!
        m_canisterControl[eCN_Slave1].launchBtn->Top      = 191;
        m_canisterControl[eCN_Slave1].cleanBtn->Top       = 191;
        m_canisterControl[eCN_Slave2].launchBtn->Top      = 107;
        m_canisterControl[eCN_Slave2].cleanBtn->Top       = 107;

        m_mplsDisplays[eMU_SlaveMPLS].battVoltsPanel->Top = 107;
        m_mplsDisplays[eMU_SlaveMPLS].tankPressPanel->Top = 175;
        m_mplsDisplays[eMU_SlaveMPLS].regPressPanel->Top  = 243;
        m_mplsDisplays[eMU_SlaveMPLS].plugImage->Top      = 134;

        m_canisterControl[eCN_Master1].launchBtn->Top     = 439;
        m_canisterControl[eCN_Master1].cleanBtn->Top      = 439;
        m_canisterControl[eCN_Master2].launchBtn->Top     = 355;
        m_canisterControl[eCN_Master2].cleanBtn->Top      = 355;

        m_mplsDisplays[eMU_MasterMPLS].battVoltsPanel->Top = 355;
        m_mplsDisplays[eMU_MasterMPLS].tankPressPanel->Top = 423;
        m_mplsDisplays[eMU_MasterMPLS].regPressPanel->Top  = 491;
        m_mplsDisplays[eMU_MasterMPLS].plugImage->Top      = m_mplsDisplays[eMU_SlaveMPLS].plugImage->Top + m_mplsDisplays[eMU_SlaveMPLS].plugImage->Height;

        m_canisterControl[eCN_Master1].launchBtn->Visible     = true;
        m_canisterControl[eCN_Master1].cleanBtn->Visible      = true;
        m_canisterControl[eCN_Master2].launchBtn->Visible     = ( m_currJob == NULL ) || ( m_currJob->LowerCanister == eCC_TwoShort );
        m_canisterControl[eCN_Master2].cleanBtn->Visible      = ( m_currJob == NULL ) || ( m_currJob->LowerCanister == eCC_TwoShort );

        m_canisterControl[eCN_Slave1].launchBtn->Visible      = true;
        m_canisterControl[eCN_Slave1].cleanBtn->Visible       = true;
        m_canisterControl[eCN_Slave2].launchBtn->Visible      = ( m_currJob == NULL ) || ( m_currJob->UpperCanister == eCC_TwoShort );
        m_canisterControl[eCN_Slave2].cleanBtn->Visible       = ( m_currJob == NULL ) || ( m_currJob->UpperCanister == eCC_TwoShort );

        m_mplsDisplays[eMU_MasterMPLS].battVoltsPanel->Visible = true;
        m_mplsDisplays[eMU_MasterMPLS].tankPressPanel->Visible = true;
        m_mplsDisplays[eMU_MasterMPLS].regPressPanel->Visible  = true;
        m_mplsDisplays[eMU_MasterMPLS].plugImage->Visible      = true;

        m_mplsDisplays[eMU_SlaveMPLS].battVoltsPanel->Visible  = true;
        m_mplsDisplays[eMU_SlaveMPLS].tankPressPanel->Visible  = true;
        m_mplsDisplays[eMU_SlaveMPLS].regPressPanel->Visible   = true;
        m_mplsDisplays[eMU_SlaveMPLS].plugImage->Visible       = true;
    }

    // If either the upper or lower canister only has a single plug
    // (eg, the second plug controls are not visible), adjust the
    // height of the single plug buttons
    if( m_canisterControl[eCN_Slave1].launchBtn->Visible && !m_canisterControl[eCN_Slave2].launchBtn->Visible )
    {
        m_canisterControl[eCN_Slave1].launchBtn->Top = m_mplsDisplays[eMU_SlaveMPLS].tankPressPanel->Top;
        m_canisterControl[eCN_Slave1].cleanBtn->Top  = m_mplsDisplays[eMU_SlaveMPLS].tankPressPanel->Top;
    }

    if( m_canisterControl[eCN_Master1].launchBtn->Visible && !m_canisterControl[eCN_Master2].launchBtn->Visible )
    {
        m_canisterControl[eCN_Master1].launchBtn->Top = m_mplsDisplays[eMU_MasterMPLS].tankPressPanel->Top;
        m_canisterControl[eCN_Master1].cleanBtn->Top  = m_mplsDisplays[eMU_MasterMPLS].tankPressPanel->Top;
    }

    // Lastly, update the captions to default values for all buttons
    for( int iCanister = 0; iCanister < eCN_NbrCanisters; iCanister++ )
    {
        if( m_currJob == NULL )
        {
            m_canisterControl[iCanister].launchBtn->Caption = "Launch " + IntToStr( iCanister + 1 );
            m_canisterControl[iCanister].cleanBtn->Caption  = "Clean "  + IntToStr( iCanister + 1 );
        }
        else
        {
            eCanisterNbr eCan = (eCanisterNbr)iCanister;

            String sPlugNbr = IntToStr( m_currJob->CanisterToPlugNbr( eCan ) );

            if( m_currJob->LaunchTime[eCan] == 0 )
                m_canisterControl[iCanister].launchBtn->Caption = "Launch " + sPlugNbr;
            else
                m_canisterControl[iCanister].launchBtn->Caption = "Re-Launch " + sPlugNbr;

            m_canisterControl[iCanister].cleanBtn->Caption  = "Clean " + sPlugNbr;
        }
    }
}


void TMPLMainForm::UpdateStatusScanning( void )
{
    // This method is called when the comms manager is scanning for either or
    // both the Base Radio and MPL Device.
    UpdatePortStatusBoxes();

    SetSmoothPanelCaption( TempStatusPanel,  EmptyCaption );
    SetSmoothPanelCaption( RPMStatusPanel,   EmptyCaption );
    SetSmoothPanelCaption( FlagStatusPanel,  EmptyCaption );
    SetSmoothPanelCaption( RSSIStatusPanel,  EmptyCaption );

    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        SetSmoothPanelCaption( m_mplsDisplays[iUnit].tankPressPanel, EmptyCaption );
        SetSmoothPanelCaption( m_mplsDisplays[iUnit].regPressPanel,  EmptyCaption );
        SetSmoothPanelCaption( m_mplsDisplays[iUnit].battVoltsPanel, EmptyCaption );
    }
}


void TMPLMainForm::UpdatePortStatusBoxes( void )
{
    String BROpenModeStr;

    if( GetAutoAssignCommPorts() )
        BROpenModeStr = "Scanning for Radio";
    else
        BROpenModeStr = "Opening Radio Port";

    switch( m_mplsMgr->PollerState )
    {
        case TMPLSManager::PS_INITIALIZING:
            SetSmoothPanelCaption( BaseRadioStatusPanel, "Initializing" );
            SetSmoothPanelCaption( MPLLinkStatusPanel,   EmptyCaption );
            BaseRadioProgBar->Visible = false;
            MPLLinkProgBar->Visible   = false;
            break;

        case TMPLSManager::PS_BR_PORT_SCAN:
            SetSmoothPanelCaption( BaseRadioStatusPanel, BROpenModeStr );
            SetSmoothPanelCaption( MPLLinkStatusPanel,   EmptyCaption );
            BaseRadioProgBar->Visible = true;
            MPLLinkProgBar->Visible   = false;
            break;

        case TMPLSManager::PS_MPL_PORT_SCAN:
            SetSmoothPanelCaption( BaseRadioStatusPanel, m_mplsMgr->BaseRadioStatus );
            SetSmoothPanelCaption( MPLLinkStatusPanel,   EmptyCaption );
            BaseRadioProgBar->Visible = false;
            MPLLinkProgBar->Visible   = true;
            break;

        case TMPLSManager::PS_MPL_CHAN_SCAN:
            SetSmoothPanelCaption( BaseRadioStatusPanel, m_mplsMgr->BaseRadioStatus );
            SetSmoothPanelCaption( MPLLinkStatusPanel,   m_mplsMgr->MPLPortDesc );
            BaseRadioProgBar->Visible = false;
            MPLLinkProgBar->Visible   = true;
            break;
    }
}


void TMPLMainForm::UpdateStatusValues( void )
{
    LOGGED_RFC_DATA lastPkt;
    TDateTime       pktTime;
    DWORD           rfcCount;

    SetSmoothPanelCaption( BaseRadioStatusPanel, m_mplsMgr->BaseRadioStatus );
    SetSmoothPanelCaption( MPLLinkStatusPanel,   m_mplsMgr->MPLPortDesc );
    SetSmoothPanelCaption( RSSIStatusPanel,      IntToStr( m_mplsMgr->MPLRSSI ) + "%" );

    // If we are getting status messages, then certainly hide the base radio progress bar.
    // For the MPL, its progress bar is not visible only if the MPL is idle (which really
    // means its running but not doing a config upload/download).
    BaseRadioProgBar->Visible = false;
    MPLLinkProgBar->Visible   = m_mplsMgr->MPLIsIdle ? false : true;

    if( m_mplsMgr->GetLastStatusPkt( pktTime, rfcCount, lastPkt ) )
    {
        SetSmoothPanelCaption( TempStatusPanel, FloatToStrF( lastPkt.temp, ffFixed, 7, 1 ) );
        SetSmoothPanelCaption( FlagStatusPanel, lastPkt.flagInActive ? String( "Tripped" ) : String( "Reset" ) );
    }
    else
    {
        SetSmoothPanelCaption( TempStatusPanel,  EmptyCaption );
        SetSmoothPanelCaption( FlagStatusPanel,  EmptyCaption );
    }

    // Get averages
    TMPLSManager::AVG_READINGS avgReadings;

    if( m_mplsMgr->GetAvgReadings( avgReadings ) )
    {
        SetSmoothPanelCaption( RPMStatusPanel, FloatToMPLSString( avgReadings.fRPM ) );

        for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
        {
            // TODO - if metric units are eventually supported, change hard-coding of units text appended to value
            SetSmoothPanelCaption( m_mplsDisplays[iUnit].tankPressPanel, FloatToMPLSString( avgReadings.fTankPress[iUnit] ) + " PSI" );
            SetSmoothPanelCaption( m_mplsDisplays[iUnit].regPressPanel,  FloatToMPLSString( avgReadings.fRegPress[iUnit]  ) + " PSI" );
        }
    }
    else
    {
        SetSmoothPanelCaption( RPMStatusPanel, EmptyCaption );

        for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
        {
            SetSmoothPanelCaption( m_mplsDisplays[iUnit].tankPressPanel, EmptyCaption );
            SetSmoothPanelCaption( m_mplsDisplays[iUnit].regPressPanel,  EmptyCaption );
        }
    }

    // Show battery info separately, as it will give averaged values (lastPkt
    // shows instantaneous value)
    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        eMPLSUnit eUnit = (eMPLSUnit)iUnit;

        BATTERY_INFO battInfo;

        if( m_mplsMgr->GetDeviceBatteryInfo( eUnit, battInfo ) )
        {
            SetSmoothPanelCaption( m_mplsDisplays[iUnit].battVoltsPanel, FloatToStrF( battInfo.avgVolts, ffFixed, 7, 2 ) );

            // Update the image - make sure the battery level is a known enum
            switch( m_mplsMgr->MPLBatteryLevel[eUnit] )
            {
                case TMPLDevice::eBL_Med:
                case TMPLDevice::eBL_OK:
                case TMPLDevice::eBL_Low:
                    m_mplsDisplays[iUnit].battVoltsPanel->Fill->Picture->LoadFromResourceName( (int)HInstance, BattLevelResNames[m_mplsMgr->MPLBatteryLevel[eUnit]] );
                    break;

                default:
                    m_mplsDisplays[iUnit].battVoltsPanel->Fill->Picture->LoadFromResourceName( (int)HInstance, BattLevelResNames[TMPLDevice::eBL_Unknown] );
                    break;
            }
        }
        else
        {
            SetSmoothPanelCaption( m_mplsDisplays[iUnit].battVoltsPanel, EmptyCaption );

            m_mplsDisplays[iUnit].battVoltsPanel->Fill->Picture->LoadFromResourceName( (int)HInstance, BattLevelResNames[TMPLDevice::eBL_Unknown] );
        }
    }
}


void TMPLMainForm::SetSmoothPanelCaption( TAdvSmoothPanel* pPanel, const String& sText )
{
    pPanel->Caption->HTMLText = "<FONT size=\"11\" face=\"Tahoma\">" + sText + "</FONT>";
}


void TMPLMainForm::AnimateActionBtn( TAdvSmoothToggleButton* pBtn )
{
    // Animate the colour. The button's Tag property stores the current
    // color index. If that index is -1, animation is starting.
    int iNextStep = pBtn->Tag + 1;

    if( ( iNextStep < 0 ) || ( iNextStep >= NBR_CLEANING_ANIMATION_STEPS ) )
        iNextStep = 0;

    pBtn->Tag           = iNextStep;  // Which now is actually the current step
    pBtn->Color         = clCleaning[iNextStep];
    pBtn->ColorDisabled = clCleaning[iNextStep];
}


void TMPLMainForm::DisableActionBtns( bool bStopAnimations )
{
    // Disables the ability to launch or clean plugs
    for( int iCanister = 0; iCanister < eCN_NbrCanisters; iCanister++ )
    {
        ResetActionBtn( m_canisterControl[iCanister].launchBtn, bStopAnimations );
        ResetActionBtn( m_canisterControl[iCanister].cleanBtn,  bStopAnimations );
    }

    // Also disable ability to reset the flag
    ResetActionBtn( ResetFlagBtn, bStopAnimations );
}


void __fastcall TMPLMainForm::SysSettingsBtnClick(TObject *Sender)
{
    String newPassword;

    if( !TPasswordInputForm::CheckPassword( PasswordDlgCaption, GetSysAdminPassword(), newPassword ) )
        return;

    if( newPassword != "" )
        SaveSysAdminPassword( newPassword );

    // Show the dlg, record if important settings changed
    DWORD dwSettingsChanged = TSystemSettingsForm::ShowDlg( m_mplsMgr, m_currJob );

    // Get current job unit type and then update the comms manager
    if( m_currJob != NULL )
         m_mplsMgr->RefreshSettings( m_currJob->Units );
    else
         m_mplsMgr->RefreshSettings( GetDefaultUnitsOfMeasure() );

    // Refresh operating params
    m_dwLaunchClickTimeout = GetLaunchClickTimeout();

    // Reload the poller if the comms or misc settings were changed
    if( dwSettingsChanged & TSystemSettingsForm::COMMS_SETTINGS_CHANGED )
    {
        // Recreate the new manager
        delete m_mplsMgr;

        m_mplsMgr = new TMPLSManager( Handle, WM_POLLER_EVENT );

        // Check for a change in simulation mode. Ignore this in test mode
        if( ( m_currJob != NULL ) && !m_currJob->IsTestJob )
        {
            if( m_mplsMgr->UsingMPLSim )
            {
                // In sim mode. If the job isn't, then warn the user about changes being lost
                if( m_currJob->InSimMode == false )
                {
                    // We are now in sim mode and the currently open job is not in sim mode. Warn the user
                    MessageDlg( "You have entered Simulation Mode. Any changes made to the currently open job "
                                "will not be saved.", mtWarning, TMsgDlgButtons() << mbOK, 0 );
                }
            }
            else
            {
                // Not in sim mode. If the current job is, warn the user and close the job
                if( m_currJob != NULL )
                {
                    if( m_currJob->InSimMode )
                    {
                        MessageDlg( "You have left Simulation Mode. The current job must be closed and any changes made to it will be lost. "
                                    "Press OK to continue.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
                    }
                }
            }

            EnterSimMode( m_mplsMgr->UsingMPLSim );
        }
    }
}


void TMPLMainForm::EnterSimMode( bool bIsSimulating )
{
    // If we want to do any extra handling/captions, do it here
    if( m_currJob == NULL )
        return;

    // First, clear sim mode if we're ignoring it
    if( m_bIgnoreSimMode )
    {
        m_currJob->InSimMode = false;
        return;
    }

    // Can only put a job into sim mode; can't clear that flag
    if( bIsSimulating && !m_currJob->Completed )
        m_currJob->InSimMode = true;
}


void TMPLMainForm::SyncJobToMPLS( time_t tWhen )
{
    // This function is called after we have a job loaded and we're
    // now connected to a MPLS. The only thing we can really check
    // is if the MPLS is a master, or master/slave system.

    // First, need not do anything unless we're in the hardware
    // wait state. This also causes an early exit if we're in
    // the disabled state.
    if( m_eJobState != eJS_HWWait )
        return;

    // Fall through can only happen if we have a job, but check just in case.
    // Also, no sync'ing required for a completed job
    if( ( m_currJob == NULL ) || m_currJob->Completed )
    {
        SyncDisplayToMPLS();
        return;
    }

    // Now do two checks. First, have the job record what MPLS we're
    // connected to and the hardware info.
    MPLS_SYSTEM_CONFIGURATION sysConfig;

    for( int iComp = 0; iComp < eMC_NbrMPLSComps; iComp++ )
    {
        MPLS_COMP_ID hwInfo;
        bool bPresent;

        if( m_mplsMgr->GetDeviceHWInfo( (eMPLSComponent)iComp, bPresent, hwInfo ) )
        {
            sysConfig.compInfo[iComp].bPresent = bPresent;
            sysConfig.compInfo[iComp].compID   = hwInfo;
        }
        else
        {
            sysConfig.compInfo[iComp].bPresent = false;
            sysConfig.compInfo[iComp].compID.sSN     = "";
            sysConfig.compInfo[iComp].compID.dwHWRev = 0;
            sysConfig.compInfo[iComp].compID.dwFWRev = 0;
        }
    }

    for( int iUnit = 0; iUnit < eMU_NbrMPLSUnits; iUnit++ )
    {
        eMPLSUnit eUnit = (eMPLSUnit)iUnit;

        sysConfig.iActiveSol[eUnit] = m_mplsMgr->ActiveSolenoid[eUnit];
    }

    // Create a device info rec for this connection
    MPLS_INFO_REC infoRec;
    infoRec.entryTime = tWhen;

    infoRec.uirMasterTec.bPresent   = sysConfig.compInfo[eMC_MasterTEC].bPresent;
    infoRec.uirMasterTec.devSN      = sysConfig.compInfo[eMC_MasterTEC].compID.sSN;
    infoRec.uirMasterTec.devFWRev   = UIntToStr( (UINT)sysConfig.compInfo[eMC_MasterTEC].compID.dwFWRev );
    infoRec.uirMasterTec.devHWRev   = UIntToStr( (UINT)sysConfig.compInfo[eMC_MasterTEC].compID.dwHWRev );
    infoRec.uirMasterTec.pibPresent = sysConfig.compInfo[eMC_MasterPIB].bPresent;
    infoRec.uirMasterTec.pibFWRev   = UIntToStr( (UINT)sysConfig.compInfo[eMC_MasterPIB].compID.dwFWRev );
    infoRec.uirMasterTec.pibHWRev   = UIntToStr( (UINT)sysConfig.compInfo[eMC_MasterPIB].compID.dwHWRev );
    infoRec.uirMasterTec.battType   = GetBattTypeText( m_mplsMgr->MPLBatteryType[eMU_MasterMPLS] );

    m_mplsMgr->GetCalDataFormatted( eMU_MasterMPLS, TMPLDevice::CI_MFG_INFO,          infoRec.uirMasterTec.mfgInfo );
    m_mplsMgr->GetCalDataFormatted( eMU_MasterMPLS, TMPLDevice::CI_MFG_DATE,          infoRec.uirMasterTec.mfgDate );
    m_mplsMgr->GetCalDataFormatted( eMU_MasterMPLS, TMPLDevice::CI_CAL_DATE,          infoRec.uirMasterTec.lastCalDate );
    m_mplsMgr->GetCalDataFormatted( eMU_MasterMPLS, TMPLDevice::CI_CAL_VER,           infoRec.uirMasterTec.calVersion );
    m_mplsMgr->GetCalDataFormatted( eMU_MasterMPLS, TMPLDevice::CI_CAL_REV,           infoRec.uirMasterTec.calRev );
    m_mplsMgr->GetCalDataFormatted( eMU_MasterMPLS, TMPLDevice::CI_TANK_PRESS_OFFSET, infoRec.uirMasterTec.tankPressOffset );
    m_mplsMgr->GetCalDataFormatted( eMU_MasterMPLS, TMPLDevice::CI_TANK_PRESS_SPAN,   infoRec.uirMasterTec.tankPressSpan );
    m_mplsMgr->GetCalDataFormatted( eMU_MasterMPLS, TMPLDevice::CI_REG_PRESS_OFFSET,  infoRec.uirMasterTec.regPressOffset );
    m_mplsMgr->GetCalDataFormatted( eMU_MasterMPLS, TMPLDevice::CI_REG_PRESS_SPAN,    infoRec.uirMasterTec.regPressSpan );

    infoRec.uirSlaveTec.bPresent   = sysConfig.compInfo[eMC_SlaveTEC].bPresent;
    infoRec.uirSlaveTec.devSN      = sysConfig.compInfo[eMC_SlaveTEC].compID.sSN;
    infoRec.uirSlaveTec.devFWRev   = UIntToStr( (UINT)sysConfig.compInfo[eMC_SlaveTEC].compID.dwFWRev );
    infoRec.uirSlaveTec.devHWRev   = UIntToStr( (UINT)sysConfig.compInfo[eMC_SlaveTEC].compID.dwHWRev );
    infoRec.uirSlaveTec.pibPresent = sysConfig.compInfo[eMC_SlavePIB].bPresent;
    infoRec.uirSlaveTec.pibFWRev   = UIntToStr( (UINT)sysConfig.compInfo[eMC_SlavePIB].compID.dwFWRev );
    infoRec.uirSlaveTec.pibHWRev   = UIntToStr( (UINT)sysConfig.compInfo[eMC_SlavePIB].compID.dwHWRev );
    infoRec.uirSlaveTec.battType   = GetBattTypeText( m_mplsMgr->MPLBatteryType[eMU_SlaveMPLS] );

    m_mplsMgr->GetCalDataFormatted( eMU_SlaveMPLS, TMPLDevice::CI_MFG_INFO,          infoRec.uirSlaveTec.mfgInfo );
    m_mplsMgr->GetCalDataFormatted( eMU_SlaveMPLS, TMPLDevice::CI_MFG_DATE,          infoRec.uirSlaveTec.mfgDate );
    m_mplsMgr->GetCalDataFormatted( eMU_SlaveMPLS, TMPLDevice::CI_CAL_DATE,          infoRec.uirSlaveTec.lastCalDate );
    m_mplsMgr->GetCalDataFormatted( eMU_SlaveMPLS, TMPLDevice::CI_CAL_VER,           infoRec.uirSlaveTec.calVersion );
    m_mplsMgr->GetCalDataFormatted( eMU_SlaveMPLS, TMPLDevice::CI_CAL_REV,           infoRec.uirSlaveTec.calRev );
    m_mplsMgr->GetCalDataFormatted( eMU_SlaveMPLS, TMPLDevice::CI_TANK_PRESS_OFFSET, infoRec.uirSlaveTec.tankPressOffset );
    m_mplsMgr->GetCalDataFormatted( eMU_SlaveMPLS, TMPLDevice::CI_TANK_PRESS_SPAN,   infoRec.uirSlaveTec.tankPressSpan );
    m_mplsMgr->GetCalDataFormatted( eMU_SlaveMPLS, TMPLDevice::CI_REG_PRESS_OFFSET,  infoRec.uirSlaveTec.regPressOffset );
    m_mplsMgr->GetCalDataFormatted( eMU_SlaveMPLS, TMPLDevice::CI_REG_PRESS_SPAN,    infoRec.uirSlaveTec.regPressSpan );

    // Only a subset of the device info is used for the plug d.devtor
    infoRec.uirPlugDet.bPresent  = sysConfig.compInfo[eMC_PlugDet].bPresent;
    infoRec.uirPlugDet.devSN     = sysConfig.compInfo[eMC_PlugDet].compID.sSN;
    infoRec.uirPlugDet.devFWRev  = UIntToStr( (UINT)sysConfig.compInfo[eMC_PlugDet].compID.dwFWRev );
    infoRec.uirPlugDet.devHWRev  = UIntToStr( (UINT)sysConfig.compInfo[eMC_PlugDet].compID.dwHWRev );

    m_currJob->AddMPLSInfoRec( infoRec );

    // SyncToHardware() will return error bits for all 'bad things'.
    // Pop up alerts for each.
    DWORD dwErrorBits = m_currJob->SyncToHardware( sysConfig, tWhen );

    if( dwErrorBits & STH_ERR_SAVE_FAILED )
    {
        // Only display this error once per job
        if( m_bShowFileSaveError )
        {
            DisplayEvent( eAET_FileSaveError, true, true );

            m_bShowFileSaveError = false;
        }
    }

    DWORD dwCfgMismatchBits = STH_ERR_MASTER_PIB_MISSING | STH_ERR_SLAVE_PRESENT | STH_ERR_SLAVE_TEC_MISSING | STH_ERR_SLAVE_PIB_MISSING | STH_ERR_PLUG_DET_MISSING;

    if( dwErrorBits & dwCfgMismatchBits )
        DisplayEvent( eAET_ConfigMismatch, true, true );

    DWORD dwStateMismatchBits = STH_ERR_UNEXP_CLEAN_ACTIVE | STH_ERR_UNEXP_LAUNCH_ACTIVE | STH_ERR_ILLEGAL_CLEAN_ACTIVE | STH_ERR_ILLEGAL_LAUNCH_ACTIVE;

    if( dwErrorBits & dwStateMismatchBits )
        DisplayEvent( eAET_StateMismatch, true, true );

    if( dwErrorBits & ( STH_WARN_MASTER_TEC_CHANGED | STH_WARN_SLAVE_TEC_CHANGED ) )
        DisplayEvent( eAET_ConfigChange, false, false );

    // Set job state. It will either be ready or busy, depending
    // on if a cycle can start
    if( m_mplsMgr->CanStartCycle )
        SetJobState( eJS_Ready );
    else
        SetJobState( eJS_Busy );

    // Reset the state of all alarms
    m_mplsMgr->ResetAlarms();

    // Now make sure the display is up to date
    SyncDisplayToMPLS();
}


void TMPLMainForm::SyncDisplayToMPLS( void )
{
    // Get state of all outputs from MPLS Mgr and set state of buttons
    switch( m_eJobState )
    {
        case eJS_Loading:
        case eJS_Disabled:
            // Don't update controls in these states
            return;

        default:
            // In any other states, we t update controls
            break;
    }

    // Update the state and hints for all controls. We must have a
    // current job to have gotten here
    for( int iCan = 0; iCan < eCN_NbrCanisters; iCan++ )
    {
        eCanisterNbr eCan = (eCanisterNbr)iCan;

        String sPlug = IntToStr( m_currJob->CanisterToPlugNbr( eCan ) );

        switch( m_currJob->PlugState[eCan] )
        {
            case ePS_Loaded:

                m_canisterControl[iCan].launchBtn->Caption = "Launch " + sPlug;
                m_canisterControl[iCan].cleanBtn->Caption  = "Clean "  + sPlug;

                m_canisterControl[iCan].launchBtn->Hint = "Ready to launch";
                m_canisterControl[iCan].cleanBtn->Hint  = "Cannot clean";

                break;

            case ePS_Launched:

                // A plug is in the launched state as soon as the launching starts.
                if( m_mplsMgr->Launching[eCan] )
                    m_canisterControl[iCan].launchBtn->Caption = "Launching " + sPlug;
                else
                    m_canisterControl[iCan].launchBtn->Caption = "Re-Launch " + sPlug;

                if( m_mplsMgr->Cleaning[eCan] )
                    m_canisterControl[iCan].cleanBtn->Caption = "Cleaning "  + sPlug;
                else
                    m_canisterControl[iCan].cleanBtn->Caption = "Clean "  + sPlug;

                m_canisterControl[iCan].launchBtn->Hint = "Launched at " + TBasePlugJob::JobTimeToLocalString( m_currJob->LaunchTime[eCan], false );

                if( m_currJob->CleanTime[eCan] == 0 )
                    m_canisterControl[iCan].cleanBtn->Hint  = "Ready for cleaning";
                else
                    m_canisterControl[iCan].cleanBtn->Hint  = "Cleaned at " + TBasePlugJob::JobTimeToLocalString( m_currJob->CleanTime[eCan], false );
                break;

            default:
                break;
        }
    }

    // Update graphics as well
    UpdateImageGraphic( m_mplsDisplays[eMU_MasterMPLS].plugImage, m_currJob->LowerCanister, m_currJob->PlugState[eCN_Master1], m_currJob->PlugState[eCN_Master2] );
    UpdateImageGraphic( m_mplsDisplays[eMU_SlaveMPLS ].plugImage, m_currJob->UpperCanister, m_currJob->PlugState[eCN_Slave1 ], m_currJob->PlugState[eCN_Slave2] );
}


void TMPLMainForm::UpdateImageGraphic( TImage* pImage, eCanisterConfig eCanCfg, ePlugState eStatePlug1, ePlugState eStatePlug2 )
{
    if( !pImage->Visible )
        return;

    eCanisterImage eNewImage = eCI_CanEmpty;

    switch( eCanCfg )
    {
        case eCC_OneLong:
            if( eStatePlug1 == ePS_Loaded )
                eNewImage = eCI_Loaded1Long;
            break;

        case eCC_OneShort:
            if( eStatePlug1 == ePS_Loaded )
                eNewImage = eCI_Loaded1Short;
            break;

        case eCC_TwoShort:
            if( ( eStatePlug1 == ePS_Loaded ) && ( eStatePlug2 == ePS_Loaded ) )
                eNewImage = eCI_Loaded2Short;
            else if( ( eStatePlug1 == ePS_Launched ) && ( eStatePlug2 == ePS_Loaded ) )
                eNewImage = eCI_LowerDropped;
            break;

        default:
            eNewImage = eCI_CanEmpty;
            break;
    }

    eCanisterImage eCurrImage = (eCanisterImage)pImage->Tag;

    if( eNewImage != eCurrImage )
    {
        pImage->Picture->Bitmap = NULL;

        CanisterImages->GetBitmap( (int)eNewImage, pImage->Picture->Bitmap );

        pImage->Tag = (int)eNewImage;
    }
}


void TMPLMainForm::UpdateJobState( eSystemEvent eWhichEvent, time_t tWhen, int iInfo )
{
    // A wrapper around the job UpdateState() method that will post alarms if necessary.
    DWORD dwUpdateResult = m_currJob->UpdateState( eWhichEvent, tWhen, iInfo );

    if( dwUpdateResult & US_ERR_SAVE_FAILED )
        DisplayEvent( eAET_FileSaveError, true, true );

    if( dwUpdateResult & US_ERR_ILLEGAL_CLEAN )
        DisplayEvent( eAET_BadCleanCan, true, true );

    if( dwUpdateResult & US_ERR_ILLEGAL_LAUNCH )
        DisplayEvent( eAET_BadLaunchPlug, true, true );

    if( dwUpdateResult & US_ERR_HARDWARE_FAULT )
        DisplayEvent( eAET_HardwareErrors, true, true );
}


void TMPLMainForm::LogRFCData( void )
{
    // Log any pending RFC recs. Only do this if we have a job and the
    // job is not complete
    if( ( m_currJob == NULL ) || m_currJob->Completed )
    {
        m_mplsMgr->ClearRFCData();
        return;
    }

    LOGGED_RFC_DATA rfcData;

    while( m_mplsMgr->GetRFCData( rfcData ) )
        m_currJob->AddRFCRecord( rfcData );

    // Tell graph to update on receipt of RFC(s)
    GraphForm->LoadNewData();
}


void TMPLMainForm::ProcessLaunchDone( eCanisterNbr eWhichPlug, time_t tWhen, int iMaxSolCurr )
{
    // Plug launch is complete. Do a little validation first.
    if( ( m_currJob == NULL ) || m_currJob->Completed )
        return;

    // Update the controls. CanisterToPlugNbr() returns zero for
    // a plug that's invalid for this job. Also note that we don't
    // need to add any job comments here as the call to UpdateState()
    // will do all that for us.
    int iLaunchedPlug = m_currJob->CanisterToPlugNbr( eWhichPlug );

    switch( iLaunchedPlug )
    {
        case 1:
        case 2:
        case 3:
        case 4:
            m_canisterControl[eWhichPlug].launchBtn->Caption = "Re-Launch " + IntToStr( iLaunchedPlug );

            // Reset the colour as the button could have been in any state of animation
            ResetActionBtn( m_canisterControl[eWhichPlug].launchBtn );

            break;

        default:
            // This should never happen!
            DisplayEvent( eAET_BadLaunchPlug, true, false );
            break;
    }

    // Log this event to the job
    UpdateJobState( eSE_LaunchDone, tWhen, eWhichPlug );

    // Manually post a comment for max sol current
    AddJobComment( "Max solenoid current: " + IntToStr( iMaxSolCurr ) + " mA" );

    // Now sync the display controls to the actual system
    SyncDisplayToMPLS();
}


void TMPLMainForm::ProcessPlugDetected( time_t tWhen )
{
    // A plug has been detected. First, clear the indication
    // from the hardware
    m_mplsMgr->ClearPlugDetected();

    // Do a little validation
    if( ( m_currJob == NULL ) || m_currJob->Completed )
        return;

/*
    The current version of the MPLS Manager does not do plug detection.
    Code is left here for future reference in case automatic plug
    detection is re-implemented.

    int iDetectedPlug = m_currJob->CanisterToPlugNbr( eWhichPlug );

    switch( iDetectedPlug )
    {
        case 1:
        case 2:
        case 3:
        case 4:
            // Don't need to update any buttons on plug detect
            break;

        default:
            // This should never happen!
            DisplayEvent( eAET_OrphanDetected, true, false );
            break;
    }

    // Log this event to the job
    UpdateJobState( eSE_PlugDetected, tWhen, eWhichPlug );

    // Now sync the display controls with the job
    SyncDisplayToJob();
*/
}


void TMPLMainForm::ProcessCanisterCleaned( eCanisterNbr eWhichCan, time_t tEvent, int iMaxSolCurr )
{
    // Plug launch is complete. Do a little validation first.
    if( ( m_currJob == NULL ) || m_currJob->Completed )
        return;

    // Update the controls. CanisterToPlugNbr() returns zero for
    // a plug that's invalid for this job. Also note that we don't
    // need to add any job comments here as the call to UpdateState()
    // will do all that for us.
    int iCleanedPlug = m_currJob->CanisterToPlugNbr( eWhichCan );

    switch( iCleanedPlug )
    {
        case 1:
        case 2:
        case 3:
        case 4:
            m_canisterControl[eWhichCan].launchBtn->Caption = "Clean " + IntToStr( iCleanedPlug );

            // Reset the colour as the button could have been in any state of animation
            ResetActionBtn( m_canisterControl[eWhichCan].launchBtn );

            break;

        default:
            // This should never happen!
            DisplayEvent( eAET_BadCleanCan, true, false );
            break;
    }

    // Log this event to the job
    UpdateJobState( eSE_CleanDone, tEvent, eWhichCan );

    // Manually post a comment for max sol current
    AddJobComment( "Max solenoid current: " + IntToStr( iMaxSolCurr ) + " mA" );

    // Now sync the display controls to the actual system
    SyncDisplayToMPLS();
}


void TMPLMainForm::ProcessResetFlagDone( time_t tEvent, int iMaxSolCurr )
{
    // Reset solenoid has completed its cycle
    if( ( m_currJob == NULL ) || m_currJob->Completed )
        return;

    ResetActionBtn( ResetFlagBtn );
    ResetFlagBtn->Caption = "Reset";

    // Log a system event, this will also post a job comment
    UpdateJobState( eSE_FlagCleared, tEvent, 0 );

    // Manually post a comment for max sol current
    AddJobComment( "Max solenoid current: " + IntToStr( iMaxSolCurr ) + " mA" );

    // Now sync the display controls to the actual system
    SyncDisplayToMPLS();
}


void TMPLMainForm::ProcessFlagDeployed( time_t tEvent )
{
    // The flag has been tripped
    if( ( m_currJob == NULL ) || m_currJob->Completed )
        return;

    // Enable the button if we're in the ready state
    if( m_eJobState == eJS_Ready )
        ResetFlagBtn->Enabled = true;

    // Log a system event, this will also post a job comment
    UpdateJobState( eSE_FlagTriggered, tEvent, 0 );

    // Now sync the display controls to the actual system
    SyncDisplayToMPLS();
}


void TMPLMainForm::ProcessAlarm( TMPLSManager::AlarmType eAlarmType, time_t tAlarm )
{
    // Map the alarm type to its corresponding system event, then update the job state
    // and display the event
    switch( eAlarmType )
    {
        case TMPLSManager::eAT_LowBattery:
            UpdateJobState( eSE_LowBattery, tAlarm, 0 );
            DisplayEvent( eAET_LowBattery, true, true );
            break;

        default:
            UpdateJobState( eSE_OtherAlarm, tAlarm, (int)eAlarmType );
            DisplayEvent( (AckEventType)97, true, true );
            break;
    }
}


void TMPLMainForm::ProcessHWCommsErrors( DWORD dwErrorFlags )
{
    // Called when one or more components are reporting errors
    // on the wired backbone of the system. dwErrorFlags
    // contains bit-masks of the component(s) reporting errors.
    String sBadComp;

    for( int iComp = 0; iComp < eMC_NbrMPLSComps; iComp++ )
    {
        if( ( dwErrorFlags & ( 1 << iComp ) ) != 0 )
        {
            String sComp;
            
            switch( iComp )
            {
                case eMC_MasterTEC:  sComp = "Master TEC";      break;
                case eMC_MasterPIB:  sComp = "Master PIB";      break;
                case eMC_SlaveTEC:   sComp = "Slave TEC";       break;
                case eMC_SlavePIB:   sComp = "Slave PIB";       break;
                case eMC_PlugDet:    sComp = "Plug Detector";   break;
            }

            if( sComp.Length() > 0 )
            {
                if( sBadComp.IsEmpty() )
                    sBadComp = sComp;
                else
                   sBadComp = sBadComp + ", " + sComp;
            }
        }
    }

    if( sBadComp.Length() > 0 )
    {
        AddJobComment( "Hardware errors being reported: " + sBadComp );

        // Treat this event as an alarm
        DisplayEvent( eAET_HardwareErrors, true, true );
    }
}


void TMPLMainForm::DisplayEvent( AckEventType aetType, bool bIsAlarm, bool bActivateAlertOutput )
{
    if( ( m_currJob != NULL ) && !m_currJob->IsTestJob )
        AckForm->DisplayEvent( aetType, bIsAlarm );

    if( bActivateAlertOutput && ( m_mplsMgr != NULL ) )
        m_mplsMgr->AssertAlertOutput();
}


void TMPLMainForm::AddJobComment( const String& sComment )
{
    if( m_currJob != NULL )
        m_currJob->AddJobComment( sComment );
}


//
// Windows Message Handlers
//

void __fastcall TMPLMainForm::WMCompleteJob( TMessage &Message )
{
    // Sets the current job's status to completed
    if( m_currJob->Completed || m_currJob->IsTestJob )
        return;

    int mrResult = MessageDlg( "You are about to mark this job as completed. You will not be able to make any more changes to the job file."
                               "\r  Click Yes to complete this job.\r  Click Cancel to leave this job open.",
                               mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    if( mrResult != mrYes )
        return;

    // Mark the job as complete. This causes the job to be saved too.
    m_currJob->Completed = true;

    // Set the job state
    SetJobState( eJS_Complete );

    RefreshCaption();
}


void __fastcall TMPLMainForm::WMReopenJob( TMessage &Message )
{
    // Sets the current job's status to in-progress (or open)
    if( !m_currJob->Completed || m_currJob->IsTestJob )
        return;

    if( m_currJob->Completed == false )
        return;

    int mrResult = MessageDlg( "This job has been completed. If you re-open it, changes will be allowed to the data."
                               "\r  Click Yes to proceed and re-open this job.\r  Click Cancel to leave this job closed.",
                               mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    if( mrResult != mrYes )
        return;

    m_currJob->Completed = false;

    // Set job state
    SetJobState( m_mplsMgr->CanStartCycle ? eJS_Ready : eJS_Busy );

    RefreshCaption();
}


void __fastcall TMPLMainForm::WMShowSysSettingsDlg( TMessage &Message )
{
    // This event is only called from the constructor when there is an error
    // with the comm settings.
    MessageDlg( "The device port could not be opened (" + m_mplsMgr->ConnectResult + "). You will have to modify the port settings before continuing.",
                mtError, TMsgDlgButtons() << mbOK, 0 );

    SysSettingsBtnClick( SysSettingsBtn );
}


void __fastcall TMPLMainForm::WMCommsEvent( TMessage &Message )
{
    // Event posted the by comms mgr when something changes.
    // It would be good to have the time of the event included in
    // the Windows message, but for now just use the current time.
    time_t tEvent = time( NULL );

    // Windows message can continue to be received for a while after closing
    // the main form. If the mplsMgr is NULL, then ignore the message.
    if( m_mplsMgr == NULL )
        return;

    // Process the message
    switch( Message.WParam )
    {
        case eMME_Initializing:
            // Event posted when comm port is opened and init process started
            UpdateStatusScanning();
            break;

        case eMME_InitComplete:
            // Ports are open and ready to start mode operating
            // Can launch if the job is not complete, otherwise can't launch
            if( m_currJob != NULL )
            {
                if( m_currJob->Completed )
                    SetJobState( eJS_Complete );
                else
                    SetJobState( eJS_HWWait );
            }
            else
            {
                // Don't have a job yet - disable the system
                SetJobState( eJS_Disabled );
            }

            break;

        case eMME_LinkStateChange:
            // MPLS device has had a change in its link state - update status boxes
            UpdatePortStatusBoxes();

            // Log serious changes
            // TODO - log serious state change issues?
            break;

        case eMME_Connected:
            // Have connected with a MPLS. Can complete init
            SyncJobToMPLS( tEvent );
            break;

        case eMME_PortLost:
            // Event posted when a port which was open has been lost
            UpdateStatusScanning();

            if( ( m_currJob == NULL ) || m_currJob->Completed )
                SetJobState( eJS_Disabled );
            else
                SetJobState( eJS_HWWait );
            break;

        case eMME_CommsLost:
            // Event posted when we've lost comms with either the MPL or base radio
            UpdateStatusScanning();

            if( ( m_currJob == NULL ) || m_currJob->Completed )
                SetJobState( eJS_Disabled );
            else
                SetJobState( eJS_HWWait );

            AddJobComment( "Communications lost" );

            DisplayEvent( eAET_CommsLost, true, true );

            break;

        case eMME_RFCRecvd:
            // RFC received, new status values will be available.

            // Enable the reset flag button, if appropriate
            if( m_mplsMgr->FlagDeployed && ( m_eJobState == eJS_Ready ) )
                ResetFlagBtn->Enabled = true;
            else
                ResetActionBtn( ResetFlagBtn );

            UpdateStatusValues();
            LogRFCData();

            break;

        case eMME_LaunchDone:
            // Launch process completed for a plug
            ProcessLaunchDone( (eCanisterNbr)( Message.LParam & 0x0000FFFF ), tEvent, ( Message.LParam >> 16 ) & 0x0000FFFF );
            break;

        case eMME_PlugDetected:
            // A plug-detection event has occurred
            ProcessPlugDetected( tEvent );
            break;

        case eMME_CleaningDone:
            // Cleaning cycle is complete
            ProcessCanisterCleaned( (eCanisterNbr)( Message.LParam & 0x0000FFFF ), tEvent, ( Message.LParam >> 16 ) & 0x0000FFFF );
            break;

        case eMME_FlagDeployed:
            // Process flag having been deployed
            ProcessFlagDeployed( tEvent );
            break;

        case eMME_ResetFlagDone:
            // Reset flag operation complete
            ProcessResetFlagDone( tEvent, ( Message.LParam >> 16 ) & 0x0000FFFF );
            break;

        case eMME_LowBattOnCycle:
            // Have a low battery on launch - warn the user
            AddJobComment( "Low battery detected on launch or clean" );
            DisplayEvent( eAET_LowBattOnCycle, false, false );
            break;

        case eMME_HWCommsErrors:
            ProcessHWCommsErrors( Message.LParam );
            break;

        case eMME_Alarm:
            ProcessAlarm( (TMPLSManager::AlarmType)Message.LParam, tEvent );
            break;

        case eMME_MPLSInitFailed:
            // MPLS could not be initialized. Log a message and raise an alarm.
            AddJobComment( "MPLS initialization failed" );
            DisplayEvent( eAET_InitFailed, true, true );
            break;

        case eMME_MPLSCmdFailed:
            // MPLS failed the processing of a command. Log a message and raise an alarm.
            AddJobComment( "MPLS failed to process a command" );

            // TODO - consider raising an alert or an alarm
            break;

        default:
            break;
    }
}


void __fastcall TMPLMainForm::WMAckEvent( TMessage &Message )
{
    // Event posted by ACK form when user has acknowledged an event
    AckEventType eAckEvent = (AckEventType)( Message.WParam );

    // We do not record acks when in test mode
    if( ( m_currJob != NULL ) && !m_currJob->IsTestJob )
    {
        String  msgFormat;
        int     iPlugNbr = 0;

        switch( eAckEvent )
        {
            case eAET_StateMismatch:  msgFormat = "State mismatch acknowledged.";      break;
            case eAET_ConfigMismatch: msgFormat = "Config mismatch acknowledged.";     break;
            case eAET_ConfigChange:   msgFormat = "Config change acknowledged.";       break;
            case eAET_LowBattery:     msgFormat = "Low battery alarm acknowledged.";   break;
            case eAET_CommsLost:      msgFormat = "Lost comms alarm acknowledged.";    break;
            case eAET_FileSaveError:  msgFormat = "File save error acknowleged";       break;
            case eAET_LowBattOnCycle: msgFormat = "Low battery on cycle start warning acknowleged";    break;
            case eAET_BadLaunchPlug:  msgFormat = "Unexpected plug launch alarm acknowledged.";        break;
            case eAET_BadCleanCan:    msgFormat = "Unexpected canister cleaning alarm acknowledged.";  break;
            case eAET_HardwareErrors: msgFormat = "MPLS hardware errors alarm acknowledged";           break;
            case eAET_InitFailed:     msgFormat = "MPLS initialization failure alarm acknowledged";    break;
            default:                  msgFormat = "Unknown event " + IntToStr( eAckEvent ) + " acknowledged."; break;
        }

        // Format the message now. We always pass the plug number as a param but
        // ack or lost messages are the only ones that will actually use it.
        String logMessage;
        logMessage.printf( msgFormat.w_str(), iPlugNbr );

        AddJobComment( logMessage );
    }

    // If we have an alarm event, ack it as well
    if( eAckEvent == eAET_LowBattery )
        m_mplsMgr->AckAlarm( TMPLSManager::eAT_LowBattery );
}


bool TMPLMainForm::HaveJobLogDir( void )
{
    // Check for that the job log dir is valid. If not, go with a default dir.
    String sJobFolder = GetJobDataDir();

    if( TSystemSettingsForm::IsValidJobLogFolder( sJobFolder, true ) )
        return true;

    // Given folder is not valid. Go try a default folder.
    sJobFolder = IncludeTrailingBackslash( GetEnvironmentVariable( "ProgramData" ) );

    sJobFolder = sJobFolder + "CanRig\\Multiplug Launcher Jobs";

    if( TSystemSettingsForm::IsValidJobLogFolder( sJobFolder, true ) )
    {
        SetJobDataDir( sJobFolder );
        return true;
    }

    // Fall through means eve the default is bad!
    int msgRes = MessageDlg( "You must specify a location to save Job logs to. This must be a directory "
                             "on a local drive that this program has write access to."
                             "\r  Press OK to show the System Settings dialog and select a Job directory."
                             "\r  Press Cancel to quit this program.",
                             mtInformation, TMsgDlgButtons() << mbOK << mbCancel, 0 );

    if( msgRes != mrOk )
        return false;

    // Show the dialog. If the job dir is still not valid then just fail
    SysSettingsBtnClick( SysSettingsBtn );

    sJobFolder = GetJobDataDir();

    return TSystemSettingsForm::IsValidJobLogFolder( sJobFolder, true );
}


/*
    PostMessage( Handle, WM_SHOW_SYS_SETTINGS_DLG, 0, 0 );
    // Check for lost comms
    if( MainState == MS_LOST_BASERADIO )
    {
        // No base radio, check if this has changed
        if( m_mplsMgr->GetLastError() == TMPLSManager::PE_SUCCESS )
            MainState = MS_IDLE;
    }
    else if( MainState == MS_LOST_TESTORK )
    {
        // No TesTORK, check if we have a base radio, or if the TesTORK is back
        if( m_mplsMgr->GetLastError() == TMPLSManager::PE_SCANNING )
        {
            // Radio was also lost
            m_mplsMgr->ResetBaseRadioUp();

            MainState = MS_LOST_BASERADIO;
        }
        else if( m_mplsMgr->DeviceIsRecving )
        {
            // TesTORK is communicating again
            MainState = MS_IDLE;
        }
    }
    else if( MainState != MS_LOADING )
    {
        if( ( m_mplsMgr->BaseRadioInitUp ) && ( m_mplsMgr->GetLastError() == TMPLSManager::PE_SCANNING ) )
        {
            // If the radio was up and we're now scanning, we lost the radio
            SysTimer->Enabled  = false;
            PollTimer->Enabled = false;

            // Warn the user based on the state of AutoAssign ports
            if( GetCommOptions() )
                MessageDlg( "Base Radio was lost. The system is auto-scanning for a new device.", mtError, TMsgDlgButtons() << mbOK, 0 );
            else
                MessageDlg( "Base Radio was lost. The system will continue to scan on the assigned ports. To modify, open the System Settings.", mtError, TMsgDlgButtons() << mbOK, 0 );

            MainState = MS_LOST_BASERADIO;

            m_mplsMgr->ResetBaseRadioUp();
            m_mplsMgr->ResetTesTORKUp();

            SysTimer->Enabled  = true;
            PollTimer->Enabled = true;
        }
        else if( ( m_mplsMgr->TesTORKInitUp ) && ( !m_mplsMgr->DeviceIsRecving ) )
        {
            // If the TesTORk was up, and we're no longer receiving packets, we lost the TesTORK
            SysTimer->Enabled  = false;
            PollTimer->Enabled = false;

            MessageDlg( L"Communications with the TesTORK have been lost. ", mtError, TMsgDlgButtons() << mbOK, 0 );

            MainState = MS_LOST_TESTORK;

            m_mplsMgr->ResetTesTORKUp();

            SysTimer->Enabled  = true;
            PollTimer->Enabled = true;
        }
    }
*/



