//---------------------------------------------------------------------------
#ifndef TMPLSManagerMainH
#define TMPLSManagerMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Graphics.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include <Vcl.Menus.hpp>
#include "AdvSmoothButton.hpp"
#include "AdvSmoothLabel.hpp"
#include "AdvSmoothStatusIndicator.hpp"
#include "AdvSmoothPanel.hpp"
#include "AdvSmoothToggleButton.hpp"
#include "AdvSmoothProgressBar.hpp"
#include "GDIPPictureContainer.hpp"
#include "MPLRegistryInterface.h"
#include "TAckDlg.h"
#include "TJobList.h"
#include "TMPLSManager.h"
#include "Messages.h"
//---------------------------------------------------------------------------
class TMPLMainForm : public TForm
{
__published:	// IDE-managed Components
    TAdvSmoothPanel *MenuPanel;
    TTimer *SystemTimer;
    TAdvSmoothPanel *ControlPanel;
    TAdvSmoothToggleButton* EnableLaunchBtn;
    TAdvSmoothToggleButton *SlaveLaunch1Btn;
    TAdvSmoothToggleButton *SlaveLaunch2Btn;
    TAdvSmoothToggleButton *ProjSettingsBtn;
    TAdvSmoothToggleButton *ProjLogBtn;
    TAdvSmoothToggleButton *SysSettingsBtn;
    TAdvSmoothToggleButton *AboutBtn;
    TAdvSmoothToggleButton *SlaveClean2Btn;
    TAdvSmoothToggleButton *SlaveClean1Btn;
    TAdvSmoothToggleButton *GraphsBtn;
    TImage *LogoImage;
    TImage *SlaveImage;
    TAdvSmoothPanel *SlaveBattLevelStatusPanel;
    TAdvSmoothPanel *SlaveTankPressStatusPanel;
    TAdvSmoothPanel *SlaveRegPressStatusPanel;
    TAdvSmoothPanel *StatusPanel;
    TAdvSmoothPanel *BaseRadioStatusPanel;
    TAdvSmoothProgressBar *BaseRadioProgBar;
    TAdvSmoothPanel *MPLLinkStatusPanel;
    TAdvSmoothProgressBar *MPLLinkProgBar;
    TAdvSmoothPanel *RPMStatusPanel;
    TAdvSmoothPanel *TempStatusPanel;
    TAdvSmoothToggleButton *MasterLaunch1Btn;
    TAdvSmoothToggleButton *MasterLaunch2Btn;
    TAdvSmoothToggleButton *MasterClean2Btn;
    TAdvSmoothToggleButton *MasterClean1Btn;
    TImage *MasterImage;
    TAdvSmoothPanel *MasterBattLevelStatusPanel;
    TAdvSmoothPanel *MasterTankPressStatusPanel;
    TAdvSmoothPanel *MasterRegPressStatusPanel;
    TAdvSmoothPanel *FlagPanel;
    TAdvSmoothPanel *FlagStatusPanel;
    TAdvSmoothToggleButton *ResetFlagBtn;
    TPopupMenu *HiddenPopup;
    TMenuItem *OpenJob1;
    TImageList *CanisterImages;
    TAdvSmoothToggleButton *ReportsBtn;
    TAdvSmoothPanel *RSSIStatusPanel;
    void __fastcall AboutBtnClick(TObject *Sender);
    void __fastcall SystemTimerTimer(TObject *Sender);
    void __fastcall ProjSettingsBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall ProjLogBtnClick(TObject *Sender);
    void __fastcall EnableLaunchBtnClick(TObject *Sender);
    void __fastcall LaunchPlugBtnClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall SysSettingsBtnClick(TObject *Sender);
    void __fastcall CleanPlugBtnClick(TObject *Sender);
    void __fastcall ResetFlagBtnClick(TObject *Sender);
    void __fastcall GraphsBtnClick(TObject *Sender);
    void __fastcall OpenJob1Click(TObject *Sender);
    void __fastcall ReportsBtnClick(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);

private:
    TMPLSManager* m_mplsMgr;
    TBasePlugJob* m_currJob;
    bool          m_bIgnoreSimMode;
    bool          m_bShowFileSaveError;

    SHUT_DOWN_TYPE   m_lastShutdownType;
    UNITS_OF_MEASURE m_defaultUOM;

    DWORD        m_dwLaunchClickTimeout;
    DWORD        m_dwEnableBtnClickTime;

    DWORD        m_dwBtnAnimationTimer;

    struct {
        TAdvSmoothToggleButton* launchBtn;
        TAdvSmoothToggleButton* cleanBtn;
    } m_canisterControl[eCN_NbrCanisters];

    struct {
        TAdvSmoothPanel* battVoltsPanel;
        TAdvSmoothPanel* tankPressPanel;
        TAdvSmoothPanel* regPressPanel;
        TImage*          plugImage;
    } m_mplsDisplays[eMU_NbrMPLSUnits];

    typedef enum {
        eJS_Loading,     // Loading a job
        eJS_Disabled,    // Cannot launch any plugs
        eJS_HWWait,      // Waiting for hardware to complete an init or calibration
        eJS_Ready,       // Can enable a launch
        eJS_Enabled,     // Can launch or clean a plug
        eJS_Busy,        // Launching or cleaning a plug - must wait for the action to finish
        eJS_Complete,    // Like disabled, but after job marked as complete by user
        eJS_NbrJobStates
    } eJobState;

    eJobState m_eJobState;

    void SetJobState( eJobState eNewState );
        // Does the work required to set a job state

    bool OpenJob( void );
        // Returns true if a job was opened

    bool CloseJob( void );
        // Returns true if the job was closed

    void UpdateStatusValues( void );
    void UpdateStatusScanning( void );
    void UpdatePortStatusBoxes( void );
    void RefreshCaption( void );
    void SetMainFormDisplay( void );
    void SetSmoothPanelCaption( TAdvSmoothPanel* pPanel, const String& sText );
    void LogRFCData( void );

    void DisableActionBtns( bool bStopAnimations );
    void AnimateActionBtn( TAdvSmoothToggleButton* pBtn );

    void EnterSimMode( bool bIsSimulating );

    bool HaveJobLogDir( void );
    void AddJobComment( const String& sComment );

    void SyncJobToMPLS( time_t tWhen );
    void SyncDisplayToMPLS( void );
    void UpdateImageGraphic( TImage* pImage, eCanisterConfig eCanCfg, ePlugState eStatePlug1, ePlugState eStatePlug2 );

    void UpdateJobState( eSystemEvent eWhichEvent, time_t tWhen, int iInfo );

    void ProcessLaunchDone( eCanisterNbr eWhichPlug, time_t tWhen, int iMaxSolCurr );
    void ProcessPlugDetected( time_t tWhen );
    void ProcessCanisterCleaned( eCanisterNbr eWhichCan, time_t tWhen, int iMaxSolCurr );
    void ProcessFlagDeployed( time_t tWhen );
    void ProcessResetFlagDone( time_t tWhen, int iMaxSolCurr );
    void ProcessAlarm( TMPLSManager::AlarmType eAlarmType, time_t tWhen );
    void ProcessHWCommsErrors( DWORD dwErrorFlags );

    void DisplayEvent( AckEventType aetType, bool bIsAlarm, bool bActivateAlertOutput );

protected:
    void __fastcall WMCompleteJob       ( TMessage &Message );
    void __fastcall WMReopenJob         ( TMessage &Message );
    void __fastcall WMShowSysSettingsDlg( TMessage &Message );
    void __fastcall WMCommsEvent        ( TMessage &Message );
    void __fastcall WMAckEvent          ( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_COMPLETE_JOB,          TMessage, WMCompleteJob    )
      MESSAGE_HANDLER( WM_REOPEN_JOB,            TMessage, WMReopenJob      )
      MESSAGE_HANDLER( WM_SHOW_SYS_SETTINGS_DLG, TMessage, WMShowSysSettingsDlg )
      MESSAGE_HANDLER( WM_POLLER_EVENT,          TMessage, WMCommsEvent )
      MESSAGE_HANDLER( WM_ACK_EVENT,             TMessage, WMAckEvent )
    END_MESSAGE_MAP(TForm)

public:		// User declarations
    __fastcall TMPLMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMPLMainForm *MPLMainForm;
//---------------------------------------------------------------------------
#endif
