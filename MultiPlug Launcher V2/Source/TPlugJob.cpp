#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>

#include "TPlugJob.h"
#include "MPLRegistryInterface.h"

#pragma package(smart_init)


//
// TPlugJog
//

__fastcall TPlugJob::TPlugJob( const String& sJobFileName ) : TBasePlugJob( sJobFileName )
{
    // After the common constructor does its work, check if our
    // job loaded and if it was in fact a real job
    if( m_jobLoaded )
    {
        // If this was a test job, clear everything out. This
        // will reset the 'is loaded' flag so that the caller
        // knows there is a problem
        if( m_jobInfo.isTestJob )
            InitJobInfo();
    }
}


__fastcall TPlugJob::~TPlugJob()
{
}


void TPlugJob::SaveBackup( void )
{
    // If the job is done, no backup is saved
    if( m_jobParams.isDone )
        return;

    // Get the backup enabled flag, and the filepath
    String sBackupDir;

    if( GetForceBackup( sBackupDir ) )
    {
        if( sBackupDir != "" )
        {
            // Create the filepath if it does not exist
            if( !CreateDirectory( sBackupDir.w_str(), NULL ) && ( GetLastError() != ERROR_ALREADY_EXISTS ) )
                return;

            // Get the filename, use it to create the full backup path
            String sLogBackupFileName  = IncludeTrailingBackslash( sBackupDir ) + ExtractFileName( m_jobLogFileName );
            String sDataBackupFileName = IncludeTrailingBackslash( sBackupDir ) + ExtractFileName( m_jobDataFileName );

            // Delete the backup files if they already exist
            if( !DeleteFile( sLogBackupFileName.w_str() ) && ( GetLastError() != ERROR_FILE_NOT_FOUND ) )
                return;

            // Delete the backup files if they already exist
            if( !DeleteFile( sDataBackupFileName.w_str() ) && ( GetLastError() != ERROR_FILE_NOT_FOUND ) )
                return;

            // Copy over the current job file to the backup dir
            CopyFile( m_jobLogFileName.w_str(),  sLogBackupFileName.w_str(),  true );
            CopyFile( m_jobDataFileName.w_str(), sDataBackupFileName.w_str(), true );
        }
    }
}


