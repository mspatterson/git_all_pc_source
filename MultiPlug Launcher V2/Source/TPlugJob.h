#ifndef TPlugJobH
#define TPlugJobH

//******************************************************************************
//
//  TPlugJob.h: declaration for the object that maintains static information
//              about a given job.
//
//******************************************************************************

#include "TBasePlugJob.h"


class TPlugJob : public TBasePlugJob
{
  private:

  protected:

  public:

    virtual __fastcall TPlugJob( const String& sJobFileName );
    virtual __fastcall ~TPlugJob();
       // Common form of constructor for a plug job. The job must
       // already exist

    // Backup
    virtual void   SaveBackup( void );

};


#endif
