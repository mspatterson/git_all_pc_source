#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>

#include "TPlugJobDetailsDlg.h"
#include "MPLRegistryInterface.h"

#pragma package(smart_init)
#pragma link "GDIPPictureContainer"
#pragma resource "*.dfm"


TPlugJobDetailsForm *PlugJobDetailsForm;


//
// Class Functions
//

__fastcall TPlugJobDetailsForm::TPlugJobDetailsForm(TComponent* Owner) : TForm(Owner)
{
    // Some edits are not required, while other edits must contain numeric
    // values. Assign a bit flag to each edit's tag member here defining
    // its requirements.
    #define CTRL_MANDATORY  0x0001
    #define CTRL_READ_ONLY  0x0002
    #define EDIT_INTEGER    0x0003
    #define EDIT_POS_FLOAT  0x0008
    #define EDIT_FLOAT      0x0010

    WellNameEdit->Tag = CTRL_READ_ONLY;
    JobDateEdit->Tag  = CTRL_READ_ONLY;
}


void __fastcall TPlugJobDetailsForm::CreateParams(Controls::TCreateParams &Params)
{
    // Set WS_EX_APPWINDOW style - this forces this dialog to appear as an
    // icon on the task bar.
    TForm::CreateParams( Params );
    Params.ExStyle   = Params.ExStyle | WS_EX_APPWINDOW;
    Params.WndParent = ParentWindow;
}


bool TPlugJobDetailsForm::CreateNewJob( PLUG_JOB_INFO& newJobInfo, TStringList* pPreJobRemStrings, TStringList* pPostJobRemStrings )
{
    // Presents the job details dialog and creates a new job if all entered params are legit.
    // Returns true on success, populating jobName with the new job's filename.

    // First, clear all edits
    ClearEdits( CfgDetailsGB );
    ClearEdits( JobInfoSheet );
    ClearEdits( RemarksSheet );

    // All group boxes are enabled when creating a new job
    EnableEdits( CfgDetailsGB, true );
    EnableEdits( JobInfoSheet, true );
    EnableEdits( RemarksSheet, true );

    UpperCanisterRG->Enabled = true;
    LowerCanisterRG->Enabled = true;

    UpperCanisterRG->ItemIndex = 1;
    LowerCanisterRG->ItemIndex = 1;

    CanisterCfgClick( NULL );

    // Okay and cancel btns are visible when creating a new job
    OKBtn->Visible     = true;
    CancelBtn->Caption = L"Cancel";

    // Populate client and location fields - these are read-only fields
    WellNameEdit->Text = newJobInfo.wellName;
    JobDateEdit->Text  = TBasePlugJob::JobDateToString( newJobInfo.stJobDate );

    // Update the caption
    Caption = L"Create New Job";

    JobDetailsPgCtrl->ActivePageIndex = 0;

    ActiveControl = LowerCanisterRG;

    // Show the dialog. Validation is performed on the OK btn click.
    if( ShowModal() != mrOk )
        return false;

    // Ok was pressed, and all params are valid. Return the information
    GetJobInfo( newJobInfo );
    GetRemarks( PreJobRemarksMemo,  pPreJobRemStrings  );
    GetRemarks( PostJobRemarksMemo, pPostJobRemStrings );

    return true;
}


bool TPlugJobDetailsForm::EditJobInfo( PLUG_JOB_INFO& jobInfo, TStringList* pPreJobRemStrings, TStringList* pPostJobRemStrings )
{
    PopulateJobInfo( jobInfo );

    SetRemarks( PreJobRemarksMemo,  pPreJobRemStrings  );
    SetRemarks( PostJobRemarksMemo, pPostJobRemStrings );

    // Okay and cancel btns are visible when editing job settings
    OKBtn->Visible     = true;
    CancelBtn->Caption = L"Cancel";

    // Set read-only controls
    // TODO - CanRig to advise if any controls should be 'read only' in
    // once job has been created.
    EnableEdits( CfgDetailsGB, true );
    EnableEdits( JobInfoSheet, true );
    EnableEdits( RemarksSheet, true );

    UpperCanisterRG->Enabled = false;
    LowerCanisterRG->Enabled = false;

    JobDetailsPgCtrl->ActivePageIndex = 0;

    ActiveControl = CasingTypeEdit;

    // Show the form
    if( ShowModal() != mrOk )
        return false;

    GetJobInfo( jobInfo );
    GetRemarks( PreJobRemarksMemo,  pPreJobRemStrings  );
    GetRemarks( PostJobRemarksMemo, pPostJobRemStrings );

    return true;
}


void TPlugJobDetailsForm::ShowJobInfo( const PLUG_JOB_INFO& jobInfo, TStringList* pPreJobRemStrings, TStringList* pPostJobRemStrings )
{
    PopulateJobInfo( jobInfo );

    SetRemarks( PreJobRemarksMemo,  pPreJobRemStrings  );
    SetRemarks( PostJobRemarksMemo, pPostJobRemStrings );

    // Okay and cancel btns are visible when editing job settings
    OKBtn->Visible     = false;
    CancelBtn->Caption = "Close";

    // All controls are read-only when showing info
    EnableEdits( CfgDetailsGB, false );
    EnableEdits( JobInfoSheet, false );
    EnableEdits( RemarksSheet, false );

    UpperCanisterRG->Enabled = false;
    LowerCanisterRG->Enabled = false;

    JobDetailsPgCtrl->ActivePageIndex = 0;

    ActiveControl = CancelBtn;

    // Show the dialog
    ShowModal();
}


void TPlugJobDetailsForm::PopulateJobInfo( const PLUG_JOB_INFO& jobInfo )
{
    // Header info - these are read only fields
    WellNameEdit->Text = jobInfo.wellName;
    JobDateEdit->Text  = TBasePlugJob::JobDateToString( jobInfo.stJobDate );

    // Config details
    CasingSizeEdit->Text   = jobInfo.casingSize;
    CasingTypeEdit->Text   = jobInfo.casingType;
    CasingWeightEdit->Text = jobInfo.casingWeight;
    SpacerVolumeEdit->Text = jobInfo.spacerVolume;
    SpacerWeightEdit->Text = jobInfo.spacerWeight;
    TailVolumeEdit->Text   = jobInfo.tailVolume;
    TailWeightEdit->Text   = jobInfo.tailWeight;
    LeadVolumeEdit->Text   = jobInfo.leadVolume;
    LeadWeightEdit->Text   = jobInfo.leadWeight;

    // Job info
    ClientEdit->Text         = jobInfo.clientName;
    WellNumEdit->Text        = jobInfo.wellNbr;
    RigEdit->Text            = jobInfo.rig;
    LeadHandEdit->Text       = jobInfo.leadHand;
    CementCoEdit->Text       = jobInfo.cementCo;
    CompayManDayEdit->Text   = jobInfo.companyManDay;
    CompayManNightEdit->Text = jobInfo.companyManNight;
    DrillerDayEdit->Text     = jobInfo.drillerDay;
    DrillerNightEdit->Text   = jobInfo.drillerNight;
    RigMgrDayEdit->Text      = jobInfo.rigMgrDay;
    RigMgrNightEdit->Text    = jobInfo.rigMgrNight;

    switch( jobInfo.eLowerCanConfig )
    {
        case eCC_OneShort:  LowerCanisterRG->ItemIndex = 0;   break;
        case eCC_OneLong:   LowerCanisterRG->ItemIndex = 2;   break;
        default:            LowerCanisterRG->ItemIndex = 1;   break;
    }

    switch( jobInfo.eUpperCanConfig )
    {
        case eCC_OneShort:  UpperCanisterRG->ItemIndex = 0;   break;
        case eCC_TwoShort:  UpperCanisterRG->ItemIndex = 1;   break;
        case eCC_OneLong:   UpperCanisterRG->ItemIndex = 2;   break;
        default:            UpperCanisterRG->ItemIndex = 3;   break;
    }

    // Fake out a click to update the graphics
    CanisterCfgClick( NULL );
}


void TPlugJobDetailsForm::GetJobInfo( PLUG_JOB_INFO& jobInfo )
{
    // Config details
    jobInfo.casingSize   = CasingSizeEdit->Text.Trim();
    jobInfo.casingType   = CasingTypeEdit->Text.Trim();
    jobInfo.casingWeight = CasingWeightEdit->Text.Trim();
    jobInfo.spacerVolume = SpacerVolumeEdit->Text.Trim();
    jobInfo.spacerWeight = SpacerWeightEdit->Text.Trim();
    jobInfo.tailVolume   = TailVolumeEdit->Text.Trim();
    jobInfo.tailWeight   = TailWeightEdit->Text.Trim();
    jobInfo.leadVolume   = LeadVolumeEdit->Text.Trim();
    jobInfo.leadWeight   = LeadWeightEdit->Text.Trim();

    // Job info
    jobInfo.clientName      = ClientEdit->Text.Trim();
    jobInfo.wellNbr         = WellNumEdit->Text.Trim();
    jobInfo.rig             = RigEdit->Text.Trim();
    jobInfo.leadHand        = LeadHandEdit->Text.Trim();
    jobInfo.cementCo        = CementCoEdit->Text.Trim();
    jobInfo.companyManDay   = CompayManDayEdit->Text.Trim();
    jobInfo.companyManNight = CompayManNightEdit->Text.Trim();
    jobInfo.drillerDay      = DrillerDayEdit->Text.Trim();
    jobInfo.drillerNight    = DrillerNightEdit->Text.Trim();
    jobInfo.rigMgrDay       = RigMgrDayEdit->Text.Trim();
    jobInfo.rigMgrNight     = RigMgrNightEdit->Text.Trim();

    switch( LowerCanisterRG->ItemIndex )
    {
        case 0:   jobInfo.eLowerCanConfig = eCC_OneShort;   break;
        case 2:   jobInfo.eLowerCanConfig = eCC_OneLong;    break;
        default:  jobInfo.eLowerCanConfig = eCC_TwoShort;   break;
    }

    switch( UpperCanisterRG->ItemIndex )
    {
        case 0:   jobInfo.eUpperCanConfig = eCC_OneShort;   break;
        case 1:   jobInfo.eUpperCanConfig = eCC_TwoShort;   break;
        case 2:   jobInfo.eUpperCanConfig = eCC_OneLong;    break;
        default:  jobInfo.eUpperCanConfig = eCC_NoPlug;     break;
    }
}


void TPlugJobDetailsForm::GetRemarks( TMemo* pSrcMemo,  TStringList* pOutStrings )
{
    if( pOutStrings == NULL )
        return;

    pOutStrings->Clear();
    pOutStrings->Assign( pSrcMemo->Lines );
}


void TPlugJobDetailsForm::SetRemarks( TMemo* pDestMemo, TStringList* pSrcStrings )
{
    if( pSrcStrings == NULL )
        pDestMemo->Lines->Clear();
    else
        pDestMemo->Lines->Assign( pSrcStrings );
}


void TPlugJobDetailsForm::ClearEdits( TWinControl* aContainer )
{
    for( int iCtrl = 0; iCtrl < aContainer->ControlCount; iCtrl++ )
    {
        TEdit* currEdit = dynamic_cast<TEdit*>( aContainer->Controls[iCtrl] );

        if( currEdit != NULL )
        {
            currEdit->Text = L"";
            continue;
        }

        // This function also clears combo boxes
        TComboBox* currCombo = dynamic_cast<TComboBox*>( aContainer->Controls[iCtrl] );

        if( currCombo != NULL )
        {
            currCombo->Items->Clear();
            currCombo->Text = L"";
            continue;
        }

        // This function also clears memos
        TMemo* currMemo = dynamic_cast<TMemo*>( aContainer->Controls[iCtrl] );

        if( currMemo != NULL )
            currMemo->Lines->Clear();
    }
}


void TPlugJobDetailsForm::EnableEdits( TWinControl* aContainer, bool isEnabled )
{
    for( int iCtrl = 0; iCtrl < aContainer->ControlCount; iCtrl++ )
    {
        TEdit* pEdit = dynamic_cast<TEdit*>(aContainer->Controls[iCtrl]);

        if( pEdit != NULL )
        {
            if( ( pEdit->Tag & CTRL_READ_ONLY ) == CTRL_READ_ONLY )
            {
                pEdit->Enabled  = true;
                pEdit->ReadOnly = true;
            }
            else
            {
                pEdit->Enabled  = true;
                pEdit->ReadOnly = !isEnabled;
            }

            continue;
        }

        // For a memo, we toggle its readonly property. This will allow a
        // user to scroll around in the memo if necessary
        TMemo* currMemo = dynamic_cast<TMemo*>( aContainer->Controls[iCtrl] );

        if( currMemo != NULL )
            currMemo->ReadOnly = !isEnabled;
    }
}


TWinControl* TPlugJobDetailsForm::FindEmptyEdit( TWinControl* aContainer )
{
    // Look for an empty edit. Ignore controls that are not
    // mandatory or ones that are disabled
    for( int iCtrl = 0; iCtrl < aContainer->ControlCount; iCtrl++ )
    {
        if( !aContainer->Controls[iCtrl]->Enabled )
            continue;

        if( ( aContainer->Controls[iCtrl]->Tag & CTRL_MANDATORY ) != 0 )
        {
            TEdit* currEdit = dynamic_cast<TEdit*>( aContainer->Controls[iCtrl] );

            if( ( currEdit != NULL ) && ( currEdit->Enabled ) )
            {
                if( currEdit->Text.Trim().Length() == 0 )
                    return currEdit;
            }
        }
    }

     return NULL;
}


void __fastcall TPlugJobDetailsForm::OKBtnClick(TObject *Sender)
{
    // Validate fields. If all are good, then set modal result to mrOk

    // First, no edits can contain blank values
    TWinControl* blankCtrl = FindEmptyEdit( JobInfoSheet );

    if( blankCtrl != NULL )
    {
        MessageDlg( "Field cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        if( blankCtrl->Enabled )
            ActiveControl = blankCtrl;

        return;
    }

    // Fall through means values are good
    ModalResult = mrOk;
}


void __fastcall TPlugJobDetailsForm::CanisterCfgClick(TObject *Sender)
{
    // Update the graphics based on the selected config. Total height
    // avail to images is 210 pixels. Image top is 18.
    const int FirtImageTop     =  18;
    const int TotalImageHeight = 210;

    if( UpperCanisterRG->ItemIndex == 3 /*not used*/ )
    {
        SlaveImage->Visible = false;

        MasterImage->Top    = FirtImageTop;
        MasterImage->Height = TotalImageHeight;
    }
    else
    {
        SlaveImage->Visible = true;

        SlaveImage->Top    = FirtImageTop;
        SlaveImage->Height = TotalImageHeight / 2;

        MasterImage->Top    = FirtImageTop + TotalImageHeight / 2;
        MasterImage->Height = TotalImageHeight / 2;
    }

    MasterImage->Picture->Bitmap = NULL;

    switch( LowerCanisterRG->ItemIndex )
    {
        case 0:   CanisterImages->GetBitmap( eCI_Loaded1Short, MasterImage->Picture->Bitmap ); break;
        case 2:   CanisterImages->GetBitmap( eCI_Loaded1Long,  MasterImage->Picture->Bitmap ); break;
        default:  CanisterImages->GetBitmap( eCI_Loaded2Short, MasterImage->Picture->Bitmap ); break;
    }

    SlaveImage->Picture->Bitmap = NULL;

    switch( UpperCanisterRG->ItemIndex )
    {
        case 0:   CanisterImages->GetBitmap( eCI_Loaded1Short, SlaveImage->Picture->Bitmap ); break;
        case 1:   CanisterImages->GetBitmap( eCI_Loaded2Short, SlaveImage->Picture->Bitmap ); break;
        case 2:   CanisterImages->GetBitmap( eCI_Loaded1Long,  SlaveImage->Picture->Bitmap ); break;
    }
}


/* Legacy code from TT Manager software. Leave here as guidance in case
   client would like to implement drop-down combos for any fields

void TPlugJobDetailsForm::PopulateRepCombos( void )
{
    TStringList* nameList = new TStringList();

    GetRecentNameList( RNL_CLIENT_REP, nameList );
    nameList->Sorted = true;
    nameList->Sort();

//    RepCombo->Items->Assign( nameList );

    GetRecentNameList( RNL_TESCO_REP, nameList );
    nameList->Sorted = true;
    nameList->Sort();

//    TescoTechCombo->Items->Assign( nameList );

    delete nameList;

    // Clear any existing text from the combos
//    RepCombo->Text       = "";
//    TescoTechCombo->Text = "";
}

*/


