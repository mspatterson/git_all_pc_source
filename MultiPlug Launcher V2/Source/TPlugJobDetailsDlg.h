#ifndef TPlugJobDetailsDlgH
#define TPlugJobDetailsDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Imaging.pngimage.hpp>
#include <Vcl.ImgList.hpp>
#include "GDIPPictureContainer.hpp"

#include "TJobList.h"

class TPlugJobDetailsForm : public TForm
{
__published:    // IDE-managed Components
    TButton *CancelBtn;
    TButton *OKBtn;
    TImageList *CanisterImages;
    TPageControl *JobDetailsPgCtrl;
    TTabSheet *SysCfgSheet;
    TGroupBox *SysCfgGB;
    TImage *SlaveImage;
    TImage *MasterImage;
    TRadioGroup *UpperCanisterRG;
    TRadioGroup *LowerCanisterRG;
    TTabSheet *JobInfoSheet;
    TTabSheet *RemarksSheet;
    TMemo *PreJobRemarksMemo;
    TLabel *Label4;
    TLabel *Label5;
    TLabel *Label7;
    TLabel *Label8;
    TLabel *Label9;
    TLabel *Label10;
    TLabel *Label11;
    TEdit *ClientEdit;
    TEdit *RigEdit;
    TEdit *CompayManDayEdit;
    TEdit *CompayManNightEdit;
    TEdit *LeadHandEdit;
    TEdit *CementCoEdit;
    TEdit *WellNumEdit;
    TGroupBox *HeaderGB;
    TLabel *Label1;
    TEdit *WellNameEdit;
    TLabel *Label2;
    TEdit *JobDateEdit;
    TGroupBox *CfgDetailsGB;
    TEdit *SpacerVolumeEdit;
    TEdit *TailVolumeEdit;
    TEdit *TailWeightEdit;
    TEdit *LeadVolumeEdit;
    TEdit *LeadWeightEdit;
    TEdit *SpacerWeightEdit;
    TLabel *Label17;
    TLabel *Label16;
    TLabel *Label15;
    TLabel *Label14;
    TLabel *Label6;
    TEdit *CasingWeightEdit;
    TLabel *Label3;
    TEdit *CasingTypeEdit;
    TEdit *CasingSizeEdit;
    TLabel *Label13;
    TLabel *Label12;
    TMemo *PostJobRemarksMemo;
    TLabel *Label18;
    TLabel *Label19;
    TLabel *Label20;
    TEdit *DrillerDayEdit;
    TEdit *DrillerNightEdit;
    TEdit *RigMgrDayEdit;
    TEdit *RigMgrNightEdit;
    TLabel *Label21;
    TLabel *Label22;
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall CreateParams(Controls::TCreateParams &Params);
    void __fastcall CanisterCfgClick(TObject *Sender);

private:
    void   ClearEdits( TWinControl* aContainer );
    void   EnableEdits( TWinControl* aContainer, bool isEnabled );

    TWinControl* FindEmptyEdit( TWinControl* aContainer );

    void   PopulateJobInfo( const PLUG_JOB_INFO& jobInfo );
    void   GetJobInfo     (       PLUG_JOB_INFO& jobInfo );

    void   GetRemarks( TMemo* pSrcMemo,  TStringList* pOutStrings );
    void   SetRemarks( TMemo* pDestMemo, TStringList* pSrcStrings );

public:
    __fastcall TPlugJobDetailsForm(TComponent* Owner);

    bool CreateNewJob( PLUG_JOB_INFO& newJobInfo, TStringList* pPreJobRemStrings, TStringList* pPostJobRemStrings );
        // Presents the job details dialog and returns the user entered params.
        // Returns true if edits are valid and OK pressed.

    bool EditJobInfo( PLUG_JOB_INFO& newJobInfo, TStringList* pPreJobRemStrings, TStringList* pPostJobRemStrings );
        // Displays job details, allowing modifiable fields to be changed.
        // Returns true if any field has been changed.

    void ShowJobInfo( const PLUG_JOB_INFO& newJobInfo, TStringList* pPreJobRemStrings, TStringList* pPostJobRemStrings );
        // Displays job details with no modifications to any fields allowed.
};

extern PACKAGE TPlugJobDetailsForm *PlugJobDetailsForm;

#endif
