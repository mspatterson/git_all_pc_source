#include <vcl.h>
#pragma hdrstop

#include "TPlugJobsDlg.h"
#include "TPlugJobDetailsDlg.h"
#include "TPlugJob.h"
#include "MPLRegistryInterface.h"
#include "ApplUtils.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TPlugJobsForm *PlugJobsForm;


// Listview columns
typedef enum {
    LVC_WELL_NAME,
    LVC_JOB_DATE,
    LVC_CLIENT_NAME,
    NBR_LISTVIEW_COLS
} LIST_VIEW_COL;


__fastcall TPlugJobsForm::TPlugJobsForm(TComponent* Owner) : TForm(Owner)
{
    UnitsRG->Items->Strings[UOM_METRIC  ] = GetUnitsText( UOM_METRIC,   MT_MEASURE_NAME );
    UnitsRG->Items->Strings[UOM_IMPERIAL] = GetUnitsText( UOM_IMPERIAL, MT_MEASURE_NAME );

    UnitsRG->ItemIndex = GetDefaultUnitsOfMeasure();

    m_pJobList = NULL;

    // Populate the timezone list combo
    TimeZonesCombo->Items->Clear();

    TStringList* pTZList = new TStringList();

    GetTimeZoneNames( pTZList );

    if( pTZList->Count > 0 )
    {
        // Don't sort the list - leave them in the order returned by the registry
        TimeZonesCombo->Items->Assign( pTZList );
        TimeZonesCombo->ItemIndex = 0;
    }

    delete pTZList;
}


void __fastcall TPlugJobsForm::FormDestroy(TObject *Sender)
{
    if( m_pJobList != NULL )
        delete m_pJobList;
}


bool TPlugJobsForm::OpenJob( const String& preferredJobFileName, String& jobFileName, bool& bIsNewJob )
{
    // Jobs are stored under the main job folder first in a folder
    // named by the well name and job date. Then, in case there are
    // multiple jobs for a given well and date each individual job
    // has a unique name within that folder. This function returns true
    // if job has been selected with it being either an existing job
    // (bIsNewJob is false), or a new job (bIsNewJob is true)

    // Clear return vars first
    jobFileName = "";
    bIsNewJob   = false;

    // Init class vars
    m_selJobName = "";
    m_bIsNewJob  = false;

    // Init dialog controls
    JobPgCtrl->ActivePageIndex = 0;
    JobsFilterRG->ItemIndex    = 0;

    JobsFilterClick( JobsFilterRG );
    JobPgCtrlChange( JobPgCtrl );

    NewJobDatePicker->Date = Date();

    // Units of measure are not supported in this version. The visual
    // control is left in the form, but not visible. Force it to
    // imperial for now
    UnitsRG->ItemIndex = UOM_IMPERIAL;

    // Select our timezone as the default for new jobs
    String sTimeZone;
    bool   isDST = false;

    TIME_ZONE_INFORMATION tzInfo;

    if( GetTimeZoneInfo( sTimeZone, tzInfo, isDST ) )
    {
        TimeZonesCombo->ItemIndex = TimeZonesCombo->Items->IndexOf( sTimeZone );
        IsDSTCB->Checked = isDST;
    }
    else
    {
        TimeZonesCombo->ItemIndex = -1;
        IsDSTCB->Checked = false;
    }

    // If the preferredJobName is not blank, select it in the list of jobs
    JobsListView->ItemIndex = -1;

    if( ( preferredJobFileName.Length() > 0 ) && ( m_pJobList != NULL ) )
    {
        int preferredJobIndex = -1;

        for( int iJob = 0; iJob < m_pJobList->Count; iJob++ )
        {
            JOB_LIST_ITEM jobItem;

            if( m_pJobList->GetItem( iJob, jobItem ) )
            {
                if( jobItem.jobFileName.CompareIC( preferredJobFileName ) == 0 )
                {
                    preferredJobIndex = iJob;
                    break;
                }
            }
        }

        // Now need to find this item in the listview
        if( preferredJobIndex >= 0 )
        {
            for( int iItem = 0; iItem < JobsListView->Items->Count; iItem++ )
            {
                TListItem* pItem = JobsListView->Items->Item[iItem];

                if( (int)( pItem->Data ) == preferredJobIndex )
                {
                    JobsListView->ItemIndex = iItem;
                    break;
                }
            }
        }
    }

    if( ShowModal() != mrOk )
        return false;

    jobFileName = m_selJobName;
    bIsNewJob   = m_bIsNewJob;

    return true;
}


void __fastcall TPlugJobsForm::JobPgCtrlChange(TObject *Sender)
{
    // Set the caption of the OK button to match what we're
    // doing on the active page
    if( JobPgCtrl->ActivePageIndex == 0 )
        OKBtn->Caption = "Open Job";
    else
        OKBtn->Caption = "Create New Job";
}


void __fastcall TPlugJobsForm::JobsFilterClick(TObject *Sender)
{
    // If the box is checked, show all jobs in the jobs data dir.
    // Otherwise, show the MRU list of jobs

    // First, delete any existing job list
    if( m_pJobList != NULL )
    {
        delete m_pJobList;
        m_pJobList = NULL;
    }

    // Create the job list based on the selected filter option
    if( JobsFilterRG->ItemIndex == 0 )
    {
        TStringList* slRecentJobs = new TStringList();

        GetMostRecentJobsList( slRecentJobs );

        m_pJobList = new TJobList( slRecentJobs );

        delete slRecentJobs;
    }
    else
    {
        m_pJobList = new TJobList( GetJobDataDir() );
    }

    PopulateJobsList();
}


void __fastcall TPlugJobsForm::JobsListViewDblClick(TObject *Sender)
{
    OKBtnClick( OKBtn );
}


void __fastcall TPlugJobsForm::TimeZonesComboClick(TObject *Sender)
{
    // When the timezone combo gets clicked, update the Is DST checkbox
    String sTimeZone = TimeZonesCombo->Items->Strings[ TimeZonesCombo->ItemIndex ];

    bool isDST = false;

    TIME_ZONE_INFORMATION tzInfo = { 0 };

    if( GetTimeZoneInfo( sTimeZone, tzInfo, isDST ) )
        IsDSTCB->Checked = isDST;
    else
        IsDSTCB->Checked = false;
}


void __fastcall TPlugJobsForm::OKBtnClick(TObject *Sender)
{
    // Okay action depends on the sheet we are on.
    if( JobPgCtrl->ActivePageIndex == 0 )
    {
        // Showing list of jobs. If an item is selected, validate it. The
        // job file name will be the index in the jobs list that is saved
        // in the listview item's Data member.
        if( JobsListView->ItemIndex >= 0 )
        {
            TListItem* pItem = JobsListView->Items->Item[ JobsListView->ItemIndex ];

            int jobListIndex = (int)( pItem->Data );

            JOB_LIST_ITEM jobItem;

            if( m_pJobList->GetItem( jobListIndex, jobItem ) )
            {
                m_selJobName = jobItem.jobFileName;
                m_bIsNewJob  = false;

                ModalResult = mrOk;
                return;
            }
        }

        // Fall through means nothing selected or there was an error opening
        // the selected item
        Beep();

        return;
    }

    // Fall through means creating a new job. Validate entries
    String wellName = NewWellNameEdit->Text.Trim();

    if( wellName.Length() == 0 )
    {
        MessageDlg( "Well Name cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewWellNameEdit;
        return;
    }

    // Convert the date the user entered to a SYSTEMTIME struct
    WORD wYear;
    WORD wMonth;
    WORD wDay;

    try
    {
        NewJobDatePicker->Date.DecodeDate( &wYear, &wMonth, &wDay );
    }
    catch( ... )
    {
        MessageDlg( "The job date is not valid. Please select another date.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewJobDatePicker;

        return;
    }

    SYSTEMTIME stJobDate = { 0 };

    stJobDate.wYear         = wYear;
    stJobDate.wMonth        = wMonth;
    stJobDate.wDay          = wDay;
    stJobDate.wDayOfWeek    = NewJobDatePicker->Date.DayOfWeek() - 1;  // Embarcardero 'day of week' is 1-based, Windows 0-based

    if( TJobList::JobsExistFor( GetJobDataDir(), wellName, stJobDate ) > 0 )
    {
        int mrResult = MessageDlg( "One or more jobs for this client at this location already exists."
                                   "\r  Press Yes to create a new job at this location anyway."
                                   "\r  Press Cancel to enter different parameters for this job.", mtInformation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

        if( mrResult != mrYes )
        {
            ActiveControl = NewWellNameEdit;
            return;
        }
    }

    // Get the timezone info we'll need. Note that we'll use the user's override
    // of DST from the checkbox, and not the current DST value from Windows
    String sTimeZone = TimeZonesCombo->Items->Strings[ TimeZonesCombo->ItemIndex ];

    bool isDST = false;

    TIME_ZONE_INFORMATION tzInfo = { 0 };

    if( !GetTimeZoneInfo( sTimeZone, tzInfo, isDST ) )
    {
        MessageDlg( "The timezone information for \"" + sTimeZone + "\" could not be found. Please select another timezone.",
                    mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = TimeZonesCombo;

        return;
    }

    // New job info looks good... try to create the job
    PLUG_JOB_INFO pjiNewJob;

    pjiNewJob.wellName        = NewWellNameEdit->Text.Trim();
    pjiNewJob.stJobDate       = stJobDate;
    pjiNewJob.jobCreateTime   = time( NULL );
    pjiNewJob.timeZoneDisplay = sTimeZone;
    pjiNewJob.isDST           = IsDSTCB->Checked;
    pjiNewJob.bias            = tzInfo.Bias;
    pjiNewJob.standardBias    = tzInfo.StandardBias;
    pjiNewJob.daylightBias    = tzInfo.DaylightBias;
    pjiNewJob.isTestJob       = false;

    pjiNewJob.eLowerCanConfig = eCC_TwoShort;
    pjiNewJob.eUpperCanConfig = eCC_NoPlug;
    pjiNewJob.unitsOfMeasure  = (UNITS_OF_MEASURE)( UnitsRG->ItemIndex );

    // If the job cannot be created, the JobDetailsForm would have displayed
    // an error message. Just quit in that case
    TStringList* pPreJobRemarks  = new TStringList();
    TStringList* pPostJobRemarks = new TStringList();

    if( !PlugJobDetailsForm->CreateNewJob( pjiNewJob, pPreJobRemarks, pPostJobRemarks ) )
    {
        delete pPreJobRemarks;
        delete pPostJobRemarks;

        return;
    }

    // The PlugJobDetailsForm only fills in job params, it doesn't create
    // an actual job file. Do that now to get the job file name.
    String newJobFileName = TJobList::CreateNewJob( pjiNewJob, false );

    if( newJobFileName.Length() == 0 )
    {
        MessageDlg( "The job file could not be created. Please ensure you have write access to the "
                    "Job Data folder.", mtError, TMsgDlgButtons() << mbOK, 0 );

        delete pPreJobRemarks;
        delete pPostJobRemarks;

        return;
    }

    // Job was created - save the job name and save to the MRU list
    m_selJobName = newJobFileName;
    m_bIsNewJob  = true;

    AddToMostRecentJobsList( newJobFileName );

    // Add the remarks strings to the job
    TPlugJob* pNewJob = new TPlugJob( newJobFileName );

    pNewJob->SaveJobRemarks( TBasePlugJob::eJRT_Pre,  pPreJobRemarks  );
    pNewJob->SaveJobRemarks( TBasePlugJob::eJRT_Post, pPostJobRemarks );

    delete pNewJob;

    delete pPreJobRemarks;
    delete pPostJobRemarks;

    // Save used units of entry for next job
    SetDefaultUnitsOfMeasure( UnitsRG->ItemIndex );

    ModalResult = mrOk;
}


void __fastcall TPlugJobsForm::TestModeBtnClick(TObject *Sender)
{
    // Create a test job. Current local time is always used for the job
    WORD wYear  = 2018;
    WORD wMonth = 1;
    WORD wDay   = 1;

    try
    {
        Now().DecodeDate( &wYear, &wMonth, &wDay );
    }
    catch( ... )
    {
    }

    SYSTEMTIME stJobDate = { 0 };

    stJobDate.wYear         = wYear;
    stJobDate.wMonth        = wMonth;
    stJobDate.wDay          = wDay;
    stJobDate.wDayOfWeek    = Now().DayOfWeek() - 1;  // Embarcardero 'day of week' is 1-based, Windows 0-based

    String sTimeZone;
    bool isDST = false;
    TIME_ZONE_INFORMATION tzInfo = { 0 };

    GetTimeZoneInfo( sTimeZone, tzInfo, isDST );

    // Use a default for the well name if the user didn't provide one
    String sWellName = NewWellNameEdit->Text.Trim();

    if( sWellName.Length() == 0 )
        sWellName = "Test Job";

    // Fill in plug info now
    PLUG_JOB_INFO jobInfo;

    jobInfo.wellName        = sWellName;
    jobInfo.isTestJob       = true;

    jobInfo.jobCreateTime   = time( NULL );
    jobInfo.stJobDate       = stJobDate;
    jobInfo.timeZoneDisplay = sTimeZone;
    jobInfo.isDST           = isDST;
    jobInfo.bias            = tzInfo.Bias;
    jobInfo.standardBias    = tzInfo.StandardBias;
    jobInfo.daylightBias    = tzInfo.DaylightBias;

    jobInfo.clientName      = "CanRig";

    jobInfo.eLowerCanConfig = eCC_TwoShort;
    jobInfo.eUpperCanConfig = eCC_TwoShort;
    jobInfo.unitsOfMeasure  = UOM_IMPERIAL;

    // Now create the new job
    String newJobFileName = TJobList::CreateNewJob( jobInfo, true );

    if( newJobFileName.Length() == 0 )
    {
        MessageDlg( "The job file could not be created. Please ensure you have write access to the "
                    "Job Data folder.", mtError, TMsgDlgButtons() << mbOK, 0 );

        return;
    }

    // Save info for return to caller. Note that for a test job
    // we do not add the job name to the MRU list
    m_selJobName = newJobFileName;
    m_bIsNewJob  = true;

    // Save used units of entry for next job
    SetDefaultUnitsOfMeasure( UnitsRG->ItemIndex );

    ModalResult = mrOk;
}


void __fastcall TPlugJobsForm::JobsListBoxDblClick(TObject *Sender)
{
    OKBtnClick( OKBtn );
}


void TPlugJobsForm::PopulateJobsList( void )
{
    // Populate the listview. Need to bear in mind that the listview
    // sorts items based on each item's Caption. Therefore, the order
    // of the items in the listview may not be the same as the order
    // in the jobs list.
    JobsListView->Items->Clear();

    for( int iJob = 0; iJob < m_pJobList->Count; iJob++ )
    {
        JOB_LIST_ITEM jobItem;

        if( m_pJobList->GetItem( iJob, jobItem ) )
        {
            TListItem* pNewItem = JobsListView->Items->Add();

            pNewItem->Caption = jobItem.jobInfo.wellName;
            pNewItem->SubItems->Add( TBasePlugJob::JobDateToString( jobItem.jobInfo.stJobDate ) );
            pNewItem->SubItems->Add( jobItem.jobInfo.clientName );

            // Store the index of the item in the job list in the listview's data member.
            // We'll need this index later
            pNewItem->Data = (void *)iJob;
        }
    }
}

