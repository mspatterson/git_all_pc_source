object PlugJobsForm: TPlugJobsForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Select Job'
  ClientHeight = 366
  ClientWidth = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  DesignSize = (
    482
    366)
  PixelsPerInch = 96
  TextHeight = 13
  object JobPgCtrl: TPageControl
    Left = 4
    Top = 4
    Width = 474
    Height = 326
    ActivePage = ExistingSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    OnChange = JobPgCtrlChange
    object ExistingSheet: TTabSheet
      Caption = 'Existing Jobs'
      DesignSize = (
        466
        298)
      object JobsListView: TListView
        Left = 11
        Top = 12
        Width = 440
        Height = 197
        Anchors = [akLeft, akTop, akRight, akBottom]
        Columns = <
          item
            Caption = 'Well Name'
            Width = 150
          end
          item
            Caption = 'Date'
            Width = 150
          end
          item
            Caption = 'Client'
            Width = 100
          end>
        GridLines = True
        ReadOnly = True
        RowSelect = True
        SortType = stText
        TabOrder = 0
        ViewStyle = vsReport
        OnDblClick = JobsListViewDblClick
      end
      object JobsFilterRG: TRadioGroup
        Left = 11
        Top = 220
        Width = 440
        Height = 68
        Anchors = [akLeft, akRight, akBottom]
        Caption = ' Display Filter '
        ItemIndex = 0
        Items.Strings = (
          'Show most recent jobs'
          'Show all jobs')
        TabOrder = 1
        OnClick = JobsFilterClick
      end
    end
    object NewSheet: TTabSheet
      Caption = 'New Job'
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 11
        Top = 5
        Width = 443
        Height = 148
        Caption = ' Create New Job'
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 27
          Width = 50
          Height = 13
          Caption = 'Well Name'
        end
        object Label2: TLabel
          Left = 12
          Top = 60
          Width = 23
          Height = 13
          Caption = 'Date'
        end
        object Label6: TLabel
          Left = 12
          Top = 87
          Width = 49
          Height = 13
          Caption = 'Time Zone'
        end
        object NewWellNameEdit: TEdit
          Left = 76
          Top = 24
          Width = 349
          Height = 21
          TabOrder = 0
        end
        object UnitsRG: TRadioGroup
          Left = 76
          Top = 148
          Width = 157
          Height = 73
          Caption = ' Units of Measure '
          ItemIndex = 1
          Items.Strings = (
            'Metric'
            'Imperial')
          TabOrder = 1
          Visible = False
        end
        object NewJobDatePicker: TDateTimePicker
          Left = 76
          Top = 54
          Width = 201
          Height = 21
          Date = 43400.644899826390000000
          Time = 43400.644899826390000000
          TabOrder = 2
        end
        object TimeZonesCombo: TComboBox
          Left = 76
          Top = 84
          Width = 349
          Height = 21
          Style = csDropDownList
          TabOrder = 3
          OnClick = TimeZonesComboClick
        end
        object IsDSTCB: TCheckBox
          Left = 76
          Top = 116
          Width = 120
          Height = 17
          Caption = 'Daylight Savings Time'
          TabOrder = 4
        end
      end
      object GroupBox2: TGroupBox
        Left = 11
        Top = 163
        Width = 443
        Height = 58
        Caption = ' Test Mode '
        TabOrder = 1
        DesignSize = (
          443
          58)
        object TestModeBtn: TButton
          Left = 76
          Top = 20
          Width = 109
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = 'Enter Test Mode'
          TabOrder = 0
          OnClick = TestModeBtnClick
        end
      end
    end
  end
  object CancelBtn: TButton
    Left = 399
    Top = 336
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object OKBtn: TButton
    Left = 248
    Top = 336
    Width = 145
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Create New Job'
    Default = True
    TabOrder = 1
    OnClick = OKBtnClick
  end
end
