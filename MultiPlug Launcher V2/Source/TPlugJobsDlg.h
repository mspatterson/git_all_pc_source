//---------------------------------------------------------------------------
#ifndef TPlugJobsDlgH
#define TPlugJobsDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "TJobList.h"
//---------------------------------------------------------------------------
class TPlugJobsForm : public TForm
{
__published:    // IDE-managed Components
    TPageControl *JobPgCtrl;
    TButton *CancelBtn;
    TButton *OKBtn;
    TTabSheet *ExistingSheet;
    TTabSheet *NewSheet;
    TListView *JobsListView;
    TRadioGroup *JobsFilterRG;
    TGroupBox *GroupBox1;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label6;
    TEdit *NewWellNameEdit;
    TRadioGroup *UnitsRG;
    TDateTimePicker *NewJobDatePicker;
    TComboBox *TimeZonesCombo;
    TCheckBox *IsDSTCB;
    TGroupBox *GroupBox2;
    TButton *TestModeBtn;
    void __fastcall JobsFilterClick(TObject *Sender);
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall JobsListBoxDblClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall JobsListViewDblClick(TObject *Sender);
    void __fastcall TimeZonesComboClick(TObject *Sender);
    void __fastcall TestModeBtnClick(TObject *Sender);
    void __fastcall JobPgCtrlChange(TObject *Sender);

private:
    TJobList* m_pJobList;

    String m_selJobName;
    bool   m_bIsNewJob;

    void PopulateJobsList( void );

public:
    __fastcall TPlugJobsForm(TComponent* Owner);

    bool OpenJob( const String& preferredJobFileName, String& jobFileName, bool& bIsNewJob );
        // Jobs are stored under the main job folder first in a folder
        // named by the well name and job date. Then, in case there are
        // multiple jobs for a given well and date each individual job
        // has a unique name within that folder. This function returns true
        // if job has been selected with it being either an existing job
        // (bIsNewJob is false), or a new job (bIsNewJob is true)
};
//---------------------------------------------------------------------------
extern PACKAGE TPlugJobsForm *PlugJobsForm;
//---------------------------------------------------------------------------
#endif
