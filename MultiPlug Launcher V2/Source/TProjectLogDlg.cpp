#include <vcl.h>
#pragma hdrstop

#include "TProjectLogDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TProjectLogForm *ProjectLogForm;


typedef enum {
    eLGC_DateCol,
    eLGC_EntryCol,
    eLGC_NbrCols
} LogGridColumns;


__fastcall TProjectLogForm::TProjectLogForm(TComponent* Owner) : TForm(Owner)
{
    m_pCurrJob = NULL;

    LogGrid->Cells[eLGC_DateCol][0]  = "Date";
    LogGrid->Cells[eLGC_EntryCol][0] = "Entry";
}


void __fastcall TProjectLogForm::FormResize(TObject *Sender)
{
    // On resize, set the column widths. Column 1 width is fixed, column 2 takes
    // up the remaining space.
    const int DateColWidth = 130;

    int iEntryWidth = LogGrid->ClientWidth - DateColWidth - LogGrid->GridLineWidth;

    LogGrid->ColWidths[eLGC_DateCol]  = DateColWidth;
    LogGrid->ColWidths[eLGC_EntryCol] = iEntryWidth;
}


void TProjectLogForm::SetJob( TBasePlugJob* pCurrJob )
{
    m_pCurrJob = pCurrJob;

    if( m_pCurrJob != NULL )
        m_pCurrJob->OnCommentAdded = OnCommentAdded;

    RefreshLog();
}


void TProjectLogForm::CloseJob( void )
{
    m_pCurrJob = NULL;
    RefreshLog();

    Close();
}


void TProjectLogForm::ViewLog( void )
{
    Show();
}


void TProjectLogForm::RefreshLog( void )
{
    if( ( m_pCurrJob == NULL ) || ( m_pCurrJob->NbrJobComments == 0 ) )
    {
        LogGrid->RowCount = LogGrid->FixedRows + 1;
        LogGrid->Rows[LogGrid->FixedRows]->Clear();
    }
    else
    {
        LogGrid->RowCount = LogGrid->FixedRows + m_pCurrJob->NbrJobComments;

        for( int iComment = 0; iComment < m_pCurrJob->NbrJobComments; iComment++ )
        {
            int iCommentRow = LogGrid->FixedRows + iComment;

            JOB_COMMENT comment;

            if( m_pCurrJob->GetJobComment( iComment, comment ) )
            {
                LogGrid->Cells[eLGC_DateCol ][iCommentRow] = TBasePlugJob::JobTimeToLocalString( comment.entryTime );
                LogGrid->Cells[eLGC_EntryCol][iCommentRow] = comment.entryText;
            }
            else
            {
                LogGrid->Rows[iCommentRow]->Clear();
            }
        }
    }
}


void __fastcall TProjectLogForm::OnCommentAdded( TObject* pSender )
{
    RefreshLog();
}


void __fastcall TProjectLogForm::CloseBtnClick(TObject *Sender)
{
    Close();
}


void __fastcall TProjectLogForm::PrintBtnClick(TObject *Sender)
{
    if( ( m_pCurrJob == NULL ) || ( m_pCurrJob->NbrJobComments == 0 ) )
    {
        Beep();
        return;
    }

    TRichEdit* pRichEdit = new TRichEdit( Handle );

    try
    {
        for( int iComment = 0; iComment < m_pCurrJob->NbrJobComments; iComment++ )
        {
            JOB_COMMENT comment;

            if( m_pCurrJob->GetJobComment( iComment, comment ) )
            {
                String sLine = TBasePlugJob::JobTimeToLocalString( comment.entryTime ) + "\t" + comment.entryText;
                pRichEdit->Lines->Add( sLine );
            }
        }

        if( PrinterSetupDlg->Execute() )
        {
            String sJobName = "MPLS Job Log - " + m_pCurrJob->WellName + " / " + m_pCurrJob->JobDate;
            pRichEdit->Print( sJobName );
        }
    }
    catch( ... )
    {
        MessageDlg( "An error occurred while printing the log.", mtError, TMsgDlgButtons() << mbOK, 0 );
    }

    delete pRichEdit;
}


void __fastcall TProjectLogForm::CommentBtnClick(TObject *Sender)
{
    if( m_pCurrJob != NULL )
    {
        String sComment;

        if( InputQuery( "Job Comment", "Enter comment: ", sComment ) )
        {
            sComment = sComment.Trim();

            if( sComment.Length() > 0 )
                m_pCurrJob->AddJobComment( sComment );
        }
    }
}

