object ProjectLogForm: TProjectLogForm
  Left = 0
  Top = 0
  Caption = 'Project Log'
  ClientHeight = 401
  ClientWidth = 589
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnResize = FormResize
  DesignSize = (
    589
    401)
  PixelsPerInch = 96
  TextHeight = 13
  object LogGrid: TStringGrid
    Left = 8
    Top = 8
    Width = 573
    Height = 353
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 2
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect]
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object PrintBtn: TButton
    Left = 8
    Top = 368
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Print'
    TabOrder = 1
    OnClick = PrintBtnClick
  end
  object CommentBtn: TButton
    Left = 113
    Top = 368
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Add Comment'
    TabOrder = 2
    OnClick = CommentBtnClick
  end
  object CloseBtn: TButton
    Left = 506
    Top = 368
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 3
    OnClick = CloseBtnClick
  end
  object PrinterSetupDlg: TPrinterSetupDialog
    Left = 504
    Top = 24
  end
end
