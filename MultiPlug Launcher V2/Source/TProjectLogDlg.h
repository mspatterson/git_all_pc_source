//---------------------------------------------------------------------------
#ifndef TProjectLogDlgH
#define TProjectLogDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.Dialogs.hpp>
#include "TBasePlugJob.h"
//---------------------------------------------------------------------------
class TProjectLogForm : public TForm
{
__published:	// IDE-managed Components
    TStringGrid *LogGrid;
    TButton *PrintBtn;
    TButton *CommentBtn;
    TButton *CloseBtn;
    TPrinterSetupDialog *PrinterSetupDlg;
    void __fastcall FormResize(TObject *Sender);
    void __fastcall PrintBtnClick(TObject *Sender);
    void __fastcall CommentBtnClick(TObject *Sender);
    void __fastcall CloseBtnClick(TObject *Sender);

private:
    TBasePlugJob* m_pCurrJob;

    void __fastcall OnCommentAdded( TObject* pSender );

public:
    __fastcall TProjectLogForm(TComponent* Owner);

    void SetJob( TBasePlugJob* pCurrJob );
    void CloseJob( void );

    void ViewLog( void );
    void RefreshLog( void );
};
//---------------------------------------------------------------------------
extern PACKAGE TProjectLogForm *ProjectLogForm;
//---------------------------------------------------------------------------
#endif
