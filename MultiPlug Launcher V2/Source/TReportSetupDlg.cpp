#include <vcl.h>
#pragma hdrstop

#include "TReportSetupDlg.h"
#include "TRptMPLSJobDlg.h"


#pragma package(smart_init)
#pragma resource "*.dfm"


TReportSetupDlg *ReportSetupDlg;


__fastcall TReportSetupDlg::TReportSetupDlg(TComponent* Owner) : TForm(Owner)
{
}


void TReportSetupDlg::ShowReportSetupDlg( TBasePlugJob* plugJob, TGraphForm* pGraphForm )
{
    if( ( plugJob == NULL ) || ( pGraphForm == NULL ) )
    {
        Beep();
        return;
    }

    m_plugJob   = plugJob;
    m_graphForm = pGraphForm;

    WellNamePanel->Caption = plugJob->WellName;
    DatePanel->Caption     = plugJob->JobDate;

    ShowModal();
}


void __fastcall TReportSetupDlg::CloseBtnClick(TObject *Sender)
{
    Close();
}

void __fastcall TReportSetupDlg::PreviewBtnClick(TObject *Sender)
{
    TMPLSJobRptDlg::REPORT_OPTIONS rptOpts = { 0 };

    rptOpts.bShowVoltsCurves  = VoltGraphCBx->Checked;
    rptOpts.bShowPressCurves  = PressureGraphCBx->Checked;
    rptOpts.bShowPlugDetCurve = SensorRPMCBx->Checked;

    // The TimeSpanCombo ItemIndex corresponds to the hours to
    // display per graph page, with zero being show just one graph
    rptOpts.iGraphHoursPerPage = TimeSpanCombo->ItemIndex;

    MPLSJobRptDlg->ShowReport( m_plugJob, m_graphForm, rptOpts );
}

