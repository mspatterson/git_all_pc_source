object ReportSetupDlg: TReportSetupDlg
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Job Report Options'
  ClientHeight = 284
  ClientWidth = 386
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    386
    284)
  PixelsPerInch = 96
  TextHeight = 13
  object CustomerLB: TLabel
    Left = 8
    Top = 16
    Width = 54
    Height = 13
    Caption = 'Well Name:'
  end
  object LocationLB: TLabel
    Left = 8
    Top = 44
    Width = 47
    Height = 13
    Caption = 'Job Date:'
  end
  object WellNamePanel: TPanel
    Left = 68
    Top = 12
    Width = 310
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    Caption = 'Well Name Panel'
    TabOrder = 0
  end
  object DatePanel: TPanel
    Left = 68
    Top = 40
    Width = 310
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    Caption = 'Date Panel'
    TabOrder = 1
  end
  object ReportContentsGB: TGroupBox
    Left = 8
    Top = 79
    Width = 370
    Height = 165
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Report Contents '
    TabOrder = 2
    object TimeSpanLB: TLabel
      Left = 19
      Top = 130
      Width = 127
      Height = 13
      Caption = 'Graph Time Span Per Page'
    end
    object JobInfoCBx: TCheckBox
      Left = 16
      Top = 24
      Width = 151
      Height = 17
      Caption = 'Show Job Information'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 0
    end
    object VoltGraphCBx: TCheckBox
      Left = 16
      Top = 48
      Width = 215
      Height = 17
      Caption = 'Show Battery Volts on Graph'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object PressureGraphCBx: TCheckBox
      Left = 16
      Top = 74
      Width = 215
      Height = 17
      Caption = 'Show Pressures on Graph'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object SensorRPMCBx: TCheckBox
      Left = 16
      Top = 98
      Width = 215
      Height = 17
      Caption = 'Show Plug Detector Data on Graph'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object TimeSpanCombo: TComboBox
      Left = 163
      Top = 127
      Width = 198
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 4
      Text = 'Entire Job'
      Items.Strings = (
        'Entire Job'
        '1 hour'
        '2 hours'
        '3 hours'
        '4 hours'
        '5 hours'
        '6 hours')
    end
  end
  object CloseBtn: TButton
    Left = 303
    Top = 252
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    TabOrder = 4
    OnClick = CloseBtnClick
  end
  object PreviewBtn: TButton
    Left = 8
    Top = 252
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Preview'
    TabOrder = 3
    OnClick = PreviewBtnClick
  end
end
