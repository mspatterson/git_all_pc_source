#ifndef TReportSetupDlgH
#define TReportSetupDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "TGraphForm.h"
#include "TBasePlugJob.h"

class TReportSetupDlg : public TForm
{
__published:	// IDE-managed Components
    TLabel *CustomerLB;
    TPanel *WellNamePanel;
    TLabel *LocationLB;
    TPanel *DatePanel;
    TGroupBox *ReportContentsGB;
    TButton *CloseBtn;
    TButton *PreviewBtn;
    TCheckBox *JobInfoCBx;
    TCheckBox *VoltGraphCBx;
    TCheckBox *PressureGraphCBx;
    TCheckBox *SensorRPMCBx;
    TLabel *TimeSpanLB;
    TComboBox *TimeSpanCombo;
    void __fastcall CloseBtnClick(TObject *Sender);
    void __fastcall PreviewBtnClick(TObject *Sender);

private:
    TBasePlugJob* m_plugJob;
    TGraphForm*   m_graphForm;

public:		// User declarations
    __fastcall TReportSetupDlg(TComponent* Owner);

    void ShowReportSetupDlg( TBasePlugJob* plugJob, TGraphForm* pGraphForm );
};
//---------------------------------------------------------------------------
extern PACKAGE TReportSetupDlg *ReportSetupDlg;
//---------------------------------------------------------------------------
#endif
