#include <vcl.h>
#pragma hdrstop

#include "TRptJobLogDlg.h"
#include "Applutils.h"
#include "RptHelpers.h"

#pragma package(smart_init)
#pragma link "frxClass"
#pragma resource "*.dfm"


TJobLogReportForm *JobLogReportForm;


__fastcall TJobLogReportForm::TJobLogReportForm(TComponent* Owner): TForm(Owner)
{
}


void TJobLogReportForm::ShowReport( TJob* currJob )
{
    // Validate current job.
    if( currJob == NULL )
        return;

    // Remember the job
    m_currJob = currJob;

    // Set number of records to display. If there are no comments,
    // set a flag and force one 'record' to be displayed.
    if( currJob->NbrMainComments > 0 )
    {
        m_haveRecs = true;

        frxCommentDataset->RangeEndCount = currJob->NbrMainComments;
    }
    else
    {
        m_haveRecs = false;

        frxCommentDataset->RangeEndCount = 1;
    }

    frxCommentDataset->RangeEnd = reCount;

    // Initialize job name memos - these never change
    RptSetMemoText( currJob, JobLogReport, "ClientNameMemo", currJob->Client );
    RptSetMemoText( currJob, JobLogReport, "JobLocnMemo",    currJob->Location );
    RptSetMemoText( currJob, JobLogReport, "JobDateMemo",    currJob->DateTimeString() );

    // preview the report
    JobLogReport->ShowReport( true );
}


void __fastcall TJobLogReportForm::frxCommentDatasetGetValue(const UnicodeString VarName, Variant &Value)
{
    if( m_haveRecs )
    {
        MAIN_COMMENT comment;

        if( m_currJob->GetMainComment( frxCommentDataset->RecNo, comment ) )
        {
            if( VarName == "CommentDate" )
                Value = m_currJob->DateTimeString();
            else if( VarName == "CommentText" )
                Value = comment.entryText;
            else
                Value = "Unknown field: " + VarName;
        }
        else
        {
            Value = "Bad rec nbr " + IntToStr( frxCommentDataset->RecNo );
        }
    }
    else
    {
        if( VarName == "CommentDate" )
            Value = "";
        else if( VarName == "CommentText" )
            Value = "(no comments)";
        else
            Value = "Unknown field: " + VarName;
    }
}


void __fastcall TJobLogReportForm::JobLogReportPreview(TObject *Sender)
{
    TfrxReport* frxReport = (TfrxReport*)Sender;
    frxReport->PreviewForm->BorderIcons = TBorderIcons() << biSystemMenu << biMaximize;
}

