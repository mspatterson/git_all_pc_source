//---------------------------------------------------------------------------
#ifndef TRptJobLogDlgH
#define TRptJobLogDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "frxClass.hpp"
#include "JobManager.h"
//---------------------------------------------------------------------------
class TJobLogReportForm : public TForm
{
__published:    // IDE-managed Components
    TfrxReport *JobLogReport;
    TfrxUserDataSet *frxCommentDataset;
    void __fastcall frxCommentDatasetGetValue(const UnicodeString VarName, Variant &Value);
    void __fastcall JobLogReportPreview(TObject *Sender);


private:
     TJob* m_currJob;
     bool  m_haveRecs;

public:
    __fastcall TJobLogReportForm(TComponent* Owner);
    void ShowReport( TJob* currJob );
};
//---------------------------------------------------------------------------
extern PACKAGE TJobLogReportForm *JobLogReportForm;
//---------------------------------------------------------------------------
#endif
