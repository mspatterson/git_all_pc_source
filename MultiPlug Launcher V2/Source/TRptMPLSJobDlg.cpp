#include <vcl.h>
#pragma hdrstop

#include <Clipbrd.hpp>

#include "TRptMPLSJobDlg.h"
#include "RptHelpers.h"
#include "Applutils.h"
#include "Messages.h"

#pragma package(smart_init)
#pragma link "frxClass"
#pragma resource "*.dfm"


TMPLSJobRptDlg *MPLSJobRptDlg;


//
// Class Implementation
//

__fastcall TMPLSJobRptDlg::TMPLSJobRptDlg(TComponent* Owner) : TForm(Owner)
{
}


void  TMPLSJobRptDlg::ShowReport( TBasePlugJob* currJob, TGraphForm* pGraphForm, const REPORT_OPTIONS& rptOptions )
{
    // Display report with the passed options
    if( currJob == NULL )
        return;

    m_currJob   = currJob;
    m_graphForm = pGraphForm;
    m_rptOpts   = rptOptions;

    // Initialize job name memos - these never change
    RptSetMemoText( m_currJob, JobReport, "WellNameMemo", m_currJob->WellName );
    RptSetMemoText( m_currJob, JobReport, "JobDateMemo",  m_currJob->JobDate  );

    if( m_currJob->IsTestJob )
        RptSetMemoText( m_currJob, JobReport, "FooterTestRptMemo", "TEST JOB" );
    else
        RptSetMemoText( m_currJob, JobReport, "FooterTestRptMemo", "" );

    // Cache a copy of the job info struct
    currJob->GetJobInfo( m_jobInfo );

    // Set the visibility of the HWSlave child based on config
    ShowSlaveHWRow( m_currJob->UpperCanister != eCC_NoPlug );

    // The 'real' report data is in the two subreports: one for the graph(s),
    // and one for comments. The main dataset will always be set to have 1
    // record. The remaining 'real' datasets will be set to have the number
    // of 'records' necessary to display the data.
    JobDataSet->RangeEndCount = 1;
    JobDataSet->RangeEnd      = reCount;

    // Set the range for hardware info recs
    // but we have it there for ease of implementation. It only has 1 rec.
    JobHWDataSet->RangeEndCount = m_currJob->NbrMPLSInfoRecs;
    JobHWDataSet->RangeEnd      = reCount;

    // Set the range counts for subreports
    JobCommentsDataSet->RangeEndCount = m_currJob->NbrJobComments;
    JobCommentsDataSet->RangeEnd      = reCount;

    // For the graph, the number of 'records' is the number of graphs we
    // breakdown the main graph in by the selected duration value.
    // First, get the range of dates in the job. Assume that the earliest
    // date is in the first RFC, and oldest in the last.
    if( m_currJob->NbrRFCRecords < 2 )
    {
        // Have nothing to display
        JobGraphDataSet->RangeEndCount = 0;
        JobGraphDataSet->RangeEnd      = reCount;
    }
    else
    {
        LOGGED_RFC_DATA firstRFC = { 0 };
        LOGGED_RFC_DATA lastRFC  = { 0 };

        m_currJob->GetRFCRecord( 0, firstRFC );
        m_currJob->GetRFCRecord( m_currJob->NbrRFCRecords - 1, lastRFC );

        // Round down the start time to a multiple of minutes
        m_tGraphStartTime = ( firstRFC.tRFC / 60 ) * 60;

        // Round up the end time to a multiple of minutes
        // (if it already isnt)
        if( ( lastRFC.tRFC % 60 ) != 0 )
            lastRFC.tRFC = ( lastRFC.tRFC / 60 + 1 ) * 60;

        if( m_rptOpts.iGraphHoursPerPage > 0 )
        {
            m_tGraphWidth = m_rptOpts.iGraphHoursPerPage * 60 * 60;

            time_t tGraphSpan = lastRFC.tRFC - m_tGraphStartTime;

            if( ( tGraphSpan % m_tGraphWidth ) == 0 )
                JobGraphDataSet->RangeEndCount = tGraphSpan / m_tGraphWidth;
            else
                JobGraphDataSet->RangeEndCount = tGraphSpan / m_tGraphWidth + 1;

            JobGraphDataSet->RangeEnd = reCount;
        }
        else
        {
            // Display entire graph in this case.
            m_tGraphWidth = 0;

            JobGraphDataSet->RangeEndCount = 1;
            JobGraphDataSet->RangeEnd      = reCount;
        }
    }

    // The report will use the graph form to render images. Save the
    // current state of the form
    TGraphForm::GRAPH_STATE graphState = { 0 };

    m_graphForm->GetGraphState( graphState );
    m_graphForm->SuspendUpdates();

    // Preview the report
    try
    {
        JobReport->ShowReport( true );
    }
    catch( ... )
    {
    }

    // Restore graph state
    m_graphForm->SetGraphState( graphState );
    m_graphForm->ResumeUpdates();
}


void __fastcall TMPLSJobRptDlg::JobDataSetGetValue(const UnicodeString VarName, Variant &Value)
{
    RptGetJobInfoField( m_currJob, m_jobInfo, VarName, Value );
}


void __fastcall TMPLSJobRptDlg::JobHWDataSetGetValue(const UnicodeString VarName, Variant &Value)
{
    RptGetMPLSInfoRecField( m_currJob, JobHWDataSet->RecNo, VarName, Value );
}


void __fastcall TMPLSJobRptDlg::CommentsDataSetGetValue(const UnicodeString VarName, Variant &Value)
{
    RptGetJobCommentField( m_currJob, JobCommentsDataSet->RecNo, VarName, Value );
}


void __fastcall TMPLSJobRptDlg::JobGraphDataSetGetValue(const UnicodeString VarName, Variant &Value)
{
    // Placeholder if we are displaying any record by record text fields
}


void __fastcall TMPLSJobRptDlg::JobReportBeforePrint(TfrxReportComponent *Sender)
{
    // Only render graphs on the final pass
    if( !JobReport->Engine->FinalPass )
        return;

    // This is where we copy the graph to the report
    if( Sender->Name == "GraphPicture" )
    {
        // Set the graph up first
        int iGraphRecNbr = JobGraphDataSet->RecNo;

        TGraphForm::GRAPH_STATE graphState = { 0 };

        graphState.tStart     = m_tGraphStartTime + m_tGraphWidth * iGraphRecNbr;
        graphState.tViewWidth = m_tGraphWidth;

        graphState.bRPMSeriesVisible   = true;
        graphState.bTempSeriesVisible  = true;
        graphState.bEventSeriesVisible = true;

        graphState.bPlugDetSeriesVisible = m_rptOpts.bShowPlugDetCurve;

        graphState.bMastTankPressSeriesVisible = m_rptOpts.bShowPressCurves;
        graphState.bMastRegPressSeriesVisible  = m_rptOpts.bShowPressCurves;
        graphState.bMastBattVoltsSeriesVisible = m_rptOpts.bShowVoltsCurves;

        if( m_currJob->UpperCanister == eCC_NoPlug )
        {
            graphState.bSlvRegPressSeriesVisible  = false;
            graphState.bSlvTankPressSeriesVisible = false;
            graphState.bSlvBattVoltsSeriesVisible = false;
        }
        else
        {
            graphState.bSlvRegPressSeriesVisible  = m_rptOpts.bShowPressCurves;
            graphState.bSlvTankPressSeriesVisible = m_rptOpts.bShowPressCurves;
            graphState.bSlvBattVoltsSeriesVisible = m_rptOpts.bShowVoltsCurves;
        }

        // Expand the graph form to give decent resolution. In the future,
        // may consider making the PrintRes a user-settable option.
        //
        // Note on print res: this value was determined by experimentation
        // to get an image of reasonable quality to print on the report.
        TfrxPictureView* pPic = (TfrxPictureView*)Sender;

        const double PrintScaleFactor = 1.25;

        graphState.formHeight = (int)( pPic->Height * PrintScaleFactor );
        graphState.formWidth  = (int)( pPic->Width  * PrintScaleFactor );

        m_graphForm->SetGraphState( graphState );

        // Tell the main form to render the graph and copy it to the clipboard
        // Set graph state
        m_graphForm->CopyGraphToClipboard();

        if( Clipboard()->HasFormat( CF_BITMAP ) )
        {
            // Now copy the clipboard contents into the report graph picture
            pPic->Picture->Bitmap->Assign( Clipboard() );
        }
    }
    else if( Sender->Name == "GraphHeaderMemo" )
    {
        TfrxMemoView* aMemo = dynamic_cast <TfrxMemoView *>( Sender );

        if( aMemo != NULL )
        {
            // Clear the memo, then add the text
            aMemo->Lines->Clear();
            aMemo->Memo->Add( "Graphs (" + IntToStr( JobGraphDataSet->RecNo + 1 ) + " of " + IntToStr( JobGraphDataSet->RecordCount() ) + ")"  );
        }
    }
}


void TMPLSJobRptDlg::ShowSlaveHWRow( bool bSlaveVisible )
{
    // Design position of the Slave and Plug Det objects are as follows:
    //
    //   <TfrxMasterData Name="HWMasterData"   Height="192.75598118" Left="0" Top="83.14966" Width="980.410082" ColumnWidth="0" ColumnGap="0" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" FooterAfterEach="True" KeepChild="True" KeepHeader="True" RowCount="0">
    //
    //   <TfrxShapeView Name="SlaveUnitRect"                          Left="7.55906"   Top="117.16543" Width="960.00062"    Height="41.57483" ShowHint="False"/>
    //   <TfrxMemoView Name="SlaveTitleMemo"                          Left="7.55906"   Top="117.16543" Width="94.48818898"  Height="41.57483" ShowHint="False" Font.Charset="1" Font.Color="0" Font.Height="-13" Font.Name="Arial" Font.Style="1" GapX="4" ParentFont="False" VAlign="vaCenter" Text="Slave Unit"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlaveTEC_SN"         Left="102.04731" Top="117.16543" Width="188.97637795" Height="18.89765" ShowHint="False" DataField="DevInfo_SlaveTEC_SN" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" GapX="4" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlaveTEC_SN&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlaveTEC_HWRev"      Left="291.02381" Top="117.16543" Width="64.2519685"   Height="18.89765" ShowHint="False" DataField="DevInfo_SlaveTEC_HWRev" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" GapX="4" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlaveTEC_HWRev&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlavePIB_FWRev"      Left="483.77984" Top="117.16543" Width="64.2519685"   Height="18.89765" ShowHint="False" DataField="DevInfo_SlavePIB_FWRev" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" GapX="4" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlavePIB_FWRev&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlaveTEC_TankOffset" Left="548.03185" Top="117.16543" Width="68.03149606"  Height="18.89765" ShowHint="False" DataField="DevInfo_SlaveTEC_TankOffset" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" GapX="4" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlaveTEC_TankOffset&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlaveTEC_TankSpan"   Left="616.06339" Top="117.16543" Width="98.26771654"  Height="18.89765" ShowHint="False" DataField="DevInfo_SlaveTEC_TankSpan" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" GapX="4" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlaveTEC_TankSpan&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlaveTEC_RegOffset"  Left="714.33117" Top="117.16543" Width="68.03149606"  Height="18.89765" ShowHint="False" DataField="DevInfo_SlaveTEC_RegOffset" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" GapX="4" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlaveTEC_RegOffset&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlaveTEC_RegSpan"    Left="782.36271" Top="117.16543" Width="98.26771654"  Height="18.89765" ShowHint="False" DataField="DevInfo_SlaveTEC_RegSpan" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" GapX="4" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlaveTEC_RegSpan&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlaveTEC_CalVer"     Left="102.04731" Top="138.84259535" Width="188.97647559" Height="18.89765" ShowHint="False" DataField="DevInfo_SlaveTEC_CalVer" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" GapX="4" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlaveTEC_CalVer&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlaveTEC_FWRev"      Left="355.27582" Top="117.16543" Width="64.2519685"   Height="18.89765" ShowHint="False" DataField="DevInfo_SlaveTEC_FWRev" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" Font.Charset="1" Font.Color="0" Font.Height="-13" Font.Name="Arial" Font.Style="0" GapX="4" ParentFont="False" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlaveTEC_FWRev&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlavePIB_HWRev"      Left="419.52783" Top="117.16543" Width="64.2519685"   Height="18.89765" ShowHint="False" DataField="DevInfo_SlavePIB_HWRev" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" Font.Charset="1" Font.Color="0" Font.Height="-13" Font.Name="Arial" Font.Style="0" GapX="4" ParentFont="False" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlavePIB_HWRev&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_SlaveTEC_BattType"   Left="880.63049" Top="117.16543" Width="83.1496063"   Height="18.89765" ShowHint="False" DataField="DevInfo_SlaveTEC_BattType" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" GapX="4" WordWrap="False" Text="[JobHWDataSet.&#34;DevInfo_SlaveTEC_BattType&#34;]"/>
    //   <TfrxLineView Name="SlaveLine1"                              Left="102.04731" Top="117.16543" Width="0"            Height="41.57483" ShowHint="False" Frame.Typ="1"/>
    //      through to
    //   <TfrxLineView Name="SlaveLine10"                             Left="880.63049" Top="117.16543" Width="0"            Height="41.57483" ShowHint="False" Frame.Typ="1"/>
    //
    //   <TfrxShapeView Name="PlugDetRect"                            Left="7.55906"   Top="158.74026" Width="411.96877"    Height="30.23622047" ShowHint="False"/>
    //   <TfrxMemoView Name="PlugDetTitleMemo"                        Left="7.55906"   Top="158.74026" Width="94.48818898"  Height="30.23622047" ShowHint="False" Font.Charset="1" Font.Color="0" Font.Height="-13" Font.Name="Arial" Font.Style="1" GapX="4" ParentFont="False" VAlign="vaCenter" Text="Plug Detector"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_PlugDet_SN"          Left="102.04731" Top="158.74026" Width="188.97637795" Height="28.34645669" ShowHint="False" DataField="DevInfo_PlugDet_SN" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" Font.Charset="1" Font.Color="0" Font.Height="-13" Font.Name="Arial" Font.Style="0" GapX="4" ParentFont="False" WordWrap="False" VAlign="vaCenter" Text="[JobHWDataSet.&#34;DevInfo_PlugDet_SN&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_PlugDet_HWRev"       Left="291.02381" Top="158.74026" Width="64.2519685"   Height="28.34645669" ShowHint="False" DataField="DevInfo_PlugDet_HWRev" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" Font.Charset="1" Font.Color="0" Font.Height="-13" Font.Name="Arial" Font.Style="0" GapX="4" ParentFont="False" VAlign="vaCenter" Text="[JobHWDataSet.&#34;DevInfo_PlugDet_HWRev&#34;]"/>
    //   <TfrxMemoView Name="JobHWDataSetDevInfo_PlugDet_FWRev"       Left="355.27582" Top="158.74026" Width="64.2519685"   Height="28.34645669" ShowHint="False" DataField="DevInfo_PlugDet_FWRev" DataSet="JobHWDataSet" DataSetName="JobHWDataSet" Font.Charset="1" Font.Color="0" Font.Height="-13" Font.Name="Arial" Font.Style="0" GapX="4" ParentFont="False" VAlign="vaCenter" Text="[JobHWDataSet.&#34;DevInfo_PlugDet_FWRev&#34;]"/>
    //   <TfrxLineView Name="PlugDetLine1"                            Left="102.04731" Top="158.74026" Width="0"            Height="30.23622047" ShowHint="False" Frame.Typ="1"/>
    //   <TfrxLineView Name="PlugDetLine2"                            Left="291.02381" Top="158.74026" Width="0"            Height="30.23622047" ShowHint="False" Frame.Typ="1"/>
    //   <TfrxLineView Name="PlugDetLine3"                            Left="355.27582" Top="158.74026" Width="0"            Height="30.23622047" ShowHint="False" Frame.Typ="1"/>

    // Design note: normally good practice calls for checking if a dynamic_cast<>
    // has resulted in a NULL value. However, the report objects never change and
    // therefore if the value after cast is not NULL then they will never be NULL.
    // If a fault happens during testing, then we've screwed up a casting here.
    //
    // Also, to prevent a problem with setting positions based on the wrong units
    // of measure, instead of using the values above (which are FastReport internal values)
    // we use the values stored in the Top and Height properties of objects at
    // run time.

    // First, set the band height if we are showing the row
    TfrxMasterData* pBand        = dynamic_cast<TfrxMasterData*>( JobReport->FindObject( "HWMasterData" ) );
    TfrxShapeView*  pMasterRect  = dynamic_cast<TfrxShapeView*> ( JobReport->FindObject( "MasterUnitRect" ) );
    TfrxShapeView*  pPlugDetRect = dynamic_cast<TfrxShapeView*> ( JobReport->FindObject( "PlugDetRect" ) );

    if( bSlaveVisible )
    {
        // Band height is set to include two unit rows plus the plug det row plus some extra space at the bottom
        pBand->Height = pMasterRect->Top + 2.0 * pMasterRect->Height + 1.125 * pPlugDetRect->Height;
    }

    // Next set the visibility of slave unit objects
    static const String SlaveObjects[] = {
        "SlaveUnitRect",
        "SlaveTitleMemo",
        "JobHWDataSetDevInfo_SlaveTEC_SN",
        "JobHWDataSetDevInfo_SlaveTEC_HWRev",
        "JobHWDataSetDevInfo_SlavePIB_FWRev",
        "JobHWDataSetDevInfo_SlaveTEC_TankOffset",
        "JobHWDataSetDevInfo_SlaveTEC_TankSpan",
        "JobHWDataSetDevInfo_SlaveTEC_RegOffset",
        "JobHWDataSetDevInfo_SlaveTEC_RegSpan",
        "JobHWDataSetDevInfo_SlaveTEC_CalVer",
        "JobHWDataSetDevInfo_SlaveTEC_FWRev",
        "JobHWDataSetDevInfo_SlavePIB_HWRev",
        "JobHWDataSetDevInfo_SlaveTEC_BattType",
        "SlaveLine1",
        "SlaveLine2",
        "SlaveLine3",
        "SlaveLine4",
        "SlaveLine5",
        "SlaveLine6",
        "SlaveLine7",
        "SlaveLine8",
        "SlaveLine9",
        "SlaveLine10",
        ""
    };

    int iIndex = 0;

    while( !SlaveObjects[iIndex].IsEmpty() )
    {
        TfrxView* pView = dynamic_cast<TfrxView*>( JobReport->FindObject( SlaveObjects[iIndex] ) );

        pView->Visible = bSlaveVisible;

        iIndex++;
    }

    // Next shift the plug detector items. This row is always visible
   static const String PlugDetObjects[] = {
        "PlugDetRect",
        "PlugDetTitleMemo",
        "JobHWDataSetDevInfo_PlugDet_SN",
        "JobHWDataSetDevInfo_PlugDet_HWRev",
        "JobHWDataSetDevInfo_PlugDet_FWRev",
        "PlugDetLine1",
        "PlugDetLine2",
        "PlugDetLine3",
        ""
    };

    TfrxView* pSlaveRect = dynamic_cast<TfrxView*>( JobReport->FindObject( "SlaveUnitRect" ) );

    if( pSlaveRect != NULL )
    {
        double dPlugDetTop;

        if( bSlaveVisible )
            dPlugDetTop = pSlaveRect->Top + pSlaveRect->Height;
        else
            dPlugDetTop = pSlaveRect->Top;

        int iIndex = 0;

        while( !PlugDetObjects[iIndex].IsEmpty() )
        {
            TfrxView* pView = dynamic_cast<TfrxView*>( JobReport->FindObject( PlugDetObjects[iIndex] ) );

            pView->Top = dPlugDetTop;

            iIndex++;
        }
    }

    // Last, shrink the band height if we just hid the row
    if( !bSlaveVisible )
    {
        // Band height is set to include two unit rows plus the plug det row plus some extra space at the bottom
        pBand->Height = pMasterRect->Top + pMasterRect->Height + 1.125 * pPlugDetRect->Height;
    }
}


void __fastcall TMPLSJobRptDlg::JobReportPreview(TObject *Sender)
{
    TfrxReport* frxReport = (TfrxReport*)Sender;
    frxReport->PreviewForm->BorderIcons = TBorderIcons() << biSystemMenu << biMaximize;
}

