//---------------------------------------------------------------------------
#ifndef TRptMPLSJobDlgH
#define TRptMPLSJobDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "frxClass.hpp"
#include "TGraphForm.h"
#include "TBasePlugJob.h"
//---------------------------------------------------------------------------
class TMPLSJobRptDlg : public TForm
{
__published: // IDE-managed Components
    TfrxUserDataSet *JobGraphDataSet;
    TfrxReport *JobReport;
    TfrxUserDataSet *JobCommentsDataSet;
    TfrxUserDataSet *JobDataSet;
    TfrxUserDataSet *JobHWDataSet;
    void __fastcall JobReportBeforePrint(TfrxReportComponent *Sender);
    void __fastcall JobReportPreview(TObject *Sender);
    void __fastcall JobDataSetGetValue(const UnicodeString VarName, Variant &Value);
    void __fastcall CommentsDataSetGetValue(const UnicodeString VarName, Variant &Value);
    void __fastcall JobGraphDataSetGetValue(const UnicodeString VarName, Variant &Value);
    void __fastcall JobHWDataSetGetValue(const UnicodeString VarName, Variant &Value);


public:

    typedef struct {
        bool bShowVoltsCurves;
        bool bShowPressCurves;
        bool bShowPlugDetCurve;
        int  iGraphHoursPerPage;
    } REPORT_OPTIONS;

private:

    TBasePlugJob*  m_currJob;
    TGraphForm*    m_graphForm;
    REPORT_OPTIONS m_rptOpts;
    time_t         m_tGraphStartTime;
    time_t         m_tGraphWidth;

    PLUG_JOB_INFO  m_jobInfo;

    void ShowSlaveHWRow( bool bRowVisible );

    void __fastcall GraphBandBeforePrint( TfrxReportComponent *Sender );

public:
    __fastcall TMPLSJobRptDlg(TComponent* Owner);

    void ShowReport( TBasePlugJob* currJob, TGraphForm* pGraphForm, const REPORT_OPTIONS& rptOptions );
};
//---------------------------------------------------------------------------
extern PACKAGE TMPLSJobRptDlg *MPLSJobRptDlg;
//---------------------------------------------------------------------------
#endif
