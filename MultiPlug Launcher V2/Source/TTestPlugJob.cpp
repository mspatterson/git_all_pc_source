#include <vcl.h>
#pragma hdrstop

#include "TTestPlugJob.h"
#include "MPLRegistryInterface.h"
#include "ApplUtils.h"


#pragma package(smart_init)


__fastcall TTestPlugJob::TTestPlugJob( const String& sJobFileName ) : TBasePlugJob( sJobFileName )
{
    // After the common constructor does its work, check if our
    // job loaded and if it was in fact a test job
    if( m_jobLoaded )
    {
        // If this isn't a test job, clear everything out. This
        // will reset the 'is loaded' flag so that the caller
        // knows there is a problem
        if( !m_jobInfo.isTestJob )
            InitJobInfo();
    }
}


__fastcall TTestPlugJob::~TTestPlugJob()
{
}


void TTestPlugJob::SaveBackup( void )
{
    // Test jobs do not do backups
}

