#ifndef TTestPlugJobH
#define TTestPlugJobH

//
// TTestPlugJob is a special job object used when we are in test mode
// (eg, that a job isn't really open.
//

#include "TBasePlugJob.h"


class TTestPlugJob : public TBasePlugJob
{
  private:

  protected:

  public:
    virtual __fastcall TTestPlugJob( const String& sJobFileName );
    virtual __fastcall ~TTestPlugJob();
       // Common form of constructor for a plug job. The job must
       // already exist

    // Backup
    virtual void SaveBackup( void );
        // Test jobs do not create backup files

};


#endif
