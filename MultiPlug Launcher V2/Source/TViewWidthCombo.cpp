#include <vcl.h>
#pragma hdrstop

#include "TViewWidthCombo.h"

#pragma package(smart_init)


const int TViewWidthCombo::ViewEnterJobValue = -1;


typedef struct {
    String sCaption;
    int    widthInSecs;
} VIEW_WIDTH_ITEM;

const VIEW_WIDTH_ITEM ViewWidthInfo[] = {
    // Caption         // Width in secs
    { "1 minute",      60 },
    { "2 minutes",     2 * 60 },
    { "5 minutes",     5 * 60 },
    { "15 minutes",    15 * 60 },
    { "30 minutes",    30 * 60 },
    { "1 hour",        60 * 60 },
    { "2 hours",       2 * 60 * 60 },
    { "3 hours",       3 * 60 * 60 },
    { "4 hours",       4 * 60 * 60 },
    { "5 hours",       5 * 60 * 60 },
    { "6 hours",       6 * 60 * 60 },
    { "Entire Job",    TViewWidthCombo::ViewEnterJobValue },
    { "",              0 }
};


__fastcall TViewWidthCombo::TViewWidthCombo( TComponent* pOwner, TWinControl* pParent )
{
    m_pCombo = new TComboBox( pOwner );

    m_pCombo->Parent    = pParent;
    m_pCombo->Style     = csDropDownList;
    m_pCombo->OnCloseUp = WidthComboCloseUp;

    m_pCombo->Items->Clear();

    int iItem = 0;

    while( ViewWidthInfo[iItem].widthInSecs != 0 )
    {
        m_pCombo->Items->AddObject( ViewWidthInfo[iItem].sCaption, (TObject*)ViewWidthInfo[iItem].widthInSecs );
        iItem++;
    }

    m_pCombo->ItemIndex = 0;

    // Init cachec vars by faking a click
    WidthComboCloseUp( m_pCombo );
}


__fastcall TViewWidthCombo::~TViewWidthCombo()
{
}



void __fastcall TViewWidthCombo::SetViewWidth( int iWidthInSecs )
{
    int iItem = 0;

    while( ViewWidthInfo[iItem].widthInSecs != 0 )
    {
        if( ViewWidthInfo[iItem].widthInSecs == iWidthInSecs )
        {
            m_iCurrViewWidth = iWidthInSecs;

            m_pCombo->ItemIndex = iItem;

            WidthComboCloseUp( m_pCombo );

            return;
        }

        iItem++;
    }
}


int __fastcall TViewWidthCombo::GetViewWidth( void )
{
    // Return the cached value of the view width
    return m_iCurrViewWidth;
}


void __fastcall TViewWidthCombo::WidthComboCloseUp(TObject *Sender)
{
    // When the combo closes, cache the value of the width.
    m_iCurrViewWidth = ViewEnterJobValue;

//    String sDebugMsg = "m_pCombText: " + m_pCombo->Text;
//    OutputDebugString( sDebugMsg.c_str() );
//
    int iItem = 0;

    while( ViewWidthInfo[iItem].widthInSecs != 0 )
    {
        if( m_pCombo->Text == ViewWidthInfo[iItem].sCaption )
        {
            m_iCurrViewWidth = ViewWidthInfo[iItem].widthInSecs;
            break;
        }

        iItem++;
    }

    if( m_onViewChange )
        m_onViewChange( this );
}
