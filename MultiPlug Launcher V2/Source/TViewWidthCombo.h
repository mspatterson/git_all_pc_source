//---------------------------------------------------------------------------
#ifndef TViewWidthComboH
#define TViewWidthComboH
//---------------------------------------------------------------------------

class TViewWidthCombo : public TObject
{
  private:

  protected:

    TComboBox*   m_pCombo;

    TNotifyEvent m_onViewChange;
    int          m_iCurrViewWidth;

    void __fastcall SetViewWidth( int iWidthInSecs );
    int  __fastcall GetViewWidth( void );

    void __fastcall WidthComboCloseUp( TObject *Sender );

    int  GetTop( void )        { return m_pCombo->Top; }
    void SetTop( int iNewVal ) { m_pCombo->Top = iNewVal; }

    int  GetLeft( void )        { return m_pCombo->Left; }
    void SetLeft( int iNewVal ) { m_pCombo->Left = iNewVal; }

    int  GetWidth( void )        { return m_pCombo->Width; }
    void SetWidth( int iNewVal ) { m_pCombo->Width = iNewVal; }

    int  GetHeight( void )        { return m_pCombo->Height; }
    void SetHeight( int iNewVal ) { m_pCombo->Height = iNewVal; }

  public:
    virtual __fastcall TViewWidthCombo( TComponent* pOwner, TWinControl* pParent );
    virtual __fastcall ~TViewWidthCombo();

    __property int Top    = { read = GetTop,    write = SetTop };
    __property int Left   = { read = GetLeft,   write = SetLeft };
    __property int Width  = { read = GetWidth,  write = SetWidth };
    __property int Height = { read = GetHeight, write = SetHeight };

    __property int          ViewWidth         = { read = GetViewWidth,   write = SetViewWidth };
    __property TNotifyEvent OnViewWidthChange = { read = m_onViewChange, write = m_onViewChange };

    static const int ViewEnterJobValue;

};

#endif
