#include <vcl.h>
#pragma hdrstop

#include "TViewWidthForm.h"

#pragma package(smart_init)
#pragma resource "*.dfm"

TViewWidthForm *ViewWidthForm;


static const int ViewEnterJobValue = -1;

typedef struct {
    String sCaption;
    int    widthInSecs;
} VIEW_WIDTH_ITEM;

const VIEW_WIDTH_ITEM ViewWidthInfo[] = {
    // Caption         // Width in secs
    { "1 minute",      60 },
    { "2 minutes",     2 * 60 },
    { "5 minutes",     5 * 60 },
    { "15 minutes",    15 * 60 },
    { "30 minutes",    30 * 60 },
    { "1 hour",        60 * 60 },
    { "2 hours",       2 * 60 * 60 },
    { "3 hours",       3 * 60 * 60 },
    { "4 hours",       4 * 60 * 60 },
    { "5 hours",       5 * 60 * 60 },
    { "6 hours",       6 * 60 * 60 },
    { "Entire Job",    ViewEnterJobValue },
    { "",              0 }
};


__fastcall TViewWidthForm::TViewWidthForm(TComponent* Owner, TWinControl* pParent ) : TForm(Owner)
{
    Parent = pParent;

    WidthCombo->Items->Clear();

    int iItem = 0;

    while( ViewWidthInfo[iItem].widthInSecs != 0 )
    {
        WidthCombo->Items->AddObject( ViewWidthInfo[iItem].sCaption, (TObject*)ViewWidthInfo[iItem].widthInSecs );
        iItem++;
    }

    WidthCombo->ItemIndex = 0;

    // Init cachec vars by faking a click
    WidthComboCloseUp( WidthCombo );
}


void __fastcall TViewWidthForm::CloseBtnClick(TObject *Sender)
{
    Visible = false;
}


void __fastcall TViewWidthForm::SetViewWidth( int iWidthInSecs )
{
    int iItem = 0;

    while( ViewWidthInfo[iItem].widthInSecs != 0 )
    {
        if( ViewWidthInfo[iItem].widthInSecs == iWidthInSecs )
        {
            m_iCurrViewWidth = iWidthInSecs;

            WidthCombo->ItemIndex = iItem;

            WidthComboCloseUp( WidthCombo );

            return;
        }

        iItem++;
    }
}


int __fastcall TViewWidthForm::GetViewWidth( void )
{
    // Return the cached value of the view width
    return m_iCurrViewWidth;
}


void __fastcall TViewWidthForm::WidthComboClick(TObject *Sender)
{
//    if( m_onViewChange )
//        m_onViewChange( this );
}


void __fastcall TViewWidthForm::WidthComboCloseUp(TObject *Sender)
{
    // When the combo closes, cache the value of the width.
    m_iCurrViewWidth = ViewEnterJobValue;

//    String sDebugMsg = "WidthComboText: " + WidthCombo->Text;
//    OutputDebugString( sDebugMsg.c_str() );
//
    int iItem = 0;

    while( ViewWidthInfo[iItem].widthInSecs != 0 )
    {
        if( WidthCombo->Text == ViewWidthInfo[iItem].sCaption )
        {
            m_iCurrViewWidth = ViewWidthInfo[iItem].widthInSecs;
            break;
        }

        iItem++;
    }

    if( m_onViewChange )
        m_onViewChange( this );
}

