object ViewWidthForm: TViewWidthForm
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = ' View Width'
  ClientHeight = 96
  ClientWidth = 151
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    151
    96)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 17
    Top = 16
    Width = 117
    Height = 13
    Caption = 'Select Graph View Width'
  end
  object CloseBtn: TButton
    Left = 41
    Top = 63
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Close'
    TabOrder = 0
    OnClick = CloseBtnClick
  end
  object WidthCombo: TComboBox
    Left = 16
    Top = 32
    Width = 120
    Height = 21
    Style = csDropDownList
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 1
    OnClick = WidthComboClick
    OnCloseUp = WidthComboCloseUp
    Items.Strings = (
      '1 minute'
      '2 minutes'
      '5 minutes'
      '15 minutes'
      '30 minutes'
      '1 hour'
      '2 hours'
      '3 hours'
      '4 hours'
      '5 hours'
      '6 hours'
      'Entire Job')
  end
end
