//---------------------------------------------------------------------------
#ifndef TViewWidthFormH
#define TViewWidthFormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class TViewWidthForm : public TForm
{
__published:	// IDE-managed Components
    TButton *CloseBtn;
    TComboBox *WidthCombo;
    TLabel *Label1;
    void __fastcall CloseBtnClick(TObject *Sender);
    void __fastcall WidthComboClick(TObject *Sender);
    void __fastcall WidthComboCloseUp(TObject *Sender);

private:
    TNotifyEvent m_onViewChange;
    int          m_iCurrViewWidth;

    void __fastcall SetViewWidth( int iWidthInSecs );
    int  __fastcall GetViewWidth( void );

public:		// User declarations
    __fastcall TViewWidthForm(TComponent* Owner, TWinControl* pParent );

    __property int          ViewWidth         = { read = GetViewWidth,   write = SetViewWidth };
    __property TNotifyEvent OnViewWidthChange = { read = m_onViewChange, write = m_onViewChange };
};
//---------------------------------------------------------------------------
extern PACKAGE TViewWidthForm *ViewWidthForm;
//---------------------------------------------------------------------------
#endif
