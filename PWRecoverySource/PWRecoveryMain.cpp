#include <vcl.h>
#pragma hdrstop

#include "PWRecoveryMain.h"
#include "TesTorkManagerPWGen.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TMainForm *MainForm;


// Constructor - Do nothing
__fastcall TMainForm::TMainForm(TComponent* Owner) : TForm(Owner)
{
}


// On show - Generate and show the password
void __fastcall TMainForm::FormShow(TObject *Sender)
{
    PasswordEdit->Text = GeneratePassword();
}


void __fastcall TMainForm::DatePickerChange(TObject *Sender)
{
    PasswordEdit->Text = GeneratePassword( DatePicker->Date );
}
