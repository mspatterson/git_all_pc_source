object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'TesTORK Manager Password Recovery Tool'
  ClientHeight = 93
  ClientWidth = 396
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  DesignSize = (
    396
    93)
  PixelsPerInch = 96
  TextHeight = 13
  object PasswordLabel: TLabel
    Left = 16
    Top = 25
    Width = 67
    Height = 19
    Caption = 'Password'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object PasswordEdit: TEdit
    Left = 112
    Top = 22
    Width = 268
    Height = 27
    Anchors = [akLeft, akTop, akRight]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    ExplicitWidth = 271
  end
  object DatePicker: TDateTimePicker
    Left = 301
    Top = 61
    Width = 79
    Height = 21
    Date = 42235.000000000000000000
    Time = 42235.000000000000000000
    TabOrder = 1
    OnChange = DatePickerChange
  end
end
