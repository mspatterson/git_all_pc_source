#ifndef PWRecoveryMainH
#define PWRecoveryMainH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <System.DateUtils.hpp>
#include <Vcl.ComCtrls.hpp>


class TMainForm : public TForm
{
__published:    // IDE-managed Components
    TLabel *PasswordLabel;
    TEdit *PasswordEdit;
    TDateTimePicker *DatePicker;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall DatePickerChange(TObject *Sender);

private:        // User declarations

public:         // User declarations
    __fastcall TMainForm(TComponent* Owner);

};

extern PACKAGE TMainForm *MainForm;

#endif
