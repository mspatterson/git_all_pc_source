//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("..\Link Tilt Source\TProjectLogDlg.cpp", ProjectLogForm);
USEFORM("..\Link Tilt Source\TLinkTiltSettingsDlg.cpp", LinkTiltSettingsDlg);
USEFORM("..\Link Tilt Source\TLinkTiltMain.cpp", LinkTiltMainForm);
USEFORM("..\Link Tilt Source\TAboutLinkTiltDlg.cpp", AboutMPLForm);
USEFORM("..\Common\ChangePasswordDlg.cpp", ChangePasswordForm);
USEFORM("..\Common\NewBattDlg.cpp", NewBattForm);
USEFORM("..\Common\PasswordInputDlg.cpp", PasswordInputForm);
//---------------------------------------------------------------------------
static BOOL CALLBACK EnumWindowsProc( HWND hwnd, LPARAM lParam )
{
    TCHAR class_name[256];

    GetClassName( hwnd, class_name, sizeof(class_name) / sizeof(TCHAR) );

    if( lstrcmp( class_name, _T("TLinkTiltMainForm") ) == 0 )
    {
        bool* pProcFound = (bool*)lParam;
        *pProcFound = true;

        // Restore the previous instance
        if( IsIconic( hwnd ) )
            ShowWindow( hwnd, SW_RESTORE );

        SetForegroundWindow( hwnd );

        // Return false to indicate enumeration should stop
        return FALSE;
    }

    // Return true to continue enumeration
    return TRUE;
}


int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
    // First check for a currently executing version of the code.
    bool previousFound = false;

    EnumWindows( (WNDENUMPROC)EnumWindowsProc, (LPARAM)(&previousFound) );

    if( previousFound )
        return 1;

    // Disable sleep while we are running
    EXECUTION_STATE esPreExState = SetThreadExecutionState( ES_CONTINUOUS | ES_DISPLAY_REQUIRED | ES_SYSTEM_REQUIRED );

    // Continue with boot here
    try
    {
         Application->Initialize();
         Application->MainFormOnTaskBar = true;
         Application->Title = "TESCO Link Tilt Manager";
         Application->CreateForm(__classid(TLinkTiltMainForm), &LinkTiltMainForm);
         Application->CreateForm(__classid(TAboutMPLForm), &AboutMPLForm);
         Application->CreateForm(__classid(TProjectLogForm), &ProjectLogForm);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }

    // Restore system execution state
    SetThreadExecutionState( esPreExState );

    return 0;
}
//---------------------------------------------------------------------------
