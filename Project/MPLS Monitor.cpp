//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("..\MultiPlug Launcher V2\MonSource\TMPLSMonMain.cpp", MPLSMonMain);
USEFORM("..\MultiPlug Launcher V2\MonSource\AutoConnDlg.cpp", AutoConnForm);
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
    try
    {
         Application->Initialize();
         Application->MainFormOnTaskBar = true;
         Application->Title = "TesTORK Monitor";
         Application->CreateForm(__classid(TMPLSMonMain), &MPLSMonMain);
         Application->CreateForm(__classid(TAutoConnForm), &AutoConnForm);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }
    return 0;
}
//---------------------------------------------------------------------------
