//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("..\MultiPlug Launcher V2\Source\TAboutMPLDlg.cpp", AboutMPLForm);
USEFORM("..\MultiPlug Launcher V2\Source\MPLSettingsDlg.cpp", SystemSettingsForm);
USEFORM("..\MultiPlug Launcher V2\Source\TGraphForm.cpp", GraphForm);
USEFORM("..\MultiPlug Launcher V2\Source\TAckDlg.cpp", AckForm);
USEFORM("..\MultiPlug Launcher V2\Source\TRptMPLSJobDlg.cpp", MPLSJobRptDlg);
USEFORM("..\MultiPlug Launcher V2\Source\TRptJobLogDlg.cpp", JobLogReportForm);
USEFORM("..\MultiPlug Launcher V2\Source\TReportSetupDlg.cpp", ReportSetupDlg);
USEFORM("..\MultiPlug Launcher V2\Source\TMPLSManagerMain.cpp", MPLMainForm);
USEFORM("..\MultiPlug Launcher V2\Source\TProjectLogDlg.cpp", ProjectLogForm);
USEFORM("..\MultiPlug Launcher V2\Source\TPlugJobsDlg.cpp", PlugJobsForm);
USEFORM("..\MultiPlug Launcher V2\Source\TPlugJobDetailsDlg.cpp", PlugJobDetailsForm);
USEFORM("..\Common\ChangePasswordDlg.cpp", ChangePasswordForm);
USEFORM("..\Common\PasswordInputDlg.cpp", PasswordInputForm);
USEFORM("..\Common\NewBattDlg.cpp", NewBattForm);
USEFORM("..\Common\TConfirmTimeDlg.cpp", ConfirmTimeDlg);
//---------------------------------------------------------------------------
static BOOL CALLBACK EnumWindowsProc( HWND hwnd, LPARAM lParam )
{
    TCHAR class_name[256];

    GetClassName( hwnd, class_name, sizeof(class_name) / sizeof(TCHAR) );

    if( lstrcmp( class_name, _T("TMPLMainForm") ) == 0 )
    {
        bool* pProcFound = (bool*)lParam;
        *pProcFound = true;

        // Restore the previous instance
        if( IsIconic( hwnd ) )
            ShowWindow( hwnd, SW_RESTORE );

        SetForegroundWindow( hwnd );

        // Return false to indicate enumeration should stop
        return FALSE;
    }

    // Return true to continue enumeration
    return TRUE;
}


int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
    // First check for a currently executing version of the code.
    bool previousFound = false;

    EnumWindows( (WNDENUMPROC)EnumWindowsProc, (LPARAM)(&previousFound) );

    if( previousFound )
        return 1;

    // Disable sleep while we are running
    EXECUTION_STATE esPreExState = SetThreadExecutionState( ES_CONTINUOUS | ES_DISPLAY_REQUIRED | ES_SYSTEM_REQUIRED );

    // Continue with boot here
    try
    {
         Application->Initialize();
         Application->MainFormOnTaskBar = true;
         Application->Title = "TESCO Multi Plug Launcher Software";
         Application->CreateForm(__classid(TMPLMainForm), &MPLMainForm);
         Application->CreateForm(__classid(TSystemSettingsForm), &SystemSettingsForm);
         Application->CreateForm(__classid(TAboutMPLForm), &AboutMPLForm);
         Application->CreateForm(__classid(TAckForm), &AckForm);
         Application->CreateForm(__classid(TPlugJobsForm), &PlugJobsForm);
         Application->CreateForm(__classid(TProjectLogForm), &ProjectLogForm);
         Application->CreateForm(__classid(TReportSetupDlg), &ReportSetupDlg);
         Application->CreateForm(__classid(TPlugJobDetailsForm), &PlugJobDetailsForm);
         Application->CreateForm(__classid(TGraphForm), &GraphForm);
         Application->CreateForm(__classid(TMPLSJobRptDlg), &MPLSJobRptDlg);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }

    // Restore system execution state
    SetThreadExecutionState( esPreExState );

    return 0;
}
//---------------------------------------------------------------------------
