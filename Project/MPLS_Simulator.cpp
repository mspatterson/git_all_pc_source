//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("..\MultiPlug Launcher V2\Simulator\MPLSSimMain.cpp", MPLSimMainForm);
USEFORM("..\MultiPlug Launcher V2\Simulator\TCalFactorsDlg.cpp", CalFactorsDlg);
USEFORM("..\MultiPlug Launcher V2\Simulator\TTECFrame.cpp", TECFrame); /* TFrame: File Type */
USEFORM("..\Common\TSliderFrame.cpp", SliderFrame); /* TFrame: File Type */
USEFORM("..\Common\TLoggingFrame.cpp", LoggingFrame); /* TFrame: File Type */
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
    try
    {
         Application->Initialize();
         Application->MainFormOnTaskBar = true;
         Application->Title = "MPL Simulator";
         Application->CreateForm(__classid(TMPLSimMainForm), &MPLSimMainForm);
         Application->CreateForm(__classid(TCalFactorsDlg), &CalFactorsDlg);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }
    return 0;
}
//---------------------------------------------------------------------------
