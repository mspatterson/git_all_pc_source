//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("..\MultiPlug Launcher Sim\MPLSimMain.cpp", MPLSimMainForm);
USEFORM("..\MultiPlug Launcher Sim\RFCDataDlg.cpp", RFCDataForm);
USEFORM("..\MultiPlug Launcher Sim\TLoggingForm.cpp", LoggingForm);
USEFORM("..\MultiPlug Launcher Sim\CalFactorsDlg.cpp", CalFactorsForm);
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
    try
    {
         Application->Initialize();
         Application->MainFormOnTaskBar = true;
         Application->Title = "MPL Simulator";
         Application->CreateForm(__classid(TMPLSimMainForm), &MPLSimMainForm);
         Application->CreateForm(__classid(TCalFactorsForm), &CalFactorsForm);
         Application->CreateForm(__classid(TRFCDataForm), &RFCDataForm);
         Application->CreateForm(__classid(TLoggingForm), &LoggingForm);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }
    return 0;
}
//---------------------------------------------------------------------------
