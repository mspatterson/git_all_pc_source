//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("..\MonSource\MonMain.cpp", MainForm);
USEFORM("..\MonSource\AutoConnDlg.cpp", AutoConnForm);
USEFORM("..\MonSource\TTempCompForm.cpp", TempCompForm);
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
    try
    {
         Application->Initialize();
         Application->MainFormOnTaskBar = true;
         Application->Title = "TesTORK Monitor";
         Application->CreateForm(__classid(TMainForm), &MainForm);
         Application->CreateForm(__classid(TTempCompForm), &TempCompForm);
         Application->CreateForm(__classid(TAutoConnForm), &AutoConnForm);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }
    return 0;
}
//---------------------------------------------------------------------------
