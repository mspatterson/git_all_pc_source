//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("..\Source\main.cpp", TesTORKMgrMainForm);
USEFORM("..\Source\MainCommentsDlg.cpp", MainCommentForm);
USEFORM("..\Source\RptConnDlg.cpp", RptConnForm);
USEFORM("..\Source\RptCalRecDlg.cpp", RptCalRecForm);
USEFORM("..\Source\RptCommentDlg.cpp", CommentReportForm);
USEFORM("..\Source\EditWitsDlg.cpp", EditWitsForm);
USEFORM("..\Source\GraphSettingDlg.cpp", GraphSettingForm);
USEFORM("..\Source\ConnectionDlg.cpp", ConnectionForm);
USEFORM("..\Source\JobsDlg.cpp", SelJobsForm);
USEFORM("..\Source\JobDetailsDlg.cpp", JobDetailsForm);
USEFORM("..\Source\RptSecSummaryDlg.cpp", RptSecSummaryForm);
USEFORM("..\Source\TCirculatingDlg.cpp", CirculatingDlg);
USEFORM("..\Source\THardwareStatusDlg.cpp", HardwareStatusDlg);
USEFORM("..\Source\TAlarmDlg.cpp", AlarmDlg);
USEFORM("..\Source\TCasingHelpDlg.cpp", CasingHelpDlg);
USEFORM("..\Source\ZeroWTTTSDlg.cpp", ZeroWTTTSForm);
USEFORM("..\Source\TLoadCasingsDlg.cpp", LoadCasingsDlg);
USEFORM("..\Source\ViewConnsDlg.cpp", ViewConnsForm);
USEFORM("..\Source\RptStatsRptDlg.cpp", RptStatsRptForm);
USEFORM("..\Source\RptSectionsDlg.cpp", RptSectionsForm);
USEFORM("..\Source\RptStatsDlg.cpp", RptStatsForm);
USEFORM("..\Source\SystemSettingsDlg.cpp", SystemSettingsForm);
USEFORM("..\Common\PasswordInputDlg.cpp", PasswordInputForm);
USEFORM("..\Common\ProgressDialog.cpp", ProgressDlg);
USEFORM("..\Common\NewBattDlg.cpp", NewBattForm);
USEFORM("..\Common\ChangePasswordDlg.cpp", ChangePasswordForm);
USEFORM("..\Common\TSerializerDlg.cpp", SerializerDlg);
//---------------------------------------------------------------------------
static BOOL CALLBACK EnumWindowsProc( HWND hwnd, LPARAM lParam )
{
    TCHAR class_name[256];

    GetClassName( hwnd, class_name, sizeof(class_name) / sizeof(TCHAR) );

    if( lstrcmp( class_name, _T("TTesTORKMgrMainForm") ) == 0 )
    {
        bool* pProcFound = (bool*)lParam;
        *pProcFound = true;

        // Restore the previous instance
        if( IsIconic( hwnd ) )
            ShowWindow( hwnd, SW_RESTORE );

        SetForegroundWindow( hwnd );

        // Return false to indicate enumeration should stop
        return FALSE;
    }

    // Return true to continue enumeration
    return TRUE;
}


int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
    // First check for a currently executing version of the code.
    bool previousFound = false;

    EnumWindows( (WNDENUMPROC)EnumWindowsProc, (LPARAM)(&previousFound) );

    if( previousFound )
        return 1;

    // Disable sleep while we are running
    EXECUTION_STATE esPreExState = SetThreadExecutionState( ES_CONTINUOUS | ES_DISPLAY_REQUIRED | ES_SYSTEM_REQUIRED );

    // Continue with boot here
    try
    {
        Application->Initialize();
        Application->MainFormOnTaskBar = true;
        Application->Title = "TesTORK Wireless Torque Turn Tension System Manager";
         Application->CreateForm(__classid(TTesTORKMgrMainForm), &TesTORKMgrMainForm);
         Application->CreateForm(__classid(TSelJobsForm), &SelJobsForm);
         Application->CreateForm(__classid(TJobDetailsForm), &JobDetailsForm);
         Application->CreateForm(__classid(TMainCommentForm), &MainCommentForm);
         Application->CreateForm(__classid(TViewConnsForm), &ViewConnsForm);
         Application->CreateForm(__classid(TRptConnForm), &RptConnForm);
         Application->CreateForm(__classid(TRptSectionsForm), &RptSectionsForm);
         Application->CreateForm(__classid(TRptSecSummaryForm), &RptSecSummaryForm);
         Application->CreateForm(__classid(TRptStatsForm), &RptStatsForm);
         Application->CreateForm(__classid(TConnectionForm), &ConnectionForm);
         Application->CreateForm(__classid(TCirculatingDlg), &CirculatingDlg);
         Application->CreateForm(__classid(TCommentReportForm), &CommentReportForm);
         Application->CreateForm(__classid(TRptStatsRptForm), &RptStatsRptForm);
         Application->CreateForm(__classid(TRptCalRecForm), &RptCalRecForm);
         Application->CreateForm(__classid(TChangePasswordForm), &ChangePasswordForm);
         Application->CreateForm(__classid(TNewBattForm), &NewBattForm);
         Application->CreateForm(__classid(TProgressDlg), &ProgressDlg);
         Application->CreateForm(__classid(TEditWitsForm), &EditWitsForm);
         Application->CreateForm(__classid(TCasingHelpDlg), &CasingHelpDlg);
         Application->CreateForm(__classid(TAlarmDlg), &AlarmDlg);
         Application->CreateForm(__classid(THardwareStatusDlg), &HardwareStatusDlg);
         Application->Run();
    }
    catch (Exception &exception)
    {
        Application->ShowException(&exception);
    }
    catch (...)
    {
        try
        {
            throw Exception("");
        }
        catch (Exception &exception)
        {
            Application->ShowException(&exception);
        }
    }

    // Restore system execution state
    SetThreadExecutionState( esPreExState );

    return 0;
}
//---------------------------------------------------------------------------
