//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("..\SimSource\RFCDataDlg.cpp", RFCDataForm);
USEFORM("..\SimSource\CalFactorsDlg.cpp", CalFactorsForm);
USEFORM("..\SimSource\SimMain.cpp", SimMainForm);
USEFORM("..\SimSource\SimPasswordDlg.cpp", SimPasswordForm);
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
    try
    {
         Application->Initialize();
         Application->MainFormOnTaskBar = true;
         Application->Title = "TesTORK Simulator";
         Application->CreateForm(__classid(TSimMainForm), &SimMainForm);
         Application->CreateForm(__classid(TCalFactorsForm), &CalFactorsForm);
         Application->CreateForm(__classid(TRFCDataForm), &RFCDataForm);
         Application->CreateForm(__classid(TSimPasswordForm), &SimPasswordForm);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }
    return 0;
}
//---------------------------------------------------------------------------
