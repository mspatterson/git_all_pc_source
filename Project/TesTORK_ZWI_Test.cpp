//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
USEFORM("..\MfgTestSource\TZWITestMain.cpp", ZWITestMain);
//---------------------------------------------------------------------------

static bool bTesTORKRunning = false;

static BOOL CALLBACK EnumWindowsProc( HWND hwnd, LPARAM lParam )
{
    TCHAR cWinName[1024];

    if( RealGetWindowClass( hwnd, cWinName, 1024 ) > 0 )
    {
        UnicodeString sWinName = cWinName;

        if( sWinName.Pos( "TTesTORKMgrMainForm" ) > 0 )
            bTesTORKRunning = true;
    }

    return TRUE;
}

int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
    EnumDesktopWindows( NULL, EnumWindowsProc, 0 );

    if( bTesTORKRunning )
        return 0;

    try
    {
         Application->Initialize();
         Application->MainFormOnTaskBar = true;
         Application->Title = "TesTORK ZWI Test Application";
         Application->CreateForm(__classid(TZWITestMain), &ZWITestMain);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    catch (...)
    {
         try
         {
             throw Exception("");
         }
         catch (Exception &exception)
         {
             Application->ShowException(&exception);
         }
    }
    return 0;
}
//---------------------------------------------------------------------------
