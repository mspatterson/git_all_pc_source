//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <tchar.h>
//---------------------------------------------------------------------------
#include <Vcl.Styles.hpp>
#include <Vcl.Themes.hpp>
USEFORM("..\Source\RptConnDlg.cpp", RptConnForm);
USEFORM("..\Source\RptCommentDlg.cpp", CommentReportForm);
USEFORM("..\Source\RptSectionsDlg.cpp", RptSectionsForm);
USEFORM("..\Source\RptSecSummaryDlg.cpp", RptSecSummaryForm);
USEFORM("..\Source\main.cpp", MainForm);
USEFORM("..\Source\JobsDlg.cpp", SelJobsForm);
USEFORM("..\Source\MainCommentsDlg.cpp", MainCommentForm);
USEFORM("..\Source\SystemSettingsDlg.cpp", SystemSettingsForm);
USEFORM("..\Source\ViewConnsDlg.cpp", ViewConnsForm);
USEFORM("..\Source\RptStatsDlg.cpp", RptStatsForm);
USEFORM("..\Common\PasswordInputDlg.cpp", PasswordInputForm);
USEFORM("..\Source\ConnectionDlg.cpp", ConnectionForm);
USEFORM("..\Source\DrillingDlg.cpp", DrillingForm);
USEFORM("..\Source\JobDetailsDlg.cpp", JobDetailsForm);
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
	try
	{
		Application->Initialize();
		Application->MainFormOnTaskBar = true;
		Application->Title = "Wireless Torque Turn System Manager";
         Application->CreateForm(__classid(TMainForm), &MainForm);
         Application->CreateForm(__classid(TSelJobsForm), &SelJobsForm);
         Application->CreateForm(__classid(TJobDetailsForm), &JobDetailsForm);
         Application->CreateForm(__classid(TMainCommentForm), &MainCommentForm);
         Application->CreateForm(__classid(TViewConnsForm), &ViewConnsForm);
         Application->CreateForm(__classid(TRptConnForm), &RptConnForm);
         Application->CreateForm(__classid(TRptSectionsForm), &RptSectionsForm);
         Application->CreateForm(__classid(TRptSecSummaryForm), &RptSecSummaryForm);
         Application->CreateForm(__classid(TRptStatsForm), &RptStatsForm);
         Application->CreateForm(__classid(TConnectionForm), &ConnectionForm);
         Application->CreateForm(__classid(TDrillingForm), &DrillingForm);
         Application->CreateForm(__classid(TCommentReportForm), &CommentReportForm);
         Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
