#include <vcl.h>
#pragma hdrstop

#include "RFCDataDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TRFCDataForm *RFCDataForm;


// All track bars range from -100 to 100. To report reasonably realistic
// values, the track bar position gets scaled when reported to the caller.
// Scale factors are listed below.
const int TqScale      = 10000;   // 24-bit value
const int TensionScale = 10000;   // 24-bit value
const int GyroScale    =    10;   // 32-bit value, increment per sample
const int CompassScale =   320;   // 16-bit value
const int AccelScale   =   320;   // 16-bit value
const int RTDScale     = 10000;   // 32-bit value

// Battery voltage and mAh rnage from 0-100. To report reasonably realistic
// values, the track bar position gets scaled when reported to the caller.
// Scale factors are listed below.
const int BatVoltScale    = 100;
const int MilliAmpHrScale = 25;

// RPM Ranges from 0-150, this currently does not need any extra scaling
const int RPMScale = 1;


__fastcall TRFCDataForm::TRFCDataForm(TComponent* Owner) : TForm(Owner)
{
}


void __fastcall TRFCDataForm::FormShow(TObject *Sender)
{
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::Tq045ZeroBtnClick(TObject *Sender)
{
    Tq045Bar->Position = 0;
}


void __fastcall TRFCDataForm::Tq225ZeroBtnClick(TObject *Sender)
{
    Tq225Bar->Position = 0;
}


void __fastcall TRFCDataForm::Tq045_R3ZeroBtnClick(TObject *Sender)
{
    Tq045_R3Bar->Position = 0;
}


void __fastcall TRFCDataForm::Tq225_R3ZeroBtnClick(TObject *Sender)
{
    Tq225_R3Bar->Position = 0;
}


void __fastcall TRFCDataForm::RTDZeroBtnClick(TObject *Sender)
{
    RTDBar->Position = 0;
}


void __fastcall TRFCDataForm::Tension000ZeroBtnClick(TObject *Sender)
{
    Tension000Bar->Position = 0;
}


void __fastcall TRFCDataForm::Tension090ZeroBtnClick(TObject *Sender)
{
    Tension090Bar->Position = 0;
}


void __fastcall TRFCDataForm::Tension180ZeroBtnClick(TObject *Sender)
{
    Tension180Bar->Position = 0;
}


void __fastcall TRFCDataForm::Tension270ZeroBtnClick(TObject *Sender)
{
    Tension270Bar->Position = 0;
}


void __fastcall TRFCDataForm::GyroZeroBtnClick(TObject *Sender)
{
    GyroBar->Position = 0;
}

void __fastcall TRFCDataForm::CompXZeroBtnClick(TObject *Sender)
{
    CompXBar->Position = 0;
}


void __fastcall TRFCDataForm::CompYZeroBtnClick(TObject *Sender)
{
    CompYBar->Position = 0;
}


void __fastcall TRFCDataForm::CompZZeroBtnClick(TObject *Sender)
{
    CompZBar->Position = 0;
}


void __fastcall TRFCDataForm::AccelXZeroBtnClick(TObject *Sender)
{
    AccelXBar->Position = 0;
}


void __fastcall TRFCDataForm::AccelYZeroBtnClick(TObject *Sender)
{
    AccelYBar->Position = 0;
}


void __fastcall TRFCDataForm::AccelZZeroBtnClick(TObject *Sender)
{
    AccelZBar->Position = 0;
}


void __fastcall TRFCDataForm::BatVoltZeroBtnClick(TObject *Sender)
{
    BatVoltBar->Position = 0;
}


void __fastcall TRFCDataForm::MilliAmpHrZeroBtnClick(TObject *Sender)
{
    MilliAmpHrBar->Position = 0;
}


void __fastcall TRFCDataForm::RPMZeroBtnClick(TObject *Sender)
{
    RPMBar->Position = 0;
}


RFCType TRFCDataForm::GetRFCType( void )
{
    if( RFCV1RB->Checked )
        return eUseRFCV1;
    else
        return eUseRFCV2;
}


int TRFCDataForm::GetTq045( void )
{
    return Tq045Bar->Position * TqScale;
}


int TRFCDataForm::GetTq225( void )
{
    return Tq225Bar->Position * TqScale;
}


int TRFCDataForm::GetTq045_R3( void )
{
    return Tq045_R3Bar->Position * TqScale;
}


int TRFCDataForm::GetTq225_R3( void )
{
    return Tq225_R3Bar->Position * TqScale;
}


int TRFCDataForm::GetRTD( void )
{
    return RTDBar->Position * RTDScale;
}


int TRFCDataForm::GetTension000( void )
{
    return Tension000Bar->Position * TensionScale;
}


int TRFCDataForm::GetTension090( void )
{
    return Tension090Bar->Position * TensionScale;
}


int TRFCDataForm::GetTension180( void )
{
    return Tension180Bar->Position * TensionScale;
}


int TRFCDataForm::GetTension270( void )
{
    return Tension270Bar->Position * TensionScale;
}


int TRFCDataForm::GetGyro( void )
{
    return GyroBar->Position * GyroScale;
}


int TRFCDataForm::GetCompX( void )
{
    return CompXBar->Position * CompassScale;
}


int TRFCDataForm::GetCompY( void )
{
    return CompYBar->Position * CompassScale;
}


int TRFCDataForm::GetCompZ( void )
{
    return CompZBar->Position * CompassScale;
}


int TRFCDataForm::GetAccelX( void )
{
    return AccelXBar->Position * AccelScale;
}


int TRFCDataForm::GetAccelY( void )
{
    return AccelYBar->Position * AccelScale;
}


int TRFCDataForm::GetAccelZ( void )
{
    return AccelZBar->Position * AccelScale;
}


int TRFCDataForm::GetBattVoltage( void )
{
    return BatVoltBar->Position * BatVoltScale;
}


int TRFCDataForm::GetBattmAhUsed( void )
{
    return MilliAmpHrBar->Position * MilliAmpHrScale;
}


int TRFCDataForm::GetRPM( void )
{
    return RPMBar->Position * RPMScale;
}


//
// Status Population
//
// All status information is displayed in a key editor grid. The grid
// operates in read-only mode though. The following list of strings
// identifies each key item.
//

typedef enum {
    SDE_TORQUE_045,
    SDE_TORQUE_225,
    SDE_TORQUE_R3_045,
    SDE_TORQUE_R3_225,
    SDE_TENSION_000,
    SDE_TENSION_090,
    SDE_TENSION_180,
    SDE_TENSION_270,
    SDE_RTD,
    SDE_GYRO,
    SDE_CAMPASS_X,
    SDE_CAMPASS_Y,
    SDE_CAMPASS_Z,
    SDE_ACCEL_X,
    SDE_ACCEL_Y,
    SDE_ACCEL_Z,
    SDE_BATTERY_VOLTAGE,
    SDE_MILLIAMP_HOUR,
    SDE_RPM,
    NBR_STATS_DATA_ENTRIES
} STATS_DATA_ENTRY;


static const AnsiString sdeKeys[NBR_STATS_DATA_ENTRIES] = {
    "Torque 045",
    "Torque 225",
    "Torque 045 (R3)",
    "Torque 225 (R3)",
    "Tension 000",
    "Tension 090",
    "Tension 180",
    "Tension 270",
    "RTD",
    "Gyro",
    "Campass X",
    "Campass Y",
    "Campass Z",
    "Accel X",
    "Accel Y",
    "Accel Z",
    "Battery Voltage",
    "MilliAmp Per Hour",
    "RPM"
};


void TRFCDataForm::UpdateValuesListview( void )
{
    RawDataVLE->Values[ sdeKeys[SDE_TORQUE_045] ]      = IntToStr( Tq045 );
    RawDataVLE->Values[ sdeKeys[SDE_TORQUE_225] ]      = IntToStr( Tq225 );
    RawDataVLE->Values[ sdeKeys[SDE_TORQUE_R3_045] ]   = IntToStr( Tq045_R3 );
    RawDataVLE->Values[ sdeKeys[SDE_TORQUE_R3_225] ]   = IntToStr( Tq225_R3 );
    RawDataVLE->Values[ sdeKeys[SDE_TENSION_000] ]     = IntToStr( Tension000 );
    RawDataVLE->Values[ sdeKeys[SDE_TENSION_090] ]     = IntToStr( Tension090 );
    RawDataVLE->Values[ sdeKeys[SDE_TENSION_180] ]     = IntToStr( Tension180 );
    RawDataVLE->Values[ sdeKeys[SDE_TENSION_270] ]     = IntToStr( Tension270 );
    RawDataVLE->Values[ sdeKeys[SDE_RTD] ]             = IntToStr( RTD );
    RawDataVLE->Values[ sdeKeys[SDE_GYRO] ]            = IntToStr( Gyro );
    RawDataVLE->Values[ sdeKeys[SDE_CAMPASS_X] ]       = IntToStr( CompX );
    RawDataVLE->Values[ sdeKeys[SDE_CAMPASS_Y] ]       = IntToStr( CompY );
    RawDataVLE->Values[ sdeKeys[SDE_CAMPASS_Z] ]       = IntToStr( CompZ );
    RawDataVLE->Values[ sdeKeys[SDE_ACCEL_X] ]         = IntToStr( AccelX );
    RawDataVLE->Values[ sdeKeys[SDE_ACCEL_Y] ]         = IntToStr( AccelY );
    RawDataVLE->Values[ sdeKeys[SDE_ACCEL_Z] ]         = IntToStr( AccelZ );
    RawDataVLE->Values[ sdeKeys[SDE_BATTERY_VOLTAGE] ] = IntToStr( BattVoltage );
    RawDataVLE->Values[ sdeKeys[SDE_MILLIAMP_HOUR] ]   = IntToStr( BattmAhUsed );
    RawDataVLE->Values[ sdeKeys[SDE_RPM] ]             = IntToStr( RPM );
}


void __fastcall TRFCDataForm::Tq045BarChange(TObject *Sender)
{
    Tq045Bar->Hint = IntToStr( Tq045);
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::Tq225BarChange(TObject *Sender)
{
    Tq225Bar->Hint = IntToStr( Tq225 );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::Tension000BarChange(TObject *Sender)
{
    Tension000Bar->Hint = IntToStr( Tension000 );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::Tension090BarChange(TObject *Sender)
{
    Tension090Bar->Hint =  IntToStr( Tension090 );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::Tension180BarChange(TObject *Sender)
{
    Tension180Bar->Hint =  IntToStr( Tension180 );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::Tension270BarChange(TObject *Sender)
{
    Tension270Bar->Hint =  IntToStr( Tension270 );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::GyroBarChange(TObject *Sender)
{
    GyroBar->Hint =  IntToStr( Gyro );
    UpdateValuesListview();
}

void __fastcall TRFCDataForm::CompXBarChange(TObject *Sender)
{
    CompXBar->Hint =  IntToStr( CompX );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::CompYBarChange(TObject *Sender)
{
    CompYBar->Hint =  IntToStr( CompY );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::CompZBarChange(TObject *Sender)
{
    CompZBar->Hint =  IntToStr( CompZ );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::AccelXBarChange(TObject *Sender)
{
    AccelXBar->Hint = IntToStr( AccelX );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::AccelYBarChange(TObject *Sender)
{
    AccelYBar->Hint = IntToStr( AccelY );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::AccelZBarChange(TObject *Sender)
{
    AccelZBar->Hint = IntToStr( AccelZ );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::BatVoltBarChange(TObject *Sender)
{
    BatVoltBar->Hint = IntToStr( BattVoltage );
    UpdateValuesListview();
}


void __fastcall TRFCDataForm::MilliAmpHrBarChange(TObject *Sender)
{
    MilliAmpHrBar->Hint = IntToStr( BattmAhUsed );
    UpdateValuesListview();
}

void __fastcall TRFCDataForm::RPMBarChange(TObject *Sender)
{
    RPMBar->Hint = IntToStr( RPM );
    UpdateValuesListview();
}
