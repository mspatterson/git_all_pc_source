object RFCDataForm: TRFCDataForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Raw Data Values'
  ClientHeight = 420
  ClientWidth = 707
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnShow = FormShow
  DesignSize = (
    707
    420)
  PixelsPerInch = 96
  TextHeight = 13
  object TqPanel: TPanel
    Left = 8
    Top = 8
    Width = 163
    Height = 201
    TabOrder = 0
    DesignSize = (
      163
      201)
    object Label1: TLabel
      Left = 22
      Top = 2
      Width = 34
      Height = 13
      Caption = 'Torque'
    end
    object Tq045ZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = Tq045ZeroBtnClick
    end
    object Tq225ZeroBtn: TSpeedButton
      Left = 47
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = Tq225ZeroBtnClick
    end
    object Tq045_R3ZeroBtn: TSpeedButton
      Left = 87
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = Tq045_R3ZeroBtnClick
    end
    object Tq225_R3ZeroBtn: TSpeedButton
      Left = 127
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = Tq225_R3ZeroBtnClick
    end
    object Tq045Bar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = Tq045BarChange
    end
    object Tq225Bar: TTrackBar
      Left = 47
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 1
      OnChange = Tq225BarChange
    end
    object Tq045_R3Bar: TTrackBar
      Left = 87
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 2
      OnChange = Tq045BarChange
    end
    object Tq225_R3Bar: TTrackBar
      Left = 127
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 3
      OnChange = Tq045BarChange
    end
  end
  object TensionPanel: TPanel
    Left = 176
    Top = 8
    Width = 168
    Height = 201
    TabOrder = 1
    DesignSize = (
      168
      201)
    object Label2: TLabel
      Left = 61
      Top = 0
      Width = 37
      Height = 13
      Caption = 'Tension'
    end
    object Tension000ZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = Tension000ZeroBtnClick
    end
    object Tension090ZeroBtn: TSpeedButton
      Left = 47
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = Tension090ZeroBtnClick
    end
    object Tension180ZeroBtn: TSpeedButton
      Left = 87
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = Tension180ZeroBtnClick
    end
    object Tension270ZeroBtn: TSpeedButton
      Left = 127
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = Tension270ZeroBtnClick
    end
    object Tension000Bar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = Tension000BarChange
    end
    object Tension090Bar: TTrackBar
      Left = 47
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 1
      OnChange = Tension090BarChange
    end
    object Tension180Bar: TTrackBar
      Left = 87
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 2
      OnChange = Tension180BarChange
    end
    object Tension270Bar: TTrackBar
      Left = 127
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 3
      OnChange = Tension270BarChange
    end
  end
  object CompPanel: TPanel
    Left = 8
    Top = 215
    Width = 126
    Height = 201
    TabOrder = 4
    DesignSize = (
      126
      201)
    object Label3: TLabel
      Left = 37
      Top = 2
      Width = 43
      Height = 13
      Caption = 'Compass'
    end
    object CompXZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = CompXZeroBtnClick
    end
    object CompYZeroBtn: TSpeedButton
      Left = 47
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = CompYZeroBtnClick
    end
    object CompZZeroBtn: TSpeedButton
      Left = 87
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = CompZZeroBtnClick
    end
    object CompXBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = CompXBarChange
    end
    object CompYBar: TTrackBar
      Left = 47
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 1
      OnChange = CompYBarChange
    end
    object CompZBar: TTrackBar
      Left = 87
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 2
      OnChange = CompZBarChange
    end
  end
  object AccelPanel: TPanel
    Left = 143
    Top = 215
    Width = 126
    Height = 201
    TabOrder = 5
    DesignSize = (
      126
      201)
    object Label4: TLabel
      Left = 45
      Top = 2
      Width = 25
      Height = 13
      Caption = 'Accel'
    end
    object AccelXZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = AccelXZeroBtnClick
    end
    object AccelYZeroBtn: TSpeedButton
      Left = 47
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = AccelYZeroBtnClick
    end
    object AccelZZeroBtn: TSpeedButton
      Left = 87
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = AccelZZeroBtnClick
    end
    object AccelXBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = AccelXBarChange
    end
    object AccelYBar: TTrackBar
      Left = 47
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 1
      OnChange = AccelYBarChange
    end
    object AccelZBar: TTrackBar
      Left = 87
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 2
      OnChange = AccelZBarChange
    end
  end
  object GyroPanel: TPanel
    Left = 350
    Top = 8
    Width = 44
    Height = 201
    TabOrder = 2
    DesignSize = (
      44
      201)
    object Label5: TLabel
      Left = 10
      Top = 2
      Width = 23
      Height = 13
      Caption = 'Gyro'
    end
    object GyroZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = GyroZeroBtnClick
    end
    object GyroBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = GyroBarChange
    end
  end
  object BatVoltPanel: TPanel
    Left = 277
    Top = 215
    Width = 44
    Height = 201
    TabOrder = 6
    DesignSize = (
      44
      201)
    object BatVoltLB: TLabel
      Left = 10
      Top = 2
      Width = 22
      Height = 13
      Caption = 'VBat'
    end
    object BatVoltZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = BatVoltZeroBtnClick
    end
    object BatVoltBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = BatVoltBarChange
    end
  end
  object MilliAmpHrPanel: TPanel
    Left = 329
    Top = 215
    Width = 44
    Height = 201
    TabOrder = 7
    DesignSize = (
      44
      201)
    object MillAmpHrLB: TLabel
      Left = 10
      Top = 2
      Width = 21
      Height = 13
      Caption = 'mAh'
    end
    object MilliAmpHrZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = MilliAmpHrZeroBtnClick
    end
    object MilliAmpHrBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = MilliAmpHrBarChange
    end
  end
  object RawDataVLE: TValueListEditor
    Left = 500
    Top = 10
    Width = 201
    Height = 406
    Anchors = [akTop, akRight, akBottom]
    TabOrder = 8
    TitleCaptions.Strings = (
      'Property'
      'Value')
    ExplicitLeft = 503
    ColWidths = (
      97
      98)
  end
  object RTDPanel: TPanel
    Left = 400
    Top = 8
    Width = 44
    Height = 201
    TabOrder = 3
    DesignSize = (
      44
      201)
    object Label6: TLabel
      Left = 10
      Top = 2
      Width = 20
      Height = 13
      Caption = 'RTD'
    end
    object RTDZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = RTDZeroBtnClick
    end
    object RTDBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 100
      Min = -100
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      ShowHint = True
      TabOrder = 0
      OnChange = GyroBarChange
    end
  end
  object GroupBox1: TGroupBox
    Left = 379
    Top = 215
    Width = 67
    Height = 201
    Caption = ' Format '
    TabOrder = 9
    object RFCV1RB: TRadioButton
      Left = 11
      Top = 19
      Width = 113
      Height = 17
      Caption = 'V1'
      TabOrder = 0
    end
    object RFCV2RB: TRadioButton
      Left = 11
      Top = 43
      Width = 113
      Height = 17
      Caption = 'V2'
      Checked = True
      TabOrder = 1
      TabStop = True
    end
  end
  object RPMPanel: TPanel
    Left = 450
    Top = 8
    Width = 44
    Height = 201
    TabOrder = 10
    DesignSize = (
      44
      201)
    object RPMLabel: TLabel
      Left = 10
      Top = 2
      Width = 21
      Height = 13
      Caption = 'RPM'
    end
    object RPMZeroBtn: TSpeedButton
      Left = 7
      Top = 176
      Width = 21
      Height = 20
      Caption = '0'
      OnClick = RPMZeroBtnClick
    end
    object RPMBar: TTrackBar
      Left = 7
      Top = 14
      Width = 34
      Height = 161
      Anchors = [akLeft, akTop, akBottom]
      Max = 150
      Orientation = trVertical
      ParentShowHint = False
      Frequency = 10
      Position = 75
      ShowHint = True
      TabOrder = 0
      OnChange = RPMBarChange
    end
  end
end
