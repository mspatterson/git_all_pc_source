#ifndef RFCDataDlgH
#define RFCDataDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>


typedef enum {
    eUseRFCV1,
    eUseRFCV2,
} RFCType;


class TRFCDataForm : public TForm
{
__published:    // IDE-managed Components
    TPanel *TqPanel;
    TTrackBar *Tq045Bar;
    TTrackBar *Tq225Bar;
    TLabel *Label1;
    TSpeedButton *Tq045ZeroBtn;
    TSpeedButton *Tq225ZeroBtn;
    TPanel *TensionPanel;
    TLabel *Label2;
    TSpeedButton *Tension000ZeroBtn;
    TSpeedButton *Tension090ZeroBtn;
    TTrackBar *Tension000Bar;
    TTrackBar *Tension090Bar;
    TSpeedButton *Tension180ZeroBtn;
    TSpeedButton *Tension270ZeroBtn;
    TTrackBar *Tension180Bar;
    TTrackBar *Tension270Bar;
    TPanel *CompPanel;
    TLabel *Label3;
    TSpeedButton *CompXZeroBtn;
    TSpeedButton *CompYZeroBtn;
    TTrackBar *CompXBar;
    TTrackBar *CompYBar;
    TTrackBar *CompZBar;
    TSpeedButton *CompZZeroBtn;
    TPanel *AccelPanel;
    TLabel *Label4;
    TSpeedButton *AccelXZeroBtn;
    TSpeedButton *AccelYZeroBtn;
    TSpeedButton *AccelZZeroBtn;
    TTrackBar *AccelXBar;
    TTrackBar *AccelYBar;
    TTrackBar *AccelZBar;
    TPanel *GyroPanel;
    TLabel *Label5;
    TSpeedButton *GyroZeroBtn;
    TTrackBar *GyroBar;
    TPanel *BatVoltPanel;
    TLabel *BatVoltLB;
    TSpeedButton *BatVoltZeroBtn;
    TTrackBar *BatVoltBar;
    TPanel *MilliAmpHrPanel;
    TLabel *MillAmpHrLB;
    TSpeedButton *MilliAmpHrZeroBtn;
    TTrackBar *MilliAmpHrBar;
    TValueListEditor *RawDataVLE;
    TTrackBar *Tq045_R3Bar;
    TSpeedButton *Tq045_R3ZeroBtn;
    TTrackBar *Tq225_R3Bar;
    TSpeedButton *Tq225_R3ZeroBtn;
    TPanel *RTDPanel;
    TLabel *Label6;
    TSpeedButton *RTDZeroBtn;
    TTrackBar *RTDBar;
    TGroupBox *GroupBox1;
    TRadioButton *RFCV1RB;
    TRadioButton *RFCV2RB;
    TPanel *RPMPanel;
    TLabel *RPMLabel;
    TSpeedButton *RPMZeroBtn;
    TTrackBar *RPMBar;
    void __fastcall Tq045ZeroBtnClick(TObject *Sender);
    void __fastcall Tq225ZeroBtnClick(TObject *Sender);
    void __fastcall Tension000ZeroBtnClick(TObject *Sender);
    void __fastcall Tension090ZeroBtnClick(TObject *Sender);
    void __fastcall Tension180ZeroBtnClick(TObject *Sender);
    void __fastcall Tension270ZeroBtnClick(TObject *Sender);
    void __fastcall CompXZeroBtnClick(TObject *Sender);
    void __fastcall CompYZeroBtnClick(TObject *Sender);
    void __fastcall CompZZeroBtnClick(TObject *Sender);
    void __fastcall AccelXZeroBtnClick(TObject *Sender);
    void __fastcall AccelYZeroBtnClick(TObject *Sender);
    void __fastcall AccelZZeroBtnClick(TObject *Sender);
    void __fastcall GyroZeroBtnClick(TObject *Sender);
    void __fastcall Tq045BarChange(TObject *Sender);
    void __fastcall Tq225BarChange(TObject *Sender);
    void __fastcall Tension000BarChange(TObject *Sender);
    void __fastcall Tension090BarChange(TObject *Sender);
    void __fastcall Tension180BarChange(TObject *Sender);
    void __fastcall Tension270BarChange(TObject *Sender);
    void __fastcall GyroBarChange(TObject *Sender);
    void __fastcall CompXBarChange(TObject *Sender);
    void __fastcall CompYBarChange(TObject *Sender);
    void __fastcall CompZBarChange(TObject *Sender);
    void __fastcall AccelXBarChange(TObject *Sender);
    void __fastcall AccelYBarChange(TObject *Sender);
    void __fastcall AccelZBarChange(TObject *Sender);
    void __fastcall BatVoltZeroBtnClick(TObject *Sender);
    void __fastcall MilliAmpHrZeroBtnClick(TObject *Sender);
    void __fastcall BatVoltBarChange(TObject *Sender);
    void __fastcall MilliAmpHrBarChange(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall Tq045_R3ZeroBtnClick(TObject *Sender);
    void __fastcall Tq225_R3ZeroBtnClick(TObject *Sender);
    void __fastcall RTDZeroBtnClick(TObject *Sender);
    void __fastcall RPMBarChange(TObject *Sender);
    void __fastcall RPMZeroBtnClick(TObject *Sender);

private:
    RFCType GetRFCType( void );

    int GetTq045( void );
    int GetTq225( void );
    int GetTq045_R3( void );
    int GetTq225_R3( void );
    int GetRTD( void );
    int GetTension000( void );
    int GetTension090( void );
    int GetTension180( void );
    int GetTension270( void );
    int GetGyro( void );
    int GetCompX( void );
    int GetCompY( void );
    int GetCompZ( void );
    int GetAccelX( void );
    int GetAccelY( void );
    int GetAccelZ( void );
    int GetBattVoltage( void );
    int GetBattmAhUsed( void );
    int GetRPM( void );

    void UpdateValuesListview( void );

public:     // User declarations
    __fastcall TRFCDataForm(TComponent* Owner);

    __property RFCType RFCType     = { read = GetRFCType };
    __property int     Tq045       = { read = GetTq045 };
    __property int     Tq225       = { read = GetTq225 };
    __property int     Tq045_R3    = { read = GetTq045_R3 };
    __property int     Tq225_R3    = { read = GetTq225_R3 };
    __property int     RTD         = { read = GetRTD };
    __property int     Tension000  = { read = GetTension000 };
    __property int     Tension090  = { read = GetTension090 };
    __property int     Tension180  = { read = GetTension180 };
    __property int     Tension270  = { read = GetTension270 };
    __property int     Gyro        = { read = GetGyro };
    __property int     CompX       = { read = GetCompX };
    __property int     CompY       = { read = GetCompY };
    __property int     CompZ       = { read = GetCompZ };
    __property int     AccelX      = { read = GetAccelX };
    __property int     AccelY      = { read = GetAccelY };
    __property int     AccelZ      = { read = GetAccelZ };
    __property int     BattVoltage = { read = GetBattVoltage };
    __property int     BattmAhUsed = { read = GetBattmAhUsed };
    __property int     RPM         = { read = GetRPM };
};

extern PACKAGE TRFCDataForm *RFCDataForm;

#endif
