#include <vcl.h>
#pragma hdrstop

#include "SimMain.h"
#include "ApplUtils.h"
#include "Comm32.cpp"
#include "CUDPPort.cpp"
#include "CalFactorsDlg.h"
#include "RFCDataDlg.h"
#include "TypeDefs.h"
#include "SimPasswordDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TSimMainForm *SimMainForm;

//
// Private declarations
//

typedef enum {
    LPG_SEQ_NBR,
    LPG_TICK,
    LPG_ROTATION,
    LPG_TORQUE,
    NBR_LPG_COLS
} LAST_PKT_GRID_COLS;


#define SIMPLE_WIDTH        436
#define SIMPLE_HEIGHT       156

#define ADVANCED_WIDTH      436
#define ADVANCED_HEIGHT     590


//
// Class Implementation
//

__fastcall TSimMainForm::TSimMainForm(TComponent* Owner) : TForm(Owner)
{
    // Winsock is required for UDP comms. Init it now
    InitWinsock();

    // Init the page state to advanced, as that is our design state.
    // But switch to simple on boot
    m_PageState = eAdvancedPage;
    AdvancedButtonClick( AdvancedButton );

    m_simData = new TStringList();

    // Init last packet grid column headers
    LastPktGrid->Cells[LPG_SEQ_NBR][0]  = "Seq Nbr";
    LastPktGrid->Cells[LPG_TICK][0]     = "Tick Cnt";
    LastPktGrid->Cells[LPG_ROTATION][0] = "Rotation";
    LastPktGrid->Cells[LPG_TORQUE][0]   = "Torque";

    // Init file reading vars
    m_PrevTime  = 0;
    m_PrevTurns = 0;

    // Just in case, clear streaming vars now
    m_streaming      = false;
    m_InFileSendLoop = false;
}


void __fastcall TSimMainForm::FormShow(TObject *Sender)
{
    // If the user cancels from the password dlg, exit from the program
    if( !SimPasswordForm->ShowDlg() )
    {
        Application->Terminate();
    }
    else
    {
        // Fake disconnect click
        if( m_commObj != NULL )
        {
            delete m_commObj;
            m_commObj = NULL;
        }

        // Convert the item back into a Connect Button
        ConnDiscButton->Tag     = eConnectButton;
        ConnDiscButton->Caption = "Connect";

        SimTimer->Enabled = false;

        ModeRBClick( SerialModeRB );
    }
}


void __fastcall TSimMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Fake disconnect click
    if( m_commObj != NULL )
    {
        delete m_commObj;
        m_commObj = NULL;
    }

    // Convert the item back into a Connect Button
    ConnDiscButton->Tag     = eConnectButton;
    ConnDiscButton->Caption = "Connect";

    SimTimer->Enabled = false;
}


void __fastcall TSimMainForm::ModeRBClick(TObject *Sender)
{
    if( SerialModeRB->Checked )
    {
        CommPortEdit->Enabled = true;
        CommPortEdit->Color   = clWindow;
        BitRateEdit->Enabled  = true;
        BitRateEdit->Color    = clWindow;

        HostIPEdit->Enabled   = false;
        HostIPEdit->Color     = clBtnFace;
        UDPPortEdit->Enabled  = false;
        UDPPortEdit->Color    = clBtnFace;
    }
    else
    {
        CommPortEdit->Enabled = false;
        CommPortEdit->Color   = clBtnFace;
        BitRateEdit->Enabled  = false;
        BitRateEdit->Color    = clBtnFace;

        HostIPEdit->Enabled   = true;
        HostIPEdit->Color     = clWindow;
        UDPPortEdit->Enabled  = true;
        UDPPortEdit->Color    = clWindow;
    }
}


void __fastcall TSimMainForm::ConnDiscButtonClick(TObject *Sender)
{
    if( ConnDiscButton->Tag == eConnectButton )
    {
        // Handle Connect click

        // First fake a disconnect click
        if( m_commObj != NULL )
        {
            delete m_commObj;
            m_commObj = NULL;
        }

        // Convert the item back into a Connect Button
        ConnDiscButton->Tag     = eConnectButton;
        ConnDiscButton->Caption = "Connect";

        SimTimer->Enabled = false;

        // Reset rate vars to defaults. These are only used for WTTTS-type connections
        m_rfcRate         = 100;
        m_rfcTimeout      = 250;
        m_streamRate      = 10;
        m_streamTimeout   = 120;
        m_pairingTimeout  = 900;

        StreamRateEdit->Text = IntToStr( m_streamRate );
        RFCRateEdit->Text    = IntToStr( m_rfcRate );

        // Create the comm object
        if( SerialModeRB->Checked )
        {
            CCommPort::CONNECT_PARAMS portParams;

            portParams.iPortNumber   = CommPortEdit->Text.ToInt();
            portParams.dwLineBitRate = BitRateEdit->Text.ToInt();

            m_commObj = new CCommPort();

            if( m_commObj->Connect( &portParams ) == CCommObj::ERR_NONE )
            {
                AddMemoEntry( "COM" + CommPortEdit->Text + " opened" );
            }
        }
        else
        {
            CUDPPort::CONNECT_PARAMS portParams;

            // We will be sending to a given destination. Set our port
            // number to zero so that Windows gives us any free UDP port.
            portParams.destAddr = HostIPEdit->Text;
            portParams.destPort = UDPPortEdit->Text.ToInt();
            portParams.portNbr  = 0;
            portParams.acceptBroadcasts = true;

            m_commObj = new CUDPPort();

            if( m_commObj->Connect( &portParams ) == CCommObj::ERR_NONE )
            {
                AddMemoEntry( "UDP" + HostIPEdit->Text + ":" + UDPPortEdit->Text + " opened" );
            }
        }

        if( m_commObj == NULL )
            return;

        if( m_commObj->isConnected )
        {
            // Convert the button into a Disconnect button
            ConnDiscButton->Tag     = eDisconnectButton;
            ConnDiscButton->Caption = "Disconnect";

            // Enable timer, with rate set at max output rate (which isn't really
            // the stream rate the WTTTS would output, but close enough for
            // simulation purposes)
            SimTimer->Interval = 20;
            SimTimer->Enabled  = true;
        }
        else
        {
            AddMemoEntry( "COM" + CommPortEdit->Text + " failed, code " + IntToStr( m_commObj->connectResult ) );

            delete m_commObj;
            m_commObj = NULL;
        }

        // Finally, make sure we're not streaming
        StopDataStreaming();
    }
    else if( ConnDiscButton->Tag == eDisconnectButton )
    {
        // Make sure we're not streaming
        StopDataStreaming();

        // Handle disconnect click
        if( m_commObj != NULL )
        {
            delete m_commObj;
            m_commObj = NULL;
        }

        // Convert the item back into a Connect Button
        ConnDiscButton->Tag     = eConnectButton;
        ConnDiscButton->Caption = "Connect";

        SimTimer->Enabled = false;
    }
}


void __fastcall TSimMainForm::SimTimerTimer(TObject *Sender)
{
    // Check for inbound messages
    m_buffCount += m_commObj->CommRecv( &(m_commBuff[m_buffCount]), COMM_BUFF_LEN - m_buffCount );

    // Simulating WTTTS sub. In this case, we need to process inbound messages.
    WTTTS_PKT_HDR      pktHdr;
    TESTORK_DATA_UNION pktData;

    if( HaveTesTORKCmdPkt( m_commBuff, m_buffCount, pktHdr, pktData ) )
    {
        // Log the packet
        AddMemoEntry( "WTTTS cmd received: " + IntToHex( pktHdr.pktType, 2 ) );

        // We only accept start / stop commands for now
        switch( pktHdr.pktType )
        {
            case WTTTS_CMD_START_STREAM:
                if( !m_streaming )
                    StartDataStreaming();
                break;

            case WTTTS_CMD_STOP_STREAM:
                StopDataStreaming();
                break;

            case WTTTS_CMD_GET_CFG_DATA:
                GetCfgData( pktData.cfgRequest.pageNbr );
                break;

            case WTTTS_CMD_SET_CFG_DATA:
                SetCfgData( pktData.cfgData );
                break;

            case WTTTS_CMD_SET_RATE:
                SetRates( pktData.ratePkt );
                break;
        }
    }
    else
    {
        // No command was pending. If we aren't streaming, send a RFC (not forced)
        if( !m_streaming )
            SendRequestForCmd( false );
    }

    if( m_streaming && !m_InFileSendLoop )
    {
        // Output packet if the timer has expired
        if( GetTickCount() - m_startTime >= m_lastReading.recTime + m_streamRate )
        {
            // First, get the data to output. This will either be from the sim
            // file, or from the controls
            SIM_READING currReading;
            ReadControls( currReading );

            // Update the rotational position of the device. If the source of
            // the data is the RFC dialog, then just add the gyro value reported
            // there to the accumulated rotation value. Otherwise, rotation is
            // based on RPM and is in units of 1,000,000 increments per revolution.
            // Calc elapsed time, converting from msecs to minutes.
            if( SimSrcCombo->ItemIndex == 2 )
            {
                m_rotation += RFCDataForm->Gyro;
            }
            else
            {
                float timeDelta = ( currReading.recTime - m_lastReading.recTime ) / 60000.0;
                float turnDelta = timeDelta * (float)currReading.RPM * 1000000.0;

                m_rotation += (int)( turnDelta + 0.5 );

                // Randomize data, if selected
                if( RandomizeCB->Checked )
                {
                    // Randomize the values somewhat. Only do so if rotation > 100000
                    if( m_rotation > 100000 )
                        m_rotation = m_rotation - 2500 + random( 5000 );

                    // Randomize torque by 75 ft lb. Only do so if torque is > 500
                    if( currReading.torque > 500 )
                        currReading.torque = currReading.torque - 75 + random( 151 );
                }
            }

            SendLastReading( currReading );
        }
    }
}


void TSimMainForm::StartDataStreaming( void )
{
    // Clear the last reading
    memset( &m_lastReading, 0, sizeof( SIM_READING ) );

    m_PrevTime  = 0;
    m_PrevTurns = 0;

    m_InFileSendLoop = false;

    // Load the simulation file, if doing file-based sim
    if( SimSrcCombo->ItemIndex == 0 )
    {
        m_nextReading = 0;

        m_simData->Clear();

        try {
            m_simData->LoadFromFile( OpenDlg->FileName );

            // First line is a header line - delete it
            if( m_simData->Count > 0 )
                m_simData->Delete( 0 );

            m_InFileSendLoop = true;
        }
        catch( ... )
        {
            // On error, switch to control-based sim
            SimSrcCombo->ItemIndex = 1;
        }

        // The following is only required for simulation
        randomize();
    }

    // Init state vars
    m_seqNbr    = 0;
    m_startTime = GetTickCount();
    m_rotation  = 0;
    m_streaming = true;

    // If sending from file, post a message now to start that process
    if( m_InFileSendLoop )
        ::PostMessage( Handle, WM_STREAM_FROM_FILE, 0, 0 );
}


void TSimMainForm::StopDataStreaming( void )
{
    m_streaming      = false;
    m_InFileSendLoop = false;
}


void __fastcall TSimMainForm::TorqueMaxComboClick(TObject *Sender)
{
    int newMax = TorqueMaxCombo->Items->Strings[ TorqueMaxCombo->ItemIndex ].ToInt();

    if( m_lastTorque > newMax )
        m_lastTorque = newMax;

    RPMTorqueBox->Invalidate();
}


void __fastcall TSimMainForm::RPMMaxComboClick(TObject *Sender)
{
    int newMax = RPMMaxCombo->Items->Strings[ RPMMaxCombo->ItemIndex ].ToInt();

    if( m_lastRPM > newMax )
        m_lastRPM = newMax;

    RPMTorqueBox->Invalidate();
}


void __fastcall TSimMainForm::FileNameEditDblClick(TObject *Sender)
{
    if( OpenDlg->Execute() )
    {
        FileNameEdit->Text     = ExtractFileName( OpenDlg->FileName );
        SimSrcCombo->ItemIndex = 0;
    }
}


void TSimMainForm::ReadSimFileEntry( SIM_READING& currReading )
{
    // Based on our state, update the simulation. If we are out of data,
    // just resend the last value.
    if( m_nextReading >= m_simData->Count )
    {
        currReading = m_lastReading;
        currReading.recTime = GetTickCount() - m_startTime + m_streamRate;

        return;
    }

    SIM_READING tempReading;

    if( !ConvertCSVStringToReading( m_simData->Strings[m_nextReading], tempReading ) )
    {
        // Line failed to convert - skip it. Report the last reading for now
        m_nextReading++;

        currReading = m_lastReading;
        currReading.recTime = GetTickCount() - m_startTime + m_streamRate;

        return;
    }

    m_PrevTime  = tempReading.recTime;
    m_PrevTurns = tempReading.turns;

    // Update the paint box to reflect the new values
    int maxTorque = TorqueMaxCombo->Items->Strings[ TorqueMaxCombo->ItemIndex ].ToInt();
    int maxRPM    = RPMMaxCombo->Items->Strings[ RPMMaxCombo->ItemIndex ].ToInt();

    if( tempReading.torque < maxTorque )
        m_lastTorque = tempReading.torque;
    else
        m_lastTorque = maxTorque;

    if( tempReading.RPM < maxRPM )
        m_lastRPM = tempReading.RPM;
    else
        m_lastRPM = maxRPM;

    // If we're in the tight spin loop to send from a file, don't bother updating the screen
    if( !m_InFileSendLoop )
        RPMTorqueBox->Invalidate();

    // Save this reading
    currReading = tempReading;

    m_nextReading++;
}


void TSimMainForm::ReadControls( SIM_READING& currReading )
{
    // In this mode, we always create a packet
    currReading.recTime = GetTickCount() - m_startTime;
    currReading.torque  = abs( m_lastTorque );
    currReading.RPM     = abs( m_lastRPM );

    // Add a tension control
    // TODO
    currReading.tension = 0;
}


bool TSimMainForm::ConvertCSVStringToReading( const UnicodeString csvLine, SIM_READING& aReading )
{
    TStringList* csvStrings = new TStringList();

    bool convertedOk = false;

    try
    {
        csvStrings->CommaText = csvLine;

        if( ( csvStrings->Count != 3 ) && ( csvStrings->Count != 4 ) )
            Abort();

        aReading.recTime = Trim( csvStrings->Strings[0] ).ToInt();
        aReading.torque  = Trim( csvStrings->Strings[2] ).ToDouble();
        aReading.turns   = Trim( csvStrings->Strings[1] ).ToInt();

        // Not all exports have a tension column
        if( csvStrings->Count > 3 )
            aReading.tension = Trim( csvStrings->Strings[3] ).ToDouble();
        else
            aReading.tension = 0;

        if( aReading.torque < 0 )
            aReading.torque = 0;

        float fDMicroTurns  = aReading.turns   - m_PrevTurns;
        float fDMilliSecs   = aReading.recTime - m_PrevTime;

        if( fDMicroTurns > 0 )
            aReading.RPM = (int)( ( fDMicroTurns / 1000000 ) / ( fDMilliSecs / 60000 ) );
        else
            aReading.RPM = 0;

        convertedOk = true;
    }
    catch( ... )
    {
    }

    delete csvStrings;

    return convertedOk;
}


DWORD TSimMainForm::BuildWTTTSPkt( BYTE txBuffer[], BYTE seqNbr, DWORD tickCount, int rotation, WORD torque, int tension )
{
    WTTTS_PKT_HDR* pPktHdr = (WTTTS_PKT_HDR*)txBuffer;

    pPktHdr->pktHdr    = TESTORK_HDR_RX;
    pPktHdr->pktType   = WTTTS_RESP_STREAM_DATA;
    pPktHdr->seqNbr    = seqNbr;
    pPktHdr->timeStamp = tickCount / 10;
    pPktHdr->dataLen   = sizeof( WTTTS_STREAM_DATA );

    WTTTS_STREAM_DATA* pData = (WTTTS_STREAM_DATA*) &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

    memset( pData, 0, sizeof( WTTTS_STREAM_DATA ) );

    // We do a low-level override here if we are to be sending raw data
    // values from the RFC dialog. Note that the rotation value passed
    // as a param is always used when sending a packet, because it is
    // an accumulated value.
    XFER_BUFFER transferBuff;

    transferBuff.dwData = rotation;

    pData->gyro[0] = transferBuff.byData[0];
    pData->gyro[1] = transferBuff.byData[1];
    pData->gyro[2] = transferBuff.byData[2];
    pData->gyro[3] = transferBuff.byData[3];

    if( SimSrcCombo->ItemIndex == 2 )
    {
        transferBuff.dwData = RFCDataForm->Tq045;

        pData->torque045[0] = transferBuff.byData[0];
        pData->torque045[1] = transferBuff.byData[1];
        pData->torque045[2] = transferBuff.byData[2];

        transferBuff.dwData = RFCDataForm->Tq225;

        pData->torque225[0] = transferBuff.byData[0];
        pData->torque225[1] = transferBuff.byData[1];
        pData->torque225[2] = transferBuff.byData[2];

        transferBuff.dwData = RFCDataForm->Tension000;

        pData->tension000[0] = transferBuff.byData[0];
        pData->tension000[1] = transferBuff.byData[1];
        pData->tension000[2] = transferBuff.byData[2];

        transferBuff.dwData = RFCDataForm->Tension090;

        pData->tension090[0] = transferBuff.byData[0];
        pData->tension090[1] = transferBuff.byData[1];
        pData->tension090[2] = transferBuff.byData[2];

        transferBuff.dwData = RFCDataForm->Tension180;

        pData->tension180[0] = transferBuff.byData[0];
        pData->tension180[1] = transferBuff.byData[1];
        pData->tension180[2] = transferBuff.byData[2];

        transferBuff.dwData = RFCDataForm->Tension270;

        pData->tension270[0] = transferBuff.byData[0];
        pData->tension270[1] = transferBuff.byData[1];
        pData->tension270[2] = transferBuff.byData[2];

        transferBuff.dwData = RFCDataForm->CompX;

        pData->compassX[0] = transferBuff.byData[0];
        pData->compassX[1] = transferBuff.byData[1];

        transferBuff.dwData = RFCDataForm->CompY;

        pData->compassY[0] = transferBuff.byData[0];
        pData->compassY[1] = transferBuff.byData[1];

        transferBuff.dwData = RFCDataForm->CompZ;

        pData->compassZ[0] = transferBuff.byData[0];
        pData->compassZ[1] = transferBuff.byData[1];

        transferBuff.dwData = RFCDataForm->AccelX;

        pData->accelX[0] = transferBuff.byData[0];
        pData->accelX[1] = transferBuff.byData[1];

        transferBuff.dwData = RFCDataForm->AccelY;

        pData->accelY[0] = transferBuff.byData[0];
        pData->accelY[1] = transferBuff.byData[1];

        transferBuff.dwData = RFCDataForm->AccelZ;

        pData->accelZ[0] = transferBuff.byData[0];
        pData->accelZ[1] = transferBuff.byData[1];
    }
    else
    {
        transferBuff.dwData = (DWORD)torque;

        pData->torque045[0] = transferBuff.byData[0];
        pData->torque045[1] = transferBuff.byData[1];
        pData->torque045[2] = 0;

        pData->torque225[0] = transferBuff.byData[0];
        pData->torque225[1] = transferBuff.byData[1];
        pData->torque225[2] = 0;

        transferBuff.dwData  = tension;

        pData->tension000[0] = transferBuff.byData[0];
        pData->tension000[1] = transferBuff.byData[1];
        pData->tension000[2] = transferBuff.byData[2];

        pData->tension090[0] = transferBuff.byData[0];
        pData->tension090[1] = transferBuff.byData[1];
        pData->tension090[2] = transferBuff.byData[2];

        pData->tension180[0] = transferBuff.byData[0];
        pData->tension180[1] = transferBuff.byData[1];
        pData->tension180[2] = transferBuff.byData[2];

        pData->tension270[0] = transferBuff.byData[0];
        pData->tension270[1] = transferBuff.byData[1];
        pData->tension270[2] = transferBuff.byData[2];
    }

    int packetHdrAndDataLen = sizeof( WTTTS_PKT_HDR ) + sizeof( WTTTS_STREAM_DATA );

    txBuffer[ packetHdrAndDataLen ] = CalcWTTTSChecksum( txBuffer, packetHdrAndDataLen );

    return packetHdrAndDataLen + 1;
}


void __fastcall TSimMainForm::WMStreamFromFile( TMessage &Message )
{
    // This message is posted when streaming data from file.
    while( m_streaming && m_InFileSendLoop )
    {
        // Let other events get handled
        Application->ProcessMessages();

        // Make sure the port hasn't been closed or that streaming has stopped
        if( !SimTimer->Enabled || ( m_commObj == NULL ) || !m_streaming || !m_InFileSendLoop )
            break;

        // Read the next reading
        SIM_READING currReading;
        ReadSimFileEntry( currReading );

        // Spin until this packet is ready to send
        while( currReading.recTime + m_startTime > GetTickCount() )
        {
            Application->ProcessMessages();

            // Make sure the port hasn't been closed
            if( !SimTimer->Enabled || ( m_commObj == NULL ) || !m_streaming || !m_InFileSendLoop )
                m_streaming = false;

            if( !m_streaming )
                break;
        }

        if( m_streaming )
        {
            // If we're simulating from a file, overwrite the rotation with our turns val
            m_rotation = currReading.turns;

            SendLastReading( currReading );
        }
    }
}


void TSimMainForm::SendLastReading( SIM_READING& currReading )
{
    // Save reading
    m_lastReading = currReading;

    // Increment the sequence number
    m_seqNbr++;

    // Next, build and send the packet
    BYTE txBuffer[200];

    DWORD buffLen = BuildWTTTSPkt( txBuffer, m_seqNbr, currReading.recTime, m_rotation, currReading.torque, currReading.tension );

    DWORD bytesSent = m_commObj->CommSend( txBuffer, buffLen );

    // If we're in the tight spin loop to send from a file, don't bother updating the screen
    if( !m_InFileSendLoop )
    {
        // Redraw the sub pos now
        SubPosBox->Invalidate();

        // Update log memo
        UnicodeString sResult;

        if( bytesSent == buffLen )
            sResult = " sent";
        else
            sResult = " send failed";

        AddMemoEntry( "tx seq nbr " + IntToStr( m_seqNbr ) + sResult );

        // Update last packet grid
        LastPktGrid->Cells[LPG_SEQ_NBR][1]  = IntToStr( m_seqNbr );
        LastPktGrid->Cells[LPG_TICK][1]     = IntToStr( (int)currReading.recTime );
        LastPktGrid->Cells[LPG_ROTATION][1] = IntToStr( (int)m_rotation );
        LastPktGrid->Cells[LPG_TORQUE][1]   = IntToStr( currReading.torque );
    }
}


bool TSimMainForm::SendCfgDataResponse( const WTTTS_CFG_DATA& cfgData )
{
    BYTE txBuffer[MAX_TESTORK_PKT_LEN];

    WTTTS_PKT_HDR* pPktHdr = (WTTTS_PKT_HDR*)txBuffer;

    pPktHdr->pktHdr    = TESTORK_HDR_RX;
    pPktHdr->pktType   = WTTTS_RESP_CFG_DATA;
    pPktHdr->seqNbr    = 0;
    pPktHdr->timeStamp = GetTickCount() / 10;
    pPktHdr->dataLen   = sizeof( WTTTS_CFG_DATA );

    WTTTS_CFG_DATA* pData = (WTTTS_CFG_DATA*) &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

    *pData = cfgData;

    int packetHdrAndDataLen = sizeof( WTTTS_PKT_HDR ) + sizeof( WTTTS_CFG_DATA );

    txBuffer[ packetHdrAndDataLen ] = CalcWTTTSChecksum( txBuffer, packetHdrAndDataLen );

    DWORD buffLen   = packetHdrAndDataLen + 1;
    DWORD bytesSent = m_commObj->CommSend( txBuffer, buffLen );

    return( buffLen == bytesSent );
}


void TSimMainForm::SendRequestForCmd( bool sendNow )
{
    // Send a RFC command now (if param is true) or if the RFC timer has expired
    if( sendNow || ( GetTickCount() >= m_lastRFCTime + m_rfcRate ) )
    {
        BYTE txBuffer[MAX_TESTORK_PKT_LEN];
        int  sizeofPayload = 0;

        union
        {
            BYTE  byData[4];
            WORD  wData[2];
            DWORD dwData;
        } transferBuff;

        WTTTS_PKT_HDR* pPktHdr = (WTTTS_PKT_HDR*)txBuffer;

        if( RFCDataForm->RFCType == eUseRFCV1 )
        {
            pPktHdr->pktHdr    = TESTORK_HDR_RX;
            pPktHdr->pktType   = WTTTS_RESP_REQ_FOR_CMD_V1;
            pPktHdr->seqNbr    = 0;
            pPktHdr->timeStamp = GetTickCount() / 10;
            pPktHdr->dataLen   = sizeof( WTTTS_RFC_V1_PKT );

            WTTTS_RFC_V1_PKT* pData = (WTTTS_RFC_V1_PKT*) &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

            memset( pData, 0, sizeof( WTTTS_RFC_V1_PKT ) );

            pData->temperature    = 0xC1;
            pData->battType       = 1;
            pData->currMode       = 1;
            pData->rpm            = m_lastRPM;
            pData->lastReset      = 1;
            pData->rfChannel      = 11;
            pData->rfcRate        = m_rfcRate;
            pData->rfcTimeout     = m_rfcTimeout;
            pData->streamRate     = m_streamRate;
            pData->streamTimeout  = m_streamTimeout;
            pData->pairingTimeout = m_pairingTimeout;
            pData->battVoltage    = (WORD)RFCDataForm->BattVoltage;
            pData->battUsed       = (WORD)RFCDataForm->BattmAhUsed;

            // Check if we should use data from controls or RFC form
            // For now, we only set RPM and Torque
            if( SimSrcCombo->ItemIndex == 2 )
            {
                pData->rpm = RFCDataForm->RPM;

                transferBuff.dwData = RFCDataForm->Tq045;
                pData->torque045[0] = transferBuff.byData[0];
                pData->torque045[1] = transferBuff.byData[1];
                pData->torque045[2] = transferBuff.byData[2];

                transferBuff.dwData = RFCDataForm->Tq225;
                pData->torque225[0] = transferBuff.byData[0];
                pData->torque225[1] = transferBuff.byData[1];
                pData->torque225[2] = transferBuff.byData[2];
            }
            else
            {
                pData->rpm = m_lastRPM;

                // We don't have access to the torque, read it directly
                SIM_READING currReading;
                ReadControls( currReading );

                transferBuff.dwData = currReading.torque;

                pData->torque045[0] = transferBuff.byData[0];
                pData->torque045[1] = transferBuff.byData[1];
                pData->torque045[2] = transferBuff.byData[2];

                transferBuff.dwData = currReading.torque;

                pData->torque225[0] = transferBuff.byData[0];
                pData->torque225[1] = transferBuff.byData[1];
                pData->torque225[2] = transferBuff.byData[2];
            }

            transferBuff.dwData = RFCDataForm->Tension000;

            pData->tension000[0] = transferBuff.byData[0];
            pData->tension000[1] = transferBuff.byData[1];
            pData->tension000[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->Tension090;

            pData->tension090[0] = transferBuff.byData[0];
            pData->tension090[1] = transferBuff.byData[1];
            pData->tension090[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->Tension180;

            pData->tension180[0] = transferBuff.byData[0];
            pData->tension180[1] = transferBuff.byData[1];
            pData->tension180[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->Tension270;

            pData->tension270[0] = transferBuff.byData[0];
            pData->tension270[1] = transferBuff.byData[1];
            pData->tension270[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->RTD;

            pData->pdsSwitch[0] = transferBuff.byData[0];
            pData->pdsSwitch[1] = transferBuff.byData[1];
            pData->pdsSwitch[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->CompX;

            pData->compassX[0] = transferBuff.byData[0];
            pData->compassX[1] = transferBuff.byData[1];

            transferBuff.dwData = RFCDataForm->CompY;

            pData->compassY[0] = transferBuff.byData[0];
            pData->compassY[1] = transferBuff.byData[1];

            transferBuff.dwData = RFCDataForm->CompZ;

            pData->compassZ[0] = transferBuff.byData[0];
            pData->compassZ[1] = transferBuff.byData[1];

            transferBuff.dwData = RFCDataForm->AccelX;

            pData->accelX[0] = transferBuff.byData[0];
            pData->accelX[1] = transferBuff.byData[1];

            transferBuff.dwData = RFCDataForm->AccelY;

            pData->accelY[0] = transferBuff.byData[0];
            pData->accelY[1] = transferBuff.byData[1];

            transferBuff.dwData = RFCDataForm->AccelZ;

            pData->accelZ[0] = transferBuff.byData[0];
            pData->accelZ[1] = transferBuff.byData[1];

            sizeofPayload = sizeof( WTTTS_RFC_V1_PKT );
        }
        else
        {
            pPktHdr->pktHdr    = TESTORK_HDR_RX;
            pPktHdr->pktType   = WTTTS_RESP_REQ_FOR_CMD_V2;
            pPktHdr->seqNbr    = 0;
            pPktHdr->timeStamp = GetTickCount() / 10;
            pPktHdr->dataLen   = sizeof( WTTTS_RFC_V2_PKT );

            WTTTS_RFC_V2_PKT* pData = (WTTTS_RFC_V2_PKT*) &( txBuffer[ sizeof( WTTTS_PKT_HDR ) ] );

            memset( pData, 0, sizeof( WTTTS_RFC_V2_PKT ) );

            pData->temperature    = 0xC1;
            pData->battType       = 1;
            pData->currMode       = 1;
            pData->lastReset      = 1;
            pData->rfChannel      = 11;
            pData->rfcRate        = m_rfcRate;
            pData->rfcTimeout     = m_rfcTimeout;
            pData->streamRate     = m_streamRate;
            pData->streamTimeout  = m_streamTimeout;
            pData->pairingTimeout = m_pairingTimeout;
            pData->battVoltage    = (WORD)RFCDataForm->BattVoltage;
            pData->battUsed       = (WORD)RFCDataForm->BattmAhUsed;

            // Check if we should use data from controls or RFC form
            // For now, we only set RPM and Torque
            if( SimSrcCombo->ItemIndex == 2 )
            {
                pData->rpm = RFCDataForm->RPM;

                transferBuff.dwData = RFCDataForm->Tq045;
                pData->torque045[0] = transferBuff.byData[0];
                pData->torque045[1] = transferBuff.byData[1];
                pData->torque045[2] = transferBuff.byData[2];

                transferBuff.dwData = RFCDataForm->Tq225;
                pData->torque225[0] = transferBuff.byData[0];
                pData->torque225[1] = transferBuff.byData[1];
                pData->torque225[2] = transferBuff.byData[2];
            }
            else
            {
                pData->rpm = m_lastRPM;

                // We don't have access to the torque, read it directly
                SIM_READING currReading;
                ReadControls( currReading );

                transferBuff.dwData = currReading.torque;

                pData->torque045[0] = transferBuff.byData[0];
                pData->torque045[1] = transferBuff.byData[1];
                pData->torque045[2] = transferBuff.byData[2];

                transferBuff.dwData = currReading.torque;

                pData->torque225[0] = transferBuff.byData[0];
                pData->torque225[1] = transferBuff.byData[1];
                pData->torque225[2] = transferBuff.byData[2];
            }

            transferBuff.dwData = RFCDataForm->Tension000;

            pData->tension000[0] = transferBuff.byData[0];
            pData->tension000[1] = transferBuff.byData[1];
            pData->tension000[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->Tension090;

            pData->tension090[0] = transferBuff.byData[0];
            pData->tension090[1] = transferBuff.byData[1];
            pData->tension090[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->Tension180;

            pData->tension180[0] = transferBuff.byData[0];
            pData->tension180[1] = transferBuff.byData[1];
            pData->tension180[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->Tension270;

            pData->tension270[0] = transferBuff.byData[0];
            pData->tension270[1] = transferBuff.byData[1];
            pData->tension270[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->CompX;

            pData->compassX[0] = transferBuff.byData[0];
            pData->compassX[1] = transferBuff.byData[1];

            transferBuff.dwData = RFCDataForm->CompY;

            pData->compassY[0] = transferBuff.byData[0];
            pData->compassY[1] = transferBuff.byData[1];

            transferBuff.dwData = RFCDataForm->CompZ;

            pData->compassZ[0] = transferBuff.byData[0];
            pData->compassZ[1] = transferBuff.byData[1];

            transferBuff.dwData = RFCDataForm->AccelX;

            pData->accelX[0] = transferBuff.byData[0];
            pData->accelX[1] = transferBuff.byData[1];

            transferBuff.dwData = RFCDataForm->AccelY;

            pData->accelY[0] = transferBuff.byData[0];
            pData->accelY[1] = transferBuff.byData[1];

            transferBuff.dwData = RFCDataForm->AccelZ;

            pData->accelZ[0] = transferBuff.byData[0];
            pData->accelZ[1] = transferBuff.byData[1];

            // V2 new fields
            transferBuff.dwData = RFCDataForm->Tq045_R3;

            pData->torque045_R3[0] = transferBuff.byData[0];
            pData->torque045_R3[1] = transferBuff.byData[1];
            pData->torque045_R3[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->Tq225_R3;

            pData->torque225_R3[0] = transferBuff.byData[0];
            pData->torque225_R3[1] = transferBuff.byData[1];
            pData->torque225_R3[2] = transferBuff.byData[2];

            transferBuff.dwData = RFCDataForm->RTD;

            pData->rtdReading[0] = transferBuff.byData[0];
            pData->rtdReading[1] = transferBuff.byData[1];
            pData->rtdReading[2] = transferBuff.byData[2];

            pData->deviceID[0] = 0;
            pData->deviceID[1] = 1;
            pData->deviceID[2] = 2;
            pData->deviceID[3] = 3;
            pData->deviceID[4] = 4;
            pData->deviceID[5] = 5;

            sizeofPayload = sizeof( WTTTS_RFC_V2_PKT );
        }

        int packetHdrAndDataLen = sizeof( WTTTS_PKT_HDR ) + sizeofPayload;

        txBuffer[ packetHdrAndDataLen ] = CalcWTTTSChecksum( txBuffer, packetHdrAndDataLen );

        DWORD buffLen   = packetHdrAndDataLen + 1;
        DWORD bytesSent = m_commObj->CommSend( txBuffer, buffLen );

        // Update log memo
        UnicodeString sResult;

        if( bytesSent == buffLen )
            sResult = "good";
        else
            sResult = "failed";

        AddMemoEntry( "send RFC " + sResult );

        m_lastRFCTime = GetTickCount();
    }
}


void TSimMainForm::GetCfgData( BYTE whichPage )
{
    WTTTS_CFG_DATA cfgData;

    bool bGetGood = CalFactorsForm->GetConfigData( whichPage, cfgData );

    SendCfgDataResponse( cfgData );

    // Update log memo
    UnicodeString sResult;

    if( bGetGood )
        sResult = " good";
    else
        sResult = " failed";

    AddMemoEntry( "get cfg page " + IntToStr( whichPage ) + sResult );

    SendRequestForCmd( true );
}


void TSimMainForm::SetCfgData( WTTTS_CFG_DATA& cfgData )
{
    if( CalFactorsForm->SetConfigData( cfgData ) )
    {
        AddMemoEntry( "set cfg page " + IntToStr( cfgData.pageNbr ) + " good" );

        // A successful 'set' requires that the WTTTS follows-up with a get of the same page
        // GetCfgData() will also trigger an RFC being sent afterwards.
        GetCfgData( cfgData.pageNbr );
    }
    else
    {
        // Set data failed!
        WTTTS_CFG_DATA failedData;

        failedData.pageNbr = cfgData.pageNbr;
        failedData.result  = WTTTS_PG_RESULT_BAD_PG;

        memset( failedData.pageData, 0xFF, NBR_BYTES_PER_WTTTS_PAGE );

        SendCfgDataResponse( failedData );

        AddMemoEntry( "set cfg page " + IntToStr( cfgData.pageNbr ) + " failed" );

        SendRequestForCmd( true );
    }
}


void TSimMainForm::SetRates( const WTTTS_RATE_PKT& ratePkt )
{
    switch( ratePkt.rateType )
    {
        case SRT_RFC_RATE:        m_rfcRate        = ratePkt.newValue;   break;
        case SRT_RFC_TIMEOUT:     m_rfcTimeout     = ratePkt.newValue;   break;
        case SRT_STREAM_RATE:     m_streamRate     = ratePkt.newValue;   break;
        case SRT_STREAM_TIMEOUT:  m_streamTimeout  = ratePkt.newValue;   break;
        case SRT_PAIR_TIMEOUT:    m_pairingTimeout = ratePkt.newValue;   break;
    }

    // Refresh status display
    StreamRateEdit->Text = IntToStr( m_streamRate );
    RFCRateEdit->Text    = IntToStr( m_rfcRate );
}


void __fastcall TSimMainForm::DebugPopupPopup(TObject *Sender)
{
    ForceStart1->Enabled = !m_streaming;
    ForceStop1->Enabled  =  m_streaming;
}


void __fastcall TSimMainForm::ForceStart1Click(TObject *Sender)
{
    StartDataStreaming();
}


void __fastcall TSimMainForm::ForceStop1Click(TObject *Sender)
{
    StopDataStreaming();
}


void __fastcall TSimMainForm::SendRFCItemClick(TObject *Sender)
{
    // User should only click this if simulating a WTTTS sub
    SendRequestForCmd( true );
}


void __fastcall TSimMainForm::RPMTorqueBoxPaint(TObject *Sender)
{
    // Paint the RPM / Torque box. Draw the outline first.
    TCanvas* pCanvas = RPMTorqueBox->Canvas;

    pCanvas->Brush->Color = clWindowFrame;
    pCanvas->Brush->Style = bsSolid;

    TRect frameRect( 0, 0, RPMTorqueBox->Width, RPMTorqueBox->Height );
    pCanvas->FrameRect( frameRect );

    pCanvas->Brush->Color = clWindow;
    pCanvas->Brush->Style = bsSolid;

    TRect fillRect( 1, 1, RPMTorqueBox->Width - 1, RPMTorqueBox->Height - 1 );
    pCanvas->FillRect( fillRect );

    // Draw the RPM / Torque point. RPM is along x-axis.
    int maxTorque = TorqueMaxCombo->Items->Strings[ TorqueMaxCombo->ItemIndex ].ToInt();
    int maxRPM    = RPMMaxCombo->Items->Strings[ RPMMaxCombo->ItemIndex ].ToInt();

    int xPos = RPMTorqueBox->Width  * abs( m_lastRPM    ) / maxRPM;
    int yPos = RPMTorqueBox->Height * abs( m_lastTorque ) / maxTorque;

    // Reflect the yPos so that we have increasing values from the bottom
    yPos = RPMTorqueBox->Height - yPos;

    // Draw the spot
    const int spotWidth = 4;

    pCanvas->Pen->Color   = clRed;
    pCanvas->Pen->Style   = psSolid;
    pCanvas->Pen->Width   = 1;

    pCanvas->Brush->Color = clRed;
    pCanvas->Brush->Style = bsSolid;

    pCanvas->Ellipse( xPos, yPos, xPos + spotWidth, yPos + spotWidth );
}


void __fastcall TSimMainForm::RPMTorqueBoxMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
    // Move the current values if the left mouse button is down
    if( Shift.Contains( ssLeft ) )
    {
        int maxTorque = TorqueMaxCombo->Items->Strings[ TorqueMaxCombo->ItemIndex ].ToInt();
        int maxRPM    = RPMMaxCombo->Items->Strings[ RPMMaxCombo->ItemIndex ].ToInt();

        // Reflect the Y pos before use
        Y = RPMTorqueBox->Height - Y;

        // Scale to 'real' values
        m_lastRPM    = X * maxRPM    / RPMTorqueBox->Width;
        m_lastTorque = Y * maxTorque / RPMTorqueBox->Height;

        RPMTorqueBox->Invalidate();
    }
}

void __fastcall TSimMainForm::RPMTorqueBoxMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y)
{
    // If the left mouse button is being clicked, call the mouse move
    // handler to do the work
    if( Button == mbLeft )
        RPMTorqueBoxMouseMove( RPMTorqueBox, TShiftState() << ssLeft, X, Y );
}


void __fastcall TSimMainForm::SubPosBoxPaint(TObject *Sender)
{
    // Paint the sub pos box. Draw the outline first.
    TCanvas* pCanvas = SubPosBox->Canvas;

    pCanvas->Brush->Color = clWindowFrame;
    pCanvas->Brush->Style = bsSolid;

    TRect frameRect( 0, 0, SubPosBox->Width, SubPosBox->Height );
    pCanvas->FrameRect( frameRect );

    pCanvas->Brush->Color = clWindow;
    pCanvas->Brush->Style = bsSolid;

    TRect fillRect( 1, 1, SubPosBox->Width - 1, SubPosBox->Height - 1 );
    pCanvas->FillRect( fillRect );

    // Draw the sub outline
    pCanvas->Pen->Color   = clBlue;
    pCanvas->Pen->Style   = psSolid;
    pCanvas->Pen->Width   = 2;

    pCanvas->Brush->Color = clWindow;
    pCanvas->Brush->Style = bsSolid;

    pCanvas->Ellipse( 2, 2, SubPosBox->Width - 2, SubPosBox->Height - 2 );

    // Draw an arm radiating outwards from the center of the box to its circumference
    float armLen = ( SubPosBox->Width - 4 ) / 2;
    float armPos = (float)( m_rotation % 1000000 );

    float armDeg = armPos / 1000000.0 * 2.0 * M_PI;

    int xOffset = (int)( armLen * cos( armDeg - M_PI_2 ) + 0.5 );
    int yOffset = (int)( armLen * sin( armDeg - M_PI_2 ) + 0.5 );

    pCanvas->Pen->Color   = clRed;
    pCanvas->Pen->Style   = psSolid;
    pCanvas->Pen->Width   = 2;

    int xCenter = SubPosBox->Width / 2;
    int yCenter = SubPosBox->Height / 2;

    pCanvas->MoveTo( xCenter, yCenter );
    pCanvas->LineTo( xCenter + xOffset, yCenter + yOffset );
}


void __fastcall TSimMainForm::CalDataBtnClick(TObject *Sender)
{
    CalFactorsForm->ShowModal();
}


void __fastcall TSimMainForm::RFCDataBtnClick(TObject *Sender)
{
    RFCDataForm->Show();
}


void TSimMainForm::AddMemoEntry( UnicodeString newEntry )
{
    if( PauseBtn->Tag == 0 )
    {
        while( LogMemo->Lines->Count > 500 )
            LogMemo->Lines->Delete( 0 );

        LogMemo->Lines->Add( Time().TimeString() + " " + newEntry );
    }
}


void __fastcall TSimMainForm::PauseBtnClick(TObject *Sender)
{
    if( PauseBtn->Tag == 1 )
    {
        PauseBtn->Tag     = 0;
        PauseBtn->Caption = "Pause";
    }
    else
    {
        PauseBtn->Tag     = 1;
        PauseBtn->Caption = "Resume";
    }
}


void __fastcall TSimMainForm::ClearMemoBtnClick(TObject *Sender)
{
    LogMemo->Lines->Clear();
}


void __fastcall TSimMainForm::AdvancedButtonClick(TObject *Sender)
{
    if( m_PageState == eSimplePage )
    {
        // Setup the page to be advanced
        m_PageState = eAdvancedPage;

        SimMainForm->Constraints->MaxWidth  = ADVANCED_WIDTH;
        SimMainForm->Constraints->MaxHeight = ADVANCED_HEIGHT;
        SimMainForm->Constraints->MinWidth  = ADVANCED_WIDTH;
        SimMainForm->Constraints->MinHeight = ADVANCED_HEIGHT;

        SimMainForm->Width  = ADVANCED_WIDTH;
        SimMainForm->Height = ADVANCED_HEIGHT;

        SimPage->TabVisible = true;
        LogPage->TabVisible = true;

        PageControl->ActivePage = SimPage;
    }
    else
    {
        // Setup the page to be simple
        m_PageState = eSimplePage;

        SimMainForm->Constraints->MaxWidth  = SIMPLE_WIDTH;
        SimMainForm->Constraints->MaxHeight = SIMPLE_HEIGHT;
        SimMainForm->Constraints->MinWidth  = SIMPLE_WIDTH;
        SimMainForm->Constraints->MinHeight = SIMPLE_HEIGHT;

        SimMainForm->Width  = SIMPLE_WIDTH;
        SimMainForm->Height = SIMPLE_HEIGHT;

        SimPage->TabVisible = false;
        LogPage->TabVisible = false;

        PageControl->ActivePage = SimPage;
    }
}


