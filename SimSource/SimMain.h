#ifndef SimMainH
#define SimMainH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Grids.hpp>
#include "CommObj.h"
#include "WTTTSProtocolUtils.h"
#include <Vcl.Imaging.jpeg.hpp>

class TSimMainForm : public TForm
{
__published:	// IDE-managed Components
    TTimer *SimTimer;
    TGroupBox *ConnectionGB;
    TLabel *Label1;
    TEdit *CommPortEdit;
    TLabel *Label2;
    TEdit *BitRateEdit;
    TLabel *Label3;
    TEdit *HostIPEdit;
    TLabel *Label4;
    TEdit *UDPPortEdit;
    TRadioButton *SerialModeRB;
    TRadioButton *UDPModeRB;
    TMemo *LogMemo;
    TLabel *Label5;
    TButton *ClearMemoBtn;
    TGroupBox *SimCtrlGB;
    TLabel *Label7;
    TComboBox *SimSrcCombo;
    TLabel *Label8;
    TLabel *Label9;
    TEdit *FileNameEdit;
    TOpenDialog *OpenDlg;
    TComboBox *RPMMaxCombo;
    TComboBox *TorqueMaxCombo;
    TCheckBox *RandomizeCB;
    TPopupMenu *DebugPopup;
    TMenuItem *ForceStart1;
    TMenuItem *ForceStop1;
    TStringGrid *LastPktGrid;
    TLabel *Label10;
    TPaintBox *RPMTorqueBox;
    TLabel *Label11;
    TLabel *Label12;
    TLabel *Label13;
    TPaintBox *SubPosBox;
    TButton *CalDataBtn;
    TMenuItem *SendRFCItem;
    TButton *PauseBtn;
    TEdit *StreamRateEdit;
    TLabel *Label14;
    TEdit *RFCRateEdit;
    TButton *RFCDataBtn;
    TPageControl *PageControl;
    TTabSheet *SimPage;
    TTabSheet *LogPage;
    TImage *TescoLogo;
    TGroupBox *SimGroupBox;
    TButton *ConnDiscButton;
    TButton *AdvancedButton;
    void __fastcall SimTimerTimer(TObject *Sender);
    void __fastcall ModeRBClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall ClearMemoBtnClick(TObject *Sender);
    void __fastcall TorqueMaxComboClick(TObject *Sender);
    void __fastcall RPMMaxComboClick(TObject *Sender);
    void __fastcall FileNameEditDblClick(TObject *Sender);
    void __fastcall ForceStart1Click(TObject *Sender);
    void __fastcall ForceStop1Click(TObject *Sender);
    void __fastcall RPMTorqueBoxPaint(TObject *Sender);
    void __fastcall RPMTorqueBoxMouseMove(TObject *Sender, TShiftState Shift, int X,
          int Y);
    void __fastcall RPMTorqueBoxMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
    void __fastcall DebugPopupPopup(TObject *Sender);
    void __fastcall SubPosBoxPaint(TObject *Sender);
    void __fastcall CalDataBtnClick(TObject *Sender);
    void __fastcall SendRFCItemClick(TObject *Sender);
    void __fastcall PauseBtnClick(TObject *Sender);
    void __fastcall RFCDataBtnClick(TObject *Sender);
    void __fastcall ConnDiscButtonClick(TObject *Sender);
    void __fastcall AdvancedButtonClick(TObject *Sender);



private:

    typedef enum {
        eConnectButton,
        eDisconnectButton,
        eNumButtonStates
    } BUTTON_STATE;

    typedef enum {
        eSimplePage,
        eAdvancedPage,
        eNumPageStates
    } PAGE_STATE;

    // Page state
    PAGE_STATE    m_PageState;

    // Simulation control vars
    bool          m_streaming;
    bool          m_InFileSendLoop;
    DWORD         m_lastTxPkt;
    DWORD         m_startTime;
    int           m_rotation;
    BYTE          m_seqNbr;
    int           m_lastTorque;
    int           m_lastRPM;

    DWORD         m_lastRFCTime;

    // Rates, as received from WTTTS Host
    WORD          m_rfcRate;
    WORD          m_rfcTimeout;
    WORD          m_streamRate;
    WORD          m_streamTimeout;
    WORD          m_pairingTimeout;

    // csv line reading vars
    int           m_PrevTime;
    int           m_PrevTurns;

    // File input vars
    TStringList*  m_simData;
    int           m_nextReading;

    CCommObj*     m_commObj;

    #define COMM_BUFF_LEN  2048
    BYTE          m_commBuff[COMM_BUFF_LEN];
    DWORD         m_buffCount;

    void AddMemoEntry( UnicodeString newEntry );

    void StartDataStreaming( void );
    void StopDataStreaming( void );

    // The following struct is used for getting simulation values,
    // and then building the output packet
    typedef struct {
        DWORD recTime;      // Relative time, in msecs, that this record should be used
        int   RPM;
        int   torque;
        int   turns;
        int   tension;
    } SIM_READING;

    SIM_READING m_lastReading;

    void ReadSimFileEntry( SIM_READING& currReading );
    void ReadControls( SIM_READING& currReading );

    bool ConvertCSVStringToReading( const UnicodeString csvLine, SIM_READING& aReading );

    DWORD BuildWTTTSPkt( BYTE txBuffer[], BYTE seqNbr, DWORD tickCount, int rotation, WORD torque, int tension );

    void GetCfgData( BYTE whichPage );
    void SetCfgData( WTTTS_CFG_DATA& cfgData );

    void SendLastReading( SIM_READING& currReading );

    bool SendCfgDataResponse( const WTTTS_CFG_DATA& cfgData );
    void SendRequestForCmd( bool sendNow );

    void SetRates( const WTTTS_RATE_PKT& ratePkt );

    #define WM_STREAM_FROM_FILE     ( WM_APP + 1 )

    void __fastcall WMStreamFromFile( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_STREAM_FROM_FILE, TMessage, WMStreamFromFile )
    END_MESSAGE_MAP(TForm)

  public:		// User declarations
    __fastcall TSimMainForm(TComponent* Owner);
};

extern PACKAGE TSimMainForm *SimMainForm;

#endif
