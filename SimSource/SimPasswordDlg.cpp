#include <vcl.h>
#pragma hdrstop

#include "SimPasswordDlg.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TSimPasswordForm *SimPasswordForm;


__fastcall TSimPasswordForm::TSimPasswordForm(TComponent* Owner) : TForm(Owner)
{
    m_Password = "tesco";
}


bool TSimPasswordForm::ShowDlg( void )
{
    // Reset our visual fields to be safe
    PasswordEdit->Clear();
    IncorrectPassLabel->Visible = false;
    EnterButton->Enabled = false;

    ShowModal();

    return ( ModalResult == mrOk );
}


void __fastcall TSimPasswordForm::EnterButtonClick(TObject *Sender)
{
    // For now, we're going to use a hardcoded password
    if( PasswordEdit->Text.Trim() != m_Password )
    {
        IncorrectPassLabel->Visible = true;
        PasswordEdit->SelectAll();

        return;
    }

    // Fall through means we're good
    ModalResult = mrOk;
}


void __fastcall TSimPasswordForm::PasswordEditChange(TObject *Sender)
{
    IncorrectPassLabel->Visible = false;

    if( PasswordEdit->Text.Trim().IsEmpty() )
        EnterButton->Enabled = false;
    else
        EnterButton->Enabled = true;
}
