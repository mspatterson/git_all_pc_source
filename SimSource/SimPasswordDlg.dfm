object SimPasswordForm: TSimPasswordForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Enter Password'
  ClientHeight = 99
  ClientWidth = 256
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    256
    99)
  PixelsPerInch = 96
  TextHeight = 13
  object IncorrectPassLabel: TLabel
    Left = 32
    Top = 39
    Width = 97
    Height = 13
    Caption = 'Incorrect Password!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object PasswordEdit: TEdit
    Left = 32
    Top = 12
    Width = 192
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    PasswordChar = '*'
    TabOrder = 0
    OnChange = PasswordEditChange
  end
  object EnterButton: TButton
    Left = 82
    Top = 62
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Enter'
    Default = True
    Enabled = False
    TabOrder = 1
    OnClick = EnterButtonClick
    ExplicitLeft = 254
    ExplicitTop = 175
  end
  object ExitButton: TButton
    Left = 169
    Top = 62
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Exit'
    ModalResult = 2
    TabOrder = 2
    ExplicitLeft = 341
    ExplicitTop = 175
  end
end
