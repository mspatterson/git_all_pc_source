#ifndef SimPasswordDlgH
#define SimPasswordDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>


class TSimPasswordForm : public TForm
{
__published:    // IDE-managed Components
    TEdit *PasswordEdit;
    TButton *EnterButton;
    TButton *ExitButton;
    TLabel *IncorrectPassLabel;
    void __fastcall PasswordEditChange(TObject *Sender);
    void __fastcall EnterButtonClick(TObject *Sender);

private:    // User declarations
    String m_Password;

public:     // User declarations
    __fastcall TSimPasswordForm(TComponent* Owner);

    bool ShowDlg( void );

};

extern PACKAGE TSimPasswordForm *SimPasswordForm;

#endif
