#include <vcl.h>
#pragma hdrstop

#include "TAvgTestMainForm.h"
#include "Averaging.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TAvgTestMainForm *AvgTestMainForm;


typedef enum {
    AGC_INPUT_COL,
    AGC_NO_AVG_COL,
    AGC_BOX_CAR_NO_VAR,
    AGC_BOX_CAR_WITH_VAR,
    AGC_EXP_NO_VAR,
    AGC_EXP_WITH_VAR,
    NBR_AVG_GRID_COLS
} AVG_GRID_COL;


__fastcall TAvgTestMainForm::TAvgTestMainForm(TComponent* Owner) : TForm(Owner)
{
    ResultsGrid->ColCount = NBR_AVG_GRID_COLS;
    ResultsGrid->RowCount = 2;

    ResultsGrid->Cells[AGC_INPUT_COL][0]        = "Input";
    ResultsGrid->Cells[AGC_NO_AVG_COL][0]       = "No Avg";
    ResultsGrid->Cells[AGC_BOX_CAR_NO_VAR][0]   = "Box Car No Var";
    ResultsGrid->Cells[AGC_BOX_CAR_WITH_VAR][0] = "Box Car With Var";
    ResultsGrid->Cells[AGC_EXP_NO_VAR][0]       = "Exp No Var";
    ResultsGrid->Cells[AGC_EXP_WITH_VAR][0]     = "Exp With Var";
}


void __fastcall TAvgTestMainForm::LoadDataBtnClick(TObject *Sender)
{
    // Make sure the variance edit is valid
    float fVariance = (float)( VarianceEdit->Text.ToIntDef( -1 ) );

    if( fVariance < 0.0 )
    {
        MessageDlg( "You have entered an invalid Variance value.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = VarianceEdit;

        return;
    }

    int iBoxcarLen = BoxcarLenEdit->Text.ToIntDef( 0 );

    if( iBoxcarLen <= 0 )
    {
        MessageDlg( "You have entered an invalid Boxcar Length value.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = BoxcarLenEdit;

        return;
    }

    float fAlpha;

    bool bAlphaValid = TryStrToFloat( ExpAlphaEdit->Text, fAlpha );

    if( !bAlphaValid || ( fAlpha < 0.0 ) || ( fAlpha > 1.0 ) )
    {
        MessageDlg( "You have entered an invalid Alpha value.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = ExpAlphaEdit;

        return;
    }

    // Now try to open the file
    if( !FileOpenDlg->Execute() )
        return;

    TStringList* pInputStrings = new TStringList();

    try
    {
        pInputStrings->LoadFromFile( FileOpenDlg->FileName );
    }
    catch( ... )
    {
        MessageDlg( "The file could not be loaded.", mtError, TMsgDlgButtons() << mbOK, 0 );

        delete pInputStrings;
        return;
    }

    // Process the string list and remove any invalid entries
    int iItem = 0;

    while( iItem < pInputStrings->Count )
    {
        UnicodeString sText = Trim( pInputStrings->Strings[iItem] );

        double testValue;

        if( !TryStrToFloat( sText, testValue ) )
        {
            pInputStrings->Delete( iItem );
            continue;
        }

        // Item is valid
        iItem++;
    }

    if( pInputStrings->Count == 0 )
    {
        MessageDlg( "There were no valid lines in the file.", mtError, TMsgDlgButtons() << mbOK, 0 );

        delete pInputStrings;
        return;
    }

    ClearGrid();

    ResultsGrid->RowCount = ResultsGrid->FixedRows + pInputStrings->Count;

    // Create averagers
    TAverage* averagers[NBR_AVG_GRID_COLS];

    averagers[AGC_INPUT_COL]        = NULL;
    averagers[AGC_NO_AVG_COL]       = new TNoAverage();
    averagers[AGC_BOX_CAR_NO_VAR]   = new TMovingAverage( iBoxcarLen, 0.0 );
    averagers[AGC_BOX_CAR_WITH_VAR] = new TMovingAverage( iBoxcarLen, fVariance );
    averagers[AGC_EXP_NO_VAR]       = new TExpAverage( fAlpha, 0.0 );
    averagers[AGC_EXP_WITH_VAR]     = new TExpAverage( fAlpha, fVariance );

    for( int iItem = 0; iItem < pInputStrings->Count; iItem++ )
    {
        float fValue = Trim( pInputStrings->Strings[iItem] ).ToDouble();

        for( int iAvgr = 0; iAvgr < NBR_AVG_GRID_COLS; iAvgr++ )
        {
            if( iAvgr == 0 )
            {
                 ResultsGrid->Cells[0][iItem+1] = Trim( pInputStrings->Strings[iItem] );
            }
            else
            {
                averagers[iAvgr]->AddValue( fValue );

                ResultsGrid->Cells[iAvgr][iItem+1] = FloatToStrF( averagers[iAvgr]->AverageValue, ffFixed, 7, 0 );
            }
        }
    }

    delete pInputStrings;
}


void TAvgTestMainForm::ClearGrid( void )
{
    ResultsGrid->RowCount = 2;
    ResultsGrid->Rows[1]->Clear();
}


void __fastcall TAvgTestMainForm::SaveBtnClick(TObject *Sender)
{
    if( !FileSaveDlg->Execute() )
        return;

    TStringList* pOutStrings = new TStringList();

    // Output the header line first
    UnicodeString sLine;

    sLine.printf( L"Input,No Average,Boxcar %s Var 0,Boxcar %s Var %s,Exp %s Var 0, Exp %s Var %s",
                    BoxcarLenEdit->Text.w_str(), BoxcarLenEdit->Text.w_str(), VarianceEdit->Text.w_str(),
                    ExpAlphaEdit->Text.w_str(),  ExpAlphaEdit->Text.w_str(),  VarianceEdit->Text.w_str() );

    pOutStrings->Add( sLine );

    // Output the cell data now
    for( int iRow = ResultsGrid->FixedRows; iRow < ResultsGrid->RowCount; iRow++ )
    {
        for( int iCol = 0; iCol < ResultsGrid->ColCount; iCol++ )
        {
            if( iCol == 0 )
                sLine = ResultsGrid->Cells[iCol][iRow];
            else
                sLine = sLine + "," + ResultsGrid->Cells[iCol][iRow];
        }

        pOutStrings->Add( sLine );
    }

    try
    {
        pOutStrings->SaveToFile( FileSaveDlg->FileName );
    }
    catch( ... )
    {
        MessageDlg( "The file could not be saved.", mtError, TMsgDlgButtons() << mbOK, 0 );
    }

    delete pOutStrings;
}

