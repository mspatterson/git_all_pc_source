object AvgTestMainForm: TAvgTestMainForm
  Left = 0
  Top = 0
  Caption = 'Average Test Program'
  ClientHeight = 467
  ClientWidth = 614
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    614
    467)
  PixelsPerInch = 96
  TextHeight = 13
  object ResultsGrid: TStringGrid
    Left = 8
    Top = 115
    Width = 598
    Height = 313
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 6
    DefaultColWidth = 90
    TabOrder = 0
  end
  object VarianceGB: TGroupBox
    Left = 8
    Top = 8
    Width = 598
    Height = 101
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Variance '
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 20
      Width = 41
      Height = 13
      Caption = 'Variance'
    end
    object Label2: TLabel
      Left = 187
      Top = 20
      Width = 251
      Height = 13
      Caption = 'Enter positive value (0 disables variance processing)'
    end
    object Label3: TLabel
      Left = 16
      Top = 47
      Width = 86
      Height = 13
      Caption = 'Exponential Alpha'
    end
    object Label4: TLabel
      Left = 16
      Top = 74
      Width = 55
      Height = 13
      Caption = 'Boxcar Size'
    end
    object VarianceEdit: TEdit
      Left = 108
      Top = 17
      Width = 65
      Height = 21
      TabOrder = 0
      Text = '0'
    end
    object ExpAlphaEdit: TEdit
      Left = 108
      Top = 44
      Width = 65
      Height = 21
      TabOrder = 1
      Text = '0.0'
    end
    object BoxcarLenEdit: TEdit
      Left = 108
      Top = 71
      Width = 65
      Height = 21
      TabOrder = 2
      Text = '1'
    end
  end
  object LoadDataBtn: TButton
    Left = 8
    Top = 434
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Load Data'
    TabOrder = 2
    OnClick = LoadDataBtnClick
  end
  object SaveBtn: TButton
    Left = 111
    Top = 434
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Save to File'
    TabOrder = 3
    OnClick = SaveBtnClick
  end
  object FileOpenDlg: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'Text Files (*.txt)'
        FileMask = '*.txt'
      end
      item
        DisplayName = 'All Files (*.*)'
        FileMask = '*.*'
      end>
    Options = [fdoPathMustExist, fdoFileMustExist]
    Title = 'Select Data File'
    Left = 568
    Top = 8
  end
  object FileSaveDlg: TFileSaveDialog
    DefaultExtension = 'csv'
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'Text Files (*.txt)'
        FileMask = '*.txt'
      end
      item
        DisplayName = 'CSV Files (*.csv)'
        FileMask = '*.csv'
      end
      item
        DisplayName = 'All Files (*.*)'
        FileMask = '*.*'
      end>
    FileTypeIndex = 2
    Options = [fdoOverWritePrompt, fdoPathMustExist]
    Title = 'Save Data to File'
    Left = 536
    Top = 8
  end
end
