//---------------------------------------------------------------------------

#ifndef TAvgTestMainFormH
#define TAvgTestMainFormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------
class TAvgTestMainForm : public TForm
{
__published:	// IDE-managed Components
    TStringGrid *ResultsGrid;
    TGroupBox *VarianceGB;
    TLabel *Label1;
    TEdit *VarianceEdit;
    TLabel *Label2;
    TButton *LoadDataBtn;
    TFileOpenDialog *FileOpenDlg;
    TButton *SaveBtn;
    TLabel *Label3;
    TEdit *ExpAlphaEdit;
    TLabel *Label4;
    TEdit *BoxcarLenEdit;
    TFileSaveDialog *FileSaveDlg;
    void __fastcall LoadDataBtnClick(TObject *Sender);
    void __fastcall SaveBtnClick(TObject *Sender);

private:
    void ClearGrid( void );

public:
    __fastcall TAvgTestMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TAvgTestMainForm *AvgTestMainForm;
//---------------------------------------------------------------------------
#endif
