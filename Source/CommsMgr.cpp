//******************************************************************************
//
//  CommsMgr.cpp: This module provides a wrapper around the Sub Device
//        classes, and implements a thread to keep the comms layers
//        constantly updated.
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "CommsMgr.h"
#include "SubDeviceWTTTS.h"
#include "SubDeviceWTTS.h"
#include "SubDeviceNone.h"
#include "CUDPPort.h"
#include "USBPortDiscovery.h"
#include "messages.h"

#pragma package(smart_init)


//
// CDS / Slips Interlock Defines
//

// Commands to set the state of the output switches are continuously
// sent to the Base Radio at this rate in seconds
#define BR_SWITCH_CMD_TIME   3

// When receiving RFC's, may not get them on exact one-second intervals.
// So add a fixed amount to the RFC delay param for interlock processing.
#define INTERLOCK_RELEASE_MARGIN 750


const String TCommPoller::ConnResultStrings[TCommPoller::eCR_NumConnectResult] =
{
    "Initializing",
    "Bad Base Radio port",
    "Bad TesTORK port",
    "Bad Management port",
    "Connected",
    "Scanning for Radio",
    "Scanning for TesTORK",
    "Winsock() initialization error",
    "Unhandled comms thread exception",
};


static bool GetMutex( HANDLE aMutex, DWORD maxWait )
{
    // Return true if the mutex is acquired
    if( aMutex == NULL )
        return false;

    switch( WaitForSingleObject( aMutex, maxWait ) )
    {
        case WAIT_ABANDONED:
        case WAIT_OBJECT_0:
            // Have access to the mutex now
            return true;

        case WAIT_FAILED:
            // Not a good thing! Call failed.
            break;

        case WAIT_TIMEOUT:
            // Timeout expired!
            break;
    }
    // Fall through means mutex not acquired
    return false;
}


//
// Poll Thread Class Implementation
//

__fastcall TCommPoller::TCommPoller( HWND hwndEvent ) : TThread( false )
{
    // Save the event handle for later use
    m_hwndEvent = hwndEvent;

    // Init class vars
    m_device = NULL;
    m_radio  = NULL;

    m_devType = DT_NULL;

    SetPollerState( PS_INITIALIZING, eCR_Initializing );

    m_interlockState = IS_NO_HARDWARE;
    m_pdsPipeState   = PDS_DISABLED;    // For now

    m_dwRadioChanWaitTime = GetRadioChanWaitTimeout();

    GetWirelessSystemInfo( eWS_TesTork, m_wirelessInfo );

    // Initialize signalling var
    m_bSignalLostComms = false;

    // Bypass variable
    m_bTempZWIBypass = eBIS_NoBypass;

    // Init alarm params
    m_alarmParams.bcCmd          = eBC_NoCmd;
    m_alarmParams.dwRequestedDur = 0;
    m_alarmParams.dwStartTime    = 0;
    m_alarmParams.dwAlarmDur     = 0;

    // Caller must delete this object on terminate
    FreeOnTerminate = false;

    // Init the thread-safe packet transfer. A critical section
    // is used for this.
    InitializeCriticalSection( &m_criticalSection );

    FlushPendingPktQueue();

    // Init empty WITS fields
    m_WitsRfcFields.iNumFields     = 0;
    m_WitsRfcFields.pFieldSettings = NULL;
    m_WitsRfcFields.pWitsFields    = NULL;

    m_WitsStreamFields.iNumFields     = 0;
    m_WitsStreamFields.pFieldSettings = NULL;
    m_WitsStreamFields.pWitsFields    = NULL;

    // Force the poller to check for registry changes
    m_refreshSettings = true;
}


__fastcall TCommPoller::~TCommPoller()
{
    Terminate();

    switch( WaitForSingleObject( (HANDLE)Handle, 1000 /*msecs*/ ) )
    {
        case WAIT_FAILED:
            // Not a good thing - don't try to delete the object.
            break;

        case WAIT_ABANDONED:
        case WAIT_OBJECT_0:
            // Safe to delete the object in both these cases
            break;

        case WAIT_TIMEOUT:
            // Timeout expired! Don't delete the object in this case
            break;
    }

    // Release the critical section
    DeleteCriticalSection( &m_criticalSection );
}


void __fastcall TCommPoller::Execute()
{
    // Initialize and create objects
    // Winsock is required for UDP ports
    NameThreadForDebugging( AnsiString( "WTTTSThread" ) );

    if( !InitWinsock() )
    {
        SetPollerState( PS_COMMS_FAILED, eCR_WinsockError );
        return;
    }

    // Set the Poller to scanning
    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );

    // Loop here now executing the thread
    try
    {
        while( !Terminated )
        {
            // Disable the signalling of lost ports until we know we are connected
            m_bSignalLostComms = false;

            if( GetPortLost() || ( PollerState == PS_BR_PORT_SCAN ) || ( PollerState == PS_TT_PORT_SCAN ) )
            {
                // Debug
//                ::PostMessage( m_hwndEvent, WM_COMMS_DEBUG_EVENT, 0x0001, 0 );

                // Check which Port Opening method to use. Read this from the
                // registry each time in case it changes
                if( GetAutoAssignCommPorts() )
                {
                    while( !OpenAutomaticPorts() )
                    {
                        if( Terminated )
                            break;

                        Sleep( 100 );
                    }
                }
                else
                {
                    while( !OpenAssignedPorts() )
                    {
                        if( Terminated )
                            break;

                        Sleep( 100 );
                    }
                }
            }

            if( Terminated )
                break;

            // If we are hunting for the TT, loop here until it is found
            DWORD dwRFCWaitStart = GetTickCount();

            while( PollerState == PS_TT_CHAN_SCAN )
            {
                m_device->Update();
                m_radio->Update();

                if( Terminated )
                    break;

                if( !m_radio->IsReceiving )
                {
                    // If the base radio is not receiving, we are in big trouble.
                    // Need to rescan for all devices
                    DeleteDevices();
                    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
                }
                else if( m_device->DeviceIsRecving )
                {
                    SetPollerState( PS_RUNNING, eCR_Connected );
                    break;
                }
                else if( HaveTimeout( dwRFCWaitStart, m_dwRadioChanWaitTime ) )
                {
                    // Time to switch channels - the base radio can figure out
                    // the next channel
                    m_radio->GoToNextRadioChannel( eBRN_1, m_wirelessInfo.chanList, MAX_CHANS_PER_SYSTEM );

                    dwRFCWaitStart = GetTickCount();
                }
            }

            if( Terminated )
                break;

            // Make sure everything is still working
            if( PollerState == PS_RUNNING )
            {
                // We are running. Any loss of comms must be posted to the client.
                m_bSignalLostComms = true;

                // Update devices first
                m_device->Update();
                m_radio->Update();

                if( !m_radio->IsReceiving )
                {
                    // If the base radio is not receiving, we are in big trouble.
                    // Need to rescan for all devices. Note that we don't have to
                    // force the device into hunting mode in this case since we'll
                    // be deleting it.
                    DeleteDevices();
                    SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );
                }
                else if( !m_device->DeviceIsRecving )
                {
                    // Note we don't delete devices in this case - first try different
                    // channels. Also note that the device will already have put itself
                    // into a hunting state.
                    SetPollerState( PS_TT_CHAN_SCAN, eCR_HuntingForTT );
                }
                else
                {
                    // Get any sample data. If a flush is pending though, flush
                    // the queue and any samples pending
                    WTTTS_READING nextSample;
                    int rpm;

                    while( m_device->GetNextSample( nextSample, rpm ) )
                    {
                        if( !m_flushPending )
                            AddPktToPendingQueue( nextSample, rpm );

                        m_device->Update();
                    }

                    if( m_flushPending )
                    {
                        FlushPendingPktQueue();
                        m_flushPending = false;
                    }

                    // Copy the last raw data across too
                    EnterCriticalSection( &m_criticalSection );

                    m_bHaveRawPkt = m_device->GetLastDataRaw( m_lastRawPktmsecs, m_lastRawPktSeqNbr, m_lastRawPkt );

                    LeaveCriticalSection( &m_criticalSection );

                    // Interlock processing - first check if a refresh of the
                    // settings has been requested
                    if( m_refreshSettings )
                    {
                        // Refresh requested - get new values
                        m_currUnits = m_newUnits;

                        GetCDSSlipsSettings( m_currUnits, m_cdsReleased, m_cdsSlipLocked, m_cdsLockSlipRel );

                        m_releaseDelay = GetInterlockReleaseTime() * 1000 + INTERLOCK_RELEASE_MARGIN;

                        AVERAGING_PARAMS avgParams;

                        GetAveragingSettings( AT_TORQUE, avgParams );
                        m_device->SetAveraging( AT_TORQUE, avgParams );

                        GetAveragingSettings( AT_TENSION, avgParams );
                        m_device->SetAveraging( AT_TENSION, avgParams );

                        int iPDSOnThresh;
                        int iPDSOffThresh;

                        GetPDSSwitchThresholds( iPDSOnThresh, iPDSOffThresh );
                        m_device->SetPDSSwitchThresholds( iPDSOnThresh, iPDSOffThresh );

                        // Some of the above code should probably be moved into RefreshSettings()
                        m_device->RefreshSettings();

                        m_refreshSettings = false;

                        // Check if the PDS pipe sense has changed if the hardware is present
                        if( ( m_pdsPipeState == PDS_OPEN ) || ( m_pdsPipeState == PDS_CLOSED ) )
                        {
                            if( m_device->GetPDSClosed() )
                                m_pdsPipeState = PDS_CLOSED;
                            else
                                m_pdsPipeState = PDS_OPEN;
                        }

                        // Check if the state of the interlock enable has been changed.
                        // Do so only if interlock processing is operating.
                        if( m_interlockState != IS_NO_HARDWARE )
                            RefreshInterlockState();

                        // Refresh WITS settings
                        ReleaseCommPort( m_witsPort );

                        COMMS_CFG commsCfg;
                        GetWitsPortsSettings( commsCfg );

                        // Recreate the new port
                        CreateCommPort( commsCfg, m_witsPort );

                        // Update the WITS RFC Sendable fields
                        ClearWitsSendFields( m_WitsRfcFields );
                        ClearWitsSendFields( m_WitsStreamFields );

                        GetWitsSendFields( m_WitsRfcFields,    TSubDevice::eWPT_RFC    );
                        GetWitsSendFields( m_WitsStreamFields, TSubDevice::eWPT_Stream );
                    }

                    // Update any ZWI Bypass
                    if( m_bTempZWIBypass == eBIS_Starting )
                    {
                        m_bTempZWIBypass = eBIS_Bypassed;
                        RefreshInterlockState();
                    }
                    else if( m_bTempZWIBypass == eBIS_Bypassed )
                    {
                        // Check if the timeout has passed
                        if( HaveTimeout( m_dwTempBypassTime, m_dwTempBypassTimeout ) )
                        {
                            // Done disab
                            m_bTempZWIBypass = eBIS_NoBypass;

                            RefreshInterlockState();
                        }
                    }

                    // Update the PDS switch state. There is hysteresis built into the
                    // on / off thresholds
                    if( ( m_pdsPipeState == PDS_OPEN ) || ( m_pdsPipeState == PDS_CLOSED ) )
                    {
                        if( m_device->GetPDSClosed() )
                            m_pdsPipeState = PDS_CLOSED;
                        else if( m_device->GetPDSOpen() )
                            m_pdsPipeState = PDS_OPEN;

                        if( m_pdsPipeState == PDS_CLOSED )
                        {
                            if( ( m_outputSwitchState & ALARM_OUTPUT3_SWITCH_BIT ) == 0 )
                            {
                                m_outputSwitchState |= ALARM_OUTPUT3_SWITCH_BIT;
                                m_nextSwitchCmdTime  = 0;
                            }
                        }
                        else if( m_pdsPipeState == PDS_OPEN )
                        {
                            if( ( m_outputSwitchState & ALARM_OUTPUT3_SWITCH_BIT ) != 0 )
                            {
                                m_outputSwitchState &= ~ALARM_OUTPUT3_SWITCH_BIT;
                                m_nextSwitchCmdTime  = 0;
                            }
                        }
                    }

                    // Process the output alarm state. Note that this output is shared
                    // with the PDS alarm output. Get the current tick count outside
                    // of the critical section, so we keep the critical code fast. As
                    // well, watch out for a count of 0 as it means the alarm is off.
                    DWORD dwCurrTickCount = GetTickCount();

                    if( dwCurrTickCount == 0 )
                        dwCurrTickCount = 1;

                    // Now do the critical stuff
                    EnterCriticalSection( &m_criticalSection );

                    if( m_alarmParams.bcCmd == eBC_TurnOff )
                    {
                        m_alarmParams.dwStartTime = 0;
                        m_alarmParams.dwAlarmDur  = 0;

                        // Request that the alarm output be turned off
                        m_outputSwitchState &= ~ALARM_OUTPUT3_SWITCH_BIT;
                        m_nextSwitchCmdTime  = 0;

                        // Okay to clear the alarm command var
                        m_alarmParams.bcCmd = eBC_NoCmd;
                    }
                    else if( m_alarmParams.bcCmd == eBC_TurnOn )
                    {
                        // Update the start time only if we didn't have one before
                        if( m_alarmParams.dwStartTime == 0 )
                            m_alarmParams.dwStartTime = dwCurrTickCount;

                        // Add the requested duration onto any current duration
                        if( m_alarmParams.dwRequestedDur == 0xFFFFFFFF )
                            m_alarmParams.dwAlarmDur = 0xFFFFFFFF;
                        else
                            m_alarmParams.dwAlarmDur += m_alarmParams.dwRequestedDur;

                        // Request that the alarm output be turned on
                        m_outputSwitchState |= ALARM_OUTPUT3_SWITCH_BIT;
                        m_nextSwitchCmdTime  = 0;

                        // Okay to clear the alarm command var
                        m_alarmParams.bcCmd = eBC_NoCmd;
                    }

                    LeaveCriticalSection( &m_criticalSection );

                    // After having check for an alarm request, see if we have a
                    // timeout on any current alarm
                    if( m_alarmParams.dwStartTime != 0 )
                    {
                        if( m_alarmParams.dwRequestedDur != 0xFFFFFFFF )
                        {
                            if( HaveTimeout( m_alarmParams.dwStartTime, m_alarmParams.dwRequestedDur ) )
                            {
                                // Alarm time done. Clear out vars
                                m_alarmParams.dwStartTime = 0;
                                m_alarmParams.dwAlarmDur  = 0;

                                // Request that the alarm output be turned off
                                m_outputSwitchState &= ~ALARM_OUTPUT3_SWITCH_BIT;
                                m_nextSwitchCmdTime  = 0;
                            }
                        }
                    }

                    // Now process the interlock state machine. Only do so if we are
                    // attached to a base radio and interlock processing is enabled.
                    if( ( m_interlockState == IS_NO_HARDWARE ) || ( m_interlockState == IS_DISABLED ) )
                    {
                        // Nothing to do in these states.
                    }
                    else
                    {
                        // Get latest tension value. Note that GetLastAvgData() will fail
                        // if the TesTORK is neither idle nor streaming
                        float avgTorque;
                        float avgTension;
                        int   avgRPM;

                        if( GetLastAvgData( avgTorque, avgTension, avgRPM ) )
                        {
                            switch( m_interlockState )
                            {
                                case IS_BOTH_HOLDING:
                                    // Both interlocks are holding. We can switch into
                                    // one of the released states from here.
                                    if( avgTension < m_cdsReleased )
                                    {
                                        m_interlockState = IS_CDS_ENABLING;
                                        m_disengageTime  = GetTickCount();
                                    }
                                    else if( avgTension > m_cdsLockSlipRel )
                                    {
                                        m_interlockState = IS_SLIPS_RELEASING;
                                        m_disengageTime  = GetTickCount();
                                    }

                                    break;

                                case IS_CDS_ENABLING:
                                    // CDS interlock is releasing. If the tension
                                    // has increased again, revert back to being
                                    // both locked
                                    if( avgTension >= m_cdsReleased )
                                    {
                                        // Tension has increased above release
                                        // threshold - revert to engaged state
                                        m_interlockState = IS_BOTH_HOLDING;
                                    }
                                    else
                                    {
                                        // Tension is still below release thresh.
                                        // Check if its been there long enough
                                        if( HaveTimeout( m_disengageTime, m_releaseDelay ) )
                                        {
                                            m_interlockState = IS_CDS_ENABLED;

                                            // Ensure only one switch is closed at a time
                                            m_outputSwitchState |=  CDS_INTERLOCK_OUTPUT_SWITCH_BIT;
                                            m_outputSwitchState &= ~SLIPS_INTERLOCK_OUTPUT_SWITCH_BIT;

                                            m_nextSwitchCmdTime  = 0;
                                        }
                                    }
                                    break;

                                case IS_SLIPS_RELEASING:
                                    // The slips interlock is releasing. If the tension
                                    // has decreased again, revert back to being both
                                    // locked
                                    if( avgTension <= m_cdsLockSlipRel )
                                    {
                                        // Tension has fallen back down
                                        m_interlockState = IS_BOTH_HOLDING;
                                    }
                                    else
                                    {
                                        // Tension is still above release thresh.
                                        // Check if its been there long enough
                                        if( HaveTimeout( m_disengageTime, m_releaseDelay ) )
                                        {
                                            m_interlockState = IS_SLIPS_RELEASED;

                                            m_outputSwitchState |= SLIPS_INTERLOCK_OUTPUT_SWITCH_BIT;
                                            m_outputSwitchState &= ~CDS_INTERLOCK_OUTPUT_SWITCH_BIT;

                                            m_nextSwitchCmdTime  = 0;
                                        }
                                    }
                                    break;

                                case IS_CDS_ENABLED:
                                    // If the tension increases above the engage thresh,
                                    // re-engage both interlocks
                                    if( avgTension > m_cdsSlipLocked )
                                    {
                                        m_interlockState = IS_BOTH_HOLDING;

                                        m_outputSwitchState &= ~SLIPS_INTERLOCK_OUTPUT_SWITCH_BIT;
                                        m_outputSwitchState &= ~CDS_INTERLOCK_OUTPUT_SWITCH_BIT;

                                        m_nextSwitchCmdTime  = 0;
                                    }
                                    break;

                                case IS_SLIPS_RELEASED:
                                    // If the tension drops below the engage thresh,
                                    // re-engage both interlocks
                                    if( avgTension < m_cdsSlipLocked )
                                    {
                                        m_interlockState = IS_BOTH_HOLDING;

                                        m_outputSwitchState &= ~SLIPS_INTERLOCK_OUTPUT_SWITCH_BIT;
                                        m_outputSwitchState &= ~CDS_INTERLOCK_OUTPUT_SWITCH_BIT;

                                        m_nextSwitchCmdTime  = 0;
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            // GetLastAvgData() failed, so the device is neither idle nor
                            // streaming. In this condition the interlock must be engaged.
                            if( m_interlockState != IS_BOTH_HOLDING )
                            {
                                m_interlockState = IS_BOTH_HOLDING;

                                m_outputSwitchState &= ~SLIPS_INTERLOCK_OUTPUT_SWITCH_BIT;
                                m_outputSwitchState &= ~CDS_INTERLOCK_OUTPUT_SWITCH_BIT;

                                m_nextSwitchCmdTime  = 0;
                            }
                        }
                    }

                    // Determine if a 'set switch' command must be sent to the
                    // base radio. A command is required if the desired switch
                    // state does not match the output switch state. A command
                    // is also required even if the switch states match but
                    // at least one switch is on.
                    bool sendCmd = false;

                    if( m_outputSwitchState != m_radio->GetOutputState() )
                    {
                        // Our desired switch state does not match that of
                        // the base radio. Send an update command if our
                        // timer has expired
                        if( time( NULL ) >= m_nextSwitchCmdTime )
                            sendCmd = true;
                    }
                    else if( m_outputSwitchState != 0 )
                    {
                        if( time( NULL ) >= m_nextSwitchCmdTime )
                            sendCmd = true;
                    }

                    if( sendCmd )
                    {
                        m_radio->SetOutputState( m_outputSwitchState );

                        m_nextSwitchCmdTime = time( NULL ) + BR_SWITCH_CMD_TIME;
                    }

                }
            }

            // Work done for this iteration
            Sleep( 5 );
        }
    }
    catch( ... )
    {
        SetPollerState( PS_EXCEPTION, eCR_ThreadException );
    }

    // Release all comm mgr resources
    DeleteDevices();

    ShutdownWinsock();
}


bool TCommPoller::OpenAssignedPorts( void )
{
    while( true )
    {
        // Delete all devices safely
        DeleteDevices();

        // Create the devices
        CreateDevices();

        // Set the result to Scanning
        SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );

        COMMS_CFG mgmtCommCfg;
        COMMS_CFG baseCommCfg;
        COMMS_CFG deviceCommCfg;

        GetCommSettings( DCT_MGMT,  mgmtCommCfg );
        GetCommSettings( DCT_BASE,  baseCommCfg );
        GetCommSettings( DCT_WTTTS, deviceCommCfg );

        // Attempt to connect our devices
        if( ( m_device->Connect( deviceCommCfg ) ) && ( m_radio->Connect( baseCommCfg ) ) && ( m_mgmtPort->Connect( mgmtCommCfg ) ) )
        {
            // Check if we have a Null-Radio
            if( m_radio->ClassNameIs( "TNullBaseRadio" ) )
            {
                SetPollerState( PS_TT_CHAN_SCAN, eCR_HuntingForTT );
                return true;
            }

            // The port could be opened, wait here for 2 seconds for a
            // a message to appear from the base radio
            DWORD dwStartPktCount = m_radio->StatusPktCount;

            DWORD dwEndWait = GetTickCount() + 2500;

            while( GetTickCount() < dwEndWait )
            {
                m_radio->Update();

                if( m_radio->StatusPktCount > dwStartPktCount + 5 )
                {
                    // Found the base radio. Set the radio port onto its
                    // default channel and then move on to the MPL scan.
                    m_radio->SetRadioChannel( eBRN_1, GetDefaultRadioChan() );

                    AfterCommConnect( baseCommCfg );

                    SetPollerState( PS_TT_CHAN_SCAN, eCR_HuntingForTT );
                    return true;
                }

                if( Terminated )
                    return false;

                Sleep( 50 );
            }

            m_radio->Disconnect();
        }

        // Could not connect to devices, sleep for one second, then try
        // again on the same ports. Break the sleep into 100ms intervals
        // so we can respond to a terminate request quickly.
        for( int iSleepIndex = 0; iSleepIndex < 10; iSleepIndex++ )
        {
            Sleep( 100 );

            if( Terminated )
                return false;
        }
    }
}


bool TCommPoller::OpenAutomaticPorts( void )
{
    while( true )
    {
        // Delete all devices safely
        DeleteDevices();

        // Create the devices
        CreateDevices();

        // Set the result to Scanning
        SetPollerState( PS_BR_PORT_SCAN, eCR_HuntingForBase );

        COMMS_CFG baseCommCfg;
        COMMS_CFG deviceCommCfg;
        COMMS_CFG mgmtCommCfg;

        GetCommSettings( DCT_BASE,  baseCommCfg );
        GetCommSettings( DCT_WTTTS, deviceCommCfg );
        GetCommSettings( DCT_MGMT,  mgmtCommCfg );

        // For automatic ports, all port types must be serial
        baseCommCfg.portType   = CT_SERIAL;
        deviceCommCfg.portType = CT_SERIAL;
        mgmtCommCfg.portType   = CT_SERIAL;

        baseCommCfg.portParam   = 115200;
        deviceCommCfg.portParam = 115200;
        mgmtCommCfg.portParam   = 115200;

        // Init search loop
        int iPortNbr   = baseCommCfg.portName.ToIntDef( 1 );
        int iPortStart = iPortNbr;

        while( true )
        {
            // Set the Port Number for the radio
            baseCommCfg.portName = IntToStr( iPortNbr );

            // Attempt to connect our Base Radio
            if( m_radio->Connect( baseCommCfg ) )
            {
                // The port could be opened, wait here for 2 seconds for a
                // a message to appear from the base radio. The base radio
                // sends periodic status messages even if no command is
                // sent to it
                DWORD dwStartPktCount = m_radio->StatusPktCount;

                DWORD dwEndWait = GetTickCount() + 2500;

                while( GetTickCount() < dwEndWait )
                {
                    m_radio->Update();

                    if( m_radio->StatusPktCount > dwStartPktCount + 5 )
                    {
                        // Found the base radio. Set the MPL radio port onto its
                        // default channel now
                        m_radio->SetRadioChannel( eBRN_1, GetDefaultRadioChan() );

                        SetPollerState( PS_TT_PORT_SCAN, eCR_HuntingForTT );

                        // If we can't find the ports, return false
                        if( !FindDevPorts( baseCommCfg, deviceCommCfg, mgmtCommCfg ) )
                            return false;

                        // If the ports fail to connect, return false
                        if( ( !m_device->Connect( deviceCommCfg ) ) || ( !m_mgmtPort->Connect( mgmtCommCfg ) ) )
                            return false;

                        SaveCommSettings( DCT_BASE,  baseCommCfg );
                        SaveCommSettings( DCT_WTTTS, deviceCommCfg );
                        SaveCommSettings( DCT_MGMT,  mgmtCommCfg );

                        AfterCommConnect( baseCommCfg );

                        SetPollerState( PS_TT_CHAN_SCAN, eCR_HuntingForTT );

                        return true;
                    }

                    if( Terminated )
                        return false;

                    Sleep( 50 );
                }

                m_radio->Disconnect();
            }

            if( Terminated )
                return false;

            // Autoscan enabled, try next port
            iPortNbr++;

            if( iPortNbr == iPortStart )
                break;

            if( iPortNbr > 999 )
                iPortNbr = 1;

            Sleep( 10 );
        }

        Sleep( 100 );
    }
}


bool TCommPoller::FindDevPorts( const COMMS_CFG& baseCommCfg, COMMS_CFG& deviceCommCfg, COMMS_CFG& mgmtCommCfg )
{
    // Set up the CommPort and Connection Params
    CCommPort* pCommPort = new CCommPort();

    CCommPort::CONNECT_PARAMS CommPortParams;

    CommPortParams.dwLineBitRate = 9600;

    int iPortNum = 0;
    int iR0Port  = -1;
    int iR1Port  = -1;

    // Iterate through 1000 port numbers, attempt to connect to each
    while( iPortNum <= 999 )
    {
        CommPortParams.iPortNumber = iPortNum;

        // Check if we can connect to the port, if so, wiggle it's signals
        if( pCommPort->Connect( &CommPortParams ) == CCommObj::ERR_NONE )
        {
            // Get the Port Type of the current CommPort
            PORT_TYPE PortType = GetPortType( pCommPort );

            // Check if the Port Type returned was one we care about
            if( PortType == eRadio0 )
                iR0Port = iPortNum;
            else if( PortType == eRadio1 )
                iR1Port = iPortNum;
        }

        // Always disconnect to be safe
        pCommPort->Disconnect();

        // If we've found both ports in this iteration, try to find Mgmt
        if( ( iR0Port >= 0 ) && ( iR1Port >= 0 ) )
        {
            TList* pPorts = new TList();

            int iRadioPort = baseCommCfg.portName.ToIntDef( -1 );

            if( !GetAssociatedPorts( iRadioPort, pPorts ) )
            {
                delete pPorts;
                break;
            }

            pPorts->Remove( (TObject*)( iRadioPort ) );
            pPorts->Remove( (TObject*)( iR0Port    ) );
            pPorts->Remove( (TObject*)( iR1Port    ) );

            if( pPorts->Count != 1 )
            {
                delete pPorts;
                break;
            }

            // The TT device uses radio port 0 (MPL uses port 1)
            deviceCommCfg.portName = IntToStr( iR0Port );
            mgmtCommCfg.portName   = IntToStr( (int)( pPorts->List[0] ) );

            delete pPorts;
            delete pCommPort;

            return true;
        }

        // Be safe and check for termination
        if( Terminated )
            break;

        Sleep( 10 );

        iPortNum++;
    }

    // Clean-up and return
    delete pCommPort;

    return false;
}


TCommPoller::PORT_TYPE TCommPoller::GetPortType( CCommPort* pPort )
{
    // Init our Ctrl Status variables
    BYTE bInitCtrlStatus = 0;
    BYTE bHighCtrlStatus = 0;
    BYTE bLastCtrlStatus = 0;

    if( !WigglePort( pPort, bInitCtrlStatus, bHighCtrlStatus, bLastCtrlStatus ) )
        return eUnknown;

    // WigglePort() will raise RTS, drop it, and then raise it again. The base
    // radio reports the inverse state of the signal, so look in the control
    // signal status byte for a low-high-low sequence.
    if( !( bInitCtrlStatus & 0x01 ) && ( bHighCtrlStatus & 0x01 ) && !( bLastCtrlStatus & 0x01 ) )
        return eRadio0;
    else if( !( bInitCtrlStatus & 0x02 ) && ( bHighCtrlStatus & 0x02 ) && !( bLastCtrlStatus & 0x02 ) )
        return eRadio1;

    return eUnknown;
}


bool TCommPoller::WigglePort( CCommPort* pPort, BYTE& bInitCtrlStatus, BYTE& bHighCtrlStatus, BYTE& bLastCtrlStatus )
{
    // Init our Base Radio Packet variables
    BASE_STATUS_STATUS_PKT RadioStatusPkt;
    time_t tLastPkt;

    // Set RTS bit to low
    pPort->SetRTS( true );

    DWORD dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the Initial CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bInitCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    // Set RTS bit to high
    pPort->SetRTS( false );

    dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the High CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bHighCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    // Set RTS bit to low
    pPort->SetRTS( true );

    dwTimeout = GetTickCount() + 100;

    while( GetTickCount() < dwTimeout )
        m_radio->Update();

    // Get the Last CtrlStatus
    if( !m_radio->GetLastStatusPkt( tLastPkt, RadioStatusPkt ) )
        return false;

    bLastCtrlStatus = RadioStatusPkt.usbCtrlSignals;

    return true;
}


void TCommPoller::AfterCommConnect( COMMS_CFG baseCommCfg )
{
    // Set initial interlock state. It depends on the type of base
    // radio we are connected to and whether the user has enabled
    // this function
    if( ( baseCommCfg.portType == CT_UNUSED ) || ( m_devType == DT_NULL ) )
        m_interlockState = IS_NO_HARDWARE;
    else if( GetInterlockEn() )
        m_interlockState = IS_BOTH_HOLDING;
    else
        m_interlockState = IS_DISABLED;

    if( GetPDSMonEnabled() == false )
        m_pdsPipeState = PDS_DISABLED;
    else if( ( baseCommCfg.portType == CT_UNUSED ) || ( m_devType == DT_NULL ) )
        m_pdsPipeState = PDS_NO_HARDWARE;
    else if( m_device->GetPDSClosed() )
        m_pdsPipeState = PDS_CLOSED;
    else
        m_pdsPipeState = PDS_OPEN;

    m_currUnits = GetDefaultUnitsOfMeasure();
    m_newUnits  = GetDefaultUnitsOfMeasure();

    GetCDSSlipsSettings( m_currUnits, m_cdsReleased, m_cdsSlipLocked, m_cdsLockSlipRel );

    // Interlock release time is given in seconds. Covert to msecs. Add some
    // margin to the number to ensure we get enough packets when in RFC mode.
    m_releaseDelay = GetInterlockReleaseTime() * 1000 + INTERLOCK_RELEASE_MARGIN;

    // Set the initial state of the output switches
    if( m_interlockState == IS_BOTH_HOLDING )
        m_outputSwitchState = 0;
    else if( m_interlockState == IS_DISABLED )
        m_outputSwitchState = ( CDS_INTERLOCK_OUTPUT_SWITCH_BIT | SLIPS_INTERLOCK_OUTPUT_SWITCH_BIT );

    m_refreshSettings   = true;
    m_nextSwitchCmdTime = 0;

    // Last raw packet init
    memset( &m_lastRawPkt, 0, sizeof( TSubDevice::DEVICE_RAW_READING ) );

    m_lastRawPktmsecs   = 0;
    m_lastRawPktSeqNbr  = 0;
    m_bHaveRawPkt       = false;
}


void TCommPoller::CreateDevices( void )
{
    // If we are auto-assigning ports, then real objects are required.
    // Otherwise, construct the object type based on the registry setting.
    if( GetAutoAssignCommPorts() )
    {
        m_mgmtPort = new TRealMgmtPort();
        m_radio    = new TRealBaseRadio();
        m_device   = new TWTTTSSub();
        m_devType  = DT_WTTTS;
    }
    else
    {
        // Create devices based on the registry setting
        COMMS_CFG mgmtCommCfg;
        GetCommSettings( DCT_MGMT, mgmtCommCfg );

        COMMS_CFG baseCommCfg;
        GetCommSettings( DCT_BASE, baseCommCfg );

        COMMS_CFG wtttsCommCfg;
        GetCommSettings( DCT_WTTTS, wtttsCommCfg );

        if( mgmtCommCfg.portType == CT_UNUSED )
            m_mgmtPort = new TNullMgmtPort();
        else
            m_mgmtPort = new TRealMgmtPort();

        if( baseCommCfg.portType == CT_UNUSED )
            m_radio = new TNullBaseRadio();
        else
            m_radio = new TRealBaseRadio();

        // For the TesTORK device, we will never instantiate a legacy device.
        if( wtttsCommCfg.portType == CT_UNUSED )
        {
            m_devType = DT_NULL;
            m_device  = new TWTTTSNullDevice();
        }
        else
        {
            m_devType = DT_WTTTS;
            m_device  = new TWTTTSSub();
        }
    }

    // Hook event handlers
    m_device->OnRFCRcvd    = DoOnRFCReceived;
    m_device->OnStreamRcvd = DoOnStreamReceived;

    // WITS Device isn't part of Auto-Assigned, make it now
    COMMS_CFG commsCfg;
    GetWitsPortsSettings( commsCfg );
    m_witsPort.portType = commsCfg.portType;

    // Attempt to create the port
    CreateCommPort( commsCfg, m_witsPort );
}


void TCommPoller::DeleteDevices( void )
{
    // Delete the radio if it exists
    if( m_radio != NULL )
    {
        delete m_radio;
        m_radio = NULL;
    }

    // Delete the device if it exists
    if( m_device != NULL )
    {
        delete m_device;
        m_device = NULL;
    }

    // Delete the Mgmt Port if it exists
    if( m_mgmtPort != NULL )
    {
        delete m_mgmtPort;
        m_mgmtPort = NULL;
    }

    // Delete Wits Port
    ReleaseCommPort( m_witsPort );
}


void TCommPoller::ClearWitsSendFields( WITS_SEND_FIELDS& sendFieldStruct )
{
    if( sendFieldStruct.pFieldSettings != NULL )
    {
        delete [] sendFieldStruct.pFieldSettings;
        sendFieldStruct.pFieldSettings = NULL;
    }

    if( sendFieldStruct.pWitsFields != NULL )
    {
        delete [] sendFieldStruct.pWitsFields;
        sendFieldStruct.pWitsFields= NULL;
    }

    sendFieldStruct.iNumFields = 0;
}


void TCommPoller::GetWitsSendFields( WITS_SEND_FIELDS& sendFieldStruct, TSubDevice::WITS_PACKET_TYPE ePktType )
{
    // Get all fields defined in the registry, but keep only those that are enabled
    int iNumFields = GetWitsFieldSettings( ePktType, NULL, 0 );

    WITS_FIELD_SETTING* pAllFields = new WITS_FIELD_SETTING[iNumFields];
    GetWitsFieldSettings( ePktType, pAllFields, iNumFields );

    // Allocate storage for the field settings. We may end up using fewer
    // slots than this if all fields are not enabled, but that's ok.
    sendFieldStruct.pFieldSettings = new WITS_FIELD_SETTING[iNumFields];
    sendFieldStruct.pWitsFields    = new WITS_FIELD[iNumFields];

    // Add all enabled fields
    int iEnabledFields = 0;

    for( int iCurrField = 0; iCurrField < iNumFields; iCurrField++ )
    {
        if( pAllFields[iCurrField].bEnabled )
        {
            sendFieldStruct.pFieldSettings[iEnabledFields] = pAllFields[iCurrField];
            iEnabledFields++;
        }
    }

    // Save enabled field count
    sendFieldStruct.iNumFields = iEnabledFields;

    delete [] pAllFields;
}


// Refresh the state of the interlock
void TCommPoller::RefreshInterlockState( void )
{
    if( GetInterlockEn() && ( m_bTempZWIBypass == eBIS_NoBypass ) )
    {
        // Interlock is to be enabled. Switch processing
        // if we were previously disabled.
        if( m_interlockState == IS_DISABLED )
        {
            // We were disabled and are now being enabled.
            // Open both output switches to cause both
            // interlocks to hold.
            m_interlockState = IS_BOTH_HOLDING;

            m_outputSwitchState &= ~( CDS_INTERLOCK_OUTPUT_SWITCH_BIT | SLIPS_INTERLOCK_OUTPUT_SWITCH_BIT );
            m_nextSwitchCmdTime  = 0;
        }
    }
    else
    {
        // Interlock function disabled. Turn on the outputs
        // which will allow the operator to control CDS and
        // slips releases manually.
        m_interlockState = IS_DISABLED;

        m_outputSwitchState |= ( CDS_INTERLOCK_OUTPUT_SWITCH_BIT | SLIPS_INTERLOCK_OUTPUT_SWITCH_BIT );
        m_nextSwitchCmdTime  = 0;
    }
}


void TCommPoller::SendWITSPkt( WITS_SEND_FIELDS& sendFieldStruct )
{
    if( ( m_witsPort.portObj == NULL ) || !m_witsPort.portObj->isConnected )
        return;

    if( sendFieldStruct.iNumFields <= 0 )
        return;

    // Have fields to send. Build the list of fields and their values
    BYTE witsTxBuffer[WITS_MAX_BUFF_LEN];

    // Loop through the list of Fields and populate it
    for( int iField = 0; iField < sendFieldStruct.iNumFields; iField++ )
    {
        AnsiString sCode  = FormatWITSCode( sendFieldStruct.pFieldSettings[iField].iCode );
        AnsiString sValue = m_device->GetWitsFieldValue( sendFieldStruct.pFieldSettings[iField].iFieldEnum );

        // Create the WITS_FIELD
        // <TODO> Check if the field was create successfully
        SetWITSField( sendFieldStruct.pWitsFields[iField], sCode, sValue );
    }

    DWORD buffLen = CreateWITSPacket( WITS_MAX_BUFF_LEN, witsTxBuffer, sendFieldStruct.pWitsFields, sendFieldStruct.iNumFields );

    if( m_witsPort.portObj->CommSend( witsTxBuffer, buffLen ) != buffLen )
    {
        // TOOD � log an error message
    }

    // Just in case, flush any packets received on this port
    buffLen = 1;

    while( buffLen > 0 )
        buffLen = m_witsPort.portObj->CommRecv( witsTxBuffer, WITS_MAX_BUFF_LEN );
}


// Event called from the SubDevice - Used for WITS RFC packets
void __fastcall TCommPoller::DoOnRFCReceived( TObject* Sender )
{
    SendWITSPkt( m_WitsRfcFields );
}


// Event called from the SubDevice - Used for WITS Stream packets
void __fastcall TCommPoller::DoOnStreamReceived( TObject* Sender )
{
    SendWITSPkt( m_WitsStreamFields );
}


bool TCommPoller::CommsGetStats( COMM_STATS& commStats )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_device != NULL )
    {
        m_device->GetCommStats( commStats );
        return true;
    }

    return false;
}


void TCommPoller::SetPollerState( POLLER_STATE epsNew, CONNECT_RESULT eCRMsgEnum )
{
    // Check to see if we should signal a lost comms event
    if( m_bSignalLostComms && ( m_pollerState == PS_RUNNING ) && ( epsNew != PS_RUNNING ) )
        ::PostMessage( m_hwndEvent, WM_POLLER_EVENT, ePE_CommsLost, 0 );
    else if( ( m_pollerState != PS_RUNNING ) && ( epsNew == PS_RUNNING ) )
        ::PostMessage( m_hwndEvent, WM_POLLER_EVENT, ePE_CommsUp, 0 );

    m_pollerState   = epsNew;
    m_connectResult = ConnResultStrings[eCRMsgEnum];
}


void TCommPoller::CommsClearStats( void )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_device != NULL )
        m_device->ClearCommStats();
}


bool TCommPoller::GetRadioStats( PORT_STATS& radioStats )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_radio != NULL )
        return m_radio->GetCommStats( radioStats );

    return false;
}


void TCommPoller::ClearRadioStats( void )
{
    // Stats operations are thread-safe. Can do these in the context
    // of the caller's thread.
    if( m_radio != NULL )
        m_radio->ClearCommStats();
}


bool TCommPoller::MgmtGetStats( PORT_STATS& mgmtStats )
{
    // We only report the connection type for the management port
    if( m_mgmtPort == NULL )
        return false;

    if( !m_mgmtPort->GetCommStats( mgmtStats ) )
        return false;

    return true;
}


void TCommPoller::ClearMgmtStats( void )
{
    // Currently this does nothing, as we don't track any data traffic
    // on the management port
}


bool TCommPoller::StartDataCollection( WORD streamInterval /*msecs*/ )
{
    if( m_device == NULL )
        return false;

    if( !m_device->IsConnected )
        return false;

    if( !m_device->CanStartDataCollection )
        return false;

    // Pass the indication down to the device handler. We can do this
    // from the context of any thread because this call does not need
    // to be thread-safe.
    bool bSuccess = m_device->StartDataCollection( streamInterval );

    m_streamStartTime = GetTickCount();

    // Flush the packet queue, and also flag the thread to do the same
    // (in case it is just about to put an incompatible packet into
    // the queue)
    m_flushPending = true;
    FlushPendingPktQueue();

    return bSuccess;
}


bool TCommPoller::StopDataCollection( void )
{
    if( m_device == NULL )
        return false;

    if( !m_device->IsConnected )
        return false;

    m_device->StopDataCollection();

    return true;
}


bool TCommPoller::StartCirculatingMode( void )
{
    if( m_device == NULL )
        return false;

    if( !m_device->IsConnected )
        return false;

    bool bSuccess = m_device->StartCirculatingMode( 0 /*not used*/ );

    // Flush the packet queue, and also flag the thread to do the same
    // (in case it is just about to put an incompatible packet into
    // the queue)
    m_flushPending = true;
    FlushPendingPktQueue();

    return bSuccess;
}


bool TCommPoller::StopCirculatingMode( void )
{
    if( m_device == NULL )
        return false;

    if( !m_device->IsConnected )
        return false;

    return m_device->StopCirculatingMode();
}


bool TCommPoller::GetNextSample( WTTTS_READING& nextSample, int& rpm )
{
    // Return true if another sample is ready
    if( m_device == NULL )
        return false;

    if( !m_device->IsConnected )
        return false;

    return GetPktFromQueue( nextSample, rpm );
}


// Temporarily disable ZWI for the given duration
void TCommPoller::TempDisableZWI( DWORD dwDuration /*msecs*/ )
{
    // Setup the bypass variables. Set the bypass state last
    // as the worker thread uses that to determine what to do
    m_dwTempBypassTime    = GetTickCount();
    m_dwTempBypassTimeout = dwDuration;
    m_bTempZWIBypass      = eBIS_Starting;
}


String TCommPoller::GetDevTypeText( void )
{
    // No need to access thread lock for this function
    return ::DeviceTypeText[m_devType];
}


String TCommPoller::GetDevStatusText( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return "";

    // If we aren't connected, overwrite the dev status with comm status
    if( PollerState != PS_RUNNING )
        return m_connectResult;

    return m_device->DeviceStatus;
}


String TCommPoller::GetDevHousingSN( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return "";

    return m_device->DeviceHousingSN;
}


bool TCommPoller::GetDevIsConn( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->IsConnected;
}


bool TCommPoller::GetDevIsIdle( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    // Rely on the 'can start data collection' property for this
    if( !m_device->IsConnected )
        return false;

    return m_device->DeviceIsIdle;
}


bool TCommPoller::GetDevIsStreaming( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    // Rely on the 'can start data collection' property for this
    if( !m_device->IsConnected )
        return false;

    return m_device->DeviceIsStreaming;
}


bool TCommPoller::GetDevIsCalibrated( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->DeviceIsCalibrated;
}


DWORD TCommPoller::GetStreamingTime( void )
{
    if( !DeviceIsStreaming )
        return 0;

    // Timestamp is in msecs - report back time in seconds
    return( TimeSince( m_streamStartTime ) / 1000 );
}


bool TCommPoller::GetDevIsRecving( void )
{
    if( m_device == NULL )
        return false;

    return m_device->DeviceIsRecving;
}


TSubDevice::LINK_STATE TCommPoller::GetDevLinkState( void )
{
    if( m_device == NULL )
        return TSubDevice::LS_CLOSED;

    return m_device->DeviceLinkState;
}


bool TCommPoller::GetRadioIsConn( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->IsConnected;
}


bool TCommPoller::GetUsingTesTORKSim( void )
{
    COMMS_CFG wtttsCfg;
    GetCommSettings( DCT_WTTTS, wtttsCfg );

    if( wtttsCfg.portType == CT_UDP )
        return true;
    else
        return false;
}


bool __fastcall TCommPoller::GetPortLost( void )
{
    // No need to access thread lock for this function
    if( m_device == NULL )
        return false;

    return m_device->PortLost;
}


int __fastcall TCommPoller::GetCfgUploadProgress( void )
{
    // No need to access thread lock for this function
    if( ( m_device == NULL ) || !m_device->IsConnected )
        return 0;

    return m_device->ConfigUploadProgress;
}


int __fastcall TCommPoller::GetTesTORKRSSI( void )
{
    if( ( m_device == NULL ) || !m_device->IsConnected )
        return 0;

    // For now, we assume the TesTORK is always connected
    // to radio port 0, so return its RSSI
    if( m_radio == NULL )
        return 0;

    DWORD dwRSSI = m_radio->RSSI[eBRN_1];

    // Map the value of 0 to 255 to percentage
    return 100 * (int)dwRSSI / 255;
}


bool TCommPoller::GetBaseIsConn( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->IsConnected;
}


bool TCommPoller::GetBaseIsRecving( void )
{
    if( m_radio == NULL )
        return false;

    return m_radio->IsReceiving;
}


bool TCommPoller::GetAlarmOn( void )
{
    if( m_radio == NULL )
        return false;

    if( m_radio->GetOutputState() & ALARM_OUTPUT3_SWITCH_BIT )
        return true;
    else
        return false;
}


void TCommPoller::GetInterlockState( bool& bSlipsLocked, bool& bCDSLocked )
{
    // Init return vars first
    bSlipsLocked = false;
    bCDSLocked   = false;

    // States are only valid if the state machine is active
    if( ( m_interlockState == IS_NO_HARDWARE ) || ( m_interlockState == IS_DISABLED ) )
        return;

    // Make sure the radio device exists
    if( m_radio == NULL )
        return;

    // Get 'real' condition of outputs
    BYTE byOutputState = m_radio->GetOutputState();

    if( byOutputState & SLIPS_INTERLOCK_OUTPUT_SWITCH_BIT )
        bSlipsLocked = false;
    else
        bSlipsLocked = true;

    if( byOutputState & CDS_INTERLOCK_OUTPUT_SWITCH_BIT )
        bCDSLocked = false;
    else
        bCDSLocked = true;
}


bool TCommPoller::SetAlarmOutput( DWORD dwDurationInMsecs )
{
    // Sets the state of the alarm output. A duration of zero clears
    // any pending alarm. Any other duration will add to any current
    // alarm duration. A duration of 0xFFFFFFFF forces the alarm to
    // remain on until a value of 0 is written some time later.

    // Set the BoolCmd var after updating the requested alarm duration
    // The worker thread will clear the command when it has started
    // the alarm.
    EnterCriticalSection( &m_criticalSection );

    bool bHaveOverrun = ( m_alarmParams.bcCmd != eBC_NoCmd );

    m_alarmParams.dwRequestedDur = dwDurationInMsecs;

    if( dwDurationInMsecs == 0 )
        m_alarmParams.bcCmd = eBC_TurnOff;
    else
        m_alarmParams.bcCmd = eBC_TurnOn;

    LeaveCriticalSection( &m_criticalSection );

    return bHaveOverrun;
}


bool TCommPoller::GetLastStatusPktInfo( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->ShowLastStatusPkt( pCaptions, pValues );
}


bool TCommPoller::GetLastStatusPkt( time_t& pktTime, TSubDevice::DEVICE_RAW_READING& lastPkt )
{
    if( m_device == NULL )
       return false;

    return m_device->GetLastStatusPkt( pktTime, lastPkt );
}


bool TCommPoller::GetLastDataRaw( DWORD& msecs, BYTE& seqNbr, TSubDevice::DEVICE_RAW_READING& lastPkt )
{
    if( m_device == NULL )
       return false;

    bool bHavePkt = false;

    EnterCriticalSection ( &m_criticalSection );

    if( m_bHaveRawPkt )
    {
        lastPkt = m_lastRawPkt;
        msecs   = m_lastRawPktmsecs;
        seqNbr  = m_lastRawPktSeqNbr;

        m_bHaveRawPkt = false;
        bHavePkt      = true;
    }

    LeaveCriticalSection ( &m_criticalSection );

    // No packet available
    return bHavePkt;
}


bool TCommPoller::GetLastDataCal( DWORD& msecs, BYTE& seqNbr, TSubDevice::DEVICE_CAL_READING& lastPkt )
{
    if( m_device == NULL )
       return false;

    return m_device->GetLastDataCal( msecs, seqNbr, lastPkt );
}


bool TCommPoller::GetLastAvgData( float& avgTorque, float& avgTension, int& avgRPM )
{
    // The last average values as reported by the device. Can come
    // from idle packets or stream packets, depending on the device
    // type and the state the device is in
    if( m_device == NULL )
       return false;

    return m_device->GetLastAvgData( avgTorque, avgTension, avgRPM );
}


bool TCommPoller::GetTempCompInfo( TSubDevice::TEMP_COMP_VALUES& tcValues )
{
    // Reports the key params from the TesTORK RTD avg'ing algorithm
    if( m_device == NULL )
       return false;

    m_device->GetLastTempCompValues( tcValues );

    return true;
}


bool TCommPoller::GetLastRadioStatusPktInfo( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_radio == NULL )
       return false;

    return m_radio->ShowLastStatusPkt( pCaptions, pValues );
}


int TCommPoller::GetRadioChannel( eBaseRadioNbr radioNbr )
{
    if( ( m_radio == NULL ) || ( !m_radio->IsReceiving ) )
        return 0;

    time_t tLastPkt;
    BASE_STATUS_STATUS_PKT statusPkt;

    if( !m_radio->GetLastStatusPkt( tLastPkt, statusPkt ) )
        return 0;

    if( ( radioNbr >= 0 ) && ( radioNbr < eBRN_NbrBaseRadios ) )
        return statusPkt.radioInfo[radioNbr].chanNbr;

    // Fall through means invalid radio number passed
    return 0;
}


TBaseRadioDevice::SET_RADIO_CHAN_RESULT TCommPoller::SetRadioChannel( eBaseRadioNbr radioNbr, int newChanNbr )
{
    if( m_radio == NULL )
       return TBaseRadioDevice::SRCR_NO_RADIO;

     return m_radio->SetRadioChannel( radioNbr, newChanNbr );
}


void TCommPoller::RefreshSettings( int unitsOfMeasure )
{
    // Record that a refresh has been requested. The actual refreshing
    // of data will happen in the worker thread.
    m_newUnits        = unitsOfMeasure;
    m_refreshSettings = true;
}


bool TCommPoller::GetCalDataRaw( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->GetCalDataRaw( pCaptions, pValues );
}


bool TCommPoller::GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( m_device == NULL )
       return false;

    return m_device->GetCalDataFormatted( pCaptions, pValues );
}


bool TCommPoller::SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    if( m_device == NULL )
       return false;

    return m_device->SetCalDataFormatted( pCaptions, pValues );
}


bool TCommPoller::ReloadCalDataFromDevice( void )
{
    if( m_device == NULL )
       return false;

    return m_device->ReloadCalDataFromDevice();
}


bool TCommPoller::WriteCalDataToDevice( void )
{
    if( m_device == NULL )
       return false;

    return m_device->WriteCalDataToDevice();
}


bool TCommPoller::SetDeviceHousingSN( const String& sNewHousingSN )
{
    // Causes the string passed to be written into the device's
    // manufacturing area.
    if( m_device == NULL )
       return false;

    return m_device->WriteHousingSN( sNewHousingSN );
}


bool TCommPoller::GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo )
{
    if( m_device == NULL )
       return false;

    return m_device->GetDeviceHWInfo( devHWInfo );
}


bool TCommPoller::GetDeviceBatteryInfo( BATTERY_INFO& battInfo )
{
    if( m_device == NULL )
       return false;

    return m_device->GetDeviceBatteryInfo( battInfo );
}


//
// Queue Handling Functions
//

static int AdvanceIndex( int currIndex )
{
    currIndex++;

    if( currIndex >= MAX_NBR_READING_PKTS )
        currIndex = 0;

    return currIndex;
}


void TCommPoller::FlushPendingPktQueue( void )
{
    // Flushes all pending packets from the queue. This is a thread-safe function.
    EnterCriticalSection( &m_criticalSection );

    try
    {
        m_readingsQueue.pktReadIndex  = 0;
        m_readingsQueue.pktWriteIndex = 0;
    }
    __finally
    {
        LeaveCriticalSection ( &m_criticalSection );
    }
}


void TCommPoller::AddPktToPendingQueue( const WTTTS_READING& nextSample, int rpm )
{
    // Adds a packet received by the serial port into the pending queue. Will
    // overwrite an old packet's data if the queue is full.
    EnterCriticalSection ( &m_criticalSection );

    // Make sure we have room for this packet. If not, toss the
    // oldest packet.
    int newWriteIndex = AdvanceIndex( m_readingsQueue.pktWriteIndex );

    if( newWriteIndex == m_readingsQueue.pktReadIndex )
        m_readingsQueue.pktReadIndex = AdvanceIndex( m_readingsQueue.pktReadIndex );

    // Store the sample
    m_readingsQueue.pktQueue[m_readingsQueue.pktWriteIndex].ttReading = nextSample;
    m_readingsQueue.pktQueue[m_readingsQueue.pktWriteIndex].rpm       = rpm;

    // Okay to advance the write index now
    m_readingsQueue.pktWriteIndex = newWriteIndex;

    LeaveCriticalSection ( &m_criticalSection );
}


bool TCommPoller::GetPktFromQueue( WTTTS_READING& nextSample, int& rpm )
{
    // Gets the next packet from the queue. Returns true if a packet was
    // pending (and copied), false otherwise. This function is thread-safe.
    bool havePkt = false;

    EnterCriticalSection ( &m_criticalSection );

    // Check if we have packets to report
    if( m_readingsQueue.pktReadIndex != m_readingsQueue.pktWriteIndex )
    {
        nextSample = m_readingsQueue.pktQueue[m_readingsQueue.pktReadIndex].ttReading;
        rpm        = m_readingsQueue.pktQueue[m_readingsQueue.pktReadIndex].rpm;

        havePkt = true;

        // Advance our read index now
        m_readingsQueue.pktReadIndex = AdvanceIndex( m_readingsQueue.pktReadIndex );
    }

    LeaveCriticalSection ( &m_criticalSection );

    return havePkt;
}


TSubDevice::BatteryLevelType TCommPoller::GetBattLevel( void )
{
    if( m_device == NULL )
        return TSubDevice::eBL_Unknown;

    return m_device->BatteryLevel;
}

