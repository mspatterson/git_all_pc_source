#ifndef CommsMgrH
#define CommsMgrH

// Later, should move system-wide defs to their own .h file.
#include "WTTTSDefs.h"
#include "SubDeviceBaseClass.h"
#include "BaseRadioDevice.h"
#include "MgmtPortDevice.h"
#include "WITS.h"

class TCommPoller : public TThread
{
  public:

    typedef enum {
        PS_INITIALIZING,       // Thread is starting
        PS_BR_PORT_SCAN,       // Looking for base radio comm port
        PS_TT_PORT_SCAN,       // Looking for TT comm port
        PS_TT_CHAN_SCAN,       // Looking for TT radio channel
        PS_RUNNING,            // Ports open and have TT
        PS_COMMS_FAILED,       // Comm error, see ConnectResult for more info
        PS_EXCEPTION,          // Thread loop failed due to exception being thrown
        NBR_POLLER_ERRORS
    } POLLER_STATE;

    typedef enum {
        IS_NO_HARDWARE,        // No base radio or WTTS/WTTTS device attached to system
        IS_DISABLED,           // Interlock function disabled by user
        IS_BOTH_HOLDING,       // Both interlocks are engaged
        IS_CDS_ENABLING,       // Criteria for CDS enable met, timer started
        IS_CDS_ENABLED,        // CDS can now be opened / closed
        IS_SLIPS_RELEASING,    // Criteria for Slips release met, timer started
        IS_SLIPS_RELEASED,     // Slips have been released
        NBR_INTERLOCK_STATES
    } INTERLOCK_STATE;

    typedef enum {
        PDS_DISABLED,
        PDS_NO_HARDWARE,
        PDS_OPEN,
        PDS_CLOSED
    } PDS_STATE;

    typedef enum {
       ePE_CommsUp,           // Posted when comms with TesTORK established
       ePE_CommsLost,         // Posted as the WParam to the event handler if comms are lost
       ePE_AlarmChange,       // Posted as the WParam to the event handler if there's a change in the alarm out state
       ePE_NbrPollerEvents
    } ePollerEvent;


  private:

    typedef enum {
        eCR_Initializing,
        eCR_BadBaseRadioPort,
        eCR_BadMPLPort,
        eCR_BadMgmtPort,
        eCR_Connected,
        eCR_HuntingForBase,
        eCR_HuntingForTT,
        eCR_WinsockError,
        eCR_ThreadException,
        eCR_NumConnectResult
    } CONNECT_RESULT;

    static const String ConnResultStrings[eCR_NumConnectResult];

    typedef enum {
        eUnknown,
        eRadio0,
        eRadio1,
        eBase,
        eMgmt
    } PORT_TYPE;

    typedef enum {
        eBIS_NoBypass,
        eBIS_Starting,
        eBIS_Bypassed,
        eBIS_NumStates
    } BYPASS_INTERLOCK_STATE;

    TSubDevice*       m_device;
    TBaseRadioDevice* m_radio;
    TMgmtPortDevice*  m_mgmtPort;
    DEVICE_TYPE       m_devType;
    DEVICE_PORT       m_witsPort;

    WIRELESS_SYSTEM_INFO m_wirelessInfo;

    DWORD             m_dwRadioChanWaitTime;

    POLLER_STATE      m_pollerState;
    String            m_connectResult;

    bool              m_flushPending;

    // Event notification vars
    HWND              m_hwndEvent;
    bool              m_bSignalLostComms;

    bool              m_bHaveRawPkt;
    DWORD             m_lastRawPktmsecs;
    BYTE              m_lastRawPktSeqNbr;
    TSubDevice::DEVICE_RAW_READING m_lastRawPkt;

    // Bypass variables
    DWORD             m_dwTempBypassTime;
    DWORD             m_dwTempBypassTimeout;
    BYPASS_INTERLOCK_STATE m_bTempZWIBypass;

    // Alarm variables
    struct {
        BoolCmd bcCmd;            // Set by main thread to request an alarm
        DWORD   dwRequestedDur;   // Duration requested by main thread
        DWORD   dwStartTime;      // Set by worker thread at time alarm turned on
        DWORD   dwAlarmDur;       // Set by worker thread to indication alarm duration
    } m_alarmParams;

    // WITS-specific vars
    typedef struct {
        int  iNumFields;
        WITS_FIELD_SETTING* pFieldSettings;
        WITS_FIELD*         pWitsFields;
    } WITS_SEND_FIELDS;

    WITS_SEND_FIELDS  m_WitsRfcFields;
    WITS_SEND_FIELDS  m_WitsStreamFields;

    bool IsThreadRunning( void ) { return( m_device != NULL ); }

    void SetPollerState( POLLER_STATE epsNew, CONNECT_RESULT eCRMsgEnum );

    bool OpenAssignedPorts( void );
    bool OpenAutomaticPorts( void );

    bool FindDevPorts( const COMMS_CFG& radioCommCfg, COMMS_CFG& deviceCommCfg, COMMS_CFG& mgmtCommCfg );
    PORT_TYPE GetPortType( CCommPort* pPort );
    bool WigglePort( CCommPort* pPort, BYTE& bInitCtrlStatus, BYTE& bHighCtrlStatus, BYTE& bLastCtrlStatus );

    void AfterCommConnect( COMMS_CFG radioCommCfg );

    void CreateDevices( void );
    void DeleteDevices( void );

    void ClearWitsSendFields( WITS_SEND_FIELDS& sendFieldStruct );
    void GetWitsSendFields  ( WITS_SEND_FIELDS& sendFieldStruct, TSubDevice::WITS_PACKET_TYPE ePktType );

    void SendWITSPkt( WITS_SEND_FIELDS& sendFieldStruct );

    void __fastcall DoOnRFCReceived( TObject* Sender );
    void __fastcall DoOnStreamReceived( TObject* Sender );

    void RefreshInterlockState( void );

  protected:

    void __fastcall Execute();
        // Worker thread function

    void FlushPendingPktQueue( void );
        // Flushes all pending packets from the queue.

    void AddPktToPendingQueue( const WTTTS_READING& nextSample, int rpm );
        // Adds a packet received by the serial port into the pending queue. Will
        // overwrite an old packet's data if the queue is full.

    bool GetPktFromQueue( WTTTS_READING& nextSample, int& rpm );
        // Gets the next packet from the queue. Returns true if a packet was
        // returned.

    // The max number of packet objects should be such that data won't be lost
    // during the longest possible delay in the calling process
    #define MAX_NBR_READING_PKTS    100

    typedef struct {
        WTTTS_READING ttReading;
        int           rpm;
    } DEVICE_READING;

    struct {
        DEVICE_READING pktQueue[MAX_NBR_READING_PKTS];
        int            pktReadIndex;
        int            pktWriteIndex;
    } m_readingsQueue;

    // Interlock management
    INTERLOCK_STATE m_interlockState;
    int             m_cdsReleased;
    int             m_cdsSlipLocked;
    int             m_cdsLockSlipRel;
    DWORD           m_releaseDelay;
    DWORD           m_disengageTime;
    PDS_STATE       m_pdsPipeState;
    int             m_currUnits;
    int             m_newUnits;

    // Streaming management
    DWORD           m_streamStartTime;    // in msecs

    // Base radio output switch management
    BYTE            m_outputSwitchState;
    time_t          m_nextSwitchCmdTime;

    // The following flag is set by the main thread if this thread should
    // reload settings from the registry. Note that comms settings are
    // not reloaded by this process
    bool            m_refreshSettings;

    // A critical section controls the access of shared objects between threads
    CRITICAL_SECTION m_criticalSection;

    POLLER_STATE    GetPollerState( void ) { return m_pollerState; }
    String          GetDevTypeText( void );
    String          GetDevStatusText( void );
    String          GetDevHousingSN( void );
    bool            GetDevIsConn( void );
    bool            GetDevIsIdle( void );
    bool            GetDevIsStreaming( void );
    bool            GetDevIsCalibrated( void );
    DWORD           GetStreamingTime( void );
    bool            GetDevIsRecving( void );
    bool            GetRadioIsConn( void );
    bool            GetUsingTesTORKSim( void );

    TSubDevice::LINK_STATE GetDevLinkState( void );

    bool            GetBaseIsConn( void );
    bool            GetBaseIsRecving( void );

    bool            GetAlarmOn( void );

    bool __fastcall GetPortLost( void );
    int  __fastcall GetCfgUploadProgress( void );

    int  __fastcall GetTesTORKRSSI( void );

    TSubDevice::BatteryLevelType GetBattLevel( void );

  public:

    __fastcall TCommPoller( HWND hwndEvent );
    virtual __fastcall ~TCommPoller();

    bool StartDataCollection( WORD streamInterval /*msecs*/ );
    bool StopDataCollection( void );

    bool StartCirculatingMode( void );
    bool StopCirculatingMode( void );

    bool GetNextSample( WTTTS_READING& nextSample, int& rpm );

    void TempDisableZWI( DWORD dwDuration /*msecs*/ );

    __property bool          ThreadRunning  = { read = IsThreadRunning };
    __property String        ConnectResult  = { read = m_connectResult };
    __property POLLER_STATE  PollerState    = { read = GetPollerState };

    __property String        DeviceTypeText  = { read = GetDevTypeText };
    __property DEVICE_TYPE   DeviceType      = { read = m_devType };
    __property String        DeviceHousingSN = { read = GetDevHousingSN };

    __property String        DeviceStatus       = { read = GetDevStatusText };
    __property bool          DeviceIsConnected  = { read = GetDevIsConn };
    __property bool          DeviceIsIdle       = { read = GetDevIsIdle };
    __property bool          DeviceIsStreaming  = { read = GetDevIsStreaming };
    __property bool          DeviceIsCalibrated = { read = GetDevIsCalibrated };
    __property bool          DeviceIsRecving    = { read = GetDevIsRecving };

    __property TSubDevice::LINK_STATE DeviceLinkState = { read = GetDevLinkState };

    __property DWORD         StreamingTime      = { read = GetStreamingTime };

    __property bool          UsingTesTORKSim    = { read = GetUsingTesTORKSim };

    __property TSubDevice::BatteryLevelType BatteryLevel = { read = GetBattLevel };


    //
    // TesTORK Specific Methods
    //

    __property bool       PortLost             = { read = GetPortLost };
    __property int        ConfigUploadProgress = { read = GetCfgUploadProgress };
    __property int        TesTORKRSSI          = { read = GetTesTORKRSSI };

    bool CommsGetStats( COMM_STATS& commStats );
    void CommsClearStats( void );

    bool GetLastStatusPktInfo( TStringList* pCaptions, TStringList* pValues );
    bool GetLastStatusPkt( time_t& pktTime, TSubDevice::DEVICE_RAW_READING& lastPkt );
        // Pass through function that pulls last status packet information
        // out of the underlying device object.

    bool GetLastDataRaw( DWORD& msecs, BYTE& seqNbr, TSubDevice::DEVICE_RAW_READING& lastPkt );
    bool GetLastDataCal( DWORD& msecs, BYTE& seqNbr, TSubDevice::DEVICE_CAL_READING& lastPkt );
        // Pass through function that pulls last packet data out of
        // the underlying device object.

    bool GetLastAvgData( float& avgTorque, float& avgTension, int& avgRPM );
        // The last average values as reported by the device. Can come
        // from idle packets or stream packets, depending on the device
        // type and the state the device is in

    bool GetTempCompInfo( TSubDevice::TEMP_COMP_VALUES& tcValues );
        // Reports the output values from the TesTORK temp comp algorithm

    bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues );
    bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
    bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
        // Pass through functions that get / set calibration data. Not
        // all devices support on-board calibration memory.

    bool ReloadCalDataFromDevice( void );
        // Causes the device to clear it internal cached cal data and
        // reload the data from the device.

    bool WriteCalDataToDevice( void );
        // Writes the calibration data currently cached in the software
        // to the WTTTS device.

    bool SetDeviceHousingSN( const String& sNewHousingSN );
        // Causes the string passed to be written into the device's
        // manufacturing area.

    bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo );
        // Returns information about the device hardware

    bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo );
        // Returns current battery information


    //
    // Base Radio Specific Methods
    //

    bool GetRadioStats( PORT_STATS& radioStats );
    void ClearRadioStats( void );

    bool GetLastRadioStatusPktInfo( TStringList* pCaptions, TStringList* pValues );
        // Pass through function that pulls last status packet information
        // out of the underlying device object.

    int GetRadioChannel( eBaseRadioNbr radioNbr );
    TBaseRadioDevice::SET_RADIO_CHAN_RESULT SetRadioChannel( eBaseRadioNbr radioNbr, int newChanNbr );
        // Gets / sets a radio to operate on a given channel

    void RefreshSettings( int unitsOfMeasure );
        // Informs the poller is must reload registry-based settings

    __property bool            BaseIsConnected = { read = GetBaseIsConn };
    __property bool            BaseIsRecving   = { read = GetBaseIsRecving };

    __property INTERLOCK_STATE InterlockState  = { read = m_interlockState };
    __property PDS_STATE       PDSPipeState    = { read = m_pdsPipeState };

    void GetInterlockState( bool& bSlipsLocked, bool& bCDSLocked );
        // Note: the InterlockState reflects the overall state of the interlock
        // system. Because comms are involved, there can be a delay from when
        // the state changes to when the hardware actually makes the change.
        // Therefore, use the method to get the actual condition of the
        // respective outputs

    bool SetAlarmOutput( DWORD dwDurationInMsecs );
        // Sets the state of the alarm output. A duration of zero clears
        // any pending alarm. Any other duration will add to any current
        // alarm duration. A duration of 0xFFFFFFFF forces the alarm to
        // remain on until a value of 0 is written some time later.

    __property bool            AlarmOn = { read = GetAlarmOn };
        // Returns the current state of the alarm output


    //
    // Management Port Specific Methods
    //

    bool MgmtGetStats( PORT_STATS& mgmtStats );
    void ClearMgmtStats( void );

};

#endif
