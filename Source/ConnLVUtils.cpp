#include <vcl.h>
#pragma hdrstop

#include "ConnLVUtils.h"
#include "JobManager.h"

#pragma package(smart_init)


//
// Private Declarations
//

// Sort order for connection results can't be done on the value of
// the connection result enum. Instead, define a custom order for
// each enum level.
const int connResultSortOrder[NBR_CONN_RESULTS] = { 0, 2, 1, 3, 4 };


//
// Class Functions
//

__fastcall TLVConnSorter::TLVConnSorter( TListView* pListView )
{
    m_pListView = pListView;

    // Hook the LV's on click and sort event handlers
    pListView->OnColumnClick = OnColumnClick;
    pListView->OnCompare     = OnCompare;

    // Set initial sort order to first column, ascending. Column type
    // is the tag of the column
    m_currSortCol = (LISTVIEW_COL)( m_pListView->Columns->Items[0]->Tag );
    m_isAscending = true;
}


void TLVConnSorter::SetAscending( bool isAscending )
{
    if( m_isAscending != isAscending )
    {
        m_isAscending = isAscending;
        m_pListView->CustomSort( NULL, 0 );
    }
}


void TLVConnSorter::SetSortCol( LISTVIEW_COL newCol )
{
    if( m_currSortCol != newCol )
    {
        m_currSortCol = newCol;
        m_pListView->CustomSort( NULL, 0 );
    }
}


void __fastcall TLVConnSorter::OnColumnClick(TObject *Sender, TListColumn *Column)
{
    LISTVIEW_COL newSortCol = (LISTVIEW_COL)( Column->Tag );

    if( m_currSortCol == newSortCol )
    {
        m_isAscending = !m_isAscending;
    }
    else
    {
        m_currSortCol = newSortCol;
        m_isAscending = true;
    }

    m_pListView->CustomSort( NULL, 0 );
}


void __fastcall TLVConnSorter::OnCompare(TObject *Sender, TListItem *Item1, TListItem *Item2, int Data, int &Compare)
{
    // Each list item's data member points to a LV_SORTER_REC struct
    LV_SORTER_REC* rec1 = (LV_SORTER_REC*)( Item1->Data );
    LV_SORTER_REC* rec2 = (LV_SORTER_REC*)( Item2->Data );

    if( ( rec1 != NULL ) && ( rec2 == NULL ) )
    {
        Compare = 1;
        return;
    }
    else if( ( rec1 == NULL ) && ( rec2 != NULL ) )
    {
        Compare = -1;
        return;
    }
    else if( ( rec1 == NULL ) && ( rec2 == NULL ) )
    {
        Compare = 0;
        return;
    }
    else if( ( rec1->iConnID == 0 ) && ( rec2->iConnID != 0 ) )
    {
        // If connID is zero, then this is an inventory record
        // Real connection recs always float above these
        Compare = 1;
        return;
    }
    else if( ( rec1->iConnID != 0 ) && ( rec2->iConnID == 0 ) )
    {
        // If connID is zero, then this is an inventory record
        // Real connection recs always float above these
        Compare = -1;
        return;
    }
    else if( ( rec1->iConnID == 0 ) && ( rec2->iConnID == 0 ) )
    {
        // If both recs are just inventory recs, then we always sort
        // by the (inventory) sequence number
        if( rec1->iSeqNbr > rec2->iSeqNbr )
            Compare = 1;
        else if( rec1->iSeqNbr < rec2->iSeqNbr )
            Compare = -1;
        else
            Compare = 0;

        return;
    }
    else
    {
        // Sort assuming ascending order for now. We'll switch the order
        // at the end of the function.
        switch( m_currSortCol )
        {
            case LVC_RESULT:

                if( connResultSortOrder[rec1->iConnResult] < connResultSortOrder[rec2->iConnResult] )
                {
                    Compare = -1;
                    break;
                }

                if( connResultSortOrder[rec1->iConnResult] > connResultSortOrder[rec2->iConnResult] )
                {
                    Compare = 1;
                    break;
                }

                // On equality, fall through to sect nbr

            case LVC_SECT_NBR:

                if( rec1->iSectNbr < rec2->iSectNbr )
                {
                    Compare = -1;
                    break;
                }

                if( rec1->iSectNbr > rec2->iSectNbr )
                {
                    Compare = 1;
                    break;
                }

                // On equality, fall through to conn nbr

            case LVC_CONN_NBR:

                if( rec1->iConnNbr < rec2->iConnNbr )
                {
                    Compare = -1;
                    break;
                }

                if( rec1->iConnNbr > rec2->iConnNbr )
                {
                    Compare = 1;
                    break;
                }

                // On equality, fall through to seq nbr

            case LVC_SEQ_NBR:

                if( rec1->iSeqNbr < rec2->iSeqNbr )
                {
                    Compare = -1;
                    break;
                }

                if( rec1->iSeqNbr > rec2->iSeqNbr )
                {
                    Compare = 1;
                    break;
                }

                Compare = 0;
                break;

            case LVC_DATE_TIME:

                if( rec1->dtConn.Val < rec2->dtConn.Val )
                {
                    Compare = -1;
                    break;
                }

                if( rec1->dtConn.Val > rec2->dtConn.Val )
                {
                    Compare = 1;
                    break;
                }

                Compare = 0;
                break;

            case LVC_LENGTH:

                if( rec1->fLength < rec2->fLength )
                {
                    Compare = -1;
                    break;
                }

                if( rec1->fLength > rec2->fLength )
                {
                    Compare = 1;
                    break;
                }

                Compare = 0;
                break;

            case LVC_COMMENT:
                if( rec1->sComment.CompareIC( rec2->sComment ) > 0 )
                    Compare = 1;
                else if( rec1->sComment.CompareIC( rec2->sComment ) < 0 )
                    Compare = -1;
                else
                    Compare = 0;
                break;

            default:
                // Should never happen
                Compare = 0;
        }
    }

    if( !m_isAscending )
        Compare = Compare * -1;
}


void TLVConnSorter::ClearListviewItems( void )
{
    while( m_pListView->Items->Count > 0 )
    {
        TListItem* topItem = m_pListView->Items->Item[0];

        LV_SORTER_REC* pRecInfo = (LV_SORTER_REC*)( topItem->Data );

        delete pRecInfo;

        m_pListView->Items->Delete( 0 );
    }
}

