#ifndef ConnLVUtilsH
#define ConnLVUtilsH

//-------------------------------------------------------------------------
//
//  Common utilities for listview components thst list connections
//
//-------------------------------------------------------------------------

// Columns supported by the sorter class
typedef enum {
   LVC_CONN_NBR,
   LVC_SEQ_NBR,
   LVC_SECT_NBR,
   LVC_DATE_TIME,
   LVC_RESULT,
   LVC_LENGTH,
   LVC_COMMENT,
   NBR_LISTVIEW_COLS
} LISTVIEW_COL;


// The 'data' property of listviews that use this class are expected to
// contain a pointer to this structure
typedef struct {
   int       iCreateNbr;
   int       iConnNbr;
   int       iSeqNbr;
   int       iSectNbr;
   TDateTime dtConn;
   int       iConnResult;
   float     fLength;
   String    sComment;
   int       iConnID;      // Not displayed by sorter, but used if connection is to be looked up
   int       iPipeInvID;   // Not displayed by sorter, but used if pipe inv rec is to be looked up
} LV_SORTER_REC;


class TLVConnSorter : public TObject
{
  private:
    TListView*   m_pListView;

    LISTVIEW_COL m_currSortCol;
    bool         m_isAscending;

    void SetAscending( bool isAscending );
    void SetSortCol( LISTVIEW_COL newCol );

    void __fastcall OnColumnClick(TObject *Sender, TListColumn *Column);
    void __fastcall OnCompare(TObject *Sender, TListItem *Item1, TListItem *Item2, int Data, int &Compare);

  protected:

  public:
    __fastcall TLVConnSorter( TListView* pListView );


    void ClearListviewItems( void );

    __property bool         Ascending  = { read = m_isAscending, write = SetAscending };
    __property LISTVIEW_COL SortColumn = { read = m_currSortCol, write = SetSortCol };

};


#endif
