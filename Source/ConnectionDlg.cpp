#include <vcl.h>
#pragma hdrstop

#include "ConnectionDlg.h"
#include "TAlarmDlg.h"
#include "Messages.h"
#include "ApplUtils.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TConnectionForm *ConnectionForm;


// Form heights in each mode
#define HIDDEN_HEIGHT 128
#define EXPAND_HEIGHT 375


__fastcall TConnectionForm::TConnectionForm(TComponent* Owner) : TForm(Owner)
{
    m_bShowFormCalled = false;
    m_bIsStartBtn     = false;
    m_bCanStartConn   = false;

    m_arParams.bAutoRecEn           = false;
    m_arParams.bAutoRecordedConn    = false;
    m_arParams.bRequireUserInput    = false;
    m_arParams.bCrossThreadDetected = false;
    m_arParams.iCrossThreadWindow   = (int)( GetConnMinTurnsValue() * 1000000.0 );
    m_arParams.dwStoppingTick       = 0;

    GetAutoRecSettings( m_arSettings );
}


void __fastcall TConnectionForm::CreateParams(Controls::TCreateParams &Params)
{
    // Add the WS_EX_APPWINDOW style to this form to cause its icon to be
    // be made present on the task bar
    TForm::CreateParams( Params );

    Params.ExStyle   = Params.ExStyle | WS_EX_APPWINDOW;
    Params.WndParent = ParentWindow;
}


void TConnectionForm::ShowForm( void )
{
    // Show this dialog modelessly.
    Show();

    // It is possible for this method to get called even if the form is
    // visible, so only do further processing if we weren't visible.
    if( !m_bShowFormCalled )
    {
        m_bShowFormCalled = true;

        // Force the user to write their own value
        m_bUserSetLength = false;

        if( ConnectionState == CDS_IDLE )
        {
            SetToStartBtn();
            HideSave();
        }
    }
}


void __fastcall TConnectionForm::FormShow(TObject *Sender)
{
    // Restore dialog position
    int top  = Top;
    int left = Left;
    int heightNotUsed = 0;
    int widthNotUsed  = 0;

    GetDialogPos( DT_CONN_DLG, top, left, heightNotUsed, widthNotUsed );

    // Only set the position if it's on screen. Avoid issues with dual monitors
    if( top < 0 )
        Top = 10;
    else if( top < Screen->DesktopHeight - Height )
        Top = top;
    else
        Top = Screen->DesktopHeight - Height;

    if( left < 0 )
        Left = 10;
    else if( left < Screen->DesktopWidth - Width )
        Left = left;
    else
        Left = Screen->DesktopWidth - Width;
}


void __fastcall TConnectionForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    // Can't close mid connection
    CanClose = ( ( ConnectionState == CDS_IDLE ) || ( ConnectionState == CDS_CONNS_PAUSED ) );

    if( !CanClose )
        MessageDlg( "You cannot close this form while receiving data.", mtError, TMsgDlgButtons() << mbOK, 0 );
}


void __fastcall TConnectionForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    m_bShowFormCalled = false;
}


void TConnectionForm::SetEventCallbacks( CONN_DLG_CALLBACKS dlgCallbacks )
{
    m_connDlgEvent  = dlgCallbacks.connDlgEvent;
    m_mainStatusReq = dlgCallbacks.mainStatusReq;
}


void __fastcall TConnectionForm::SetCurrJob( TJob* newJob )
{
    if( newJob == NULL )
    {
        m_currJob = NULL;

        // Set units to defaults in Sys Settings
        LengthLabel->Caption = L"Length (" + GetUnitsText( GetDefaultUnitsOfMeasure(), MT_DIST1_SHORT ) + L")";
    }
    else
    {
        m_currJob = newJob;

        // Set units to job UOM
        LengthLabel->Caption = L"Length (" + GetUnitsText( m_currJob->Units, MT_DIST1_SHORT ) + L")";
    }

    Caption = L"Connection Control";

    ConnectionState = CDS_IDLE;
}


void __fastcall TConnectionForm::WMExitMove( TMessage &Message )
{
    // Save form position, if visible
    if( Visible )
        SaveDialogPos( DT_CONN_DLG, Top, Left, Height, Width );
}


void TConnectionForm::PrepareForConnection( bool resetLengthEdit )
{
    // Clear previous results
    CommentMemo->Text = L"";
    ResultCombo->Items->Clear();

    // Set the length. If we have pipe inventory, then we set it to
    // that length. Otherwise, either reset or leave the length as
    // is based on the passed param
    if( m_currJob != NULL )
    {
        if( m_currJob->HavePipeInventory )
        {
            // User doesn't need to input length in this case
            m_bUserSetLength = true;
            LengthEdit->Text = FloatToStrF( m_currJob->NextPipeLength, ffFixed, 7, 2 );
        }
        else
        {
            // Only reset length if we're told to
            if( resetLengthEdit )
                LengthEdit->Text  = L"0.0";
        }
    }
    else
    {
        // Only reset length if we're told to
        if( resetLengthEdit )
            LengthEdit->Text  = L"0.0";
    }

    if( m_currJob == NULL )
    {
        ConnNbrEdit->Text = L"";
    }
    else
    {
        ConnNbrEdit->Text = IntToStr( m_currJob->GetNextConnectionNbr() );
        Caption           = L"Connection " + ConnNbrEdit->Text;
    }
}


void TConnectionForm::ForceStop( void )
{
    if( ConnectionState == CDS_CONNECTING )
    {
        StartStopConnBtnClick( NULL );

        // Make sure the dialog is visible as the user must intervene
        Show();
    }
}


void TConnectionForm::ShoulderSet( void )
{
    // This event is only valid while we are waiting for a connection result
    // to be entered
    if( ConnectionState == CDS_RESULT_WAIT )
    {
        // For now, we do nothing with this event.
    }
}


void TConnectionForm::SetResultOptions( ConnResultSet resultSet )
{
    // First, clear any current results
    ResultCombo->Items->Clear();

    if( !resultSet.Empty() )
    {
        // Add each element contained in the set to the combo
        for( int iConnResult = 0; iConnResult < NBR_CONN_RESULTS; iConnResult++ )
        {
            CONN_RESULT crResult = (CONN_RESULT)iConnResult;

            if( resultSet.Contains( crResult ) )
                ResultCombo->Items->Add( iConnResult );
        }
    }

    // If there are any items in the combo, select the first one
    if( ResultCombo->Items->Count > 0 )
        ResultCombo->ItemIndex = 0;
}


bool TConnectionForm::ShowConnection( const CONNECTION_INFO* pConnInfo )
{
    // Force the user to write their own value
    m_bUserSetLength = false;

    if( pConnInfo != NULL )
    {
        ConnNbrEdit->Text = IntToStr( pConnInfo->connNbr );
        LengthEdit->Text  = FloatToStrF( pConnInfo->length, ffFixed, 7, 2 );
        CommentMemo->Text = pConnInfo->comment;

        ResultCombo->Items->Clear();
        ResultCombo->Items->Add( pConnInfo->crResult );
        ResultCombo->ItemIndex = 0;

        Caption = L"Connection " + ConnNbrEdit->Text;

        ShowSave();
    }
    else
    {
        ConnNbrEdit->Text      = L"";
        LengthEdit->Text       = L"0.0";
        CommentMemo->Text      = L"";
        ResultCombo->ItemIndex = -1;

        Caption = L"Connection Control";

        HideSave();
    }

    return true;
}


void __fastcall TConnectionForm::SetCanStartConn( bool bCanNowStart )
{
    // A connection state will depend on the value of the m_bCanStartConn
    // var, so we need to set that first before changing the dlg state.
    m_bCanStartConn = bCanNowStart;

    if( bCanNowStart )
    {
        // Connections are now allowed to start. If we are in the
        // paused state, can go to idle. Otherwise, we wait until
        // a state transition to handle this flag
        if( ConnectionState == CDS_CONNS_PAUSED )
            ConnectionState = CDS_IDLE;
    }
    else
    {
        // Can't start connections at the moment. If we're idle,
        // immediately switch states. Otherwise wait until a state
        // transition to act
        if( ConnectionState == CDS_IDLE )
            ConnectionState = CDS_CONNS_PAUSED;
    }
}


void __fastcall TConnectionForm::SetAutoRecEn( bool bIsNowEnabled )
{
    m_arParams.bAutoRecEn = bIsNowEnabled;

    // Whenever auto-record is enabled, always get the latest auto
    // record params
    GetAutoRecSettings( m_arSettings );
}


void TConnectionForm::HideSave( void )
{
    ClientHeight = HIDDEN_HEIGHT;
}


void TConnectionForm::ShowSave( void )
{
    ClientHeight = EXPAND_HEIGHT;
}


void TConnectionForm::SetToStopBtn( void )
{
    m_bIsStartBtn = false;

    StartStopConnBtn->Caption = "Stop Connection (F1)";
}


void TConnectionForm::SetToStartBtn( void )
{
    m_bIsStartBtn = true;

    StartStopConnBtn->Caption = "Start Connection (F1)";
}


void __fastcall TConnectionForm::ResultComboDrawItem(TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State)
{
    const int Offset = 2;   // Default text offset width

    TCanvas *pCanvas = ResultCombo->Canvas;

    pCanvas->FillRect( Rect ); // clear the rectangle

    if( Index >= 0 )
    {
        CONN_RESULT crResult = ConnResultFromTextEnum( ResultCombo->Items->Strings[ Index ] );

        pCanvas->TextOut( Rect.Left + Offset, Rect.Top, ConnResultText[crResult] );
    }
    else
    {
        pCanvas->TextOut( Rect.Left + Offset, Rect.Top, "" );
    }
}


bool __fastcall TConnectionForm::GetInShoulderMode( void )
{
    if( !Visible )
        return false;

    if( m_currJob == NULL )
        return false;

    return SetShoulderBtn->Down;
}


bool __fastcall TConnectionForm::GetInConnection( void )
{
    if( !Visible )
        return false;

    if( m_currJob == NULL )
        return false;

    return( ( ConnectionState == CDS_CONNECTING ) || ( ConnectionState == CDS_RESULT_WAIT ) );
}


void __fastcall TConnectionForm::StartStopConnBtnClick(TObject *Sender)
{
    if( m_bIsStartBtn )
    {
        if( m_connDlgEvent( CDE_START_CONNECTION ) )
            ConnectionState = CDS_CONNECTING;
    }
    else
    {
        // If we've made at least one rotation, the user must save this as
        // as a connection. Otherwise, discard the data
        MAIN_STATUS_DATA statusData;
        m_mainStatusReq( statusData );

        if( statusData.nbrDataPtsRecorded == 0 )
        {
            // No points recorded - go to idle
            m_connDlgEvent( CDE_CONN_DONE );
            ConnectionState = CDS_IDLE;
        }
        else
        {
            // Points have been record. Check for clockwise rotation that
            // is greater than the min amount
            float maxTurns = 0.0;

            for( int iPt = 0; iPt < statusData.nbrDataPtsRecorded; iPt++ )
            {
                float turns = (float)statusData.dataPts[iPt].rotation / 1000000.0;

                if( turns > maxTurns )
                    maxTurns = turns;
            }

            bool saveConnection = false;

            if( maxTurns >= GetConnMinTurnsValue() )
            {
                saveConnection = true;
            }
            else if( !m_arParams.bAutoRecordedConn )
            {
                // For manual connections, ask the user what to do with runts
                int mrResult = MessageDlg( "Fewer than the minimum number of turns of data has been recorded. Do you want to still save this connection?"
                                           "\r  Press Yes to save this connection."
                                           "\r  Press No to discard the data from this connection.",
                                           mtConfirmation, TMsgDlgButtons() << mbYes << mbNo, 0, mbNo );

                if( mrResult == mrYes )
                    saveConnection = true;
            }

            if( saveConnection )
            {
                if( m_connDlgEvent( CDE_WAIT_ON_RESULT ) )
                {
                    ConnectionState = CDS_RESULT_WAIT;

                    // When going to the result wait state, the main form will have
                    // tried to find a shoulder point. If a shoulder was not found,
                    // automatically go into shoulder mode. Re-request status data
                    // to update shoulder info
                    m_mainStatusReq( statusData );

                    // Enabled or disable the shoulder button based on the the section
                    if( statusData.hasShoulder )
                        SetShoulderBtn->Enabled = true;
                    else
                        SetShoulderBtn->Enabled = false;

                    // If we have a shoulder and it hasn't been set, force the button down
                    if( !statusData.shoulderSet && statusData.hasShoulder )
                        SetShoulderBtn->Down = true;
                }
            }
            else
            {
                m_connDlgEvent( CDE_CONN_DONE );
                ConnectionState = CDS_IDLE;
            }
        }
    }
}


void __fastcall TConnectionForm::SetShoulderActionExec(TObject *Sender)
{
    if( SetShoulderBtn->Down )
    {
        // Determine if we should nag the user about using this button
        MAIN_STATUS_DATA statusData;
        m_mainStatusReq( statusData );

        if( statusData.autoShldrMode && statusData.shoulderSet && GetNagOnAutoShoulderOverride() )
        {
            int mrResult = MessageDlg( "You are in auto-shoulder mode. Are you sure you want to override the shoulder point?"
                                       "\r  Press Yes to override the shoulder."
                                       "\r  Press Cancel to keep the auto-detected shoulder.",
                                        mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

            if( mrResult != mrYes )
            {
                SetShoulderBtn->Down = false;
                return;
            }
        }

        if( !m_connDlgEvent( CDE_SHOULDER_MODE ) )
            SetShoulderBtn->Down = false;
    }
    else
    {
        m_connDlgEvent( CDE_SHOULDER_OFF );
    }
}


void __fastcall TConnectionForm::SaveActionExec(TObject *Sender)
{
    // Make sure the shoulder btn is not down
    SetShoulderBtn->Down = false;

    // Get the status info we need from main
    MAIN_STATUS_DATA statusData;
    m_mainStatusReq( statusData );

    // Now validate the data
    float connLength = 0.0;

    try
    {
        connLength = LengthEdit->Text.ToDouble();

        if( ( connLength <= 0.0 ) || ( connLength > GetMaxLength() ) )
            Abort();
    }
    catch (...)
    {
        MessageDlg( "You have entered an invalid length.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = LengthEdit;
        return;
    }

    if( !m_bUserSetLength )
    {
        MessageDlg( "You must specify a length value.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = LengthEdit;
        return;
    }

    if( ResultCombo->ItemIndex < 0 )
    {
        MessageDlg( "You must specify the connection's status.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    String      connResText = ResultCombo->Items->Strings[ ResultCombo->ItemIndex ];
    String      connComment = Trim( CommentMemo->Text );
    CONN_RESULT crResult    = ConnResultFromTextEnum( connResText );

    // Check the shoulder issue first, as that is the first message we'll want to see on passing conditions
    if( ( crResult == CR_FORCE_PASSED ) || ( crResult == CR_PASSED ) )
    {
        if( statusData.hasShoulder && !statusData.shoulderSet )
        {
            MessageDlg( L"You must set the shoulder point.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return;
        }
    }

    if( crResult == CR_FORCE_PASSED )
    {
        if( connComment.Length() == 0 )
        {
            MessageDlg( "When forcing a connection's status, you must enter a comment.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return;
        }
    }

    if( ( crResult == CR_FAILED ) || ( crResult == CR_FORCE_FAILED ) )
    {
        if( connComment.Length() == 0 )
        {
            MessageDlg( "When failing a connection, you must enter a comment.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return;
        }
    }

    if( ( statusData.nbrDataPtsRecorded == 0 ) && !m_arParams.bAutoRecordedConn )
    {
        int confirmResult = MessageDlg( "No data points were recorded for this connection. Are you sure you want to save this connection?"
                                        "\r  Press Yes to keep this connection."
                                        "\r  Press Cancel to discard this connection.",
                                        mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

        if( confirmResult != mrYes )
            return;
    }

    bool bKeepInInventory = false;

    if( ( crResult == CR_FAILED ) || ( crResult == CR_FORCE_FAILED ) )
    {
        int confirmResult = MessageDlg( "Will this casing length be re-used?"
                                        "\r  Press Yes to re-use this casing length for the next connection."
                                        "\r  Press No to discard this casing length.",
                                        mtConfirmation, TMsgDlgButtons() << mbYes << mbNo, 0 );

        bKeepInInventory = ( confirmResult == mrYes );
    }

    // Validation is complete. Add the joint to the job.
    CONNECTION_INFO newConn;

    newConn.length      = connLength;
    newConn.shoulderSet = true;
    newConn.shoulderPt  = statusData.shoulderPt;
    newConn.shoulderTq  = statusData.shoulderTq;
    newConn.mostNegPt   = statusData.mostNegPt;
    newConn.pkTorque    = statusData.pkTorque;
    newConn.pkTorquePt  = statusData.pkTorquePt;
    newConn.battVolts   = statusData.battVolts;
    newConn.crResult    = crResult;
    newConn.comment     = connComment;
    newConn.connNbr     = ConnNbrEdit->Text.ToIntDef( 0 );
    newConn.autoRecConn = false;   // Connections added this way are always manual connections
    newConn.creationNbr = 0;  // will be assigned by job mgr
    newConn.calRecNbr   = 0;  // will be assigned by job mgr
    newConn.seqNbr      = 0;  // will be assigned by job mgr
    newConn.sectionNbr  = 0;  // will be assigned by job mgr
    newConn.dateTime    = 0;  // will be assigned by job mgr

    m_currJob->AddConnection( newConn, statusData.dataPts, statusData.nbrDataPtsRecorded, bKeepInInventory );

    // Once the connection has been added, we must go to the idle state.
    m_connDlgEvent( CDE_CONN_DONE );
    ConnectionState = CDS_IDLE;

    // Lastly, select the length edit for easy use
    ActiveControl = LengthEdit;
}


void __fastcall TConnectionForm::SetConnDlgState( CONN_DLG_STATE newState )
{
    bool canRecord = false;

    if( m_currJob != NULL )
        canRecord = !m_currJob->Completed;

    // If we are going to the idle state, check if we can start a new connection.
    // If not, must switch to the paused state
    if( newState == CDS_IDLE )
    {
        if( !m_bCanStartConn )
            newState = CDS_CONNS_PAUSED;
    }

    // Now update based on new state
    switch( newState )
    {
        case CDS_IDLE:
            SetShoulderBtn->Down      = false;
            SetShoulderBtn->Enabled   = false;
            ResultCombo->Enabled      = false;
            CommentMemo->Enabled      = false;
            SaveBtn->Enabled          = false;

            HideSave();

            SetToStartBtn();
            StartStopConnBtn->Enabled = canRecord;

            m_arParams.bAutoRecordedConn    = false;
            m_arParams.bRequireUserInput    = false;
            m_arParams.bCrossThreadDetected = false;
            m_arParams.iCrossThreadWindow   = (int)( GetConnMinTurnsValue() * 1000000.0 );

            break;

        case CDS_CONNS_PAUSED:
            SetShoulderBtn->Down      = false;
            SetShoulderBtn->Enabled   = false;
            ResultCombo->Enabled      = false;
            CommentMemo->Enabled      = false;
            SaveBtn->Enabled          = false;

            HideSave();

            SetToStartBtn();
            StartStopConnBtn->Enabled = false;

            break;

        case CDS_CONNECTING:
            SetShoulderBtn->Down      = false;
            SetShoulderBtn->Enabled   = false;
            ResultCombo->Enabled      = false;
            CommentMemo->Enabled      = false;
            SaveBtn->Enabled          = false;

            // Refresh alarm / cross-thread vars whenever a connection starts
            m_arParams.bRequireUserInput    = false;
            m_arParams.bCrossThreadDetected = false;
            m_arParams.iCrossThreadWindow   = (int)( GetConnMinTurnsValue() * 1000000.0 );

            HideSave();

            SetToStopBtn();
            StartStopConnBtn->Enabled = true;

            break;

        case CDS_RESULT_WAIT:
            SetShoulderBtn->Down      = false;
            SetShoulderBtn->Enabled   = true;
            ResultCombo->Enabled      = true;
            CommentMemo->Enabled      = true;
            SaveBtn->Enabled          = true;

            ShowSave();

            SetToStartBtn();
            StartStopConnBtn->Enabled = false;

            break;
    }

    m_connDlgState = newState;
}


void __fastcall TConnectionForm::StartStopItemClick(TObject *Sender)
{
    // Attempt to start or stop the connection
    if( StartStopConnBtn->Enabled )
        StartStopConnBtnClick( StartStopConnBtn );
}


void __fastcall TConnectionForm::LengthEditChange(TObject *Sender)
{
    // If the user has set the value, show it as black/red, otherwise show as gray
    if( m_bUserSetLength )
    {
        float fLength = 0.0;

        try
        {
            // Get our value and check if it is valid
            fLength = LengthEdit->Text.ToDouble();

            if( ( fLength <= 0.0 ) || ( fLength > GetMaxLength() ) )
                Abort();

            // Fall through means a good value
            LengthEdit->Font->Color = clBlack;
        }
        catch( ... )
        {
            // Bad value, color it red
            LengthEdit->Font->Color = clRed;
        }
    }
    else
    {
        LengthEdit->Font->Color = clGrayText;
    }
}


void __fastcall TConnectionForm::LengthEditKeyPress(TObject *Sender, System::WideChar &Key)
{
    // If a key has been set, then we assume the user has changed the text
    m_bUserSetLength = true;
}


// Get the max length based on the current jobs UOM
float TConnectionForm::GetMaxLength()
{
    UNITS_OF_MEASURE iUOM;

    // No job selected, return imperial length
    if( m_currJob == NULL )
        iUOM = (UNITS_OF_MEASURE) GetDefaultUnitsOfMeasure();
    else
        iUOM = m_currJob->Units;

    return TJob::MaxCasingLength( iUOM );
}


bool TConnectionForm::ProcessSample( const WTTTS_READING& nextReading, int rpm )
{
    // Used when auto-recording. Process the sample if auto-record is enabled
    // or the current connection is an auto-record one
    bool bDiscardSample = false;

    if( ( m_arParams.bAutoRecEn || m_arParams.bAutoRecordedConn ) && !m_arParams.bRequireUserInput )
    {
        switch( ConnectionState )
        {
            case CDS_IDLE:
                if( ( rpm >= m_arSettings.minRPMToStart ) && ( nextReading.torque >= m_arSettings.minTorqueToStart ) )
                {
                    // Have a start condition - start the connection. Note that the
                    // state machine will clear this var when the connection is done.
                    m_arParams.bAutoRecordedConn = true;

                    // Note this time as a potential end of connection time
                    m_arParams.dwStoppingTick = GetTickCount();

                    StartStopConnBtnClick( NULL );

                    // Make sure the dialog is visible, in case the user must intervene
                    Show();

                    // This sample would have been from RFC data. Tell main to not plot this point
                    bDiscardSample = true;
                }
                break;

            case CDS_CONNECTING:
                // While connecting, wait for a stop condition
                if( ( rpm < m_arSettings.minRPMToStart ) && ( nextReading.torque < m_arSettings.minTorqueToStart ) )
                {
                    // Have a start condition - start the connection
                    if( HaveTimeout( m_arParams.dwStoppingTick, m_arSettings.stopTime * 1000 ) )
                    {
                        StartStopConnBtnClick( NULL );

                        // Make sure the dialog is visible, in case the user must intervene
                        Show();
                    }
                }
                else
                {
                    // Either torque or RPM not meeting stop condition, so reset timeout
                    m_arParams.dwStoppingTick = GetTickCount();

                    // While connecting, watch for a cross thread condition if we are in the
                    // early turn stages of the connection. Only check when the rotation has
                    // gone positive though.
                    if( ( nextReading.rotation > 0 ) && ( nextReading.rotation < m_arParams.iCrossThreadWindow ) )
                    {
                        if( nextReading.torque > m_arSettings.crossThreadThresh )
                        {
                            // Alarm has occurred. Let the user know if this is the first occurance
                            // in this connection
                            if( !m_arParams.bCrossThreadDetected )
                            {
                                m_arParams.bCrossThreadDetected = true;
                                ::PostMessage( Application->MainForm->Handle, WM_SHOW_ALARM_EVENT, TAlarmDlg::eET_CrossThread, 0 );
                            }
                        }
                    }
                }
                break;

            case CDS_RESULT_WAIT:
                // Analyze the results in this state, possibly throwing an alarm if required.
                // Unfortunately, there is some duplication in logic between this code and
                // the Save button exec code. But we can only call the Save button code
                // after possibly posting an alarm message to the main form.
                {
                    // Get the status info we need from main. If there are no data points,
                    // then immediately go back to idle
                    MAIN_STATUS_DATA statusData;
                    m_mainStatusReq( statusData );

                    if( statusData.nbrDataPtsRecorded == 0 )
                    {
                        m_connDlgEvent( CDE_CONN_DONE );
                        ConnectionState = CDS_IDLE;
                    }
                    else
                    {
                        bool bHaveAlarm = false;
                        TAlarmDlg::EventType eType = (TAlarmDlg::EventType)-1;

                        // Validate length
                        float connLength = StrToFloatDef( LengthEdit->Text, 0.0 );

                        if( ( connLength <= 0.0 ) || ( connLength > GetMaxLength() ) )
                        {
                            // Invalid length
                            bHaveAlarm = true;
                            eType      = TAlarmDlg::eET_BadConnLength;
                        }
                        else
                        {
                            // Ensure shoulder found
                            if( statusData.hasShoulder && !statusData.shoulderSet  )
                            {
                                // No shoulder
                                bHaveAlarm = true;
                                eType      = TAlarmDlg::eET_NoShoulder;
                            }
                            else
                            {
                                // Check results - if passed is not an option, then user must be pick
                                bool bPassPresent = false;

                                for( int iItem = 0; iItem < ResultCombo->Items->Count; iItem++ )
                                {
                                    CONN_RESULT crResult = ConnResultFromTextEnum( ResultCombo->Items->Strings[iItem] );

                                    if( crResult == CR_PASSED )
                                    {
                                        // Select this result
                                        ResultCombo->ItemIndex = iItem;
                                        bPassPresent = true;
                                        break;
                                    }
                                }

                                if( !bPassPresent )
                                {
                                    // Invalid result alarm
                                    bHaveAlarm = true;
                                    eType      = TAlarmDlg::eET_ConnFailed;
                                }
                            }
                        }

                        if( bHaveAlarm )
                        {
                            // Require user-intervention.
                            m_arParams.bRequireUserInput = true;

                            // Raise an alarm.
                            ::PostMessage( Application->MainForm->Handle, WM_SHOW_ALARM_EVENT, eType, 0 );

                            // Make sure the dialog is visible
                            Show();
                        }
                        else
                        {
                            // Validation is complete. Add the joint to the job.
                            CONNECTION_INFO newConn;

                            newConn.length      = connLength;
                            newConn.shoulderSet = true;
                            newConn.shoulderPt  = statusData.shoulderPt;
                            newConn.shoulderTq  = statusData.shoulderTq;
                            newConn.mostNegPt   = statusData.mostNegPt;
                            newConn.pkTorque    = statusData.pkTorque;
                            newConn.pkTorquePt  = statusData.pkTorquePt;
                            newConn.battVolts   = statusData.battVolts;
                            newConn.crResult    = CR_PASSED;   // Must be the case for auto-recorded conn
                            newConn.comment     = "";
                            newConn.connNbr     = ConnNbrEdit->Text.ToIntDef( 0 );
                            newConn.autoRecConn = true;
                            newConn.creationNbr = 0;  // will be assigned by job mgr
                            newConn.calRecNbr   = 0;  // will be assigned by job mgr
                            newConn.seqNbr      = 0;  // will be assigned by job mgr
                            newConn.sectionNbr  = 0;  // will be assigned by job mgr
                            newConn.dateTime    = 0;  // will be assigned by job mgr

                            m_currJob->AddConnection( newConn, statusData.dataPts, statusData.nbrDataPtsRecorded, false );

                            // Once the connection has been added, we must go to the idle state.
                            m_connDlgEvent( CDE_CONN_DONE );
                            ConnectionState = CDS_IDLE;
                        }
                    }
                }
                break;

            default:
                // In any other states, we do no processing
                break;
        }
    }

    return bDiscardSample;
}

