object ConnectionForm: TConnectionForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Connection nnn'
  ClientHeight = 374
  ClientWidth = 207
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PopupMenu = HiddenPopUp
  Position = poDesigned
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  DesignSize = (
    207
    374)
  PixelsPerInch = 96
  TextHeight = 13
  object NewConnGB: TGroupBox
    Left = 8
    Top = 8
    Width = 191
    Height = 358
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Connection '
    TabOrder = 0
    object Label2: TLabel
      Left = 12
      Top = 21
      Width = 37
      Height = 13
      Caption = 'Number'
    end
    object LengthLabel: TLabel
      Left = 103
      Top = 21
      Width = 52
      Height = 13
      Caption = 'Length (ft)'
    end
    object Label4: TLabel
      Left = 12
      Top = 160
      Width = 95
      Height = 13
      Caption = 'Mark Connection As'
    end
    object Label13: TLabel
      Left = 12
      Top = 207
      Width = 45
      Height = 13
      Caption = 'Comment'
    end
    object SetShoulderBtn: TSpeedButton
      Left = 12
      Top = 112
      Width = 167
      Height = 40
      AllowAllUp = True
      GroupIndex = 2
      Caption = 'Set Shoulder'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = SetShoulderActionExec
    end
    object ConnNbrEdit: TEdit
      Left = 12
      Top = 35
      Width = 76
      Height = 21
      TabStop = False
      Color = clBtnFace
      NumbersOnly = True
      ReadOnly = True
      TabOrder = 0
      Text = '0'
    end
    object LengthEdit: TEdit
      Left = 103
      Top = 35
      Width = 76
      Height = 21
      TabOrder = 1
      Text = '0'
      OnChange = LengthEditChange
      OnKeyPress = LengthEditKeyPress
    end
    object ResultCombo: TComboBox
      Left = 12
      Top = 177
      Width = 167
      Height = 22
      Style = csOwnerDrawFixed
      TabOrder = 2
      OnDrawItem = ResultComboDrawItem
    end
    object SaveBtn: TButton
      Left = 12
      Top = 306
      Width = 167
      Height = 40
      Caption = 'Save'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = SaveActionExec
    end
    object CommentMemo: TMemo
      Left = 12
      Top = 224
      Width = 167
      Height = 75
      Lines.Strings = (
        '')
      ScrollBars = ssVertical
      TabOrder = 3
    end
    object StartStopConnBtn: TButton
      Left = 13
      Top = 64
      Width = 166
      Height = 40
      Caption = 'Start Connection (F1)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = StartStopConnBtnClick
    end
  end
  object HiddenPopUp: TPopupMenu
    AutoPopup = False
    Left = 88
    Top = 8
    object StartStopItem: TMenuItem
      Caption = 'Start / Stop'
      ShortCut = 112
      OnClick = StartStopItemClick
    end
  end
end
