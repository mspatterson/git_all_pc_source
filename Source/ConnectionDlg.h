#ifndef ConnectionDlgH
#define ConnectionDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.Menus.hpp>
#include "JobManager.h"
#include "RegistryInterface.h"


// This dialog is an extension of the main form. It manages the connection
// state and button states in this dialog, but must keep its state in sync
// with what is happening on the main form. Main provides a pair of callback
// functions that helps this dialog controls its state. One callback is used
// to send button presses to the main form, the other is used to get current
// state info from main.
typedef enum {
    CDE_START_CONNECTION,
    CDE_WAIT_ON_RESULT,
    CDE_SHOULDER_MODE,
    CDE_SHOULDER_OFF,
    CDE_CONN_DONE,
    NBR_CONN_DLG_EVENTS
} CONN_DLG_EVENT;

typedef bool __fastcall (__closure *TConnectionDlgEvent)( CONN_DLG_EVENT anEvent );


// The following structure and callback is used by the connection dialog
// to request current status information from main
typedef struct {
    bool           autoShldrMode;
    bool           hasShoulder;
    bool           isDataStreaming;
    int            nbrDataPtsRecorded;
    WTTTS_READING* dataPts;
    bool           shoulderSet;
    float          shoulderPt;   // In turns
    float          shoulderTq;   // In job units
    float          mostNegPt;
    float          pkTorque;
    float          pkTorquePt;
    float          battVolts;
} MAIN_STATUS_DATA;

typedef void __fastcall (__closure *TConnectionDlgStatusReq)( MAIN_STATUS_DATA& statusData );


class TConnectionForm : public TForm
{
__published:    // IDE-managed Components
    TGroupBox *NewConnGB;
    TLabel *Label2;
    TLabel *LengthLabel;
    TLabel *Label4;
    TLabel *Label13;
    TSpeedButton *SetShoulderBtn;
    TEdit *ConnNbrEdit;
    TEdit *LengthEdit;
    TComboBox *ResultCombo;
    TButton *SaveBtn;
    TMemo *CommentMemo;
    TButton *StartStopConnBtn;
    TPopupMenu *HiddenPopUp;
    TMenuItem *StartStopItem;
    void __fastcall SetShoulderActionExec(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
    void __fastcall SaveActionExec(TObject *Sender);
    void __fastcall ResultComboDrawItem(TWinControl *Control, int Index, TRect &Rect, TOwnerDrawState State);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall StartStopConnBtnClick(TObject *Sender);
    void __fastcall CreateParams(Controls::TCreateParams &Params);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall StartStopItemClick(TObject *Sender);
    void __fastcall LengthEditChange(TObject *Sender);
    void __fastcall LengthEditKeyPress(TObject *Sender, System::WideChar &Key);

  private:

    void HideSave( void );
    void ShowSave( void );

    void SetToStopBtn( void );
    void SetToStartBtn( void );

    bool m_bIsStartBtn;
    bool m_bShowFormCalled;

    TJob* m_currJob;

    AUTO_REC_SETTINGS m_arSettings;

    struct {
        bool  bAutoRecEn;           // True if auto-record is enabled
        bool  bAutoRecordedConn;    // Set to true when the current connection started via auto-record feature
        bool  bRequireUserInput;    // Set to true when an auto-recorded connection requires user input
        bool  bCrossThreadDetected; // Set to true if a cross-thread condition was detected
        int   iCrossThreadWindow;   // Number of turns below which cross-threading is monitored
        DWORD dwStoppingTick;
    } m_arParams;

    // Main form callback events
    TConnectionDlgEvent     m_connDlgEvent;
    TConnectionDlgStatusReq m_mainStatusReq;

    bool __fastcall GetInConnection( void );
    bool __fastcall GetInShoulderMode( void );

    void __fastcall SetCurrJob( TJob* newJob );

    bool m_bCanStartConn;

    void __fastcall SetCanStartConn( bool bCanNowStart );
    void __fastcall SetAutoRecEn( bool bIsNowEnabled );

    typedef enum {
        CDS_IDLE,
        CDS_CONNS_PAUSED,
        CDS_CONNECTING,
        CDS_RESULT_WAIT,
        NBR_CONN_DLG_STATES
    } CONN_DLG_STATE;

    CONN_DLG_STATE m_connDlgState;

    void __fastcall SetConnDlgState( CONN_DLG_STATE newState );

    __property CONN_DLG_STATE ConnectionState = { read = m_connDlgState, write = SetConnDlgState };
       // Do not make this property public!

    bool m_bUserSetLength;

    float GetMaxLength();

    // Auto-record support
    typedef enum {
        eARS_Off,                // Auto-record is disabled
        eARS_Waiting,            // Auto-record enabled, monitoring for start condition
        eARS_Connecting,         // Performing an auto-record connection
        eARS_UserOverride,       // User performing a manual connnection while auto-rec enabled
        eARS_NbrAutoRecStates
    } eAutoRecState;

  protected:

    void __fastcall WMExitMove( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_EXITSIZEMOVE, TMessage, WMExitMove )
    END_MESSAGE_MAP(TForm)

  public:
    __fastcall TConnectionForm(TComponent* Owner);

    // Set the event callbacks immediately after the form has been contructed
    typedef struct {
        TConnectionDlgEvent     connDlgEvent;
        TConnectionDlgStatusReq mainStatusReq;
    } CONN_DLG_CALLBACKS;

    void SetEventCallbacks( CONN_DLG_CALLBACKS dlgCallbacks );

    void ShowForm( void );

    __property TJob*         CurrentJob        = { read = m_currJob, write = SetCurrJob };

    __property bool          InConnection      = { read = GetInConnection };
    __property bool          InShoulderMode    = { read = GetInShoulderMode };
    __property bool          IsActive          = { read = m_bShowFormCalled };

    __property bool          CanStartConn      = { read = m_bCanStartConn,       write = SetCanStartConn };
    __property bool          AutoRecordEnabled = { read = m_arParams.bAutoRecEn, write = SetAutoRecEn };
    __property bool          AutoRecordedConn  = { read = m_arParams.bAutoRecordedConn };

    // While connecting, calling ForceStop() will cause the Stop button
    // to be 'pressed'. Only call this if something catastrohpic has happened.
    void ForceStop( void );

    // Main calls ShoulderSet() when either the auto-shoulder function finds
    // a shoulder or the user manually identifies the shoulder
    void ShoulderSet( void );

    // Main calls SetResultOptions() in the result wait state to populate
    // the result combo with options
    void SetResultOptions( ConnResultSet resultSet );

    // Call PrepareForConnection to prepare the dialog for the next connection
    void PrepareForConnection( bool resetLengthEdit );

    // When not in a connection, this method will display the result of a
    // previously completed connection. Can only be called when not streaming
    // or not in a connection
    bool ShowConnection( const CONNECTION_INFO* pConnInfo );

    // Auto-record Support. Returns true if reading should *not* be plotted
    bool ProcessSample( const WTTTS_READING& nextReading, int rpm );

};

extern PACKAGE TConnectionForm *ConnectionForm;

#endif
