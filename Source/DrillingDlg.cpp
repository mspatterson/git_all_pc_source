#include <vcl.h>
#pragma hdrstop

#include "DrillingDlg.h"
#include "RegistryInterface.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TDrillingForm *DrillingForm;



static const String StartDrillingCaption( "Start Drilling (F1)" );
static const String StopDrillingCaption ( "Stop Drilling (F1)" );


__fastcall TDrillingForm::TDrillingForm(TComponent* Owner) : TForm(Owner)
{
    // Set state
    SystemUp = false;
}


void TDrillingForm::SetEventCallbacks( TDrillingDlgEvent dlgCallback )
{
    m_drillDlgEvent = dlgCallback;
}


void TDrillingForm::ShowForm( void )
{
    // Show the dialog modelessly
    Show();
}


void __fastcall TDrillingForm::FormShow(TObject *Sender)
{
    // Restore dialog position
    int top  = Top;
    int left = Left;
    int heightNotUsed = 0;
    int widthNotUsed  = 0;

    GetDialogPos( DT_DRILLING_DLG, top, left, heightNotUsed, widthNotUsed );

    // Only set the position if it's on screen. Avoid issues with dual monitors
    if( top < 0 )
        Top = 10;
    else if( top < Screen->DesktopHeight - Height )
        Top = top;
    else
        Top = Screen->DesktopHeight - Height;

    if( left < 0 )
        Left = 10;
    else if( left < Screen->DesktopWidth - Width )
        Left = left;
    else
        Left = Screen->DesktopWidth - Width;

    // Enabled poll timer on show
    PollTimer->Enabled = true;
}


void __fastcall TDrillingForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    // Can't close while drilling
    if( StartStopDrillButton->Tag == eButtonTypeStop )
    {
        MessageDlg( "You cannot close this form while drilling is in progress.", mtError, TMsgDlgButtons() << mbOK, 0 );
        CanClose = false;
    }
    else
    {
        CanClose = true;
    }
}


void __fastcall TDrillingForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Disable poll timer on close
    PollTimer->Enabled = false;
}


void __fastcall TDrillingForm::WMExitMove( TMessage &Message )
{
    // Save form position, if visible
    if( Visible )
        SaveDialogPos( DT_DRILLING_DLG, Top, Left, Height, Width );
}


void __fastcall TDrillingForm::CreateParams(Controls::TCreateParams &Params)
{
    // Add the WS_EX_APPWINDOW style to this form to cause its icon to be
    // be made present on the task bar
    TForm::CreateParams(Params);

    Params.ExStyle   = Params.ExStyle | WS_EX_APPWINDOW;
    Params.WndParent = ParentWindow;
}


void __fastcall TDrillingForm::SetSystemUp( bool bIsNowUp )
{
    if( bIsNowUp )
    {
        // System is up and we can drill. If we already knew we were
        // up then no need to do anything. Otherwise, enable our buttons
        if( !m_bSystemUp )
        {
            StartStopDrillButton->Caption = StartDrillingCaption;
            StartStopDrillButton->Tag     = eButtonTypeStart;
            StartStopDrillButton->Enabled = true;
        }
    }
    else
    {
        // System is now down. Disable things
        StartStopDrillButton->Caption = StartDrillingCaption;
        StartStopDrillButton->Tag     = eButtonTypeStart;
        StartStopDrillButton->Enabled = false;
    }

    m_bSystemUp = bIsNowUp;
}


void TDrillingForm::DoingStreamCheck( bool bDoingCheck )
{
    // Call this function when a stream check is underway or stopped
    StartStopDrillButton->Caption = StartDrillingCaption;
    StartStopDrillButton->Tag     = eButtonTypeStart;
    StartStopDrillButton->Enabled = !bDoingCheck;
}


bool __fastcall TDrillingForm::GetIsDrilling( void )
{
    if( !Visible )
        return false;

    return ( StartStopDrillButton->Tag == eButtonTypeStop );
}


bool __fastcall TDrillingForm::GetIsActive( void )
{
    // We are active if the poll timer is enabled
    return PollTimer->Enabled;
}


void __fastcall TDrillingForm::PollTimerTimer(TObject *Sender)
{
    // Poll timer is always enabled so that the comms status is up to date.
    // Elapsed time is only refreshed if we are drilling
    if( StartStopDrillButton->Tag == eButtonTypeStop )
    {
        // Refresh the elapsed time
        TDateTime elapsedTime = Now() - m_StartTime;

        TimePanel->Caption = "Elapsed Time " + FormatDateTime( "hh:nn:ss", elapsedTime );
    }
}


void __fastcall TDrillingForm::StartStopDrillButtonClick(TObject *Sender)
{
    if( StartStopDrillButton->Tag == eButtonTypeStart )
    {
        // Handle Start Button Click here
        if( m_drillDlgEvent( DDE_START_DRILLING ) )
        {
            StartStopDrillButton->Caption = StopDrillingCaption;
            StartStopDrillButton->Tag     = eButtonTypeStop;

            m_StartTime = Now();
        }
    }
    else if( StartStopDrillButton->Tag == eButtonTypeStop )
    {
        // Handle Stop Button Click here
        if( m_drillDlgEvent( DDE_STOP_DRILLING ) )
        {
            StartStopDrillButton->Caption = StartDrillingCaption;
            StartStopDrillButton->Tag     = eButtonTypeStart;
        }
    }
}


void __fastcall TDrillingForm::StartStopItemClick(TObject *Sender)
{
    // Attempt to start or stop the connection
    if( StartStopDrillButton->Enabled )
        StartStopDrillButtonClick( StartStopDrillButton );
}
