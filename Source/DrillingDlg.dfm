object DrillingForm: TDrillingForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Drilling Control'
  ClientHeight = 142
  ClientWidth = 207
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  PopupMenu = HiddenPopUp
  Position = poDesigned
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  DesignSize = (
    207
    142)
  PixelsPerInch = 96
  TextHeight = 13
  object NewConnGB: TGroupBox
    Left = 8
    Top = 8
    Width = 191
    Height = 126
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Control '
    TabOrder = 0
    DesignSize = (
      191
      126)
    object TimePanel: TPanel
      Left = 12
      Top = 72
      Width = 165
      Height = 40
      Anchors = [akLeft, akTop, akRight]
      BevelKind = bkFlat
      BevelOuter = bvNone
      Caption = 'Elapsed Time 00:00'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object StartStopDrillButton: TButton
      Left = 12
      Top = 20
      Width = 165
      Height = 40
      Caption = 'Start Drilling'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = StartStopDrillButtonClick
    end
  end
  object PollTimer: TTimer
    Enabled = False
    OnTimer = PollTimerTimer
    Left = 136
    Top = 8
  end
  object HiddenPopUp: TPopupMenu
    AutoPopup = False
    Left = 80
    Top = 8
    object StartStopItem: TMenuItem
      Caption = 'Start / Stop'
      ShortCut = 112
      OnClick = StartStopItemClick
    end
  end
end
