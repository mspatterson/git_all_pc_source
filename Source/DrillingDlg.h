#ifndef DrillingDlgH
#define DrillingDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>

// This dialog is an extension of the main form. It manages the drilling
// state and button states in this dialog, but must keep its state in sync
// with what is happening on the main form. Main provides a callback function
// that helps this dialog controls its state.
typedef enum {
    DDE_START_DRILLING,
    DDE_STOP_DRILLING,
    NBR_DRILLING_DLG_EVENTS
} DRILLING_DLG_EVENT;

typedef bool __fastcall (__closure *TDrillingDlgEvent)( DRILLING_DLG_EVENT anEvent );


class TDrillingForm : public TForm
{
__published:    // IDE-managed Components
    TGroupBox *NewConnGB;
    TPanel *TimePanel;
    TTimer *PollTimer;
    TButton *StartStopDrillButton;
    TPopupMenu *HiddenPopUp;
    TMenuItem *StartStopItem;
    void __fastcall PollTimerTimer(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall StartStopDrillButtonClick(TObject *Sender);
    void __fastcall CreateParams(Controls::TCreateParams &Params);
    void __fastcall StartStopItemClick(TObject *Sender);

  private:

    typedef enum{
        eButtonTypeStart,
        eButtonTypeStop
    } BUTTON_TYPE_TAG;

    TDateTime         m_StartTime;
    TDrillingDlgEvent m_drillDlgEvent;

    bool __fastcall GetIsDrilling( void );
    bool __fastcall GetIsActive( void );

    bool m_bSystemUp;

    void __fastcall SetSystemUp( bool bIsNowUp );

  protected:

    void __fastcall WMExitMove( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_EXITSIZEMOVE, TMessage, WMExitMove )
    END_MESSAGE_MAP(TForm)

  public:
    __fastcall TDrillingForm(TComponent* Owner);

    // Set the event callbacks immediately after the form has been contructed
    void SetEventCallbacks( TDrillingDlgEvent dlgCallback );

    void ShowForm( void );

    __property bool IsDrilling = { read = GetIsDrilling };
    __property bool IsActive   = { read = GetIsActive };

    __property bool SystemUp   = { read = m_bSystemUp, write = SetSystemUp };

    // Call this function when a stream check is underway or stopped
    void DoingStreamCheck( bool bDoingCheck );
};
//---------------------------------------------------------------------------
extern PACKAGE TDrillingForm *DrillingForm;
//---------------------------------------------------------------------------
#endif
