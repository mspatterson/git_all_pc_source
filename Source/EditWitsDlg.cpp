#include <vcl.h>
#pragma hdrstop

#include "EditWitsDlg.h"
#include "SubDeviceBaseClass.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TEditWitsForm *EditWitsForm;


__fastcall TEditWitsForm::TEditWitsForm(TComponent* Owner) : TForm(Owner)
{
}


bool TEditWitsForm::ShowAsAddDlg( WITS_FIELD_SETTING& fieldSettings )
{
    // Format the dialog to allow adding a WITS packet
    EditWitsForm->Caption = "Add WITS Field";
    WitsDoneBtn->Caption  = "Add";

    // Load the Field Combo
    WitsFieldCombo->Clear();

    for( int iFieldNum = 0; iFieldNum < TSubDevice::eWF_NbrWITSFields; iFieldNum++ )
        WitsFieldCombo->Items->Add( TSubDevice::WitsFieldCaptions[iFieldNum] );

    WitsFieldCombo->ItemIndex = 0;

    // When adding, we set defualt field values
    WitsEnableCB->Checked     = true;
    WitsCodeEdit->Text        = "0000";
    WitsFieldCombo->ItemIndex = 0;

    ActiveControl = WitsFieldCombo;

    // Show this dialog - Return false if cancel/x was clicked
    if( ShowModal() != mrOk )
        return false;

    // The add/save button click ensures that these values are safe
    fieldSettings.bEnabled   = WitsEnableCB->Checked;
    fieldSettings.iCode      = StrToInt( WitsCodeEdit->Text );
    fieldSettings.iFieldEnum = WitsFieldCombo->ItemIndex;

    // Otherwise, return true
    return true;
}


bool TEditWitsForm::ShowAsEditDlg( WITS_FIELD_SETTING& fieldSettings )
{
    // Format the dialog to allow editing of a WITS packet
    EditWitsForm->Caption = "Edit WITS Field";
    WitsDoneBtn->Caption  = "Save";

    // Load the Field Combo
    WitsFieldCombo->Clear();

    for( int iFieldNum = 0; iFieldNum < TSubDevice::eWF_NbrWITSFields; iFieldNum++ )
        WitsFieldCombo->Items->Add( TSubDevice::WitsFieldCaptions[iFieldNum] );

    WitsFieldCombo->ItemIndex = 0;

    // When editing, we need to load the values
    WitsEnableCB->Checked     = fieldSettings.bEnabled;
    WitsCodeEdit->Text        = FormatWITSCode( fieldSettings.iCode );
    WitsFieldCombo->ItemIndex = fieldSettings.iFieldEnum;

    ActiveControl = WitsFieldCombo;

    // Show this dialog - Return false if cancel/x was clicked
    if( ShowModal() != mrOk )
        return false;

    // The add/save button click ensures that these values are safe
    fieldSettings.bEnabled   = WitsEnableCB->Checked;
    fieldSettings.iCode      = StrToInt( WitsCodeEdit->Text );
    fieldSettings.iFieldEnum = WitsFieldCombo->ItemIndex;

    // Otherwise, return true
    return true;
}


// Done button clicked - Check the field information, then save/add
void __fastcall TEditWitsForm::WitsDoneBtnClick(TObject *Sender)
{
    // First check that the code is a legit value
    int iHdrCode = StrToIntDef( WitsCodeEdit->Text, 0 );

    if( ( iHdrCode < 1 ) || ( iHdrCode > 9999 ) )
    {
        MessageDlg( "The WITS code must be an integer value from 1 to 9999.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    // With the field being a legit value, now ensure the user has
    // specified the correct length
    if( WitsCodeEdit->Text.Length() != 4 )
    {
        MessageDlg( "The WITS code must be four digits long.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    if( WitsFieldCombo->ItemIndex < 0 )
    {
        MessageDlg( "You must select a Field to associate to this code.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    // Values are good, allow closing of the dialog
    ModalResult = mrOk;
}

