object EditWitsForm: TEditWitsForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Edit WITS Field'
  ClientHeight = 166
  ClientWidth = 250
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    250
    166)
  PixelsPerInch = 96
  TextHeight = 13
  object WitsSettingsGB: TGroupBox
    Left = 8
    Top = 8
    Width = 234
    Height = 150
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Settings '
    TabOrder = 0
    DesignSize = (
      234
      150)
    object WitsCodeLB: TLabel
      Left = 16
      Top = 52
      Width = 25
      Height = 13
      Caption = 'Code'
    end
    object WitsFieldLB: TLabel
      Left = 16
      Top = 22
      Width = 22
      Height = 13
      Caption = 'Field'
    end
    object WitsEnableCB: TCheckBox
      Left = 16
      Top = 81
      Width = 65
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Enabled'
      TabOrder = 0
    end
    object WitsCodeEdit: TEdit
      Left = 68
      Top = 49
      Width = 65
      Height = 21
      TabOrder = 1
    end
    object WitsFieldCombo: TComboBox
      Left = 68
      Top = 19
      Width = 145
      Height = 21
      TabOrder = 2
    end
    object WitsDoneBtn: TButton
      Left = 61
      Top = 114
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'OK'
      Default = True
      TabOrder = 3
      OnClick = WitsDoneBtnClick
    end
    object WitsCancelBtn: TButton
      Left = 148
      Top = 114
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 4
    end
  end
end
