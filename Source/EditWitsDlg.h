#ifndef EditWitsDlgH
#define EditWitsDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>

#include "RegistryInterface.h"

class TEditWitsForm : public TForm
{
    __published:    // IDE-managed Components
        TGroupBox *WitsSettingsGB;
        TCheckBox *WitsEnableCB;
        TLabel *WitsCodeLB;
        TLabel *WitsFieldLB;
        TEdit *WitsCodeEdit;
        TComboBox *WitsFieldCombo;
        TButton *WitsDoneBtn;
        TButton *WitsCancelBtn;
        void __fastcall WitsDoneBtnClick(TObject *Sender);

    private:        // User declarations

    public:         // User declarations
        __fastcall TEditWitsForm(TComponent* Owner);

        bool ShowAsAddDlg ( WITS_FIELD_SETTING& fieldSettings );
        bool ShowAsEditDlg( WITS_FIELD_SETTING& fieldSettings );

};

extern PACKAGE TEditWitsForm *EditWitsForm;

#endif
