#include <vcl.h>
#pragma hdrstop

#include "GraphSettingDlg.h"
#include "RegistryInterface.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TGraphSettingForm *GraphSettingForm;


//
// Private Functions
//

static bool IsValidFloat( TEdit* anEdit )
{
    float fVal = 0.0;

    try
    {
        fVal = anEdit->Text.ToDouble();
    }
    catch( ... )
    {
    }

    return( fVal > 0.0 );
}


static bool IsValidInt( TEdit* anEdit )
{
    int iVal = 0;

    try
    {
        iVal = anEdit->Text.ToInt();
    }
    catch( ... )
    {
    }

    return( iVal > 0 );
}


//
// Class Implementation
//

__fastcall TGraphSettingForm::TGraphSettingForm(TComponent* Owner) : TForm(Owner)
{
}


bool TGraphSettingForm::ShowDlg( UNITS_OF_MEASURE uomType )
{
    bool okPressed = false;

    TGraphSettingForm* currForm = new TGraphSettingForm( NULL );

    currForm->UnitsOfMeasure = uomType;

    try
    {
        if( currForm->ShowModal() == mrOk )
            okPressed = true;
    }
    __finally
    {
        delete currForm;
    }

    return okPressed;
}


void __fastcall TGraphSettingForm::FormShow(TObject *Sender)
{
    // On show, populate current settings
    LoadGraphPage();
}


void __fastcall TGraphSettingForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // On close, if ModalResult == mrOK, then save current settings.
    // In this case, all settings are guaranteed to be valid.
    if( ModalResult == mrOk )
    {
        SaveGraphPage();
    }
}


void __fastcall TGraphSettingForm::OKBtnClick(TObject *Sender)
{
    // Validate settings
    TWinControl* invalidControl = CheckGraphPage();

    if( invalidControl != NULL )
    {
        ActiveControl = invalidControl;
        return;
    }

    // Values look good. Can close dialog
    ModalResult = mrOk;
}


void TGraphSettingForm::LoadGraphPage( void )
{
    // Graph axis spans
    GRAPH_AXIS_VALUES axisParams;

    GetGraphAxisSpan ( m_uomType, axisParams );

    TqAxisSpanEdit->Text    = IntToStr( axisParams.torque );
    RPMAxisSpanEdit->Text   = IntToStr( axisParams.RPM );
    TenAxisSpanEdit->Text   = IntToStr( axisParams.tension );
    TurnsAxisSpanEdit->Text = IntToStr( axisParams.turns );
    TimeAxisSpanEdit->Text  = IntToStr( axisParams.time );

    AutoCalSpansCB->Checked = axisParams.bAutoCalTq;

    // Force refresh on torque span
    AutoCalSpansCBClick( AutoCalSpansCB );

    // Get increments
    GetGraphAxisIncrements( m_uomType, axisParams );

    TurnsAxisIncrEdit->Text = IntToStr( axisParams.turns );
    TimeAxisIncrEdit->Text  = IntToStr( axisParams.time );

    // Update units labels
    UnitAxisSpanLB->Caption = "(" + GetUnitsText( m_uomType, MT_TORQUE_SHORT ) + ")";

    // Misc graph settings
    ShldrBoxSizeEdit->Text      = IntToStr( GetShoulderBoxSize() );
    ShowShldrCursorsCB->Checked = GetShowShldrCursors();

    TColor rpmColor;
    TColor torqueColor;
    TColor tenColor;

    GetGraphColors( rpmColor, torqueColor, tenColor );

    TorqueColorShape->Brush->Color  = torqueColor;
    RPMColorShape->Brush->Color     = rpmColor;
    TensionColorShape->Brush->Color = tenColor;

    // RPM Averaging
    RPMSamplesEdit->Text = IntToStr( GetRPMAvgingValue() );
}


void TGraphSettingForm::SaveGraphPage( void )
{
    // Graph axis settings. Note that with torque, the edit may be
    // disabled and contain a bogus value if the auto-tq checkbox
    // is checked. So use ToIntDef() for it (default value is
    // copied from registry.cpp).
    GRAPH_AXIS_VALUES axisParams;

    axisParams.torque     = TqAxisSpanEdit->Text.ToIntDef( 16000 );
    axisParams.RPM        = RPMAxisSpanEdit->Text.ToInt();
    axisParams.tension    = TenAxisSpanEdit->Text.ToInt();
    axisParams.turns      = TurnsAxisSpanEdit->Text.ToInt();
    axisParams.time       = TimeAxisSpanEdit->Text.ToInt();
    axisParams.bAutoCalTq = AutoCalSpansCB->Checked;

    SaveGraphAxisSpan ( m_uomType, axisParams );

    axisParams.turns = TurnsAxisIncrEdit->Text.ToInt();
    axisParams.time  = TimeAxisIncrEdit->Text.ToInt();

    SaveGraphAxisIncrements( m_uomType, axisParams );

    // Misc graph params
    SaveShoulderBoxSize( ShldrBoxSizeEdit->Text.ToInt() );
    SaveShowShldrCursors( ShowShldrCursorsCB->Checked );

    SaveGraphColors( RPMColorShape->Brush->Color, TorqueColorShape->Brush->Color, TensionColorShape->Brush->Color );

    // RPM averaging
    SaveRPMAvgingValue( RPMSamplesEdit->Text.ToInt() );
}


void __fastcall TGraphSettingForm::ChangeRPMColorBtnClick(TObject *Sender)
{
    LineColorDlg->Color = RPMColorShape->Brush->Color;

    if( LineColorDlg->Execute() )
        RPMColorShape->Brush->Color = LineColorDlg->Color;
}


void __fastcall TGraphSettingForm::ChangeTorqueColorBtnClick(TObject *Sender)
{
    LineColorDlg->Color = TorqueColorShape->Brush->Color;

    if( LineColorDlg->Execute() )
        TorqueColorShape->Brush->Color = LineColorDlg->Color;
}


void __fastcall TGraphSettingForm::ChangeTensionColorBtnClick(TObject *Sender)
{
    LineColorDlg->Color = TensionColorShape->Brush->Color;

    if( LineColorDlg->Execute() )
        TensionColorShape->Brush->Color = LineColorDlg->Color;
}


TWinControl* TGraphSettingForm::CheckGraphPage( void )
{
    UnicodeString sInvalidValue( "You have entered an invalid value." );

    for( int iCtrl = 0; iCtrl < GraphAxisGB->ControlCount; iCtrl++ )
    {
        TEdit* anEdit = dynamic_cast<TEdit*>( GraphAxisGB->Controls[iCtrl] );

        if( ( anEdit != NULL ) && ( anEdit->Enabled ) )
        {
            if( !IsValidInt( anEdit ) )
            {
                MessageDlg( sInvalidValue, mtError, TMsgDlgButtons() << mbOK, 0 );
                return anEdit;
            }
        }
    }

    if( !IsValidInt( ShldrBoxSizeEdit ) )
    {
        MessageDlg( sInvalidValue, mtError, TMsgDlgButtons() << mbOK, 0 );
        return ShldrBoxSizeEdit;
    }

    if( !IsValidInt( RPMSamplesEdit ) )
    {
        MessageDlg( sInvalidValue, mtError, TMsgDlgButtons() << mbOK, 0 );
        return RPMSamplesEdit;
    }

    return NULL;
}


void __fastcall TGraphSettingForm::AutoCalSpansCBClick(TObject *Sender)
{
    if( AutoCalSpansCB->Checked )
    {
        TqAxisSpanEdit->Enabled = false;
        TqAxisSpanEdit->Color   = clBtnFace;
    }
    else
    {
        TqAxisSpanEdit->Enabled = true;
        TqAxisSpanEdit->Color   = clWindow;
    }
}

