object GraphSettingForm: TGraphSettingForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Graph Settings'
  ClientHeight = 540
  ClientWidth = 356
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object OKBtn: TButton
    Left = 186
    Top = 508
    Width = 75
    Height = 25
    Caption = 'O&K'
    Default = True
    TabOrder = 3
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 271
    Top = 508
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object GraphMiscGB: TGroupBox
    Left = 8
    Top = 8
    Width = 340
    Height = 185
    Caption = ' Miscellaneous Graph Settings '
    TabOrder = 0
    object TorqueColorShape: TShape
      Left = 157
      Top = 95
      Width = 73
      Height = 8
      Brush.Color = clGreen
    end
    object RPMColorShape: TShape
      Left = 157
      Top = 62
      Width = 73
      Height = 8
      Brush.Color = clSkyBlue
    end
    object Label17: TLabel
      Left = 14
      Top = 92
      Width = 92
      Height = 13
      Caption = 'Torque Curve color'
    end
    object Label16: TLabel
      Left = 14
      Top = 59
      Width = 79
      Height = 13
      Caption = 'RPM Curve color'
    end
    object Label11: TLabel
      Left = 13
      Top = 26
      Width = 122
      Height = 13
      Caption = 'Shoulder Box size (pixels)'
    end
    object TensionColorShape: TShape
      Left = 157
      Top = 128
      Width = 73
      Height = 9
      Brush.Color = 33023
    end
    object Label1: TLabel
      Left = 14
      Top = 125
      Width = 95
      Height = 13
      Caption = 'Tension Curve color'
    end
    object ChangeTorqueColorBtn: TButton
      Left = 248
      Top = 87
      Width = 75
      Height = 25
      Caption = 'Change'
      TabOrder = 2
      OnClick = ChangeTorqueColorBtnClick
    end
    object ChangeRPMColorBtn: TButton
      Left = 248
      Top = 54
      Width = 75
      Height = 25
      Caption = 'Change'
      TabOrder = 1
      OnClick = ChangeRPMColorBtnClick
    end
    object ShowShldrCursorsCB: TCheckBox
      Left = 14
      Top = 154
      Width = 196
      Height = 17
      Caption = 'Show shoulder cursors'
      TabOrder = 3
    end
    object ShldrBoxSizeEdit: TEdit
      Left = 157
      Top = 23
      Width = 73
      Height = 21
      BiDiMode = bdLeftToRight
      NumbersOnly = True
      ParentBiDiMode = False
      TabOrder = 0
      Text = '0'
    end
    object ChangeTensionColorBtn: TButton
      Left = 248
      Top = 120
      Width = 75
      Height = 25
      Caption = 'Change'
      TabOrder = 4
      OnClick = ChangeTensionColorBtnClick
    end
  end
  object GraphAxisGB: TGroupBox
    Left = 8
    Top = 199
    Width = 340
    Height = 241
    Caption = ' Graph Axis Settings '
    TabOrder = 1
    object Label12: TLabel
      Left = 14
      Top = 26
      Width = 52
      Height = 13
      Caption = 'Axis Spans'
    end
    object Label25: TLabel
      Left = 101
      Top = 26
      Width = 34
      Height = 13
      Caption = 'Torque'
    end
    object Label26: TLabel
      Left = 101
      Top = 103
      Width = 21
      Height = 13
      Caption = 'RPM'
    end
    object Label27: TLabel
      Left = 101
      Top = 130
      Width = 27
      Height = 13
      Caption = 'Turns'
    end
    object Label28: TLabel
      Left = 101
      Top = 157
      Width = 22
      Height = 13
      Caption = 'Time'
    end
    object Label14: TLabel
      Left = 14
      Top = 184
      Width = 77
      Height = 13
      Caption = 'Axis Increments'
    end
    object Label29: TLabel
      Left = 101
      Top = 211
      Width = 22
      Height = 13
      Caption = 'Time'
    end
    object Label30: TLabel
      Left = 101
      Top = 184
      Width = 27
      Height = 13
      Caption = 'Turns'
    end
    object SecAxisSpanLB: TLabel
      Left = 240
      Top = 157
      Width = 29
      Height = 13
      Caption = '(secs)'
    end
    object SecAxisIncrLB: TLabel
      Left = 240
      Top = 211
      Width = 29
      Height = 13
      Caption = '(secs)'
    end
    object UnitAxisSpanLB: TLabel
      Left = 240
      Top = 26
      Width = 35
      Height = 13
      Caption = '([Unit])'
    end
    object Label2: TLabel
      Left = 101
      Top = 76
      Width = 37
      Height = 13
      Caption = 'Tension'
    end
    object Label3: TLabel
      Left = 240
      Top = 76
      Width = 24
      Height = 13
      Caption = '(+/-)'
    end
    object TqAxisSpanEdit: TEdit
      Left = 157
      Top = 23
      Width = 73
      Height = 21
      NumbersOnly = True
      TabOrder = 1
      Text = '0'
    end
    object RPMAxisSpanEdit: TEdit
      Left = 157
      Top = 100
      Width = 73
      Height = 21
      NumbersOnly = True
      TabOrder = 3
      Text = '0'
    end
    object TurnsAxisSpanEdit: TEdit
      Left = 157
      Top = 127
      Width = 73
      Height = 21
      NumbersOnly = True
      TabOrder = 4
      Text = '0'
    end
    object TimeAxisSpanEdit: TEdit
      Left = 157
      Top = 154
      Width = 73
      Height = 21
      NumbersOnly = True
      TabOrder = 5
      Text = '0'
    end
    object TurnsAxisIncrEdit: TEdit
      Left = 157
      Top = 181
      Width = 73
      Height = 21
      NumbersOnly = True
      TabOrder = 6
      Text = '0'
    end
    object TimeAxisIncrEdit: TEdit
      Left = 157
      Top = 208
      Width = 73
      Height = 21
      NumbersOnly = True
      TabOrder = 7
      Text = '0'
    end
    object AutoCalSpansCB: TCheckBox
      Left = 101
      Top = 50
      Width = 140
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Auto Scale Torque Span'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = AutoCalSpansCBClick
    end
    object TenAxisSpanEdit: TEdit
      Left = 157
      Top = 73
      Width = 73
      Height = 21
      NumbersOnly = True
      TabOrder = 2
      Text = '0'
    end
  end
  object RPMAvgGB: TGroupBox
    Left = 8
    Top = 446
    Width = 340
    Height = 54
    Caption = ' RPM Averaging '
    TabOrder = 2
    object Label15: TLabel
      Left = 14
      Top = 24
      Width = 149
      Height = 13
      Caption = 'Number of Samples to Average'
    end
    object RPMSamplesEdit: TEdit
      Left = 221
      Top = 21
      Width = 73
      Height = 21
      NumbersOnly = True
      TabOrder = 0
      Text = '1'
    end
  end
  object LineColorDlg: TColorDialog
    Options = [cdFullOpen]
    Left = 61
    Top = 488
  end
end
