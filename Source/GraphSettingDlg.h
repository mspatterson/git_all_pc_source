#ifndef GraphSettingDlgH
#define GraphSettingDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include "UnitsOfMeasure.h"


class TGraphSettingForm : public TForm
{
__published:    // IDE-managed Components
    TGroupBox *GraphMiscGB;
    TShape *TorqueColorShape;
    TShape *RPMColorShape;
    TLabel *Label17;
    TLabel *Label16;
    TLabel *Label11;
    TButton *ChangeTorqueColorBtn;
    TButton *ChangeRPMColorBtn;
    TCheckBox *ShowShldrCursorsCB;
    TEdit *ShldrBoxSizeEdit;
    TGroupBox *GraphAxisGB;
    TLabel *Label12;
    TLabel *Label25;
    TLabel *Label26;
    TLabel *Label27;
    TLabel *Label28;
    TLabel *Label14;
    TLabel *Label29;
    TLabel *Label30;
    TEdit *TqAxisSpanEdit;
    TEdit *RPMAxisSpanEdit;
    TEdit *TurnsAxisSpanEdit;
    TEdit *TimeAxisSpanEdit;
    TEdit *TurnsAxisIncrEdit;
    TEdit *TimeAxisIncrEdit;
    TGroupBox *RPMAvgGB;
    TLabel *Label15;
    TEdit *RPMSamplesEdit;
    TButton *OKBtn;
    TButton *CancelBtn;
    TColorDialog *LineColorDlg;
    TLabel *SecAxisSpanLB;
    TLabel *SecAxisIncrLB;
    TLabel *UnitAxisSpanLB;
    TCheckBox *AutoCalSpansCB;
    TLabel *Label1;
    TShape *TensionColorShape;
    TButton *ChangeTensionColorBtn;
    TEdit *TenAxisSpanEdit;
    TLabel *Label2;
    TLabel *Label3;
    void __fastcall ChangeRPMColorBtnClick(TObject *Sender);
    void __fastcall ChangeTorqueColorBtnClick(TObject *Sender);
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall ChangeTensionColorBtnClick(TObject *Sender);
    void __fastcall AutoCalSpansCBClick(TObject *Sender);

private:    // User declarations
    UNITS_OF_MEASURE m_uomType;

    void LoadGraphPage( void );

    TWinControl* CheckGraphPage( void );

    // All controls must be valid when calling the following 'Save' functions
    void SaveGraphPage( void );

public:     // User declarations
    __fastcall TGraphSettingForm(TComponent* Owner);

    __property UNITS_OF_MEASURE UnitsOfMeasure = { write = m_uomType };

    // This is not an auto-created form. Must call the static ShowDlg() method
    // to construct, show, and then destroy the dialog.
    static bool ShowDlg( UNITS_OF_MEASURE uomType );
};

extern PACKAGE TGraphSettingForm *GraphSettingForm;

#endif
