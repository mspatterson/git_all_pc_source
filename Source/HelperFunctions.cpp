#include <vcl.h>
#pragma hdrstop

#include "HelperFunctions.h"

#pragma package(smart_init)


//
// Common Helper Functions
//

UnicodeString TurnsToText( float turnsValue )
{
    return FloatToStrF( turnsValue, ffFixed, 7, 4 );
}


void DrawSimpleHzDashedLine( TCanvas3D* pCanvas, const DASHED_LINE_PARAMS& params, int xStart, int xEnd, int yPos )
{
    if( pCanvas == NULL )
        return;

    pCanvas->Pen->Color = params.color;
    pCanvas->Pen->Width = params.width;
    pCanvas->Pen->Style = psSolid;

    int xCurr = xStart;

    while( xCurr < xEnd )
    {
        int nextEnd = xCurr + params.length;

        if( nextEnd > xEnd )
            nextEnd = xEnd;

        pCanvas->MoveTo( xCurr,   yPos );
        pCanvas->LineTo( nextEnd, yPos );

        // Make next position start after dashLength of blank space
        xCurr = nextEnd + params.length;
    }
}

