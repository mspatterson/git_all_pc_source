#ifndef HelperFunctionsH
#define HelperFunctionsH

#include <VCLTee.TeEngine.hpp>

    //
    // Common Helper Functions
    //

    UnicodeString TurnsToText( float turnsValue );

    typedef struct
    {
        TColor color;
        int    width;
        int    length;
    } DASHED_LINE_PARAMS;

    void DrawSimpleHzDashedLine( TCanvas3D* pCanvas, const DASHED_LINE_PARAMS& params, int xStart, int xEnd, int yPos );

#endif
