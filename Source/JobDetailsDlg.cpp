#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>

#include "JobDetailsDlg.h"
#include "RegistryInterface.h"
#include "HelperFunctions.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TJobDetailsForm *JobDetailsForm;


// Certain labels on this form have units appended to their text. The following
// structure defines the base text for each label and its type of measure.
typedef struct {
    TLabel*       pLabel;
    UnicodeString baseText;
    MEASURE_TYPE  mtType;
} MEASURED_LABEL_INFO;

typedef enum {
    ML_CASING_DIA,
    ML_CASING_WEIGHT,
    ML_AUTO_SHLDR_MAX_T,
    ML_AUTO_SHLDR_MIN_T,
    ML_PTT_MAX_T,
    ML_PTT_MIN_T,
    ML_PTT_OPT_T,
    ML_PTT_OVERSHOOT,
    NBR_MEASURED_LABELS
} MEASURED_LABEL;

MEASURED_LABEL_INFO mlInfo[NBR_MEASURED_LABELS] = {
    // Enum                      Label   baseText               mtType
  { /* ML_CASING_DIA        */   NULL,   L"Diameter ",           MT_DIST2_SHORT             },
  { /* ML_CASING_WEIGHT     */   NULL,   L"Weight ",             MT_WEIGHT_BY_LENGTH_SHORT  },
  { /* ML_AUTO_SHLDR_MAX_T  */   NULL,   L"Max Torque ",         MT_TORQUE_SHORT            },
  { /* ML_AUTO_SHLDR_MIN_T  */   NULL,   L"Min Torque ",         MT_TORQUE_SHORT            },
  { /* ML_PTT_MAX_T         */   NULL,   L"Max Torque ",         MT_TORQUE_SHORT            },
  { /* ML_PTT_MIN_T         */   NULL,   L"Min Torque ",         MT_TORQUE_SHORT            },
  { /* ML_PTT_OPT_T         */   NULL,   L"Opt Torque ",         MT_TORQUE_SHORT            },
  { /* ML_PTT_OVERSHOOT     */   NULL,   L"Overshoot ",          MT_TORQUE_SHORT            },
};


//
// Class Functions
//

__fastcall TJobDetailsForm::TJobDetailsForm(TComponent* Owner) : TForm(Owner)
{
    // Assign the labels to the measured label info array.
    mlInfo[ML_CASING_DIA       ].pLabel = CasingDiaLabel;
    mlInfo[ML_CASING_WEIGHT    ].pLabel = CasingWeightLabel;
    mlInfo[ML_AUTO_SHLDR_MAX_T ].pLabel = AutoShldrMaxTLabel;
    mlInfo[ML_AUTO_SHLDR_MIN_T ].pLabel = AutoShldrMinTLabel;
    mlInfo[ML_PTT_MAX_T        ].pLabel = PTTMaxTLabel;
    mlInfo[ML_PTT_MIN_T        ].pLabel = PTTMinTLabel;
    mlInfo[ML_PTT_OPT_T        ].pLabel = PTTOptTLabel;
    mlInfo[ML_PTT_OVERSHOOT    ].pLabel = PTTOvershootLabel;

    // Some edits are not required, while other edits must contain numeric
    // values. Assign a bit flag to each edit's tag member here defining
    // its requirements.
    #define CTRL_MANDATORY  0x0001
    #define EDIT_INTEGER    0x0002
    #define EDIT_POS_FLOAT  0x0004
    #define EDIT_FLOAT      0x0008

    ClientEdit->Tag              = CTRL_MANDATORY;
    LocnEdit->Tag                = CTRL_MANDATORY;
    RepCombo->Tag                = CTRL_MANDATORY;
    TescoTechCombo->Tag          = CTRL_MANDATORY;
    ThreadRepCombo->Tag          = CTRL_MANDATORY;
    CasingMfgEdit->Tag           = CTRL_MANDATORY;
    CasingNameEdit->Tag          = CTRL_MANDATORY;
    CasingTypeEdit->Tag          = CTRL_MANDATORY;
    CasingGradeEdit->Tag         = CTRL_MANDATORY;
    CasingDiaEdit->Tag           = CTRL_MANDATORY | EDIT_POS_FLOAT;
    CasingWeightEdit->Tag        = CTRL_MANDATORY | EDIT_POS_FLOAT;
    ShldrMaxTEdit->Tag           = CTRL_MANDATORY | EDIT_POS_FLOAT;
    ShldrPostTurnsEdit->Tag      = CTRL_MANDATORY | EDIT_POS_FLOAT;
    ShldrMinTEdit->Tag           = CTRL_MANDATORY | EDIT_POS_FLOAT;
    ShldrDeltaEdit->Tag          = CTRL_MANDATORY | EDIT_POS_FLOAT;
    PTTMaxTEdit->Tag             = CTRL_MANDATORY | EDIT_POS_FLOAT;
    PTTTurnsEdit->Tag            = CTRL_MANDATORY | EDIT_POS_FLOAT;
    PTTOptTEdit->Tag             = CTRL_MANDATORY | EDIT_POS_FLOAT;
    PTTDeltaEdit->Tag            = CTRL_MANDATORY | EDIT_POS_FLOAT;
    PTTMinTEdit->Tag             = CTRL_MANDATORY | EDIT_POS_FLOAT;
    PTTHoldEdit->Tag             = CTRL_MANDATORY | EDIT_POS_FLOAT;
    PTTOvershootEdit->Tag        = CTRL_MANDATORY | EDIT_POS_FLOAT;

    // Default for editing is false
    m_bIsEdit = false;
}


void __fastcall TJobDetailsForm::CreateParams(Controls::TCreateParams &Params)
{
    TForm::CreateParams(Params);
    Params.ExStyle   = Params.ExStyle | WS_EX_APPWINDOW;
    Params.WndParent = ParentWindow;
}


bool TJobDetailsForm::CreateNewJob( const NEW_JOB_KEY& newJobInfo, UnicodeString& jobFileName )
{
    // Presents the job details dialog and creates a new job if all entered params are legit.
    // Returns true on success, populating jobName with the new job's filename.

    // No job currently being viewed
    m_currJob = NULL;

    // First, clear all edits
    ClearGBEdits( DetailsGB );
    ClearGBEdits( CasingGB );
    ClearGBEdits( ShoulderGB );
    ClearGBEdits( PeakTorqueGB );

    // All group boxes are enabled when creating a new job
    EnableGBControls( DetailsGB,    true );
    EnableGBControls( CasingGB,     true );
    EnableGBControls( ShoulderGB,   true );
    EnableGBControls( PeakTorqueGB, true );

    // Disable Delete for creating a new job
    DeleteSectBtn->Enabled = false;

    // Okay and cancel btns are visible when creating a new job
    OKBtn->Visible     = true;
    CancelBtn->Caption = L"Cancel";

    // Populate client and location fields - these are read-only fields
    ClientEdit->Text = newJobInfo.clientName;
    LocnEdit->Text   = newJobInfo.location;

    // Populate name drop-downlists
    PopulateRepCombos();

    // Default pipe has a shoulder
    ShoulderlessCB->Checked = false;
    ShoulderlessCBClick( ShoulderlessCB );

    // Default is auto-shoulder mode
    AutoShldrEnabledCB->Checked = true;

    // Can only auto-record if we have been given casings
    if( newJobInfo.sCasingLengths.Length() > 0 )
    {
        AutoRecordCB->Enabled = true;
        AutoRecordCB->Checked = false;
    }
    else
    {
        AutoRecordCB->Enabled = false;
        AutoRecordCB->Checked = false;
    }

    // Update the 'measured' labels.
    for( int iLabel = 0; iLabel < NBR_MEASURED_LABELS; iLabel++ )
        mlInfo[iLabel].pLabel->Caption = mlInfo[iLabel].baseText + GetUnitsText( newJobInfo.unitsOfMeasure, mlInfo[iLabel].mtType );

    // Update the caption
    Caption = L"Create New Job";

    ActiveControl = RepCombo;

    // Default for editing is false
    m_bIsEdit = false;

    // Show the dialog. Validation is performed on the OK btn click.
    if( ShowModal() != mrOk )
        return false;

    // Ok was pressed, and all params are valid. Try to create the job.
    JOB_INFO     jobInfo;
    SECTION_INFO sectionInfo;

    jobInfo.clientName       = newJobInfo.clientName;
    jobInfo.location         = newJobInfo.location;
    jobInfo.firstConnNbr     = newJobInfo.firstConnNbr;
    jobInfo.sCasingsFileName = newJobInfo.sCasingFileName;
    jobInfo.sCasingsList     = newJobInfo.sCasingLengths;
    jobInfo.unitsOfMeasure   = newJobInfo.unitsOfMeasure;
    jobInfo.jobStartTime     = Now();

    GetSectionInfo( sectionInfo );

    if( ::CreateNewJob( jobFileName, GetJobDataDir(), jobInfo, sectionInfo ) )
        return true;

    MessageDlg( "The job could not be created. Ensure you have access to the job data directory.", mtError, TMsgDlgButtons() << mbOK, 0 );

    return false;
}


bool TJobDetailsForm::AddSection( TJob* currJob )
{
    // Adds a new section to the current job
    if( currJob == NULL )
        return false;

    // Set the current job being viewed
    m_currJob = currJob;

    // Update the caption
    Caption = L"Add New Section";

    // All group boxes are enabled when adding a section
    EnableGBControls( DetailsGB,    true );
    EnableGBControls( CasingGB,     true );
    EnableGBControls( ShoulderGB,   true );
    EnableGBControls( PeakTorqueGB, true );

    // Disable Delete for creating a new section
    DeleteSectBtn->Enabled = false;

    // Okay and cancel btns are visible when adding a section
    OKBtn->Visible     = true;
    CancelBtn->Caption = L"Cancel";

    // Update the 'measured' labels.
    for( int iLabel = 0; iLabel < NBR_MEASURED_LABELS; iLabel++ )
        mlInfo[iLabel].pLabel->Caption = mlInfo[iLabel].baseText + GetUnitsText( currJob->Units, mlInfo[iLabel].mtType );

    // Populate the dialog with the last section's settings
    JOB_INFO     jobInfo;
    SECTION_INFO aSection;

    if( !currJob->GetSectionRec( currJob->NbrSections - 1, aSection ) )
        return false;

    currJob->GetJobInfo( jobInfo );

    PopulateJobInfo( jobInfo );
    PopulateRepCombos();
    PopulateSectionInfo( aSection );

    ShoulderlessCBClick( ShoulderlessCB );

    GetSectionInfo( m_savedSection );

    // Creating a new section, editing is false
    m_bIsEdit = false;

    if( ShowModal() != mrOk )
        return false;

    GetSectionInfo( aSection );

    currJob->AddSectionRec( aSection );

    return true;
}


bool TJobDetailsForm::EditSection( TJob* currJob, SECTION_INFO& siInfo )
{
    // Adds a new section to the current job
    if( currJob == NULL )
        return false;

    // Set the current job being viewed
    m_currJob = currJob;

    // Edits the passed section. First, update the caption
    Caption = L"Edit Section " + IntToStr( siInfo.sectionNbr );

    // All group boxes are enabled when adding a section
    EnableGBControls( DetailsGB,    true );
    EnableGBControls( CasingGB,     true );
    EnableGBControls( ShoulderGB,   true );
    EnableGBControls( PeakTorqueGB, true );

    // Check if we should enable the Delete button
    if( currJob->CanRemoveSection( siInfo.sectionNbr ) )
        DeleteSectBtn->Enabled = true;
    else
        DeleteSectBtn->Enabled = false;

    // Okay and cancel btns are visible when editing a section
    OKBtn->Visible     = true;
    CancelBtn->Caption = L"Cancel";

    // Update the 'measured' labels.
    for( int iLabel = 0; iLabel < NBR_MEASURED_LABELS; iLabel++ )
        mlInfo[iLabel].pLabel->Caption = mlInfo[iLabel].baseText + GetUnitsText( currJob->Units, mlInfo[iLabel].mtType );

    // Populate the dialog with this section's settings
    JOB_INFO jobInfo;
    currJob->GetJobInfo( jobInfo );

    PopulateJobInfo( jobInfo );
    PopulateRepCombos();
    PopulateSectionInfo( siInfo );

    ShoulderlessCBClick( ShoulderlessCB );

    // Editing a section, editing is true
    m_bIsEdit = true;

    if( ShowModal() != mrOk )
        return false;

    GetSectionInfo( siInfo );

    currJob->EditSectionRec( siInfo );

    return true;
}


bool TJobDetailsForm::ViewSection( TJob* currJob, int whichSection )
{
    // Adds a new section to the current job
    if( currJob == NULL )
        return false;

    // Set the current job being viewed
    m_currJob = currJob;

    // Views a section's info. If whichSection is <= 0, shows the current section.
    if( whichSection <= 0 )
        whichSection = currJob->CurrSectIndex + 1;

    // Update the caption
    Caption = L"View Section " + IntToStr( whichSection );

    // Update the 'measured' labels.
    for( int iLabel = 0; iLabel < NBR_MEASURED_LABELS; iLabel++ )
        mlInfo[iLabel].pLabel->Caption = mlInfo[iLabel].baseText + GetUnitsText( currJob->Units, mlInfo[iLabel].mtType );

    // All group boxes are disabled when viewing
    EnableGBControls( DetailsGB,    false );
    EnableGBControls( CasingGB,     false );
    EnableGBControls( ShoulderGB,   false );
    EnableGBControls( PeakTorqueGB, false );

    // Disable Delete when viewing
    DeleteSectBtn->Enabled = false;

    // Okay button is hidden when viewing
    OKBtn->Visible     = false;
    CancelBtn->Caption = L"Close";

    JOB_INFO     jobInfo;
    SECTION_INFO currSection;

    if( !currJob->GetSectionRec( whichSection - 1, currSection ) )
        return false;

    currJob->GetJobInfo( jobInfo );

    PopulateJobInfo( jobInfo );
    PopulateSectionInfo( currSection );

    ShowModal();

    return true;
}


void TJobDetailsForm::PopulateJobInfo( const JOB_INFO& jobInfo )
{
    ClientEdit->Text = jobInfo.clientName;
    LocnEdit->Text   = jobInfo.location;
}


void TJobDetailsForm::PopulateRepCombos( void )
{
    TStringList* nameList = new TStringList();

    GetRecentNameList( RNL_CLIENT_REP, nameList );
    nameList->Sorted = true;
    nameList->Sort();

    RepCombo->Items->Assign( nameList );

    GetRecentNameList( RNL_TESCO_REP, nameList );
    nameList->Sorted = true;
    nameList->Sort();

    TescoTechCombo->Items->Assign( nameList );

    GetRecentNameList( RNL_THREAD_REP, nameList );
    nameList->Sorted = true;
    nameList->Sort();

    ThreadRepCombo->Items->Assign( nameList );

    delete nameList;

    // Clear any existing text from the combos
    RepCombo->Text       = "";
    TescoTechCombo->Text = "";
    ThreadRepCombo->Text = "";
}


void TJobDetailsForm::PopulateSectionInfo( const SECTION_INFO& siInfo )
{
    RepCombo->Text         = siInfo.clientRep;
    TescoTechCombo->Text   = siInfo.tescoTech;
    ThreadRepCombo->Text   = siInfo.threadRep;

    CasingMfgEdit->Text    = siInfo.casingMfgr;
    CasingNameEdit->Text   = siInfo.casingName;
    CasingTypeEdit->Text   = siInfo.casingType;
    CasingGradeEdit->Text  = siInfo.casingGrade;
    CasingDiaEdit->Text    = FloatToStrF( siInfo.casingDia,    ffFixed, 7, 2 );
    CasingWeightEdit->Text = FloatToStrF( siInfo.casingWeight, ffFixed, 7, 2 );

    ShoulderlessCB->Checked = !siInfo.hasShoulder;

    if( m_currJob->CanAutoRecord )
    {
        AutoRecordCB->Enabled = true;
        AutoRecordCB->Checked = siInfo.autoRecordOn;
    }
    else
    {
        AutoRecordCB->Enabled = false;
        AutoRecordCB->Checked = false;
    }

    ShldrMinTEdit->Text      = FloatToStrF( siInfo.shoulderMinTorque, ffFixed, 7, 2 );
    ShldrMaxTEdit->Text      = FloatToStrF( siInfo.shoulderMaxTorque, ffFixed, 7, 2 );
    ShldrPostTurnsEdit->Text = TurnsToText( siInfo.shoulderPostTurns );
    ShldrDeltaEdit->Text     = TurnsToText( siInfo.shoulderDelta );

    AutoShldrEnabledCB->Checked   = siInfo.autoShldrEnabled;

    PTTMinTEdit->Text      = FloatToStrF( siInfo.peakTargetMinTorque, ffFixed, 7, 2 );
    PTTMaxTEdit->Text      = FloatToStrF( siInfo.peakTargetMaxTorque, ffFixed, 7, 2 );
    PTTOptTEdit->Text      = FloatToStrF( siInfo.peakTargetOptTorque, ffFixed, 7, 2 );
    PTTHoldEdit->Text      = FloatToStrF( siInfo.peakTargetHold,      ffFixed, 7, 2 );
    PTTOvershootEdit->Text = FloatToStrF( siInfo.peakTargetOvershoot, ffFixed, 7, 2 );
    PTTTurnsEdit->Text     = TurnsToText( siInfo.peakTargetTurns );
    PTTDeltaEdit->Text     = TurnsToText( siInfo.peakTargetDelta );
}


void TJobDetailsForm::GetSectionInfo( SECTION_INFO& siInfo )
{
    siInfo.clientRep = RepCombo->Text.Trim();
    siInfo.tescoTech = TescoTechCombo->Text.Trim();
    siInfo.threadRep = ThreadRepCombo->Text.Trim();

    SaveRecentName( RNL_CLIENT_REP, siInfo.clientRep );
    SaveRecentName( RNL_TESCO_REP,  siInfo.tescoTech );
    SaveRecentName( RNL_THREAD_REP, siInfo.threadRep );

    siInfo.casingMfgr   = CasingMfgEdit->Text.Trim();
    siInfo.casingName   = CasingNameEdit->Text.Trim();
    siInfo.casingType   = CasingTypeEdit->Text.Trim();
    siInfo.casingGrade  = CasingGradeEdit->Text.Trim();
    siInfo.casingDia    = CasingDiaEdit->Text.ToDouble();
    siInfo.casingWeight = CasingWeightEdit->Text.ToDouble();

    siInfo.hasShoulder = !ShoulderlessCB->Checked;

    if( AutoRecordCB->Enabled )
        siInfo.autoRecordOn = AutoRecordCB->Checked;
    else
        siInfo.autoRecordOn = false;

    // Shoulder edits may be disabled and therefore not validated.
    // So use StrToFloatDef() on those edit.
    siInfo.shoulderMinTorque = StrToFloatDef( ShldrMinTEdit->Text,      0.0 );
    siInfo.shoulderMaxTorque = StrToFloatDef( ShldrMaxTEdit->Text,      0.0 );
    siInfo.shoulderPostTurns = StrToFloatDef( ShldrPostTurnsEdit->Text, 0.0 );
    siInfo.shoulderDelta     = StrToFloatDef( ShldrDeltaEdit->Text,     0.0 );

    // Auto-shoulder edits may be disabled and therefore not validated.
    // So use StrToFloatDef() on those edit.
    siInfo.autoShldrEnabled     = AutoShldrEnabledCB->Checked;

    siInfo.peakTargetMinTorque = PTTMinTEdit->Text.ToDouble();
    siInfo.peakTargetMaxTorque = PTTMaxTEdit->Text.ToDouble();
    siInfo.peakTargetOptTorque = PTTOptTEdit->Text.ToDouble();
    siInfo.peakTargetTurns     = PTTTurnsEdit->Text.ToDouble();
    siInfo.peakTargetDelta     = PTTDeltaEdit->Text.ToDouble();
    siInfo.peakTargetHold      = PTTHoldEdit->Text.ToDouble();
    siInfo.peakTargetOvershoot = PTTOvershootEdit->Text.ToDouble();
}


bool TJobDetailsForm::SectionHasChanged( void )
{
    SECTION_INFO newSection;
    GetSectionInfo( newSection );

    if( newSection.clientRep != m_savedSection.clientRep )
        return true;

    if( newSection.tescoTech != m_savedSection.tescoTech )
        return true;

    if( newSection.threadRep != m_savedSection.threadRep )
        return true;

    if( newSection.casingMfgr != m_savedSection.casingMfgr )
        return true;

    if( newSection.casingName != m_savedSection.casingName )
        return true;

    if( newSection.casingType != m_savedSection.casingType )
        return true;

    if( newSection.casingGrade != m_savedSection.casingGrade )
        return true;

    if( newSection.casingDia != m_savedSection.casingDia )
        return true;

    if( newSection.casingWeight != m_savedSection.casingWeight )
        return true;

    if( newSection.hasShoulder != m_savedSection.hasShoulder )
        return true;

    if( newSection.shoulderMinTorque != m_savedSection.shoulderMinTorque )
        return true;

    if( newSection.shoulderMaxTorque != m_savedSection.shoulderMaxTorque )
        return true;

    if( newSection.shoulderPostTurns != m_savedSection.shoulderPostTurns )
        return true;

    if( newSection.shoulderDelta != m_savedSection.shoulderDelta )
        return true;

    if( newSection.autoShldrEnabled != m_savedSection.autoShldrEnabled )
        return true;

    if( newSection.peakTargetMinTorque != m_savedSection.peakTargetMinTorque )
        return true;

    if( newSection.peakTargetMaxTorque != m_savedSection.peakTargetMaxTorque )
        return true;

    if( newSection.peakTargetOptTorque != m_savedSection.peakTargetOptTorque )
        return true;

    if( newSection.peakTargetTurns != m_savedSection.peakTargetTurns )
        return true;

    if( newSection.peakTargetDelta != m_savedSection.peakTargetDelta )
        return true;

    if( newSection.peakTargetHold != m_savedSection.peakTargetHold )
        return true;

    if( newSection.peakTargetOvershoot != m_savedSection.peakTargetOvershoot )
        return true;

    if( newSection.autoRecordOn != m_savedSection.autoRecordOn )
        return true;

    // Fall through means nothing has changed
    return false;
}


void TJobDetailsForm::ClearGBEdits( TGroupBox* aGB )
{
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
    {
        TEdit* currEdit = dynamic_cast<TEdit*>( aGB->Controls[iCtrl] );

        if( currEdit != NULL )
        {
            currEdit->Text = L"";
            continue;
        }

        // This function also clears combo boxes
        TComboBox* currCombo = dynamic_cast<TComboBox*>( aGB->Controls[iCtrl] );

        if( currCombo != NULL )
        {
            currCombo->Items->Clear();
            currCombo->Text = L"";
        }
    }
}


void TJobDetailsForm::EnableGBControls( TGroupBox* aGB, bool isEnabled )
{
    // Handle special case for DetailsGB
    if( aGB == DetailsGB )
    {
        RepCombo->Enabled       = isEnabled;
        TescoTechCombo->Enabled = isEnabled;
        ThreadRepCombo->Enabled = isEnabled;
    }
    else
    {
        for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
            aGB->Controls[iCtrl]->Enabled = isEnabled;
    }
}


TWinControl* TJobDetailsForm::FindEmptyGBCtrl( TGroupBox* aGB )
{
    // Look for an empty edit or combo in the group box. Ignore
    // controls that are not mandatory or ones that are disabled
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
    {
        if( !aGB->Controls[iCtrl]->Enabled )
            continue;

        if( ( aGB->Controls[iCtrl]->Tag & CTRL_MANDATORY ) != 0 )
        {
            TEdit* currEdit = dynamic_cast<TEdit*>( aGB->Controls[iCtrl] );

            if( ( currEdit != NULL ) && ( currEdit->Enabled ) )
            {
                if( currEdit->Text.Trim().Length() == 0 )
                    return currEdit;
            }

            TComboBox* currCombo = dynamic_cast<TComboBox*>( aGB->Controls[iCtrl] );

            if( ( currCombo != NULL ) && ( currCombo->Enabled ) )
            {
                if( currCombo->Text.Trim().Length() == 0 )
                    return currCombo;
            }
        }
    }

     return NULL;
}


TWinControl* TJobDetailsForm::FindBadGBFloat( TGroupBox* aGB )
{
    // Validate edits that represent floats
    for( int iCtrl = 0; iCtrl < aGB->ControlCount; iCtrl++ )
    {
        TEdit* currEdit = dynamic_cast<TEdit*>( aGB->Controls[iCtrl] );

        if( ( currEdit != NULL ) && ( currEdit->Enabled ) )
        {
            if( ( currEdit->Tag & EDIT_POS_FLOAT ) != 0 )
            {
                try
                {
                    if( currEdit->Text.ToDouble() < 0.0 )
                        Abort();
                }
                catch( ... )
                {
                    return currEdit;
                }
            }

            if( ( currEdit->Tag & EDIT_FLOAT ) != 0 )
            {
                try
                {
                    float testValue = currEdit->Text.ToDouble();
                }
                catch( ... )
                {
                    return currEdit;
                }
            }
        }
    }

    return NULL;
}


void __fastcall TJobDetailsForm::OKBtnClick(TObject *Sender)
{
    // Validate fields. If all are good, then set modal result to mrOk

    // First, no edits can contain blank values
    TWinControl* blankCtrl = FindEmptyGBCtrl( DetailsGB );

    if( blankCtrl == NULL )
        blankCtrl = FindEmptyGBCtrl( CasingGB );

    if( blankCtrl == NULL )
        blankCtrl = FindEmptyGBCtrl( ShoulderGB );

    if( blankCtrl == NULL )
        blankCtrl = FindEmptyGBCtrl( PeakTorqueGB );

    if( blankCtrl != NULL )
    {
        MessageDlg( "Field cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        if( blankCtrl->Enabled )
            ActiveControl = blankCtrl;

        return;
    }

    // Validate numeric fields next
    TWinControl* badCtrl = FindBadGBFloat( CasingGB );

    if( badCtrl == NULL )
        badCtrl = FindBadGBFloat( ShoulderGB );

    if( badCtrl == NULL )
        badCtrl = FindBadGBFloat( PeakTorqueGB );

    if( badCtrl != NULL )
    {
        MessageDlg( "You have entered an invalid value.", mtError, TMsgDlgButtons() << mbOK, 0 );

        if( badCtrl->Enabled )
            ActiveControl = badCtrl;

        return;
    }

    // Finally, validate field values relative to each other
    if( !ShoulderlessCB->Checked )
    {
        float tqMax = ShldrMaxTEdit->Text.ToDouble();
        float tqMin = ShldrMinTEdit->Text.ToDouble();

        if( ( tqMax > 0.0 ) && ( tqMin > 0.0 ) )
        {
            if( tqMax < tqMin )
            {
                MessageDlg( "Max torque value must be greater than min torque value.", mtError, TMsgDlgButtons() << mbOK, 0 );

                if( ShldrMaxTEdit->Enabled )
                    ActiveControl = ShldrMaxTEdit;

                return;
            }
        }
    }

    if( AutoRecordCB->Checked )
    {
        // Auto-record is checked. If this section has a shoulder, then
        // auto-shoulder must be enabled
        if( !AutoShldrEnabledCB->Checked && !ShoulderlessCB->Checked )
        {
            MessageDlg( "To enable auto-recording you must either be using shoulderless casings or you must enable auto-shoulder detection.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return;
        }
    }

    float tqMax = PTTMaxTEdit->Text.ToDouble();
    float tqMin = PTTMinTEdit->Text.ToDouble();
    float tqOpt = PTTOptTEdit->Text.ToDouble();

    if( tqMax < tqOpt )
    {
        MessageDlg( "Max torque value must be greater than optimum torque value.", mtError, TMsgDlgButtons() << mbOK, 0 );

        if( PTTMaxTEdit->Enabled )
            ActiveControl = PTTMaxTEdit;

        return;
    }

    if( tqOpt < tqMin )
    {
        MessageDlg( "Optimum torque value must be greater than min torque value.", mtError, TMsgDlgButtons() << mbOK, 0 );

        if( PTTMinTEdit->Enabled )
            ActiveControl = PTTMinTEdit;

        return;
    }

    // Check if any section values have changed
    if( !SectionHasChanged() )
    {
        if( m_bIsEdit && DeleteSectBtn->Enabled )
        {
            // If we're editing, give them the option to delete
            int iResult = MessageDlg( "The current section is no longer different than the previous, do you wish to delete it?"
                                      "\r  Press Yes to delete this section."
                                      "\r  Press Cancel to reconsider.",
                                       mtError, TMsgDlgButtons() << mbYes << mbCancel , 0, mbCancel );

            // If the user wants to delete, ensure they can, and attempt to delete
            if( iResult == mrYes )
                DeleteSectBtnClick( DeleteSectBtn );
        }
        else
        {
            // Attempt to save the section as the same as the last section
            MessageDlg( "You need to change at least one section value from the previous section.", mtError, TMsgDlgButtons() << mbOK, 0 );
        }

        return;
    }

    // Fall through means values are good
    ModalResult = mrOk;
}


void __fastcall TJobDetailsForm::ShoulderlessCBClick(TObject *Sender)
{
    // Toggle the state of the Auto-Shoulder checkbox. Note that if the
    // check box is disabled, we have to disable all other shoulder controls
    if( ShoulderlessCB->Enabled && !ShoulderlessCB->Checked )
    {
        ShldrMaxTEdit->Enabled      = true;
        ShldrMinTEdit->Enabled      = true;
        ShldrDeltaEdit->Enabled     = true;
        ShldrPostTurnsEdit->Enabled = true;

        ShldrMaxTEdit->Color        = clWindow;
        ShldrMinTEdit->Color        = clWindow;
        ShldrDeltaEdit->Color       = clWindow;
        ShldrPostTurnsEdit->Color   = clWindow;

        AutoShldrEnabledCB->Enabled = true;
    }
    else
    {
        ShldrMaxTEdit->Enabled      = false;
        ShldrMinTEdit->Enabled      = false;
        ShldrDeltaEdit->Enabled     = false;
        ShldrPostTurnsEdit->Enabled = false;

        ShldrMaxTEdit->Color        = clBtnFace;
        ShldrMinTEdit->Color        = clBtnFace;
        ShldrDeltaEdit->Color       = clBtnFace;
        ShldrPostTurnsEdit->Color   = clBtnFace;

        AutoShldrEnabledCB->Enabled = false;
    }
}


void __fastcall TJobDetailsForm::DeleteSectBtnClick( TObject *Sender )
{
    // Don't use a null job
    if( m_currJob == NULL )
        return;

    // Attempt to delete the current section - Warn first
    // Show the Confirmation Dialog
    int mrResult = MessageDlg( "You are about to delete this section, are you sure you wish to continue?"
                               "\r  Press Yes to continue and delete this section."
                               "\r  Press Cancel to reconsider. ",
                               mtWarning, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    // If the result is bad, stop deletion
    if( mrResult != mrYes )
        return;

    m_currJob->RemoveLastSection();

    // Let the dialog close nicely
    ModalResult = mrOk;
}
