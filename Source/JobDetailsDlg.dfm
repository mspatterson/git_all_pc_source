object JobDetailsForm: TJobDetailsForm
  Left = 0
  Top = 0
  Caption = 'Job Configuration'
  ClientHeight = 389
  ClientWidth = 727
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    727
    389)
  PixelsPerInch = 96
  TextHeight = 13
  object CasingGB: TGroupBox
    Left = 8
    Top = 119
    Width = 234
    Height = 228
    Caption = ' Casing '
    TabOrder = 1
    object Label7: TLabel
      Left = 12
      Top = 20
      Width = 65
      Height = 13
      Caption = 'Manufacturer'
    end
    object Label8: TLabel
      Left = 12
      Top = 60
      Width = 27
      Height = 13
      Caption = 'Name'
    end
    object Label9: TLabel
      Left = 12
      Top = 100
      Width = 24
      Height = 13
      Caption = 'Type'
    end
    object Label10: TLabel
      Left = 12
      Top = 140
      Width = 29
      Height = 13
      Caption = 'Grade'
    end
    object CasingDiaLabel: TLabel
      Left = 12
      Top = 180
      Width = 77
      Height = 13
      Caption = 'Diameter (units)'
    end
    object CasingWeightLabel: TLabel
      Left = 124
      Top = 180
      Width = 73
      Height = 13
      Caption = 'Weight (un/un)'
    end
    object CasingMfgEdit: TEdit
      Left = 12
      Top = 35
      Width = 209
      Height = 21
      TabOrder = 0
    end
    object CasingNameEdit: TEdit
      Left = 12
      Top = 75
      Width = 209
      Height = 21
      TabOrder = 1
    end
    object CasingTypeEdit: TEdit
      Left = 12
      Top = 115
      Width = 209
      Height = 21
      TabOrder = 2
    end
    object CasingGradeEdit: TEdit
      Left = 12
      Top = 155
      Width = 209
      Height = 21
      TabOrder = 3
    end
    object CasingDiaEdit: TEdit
      Left = 12
      Top = 195
      Width = 95
      Height = 21
      TabOrder = 4
    end
    object CasingWeightEdit: TEdit
      Left = 124
      Top = 195
      Width = 95
      Height = 21
      TabOrder = 5
    end
  end
  object PeakTorqueGB: TGroupBox
    Left = 487
    Top = 119
    Width = 232
    Height = 187
    Caption = ' Peak Torque Target '
    TabOrder = 4
    object PTTMaxTLabel: TLabel
      Left = 12
      Top = 20
      Width = 91
      Height = 13
      Caption = 'Max Torque (units)'
    end
    object Label18: TLabel
      Left = 124
      Top = 20
      Width = 27
      Height = 13
      Caption = 'Turns'
    end
    object PTTOptTLabel: TLabel
      Left = 11
      Top = 60
      Width = 89
      Height = 13
      Caption = 'Opt Torque (units)'
    end
    object Label20: TLabel
      Left = 124
      Top = 60
      Width = 44
      Height = 13
      Caption = '+/- Delta'
    end
    object PTTMinTLabel: TLabel
      Left = 11
      Top = 100
      Width = 87
      Height = 13
      Caption = 'Min Torque (units)'
    end
    object Label22: TLabel
      Left = 124
      Top = 100
      Width = 48
      Height = 13
      Caption = 'Hold (sec)'
    end
    object PTTOvershootLabel: TLabel
      Left = 11
      Top = 140
      Width = 85
      Height = 13
      Caption = 'Overshoot (units)'
    end
    object PTTMaxTEdit: TEdit
      Left = 12
      Top = 35
      Width = 93
      Height = 21
      TabOrder = 0
    end
    object PTTTurnsEdit: TEdit
      Left = 124
      Top = 35
      Width = 93
      Height = 21
      TabOrder = 4
    end
    object PTTOptTEdit: TEdit
      Left = 12
      Top = 75
      Width = 93
      Height = 21
      TabOrder = 1
    end
    object PTTDeltaEdit: TEdit
      Left = 124
      Top = 75
      Width = 93
      Height = 21
      TabOrder = 5
    end
    object PTTMinTEdit: TEdit
      Left = 12
      Top = 115
      Width = 93
      Height = 21
      TabOrder = 2
    end
    object PTTHoldEdit: TEdit
      Left = 124
      Top = 115
      Width = 93
      Height = 21
      TabOrder = 6
    end
    object PTTOvershootEdit: TEdit
      Left = 12
      Top = 155
      Width = 93
      Height = 21
      TabOrder = 3
    end
  end
  object ShoulderGB: TGroupBox
    Left = 251
    Top = 119
    Width = 227
    Height = 151
    Caption = ' Shoulder '
    TabOrder = 2
    object AutoShldrMaxTLabel: TLabel
      Left = 12
      Top = 42
      Width = 91
      Height = 13
      Caption = 'Max Torque (units)'
    end
    object Label13: TLabel
      Left = 123
      Top = 42
      Width = 78
      Height = 13
      Caption = 'Post Shldr Turns'
    end
    object AutoShldrMinTLabel: TLabel
      Left = 12
      Top = 82
      Width = 87
      Height = 13
      Caption = 'Min Torque (units)'
    end
    object Label15: TLabel
      Left = 123
      Top = 82
      Width = 44
      Height = 13
      Caption = '+/- Delta'
    end
    object ShldrMaxTEdit: TEdit
      Left = 12
      Top = 57
      Width = 93
      Height = 21
      TabOrder = 1
    end
    object ShldrPostTurnsEdit: TEdit
      Left = 123
      Top = 57
      Width = 93
      Height = 21
      TabOrder = 3
    end
    object ShldrMinTEdit: TEdit
      Left = 12
      Top = 97
      Width = 93
      Height = 21
      TabOrder = 2
    end
    object ShldrDeltaEdit: TEdit
      Left = 123
      Top = 97
      Width = 93
      Height = 21
      TabOrder = 4
    end
    object AutoShldrEnabledCB: TCheckBox
      Left = 12
      Top = 124
      Width = 205
      Height = 17
      Caption = 'Enable Auto Shoulder Detection'
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
    object ShoulderlessCB: TCheckBox
      Left = 12
      Top = 19
      Width = 97
      Height = 17
      Caption = 'Shoulderless'
      TabOrder = 0
      OnClick = ShoulderlessCBClick
    end
  end
  object CancelBtn: TButton
    Left = 639
    Top = 356
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 7
  end
  object OKBtn: TButton
    Left = 558
    Top = 356
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'O&K'
    TabOrder = 6
    OnClick = OKBtnClick
  end
  object DetailsGB: TGroupBox
    Left = 8
    Top = 8
    Width = 711
    Height = 105
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Job Details '
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 20
      Width = 75
      Height = 13
      Caption = 'Client Company'
    end
    object Label2: TLabel
      Left = 255
      Top = 20
      Width = 40
      Height = 13
      Caption = 'Location'
    end
    object Label4: TLabel
      Left = 12
      Top = 60
      Width = 123
      Height = 13
      Caption = 'Customer Representative'
    end
    object Label5: TLabel
      Left = 255
      Top = 60
      Width = 50
      Height = 13
      Caption = 'Technician'
    end
    object Label6: TLabel
      Left = 489
      Top = 60
      Width = 111
      Height = 13
      Caption = 'Thread Representative'
    end
    object ClientEdit: TEdit
      Left = 12
      Top = 35
      Width = 221
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
    end
    object LocnEdit: TEdit
      Left = 255
      Top = 35
      Width = 211
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
    end
    object RepCombo: TComboBox
      Left = 12
      Top = 75
      Width = 221
      Height = 21
      TabOrder = 2
    end
    object TescoTechCombo: TComboBox
      Left = 255
      Top = 75
      Width = 211
      Height = 21
      TabOrder = 3
    end
    object ThreadRepCombo: TComboBox
      Left = 488
      Top = 75
      Width = 211
      Height = 21
      TabOrder = 4
    end
  end
  object PrintBtn: TButton
    Left = 8
    Top = 356
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '&Print'
    TabOrder = 5
    Visible = False
  end
  object DeleteSectBtn: TButton
    Left = 472
    Top = 356
    Width = 80
    Height = 25
    Caption = 'Undo Change'
    Enabled = False
    TabOrder = 8
    OnClick = DeleteSectBtnClick
  end
  object AutoRecGB: TGroupBox
    Left = 251
    Top = 276
    Width = 227
    Height = 45
    Caption = ' Auto-Record '
    TabOrder = 3
    object AutoRecordCB: TCheckBox
      Left = 12
      Top = 19
      Width = 212
      Height = 17
      Caption = 'Enable Auto Recording of Connections'
      TabOrder = 0
    end
  end
end
