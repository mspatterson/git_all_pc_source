#ifndef JobDetailsDlgH
#define JobDetailsDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include "JobManager.h"


class TJobDetailsForm : public TForm
{
__published:    // IDE-managed Components
    TGroupBox *CasingGB;
    TGroupBox *PeakTorqueGB;
    TGroupBox *ShoulderGB;
    TButton *CancelBtn;
    TButton *OKBtn;
    TGroupBox *DetailsGB;
    TLabel *Label1;
    TLabel *Label2;
    TEdit *ClientEdit;
    TEdit *LocnEdit;
    TLabel *Label4;
    TLabel *Label5;
    TLabel *Label6;
    TButton *PrintBtn;
    TLabel *Label7;
    TEdit *CasingMfgEdit;
    TLabel *Label8;
    TEdit *CasingNameEdit;
    TLabel *Label9;
    TEdit *CasingTypeEdit;
    TLabel *Label10;
    TEdit *CasingGradeEdit;
    TEdit *CasingDiaEdit;
    TLabel *CasingDiaLabel;
    TLabel *CasingWeightLabel;
    TEdit *CasingWeightEdit;
    TLabel *AutoShldrMaxTLabel;
    TEdit *ShldrMaxTEdit;
    TLabel *Label13;
    TEdit *ShldrPostTurnsEdit;
    TLabel *AutoShldrMinTLabel;
    TEdit *ShldrMinTEdit;
    TLabel *Label15;
    TEdit *ShldrDeltaEdit;
    TLabel *PTTMaxTLabel;
    TEdit *PTTMaxTEdit;
    TLabel *Label18;
    TEdit *PTTTurnsEdit;
    TLabel *PTTOptTLabel;
    TEdit *PTTOptTEdit;
    TLabel *Label20;
    TEdit *PTTDeltaEdit;
    TLabel *PTTMinTLabel;
    TEdit *PTTMinTEdit;
    TLabel *Label22;
    TEdit *PTTHoldEdit;
    TLabel *PTTOvershootLabel;
    TEdit *PTTOvershootEdit;
    TComboBox *RepCombo;
    TComboBox *TescoTechCombo;
    TComboBox *ThreadRepCombo;
    TCheckBox *AutoShldrEnabledCB;
    TCheckBox *ShoulderlessCB;
    TButton *DeleteSectBtn;
    TGroupBox *AutoRecGB;
    TCheckBox *AutoRecordCB;
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall ShoulderlessCBClick(TObject *Sender);
    void __fastcall CreateParams(Controls::TCreateParams &Params);
    void __fastcall DeleteSectBtnClick(TObject *Sender);

private:
    TJob*  m_currJob;

    // Editing bool used to determine error dialog when changes were not made
    bool   m_bIsEdit;

    void   ClearGBEdits( TGroupBox* aGB );
    void   EnableGBControls( TGroupBox* aGB, bool isEnabled );
    TWinControl* FindEmptyGBCtrl( TGroupBox* aGB );
    TWinControl* FindBadGBFloat( TGroupBox* aGB );

    void   PopulateJobInfo( const JOB_INFO& jobInfo );
    void   PopulateRepCombos( void );

    void   PopulateSectionInfo( const SECTION_INFO& siInfo );
    void   GetSectionInfo( SECTION_INFO& siInfo );
    bool   SectionHasChanged( void );

    SECTION_INFO m_savedSection;

public:
    __fastcall TJobDetailsForm(TComponent* Owner);

    typedef struct {
        String clientName;
        String location;
        UNITS_OF_MEASURE unitsOfMeasure;
        int    firstConnNbr;
        String sCasingFileName;
        String sCasingLengths;   // CSV list, in units of 100's of UOM
    } NEW_JOB_KEY;

    bool CreateNewJob( const NEW_JOB_KEY& newJobInfo, UnicodeString& jobFileName );
        // Presents the job details dialog and creates a new job if all entered params are legit.
        // Returns true on success, populating jobName with the new job's filename.

    bool AddSection( TJob* currJob );
        // Adds a new section to the current job

    bool EditSection( TJob* currJob, SECTION_INFO& siInfo );
        // Edits the passed section

    bool ViewSection( TJob* currJob, int whichSection );
        // Views a section's info. If whichSection is <= 0, shows the current section.

};

extern PACKAGE TJobDetailsForm *JobDetailsForm;

#endif
