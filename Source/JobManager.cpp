#include <vcl.h>
#pragma hdrstop

#include <IOUtils.hpp>
#include <msxmldom.hpp>
#include <XMLDoc.hpp>
#include <xmldom.hpp>
#include <DateUtils.hpp>

#include "JobManager.h"
#include "RegistryInterface.h"

#pragma package(smart_init)


//
// Public Symbols
//

const String ConnResultText[NBR_CONN_RESULTS] = {
  /* CR_PASSED        */   L"Passed",
  /* CR_FAILED        */   L"Failed",
  /* CR_FORCE_PASSED  */   L"Force Passed",
  /* CR_FORCE_FAILED  */   L"Force Failed",
  /* CR_REMOVED       */   L"Removed"
};


//
// This file implements support routines for creating, enumerating,
// and updating job files.
//

static const String m_JobExt( ".wjb" );

#define CURR_JOB_FILE_VER  1000


//
// Static class variables
//

TNotifyEvent TJob::m_onPipeTallyUpdated   = NULL;
TNotifyEvent TJob::m_onConnectionsUpdated = NULL;


//
// General static functions
//

String CreateJobName( const String& client, const String& location )
{
    return client + "." + location;
}


const int m_sizeofCount   = 8;
const int m_sizeofTime    = 6;
const int m_sizeofTurns   = 8;
const int m_sizeofTorque  = 8;
const int m_sizeofTension = 8;
const int m_sizeofTextWTTTSRec1 = m_sizeofTime + m_sizeofTurns + m_sizeofTorque;
const int m_sizeofTextWTTTSRec2 = m_sizeofTime + m_sizeofTurns + m_sizeofTorque + m_sizeofTension;


typedef union {
    DWORD dwData;
    float fData;
} FLOAT_CONVERTER;


static String WTTTSArrayToString( const WTTTS_READING pReadings[], int nbrReadings )
{
    // Convert the passed array of readings into a string format for saving
    // in an XML file. Format of the string is as follows:
    //    number of readings, 8 chars representing a 4 byte value
    //    for each reading:
    //      6 chars representing the low order 3 bytes of the time DWORD
    //      8 chars representing the rotation as a hex value
    //      8 chars representing the float torque value as hex chars
    //      8 chars representing the float tension value as hex chars

    // Calculate the overall length of the string required
    int bytesReqd = m_sizeofCount + nbrReadings * m_sizeofTextWTTTSRec2;

    String retVal;
    retVal.SetLength( bytesReqd );

    String tempStr;
    tempStr = IntToHex( nbrReadings, m_sizeofCount );

    for( int iOffset = 1; iOffset <= m_sizeofCount; iOffset++ )
        retVal[iOffset] = tempStr[iOffset];

    int charOffset = m_sizeofCount;

    for( int iRec = 0; iRec < nbrReadings; iRec++ )
    {
        FLOAT_CONVERTER tqFloatConverter;
        tqFloatConverter.fData = pReadings[iRec].torque;

        FLOAT_CONVERTER tnFloatConverter;
        tnFloatConverter.fData = pReadings[iRec].tension;

        tempStr.printf( L"%06X%08X%08X%08X", pReadings[iRec].msecs, pReadings[iRec].rotation, tqFloatConverter.dwData, tnFloatConverter.dwData );

        for( int iOffset = 1; iOffset <= m_sizeofTextWTTTSRec2; iOffset++ )
            retVal[charOffset + iOffset] = tempStr[iOffset];

        charOffset += m_sizeofTextWTTTSRec2;
    }

    return retVal;
}


static int WTTTSStringToArray( const String& wtttsStr, WTTTS_READING pRecs[], int maxRecs )
{
    // Converts a WTTTS string representation to an array of records. If
    // pRecs is NULL, this function returns the number of recs stored in
    // the string. Stores up to maxRecs items in the pRecs array, returning
    // the number of items actually stored.
    UnicodeString tempStr;
    tempStr = wtttsStr.SubString( 1, m_sizeofCount );

    int itemCount = StrToIntDef( L"$" + tempStr, 0 );

    if( itemCount == 0 )
         return 0;

    if( pRecs == NULL )
        return itemCount;

    // Extract up to maxRecs items
    int recOffset = m_sizeofCount;
    int recsSaved = 0;

    int iReadingSize = m_sizeofTextWTTTSRec1;

    if( ( ( wtttsStr.Length() - m_sizeofCount ) / itemCount ) == m_sizeofTextWTTTSRec2 )
        iReadingSize = m_sizeofTextWTTTSRec2;

    for( int iItem = 0; iItem < maxRecs; iItem++ )
    {
        if( wtttsStr.Length() < recOffset + iReadingSize )
            break;

        tempStr = wtttsStr.SubString( recOffset + 1, iReadingSize );

        UnicodeString msecsStr   = tempStr.SubString( 1, m_sizeofTime );
        UnicodeString turnsStr   = tempStr.SubString( m_sizeofTime + 1, m_sizeofTurns );
        UnicodeString torqueStr  = tempStr.SubString( m_sizeofTime + m_sizeofTurns + 1, m_sizeofTorque );
        UnicodeString tensionStr = "0.0";

        if( iReadingSize == m_sizeofTextWTTTSRec2 )
            tensionStr = tempStr.SubString( m_sizeofTime + m_sizeofTurns + m_sizeofTorque + 1, m_sizeofTension );

        WTTTS_READING* pReading = &( pRecs[iItem] );

        pReading->msecs    = StrToIntDef( L"$" + msecsStr, 0 );
        pReading->rotation = StrToIntDef( L"$" + turnsStr, 0 );

        FLOAT_CONVERTER floatConverter;

        floatConverter.dwData = StrToIntDef( L"$" + torqueStr, 0 );
        pReading->torque      = floatConverter.fData;

        floatConverter.dwData = StrToIntDef( L"$" + tensionStr, 0 );
        pReading->tension     = floatConverter.fData;

        recsSaved++;
        recOffset += iReadingSize;
    }

    return recsSaved;
}


//
// Root-level XML doc keys and subkeys.
//
static const String sRootNode   = "TESCOJobFile";
static const String rnFileVer   = "FileVer";

static const String sJobInfoNode = "JobInfo";
    static const String jiClient           = "Client";
    static const String jiLocn             = "Locn";
    static const String jiUOM              = "Units";
    static const String jiStartTime        = "StartTime";
    static const String jiFirstConn        = "FirstConn";
    static const String jiCasingFileName   = "CasingFileName";
    static const String jiCasingsList      = "CasingsList";

static const String sJobParamsNode = "JobParams";
    static const String jpStatus           = "Done";
    static const String jpCreateNbr        = "CreateNbr";
    static const String jpCurrSectIdx      = "CurrSectIdx";

static const String sCalibrationRecs = "CalibrationRecs";
    static const String krCreateNbr        = "CreateNbr";
    static const String krHousingSN        = "HousingSN";
    static const String krMfgSN            = "DevSN";
    static const String krFWRev            = "FWRev";
    static const String krCfgStraps        = "CfgStraps";
    static const String krMfgInfo          = "MfgInfo";
    static const String krMfgDate          = "MfgDate";
    static const String krTorque000Offset  = "Torque000Offset";
    static const String krTorque000Span    = "Torque000Span";
    static const String krTorque180Offset  = "Torque180Offset";
    static const String krTorque180Span    = "Torque180Span";
    static const String krTension000Offset = "Tension000Offset";
    static const String krTension000Span   = "Tension000Span";
    static const String krTension090Offset = "Tension090Offset";
    static const String krTension090Span   = "Tension090Span";
    static const String krTension180Offset = "Tension180Offset";
    static const String krTension180Span   = "Tension180Span";
    static const String krTension270Offset = "Tension270Offset";
    static const String krTension270Span   = "Tension270Span";
    static const String krXTalkTqTq        = "XTalkTqTq";
    static const String krXTalkTqTen       = "XTalkTqTen";
    static const String krXTalkTenTq       = "XTalkTenTq";
    static const String krXTalkTenTen      = "XTalkTenTen";
    static const String krGyroOffset       = "GyroOffset";
    static const String krGyroSpan         = "GyroSpan";
    static const String krPressureOffset   = "PressureOffset";
    static const String krPressureSpan     = "PressureSpan";
    static const String krBattCapacity     = "BattCapacity";
    static const String krBattType         = "BattType";
    static const String krLastCalDate      = "LastCalDate";

static const String sSectionRecs = "SectionRecs";
    static const String srCreateNbr        = "CreateNbr";
    static const String srSectionNbr       = "SectionNbr";
    static const String srAutoRecOn        = "AutoRecording";
    static const String srClientRep        = "ClientRep";
    static const String srTescoTech        = "TescoRep";
    static const String srThreadRep        = "ThreadRep";
    static const String srCasingMfgr       = "CasingMfgr";
    static const String srCasingName       = "CasingName";
    static const String srCasingType       = "CasingType";
    static const String srCasingGrade      = "CasingGrade";
    static const String srCasingDia        = "CasingDia";
    static const String srCasingWeight     = "CasingWeight";
    static const String srHasShoulder      = "HasShoulder";
    static const String srAutoShldrEn      = "AutoShoulder";
    static const String srAutoShldrOff     = "AutoShldrTrqOffset";
    static const String srShldrMinTrq      = "ShldrMinTrq";
    static const String srShldrMaxTrq      = "ShldrMaxTrq";
    static const String srShldrPostTrns    = "ShldrPostTrns";
    static const String srShldrDelta       = "ShldrDelta";
    static const String srTargetMinTrq     = "TargetMinTrq";
    static const String srTargetMaxTrq     = "TargetMaxTrq";
    static const String srTargetOptTrq     = "TargetOptTrq";
    static const String srTargetTurns      = "TargetTurns";
    static const String srTargetDelta      = "TargetDelta";
    static const String srTargetHold       = "TargetHold";
    static const String srTargetOShoot     = "TargetOShoot";

static const String sPipeInvRecs = "InventoryRecs";
    static const String pirCreateNbr       = "CreateNbr";
    static const String pirCreateType      = "CreationType";
    static const String pirLength          = "Length";
    static const String pirConnID          = "ConnID";
    static const String pirState           = "State";

static const String sMainComments = "MainComments";
    static const String mcCreateNbr        = "CreateNbr";
    static const String mcTime             = "CommentTime";
    static const String mcComment          = "CommentText";

static const String sConnRecs = "ConnectionRecs";
    static const String crCreateNbr        = "CreateNbr";
    static const String crConnNbr          = "ConnNbr";
    static const String crSeqNbr           = "SeqNbr";
    static const String crSectNbr          = "SectNbr";
    static const String crAutoRecd         = "AutoRecorded";
    static const String crCalRecNbr        = "CalRecNbr";
    static const String crBattVolts        = "BattVolts";
    static const String crLength           = "Length";
    static const String crDate             = "Date";
    static const String crStatus           = "Status";
    static const String crComment          = "Comment";
    static const String crShldrSet         = "ShldrSet";
    static const String crShldrPt          = "ShldrPt";
    static const String crShldrTq          = "ShldrTq";
    static const String crMostNegPt        = "MostNegPt";
    static const String crPkTorque         = "PkTorque";
    static const String crPkTorquePt       = "PkTorquePt";
    static const String crPtsStr           = "Points";


//
// Basic type handlers
//

int   GetValueFromNode( _di_IXMLNode aNode, UnicodeString keyName, int defaultValue );
void  SaveValueToNode ( _di_IXMLNode aNode, UnicodeString keyName, int value );

float GetValueFromNode( _di_IXMLNode aNode, UnicodeString keyName, float defaultValue );
void  SaveValueToNode ( _di_IXMLNode aNode, UnicodeString keyName, float value );

TDateTime GetValueFromNode( _di_IXMLNode aNode, UnicodeString keyName, TDateTime defaultValue );
void      SaveValueToNode ( _di_IXMLNode aNode, UnicodeString keyName, TDateTime value );

bool GetValueFromNode( _di_IXMLNode aNode, UnicodeString keyName, bool defaultValue );
void SaveValueToNode ( _di_IXMLNode aNode, UnicodeString keyName, bool value );

UnicodeString GetValueFromNode( _di_IXMLNode aNode, UnicodeString keyName, UnicodeString defaultValue );
void          SaveValueToNode ( _di_IXMLNode aNode, UnicodeString keyName, UnicodeString value );


// Record handlers. These handlers return true if keyName was found in aNode,
// false otherwise (for both get...() and set...() functions). The functions will
// also return false if the underlying XML doc is not active or an exception occurs
// during the get...() or set... () operation.
//
// All record handlers for the same type of operation have the same function name
// and are overloaded by the param list. This allows the functions to be called
// from within the template class implementation.

static bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName,       JOB_INFO& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, UnicodeString keyName, const JOB_INFO& entry );

static bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName,       JOB_PARAMS& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, UnicodeString keyName, const JOB_PARAMS& entry );

static bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName,       SECTION_INFO& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, UnicodeString keyName, const SECTION_INFO& entry );

static bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName,       PIPE_INVENTORY_ITEM& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, UnicodeString keyName, const PIPE_INVENTORY_ITEM& entry );

static bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName,       MAIN_COMMENT& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, UnicodeString keyName, const MAIN_COMMENT& entry );

static bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName,       CONNECTION_REC& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, UnicodeString keyName, const CONNECTION_REC& entry );

static bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName,       CALIBRATION_REC& entry );
static bool SaveEntryToNode ( _di_IXMLNode aNode, UnicodeString keyName, const CALIBRATION_REC& entry );

//
// Data handler functions
//

static void SetXMLDocOptions( _di_IXMLDocument& xmlDoc, WORD objFileVer, const JOB_INFO& jobInfo )
{
    // Set the basic structure for an empty xml doc
    xmlDoc->NodeIndentStr = "  ";
    xmlDoc->Options  = TXMLDocOptions() << doNodeAutoIndent << doAttrNull << doAutoPrefix << doNamespaceDecl;
    xmlDoc->Encoding = "ISO-8859-1";
    xmlDoc->Version  = "1.0";

    // Add the root node
    _di_IXMLNode rootNode = xmlDoc->AddChild( sRootNode );

    // Add the file version
    _di_IXMLNode verNode = rootNode->AddChild( rnFileVer );
    verNode->Text = IntToStr( objFileVer );

    // Add the job info
    SaveEntryToNode( rootNode, sJobInfoNode, jobInfo );
}


static bool GetXMLDocOptions( _di_IXMLDocument& xmlDoc, WORD& objFileVer, JOB_INFO& jobInfo )
{
    // Get the root node.
    _di_IXMLNode rootNode = xmlDoc->DocumentElement;

    if( rootNode == NULL )
        return false;

    // Return the file version
    objFileVer = (WORD)GetValueFromNode( rootNode, rnFileVer, 0 );

    if( !GetEntryFromNode( rootNode, sJobInfoNode, jobInfo ) )
        return false;

     return true;
}


static _di_IXMLNode FindOrCreateNode( _di_IXMLNode rootNode, UnicodeString childName )
{
    _di_IXMLNode childNode = rootNode->ChildNodes->FindNode( childName );

    if( childNode == NULL )
        childNode = rootNode->AddChild( childName );

    return childNode;
}


static _di_IXMLNode FindChildNode( _di_IXMLNode rootNode, UnicodeString childName )
{
    if( rootNode == NULL )
        return NULL;

    return rootNode->ChildNodes->FindNode( childName );
}


static String ItemKeyName( int iItem )
{
    return String( L"Item" + IntToStr( iItem ) );
}


//
// TStructArray
//

// We have to do a trick here to force the compiler to instantiate the
// template class code. If you get an 'unresolved external', try adding
// that function to this initializer.

static bool DoInit( void )
{
    TStructArray<SECTION_INFO> sectionRecs;

    SECTION_INFO temp;

    sectionRecs.Append( temp );
    sectionRecs.Modify( 0, temp );

    return true;
}

static bool initDone = DoInit();


//
// NOTE! NOTE! NOTE! Do not use memmove(), memcpy(), etc, where
// copying or moving members of the templated class. If the
// class contains Unicode strings, using mem...() will make bad
// things happen. You have to do an item by item copy instead.
//

template <class T> TStructArray<T>::TStructArray()
{
   m_allocElements = 16;
   m_allocIncrease = 16;
   m_nbrElements   = 0;

   m_pData = new T[m_allocElements];
}


template <class T> TStructArray<T>::~TStructArray()
{
  if( m_allocElements )
      delete [] m_pData;
}


template <class T> void TStructArray<T>::CheckAndAllocMem( int newNbrElements )
{
    // Checks to see if the object contains the number of records passed.
    // If not, allocates new memory and copies existing data over.
    if( newNbrElements >= m_allocElements )
    {
        // More memory required. Over-allocate it
        T *pTmp = new T[ newNbrElements + m_allocIncrease ];

        // Copy any existing elements over. Cannot use memcpy in case
        // element members are classes (eg, UnicodeString)
        if( m_nbrElements )
        {
            for( int iEl = 0; iEl < m_nbrElements; iEl++ )
                pTmp[iEl] = m_pData[iEl];

            delete [] m_pData;
        }

        m_pData = pTmp;
        m_allocElements = newNbrElements + m_allocIncrease;
    }
}


template <class T> TStructArray<T>& TStructArray<T>::operator =(const TStructArray &equ)
{
    try
    {
        if( this == &equ )
            return *this;

        CheckAndAllocMem( equ.m_nbrElements );

        // Copy data element by element. Cannot use memcpy() in case an
        // element member is a class
        for( int iEl = 0; iEl < equ.m_nbrElements; iEl++ )
            m_pData[iEl] = equ.m_pData[iEl];

        m_nbrElements = equ.m_nbrElements;

        return *this;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::operator =" );
        return *this;
    }
}


template <class T> TStructArray<T>& TStructArray<T>::operator += (const TStructArray &plus)
{
    try
    {
        CheckAndAllocMem( m_nbrElements + plus.m_nbrElements );

        // Copy data element by element. Cannot use memcpy() in case an
        // element member is a class
        for( int iEl = 0; iEl < plus.m_nbrElements; iEl++ )
            m_pData[m_nbrElements+iEl] = plus.m_pData[iEl];

        m_nbrElements += plus.m_nbrElements;

        return *this;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::operator +=" );
        return *this;
    }
}


template <class T> void TStructArray<T>::Append( const T& add )
{
    try
    {
        CheckAndAllocMem( m_nbrElements + 1 );

        m_pData[m_nbrElements++] = add;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::Append()" );
    }
}


template <class T> void TStructArray<T>::Insert( int index, const T& insert )
{
    // Inserts an item at the given index
    try
    {
        CheckAndAllocMem( m_nbrElements + 1 );

        for( int i = m_nbrElements; i > index; i-- )
            m_pData[i] = m_pData[i-1];

        m_pData[index] = insert;

        m_nbrElements++;
    }
    catch(...)
    {
        ShowMessage( "Not enough memory for TStructArray::Insert()" );
    }
}


template <class T> void TStructArray<T>::Modify( int index, const T& modify )
{
    if( ( index >= 0 ) && ( index < m_nbrElements ) )
    {
        m_pData[index] = modify;
    }
}


template <class T> void TStructArray<T>::Del( int index )
{
    if( ( index >= 0 ) && ( index < m_nbrElements ) )
    {
        for( int i = index; i < m_nbrElements - 1; i++ )
            m_pData[i] = m_pData[i+1];

        m_nbrElements--;
    }
}


template <class T> void TStructArray<T>::LoadFromNode( _di_IXMLNode aNode )
{
    // Assumes that each element T is stored under aNode under 0-based
    // key values (eg, "0" to "n-1" where n is the number of elements
    // in the array.

    // Clear all elements from array first
    Clear();

    if( aNode != NULL )
    {
        // The ChildNodes->Count property gives bogus values at times for some
        // reason. So don't use it. Instead, loop here until the next item
        // number is not found.
        int itemNbr = 0;

        while( itemNbr >= 0 )
        {
            T tempData;

            if( !GetEntryFromNode( aNode, ItemKeyName( itemNbr ), tempData ) )
                break;

            Append( tempData );

            itemNbr++;
        }
    }
}


template <class T> void TStructArray<T>::SaveToNode( _di_IXMLNode aNode )
{
    // Stores each element T under aNode using 0-based key values
    // (eg, "0" to "n-1" where n is the number of elements in the array).

    if( aNode != NULL )
    {
        // Delete any existing children (and presumably leaf nodes)
        aNode->ChildNodes->Clear();

        for( int iItem = 0; iItem < m_nbrElements; iItem++ )
            SaveEntryToNode( aNode, ItemKeyName( iItem ), m_pData[iItem] );
    }
}


//
// TJobList Implementation
//

__fastcall TJobList::TJobList( const String& dataDir )
{
    pJobList = new TList();

    TStringDynArray list;
    TSearchOption searchOption = TSearchOption::soTopDirectoryOnly;

    try
    {
        list = TDirectory::GetFiles( dataDir, L"*" + m_JobExt, searchOption );
    }
    catch( ... )
    {
    }

    for( int iItem = 0; iItem < list.Length; iItem++ )
    {
        // Only list valid jobs
        JOB_INFO jobInfo;

        if( GetJobInfo( list[iItem], jobInfo ) )
        {
             JOB_LIST_ITEM* pJobItem = new JOB_LIST_ITEM;

             // Assign the items to be stored in the struct. Force the
             // job file name string to be unique.
             pJobItem->jobInfo     = jobInfo;
             pJobItem->jobFileName = list[iItem];
             pJobItem->jobFileName.Unique();

             pJobList->Add( pJobItem );
        }
    }
}


__fastcall TJobList::TJobList( TStrings* fileNameList )
{
    pJobList = new TList();

    if( fileNameList == NULL )
        return;

    for( int iItem = 0; iItem < fileNameList->Count; iItem++ )
    {
        // Only list valid jobs
        JOB_INFO jobInfo;

        if( GetJobInfo( fileNameList->Strings[iItem], jobInfo ) )
        {
             JOB_LIST_ITEM* pJobItem = new JOB_LIST_ITEM;

             // Assign the items to be stored in the struct. Force the
             // job file name string to be unique.
             pJobItem->jobInfo     = jobInfo;
             pJobItem->jobFileName = fileNameList->Strings[iItem];
             pJobItem->jobFileName.Unique();

             pJobList->Add( pJobItem );
        }
    }
}


__fastcall TJobList::~TJobList( void )
{
    while( pJobList->Count > 0 )
        DeleteItem( 0 );
}


bool TJobList::GetItem( int index, JOB_LIST_ITEM& jobItem )
{
    if( ( index < 0 ) || ( index >= pJobList->Count ) )
        return false;

    JOB_LIST_ITEM* pJobItem = (JOB_LIST_ITEM*)( pJobList->Items[index] );

    jobItem = *pJobItem;

    return true;
}


void TJobList::DeleteItem( int index )
{
    if( ( index < 0 ) || ( index >= pJobList->Count ) )
        return;

    JOB_LIST_ITEM* pJobItem = (JOB_LIST_ITEM*)( pJobList->Items[index] );

    delete pJobItem;

    pJobList->Delete( index );
}


int JobsExistFor( UnicodeString dataDir, UnicodeString clientName, UnicodeString location )
{
    // Returns how many jobs exists for the passed client and location pair
    // in the dataDir. If none exist, return zero.
    TStringDynArray list;
    TSearchOption searchOption = TSearchOption::soTopDirectoryOnly;

    try
    {
        list = TDirectory::GetFiles( dataDir, L"*" + m_JobExt, searchOption );
    }
    catch( ... )
    {
    }

    int nbrJobs = 0;

    for( int iItem = 0; iItem < list.Length; iItem++ )
    {
        // Only list valid jobs
        JOB_INFO jobInfo;

        if( GetJobInfo( list[iItem], jobInfo ) )
        {
            if( ( jobInfo.clientName.CompareIC( clientName ) == 0 )
                  &&
                ( jobInfo.location.CompareIC( location ) == 0 )
              )
            {
                nbrJobs++;
            }
        }
    }

    return nbrJobs;
}


bool GetJobInfo( UnicodeString jobFileName, JOB_INFO& jobInfo )
{
    // Returns true if the job name exists and populates the jobInfo struct.
    // Returns false if the job is not found.
    if( !FileExists( jobFileName ) )
        return false;

    bool bSuccess = false;

    _di_IXMLDocument xmlDoc = NULL;

    try
    {
        _di_IXMLDocument xmlDoc = LoadXMLDocument( jobFileName );

        /* This is where we'd validate the MD5 checksum */

        // Validate the root node contents
        WORD jobFileVer;

        if( !GetXMLDocOptions( xmlDoc, jobFileVer, jobInfo ) )
            Abort();

        if( jobFileVer != CURR_JOB_FILE_VER )
            Abort();

        // Fall through means success
        bSuccess = true;
    }
    catch( ... )
    {
    }

    if( xmlDoc != NULL )
        xmlDoc.Release();

    return bSuccess;
}


bool CreateNewJob( UnicodeString& jobFileName, UnicodeString dataDir,
                   const JOB_INFO& jobInfo, const SECTION_INFO& sectionInfo )
{
    // Creates a new job with the passed parameters in the current data dir
    jobFileName = IncludeTrailingBackslash( dataDir ) + time( NULL ) + m_JobExt;

    // First create an empty XML file for this job
    DeleteFile( jobFileName );

    bool bSuccess = false;

    _di_IXMLDocument xmlDoc = NewXMLDocument();

    try
    {
        // Initialize new doc
        SetXMLDocOptions( xmlDoc, CURR_JOB_FILE_VER, jobInfo );

        _di_IXMLNode rootNode = xmlDoc->DocumentElement;

        if( rootNode == NULL )
            Abort();

        JOB_PARAMS jobParams;
        jobParams.isDone          = false;
        jobParams.lastCreationNbr = 1;   // Set to 1 because the section below will be assigned that value
        jobParams.currSectIndex   = 0;   // First section rec added below

        if( !SaveEntryToNode( rootNode, sJobParamsNode, jobParams ) )
            Abort();

        _di_IXMLNode sectRecsNode = FindOrCreateNode( rootNode, sSectionRecs );

        TStructArray<SECTION_INFO> sectionRecs;
        sectionRecs.Append( sectionInfo );

        SECTION_INFO tempSection = sectionRecs[0];

        tempSection.creationNbr = 1;
        tempSection.sectionNbr  = 1;

        sectionRecs.Modify( 0, tempSection );

        sectionRecs.SaveToNode( sectRecsNode );

        // If we've been given a casings list, create the pipe inventory now
        if( jobInfo.sCasingsList.Length() > 0 )
        {
            TStructArray<PIPE_INVENTORY_ITEM> inventoryRecs;

            // Keep track of the creation number so that we can update the
            // job info once all pipe inv recs have been added
            int iCreationNbr = tempSection.creationNbr;
            int iSequenceNbr = 0;

            TStringList* pCasingLengths = new TStringList();
            pCasingLengths->CommaText = jobInfo.sCasingsList;

            for( int iCasingItem = 0; iCasingItem < pCasingLengths->Count; iCasingItem++ )
            {
                PIPE_INVENTORY_ITEM nextCasing;

                iCreationNbr++;
                iSequenceNbr++;

                nextCasing.creationNbr  = iCreationNbr;
                nextCasing.iSeqNbr      = iSequenceNbr;
                nextCasing.ictType      = ICT_INIT_LOAD;
                nextCasing.iLength      = pCasingLengths->Strings[iCasingItem].ToIntDef( 0 );
                nextCasing.connID       = 0;
                nextCasing.iisState     = IIS_AVAILABLE;

                inventoryRecs.Append( nextCasing );
            }

            // Done with our string list
            delete pCasingLengths;

            _di_IXMLNode invRecsNode = FindOrCreateNode( rootNode, sPipeInvRecs );

            inventoryRecs.SaveToNode( invRecsNode );

            // Now update the job info with the updated creation number.
            // Only need to do so if the number has changed
            if( iCreationNbr != tempSection.creationNbr )
            {
                jobParams.lastCreationNbr = iCreationNbr;

                if( !SaveEntryToNode( rootNode, sJobParamsNode, jobParams ) )
                    Abort();
            }
        }

        // Done adding elements - save the file now
        xmlDoc->SaveToFile( jobFileName );

        // Fall through mean succcess
        bSuccess = true;
    }
    catch( ... )
    {
    }

    xmlDoc.Release();

    return bSuccess;
}


bool ConnGood( const CONNECTION_INFO* pConn )
{
    if( pConn == NULL )
        return false;

    return( ( pConn->crResult == CR_PASSED ) || ( pConn->crResult == CR_FORCE_PASSED ) );
}


CONN_RESULT ConnResultFromTextEnum( const String& connText )
{
    int iResult = connText.ToIntDef( -1 );

    if( ( iResult < 0 ) || ( iResult >= NBR_CONN_RESULTS ) )
        return CR_FAILED;

    return (CONN_RESULT)iResult;
}


//
// GetValue...() / SaveValue...() functions are overloaded implementations
// for getting and saving basic data types
//

int GetValueFromNode( _di_IXMLNode aNode, UnicodeString keyName, int defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    AnsiString sText = node->Text;

    return sText.ToIntDef( defaultValue );
}


void SaveValueToNode( _di_IXMLNode aNode, UnicodeString keyName, int value )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( node == NULL )
        node = aNode->AddChild( keyName );

    node->Text = IntToStr( value );
}


float GetValueFromNode( _di_IXMLNode aNode, UnicodeString keyName, float defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    AnsiString sText = node->Text;

    return StrToFloatDef( sText, defaultValue );
}


void SaveValueToNode( _di_IXMLNode aNode, UnicodeString keyName, float value )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( node == NULL )
        node = aNode->AddChild( keyName );

    node->Text = FloatToStr( value );
}


TDateTime GetValueFromNode( _di_IXMLNode aNode, UnicodeString keyName, TDateTime defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    String sText = node->Text;

    TDateTime dtReturn;

    try
    {
        if( sText.Pos( "-" ) > 0 )
        {
            dtReturn = TDateTime( sText.SubString(  1, 4 ).ToIntDef( 0 ),
                                  sText.SubString(  6, 2 ).ToIntDef( 0 ),
                                  sText.SubString(  9, 2 ).ToIntDef( 0 ),
                                  sText.SubString( 12, 2 ).ToIntDef( 0 ),
                                  sText.SubString( 15, 2 ).ToIntDef( 0 ),
                                  sText.SubString( 18, 2 ).ToIntDef( 0 ),
                                  0 );
        }
        else
        {
            dtReturn = UnixToDateTime( sText.ToIntDef( 0 ), false );
        }
    }
    catch( ... )
    {
        return 0;
    }

    return dtReturn;
}


void SaveValueToNode( _di_IXMLNode aNode, UnicodeString keyName, TDateTime value )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( node == NULL )
        node = aNode->AddChild( keyName );

    node->Text = value.FormatString( "yyyy-mm-dd hh:nn:ss" );
}


UnicodeString GetValueFromNode( _di_IXMLNode aNode, UnicodeString keyName, UnicodeString defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    return node->Text;
}


void SaveValueToNode( _di_IXMLNode aNode, UnicodeString keyName, UnicodeString value )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    // If the node doesn't exist, and the value is an empty string, then
    // we don't have to do anything
    if( value.IsEmpty() )
    {
        if( node != NULL )
            aNode->ChildNodes->Delete( keyName );
    }
    else
    {
        if( node == NULL )
            node = aNode->AddChild( keyName );

        node->Text = value;
    }
}


bool GetValueFromNode( _di_IXMLNode aNode, UnicodeString keyName, bool defaultValue )
{
    if( aNode == NULL )
        return defaultValue;

    if( !aNode->OwnerDocument->Active )
        return defaultValue;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( ( node == NULL ) || ( !node->IsTextElement ) )
        return defaultValue;

    AnsiString sText = node->Text;

    return sText.ToIntDef( defaultValue );
}


void SaveValueToNode( _di_IXMLNode aNode, UnicodeString keyName, bool value )
{
    if( aNode == NULL )
        return;

    if( !aNode->OwnerDocument->Active )
        return;

    _di_IXMLNode node = aNode->ChildNodes->FindNode( keyName );

    if( node == NULL )
        node = aNode->AddChild( keyName );

    node->Text = IntToStr( (int)value );
}


//
// GetEntry...() / SaveEntry...() functions are overloaded implementations
// for getting and saving structure types
//

bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName, JOB_INFO& entry )
{
    // Initialize to default values
    entry.clientName       = L"";
    entry.location         = L"";
    entry.unitsOfMeasure   = UOM_METRIC;
    entry.jobStartTime     = 0;
    entry.firstConnNbr     = 0;
    entry.sCasingsFileName = "";
    entry.sCasingsList     = "";

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.clientName       = GetValueFromNode( infoNode, jiClient,         entry.clientName       );
        entry.location         = GetValueFromNode( infoNode, jiLocn,           entry.location         );
        entry.unitsOfMeasure   = GetValueFromNode( infoNode, jiUOM,            entry.unitsOfMeasure   );
        entry.jobStartTime     = GetValueFromNode( infoNode, jiStartTime,      entry.jobStartTime     );
        entry.firstConnNbr     = GetValueFromNode( infoNode, jiFirstConn,      entry.firstConnNbr     );
        entry.sCasingsFileName = GetValueFromNode( infoNode, jiCasingFileName, entry.sCasingsFileName );
        entry.sCasingsList     = GetValueFromNode( infoNode, jiCasingsList,    entry.sCasingsList     );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode( _di_IXMLNode aNode, UnicodeString keyName, const JOB_INFO& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, jiClient,         entry.clientName       );
        SaveValueToNode( infoNode, jiLocn,           entry.location         );
        SaveValueToNode( infoNode, jiUOM,            entry.unitsOfMeasure   );
        SaveValueToNode( infoNode, jiStartTime,      entry.jobStartTime     );
        SaveValueToNode( infoNode, jiFirstConn,      entry.firstConnNbr     );
        SaveValueToNode( infoNode, jiCasingFileName, entry.sCasingsFileName );
        SaveValueToNode( infoNode, jiCasingsList,    entry.sCasingsList     );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName, JOB_PARAMS& entry )
{
    // Initialize to default values
    entry.isDone          = false;
    entry.lastCreationNbr = 0;
    entry.currSectIndex   = 0;

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.isDone          = GetValueFromNode( infoNode, jpStatus,      entry.isDone          );
        entry.lastCreationNbr = GetValueFromNode( infoNode, jpCreateNbr,   entry.lastCreationNbr );
        entry.currSectIndex   = GetValueFromNode( infoNode, jpCurrSectIdx, entry.currSectIndex   );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode( _di_IXMLNode aNode, UnicodeString keyName, const JOB_PARAMS& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, jpStatus,      entry.isDone          );
        SaveValueToNode( infoNode, jpCreateNbr,   entry.lastCreationNbr );
        SaveValueToNode( infoNode, jpCurrSectIdx, entry.currSectIndex   );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName, SECTION_INFO& entry )
{
    // Initialize to default values
    entry.creationNbr          = 0;
    entry.sectionNbr           = 0;
    entry.clientRep            = L"";
    entry.tescoTech            = L"";
    entry.threadRep            = L"";
    entry.casingMfgr           = L"";
    entry.casingName           = L"";
    entry.casingType           = L"";
    entry.casingGrade          = L"";
    entry.casingDia            = 0.0;
    entry.casingWeight         = 0.0;
    entry.autoRecordOn         = false;
    entry.hasShoulder          = true;
    entry.autoShldrEnabled     = true;
    entry.shoulderMinTorque    = 0.0;
    entry.shoulderMaxTorque    = 0.0;
    entry.shoulderPostTurns    = 0.0;
    entry.shoulderDelta        = 0.0;
    entry.peakTargetMinTorque  = 0.0;
    entry.peakTargetMaxTorque  = 0.0;
    entry.peakTargetOptTorque  = 0.0;
    entry.peakTargetTurns      = 0.0;
    entry.peakTargetDelta      = 0.0;
    entry.peakTargetHold       = 0.0;
    entry.peakTargetOvershoot  = 0.0;

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.creationNbr          = GetValueFromNode( infoNode, srCreateNbr,     0 );
        entry.sectionNbr           = GetValueFromNode( infoNode, srSectionNbr,    0 );
        entry.autoRecordOn         = GetValueFromNode( infoNode, srAutoRecOn,     false );
        entry.clientRep            = GetValueFromNode( infoNode, srClientRep,     String( L"" ) );
        entry.tescoTech            = GetValueFromNode( infoNode, srTescoTech,     String( L"" ) );
        entry.threadRep            = GetValueFromNode( infoNode, srThreadRep,     String( L"" ) );
        entry.casingMfgr           = GetValueFromNode( infoNode, srCasingMfgr,    String( L"" ) );
        entry.casingName           = GetValueFromNode( infoNode, srCasingName,    String( L"" ) );
        entry.casingType           = GetValueFromNode( infoNode, srCasingType,    String( L"" ) );
        entry.casingGrade          = GetValueFromNode( infoNode, srCasingGrade,   String( L"" ) );
        entry.casingDia            = GetValueFromNode( infoNode, srCasingDia,     (float)0.0 );
        entry.casingWeight         = GetValueFromNode( infoNode, srCasingWeight,  (float)0.0 );
        entry.hasShoulder          = GetValueFromNode( infoNode, srHasShoulder,   true );
        entry.autoShldrEnabled     = GetValueFromNode( infoNode, srAutoShldrEn,   true );
        entry.shoulderMinTorque    = GetValueFromNode( infoNode, srShldrMinTrq,   (float)0.0 );
        entry.shoulderMaxTorque    = GetValueFromNode( infoNode, srShldrMaxTrq,   (float)0.0 );
        entry.shoulderPostTurns    = GetValueFromNode( infoNode, srShldrPostTrns, (float)0.0 );
        entry.shoulderDelta        = GetValueFromNode( infoNode, srShldrDelta,    (float)0.0 );
        entry.peakTargetMinTorque  = GetValueFromNode( infoNode, srTargetMinTrq,  (float)0.0 );
        entry.peakTargetMaxTorque  = GetValueFromNode( infoNode, srTargetMaxTrq,  (float)0.0 );
        entry.peakTargetOptTorque  = GetValueFromNode( infoNode, srTargetOptTrq,  (float)0.0 );
        entry.peakTargetTurns      = GetValueFromNode( infoNode, srTargetTurns,   (float)0.0 );
        entry.peakTargetDelta      = GetValueFromNode( infoNode, srTargetDelta,   (float)0.0 );
        entry.peakTargetHold       = GetValueFromNode( infoNode, srTargetHold,    (float)0.0 );
        entry.peakTargetOvershoot  = GetValueFromNode( infoNode, srTargetOShoot,  (float)0.0 );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode( _di_IXMLNode aNode, UnicodeString keyName, const SECTION_INFO& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, srCreateNbr,     entry.creationNbr );
        SaveValueToNode( infoNode, srSectionNbr,    entry.sectionNbr );
        SaveValueToNode( infoNode, srAutoRecOn,     entry.autoRecordOn );
        SaveValueToNode( infoNode, srClientRep,     entry.clientRep );
        SaveValueToNode( infoNode, srTescoTech,     entry.tescoTech );
        SaveValueToNode( infoNode, srThreadRep,     entry.threadRep );
        SaveValueToNode( infoNode, srCasingMfgr,    entry.casingMfgr );
        SaveValueToNode( infoNode, srCasingName,    entry.casingName );
        SaveValueToNode( infoNode, srCasingType,    entry.casingType );
        SaveValueToNode( infoNode, srCasingGrade,   entry.casingGrade );
        SaveValueToNode( infoNode, srCasingDia,     entry.casingDia );
        SaveValueToNode( infoNode, srCasingWeight,  entry.casingWeight );
        SaveValueToNode( infoNode, srHasShoulder,   entry.hasShoulder );
        SaveValueToNode( infoNode, srAutoShldrEn,   entry.autoShldrEnabled );
        SaveValueToNode( infoNode, srShldrMinTrq,   entry.shoulderMinTorque );
        SaveValueToNode( infoNode, srShldrMaxTrq,   entry.shoulderMaxTorque );
        SaveValueToNode( infoNode, srShldrPostTrns, entry.shoulderPostTurns );
        SaveValueToNode( infoNode, srShldrDelta,    entry.shoulderDelta );
        SaveValueToNode( infoNode, srTargetMinTrq,  entry.peakTargetMinTorque );
        SaveValueToNode( infoNode, srTargetMaxTrq,  entry.peakTargetMaxTorque );
        SaveValueToNode( infoNode, srTargetOptTrq,  entry.peakTargetOptTorque );
        SaveValueToNode( infoNode, srTargetTurns,   entry.peakTargetTurns );
        SaveValueToNode( infoNode, srTargetDelta,   entry.peakTargetDelta );
        SaveValueToNode( infoNode, srTargetHold,    entry.peakTargetHold );
        SaveValueToNode( infoNode, srTargetOShoot,  entry.peakTargetOvershoot );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName, PIPE_INVENTORY_ITEM& entry )
{
    // Initialize to default values
    entry.creationNbr  = 0;
    entry.iSeqNbr      = 0;   // Note: this member not saved to file
    entry.ictType      = ICT_AUTO_CREATE;
    entry.iLength      = 0;
    entry.connID       = 0;
    entry.iisState     = IIS_AVAILABLE;

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.creationNbr  = GetValueFromNode( infoNode, pirCreateNbr,  0 );
        entry.ictType      = GetValueFromNode( infoNode, pirCreateType, (int)ICT_AUTO_CREATE );
        entry.iLength      = GetValueFromNode( infoNode, pirLength,     0 );
        entry.connID       = GetValueFromNode( infoNode, pirConnID,     false );
        entry.iisState     = GetValueFromNode( infoNode, pirState,      (int)IIS_AVAILABLE );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode ( _di_IXMLNode aNode, UnicodeString keyName, const PIPE_INVENTORY_ITEM& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, pirCreateNbr,  entry.creationNbr );
        SaveValueToNode( infoNode, pirCreateType, entry.ictType     );
        SaveValueToNode( infoNode, pirLength,     entry.iLength     );
        SaveValueToNode( infoNode, pirConnID,     entry.connID      );
        SaveValueToNode( infoNode, pirState,      entry.iisState    );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName, MAIN_COMMENT& entry )
{
    // Initialize to default values
    entry.creationNbr = 0;
    entry.entryTime   = 0;
    entry.entryText   = L"";

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.creationNbr = GetValueFromNode( infoNode, mcCreateNbr, 0                    );
        entry.entryTime   = GetValueFromNode( infoNode, mcTime,      (TDateTime) 0        );
        entry.entryText   = GetValueFromNode( infoNode, mcComment,   UnicodeString( L"" ) );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode ( _di_IXMLNode aNode, UnicodeString keyName, const MAIN_COMMENT& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, mcCreateNbr, entry.creationNbr );
        SaveValueToNode( infoNode, mcTime,      entry.entryTime   );
        SaveValueToNode( infoNode, mcComment,   entry.entryText   );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName, CALIBRATION_REC& entry )
{
    // Initialize to default values
    entry.creationNbr      = 0;
    entry.housingSN        = L"";
    entry.mfgSN            = L"";
    entry.fwRev            = L"";
    entry.cfgStraps        = L"";
    entry.mfgInfo          = L"";
    entry.mfgDate          = L"";
    entry.lastCalDate      = L"----/--/--";
    entry.torque000Offset  = L"";
    entry.torque000Span    = L"";
    entry.torque180Offset  = L"";
    entry.torque180Span    = L"";
    entry.tension000Offset = L"";
    entry.tension000Span   = L"";
    entry.tension090Offset = L"";
    entry.tension090Span   = L"";
    entry.tension180Offset = L"";
    entry.tension180Span   = L"";
    entry.tension270Offset = L"";
    entry.tension270Span   = L"";
    entry.xTalkTqTq        = L"";
    entry.xTalkTqTen       = L"";
    entry.xTalkTenTq       = L"";
    entry.xTalkTenTen      = L"";
    entry.gyroOffset       = L"";
    entry.gyroSpan         = L"";
    entry.pressureOffset   = L"";
    entry.pressureSpan     = L"";
    entry.battCapacity     = L"";
    entry.battType         = L"";

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.creationNbr      = GetValueFromNode( infoNode, krCreateNbr,        0 );
        entry.housingSN        = GetValueFromNode( infoNode, krHousingSN,        UnicodeString( L"" ) );
        entry.mfgSN            = GetValueFromNode( infoNode, krMfgSN,            UnicodeString( L"" ) );
        entry.fwRev            = GetValueFromNode( infoNode, krFWRev,            UnicodeString( L"" ) );
        entry.cfgStraps        = GetValueFromNode( infoNode, krCfgStraps,        UnicodeString( L"" ) );
        entry.mfgInfo          = GetValueFromNode( infoNode, krMfgInfo,          UnicodeString( L"" ) );
        entry.mfgDate          = GetValueFromNode( infoNode, krMfgDate,          UnicodeString( L"" ) );
        entry.torque000Offset  = GetValueFromNode( infoNode, krTorque000Offset,  UnicodeString( L"" ) );
        entry.torque000Span    = GetValueFromNode( infoNode, krTorque000Span,    UnicodeString( L"" ) );
        entry.torque180Offset  = GetValueFromNode( infoNode, krTorque180Offset,  UnicodeString( L"" ) );
        entry.torque180Span    = GetValueFromNode( infoNode, krTorque180Span,    UnicodeString( L"" ) );
        entry.tension000Offset = GetValueFromNode( infoNode, krTension000Offset, UnicodeString( L"" ) );
        entry.tension000Span   = GetValueFromNode( infoNode, krTension000Span,   UnicodeString( L"" ) );
        entry.tension090Offset = GetValueFromNode( infoNode, krTension090Offset, UnicodeString( L"" ) );
        entry.tension090Span   = GetValueFromNode( infoNode, krTension090Span,   UnicodeString( L"" ) );
        entry.tension180Offset = GetValueFromNode( infoNode, krTension180Offset, UnicodeString( L"" ) );
        entry.tension180Span   = GetValueFromNode( infoNode, krTension180Span,   UnicodeString( L"" ) );
        entry.tension270Offset = GetValueFromNode( infoNode, krTension270Offset, UnicodeString( L"" ) );
        entry.tension270Span   = GetValueFromNode( infoNode, krTension270Span,   UnicodeString( L"" ) );
        entry.xTalkTqTq        = GetValueFromNode( infoNode, krXTalkTqTq,        UnicodeString( L"" ) );
        entry.xTalkTqTen       = GetValueFromNode( infoNode, krXTalkTqTen,       UnicodeString( L"" ) );
        entry.xTalkTenTq       = GetValueFromNode( infoNode, krXTalkTenTq,       UnicodeString( L"" ) );
        entry.xTalkTenTen      = GetValueFromNode( infoNode, krXTalkTenTen,      UnicodeString( L"" ) );
        entry.gyroOffset       = GetValueFromNode( infoNode, krGyroOffset,       UnicodeString( L"" ) );
        entry.gyroSpan         = GetValueFromNode( infoNode, krGyroSpan,         UnicodeString( L"" ) );
        entry.pressureOffset   = GetValueFromNode( infoNode, krPressureOffset,   UnicodeString( L"" ) );
        entry.pressureSpan     = GetValueFromNode( infoNode, krPressureSpan,     UnicodeString( L"" ) );
        entry.battCapacity     = GetValueFromNode( infoNode, krBattCapacity,     UnicodeString( L"" ) );
        entry.battType         = GetValueFromNode( infoNode, krBattType,         UnicodeString( L"" ) );
        entry.lastCalDate      = GetValueFromNode( infoNode, krLastCalDate,      entry.lastCalDate );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode( _di_IXMLNode aNode, UnicodeString keyName, const CALIBRATION_REC& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, krCreateNbr,        entry.creationNbr      );
        SaveValueToNode( infoNode, krHousingSN,        entry.housingSN        );
        SaveValueToNode( infoNode, krMfgSN,            entry.mfgSN            );
        SaveValueToNode( infoNode, krFWRev,            entry.fwRev            );
        SaveValueToNode( infoNode, krCfgStraps,        entry.cfgStraps        );
        SaveValueToNode( infoNode, krMfgInfo,          entry.mfgInfo          );
        SaveValueToNode( infoNode, krMfgDate,          entry.mfgDate          );
        SaveValueToNode( infoNode, krTorque000Offset,  entry.torque000Offset  );
        SaveValueToNode( infoNode, krTorque000Span,    entry.torque000Span    );
        SaveValueToNode( infoNode, krTorque180Offset,  entry.torque180Offset  );
        SaveValueToNode( infoNode, krTorque180Span,    entry.torque180Span    );
        SaveValueToNode( infoNode, krTension000Offset, entry.tension000Offset );
        SaveValueToNode( infoNode, krTension000Span,   entry.tension000Span   );
        SaveValueToNode( infoNode, krTension090Offset, entry.tension090Offset );
        SaveValueToNode( infoNode, krTension090Span,   entry.tension090Span   );
        SaveValueToNode( infoNode, krTension180Offset, entry.tension180Offset );
        SaveValueToNode( infoNode, krTension180Span,   entry.tension180Span   );
        SaveValueToNode( infoNode, krTension270Offset, entry.tension270Offset );
        SaveValueToNode( infoNode, krTension270Span,   entry.tension270Span   );
        SaveValueToNode( infoNode, krXTalkTqTq,        entry.xTalkTqTq        );
        SaveValueToNode( infoNode, krXTalkTqTen,       entry.xTalkTqTen       );
        SaveValueToNode( infoNode, krXTalkTenTq,       entry.xTalkTenTq       );
        SaveValueToNode( infoNode, krXTalkTenTen,      entry.xTalkTenTen      );
        SaveValueToNode( infoNode, krGyroOffset,       entry.gyroOffset       );
        SaveValueToNode( infoNode, krGyroSpan,         entry.gyroSpan         );
        SaveValueToNode( infoNode, krPressureOffset,   entry.pressureOffset   );
        SaveValueToNode( infoNode, krPressureSpan,     entry.pressureSpan     );
        SaveValueToNode( infoNode, krBattCapacity,     entry.battCapacity     );
        SaveValueToNode( infoNode, krBattType,         entry.battType         );
        SaveValueToNode( infoNode, krLastCalDate,      entry.lastCalDate      );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool GetEntryFromNode( _di_IXMLNode aNode, UnicodeString keyName, CONNECTION_REC& entry )
{
    entry.connInfo.creationNbr = 0;
    entry.connInfo.connNbr     = 0;
    entry.connInfo.seqNbr      = 0;
    entry.connInfo.sectionNbr  = 0;
    entry.connInfo.autoRecConn = false;
    entry.connInfo.calRecNbr   = 0;
    entry.connInfo.battVolts   = 0.0;
    entry.connInfo.length      = 0.0;
    entry.connInfo.dateTime    = 0;
    entry.connInfo.crResult    = CR_FAILED;
    entry.connInfo.comment     = L"";
    entry.connInfo.shoulderSet = false;
    entry.connInfo.shoulderPt  = 0.0;
    entry.connInfo.shoulderTq  = 0.0;
    entry.connInfo.pkTorque    = 0.0;
    entry.connInfo.pkTorquePt  = 0.0;
    entry.connInfo.mostNegPt   = 0.0;
    entry.connPtsStr           = L"";

    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            return false;

        entry.connInfo.creationNbr = GetValueFromNode( infoNode, crCreateNbr,  entry.connInfo.creationNbr   );
        entry.connInfo.connNbr     = GetValueFromNode( infoNode, crConnNbr,    entry.connInfo.connNbr       );
        entry.connInfo.seqNbr      = GetValueFromNode( infoNode, crSeqNbr,     entry.connInfo.seqNbr        );
        entry.connInfo.sectionNbr  = GetValueFromNode( infoNode, crSectNbr,    entry.connInfo.sectionNbr    );
        entry.connInfo.autoRecConn = GetValueFromNode( infoNode, crAutoRecd,   entry.connInfo.autoRecConn   );
        entry.connInfo.calRecNbr   = GetValueFromNode( infoNode, crCalRecNbr,  entry.connInfo.calRecNbr     );
        entry.connInfo.battVolts   = GetValueFromNode( infoNode, crBattVolts,  entry.connInfo.battVolts     );
        entry.connInfo.length      = GetValueFromNode( infoNode, crLength,     entry.connInfo.length        );
        entry.connInfo.dateTime    = GetValueFromNode( infoNode, crDate,       entry.connInfo.dateTime      );
        entry.connInfo.crResult    = GetValueFromNode( infoNode, crStatus,     (int)entry.connInfo.crResult );
        entry.connInfo.comment     = GetValueFromNode( infoNode, crComment,    entry.connInfo.comment       );
        entry.connInfo.shoulderSet = GetValueFromNode( infoNode, crShldrSet,   entry.connInfo.shoulderSet   );
        entry.connInfo.shoulderPt  = GetValueFromNode( infoNode, crShldrPt,    entry.connInfo.shoulderPt    );
        entry.connInfo.shoulderTq  = GetValueFromNode( infoNode, crShldrTq,    entry.connInfo.shoulderTq    );
        entry.connInfo.pkTorque    = GetValueFromNode( infoNode, crPkTorque,   entry.connInfo.pkTorque      );
        entry.connInfo.pkTorquePt  = GetValueFromNode( infoNode, crPkTorquePt, entry.connInfo.pkTorquePt    );
        entry.connInfo.mostNegPt   = GetValueFromNode( infoNode, crMostNegPt,  entry.connInfo.mostNegPt     );
        entry.connPtsStr           = GetValueFromNode( infoNode, crPtsStr,     entry.connPtsStr             );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


bool SaveEntryToNode( _di_IXMLNode aNode, UnicodeString keyName, const CONNECTION_REC& entry )
{
    if( aNode == NULL )
        return false;

    if( !aNode->OwnerDocument->Active )
        return false;

    try
    {
        // Find the key in this node. If it doesn't exist, create it
        _di_IXMLNode infoNode = aNode->ChildNodes->FindNode( keyName );

        if( infoNode == NULL )
            infoNode = aNode->AddChild( keyName );

        if( infoNode == NULL )
            Abort();

        SaveValueToNode( infoNode, crCreateNbr,  entry.connInfo.creationNbr   );
        SaveValueToNode( infoNode, crConnNbr,    entry.connInfo.connNbr       );
        SaveValueToNode( infoNode, crSeqNbr,     entry.connInfo.seqNbr        );
        SaveValueToNode( infoNode, crSectNbr,    entry.connInfo.sectionNbr    );
        SaveValueToNode( infoNode, crAutoRecd,   entry.connInfo.autoRecConn   );
        SaveValueToNode( infoNode, crCalRecNbr,  entry.connInfo.calRecNbr     );
        SaveValueToNode( infoNode, crBattVolts,  entry.connInfo.battVolts     );
        SaveValueToNode( infoNode, crLength,     entry.connInfo.length        );
        SaveValueToNode( infoNode, crDate,       entry.connInfo.dateTime      );
        SaveValueToNode( infoNode, crStatus,     (int)entry.connInfo.crResult );
        SaveValueToNode( infoNode, crComment,    entry.connInfo.comment       );
        SaveValueToNode( infoNode, crShldrSet,   entry.connInfo.shoulderSet   );
        SaveValueToNode( infoNode, crShldrPt,    entry.connInfo.shoulderPt    );
        SaveValueToNode( infoNode, crShldrTq,    entry.connInfo.shoulderTq    );
        SaveValueToNode( infoNode, crPkTorque,   entry.connInfo.pkTorque      );
        SaveValueToNode( infoNode, crPkTorquePt, entry.connInfo.pkTorquePt    );
        SaveValueToNode( infoNode, crMostNegPt,  entry.connInfo.mostNegPt     );
        SaveValueToNode( infoNode, crPtsStr,     entry.connPtsStr             );
    }
    catch( ... )
    {
        return false;
    }

    return true;
}


//
// TJob Implementation
//

__fastcall TJob::TJob( const String& jobName )
{
    // Remember the file name, and load the file
    m_fileName  = jobName;
    m_jobLoaded = false;
    m_SimMode   = false;

    // Get the basic job info. This also validates the job
    if( !::GetJobInfo( jobName, m_jobInfo ) )
        return;

    // Load the remaining elements of the job
    _di_IXMLDocument xmlDoc = NULL;

    try
    {
        xmlDoc = LoadXMLDocument( m_fileName );

        // Get the root node.
        _di_IXMLNode rootNode = xmlDoc->DocumentElement;

        if( rootNode != NULL )
        {
            GetEntryFromNode( rootNode, sJobInfoNode,   m_jobInfo );
            GetEntryFromNode( rootNode, sJobParamsNode, m_jobParams );

            m_jobWasDone = m_jobParams.isDone;

            _di_IXMLNode commentRecsNode = FindOrCreateNode( rootNode, sMainComments );
            m_mainComments.LoadFromNode( commentRecsNode );

            _di_IXMLNode sectionsNode = FindOrCreateNode( rootNode, sSectionRecs );
            m_sectionRecs.LoadFromNode( sectionsNode );

            _di_IXMLNode connsNode = FindOrCreateNode( rootNode, sConnRecs );
            m_connections.LoadFromNode( connsNode );

            _di_IXMLNode pipeInvNode = FindOrCreateNode( rootNode, sPipeInvRecs );
            m_pipeInventory.LoadFromNode( pipeInvNode );

            // Get Calibration node
            _di_IXMLNode calNode = FindOrCreateNode( rootNode, sCalibrationRecs );
            m_calibrationRecs.LoadFromNode( calNode );

            // Assume the last calibration rec has connections associated with it.
            // Note that if we wanted to be certain, we could iterate over all the
            // connection recs checking their cal rec number. But assuming the last
            // one has a cal rec is safe.
            if( m_calibrationRecs.Count > 0 )
                m_lastCalRecHasConns = true;
            else
                m_lastCalRecHasConns = false;

            BuildPipeTally();

            m_jobLoaded = true;
        }
    }
    catch( ... )
    {
        // Don't do anything - the caller will see m_jobLoaded is not
        // set when the check how the construction went
    }

    if( xmlDoc != NULL )
        xmlDoc.Release();
}


__fastcall TJob::~TJob( void )
{
}


int TJob::GetNextCreationNbr( void )
{
    // Returns a unique sequence number for a section rec, connection
    // rec, or main comment
    m_jobParams.lastCreationNbr++;

    return m_jobParams.lastCreationNbr;
}


bool TJob::Save( void )
{
    // Internal Save() handler. This function saves the file even if
    // m_jobInfo.isOpen is false, so it is up to calling functions to
    // ensure a job is open before properties are modified.

    // If in sim mode, we don't save the file but report that we did
    if( m_SimMode )
        return true;

    bool bSuccess = false;

    _di_IXMLDocument xmlDoc = NULL;

    try
    {
        xmlDoc = LoadXMLDocument( m_fileName );

        if( xmlDoc == NULL )
            Abort();

        xmlDoc->NodeIndentStr = "  ";
        xmlDoc->Options  = TXMLDocOptions() << doNodeAutoIndent << doAttrNull << doAutoPrefix << doNamespaceDecl;

        _di_IXMLNode rootNode = xmlDoc->DocumentElement;

        if( rootNode == NULL )
            Abort();

        // Update job params and rep info
        if( !SaveEntryToNode( rootNode, sJobParamsNode, m_jobParams ) )
            Abort();

        // Refresh the main comments node
        rootNode->ChildNodes->Delete( sMainComments );

        _di_IXMLNode commentsNode = FindOrCreateNode( rootNode, sMainComments );
        m_mainComments.SaveToNode( commentsNode );

        // Refresh the sections list
        rootNode->ChildNodes->Delete( sSectionRecs );

        _di_IXMLNode sectionsNode = FindOrCreateNode( rootNode, sSectionRecs );
        m_sectionRecs.SaveToNode( sectionsNode );

        // Refresh pipe inventory
        rootNode->ChildNodes->Delete( sPipeInvRecs );

        _di_IXMLNode pipeInvNode = FindOrCreateNode( rootNode, sPipeInvRecs );
        m_pipeInventory.SaveToNode( pipeInvNode );

        // Refresh the connections
        rootNode->ChildNodes->Delete( sConnRecs );

        _di_IXMLNode connsNode = FindOrCreateNode( rootNode, sConnRecs );
        m_connections.SaveToNode( connsNode );

        // Refresh the calibration
        rootNode->ChildNodes->Delete( sCalibrationRecs );

        _di_IXMLNode calNode = FindOrCreateNode( rootNode, sCalibrationRecs );
        m_calibrationRecs.SaveToNode( calNode );

        // Done adding elements - save the file now
        xmlDoc->SaveToFile( m_fileName );

        // Fall through mean succcess
        bSuccess = true;
    }
    catch( ... )
    {
    }

    if( xmlDoc != NULL )
        xmlDoc.Release();

    return bSuccess;
}


void TJob::GetInvRecActions( int iInvID, InvActionSet& actions )
{
    // Returns the allowed actions for the pipe inventory item with
    // the passed ID
    actions.Clear();

    // First, can always add a record
    actions = InvActionSet() << eIA_CanAdd;

    // Must have records for next set of actions
    if( m_pipeInventory.Count == 0 )
        return;

    // Insert / edit / delete depend on the record
    for( int iRec = 0; iRec < m_pipeInventory.Count; iRec++ )
    {
        if( m_pipeInventory[iRec].creationNbr == iInvID )
        {
            // Can insert a rec ahead of this rec if this rec is available
            if( m_pipeInventory[iRec].iisState == IIS_AVAILABLE )
                 actions = actions << eIA_CanInsert;

            // Can delete this record if it is not in use
            if( m_pipeInventory[iRec].iisState == IIS_AVAILABLE )
                 actions = actions << eIA_CanDelete;

            // Can edit this length if it is available
            if( m_pipeInventory[iRec].iisState == IIS_AVAILABLE )
                 actions = actions << eIA_CanEdit;

            // Found our rec - can break out of loop now
            break;
        }
    }
}


bool TJob::GetInvRecByID( int invRecID, PIPE_INVENTORY_ITEM& pipeInvRec )
{
    // Gets a PIPE_INVENTORY_ITEM by its ID, not its index in the list
    for( int iRec = 0; iRec < m_pipeInventory.Count; iRec++ )
    {
        if( m_pipeInventory[iRec].creationNbr == invRecID )
        {
            pipeInvRec = m_pipeInventory[iRec];
            return true;
        }
    }

    // Fall through means ID not found
    return false;
}


bool TJob::AddInventoryRec( float fLength )
{
    // Validate first...
    if( fLength <= 0.0 )
        return false;

    if( m_jobParams.isDone )
        return false;

    PIPE_INVENTORY_ITEM newItem;

    newItem.creationNbr = GetNextCreationNbr();
    newItem.ictType     = ICT_USER_CREATE;
    newItem.iLength     = (int)( fLength * 100.0 + 0.5 );
    newItem.connID      = 0;
    newItem.iisState    = IIS_AVAILABLE;

    m_pipeInventory.Append( newItem );

    // Rebuld the pipe tally now
    BuildPipeTally();

    // Okay to save all changes
    Save();

    // Broadcast notification of change
    if( m_onPipeTallyUpdated )
        m_onPipeTallyUpdated( this );

    return true;
}


bool TJob::InsertInventoryRec( int iInvIDToInsertBefore, float fLength )
{
    // Job must be open
    if( m_jobParams.isDone )
        return false;

    // Must have a valid length
    if( fLength <= 0.0 )
        return false;

    // Must have at least one record to insert before
    if( m_pipeInventory.Count == 0 )
        return false;

    // Find the index to insert before
    int iIndexToInsertBefore = -1;

    for( int iInvRec = 0; iInvRec < m_pipeInventory.Count; iInvRec++ )
    {
        if( m_pipeInventory[iInvRec].creationNbr == iInvIDToInsertBefore )
        {
            iIndexToInsertBefore = iInvRec;
            break;
        }
    }

    // Can't insert if rec not found
    if( iIndexToInsertBefore < 0 )
        return false;

    // Can't insert before a used record
    if( iIndexToInsertBefore + 1 < m_pipeInventory.Count )
    {
        if( m_pipeInventory[iIndexToInsertBefore + 1].iisState == IIS_USED )
            return false;
    }

    // Okay to insert
    PIPE_INVENTORY_ITEM newItem;

    newItem.creationNbr = GetNextCreationNbr();
    newItem.ictType     = ICT_USER_CREATE;
    newItem.iLength     = (int)( fLength * 100.0 + 0.5 );
    newItem.connID      = 0;
    newItem.iisState    = IIS_AVAILABLE;

    m_pipeInventory.Insert( iIndexToInsertBefore, newItem );

    // Rebuld the pipe tally now
    BuildPipeTally();

    // Okay to save all changes
    Save();

    // Broadcast notification of change
    if( m_onPipeTallyUpdated )
        m_onPipeTallyUpdated( this );

    return true;
}


bool TJob::ModifyInventoryRec( int iInvID, float fNewLength )
{
    // Job must be open
    if( m_jobParams.isDone )
        return false;

    // Must have a valid length
    if( fNewLength <= 0.0 )
        return false;

    // Find the record to modify
    for( int iInvRec = 0; iInvRec < m_pipeInventory.Count; iInvRec++ )
    {
        if( m_pipeInventory[iInvRec].creationNbr == iInvID )
        {
            // Found ID, can only in change an unused casing
            if( m_pipeInventory[iInvRec].iisState != IIS_AVAILABLE )
                return false;

            PIPE_INVENTORY_ITEM currItem = m_pipeInventory[iInvRec];

            currItem.iLength = (int)( fNewLength * 100.0 + 0.5 );

            m_pipeInventory.Modify( iInvRec, currItem );

            // Rebuld the pipe tally now
            BuildPipeTally();

            // Okay to save all changes
            Save();

            // Broadcast notification of change
            if( m_onPipeTallyUpdated )
                m_onPipeTallyUpdated( this );

            return true;
        }
    }

    // Fall through means ID not found
    return false;
}


bool TJob::DeleteInventoryRec( int iInvID )
{
    // Job must be open
    if( m_jobParams.isDone )
        return false;

    // Find the record to delete. We don't actually delete recs,
    // just change their state. Can only delete available recs
    for( int iInvRec = 0; iInvRec < m_pipeInventory.Count; iInvRec++ )
    {
        if( m_pipeInventory[iInvRec].creationNbr == iInvID )
        {
            // Found ID, can only in change an unused casing
            if( m_pipeInventory[iInvRec].iisState != IIS_AVAILABLE )
                return false;

            PIPE_INVENTORY_ITEM currItem = m_pipeInventory[iInvRec];

            currItem.iisState = IIS_DELETED;
            currItem.connID   = 0;

            m_pipeInventory.Modify( iInvRec, currItem );

            // Rebuld the pipe tally now
            BuildPipeTally();

            // Okay to save all changes
            Save();

            // Broadcast notification of change
            if( m_onPipeTallyUpdated )
                m_onPipeTallyUpdated( this );

            return true;
        }
    }

    // Fall through means ID not found
    return false;
}


bool TJob::GetMainComment( int whichComment, MAIN_COMMENT& comment )
{
    if( ( whichComment >= 0 ) && ( whichComment < m_mainComments.Count ) )
    {
        comment = m_mainComments[whichComment];
        return true;
    }

    return false;
}


void TJob::AddMainComment( UnicodeString commentText )
{
    if( m_jobParams.isDone )
        return;

    MAIN_COMMENT newComment;

    newComment.creationNbr = GetNextCreationNbr();
    newComment.entryTime   = Now();
    newComment.entryText   = commentText;

    m_mainComments.Append( newComment );

    Save();
}


bool TJob::GetCalibrationRec( int whichCalibrationRec, CALIBRATION_REC& calibrationRecs )
{
    if( ( whichCalibrationRec >= 0 ) && ( whichCalibrationRec < m_calibrationRecs.Count ) )
    {
        calibrationRecs = m_calibrationRecs[whichCalibrationRec];
        return true;
    }

    return false;
}


void TJob::AddCalibrationRec( CALIBRATION_REC& newCalibrationRec )
{
    if( m_jobParams.isDone )
        return;

    // Add the calibration record only if
    // - There are currently no records or
    // - This new record differs from the last one
    if( ( m_calibrationRecs.Count == 0 )
          ||
        !CalRecsEqual( m_calibrationRecs[m_calibrationRecs.Count - 1], newCalibrationRec )
      )
    {
        // We don't want to 'flood' the job file with calibration records
        // that are not associated with a connection. If the current 'last'
        // calibration rec has connections associated to it, then add this
        // new record. Otherwise, replace the last cal rec.
        if( m_lastCalRecHasConns )
        {
            newCalibrationRec.creationNbr = GetNextCreationNbr();

            m_calibrationRecs.Append( newCalibrationRec );

            m_lastCalRecHasConns = false;
        }
        else
        {
            newCalibrationRec.creationNbr = m_calibrationRecs[m_calibrationRecs.Count - 1].creationNbr;

            m_calibrationRecs.Modify( m_calibrationRecs.Count - 1, newCalibrationRec );
        }

        Save();
    }
}


static int __fastcall CompareCalListRecNbrs( void *Item1, void *Item2 )
{
    int iNumberOne = (int)Item1;
    int iNumberTwo = (int)Item2;

    if( iNumberOne > iNumberTwo )
        return 1;
    else if( iNumberOne < iNumberTwo )
        return -1;
    else
        return 0;
}


String TJob::GetCalRecConnList( int calRecCreationNbr )
{
    // Return the list of connections using this calibration record. The
    // rec ID passed is its creation number, but the connection recs
    // refer to a cal rec by its order in the list. So first find the
    // index of the cal rec with the given creation number
    String sConnListString;

    for( int iCalRec = 0; iCalRec < m_calibrationRecs.Count; iCalRec++ )
    {
        if( m_calibrationRecs[iCalRec].creationNbr == calRecCreationNbr )
        {
            // Found the rec. The connection list refers to that rec
            // by its 1-based offset.
            int iConnCalRecNbr = iCalRec + 1;

            // Now scan the connection list, saving all connections that
            // refer to that connection record.
            TList* pConnNbrList = new TList();

            for( int iConn = 0; iConn < m_connections.Count; iConn++ )
            {
                if( m_connections[iConn].connInfo.calRecNbr == iConnCalRecNbr )
                    pConnNbrList->Add( (void*)m_connections[iConn].connInfo.connNbr );
            }

            // Now order the list in ascending order
            pConnNbrList->Sort( CompareCalListRecNbrs );

            // Now remove any duplicates
            int iListIndex = 1;

            while( iListIndex < pConnNbrList->Count )
            {
                int iNbrOne = (int)( pConnNbrList->Items[iListIndex-1] );
                int iNbrTwo = (int)( pConnNbrList->Items[iListIndex] );

                if( iNbrOne == iNbrTwo )
                    pConnNbrList->Delete( iListIndex );
                else
                    iListIndex++;
            }

            // Now format the string. Where there are three or more contiguous
            // connections numbers, show them as "x - y". Otherwise, show the
            // conn numbers individually separated by commas
            int iRangeStart = 0;
            int iRangeEnd   = 0;

            for( iListIndex = 0; iListIndex < pConnNbrList->Count; iListIndex++ )
            {
                // Remember this connection number as a possible start range
                if( iRangeStart == 0 )
                {
                    iRangeStart = (int)( pConnNbrList->Items[iListIndex] );
                    iRangeEnd   = (int)( pConnNbrList->Items[iListIndex] );
                }
                else
                {
                    // We're working on a range.
                    int iCurrConnNbr = (int)( pConnNbrList->Items[iListIndex] );

                    if( iCurrConnNbr == iRangeEnd + 1 )
                    {
                        // This conn number is part of the range
                        iRangeEnd = iCurrConnNbr;
                    }
                    else
                    {
                        // This conn number is out of the range - append the
                        // range values to our string
                        if( sConnListString.Length() > 0 )
                            sConnListString += ", ";

                        if( iRangeStart == iRangeEnd )
                        {
                            sConnListString += IntToStr( iRangeStart );
                        }
                        else if( iRangeEnd == iRangeStart + 1 )
                        {
                            sConnListString += IntToStr( iRangeStart ) + ", " + IntToStr( iRangeEnd );
                        }
                        else
                        {
                            sConnListString += IntToStr( iRangeStart ) + " - " + IntToStr( iRangeEnd );
                        }

                        // Now set our range to start at this number
                        iRangeStart = iCurrConnNbr;
                        iRangeEnd   = iCurrConnNbr;
                    }
                }
            }

            // Check if the last connections were part of a range list
            if( iRangeStart > 0 )
            {
                if( sConnListString.Length() > 0 )
                    sConnListString += ", ";

                if( iRangeStart == iRangeEnd )
                {
                    sConnListString += IntToStr( iRangeStart );
                }
                else if( iRangeEnd == iRangeStart + 1 )
                {
                    sConnListString += IntToStr( iRangeStart ) + ", " + IntToStr( iRangeEnd );
                }
                else
                {
                    sConnListString += IntToStr( iRangeStart ) + " - " + IntToStr( iRangeEnd );
                }
            }

            // Done with the temp connection list
            delete pConnNbrList;

            // Can exit our search now
            break;
        }
    }
    return sConnListString;
}


bool TJob::CalRecsEqual( const CALIBRATION_REC& calibrationRec1, const CALIBRATION_REC& calibrationRec2 )
{
    if( ( calibrationRec1.housingSN        == calibrationRec2.housingSN        ) &&
        ( calibrationRec1.mfgSN            == calibrationRec2.mfgSN            ) &&
        ( calibrationRec1.fwRev            == calibrationRec2.fwRev            ) &&
        ( calibrationRec1.cfgStraps        == calibrationRec2.cfgStraps        ) &&
        ( calibrationRec1.mfgInfo          == calibrationRec2.mfgInfo          ) &&
        ( calibrationRec1.mfgDate          == calibrationRec2.mfgDate          ) &&
        ( calibrationRec1.lastCalDate      == calibrationRec2.lastCalDate      ) &&
        ( calibrationRec1.torque000Offset  == calibrationRec2.torque000Offset  ) &&
        ( calibrationRec1.torque000Span    == calibrationRec2.torque000Span    ) &&
        ( calibrationRec1.torque180Offset  == calibrationRec2.torque180Offset  ) &&
        ( calibrationRec1.torque180Span    == calibrationRec2.torque180Span    ) &&
        ( calibrationRec1.tension000Offset == calibrationRec2.tension000Offset ) &&
        ( calibrationRec1.tension000Span   == calibrationRec2.tension000Span   ) &&
        ( calibrationRec1.tension090Offset == calibrationRec2.tension090Offset ) &&
        ( calibrationRec1.tension090Span   == calibrationRec2.tension090Span   ) &&
        ( calibrationRec1.tension180Offset == calibrationRec2.tension180Offset ) &&
        ( calibrationRec1.tension180Span   == calibrationRec2.tension180Span   ) &&
        ( calibrationRec1.tension270Offset == calibrationRec2.tension270Offset ) &&
        ( calibrationRec1.tension270Span   == calibrationRec2.tension270Span   ) &&
        ( calibrationRec1.xTalkTqTq        == calibrationRec2.xTalkTqTq        ) &&
        ( calibrationRec1.xTalkTqTen       == calibrationRec2.xTalkTqTen       ) &&
        ( calibrationRec1.xTalkTenTq       == calibrationRec2.xTalkTenTq       ) &&
        ( calibrationRec1.xTalkTenTen      == calibrationRec2.xTalkTenTen      ) &&
        ( calibrationRec1.gyroOffset       == calibrationRec2.gyroOffset       ) &&
        ( calibrationRec1.gyroSpan         == calibrationRec2.gyroSpan         ) &&
        ( calibrationRec1.pressureOffset   == calibrationRec2.pressureOffset   ) &&
        ( calibrationRec1.pressureSpan     == calibrationRec2.pressureSpan     ) &&
        ( calibrationRec1.battCapacity     == calibrationRec2.battCapacity     ) &&
        ( calibrationRec1.battType         == calibrationRec2.battType         )
      )
    {
        return true;
    }

    return false;
}


String TJob::TimeString( void )
{
    return m_jobInfo.jobStartTime.FormatString( "hh:nn:ss" );
}


UnicodeString TJob::DateString( void )
{
    return m_jobInfo.jobStartTime.FormatString( "yyyy-mm-dd" );
}


UnicodeString TJob::DateTimeString( void )
{
    return m_jobInfo.jobStartTime.FormatString( "yyyy-mm-dd hh:nn:ss" );
}


void TJob::GetJobInfo( JOB_INFO& jobInfo )
{
    jobInfo = m_jobInfo;
}


bool TJob::GetSectionRec( int whichSection, SECTION_INFO& sectionInfo )
{
    if( ( whichSection < 0 ) || ( whichSection >= m_sectionRecs.Count ) )
        return false;

    sectionInfo = m_sectionRecs[whichSection];

    return true;
}


void TJob::AddSectionRec( SECTION_INFO& newSectionInfo )
{
    // Can't add sections if the job is done
    if( m_jobParams.isDone )
        return;

    newSectionInfo.creationNbr = GetNextCreationNbr();
    newSectionInfo.sectionNbr  = m_sectionRecs.Count + 1;

    m_sectionRecs.Append( newSectionInfo );

    m_jobParams.currSectIndex = m_sectionRecs.Count - 1;

    Save();
}


bool TJob::EditSectionRec( SECTION_INFO& sectionInfo )
{
    // Can't edit a section if the job is done
    if( m_jobParams.isDone )
        return false;

    // Can't edit sections that have connections
    if( GetSectHasConns( sectionInfo.sectionNbr ) )
        return false;

    // Find this section and edit if
    for( int iSect = 0; iSect < m_sectionRecs.Count; iSect++ )
    {
        if( m_sectionRecs[iSect].sectionNbr == sectionInfo.sectionNbr )
        {
            m_sectionRecs.Modify( iSect, sectionInfo );

            Save();

            return true;
        }
    }

    // Fall through means this section was not found
    return false;
}


bool TJob::CanRemoveSection( int iSectionNbr )
{
    // Cannot remove is the job is not yet done
    if( m_jobParams.isDone )
        return false;

    // Check if the Section has had any connections previously
    if( GetSectHasConns( iSectionNbr ) )
        return false;

    // Fall through means it is safe to delete
    return true;
}


bool TJob::RemoveLastSection( void )
{
    // Remove the last Section and decrement the current index
    m_sectionRecs.Del( m_jobParams.currSectIndex );
    m_jobParams.currSectIndex--;

    Save();

    return true;
}


bool TJob::SyncToSection( int sectionNbr )
{
    // Search for the section record that matches the passed section number,
    // and make that one the current section. Note that this function can
    // only be used to advance the current section index.
    bool currSectionChanged = false;

    for( int iSect = 0; iSect < m_sectionRecs.Count; iSect++ )
    {
        if( m_sectionRecs[iSect].sectionNbr == sectionNbr )
        {
            if( iSect > m_jobParams.currSectIndex )
            {
                m_jobParams.currSectIndex = iSect;
                currSectionChanged        = true;
            }

            break;
        }
    }

    return currSectionChanged;
}


int TJob::GetNextConnectionNbr( void )
{
    // Next connection number is based on the pipe tally.
    // Check for special cases first
    if( m_pipeTally.Count <= 0 )
        return m_jobInfo.firstConnNbr;

    // Return the last connection in the pipe tally. The pipe tally has
    // unused casing inventory at the end of the list, so skip over any
    // entries whose connection number is zero.
    for( int iTallyIndex = m_pipeTally.Count-1; iTallyIndex >= 0; iTallyIndex-- )
    {
        if( m_pipeTally[iTallyIndex].iConnNbr > 0 )
            return m_pipeTally[iTallyIndex].iConnNbr + 1;
    }

    // Fall through means all inventory is currently unused
    return m_jobInfo.firstConnNbr;
}


bool TJob::GetConnection( int whichConn, CONNECTION_INFO& connectionInfo )
{
    if( ( whichConn >= 0 ) && ( whichConn < m_connections.Count ) )
    {
        connectionInfo = m_connections[whichConn].connInfo;
        return true;
    }

    return false;
}


bool TJob::GetConnectionByID( int connID, CONNECTION_INFO& connectionInfo )
{
    for( int iConn = 0; iConn < m_connections.Count; iConn++ )
    {
        if( m_connections[iConn].connInfo.creationNbr == connID )
        {
            connectionInfo = m_connections[iConn].connInfo;
            return true;
        }
    }

    // Fall through means ID not found
    return false;
}


int TJob::GetConnectionPts( int connID, WTTTS_READING pReadings[], int maxPts )
{
    // Fill pBuffer with up to maxPts of WTTTS_READING records. If pBuffer,
    // is NULL, return the size (in array elements) required for pBuffer.
    for( int iConn = 0; iConn < m_connections.Count; iConn++ )
    {
        if( m_connections[iConn].connInfo.creationNbr == connID )
        {
            String ptsStr = m_connections[iConn].connPtsStr;

            return WTTTSStringToArray( ptsStr, pReadings, maxPts );
        }
    }

    // Fall through means ID not found
    return 0;
}


void TJob::AddConnection( CONNECTION_INFO& newConnection, const WTTTS_READING pReadings[], int nbrReadings, bool bKeepInInventoryOnFail )
{
    if( m_jobParams.isDone )
        return;

    // Update the members of the struct that are managed by the job.
    newConnection.creationNbr = GetNextCreationNbr();
    newConnection.sectionNbr  = m_jobParams.currSectIndex + 1;
    newConnection.dateTime    = Now();
    newConnection.seqNbr      = GetNextConnSeqNbr( newConnection.connNbr );

    // Assign the current calibration record to this connection. A connection
    // always uses the last calibration record saved in the job. Also set the
    // flag indicating the cal rec has been 'used'
    newConnection.calRecNbr = m_calibrationRecs.Count;

    m_lastCalRecHasConns = true;

    // Now create the record that gets saved to the job file
    CONNECTION_REC newConnRec;

    newConnRec.connInfo   = newConnection;
    newConnRec.connPtsStr = WTTTSArrayToString( pReadings, nbrReadings );

    m_connections.Append( newConnRec );

    // For a passed connection, or a failed-one where re-use has been
    // requested, associate the next pipe inventory rec to this connection.
    // If no pipe inventory exists, auto-create a rec and associate it to
    // this connection
    bool bUseInventory = false;

    if( ( newConnection.crResult == CR_PASSED ) || ( newConnection.crResult == CR_FORCE_PASSED ) )
    {
        bUseInventory = true;
    }
    else if( ( newConnection.crResult == CR_FAILED ) || ( newConnection.crResult == CR_FORCE_FAILED ) )
    {
        bUseInventory = !bKeepInInventoryOnFail;
    }

    if( bUseInventory )
    {
        int iNextFreeInvRec = -1;

        for( int iInvRec = m_pipeInventory.Count - 1; iInvRec >= 0; iInvRec-- )
        {
            // If this record is available, remember it. Otherwise, if the record
            // is used then we stop our search. The only other state will be
            // deleted, and we skip over those records in our search
            if( m_pipeInventory[iInvRec].iisState == IIS_AVAILABLE )
                iNextFreeInvRec = iInvRec;
            else if( m_pipeInventory[iInvRec].iisState == IIS_USED )
                break;
        }

        // If we didn't find a rec, append a new one to the inventory list
        if( iNextFreeInvRec < 0 )
        {
            PIPE_INVENTORY_ITEM newItem;

            newItem.creationNbr = GetNextCreationNbr();
            newItem.ictType     = ICT_AUTO_CREATE;
            newItem.iLength     = (int)( newConnection.length * 100.0 + 0.5 );
            newItem.connID      = newConnection.creationNbr;
            newItem.iisState    = IIS_USED;

            m_pipeInventory.Append( newItem );
        }
        else
        {
            // Have a free inventory item - update its state
            PIPE_INVENTORY_ITEM currItem = m_pipeInventory[iNextFreeInvRec];

            // Always update the length of the inventory item with that
            // input by the user
            currItem.iLength  = (int)( newConnection.length * 100.0 + 0.5 );
            currItem.connID   = newConnection.creationNbr;
            currItem.iisState = IIS_USED;

            m_pipeInventory.Modify( iNextFreeInvRec, currItem );
        }
    }

    // Rebuld the pipe tally now
    BuildPipeTally();

    // Okay to save all changes
    Save();

    // Broadcast notification of changes
    if( m_onConnectionsUpdated )
        m_onConnectionsUpdated( this );

    if( m_onPipeTallyUpdated )
        m_onPipeTallyUpdated( this );
}


bool TJob::CanRemove( int connID, String& sErrorMessage )
{
    // Validate the connection info
    if( connID == 0 )
    {
        sErrorMessage = "The connection is not valid.";
        return false;
    }

    // Cannot remove connections from a closed job
    if( m_jobParams.isDone )
    {
        sErrorMessage = "Job must be open to remove a connection.";
        return false;
    }

    // The pipe tally has the updated list of connections. Only the
    // last connection in it can be removed.
    if( m_pipeTally.Count == 0 )
    {
        sErrorMessage = "There are no passed connections to remove.";
        return false;
    }

    // Find the last last used item in the pipe tally. The pipe tally
    // may have records for casings that have not yet been used in the
    // drill stem, so need to skip over those.
    int iLastConnRecID = 0;

    for( int iCasingIndex = m_pipeTally.Count - 1; iCasingIndex >= 0; iCasingIndex-- )
    {
        if( m_pipeTally[iCasingIndex].iConnInfoRecNbr != 0 )
        {
            iLastConnRecID = m_pipeTally[iCasingIndex].iConnInfoRecNbr;
            break;
        }
    }

    // This should never happen, but just in case...
    if( iLastConnRecID <= 0 )
    {
        sErrorMessage = "There are no passed connections to remove.";
        return false;
    }

    // The last used tally rec must be the connection rec passed
    if( iLastConnRecID != connID )
    {
        sErrorMessage = "You can only remove the last passed connection.";
        return false;
    }

    // Fall through means success. No need to test the connection result
    // because for the connection to have been in the pipe tally it must
    // have been in either the passed or force-passed state.
    return true;
}


bool TJob::RemoveConnection( int connID, const String& comment, bool bAddToPipeTally )
{
    // Removes a previous connection. The connection number must be the last
    // (numerically) passed connection, the comment cannot be empty, and the
    // last record for this connection must indicate a pass.
    String sRemoveError;

    if( !CanRemove( connID, sRemoveError ) )
        return false;

    if( Trim( comment ).Length() == 0 )
        return false;

    // Find this connection.
    int iConnIndex = -1;
    CONNECTION_REC connRec;

    for( int iIndex = 0; iIndex < m_connections.Count; iIndex++ )
    {
        connRec = m_connections[iIndex];

        if( connRec.connInfo.creationNbr == connID )
        {
            // Found this entry in the list. Can only remove it if it is in
            // a passed state
            if( ( connRec.connInfo.crResult == CR_PASSED ) || ( connRec.connInfo.crResult == CR_FORCE_PASSED ) )
                iConnIndex = iIndex;

            break;
        }
    }

    // If no removable connection was found, return false
    if( iConnIndex < 0 )
        return false;

#if 0
// TODO - confirm with BD that the section number should *not* be changed
    // Back up the job's section number to match this connection's section
    if( connRec.connInfo.sectionNbr > 0 )
        m_jobParams.currSectIndex = connRec.connInfo.sectionNbr - 1;
    else
        m_jobParams.currSectIndex = 0;
#endif

    connRec.connInfo.creationNbr = GetNextCreationNbr();
    connRec.connInfo.dateTime    = Now();
    connRec.connInfo.crResult    = CR_REMOVED;
    connRec.connInfo.comment     = Trim( comment );
    connRec.connInfo.seqNbr      = GetNextConnSeqNbr( connRec.connInfo.connNbr );

    // Add a 'removed' record that has all the information of the rec
    // being removed, but with no data points.
    connRec.connPtsStr = "";

    m_connections.Append( connRec );

    // Update the pipe inventory and possibly the pipe tally.
    // Scan the pipe inventory records for one that is associated
    // to this connection. If found, update it
    bool bFoundInInventory = false;

    for( int iInvRec = 0; iInvRec < m_pipeInventory.Count; iInvRec++ )
    {
        // If this re  cord is available, remember it. Otherwise, if the record
        // is used then we stop our search. The only other state will be
        // deleted, and we skip over those records in our search
        if( m_pipeInventory[iInvRec].connID == connID )
        {
             PIPE_INVENTORY_ITEM currInvItem = m_pipeInventory[iInvRec];

             currInvItem.connID   = 0;
             currInvItem.iisState = bAddToPipeTally ? IIS_AVAILABLE : IIS_DELETED;

             m_pipeInventory.Modify( iInvRec, currInvItem );

             bFoundInInventory = true;

             // Theoretically we should be able to break out of our search
             // here, as there should only ever be one pipe inventory record
             // associated with a given connection. But keep searching just
             // in case.
        }
    }

    // If this connection wasn't found in the inventory, then make it the
    // next available inventory record for use
    if( !bFoundInInventory && bAddToPipeTally )
    {
        PIPE_INVENTORY_ITEM newInvItem;

        newInvItem.creationNbr = GetNextCreationNbr();
        newInvItem.ictType     = ICT_AUTO_CREATE;
        newInvItem.iLength     = (int)( connRec.connInfo.length * 100 + 0.05 );
        newInvItem.connID      = 0;
        newInvItem.iisState    = IIS_AVAILABLE;

        if( m_pipeInventory.Count == 0 )
        {
            m_pipeInventory.Append( newInvItem );
        }
        else
        {
            // Find the last available rec, and insert ahead of it
            int iInsertBefore = -1;

            for( int iInvRec = m_pipeInventory.Count - 1; iInvRec >= 0; iInvRec-- )
            {
                // If this re  cord is available, remember it. Otherwise, if the record
                // is used then we stop our search. The only other state will be
                // deleted, and we skip over those records in our search
                if( m_pipeInventory[iInvRec].iisState == IIS_AVAILABLE )
                    iInsertBefore = iInvRec;
                else if( m_pipeInventory[iInvRec].iisState == IIS_USED )
                    break;
            }

            // If we found a slot, insert it. Otherwise we have to append
            // this record to the end of the list
            if( iInsertBefore >= 0 )
                 m_pipeInventory.Insert( iInsertBefore, newInvItem );
            else
                 m_pipeInventory.Append( newInvItem );

        }
    }

    // Rebuld the pipe tally now
    BuildPipeTally();

    // Now save all changes
    Save();

    // Broadcast notification of changes
    if( m_onConnectionsUpdated )
        m_onConnectionsUpdated( this );

    if( m_onPipeTallyUpdated )
        m_onPipeTallyUpdated( this );

    return true;
}


bool TJob::GetSectHasConns( int whichSection )
{
    // Return true if the passed section number (not section index)
    // has any connections.
    for( int iIndex = 0; iIndex < m_connections.Count; iIndex++ )
    {
        if( m_connections[iIndex].connInfo.sectionNbr == whichSection )
            return true;
    }

    // Fall through means this section has no connections
    return false;
}


int TJob::GetNextConnSeqNbr( int iConnNbr )
{
    // Returns the next sequence number to use for a given connection.
    // First sequence nbr is always zero, so init our return var so that
    // zero is returned if this is the first of this connection number.
    int iLastSeqNbr = -1;

    for( int iRec = 0; iRec < m_connections.Count; iRec++ )
    {
        if( m_connections[iRec].connInfo.connNbr == iConnNbr )
        {
            if( m_connections[iRec].connInfo.seqNbr > iLastSeqNbr )
                iLastSeqNbr = m_connections[iRec].connInfo.seqNbr;
        }
    }

    return iLastSeqNbr + 1;
}


bool TJob::GetCanAutoRecord( void )
{
    // We can auto-record as long as there are pipe inventory records
    // that are in the available state and the auto-rec feature is
    // turned on
    if( !GetAutoRecordEnabled() || ( m_pipeInventory.Count == 0 ) || m_jobParams.isDone )
        return false;

    // Auto-rec feature enabled and there is inventory. Report based on the
    // state of the last item.
    return( m_pipeInventory[m_pipeInventory.Count - 1].iisState == IIS_AVAILABLE );
}


float TJob::MaxCasingLength( UNITS_OF_MEASURE uom )
{
    // Gets a max casing length a user can enter based on UOM
    if( uom == UOM_IMPERIAL )
        return 100.0;
    else
        return 30.0;
}


float TJob::GetPipeTallyLength( void )
{
    // Returns the total length of all currently used pipe casings
    float fTotDist = 0.0;

    for( int iTallyConn = 0; iTallyConn < m_pipeTally.Count; iTallyConn++ )
    {
        if( m_pipeTally[iTallyConn].iConnInfoRecNbr != 0 )
        {
            CONNECTION_INFO connectionInfo;

            if( GetConnectionByID( m_pipeTally[iTallyConn].iConnInfoRecNbr, connectionInfo ) )
                fTotDist += connectionInfo.length;
        }
    }

    return fTotDist;
}


int TJob::GetNbrPassedTallyRecs( void )
{
    // Returns the total number of all currently used pipe casings
    int iPassedCasings = 0;

    for( int iTallyConn = 0; iTallyConn < m_pipeTally.Count; iTallyConn++ )
    {
        if( m_pipeTally[iTallyConn].iConnInfoRecNbr != 0 )
            iPassedCasings++;
    }

    return iPassedCasings;
}


void TJob::BuildPipeTally( void )
{
    // This is called when a job is initially opened to ensure the
    // pipe tally list is sync'd with the connection list.
    //
    // Process:
    //   1. Build a temp tally list that contains the set of the
    //      last passed connections. The list is sorted in ascending
    //      order of connection number. Note that when connections
    //      are added to the connections list, the sequence number
    //      increments. So implicitly, we don't need to check sequence
    //      numbers when doing this.
    //
    //   2. Merge the passed connections list with any pipe inventory
    //      records in the job. Merge is done via the connID number.
    //
    //   3. For any merged records, update the length of the inventory
    //      record with the length as reported in the connection. This
    //      is because a user can override preloaded inventory lengths
    //      with actual lengths as measured while drilling.

    // First, update the sequence numbers in the pipe inventory list
    // in case there's been a change there
    for( int iRec = 0; iRec < m_pipeInventory.Count; iRec++ )
    {
        PIPE_INVENTORY_ITEM invItem = m_pipeInventory[iRec];

        invItem.iSeqNbr = iRec + 1;

        m_pipeInventory.Modify( iRec, invItem );
    }

    // Clear the current pipe tally list
    m_pipeTally.Clear();

    // Find the largest connection number. This isn't necessarily
    // the last connection in the list.
    int iMaxConnNbr = 0;

    for( int iConn = 0; iConn < m_connections.Count; iConn++ )
    {
        if( m_connections[iConn].connInfo.connNbr > iMaxConnNbr )
            iMaxConnNbr = m_connections[iConn].connInfo.connNbr;
    }

    // Now go through the connection list in increasing connection
    // number. Find the last record by seq nbr for each connection,
    // which will be found by going through the conn list backwards.
    // In theory, we only need to search until we find the first
    // non-passed connection. However, because of a legacy code bug
    // it is possible to have future passed connection. So scan
    // through all connections to the max conn nbr.
    for( int iConnNbr = 1; iConnNbr <= iMaxConnNbr; iConnNbr++ )
    {
        for( int iConn = m_connections.Count - 1; iConn >= 0; iConn-- )
        {
            CONNECTION_INFO currConn = m_connections[iConn].connInfo;

            if( currConn.connNbr == iConnNbr )
            {
                if( ( currConn.crResult == CR_PASSED ) || ( currConn.crResult == CR_FORCE_PASSED ) )
                {
                    PIPE_TALLY_REC newRec;

                    newRec.iPipeInvRecNbr  = 0;
                    newRec.iInvSeqNbr      = 0;
                    newRec.iConnInfoRecNbr = currConn.creationNbr;
                    newRec.iConnNbr        = currConn.connNbr;
                    newRec.iSection        = currConn.sectionNbr;
                    newRec.fLength         = currConn.length;
                    newRec.dtConn          = currConn.dateTime;

                    m_pipeTally.Append( newRec );
                }

                // Quit our search upon finding a rec with the matching conn nbr
                break;
            }
        }
    }

#ifdef USE_OLD_BUILD_TALLY_METHOD
    // Add passed connections. Work through the connection list
    // backwards and only add a connection if it is good and that
    // connection number isn't already in the list.
    for( int iConn = m_connections.Count - 1; iConn >= 0; iConn-- )
    {
        CONNECTION_INFO currConn = m_connections[iConn].connInfo;

        if( ( currConn.crResult != CR_PASSED ) && ( currConn.crResult != CR_FORCE_PASSED ) )
            continue;

        // Scan for the pipe tally list for this connection. Note that at
        // this point in the code the a pipe tally rec will never have a
        // NULL connection rec pointer, so we don't need to check for that.
        bool connFound = false;

        for( int iTallyConn = 0; iTallyConn < m_pipeTally.Count; iTallyConn++ )
        {
            CONNECTION_INFO tempConnInfo;

            if( GetConnectionByID( m_pipeTally[iTallyConn].iConnInfoRecNbr, tempConnInfo ) )
            {
                if( tempConnInfo.connNbr == currConn.connNbr )
                {
                    connFound = true;
                    break;
                }
            }
        }

        // If the connection hasn't been found, insert a new record into the
        // start of the list
        if( !connFound )
        {
            PIPE_TALLY_REC newRec;

            newRec.iPipeInvRecNbr  = 0;
            newRec.iInvSeqNbr      = 0;
            newRec.iConnInfoRecNbr = currConn.creationNbr;
            newRec.iConnNbr        = currConn.connNbr;
            newRec.iSection        = currConn.sectionNbr;
            newRec.fLength         = currConn.length;
            newRec.dtConn          = currConn.dateTime;

            if( m_pipeTally.Count == 0 )
                m_pipeTally.Append( newRec );
            else
                m_pipeTally.Insert( 0, newRec );
        }
    }
#endif

    // Next, merge in any pipe inventory we may have left over. Scan
    // through the pipe inventory until we find the first available
    // item. Add all items after that, except for deleted items
    int iFirstAvailRec = -1;

    for( int iInvRec = 0; iInvRec < m_pipeInventory.Count; iInvRec++ )
    {
        if( m_pipeInventory[iInvRec].iisState == IIS_AVAILABLE )
        {
            iFirstAvailRec = iInvRec;
            break;
        }
    }

    // Add all recs from the first available one
    if( iFirstAvailRec >= 0 )
    {
        for( int iInvRec = iFirstAvailRec; iInvRec < m_pipeInventory.Count; iInvRec++ )
        {
            if( m_pipeInventory[iInvRec].iisState == IIS_DELETED )
                continue;

            PIPE_TALLY_REC newRec;

            newRec.iPipeInvRecNbr  = m_pipeInventory[iInvRec].creationNbr;
            newRec.iInvSeqNbr      = m_pipeInventory[iInvRec].iSeqNbr;
            newRec.iConnInfoRecNbr = 0;
            newRec.iConnNbr        = 0;
            newRec.iSection        = 0;
            newRec.fLength         = (float) m_pipeInventory[iInvRec].iLength / 100.0;
            newRec.dtConn.Val      = 0.0;

            m_pipeTally.Append( newRec );
        }
    }
}


bool TJob::GetTallyRec( int tallyIndex, PIPE_TALLY_REC& tallyRec )
{
    if( ( tallyIndex >= 0 ) && ( tallyIndex < m_pipeTally.Count ) )
    {
        tallyRec = m_pipeTally[tallyIndex];
        return true;
    }

    // Fall through means a bad index passed
    return false;
}


bool TJob::GetHavePipeInv( void )
{
    // Returns true if there is at least one pipe is available
    // for use in the pipe inventory
    if( m_pipeInventory.Count == 0 )
        return false;

    // Scan backwards through the list. We ignore deleted records. If
    // the first non-deleted rec we find is available, return true.
    // Otherwise the rec will be used so return false.
    for( int iInvIndex = m_pipeInventory.Count - 1; iInvIndex >= 0; iInvIndex-- )
    {
        if( m_pipeInventory[iInvIndex].iisState == IIS_AVAILABLE )
            return true;
        else if( m_pipeInventory[iInvIndex].iisState == IIS_USED )
            return false;
    }

    // Fall through means we have no inventory
    return false;
}


float TJob::GetNextPipeLength( void )
{
    // Returns the length of the next pipe available in inventory.
    // Returns 0.0 if no inventory is available
    if( m_pipeInventory.Count == 0 )
        return 0.0;

    float fNextLength = 0.0;

    // Scan backwards through the list. We ignore deleted records. Save
    // the length of the last
    for( int iInvIndex = m_pipeInventory.Count - 1; iInvIndex >= 0; iInvIndex-- )
    {
        if( m_pipeInventory[iInvIndex].iisState == IIS_AVAILABLE )
            fNextLength = m_pipeInventory[iInvIndex].iLength;
        else if( m_pipeInventory[iInvIndex].iisState == IIS_USED )
            break;
    }

    // Fall through means we have no inventory
    return fNextLength / 100.0;;
}


void TJob::SetJobStatus( bool newStatus )
{
    if( m_jobParams.isDone != newStatus )
    {
        m_jobParams.isDone = newStatus;
        Save();
    }
}


void TJob::SaveBackup( void )
{
    // Check if the job was done at launch and is now done, return if both are true
    if( ( m_jobParams.isDone ) && ( m_jobWasDone ) )
        return;

    // Get the backup enabled flag, and the filepath
    String sBackupDir;

    if( GetForceBackup( sBackupDir ) )
    {
        if( sBackupDir != "" )
        {
            // Get the filename, use it to create the full backup path
            String sFileName   = ExtractFileName( m_fileName );
            String sBackupPath = IncludeTrailingBackslash( sBackupDir ) + sFileName;

            // Create the filepath if it does not exist
            if( !CreateDirectory( sBackupDir.w_str(), NULL ) && ( GetLastError() != ERROR_ALREADY_EXISTS ) )
                return;

            // Delete the backup file if it already exists
            if( !DeleteFile( sBackupPath.w_str() ) && ( GetLastError() != ERROR_FILE_NOT_FOUND ) )
                return;

            // Copy over the current job file to the backup dir
            CopyFile( m_fileName.w_str(), sBackupPath.w_str(), true );
        }
    }
}

