#ifndef JobManagerH
#define JobManagerH

    //
    // This file implements support routines for creating, enumerating,
    // and updating job files.
    //

    #include <time.h>
    #include <XMLIntf.hpp>

    #include "WTTTSDefs.h"
    #include "UnitsOfMeasure.h"

    // The JOB_INFO contains 'unchangeable' information about a job.
    typedef struct {
        String    clientName;
        String    location;
        TDateTime jobStartTime;
        int       firstConnNbr;
        String    sCasingsFileName;    // If non-blank, name of casings data file loaded at time of job creation
        String    sCasingsList;        // CSV list of casing lengths, in units of 100's of UOM in effect
        UNITS_OF_MEASURE unitsOfMeasure;
    } JOB_INFO;

    // JOB_PARAMS contains information about a job that can change
    // through the job
    typedef struct {
        bool             isDone;
        int              lastCreationNbr;
        int              currSectIndex;     // Index into the sections array, not a section number
    } JOB_PARAMS;

    // The JOB_LIST_ITEM is used by TJobList to describe each job in the list
    typedef struct {
        JOB_INFO      jobInfo;
        UnicodeString jobFileName;
    } JOB_LIST_ITEM;

    typedef struct {
        int       creationNbr;
        int       sectionNbr;
        bool      autoRecordOn;
        String    clientRep;
        String    tescoTech;
        String    threadRep;
        String    casingMfgr;
        String    casingName;
        String    casingType;
        String    casingGrade;
        float     casingDia;
        float     casingWeight;
        bool      hasShoulder;
        float     shoulderMinTorque;
        float     shoulderMaxTorque;
        float     shoulderPostTurns;
        float     shoulderDelta;
        bool      autoShldrEnabled;
        float     peakTargetMinTorque;
        float     peakTargetMaxTorque;
        float     peakTargetOptTorque;
        float     peakTargetTurns;
        float     peakTargetDelta;
        float     peakTargetHold;
        float     peakTargetOvershoot;
    } SECTION_INFO;


    //
    // Pipe Inventory
    //

    typedef enum {
        IIS_AVAILABLE,     // Available for use
        IIS_USED,          // Is used in the currently drill stem, connID is valid
        IIS_DELETED,       // Has been deleted from available inventory of casings
    } INV_ITEM_STATE;

    typedef enum {
       ICT_INIT_LOAD,      // Inventory item loaded when job first created
       ICT_AUTO_CREATE,    // Inventory item created as job was proceeding, with no remaining inv items available
       ICT_USER_CREATE     // User-created inventory item, after job was created
    } INV_CREATE_TYPE;

    typedef struct {
        int              creationNbr;
        int              iSeqNbr;        // Not saved, created a run time and used for sorting
        INV_CREATE_TYPE  ictType;
        int              iLength;        // Length of casing, in 100's of job UOM
        int              connID;         // Connection associated by this item. Only valid if iisState == IIS_USED
        INV_ITEM_STATE   iisState;
    } PIPE_INVENTORY_ITEM;


    //
    // Connections
    //

    typedef enum {
       CR_PASSED,
       CR_FAILED,
       CR_FORCE_PASSED,
       CR_FORCE_FAILED,
       CR_REMOVED,
       NBR_CONN_RESULTS
    } CONN_RESULT;

    extern const String ConnResultText[NBR_CONN_RESULTS];

    typedef Set <CONN_RESULT, CR_PASSED, CR_REMOVED> ConnResultSet;

    CONN_RESULT ConnResultFromTextEnum( const String& connText );

    // The CONNECTION_INFO struct is used internally for passing
    // connection info between modules. The CONNECTION_REC struct
    // is used only by JobManager to store all information about
    // a connection.
    typedef struct {
        int              creationNbr;  // Unique index among all connections
        int              connNbr;      // Connection nbr, may be repeated in list
        int              seqNbr;       // Seq nbr: increments for each connection attempt
        bool             autoRecConn;  // True if created through auto-record mechanism, false for user created
        int              sectionNbr;
        int              calRecNbr;
        float            battVolts;
        float            length;
        bool             shoulderSet;
        float            shoulderPt;   // In turns
        float            shoulderTq;   // In job units
        float            mostNegPt;    // In turns; records if unit was rotated counter-clockwise before connection started
        float            pkTorque;     // In job units
        float            pkTorquePt;   // In turns
        TDateTime        dateTime;
        CONN_RESULT      crResult;
        UnicodeString    comment;
    } CONNECTION_INFO;

    typedef struct {
        CONNECTION_INFO  connInfo;
        UnicodeString    connPtsStr;
    } CONNECTION_REC;


    //
    // Pipe Tally Records
    //

    typedef struct {                // Note: both rec nbrs will never be 0 in a given record
        int       iPipeInvRecNbr;   // Pipe inventory rec creation nbr if > 0, otherwise not assoc'd with a pipe inv rec
        int       iConnInfoRecNbr;  // Conn rec creation nbr if > 0, otherwise not assoc'd with a conn rec
        int       iInvSeqNbr;       // Valid if iPipeInvRecNbr > 0, used for sorting only
        int       iConnNbr;         // Valid if iConnInfoRecNbr > 0
        int       iSection;         // Valid if iConnInfoRecNbr > 0
        float     fLength;          // Always valid
        TDateTime dtConn;           // Valid if iConnInfoRecNbr > 0
    } PIPE_TALLY_REC;


    //
    // Other Records
    //

    typedef struct {
        int        creationNbr;
        TDateTime  entryTime;
        String     entryText;
    } MAIN_COMMENT;

    typedef struct {
        int       creationNbr;
        String    housingSN;
        String    mfgSN;
        String    fwRev;
        String    cfgStraps;
        String    mfgInfo;
        String    mfgDate;
        String    lastCalDate;
        String    torque000Offset;
        String    torque000Span;
        String    torque180Offset;
        String    torque180Span;
        String    tension000Offset;
        String    tension000Span;
        String    tension090Offset;
        String    tension090Span;
        String    tension180Offset;
        String    tension180Span;
        String    tension270Offset;
        String    tension270Span;
        String    xTalkTqTq;
        String    xTalkTqTen;
        String    xTalkTenTq;
        String    xTalkTenTen;
        String    gyroOffset;
        String    gyroSpan;
        String    pressureOffset;
        String    pressureSpan;
        String    battCapacity;
        String    battType;
    } CALIBRATION_REC;


    // TJobList manages a list of jobs. The object can be constructed by
    // passing a directory that contains a list of jobs, or passing a
    // list of file names of jobs.
    class TJobList : public TObject
    {
        private:

        protected:
            TList* pJobList;

            __fastcall int GetJobCount( void ) { return pJobList->Count; }

        public:

            virtual __fastcall TJobList( const String& dataDir );
            virtual __fastcall TJobList( TStrings* fileNameList );

            virtual __fastcall ~TJobList( void );

            __property int Count = { read = GetJobCount };

            bool GetItem( int index, JOB_LIST_ITEM& jobItem );
            void DeleteItem( int index );
    };

    int JobsExistFor( UnicodeString dataDir, UnicodeString clientName, UnicodeString location );
        // Returns how many jobs exists for the passed client and location pair
        // in the dataDir. If none exist, return zero.

    bool GetJobInfo( UnicodeString jobFileName, JOB_INFO& jobInfo );
        // Returns true if the job name exists and populates the jobInfo struct.
        // Returns false if the job is not found.

    bool CreateNewJob( UnicodeString& jobFileName, UnicodeString dataDir,
                       const JOB_INFO& jobInfo, const SECTION_INFO& sectionInfo );
        // Creates a new job with the passed parameters in the current data dir


    bool ConnGood( const CONNECTION_INFO* pConn );
        // Returns true if the passed connection is good

    // Define a template class to manage the arrays of a structure type
    template <class T> class TStructArray
    {
      private:
        int m_allocElements;      // number of elements memory currently allocated for
        int m_allocIncrease;      // incremental size of memory added when more memory needed
        T*  m_pData;              // pointer to the element array
        int m_nbrElements;        // number of elements in the array (<= m_allocElements)

        void CheckAndAllocMem( int nbrElements );

      public:
        TStructArray();
        ~TStructArray();

        const T& operator[] (int i) const { return m_pData[i]; }

        TStructArray& operator  = ( const TStructArray& equ );
        TStructArray& operator += ( const TStructArray& plus );

        void Clear( void ) { m_nbrElements = 0; }

        void Append( const T &add );
        void Insert( int index, const T& insert );
        void Del( int index );
        void Modify( int index, const T& modify );

        void LoadFromNode( _di_IXMLNode aNode );
        void SaveToNode( _di_IXMLNode aNode );

        __property int Count = { read = m_nbrElements };
    };

    class TJob : public TObject
    {
      private:

        UnicodeString m_fileName;
        bool          m_jobLoaded;

        bool          m_jobWasDone;

        JOB_INFO      m_jobInfo;
        JOB_PARAMS    m_jobParams;

        bool          m_lastCalRecHasConns;

        bool          m_SimMode;

        static TNotifyEvent m_onPipeTallyUpdated;
        static TNotifyEvent m_onConnectionsUpdated;

        UNITS_OF_MEASURE GetUOM( void ) { return m_jobInfo.unitsOfMeasure; }

        bool GetCanAutoRecord( void );

        TStructArray<SECTION_INFO> m_sectionRecs;
        int GetNbrSections( void ) { return m_sectionRecs.Count; }

        TStructArray<MAIN_COMMENT> m_mainComments;
        int GetNbrMainComments( void ) { return m_mainComments.Count; }

        TStructArray<CALIBRATION_REC> m_calibrationRecs;
        int GetNbrCalibrationRecs( void ) { return m_calibrationRecs.Count; }

        TStructArray<CONNECTION_REC> m_connections;
        int GetNbrConnectionRecs( void ) { return m_connections.Count; }

        TStructArray<PIPE_TALLY_REC> m_pipeTally;
        int GetTotalNbrTallyRecs( void ) { return m_pipeTally.Count; }

        TStructArray<PIPE_INVENTORY_ITEM> m_pipeInventory;
        int GetNbrPipeInvRecs( void ) { return m_pipeInventory.Count; }

        bool CalRecsEqual( const CALIBRATION_REC& calibrationRec1, const CALIBRATION_REC& calibrationRec2 );

      protected:
        // Helper functions that load / save a jobs properties from / to an XML doc
        void LoadFromXML( _di_IXMLDocument& xmlDoc );
        void SaveToXML( _di_IXMLDocument& xmlDoc );

        int GetNextCreationNbr( void );
            // Returns a unique sequence number for a section rec, connection
            // rec, or main comment

        bool Save( void );
            // Internal Save() handler. This function saves the file even if
            // m_jobInfo.isOpen is false, so it is up to calling functions to
            // ensure a job is open before properties are modified.

        bool GetSectHasConns( int whichSection );
            // Return true if the passed section number (not section index)
            // has any connections.

        int  GetNextConnSeqNbr( int iConnNbr );
            // Returns the next sequence number to use for a given connection

        float GetPipeTallyLength( void );
        int   GetNbrPassedTallyRecs( void );

        void  BuildPipeTally( void );

        bool  GetHavePipeInv( void );
        float GetNextPipeLength( void );

        void SetJobStatus( bool newStatus );

      public:
        virtual __fastcall TJob( const String& jobName );
        virtual __fastcall ~TJob( void );

        __property bool Loaded = { read = m_jobLoaded };
            // Returns true if the job was successfully loaded

        __property bool Completed = { read = m_jobParams.isDone, write = SetJobStatus };
            // Gets / sets completion status. No changes are allowed to completed jobs

        // Generic read-only properties
        __property UnicodeString    Client        = { read = m_jobInfo.clientName };
        __property UnicodeString    Location      = { read = m_jobInfo.location };
        __property UNITS_OF_MEASURE Units         = { read = GetUOM };
        __property TDateTime        StartTime     = { read = m_jobInfo.jobStartTime };
        __property int              FirstConn     = { read = m_jobInfo.firstConnNbr };
        __property bool             InSimMode     = { read = m_SimMode, write = m_SimMode };
        __property bool             CanAutoRecord = { read = GetCanAutoRecord };

        static float MaxCasingLength( UNITS_OF_MEASURE uom );
            // Gets a max casing length a user can enter based on UOM

        // Date/time functions that return a string version of the Job's start
        // date and/or time.
        UnicodeString TimeString( void );
        UnicodeString DateString( void );
        UnicodeString DateTimeString( void );

        // JOB_INFO handlers
        void GetJobInfo( JOB_INFO& jobInfo );

        // SECTION_INFO handlers
        __property int  CurrSectIndex                     = { read = m_jobParams.currSectIndex };
        __property int  NbrSections                       = { read = GetNbrSections };
        __property bool SectionHasConns[int whichSection] = { read = GetSectHasConns };
        bool            GetSectionRec( int whichSection, SECTION_INFO& sectionInfo );
        void            AddSectionRec( SECTION_INFO& newSectionInfo );
        bool            EditSectionRec( SECTION_INFO& sectionInfo );
        bool            CanRemoveSection( int iSectionNbr );
        bool            RemoveLastSection( void );
        // Removes a previous section. The section number must exist,
        // and there may be no connections made or attempted for this section.

        bool            SyncToSection( int sectionNbr );
            // Make the passed section number the current section number. Note that
            // the param is a section number, not a section index.

        // CONNECTION_INFO handlers

        __property int NbrConnRecs = { read = GetNbrConnectionRecs };
            // Returns the total number of connections records.

        int            GetNextConnectionNbr( void );

        bool           GetConnection( int whichConn, CONNECTION_INFO& connectionInfo );
        void           AddConnection( CONNECTION_INFO& newConnection, const WTTTS_READING pReadings[], int nbrReadings, bool bKeepInInventoryOnFail );
            // Methods that operate with connection index number (not a
            // connection number).

        bool           CanRemove( int connID, String& sErrorMessage );
        bool           RemoveConnection( int connID, const String& comment, bool bAddToPipeTally );
            // Removes a previous connection. The connection number must exist, the
            // comment cannot be empty, and the last record for this connection must
            // indicate a pass. Passed value is a connection ID, not a connection nbr.

        bool           GetConnectionByID( int connID, CONNECTION_INFO& connectionInfo );
        int            GetConnectionPts ( int connID, WTTTS_READING pReadings[], int maxPts );
            // Methods that operate with connection ID, not index number
            // For GetConnectionPts(), if pBuffer is NULL the function
            // will return the size of the buffer required (in terms of
            // WTTTS_READING recs)

        static void SetOnConnRecsUpdatedHandler( TNotifyEvent pEvent ) { TJob::m_onConnectionsUpdated = pEvent; }

        // Pipe Tally

        __property int   TotalNbrTallyRecs = { read = GetTotalNbrTallyRecs };
            // Returns the total number of connections records.

        __property int   NbrPassedTallyRecs = { read = GetNbrPassedTallyRecs };
            // Returns the total number of connections records.

        __property float PipeTallyLength = { read = GetPipeTallyLength };
            // Returns the total length of all currently 'used' casing lengths

        bool GetTallyRec( int tallyIndex, PIPE_TALLY_REC& tallyRec );
            // Gets a pipe tally record, by its index number.

        static void SetOnPipeTallyUpdatedHandler( TNotifyEvent pEvent ) { TJob::m_onPipeTallyUpdated = pEvent; }

        // Pipe Inventory

        __property int NbrPipeInvRecs = { read = GetNbrPipeInvRecs };
            // Returns the total number of pipe inventory records

        __property bool HavePipeInventory = { read = GetHavePipeInv };
            // Returns true if there is at least one pipe is available
            // for use in the pipe inventory

        __property float NextPipeLength = { read = GetNextPipeLength };
            // Returns the length of the next pipe available in inventory.
            // Returns 0.0 if no inventory is available

        typedef enum {
            eIA_CanAdd,      // Means can add an item to end of the list
            eIA_CanInsert,   // Means can insert an item above the current selection
            eIA_CanEdit,     // Means can edit the current selection
            eIA_CanDelete    // Means can delete the current selection
        } eInvActions;

        typedef Set <eInvActions, eIA_CanAdd, eIA_CanDelete> InvActionSet;

        void GetInvRecActions( int iInvID, InvActionSet& actions );
            // Returns the allowed actions for the pipe inventory item with
            // the passed ID

        bool GetInvRecByID( int invRecID, PIPE_INVENTORY_ITEM& pipeInvRec );
            // Gets a PIPE_INVENTORY_ITEM by its ID, not its index in the list

        bool AddInventoryRec( float fLength );
        bool InsertInventoryRec( int iInvIDToInsertBefore, float fLength );
        bool ModifyInventoryRec( int iInvID, float fNewLength );
        bool DeleteInventoryRec( int iInvID );
            // Handlers to add / insert / modify / delete inventory recs

        // Main Comments

        __property int NbrMainComments  = { read = GetNbrMainComments };
        bool           GetMainComment( int whichComment, MAIN_COMMENT& comment );
        void           AddMainComment( UnicodeString commentText );

        // Calibration Records

        __property int NbrCalibrationRecs = { read = GetNbrCalibrationRecs };
        bool           GetCalibrationRec( int whichCalibrationRec, CALIBRATION_REC& calibrationRecs );
        void           AddCalibrationRec( CALIBRATION_REC& newCalibrationRec );
        String         GetCalRecConnList( int calRecCreationNbr );

        void           SaveBackup( void );
};

#endif
