#include <vcl.h>
#pragma hdrstop

#include "JobsDlg.h"
#include "JobDetailsDlg.h"
#include "TLoadCasingsDlg.h"
#include "RegistryInterface.h"
#include "ApplUtils.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TSelJobsForm *SelJobsForm;


// Listview columns
typedef enum {
    LVC_CLIENT_NAME,
    LVC_LOCATION,
    LVC_START_TIME,
    NBR_LISTVIEW_COLS
} LIST_VIEW_COL;


__fastcall TSelJobsForm::TSelJobsForm(TComponent* Owner) : TForm(Owner)
{
    UnitsRG->Items->Strings[UOM_METRIC  ] = GetUnitsText( UOM_METRIC,   MT_MEASURE_NAME );
    UnitsRG->Items->Strings[UOM_IMPERIAL] = GetUnitsText( UOM_IMPERIAL, MT_MEASURE_NAME );

    UnitsRG->ItemIndex = GetDefaultUnitsOfMeasure();

    pJobList = NULL;
}


void __fastcall TSelJobsForm::FormDestroy(TObject *Sender)
{
    if( pJobList != NULL )
        delete pJobList;
}


bool TSelJobsForm::OpenJob( String& jobFileName, const String& preferredJobFileName )
{
    // Return true if an existing job is selected for new job is created.
    // Return false is cancel is pressed.
    JobPgCtrl->ActivePageIndex    = 0;
    ShowMostRecentJobsRB->Checked = true;

    ClientFilterEdit->Text   = "";
    LocationFilterEdit->Text = "";

    JobsFilterClick( NULL );

    FirstConnEdit->Text = L"1";

    // If the preferredJobName is not blank, select it in the list of jobs
    JobsListView->ItemIndex = -1;

    if( ( preferredJobFileName.Length() > 0 ) && ( pJobList != NULL ) )
    {
        int preferredJobIndex = -1;

        for( int iJob = 0; iJob < pJobList->Count; iJob++ )
        {
            JOB_LIST_ITEM jobItem;

            if( pJobList->GetItem( iJob, jobItem ) )
            {
                if( jobItem.jobFileName.CompareIC( preferredJobFileName ) == 0 )
                {
                    preferredJobIndex = iJob;
                    break;
                }
            }
        }

        // Now need to find this item in the listview
        if( preferredJobIndex >= 0 )
        {
            for( int iItem = 0; iItem < JobsListView->Items->Count; iItem++ )
            {
                TListItem* pItem = JobsListView->Items->Item[iItem];

                if( (int)( pItem->Data ) == preferredJobIndex )
                {
                    JobsListView->ItemIndex = iItem;
                    break;
                }
            }
        }
    }

    m_selJobName = "";

    m_sCasingsList    = "";
    m_sCasingFileName = "";

    if( ShowModal() != mrOk )
        return false;

    jobFileName = m_selJobName;

    AddToMostRecentJobsList( jobFileName );

    return true;
}


void __fastcall TSelJobsForm::JobsFilterClick(TObject *Sender)
{
    // If the box is checked, show all jobs in the jobs data dir.
    // Otherwise, show the MRU list of jobs

    // First, delete any existing job list
    if( pJobList != NULL )
    {
        delete pJobList;
        pJobList = NULL;
    }

    // Create the job list based on the selected filter option
    if( ShowMostRecentJobsRB->Checked )
    {
        TStringList* slRecentJobs = new TStringList();

        GetMostRecentJobsList( slRecentJobs );

        pJobList = new TJobList( slRecentJobs );

        delete slRecentJobs;
    }
    else
    {
        pJobList = new TJobList( GetJobDataDir() );
    }

    // Now apply client / location filters
    if( ClientFilterEdit->Text.Length() > 0 )
    {
        int iMatchLength = ClientFilterEdit->Text.Length();

        int iJob = 0;

        while( iJob < pJobList->Count )
        {
            JOB_LIST_ITEM jobItem;
            pJobList->GetItem( iJob, jobItem );

            String sClientSubStr = jobItem.jobInfo.clientName.SubString( 1, iMatchLength );

            if( sClientSubStr.CompareIC( ClientFilterEdit->Text ) == 0 )
            {
                // Client matches the substring - keep it
                iJob++;
            }
            else
            {
                // Client doesn't match - remove the job but don't advance the index
                pJobList->DeleteItem( iJob );
            }
        }
    }

    if( LocationFilterEdit->Text.Length() > 0 )
    {
        int iMatchLength = LocationFilterEdit->Text.Length();

        int iJob = 0;

        while( iJob < pJobList->Count )
        {
            JOB_LIST_ITEM jobItem;
            pJobList->GetItem( iJob, jobItem );

            String sLocnSubStr = jobItem.jobInfo.location.SubString( 1, iMatchLength );

            if( sLocnSubStr.CompareIC( LocationFilterEdit->Text ) == 0 )
            {
                // Client matches the substring - keep it
                iJob++;
            }
            else
            {
                // Client doesn't match - remove the job but don't advance the index
                pJobList->DeleteItem( iJob );
            }
        }
    }

    PopulateJobsList();
}


void __fastcall TSelJobsForm::JobsListViewDblClick(TObject *Sender)
{
    OKBtnClick( OKBtn );
}


void __fastcall TSelJobsForm::OKBtnClick(TObject *Sender)
{
    // Okay action depends on the sheet we are on.
    if( JobPgCtrl->ActivePageIndex == 0 )
    {
        // Showing list of jobs. If an item is selected, validate it. The
        // job file name will be the index in the jobs list that is saved
        // in the listview item's Data member.
        if( JobsListView->ItemIndex >= 0 )
        {
            TListItem* pItem = JobsListView->Items->Item[ JobsListView->ItemIndex ];

            int jobListIndex = (int)( pItem->Data );

            JOB_LIST_ITEM jobItem;

            if( pJobList->GetItem( jobListIndex, jobItem ) )
            {
                m_selJobName = jobItem.jobFileName;

                ModalResult = mrOk;
                return;
            }
        }

        // Fall through means nothing selected or there was an error opening
        // the selected item
        Beep();

        return;
    }

    // Fall through means creating a new job. Validate entries
    String clientName = NewClientEdit->Text.Trim();
    String jobLocn    = NewLocnEdit->Text.Trim();

    if( clientName.Length() == 0 )
    {
        MessageDlg( "Client name cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewClientEdit;
        return;
    }

    if( jobLocn.Length() == 0 )
    {
        MessageDlg( "Location cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewLocnEdit;
        return;
    }

    if( JobsExistFor( GetJobDataDir(), clientName, jobLocn ) > 0 )
    {
        int mrResult = MessageDlg( "One or more jobs for this client at this location already exists."
                                   "\r  Press Yes to create a new job at this location anyway."
                                   "\r  Press Cancel to enter different parameters for this job.", mtInformation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

        if( mrResult != mrYes )
        {
            ActiveControl = NewClientEdit;
            return;
        }
    }

    int firstConn = FirstConnEdit->Text.ToIntDef( -1 );

    if( firstConn < 0 )
    {
        MessageDlg( "You have entered an invalid starting connection number.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = FirstConnEdit;
        return;
    }

    // New job info looks good... try to create the job
    TJobDetailsForm::NEW_JOB_KEY newJobKey;

    newJobKey.clientName     = clientName;
    newJobKey.location       = jobLocn;
    newJobKey.firstConnNbr   = firstConn;
    newJobKey.unitsOfMeasure = (UNITS_OF_MEASURE)( UnitsRG->ItemIndex );

    if( m_sCasingsList.Length() > 0 )
    {
        newJobKey.sCasingFileName = m_sCasingFileName;
        newJobKey.sCasingLengths  = m_sCasingsList;
    }
    else
    {
        newJobKey.sCasingFileName = "";
        newJobKey.sCasingLengths  = "";
    }

    // If the job cannot be created, the JobDetailsForm would have displayed
    // an error message. Just quit in that case
    String newJobFileName;

    if( !JobDetailsForm->CreateNewJob( newJobKey, newJobFileName ) )
        return;

    // Save used units of entry for next job
    SetDefaultUnitsOfMeasure( UnitsRG->ItemIndex );

    m_selJobName = newJobFileName;

    ModalResult = mrOk;
}


void __fastcall TSelJobsForm::JobsListBoxDblClick(TObject *Sender)
{
    OKBtnClick( OKBtn );
}


void TSelJobsForm::PopulateJobsList( void )
{
    // Populate the listview. Need to bear in mind that the listview
    // sorts items based on each item's Caption. Therefore, the order
    // of the items in the listview may not be the same as the order
    // in the jobs list.
    JobsListView->Items->Clear();

    for( int iJob = 0; iJob < pJobList->Count; iJob++ )
    {
        JOB_LIST_ITEM jobItem;

        if( pJobList->GetItem( iJob, jobItem ) )
        {
            TListItem* pNewItem = JobsListView->Items->Add();

            pNewItem->Caption = jobItem.jobInfo.clientName;
            pNewItem->SubItems->Add( jobItem.jobInfo.location );
            pNewItem->SubItems->Add( jobItem.jobInfo.jobStartTime.FormatString( "yyyy-mm-dd hh:nn:ss" ) );

            // Store the index of the item in the job list in the listview's data member.
            // We'll need this index later
            pNewItem->Data = (void *)iJob;
        }
    }
}


void __fastcall TSelJobsForm::LoadCasingsBtnClick(TObject *Sender)
{
    UNITS_OF_MEASURE uomType = (UNITS_OF_MEASURE)( UnitsRG->ItemIndex );

    // Load sLoadedFile from job manager so that it gets shown again
    if( !TLoadCasingsDlg::ShowDlg( this, uomType, m_sCasingFileName, m_sCasingsList ) )
    {
        m_sCasingsList = "";
    }
}


void __fastcall TSelJobsForm::FilterEditChange(TObject *Sender)
{
    // Just fake out a filter radio button click - its handler
    // will do the work
    JobsFilterClick( NULL );
}

