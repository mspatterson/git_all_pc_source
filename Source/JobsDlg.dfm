object SelJobsForm: TSelJobsForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Select Job'
  ClientHeight = 369
  ClientWidth = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  DesignSize = (
    482
    369)
  PixelsPerInch = 96
  TextHeight = 13
  object JobPgCtrl: TPageControl
    Left = 4
    Top = 4
    Width = 474
    Height = 329
    ActivePage = ExistingSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    ExplicitHeight = 294
    object ExistingSheet: TTabSheet
      Caption = 'Existing Jobs'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 266
      DesignSize = (
        466
        301)
      object JobsListView: TListView
        Left = 11
        Top = 12
        Width = 440
        Height = 167
        Anchors = [akLeft, akTop, akRight, akBottom]
        Columns = <
          item
            Caption = 'Client'
            Width = 150
          end
          item
            Caption = 'Location'
            Width = 150
          end
          item
            Caption = 'Date'
            Width = 100
          end>
        GridLines = True
        ReadOnly = True
        RowSelect = True
        SortType = stText
        TabOrder = 0
        ViewStyle = vsReport
        OnDblClick = JobsListViewDblClick
      end
      object JobFiltersGB: TGroupBox
        Left = 11
        Top = 189
        Width = 440
        Height = 102
        Caption = ' Job Filters '
        TabOrder = 1
        object Label4: TLabel
          Left = 16
          Top = 52
          Width = 133
          Height = 13
          Caption = 'Show Clients beginning with'
        end
        object Label5: TLabel
          Left = 224
          Top = 52
          Width = 146
          Height = 13
          Caption = 'Show Locations beginning with'
        end
        object ShowMostRecentJobsRB: TRadioButton
          Left = 16
          Top = 24
          Width = 162
          Height = 17
          Caption = 'Show most recent Jobs'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = JobsFilterClick
        end
        object ShowAllJobsRB: TRadioButton
          Left = 224
          Top = 24
          Width = 113
          Height = 17
          Caption = 'Show all Jobs'
          TabOrder = 1
          OnClick = JobsFilterClick
        end
        object ClientFilterEdit: TEdit
          Left = 16
          Top = 69
          Width = 182
          Height = 21
          TabOrder = 2
          OnChange = FilterEditChange
        end
        object LocationFilterEdit: TEdit
          Left = 224
          Top = 69
          Width = 182
          Height = 21
          TabOrder = 3
          OnChange = FilterEditChange
        end
      end
    end
    object NewSheet: TTabSheet
      Caption = 'New Job'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 266
      DesignSize = (
        466
        301)
      object NewJobPanel: TPanel
        Left = 11
        Top = 12
        Width = 442
        Height = 197
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkTile
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Client'
        end
        object Label2: TLabel
          Left = 12
          Top = 48
          Width = 40
          Height = 13
          Caption = 'Location'
        end
        object NewClientEdit: TEdit
          Left = 76
          Top = 13
          Width = 341
          Height = 21
          TabOrder = 0
        end
        object NewLocnEdit: TEdit
          Left = 76
          Top = 45
          Width = 341
          Height = 21
          TabOrder = 1
        end
        object UnitsRG: TRadioGroup
          Left = 12
          Top = 80
          Width = 185
          Height = 92
          Caption = ' Units of Measure '
          ItemIndex = 0
          Items.Strings = (
            'Metric'
            'Imperial')
          TabOrder = 2
        end
        object OptionsGB: TGroupBox
          Left = 216
          Top = 80
          Width = 201
          Height = 92
          Caption = ' Casing Options '
          TabOrder = 3
          object Label3: TLabel
            Left = 12
            Top = 25
            Width = 98
            Height = 13
            Caption = 'First Connection Nbr'
          end
          object FirstConnEdit: TEdit
            Left = 116
            Top = 22
            Width = 66
            Height = 21
            NumbersOnly = True
            TabOrder = 0
            Text = '1'
          end
          object LoadCasingsBtn: TButton
            Left = 22
            Top = 53
            Width = 154
            Height = 25
            Caption = 'Load Casing Data'
            TabOrder = 1
            OnClick = LoadCasingsBtnClick
          end
        end
      end
    end
  end
  object CancelBtn: TButton
    Left = 399
    Top = 339
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    ExplicitTop = 304
  end
  object OKBtn: TButton
    Left = 318
    Top = 339
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'O&K'
    Default = True
    TabOrder = 1
    OnClick = OKBtnClick
    ExplicitTop = 304
  end
end
