//---------------------------------------------------------------------------
#ifndef JobsDlgH
#define JobsDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "JobManager.h"
//---------------------------------------------------------------------------
class TSelJobsForm : public TForm
{
__published:    // IDE-managed Components
    TPageControl *JobPgCtrl;
    TButton *CancelBtn;
    TButton *OKBtn;
    TTabSheet *ExistingSheet;
    TTabSheet *NewSheet;
    TPanel *NewJobPanel;
    TLabel *Label1;
    TLabel *Label2;
    TEdit *NewClientEdit;
    TEdit *NewLocnEdit;
    TRadioGroup *UnitsRG;
    TListView *JobsListView;
    TGroupBox *OptionsGB;
    TLabel *Label3;
    TEdit *FirstConnEdit;
    TButton *LoadCasingsBtn;
    TGroupBox *JobFiltersGB;
    TRadioButton *ShowMostRecentJobsRB;
    TRadioButton *ShowAllJobsRB;
    TLabel *Label4;
    TEdit *ClientFilterEdit;
    TLabel *Label5;
    TEdit *LocationFilterEdit;
    void __fastcall JobsFilterClick(TObject *Sender);
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall JobsListBoxDblClick(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall JobsListViewDblClick(TObject *Sender);
    void __fastcall LoadCasingsBtnClick(TObject *Sender);
    void __fastcall FilterEditChange(TObject *Sender);

private:
    TJobList* pJobList;

    String m_selJobName;

    String m_sCasingFileName;
    String m_sCasingsList;

    void PopulateJobsList( void );

public:
    __fastcall TSelJobsForm(TComponent* Owner);

    bool OpenJob( String& jobFileName, const String& preferredJobFileName );
};
//---------------------------------------------------------------------------
extern PACKAGE TSelJobsForm *SelJobsForm;
//---------------------------------------------------------------------------
#endif
