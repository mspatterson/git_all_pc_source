#include <vcl.h>
#pragma hdrstop

#include "MainCommentsDlg.h"
#include "ApplUtils.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TMainCommentForm *MainCommentForm;


__fastcall TMainCommentForm::TMainCommentForm(TComponent* Owner) : TForm(Owner)
{
}


void TMainCommentForm::ShowMainComments( TJob* currJob, bool commentRequired )
{
    // Shows the comments dialog. If the job is completed, no
    // additional comments are allowed. If param commentRequired is
    // true, then the dialog cannot be dismissed until a user
    // enters a comment.

    if( currJob == NULL )
    {
        Beep();
        return;
    }

    // Save params, and enable/disable controls based on job state
    m_currJob = currJob;

    if( currJob->Completed )
    {
        m_isRequired  = false;
        m_commentMade = false;

        // Cannot enter a comment if the job is completed
        AddBtn->Enabled         = false;
        NewCommentMemo->Enabled = false;
    }
    else
    {
        m_isRequired  = commentRequired;
        m_commentMade = false;

        // Can enter a comment if the job is completed
        AddBtn->Enabled         = true;
        NewCommentMemo->Enabled = true;
    }

    LoadComments();

    ShowModal();
}


void __fastcall TMainCommentForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    // If a comment is required, can only close if at least one has been made
    if( m_isRequired )
        CanClose = m_commentMade;
    else
        CanClose = true;
}


void __fastcall TMainCommentForm::AddBtnClick(TObject *Sender)
{
    UnicodeString newText = NewCommentMemo->Text.Trim();

    if( newText.Length() == 0 )
    {
        Beep();

        NewCommentMemo->SelectAll();
        ActiveControl = NewCommentMemo;

        return;
    }

    m_currJob->AddMainComment( newText );

    m_commentMade = true;

    LoadComments();

    NewCommentMemo->Lines->Clear();
}


void TMainCommentForm::LoadComments( void )
{
    CommentsMemo->Lines->Clear();

    if( m_currJob != NULL )
    {
        for( int iComment = 0; iComment < m_currJob->NbrMainComments; iComment++ )
        {
            MAIN_COMMENT aComment;

            if( m_currJob->GetMainComment( iComment, aComment ) )
            {
                if( CommentsMemo->Lines->Count > 0 )
                    CommentsMemo->Lines->Add( L"" );

                UnicodeString dateText = aComment.entryTime.FormatString( "yyyy-mm-dd hh:nn:ss" );

                CommentsMemo->Lines->Add( dateText + L": " + aComment.entryText );
            }
        }
    }
}


void __fastcall TMainCommentForm::NewCommentMemoKeyUp(TObject *Sender, WORD &Key, TShiftState Shift)
{
    if( Key == VK_RETURN )
    {
        AddBtnClick( AddBtn );
        Key = 0;
    }
}


