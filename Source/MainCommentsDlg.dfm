object MainCommentForm: TMainCommentForm
  Left = 0
  Top = 0
  Caption = ' Job Comments'
  ClientHeight = 362
  ClientWidth = 548
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  DesignSize = (
    548
    362)
  PixelsPerInch = 96
  TextHeight = 13
  object CommentsMemo: TMemo
    Left = 8
    Top = 8
    Width = 532
    Height = 229
    Anchors = [akLeft, akTop, akRight, akBottom]
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    WantReturns = False
  end
  object AddCommentGB: TGroupBox
    Left = 8
    Top = 244
    Width = 532
    Height = 78
    Anchors = [akLeft, akRight, akBottom]
    Caption = ' Add Comment '
    TabOrder = 1
    DesignSize = (
      532
      78)
    object NewCommentMemo: TMemo
      Left = 12
      Top = 20
      Width = 426
      Height = 49
      Anchors = [akLeft, akTop, akRight, akBottom]
      ScrollBars = ssVertical
      TabOrder = 0
      WantReturns = False
      OnKeyUp = NewCommentMemoKeyUp
    end
    object AddBtn: TButton
      Left = 448
      Top = 20
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Add'
      TabOrder = 1
      OnClick = AddBtnClick
    end
  end
  object CloseBtn: TButton
    Left = 456
    Top = 330
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    ModalResult = 2
    TabOrder = 2
  end
end
