//---------------------------------------------------------------------------
#ifndef MainCommentsDlgH
#define MainCommentsDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "JobManager.h"
//---------------------------------------------------------------------------
class TMainCommentForm : public TForm
{
__published:    // IDE-managed Components
    TMemo *CommentsMemo;
    TGroupBox *AddCommentGB;
    TButton *CloseBtn;
    TMemo *NewCommentMemo;
    TButton *AddBtn;
    void __fastcall AddBtnClick(TObject *Sender);
    void __fastcall NewCommentMemoKeyUp(TObject *Sender, WORD &Key, TShiftState Shift);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);


private:
    TJob* m_currJob;
    bool  m_isRequired;
    bool  m_commentMade;

    void LoadComments( void );

public:
    __fastcall TMainCommentForm(TComponent* Owner);

    void ShowMainComments( TJob* currJob, bool commentRequired = false );
        // Shows the comments dialog. If the job is completed, no
        // additional comments are allowed. If param commentRequired is
        // true, then the dialog cannot be dismissed until a user
        // enters a comment.
};
//---------------------------------------------------------------------------
extern PACKAGE TMainCommentForm *MainCommentForm;
//---------------------------------------------------------------------------
#endif
