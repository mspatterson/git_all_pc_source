#include <vcl.h>
#pragma hdrstop

#include <registry.hpp>

#include "RegistryInterface.h"
#include "ApplUtils.h"
#include "TescoShared.h"

#pragma package(smart_init)


//
// Private Registry Defines
//

static const UnicodeString m_BootSection( "Boot" );
    static const UnicodeString m_LastShutdown( "LastShutdown" );
    static const UnicodeString m_ProcPriority( "ProcessPriority" );

static const UnicodeString m_JobsMRUSection( "MostRecentJobs" );
    static const UnicodeString m_MRUItem( "LastJob%d" );

static const UnicodeString m_RecentNameSection( "RecentNames %d" );
    static const UnicodeString m_recentNameItem   ( "Name %d" );

static const UnicodeString m_SizesSection( "Sizes" );
    static const UnicodeString m_SizeEntry( "Control%d" );

static const UnicodeString m_SettingsSection( "Settings" );
    static const UnicodeString m_JobsDir            ( "DataDir" );
    static const UnicodeString m_ShldrBoxSize       ( "Shoulder Box Size" );
    static const UnicodeString m_AutoShldrNag       ( "Nag On AutoShoulder Override" );
    static const UnicodeString m_TorqueAxisDefault  ( "TorqueAxis %d Default Span" );
    static const UnicodeString m_TorqueAxisIncr     ( "TorqueAxis %d Increment" );
    static const UnicodeString m_TorqueAxisAutoCal  ( "TorqueAxis %d Auto Calc" );
    static const UnicodeString m_TimeAxisDefault    ( "TimeAxis %d Default Span" );
    static const UnicodeString m_TimeAxisIncr       ( "TimeAxis %d Increment" );
    static const UnicodeString m_RPMAxisDefault     ( "RPMAxis %d Default Span" );
    static const UnicodeString m_RPMAxisIncr        ( "RPMAxis %d Increment" );
    static const UnicodeString m_TenAxisDefault     ( "TenAxis %d Default Span" );
    static const UnicodeString m_TenAxisIncr        ( "TenAxis %d Increment" );
    static const UnicodeString m_TurnsAxisDefault   ( "TurnsAxis %d Default Span" );
    static const UnicodeString m_TurnsAxisIncr      ( "TurnsAxis %d Increment" );
    static const UnicodeString m_DefaultUOM         ( "Default UOM" );
    static const UnicodeString m_Password           ( "WindowAttributes" );
    static const UnicodeString m_ShowShldrCursors   ( "ShowShoulderCursors" );
    static const UnicodeString m_RPMColor           ( "RPMColor" );
    static const UnicodeString m_TenColor           ( "TenColor" );
    static const UnicodeString m_TorqueColor        ( "TorqueColor" );
    static const UnicodeString m_RPMAveraging       ( "RPMAveraging" );
    static const UnicodeString m_ConnMinTurns       ( "ConnMinTurns" );
    static const UnicodeString m_CDSReleaseThld     ( "CDS Relese %d Threshold" );
    static const UnicodeString m_CDSSlipLockThld    ( "CDS and Slips Locked %d Threshold" );
    static const UnicodeString m_CDSLockSlipRelThld ( "CDS Locked Slips Released %d Threshold" );
    static const UnicodeString m_InterlockRelTime   ( "Interlock Release Time" );
    static const UnicodeString m_AvgSettingMethod   ( "Avg %s Method" );
    static const UnicodeString m_AvgSettingLevel    ( "Avg %s Level" );
    static const UnicodeString m_AvgSettingVariance ( "Avg %s Variance" );
    static const UnicodeString m_ForceBackupSave    ( "Force Backup" );
    static const UnicodeString m_ForceBackupSaveDir ( "Force Backup Dir" );
    static const UnicodeString m_PDSOnThreshold     ( "PDS Switch On Thresh" );
    static const UnicodeString m_PDSOffThreshold    ( "PDS Switch Off Thresh" );
    static const UnicodeString m_PDSMonEnabled      ( "PDS Monitoring Enabled" );
    static const UnicodeString m_MinBattLevel       ( "Min Batt Level" );
    static const UnicodeString m_BattValidThresh    ( "Battery Valid Threshold" );
    static const UnicodeString m_BattMinOperV       ( "Battery Min Oper Volts" );
    static const UnicodeString m_BattGoodV          ( "Battery Good Volts" );
    static const UnicodeString m_BROut3Fcn          ( "Base Radio Output 3" );

static const UnicodeString m_AvgSection( "Averaging Settings" );
    static const UnicodeString m_AvgSettingAlpha    ( "%s %s Alpha" );

static const UnicodeString m_CommsSection( "Comms Settings" );
    static const UnicodeString m_portType         ( "Port %d Type" );
    static const UnicodeString m_portName         ( "Port %d Name" );
    static const UnicodeString m_portParam        ( "Port %d Param" );
    static const UnicodeString m_autoAssignPorts  ( "AutoAssignPorts" );
    static const UnicodeString m_radioChanTimeout ( "Radio Chan Timeout" );
    static const UnicodeString m_defaultRadioChan ( "Default Radio Chan" );

static const UnicodeString m_DialogPosSection( "Dialog Positions" );
    static const UnicodeString m_dlgPosTopEntry   ( "Dialog %d Top" );
    static const UnicodeString m_dlgPosLeftEntry  ( "Dialog %d Left" );
    static const UnicodeString m_dlgPosHeightEntry( "Dialog %d Height" );
    static const UnicodeString m_dlgPosWidthEntry ( "Dialog %d Width" );

static const UnicodeString m_TempCompSection( "Temp Comp" );
    static const UnicodeString m_tcVersion        ( "Version" );
    static const UnicodeString m_tcApplyTempComp  ( "Apply Temp Comp" );
    static const UnicodeString m_tcRiseSlopeThresh( "RTD Pos ROC Thresh" );
    static const UnicodeString m_tcRiseSlopeFactor( "RTD Pos ROC Factor" );
    static const UnicodeString m_tcFallSlopeThresh( "RTD Neg ROC Thresh" );
    static const UnicodeString m_tcFallSlopeFactor( "RTD Neg ROC Factor" );
    static const UnicodeString m_tcRTDAlpha       ( "RTD Alpha" );
    static const UnicodeString m_tcRTDMaxVar      ( "RTD Max Var" );
    static const UnicodeString m_tcApplySlopeComp ( "Apply RTD Slope Comp" );
    static const UnicodeString m_tcRTDSlopeAlpha  ( "RTD Slope Alpha" );
    static const UnicodeString m_tcRTDSlopMaxVar  ( "RTD Slope Max Var" );
    static const UnicodeString m_tcRTDSlopePThresh( "RTD Slope Pos Thresh" );
    static const UnicodeString m_tcRTDSlopeNThresh( "RTD Slope Neg Thresh" );
    static const UnicodeString m_tcRTDSlopeFactor ( "RTD Slope Factor" );

static const UnicodeString m_WitsSection( "WITS Settings" );
    static const UnicodeString m_WitsPortType      ( "Port Type" );
    static const UnicodeString m_WitsPortName      ( "Port Name" );
    static const UnicodeString m_WitsPortParam     ( "Port Param" );
    static const UnicodeString m_WitsRfcHdr        ( "RFC Header Enabled" );
    static const UnicodeString m_WitsRfcHdrCode    ( "RFC Header Code" );
    static const UnicodeString m_WitsRfcHdrData    ( "RFC Header Data" );
    static const UnicodeString m_WitsStreamHdr     ( "Stream Header Enabled" );
    static const UnicodeString m_WitsStreamHdrCode ( "Stream Header Code" );
    static const UnicodeString m_WitsStreamHdrData ( "Stream Header Data" );
    static const UnicodeString m_WitsFieldCount    ( "Type %d Field Count" );
    static const UnicodeString m_WitsFieldInfo     ( "Type %d Field %d" );

static const UnicodeString m_AutoRecSection( "Auto Record Settings" );
    static const UnicodeString m_AREnabled       ( "Auto Record Enabled" );
    static const UnicodeString m_ARMinRPM        ( "Min RPM to Start" );
    static const UnicodeString m_ARMinTorque     ( "Min Torque to Start" );
    static const UnicodeString m_ARStopTime      ( "Stop Time" );
    static const UnicodeString m_ARXThreadThresh ( "Cross Thread Thresh" );
    static const UnicodeString m_ARAlarmOnError  ( "Alarm on Error" );



#define NBR_MRU_JOBS  4

#define CURR_RTD_TC_VER 3


// Default Averaging Level Properties ( Param, Variance ) - Torque
static const AVG_LEVEL_PROPS DefaultAvgLevels[NBR_AVG_TYPES][NBR_AVG_LEVELS] =
{
    //                AL_OFF       AL_LIGHT     AL_MEDIUM    AL_HEAVY
    /*AT_TORQUE */  { { 1.00, 0 }, { 0.99, 0 }, { 0.85, 0 }, { 0.70, 0 } },
    /*AT_TENSION*/  { { 1.00, 0 }, { 0.99, 0 }, { 0.85, 0 }, { 0.70, 0 } },
};

static const String AvgTypeName[NBR_AVG_TYPES] = { "Torque", "Tension" };


static TRegistryIniFile* CreateRegIni( void );
    // Creates a reg ini file object. Caller is responsible for releasing it


// Some properties only persist through the execution of the application.
// Those are stored here.

static bool   m_loggingEnabled[NBR_DATA_LOG_TYPES] = { false, false, false, false };
static String m_logFileName[NBR_DATA_LOG_TYPES]    = { "",    "",    "",    ""    };

static bool   m_interlockEnable = true;

static BoolState m_bsAutoRecEn = eBS_Unknown;


//
// Public Constants
//

const String DeviceTypeText[NBR_DEVICE_TYPES] = {
    L"TesTORK",        // DT_WTTTS
    L"Legacy Sub",     // DT_LEGACY_SUB
    L"No Device",      // DT_NULL
};


//
// Public Functions
//

SHUT_DOWN_TYPE GetLastShutdown( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    SHUT_DOWN_TYPE sdtReturn = (SHUT_DOWN_TYPE)(regIni->ReadInteger( m_BootSection, m_LastShutdown, SDT_NEW ) );

    delete regIni;

    return sdtReturn;
}


void SetProgramInUse( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_LastShutdown, SDT_CRASH );

    delete regIni;
}


void SetProgramShutdownGood( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_LastShutdown, SDT_GOOD );

    delete regIni;
}


DWORD GetProcessPriority( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    DWORD procPriority = regIni->ReadInteger( m_BootSection, m_ProcPriority, NORMAL_PRIORITY_CLASS );

    delete regIni;

    return procPriority;
}


void SetProcessPriority( DWORD newPriority )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_BootSection, m_ProcPriority, newPriority );

    delete regIni;
}


void GetMostRecentJobsList( TStrings* slJobList )
{
    if( slJobList == NULL )
        return;

    slJobList->Clear();

    TRegistryIniFile* regIni = CreateRegIni();

    // Add strings from the MRU list until we've either max'd out or
    // we encounter the first empty string.
    for( int iItem = 0; iItem < NBR_MRU_JOBS; iItem++ )
    {
        UnicodeString sKey;
        sKey.printf( m_MRUItem.w_str(), iItem + 1 );

        UnicodeString sEntry = regIni->ReadString( m_JobsMRUSection, sKey, L"" );

        if( sEntry.Length() == 0 )
            break;

        if( FileExists( sEntry ) )
            slJobList->Add( sEntry );

    }

    delete regIni;
}


UnicodeString GetMostRecentJob( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    // Return the most recent entry from the MRU list
    UnicodeString sKey;
    sKey.printf( m_MRUItem.w_str(), 1 );

    UnicodeString sEntry = regIni->ReadString( m_JobsMRUSection, sKey, L"" );

    delete regIni;

    return sEntry;
}


void AddToMostRecentJobsList( UnicodeString sNewJob )
{
    TRegistryIniFile* regIni = CreateRegIni();

    // Copy all of the existing strings in order to a string list. It
    // will be easier to work with than the actual registry entries.
    TStringList* mruStrings = new TStringList();

    GetMostRecentJobsList( mruStrings );

    // Check if the entry exists. If it does, make sure it is the topmost
    // entry in the list
    int currIndex = mruStrings->IndexOf( sNewJob );

    if( currIndex > 0 )
    {
        // The string exists, but is not the top-most entry. Delete it
        // from its current position, then add it back at the top of
        // the list.
        mruStrings->Delete( currIndex );
        mruStrings->Insert( 0, sNewJob );
    }
    else if( currIndex < 0 )
    {
        // The string does not exist. Insert it at the top of the
        // list. We don't need to delete items in this case because
        // of how we store the strings back in the registry below.
        mruStrings->Insert( 0, sNewJob );
    }
    else
    {
        // currIndex == 0 in this case, meaning the string exists and
        // is at the top of list. No work needs to be done.
    }

    // mruStrings has been setup. Now re-create the section. First,
    // delete all existing entries.
    regIni->EraseSection( m_JobsMRUSection );

    // Now add all items to the section
    for( int iItem = 0; iItem < mruStrings->Count; iItem++ )
    {
        // Only add up to NBR_MRU_JOBS items
        if( iItem >= NBR_MRU_JOBS )
            break;

        // Now add this job to the top of the list
        UnicodeString sKey;
        sKey.printf( m_MRUItem.w_str(), iItem + 1 );

        regIni->WriteString( m_JobsMRUSection, sKey, mruStrings->Strings[iItem] );
    }

    delete mruStrings;
    delete regIni;
}


UnicodeString GetJobDataDir( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    UnicodeString sReturn = regIni->ReadString( m_SettingsSection, m_JobsDir, L"" );

    delete regIni;

    return sReturn;
}


void SetJobDataDir( UnicodeString newDir )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteString( m_SettingsSection, m_JobsDir, newDir );

    delete regIni;
}


int GetDefaultUnitsOfMeasure( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    int iResult = regIni->ReadInteger( m_SettingsSection, m_DefaultUOM, 1 /*imperial*/ );

    delete regIni;

    return iResult;
}


void SetDefaultUnitsOfMeasure( int newDefaultUOM )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_SettingsSection, m_DefaultUOM, newDefaultUOM );

    delete regIni;
}


int GetCtrlSize( CTRL_NAME aCtrl )
{
    TRegistryIniFile* regIni = CreateRegIni();

    UnicodeString sKey;
    sKey.printf( m_SizeEntry.w_str(), aCtrl );

    int result = regIni->ReadInteger( m_SizesSection, sKey, -1 );

    delete regIni;

    return result;
}


void SaveCtrlSize( CTRL_NAME aCtrl, int newSize )
{
    TRegistryIniFile* regIni = CreateRegIni();

    UnicodeString sKey;
    sKey.printf( m_SizeEntry.w_str(), aCtrl );

    regIni->WriteInteger( m_SizesSection, sKey, newSize );

    delete regIni;
}


int GetShoulderBoxSize( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    int iReturn = regIni->ReadInteger( m_SettingsSection, m_ShldrBoxSize, 12 );

    delete regIni;

    return iReturn;
}


void SaveShoulderBoxSize( int newSize )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_SettingsSection, m_ShldrBoxSize, newSize );

    delete regIni;
}


bool GetShowShldrCursors( void )
{
    TRegistryIniFile* regIni = CreateRegIni();

    bool bReturn = regIni->ReadBool( m_SettingsSection, m_ShowShldrCursors, true );

    delete regIni;

    return bReturn;
}


void SaveShowShldrCursors( bool newSetting )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteBool( m_SettingsSection, m_ShowShldrCursors, newSetting);

    delete regIni;

}


void GetGraphColors( TColor& rpmColor, TColor& torqueColor, TColor& tenColor )
{
    rpmColor    = TColor( 0x00004000 );
    torqueColor = TColor( 0x00A36644 );
    tenColor    = TColor( 0x000080FF );

    TRegistryIniFile* regIni = CreateRegIni();

    rpmColor    = (TColor)( regIni->ReadInteger( m_SettingsSection, m_RPMColor,    rpmColor ) );
    torqueColor = (TColor)( regIni->ReadInteger( m_SettingsSection, m_TorqueColor, torqueColor ) );
    tenColor    = (TColor)( regIni->ReadInteger( m_SettingsSection, m_TenColor,    tenColor ) );

    delete regIni;
}


void SaveGraphColors( TColor rpmColor, TColor torqueColor, TColor tenColor )
{
    TRegistryIniFile* regIni = CreateRegIni();

    regIni->WriteInteger( m_SettingsSection, m_RPMColor,    rpmColor );
    regIni->WriteInteger( m_SettingsSection, m_TorqueColor, torqueColor );
    regIni->WriteInteger( m_SettingsSection, m_TenColor,    tenColor );

    delete regIni;
}


bool GetNagOnAutoShoulderOverride( void )
{
    bool bResult = true;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bResult = regIni->ReadBool( m_SettingsSection, m_AutoShldrNag, bResult );

        delete regIni;
    }

    return bResult;
}


void SaveNagOnAutoShoulderOverride( bool newValue )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool( m_SettingsSection, m_AutoShldrNag, newValue );

        delete regIni;
    }
}


void GetCDSSlipsSettings( int unitsOfMeasure, int& cdsRel, int& cdsSlipLock, int& cdsLockSlipsRel )
 {
    // Set default return values first. Unfortunately, at this level
    // in the code we don't 'know about' the unitsOfMeasure enum. So
    // for now, we do a hardcoded hack.
    const int nbrUnitsOfMeasure = 2;
                                                          /* metric  imperial */
    int defaultCDSRelValues[nbrUnitsOfMeasure]          = {    0,       0 };
    int defaultCDSSlipLockValues[nbrUnitsOfMeasure]     = {  400,     180 };
    int defaultCDSLockSlipsRelValues[nbrUnitsOfMeasure] = { 2000,     900 };

    if( ( unitsOfMeasure < 0 ) || ( unitsOfMeasure >= nbrUnitsOfMeasure ) )
        unitsOfMeasure = nbrUnitsOfMeasure - 1;

    cdsRel          = defaultCDSRelValues[unitsOfMeasure];
    cdsSlipLock     = defaultCDSSlipLockValues[unitsOfMeasure];
    cdsLockSlipsRel = defaultCDSLockSlipsRelValues[unitsOfMeasure];

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_CDSReleaseThld.w_str(), unitsOfMeasure );
        cdsRel = regIni->ReadInteger( m_SettingsSection, sKey, cdsRel );

        sKey.printf( m_CDSSlipLockThld.w_str(), unitsOfMeasure );
        cdsSlipLock = regIni->ReadInteger( m_SettingsSection, sKey, cdsSlipLock );

        sKey.printf( m_CDSLockSlipRelThld.w_str(), unitsOfMeasure );
        cdsLockSlipsRel = regIni->ReadInteger( m_SettingsSection, sKey, cdsLockSlipsRel );

        delete regIni;
     }
 }


void SaveCDSSlipsSettings( int unitsOfMeasure, int cdsRel, int cdsSlipLock, int cdsLockSlipsRel )
{
    TRegistryIniFile* regIni = CreateRegIni();

    // regIni created
    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_CDSReleaseThld.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, cdsRel );

        sKey.printf( m_CDSSlipLockThld.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, cdsSlipLock );

        sKey.printf( m_CDSLockSlipRelThld.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, cdsLockSlipsRel );

        delete regIni;
    }
}


int GetInterlockReleaseTime( void )
{
    // Set default return values first
    int interlockReltime = 1;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        interlockReltime = regIni->ReadInteger( m_SettingsSection, m_InterlockRelTime, interlockReltime );

        delete regIni;
    }

    return interlockReltime;
}


void SaveInterlockReleaseTime( int newValue )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_SettingsSection, m_InterlockRelTime, newValue );

        delete regIni;
    }
}


void GetAveragingSettings( AVG_TYPE avgType, AVERAGING_PARAMS& avgParams )
{
    // This should never happen, but just in case...
    if( ( avgType < 0 ) || ( avgType >= NBR_AVG_TYPES ) )
        avgType = AT_TORQUE;

    // Set default return values first
    avgParams.avgMethod   = AM_EXPONENTIAL;
    avgParams.avgLevel    = AL_LIGHT;
    avgParams.absVariance = DefaultAvgLevels[avgType][AL_LIGHT].absVariance;
    avgParams.param       = DefaultAvgLevels[avgType][AL_LIGHT].param;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sKey;

        // Added in 1.0.5.4
        // Method forced to be Exponential
        // Param is based on averaging level
        sKey.printf( m_AvgSettingLevel.w_str(), AvgTypeName[avgType] );
        avgParams.avgLevel = (AVG_LEVEL) regIni->ReadInteger( m_SettingsSection, sKey, avgParams.avgLevel );

        sKey.printf( m_AvgSettingVariance.w_str(), AvgTypeName[avgType] );
        avgParams.absVariance = regIni->ReadInteger( m_SettingsSection, sKey, (int)avgParams.absVariance );

        if( ( avgParams.avgLevel >= AL_LIGHT ) && ( avgParams.avgLevel < NBR_AVG_LEVELS ) )
        {
            // Lookup the Param based on the Level & Average Type
            sKey.printf( m_AvgSettingAlpha.w_str(), AvgTypeName[avgType], AvgLevelDesc[avgParams.avgLevel] );

            String sAlpha = regIni->ReadString( m_AvgSection, sKey, FloatToStrF( avgParams.param, ffFixed, 7, 3 ) );

            avgParams.param = StrToFloatDef( sAlpha, DefaultAvgLevels[avgType][avgParams.avgLevel].param );
        }
        else
        {
            // Averaging is off - leave the other values as defaults to ensure no averaging happens
            avgParams.avgLevel = AL_OFF;
        }

        delete regIni;
    }
}


void SaveAveragingSettings( AVG_TYPE avgType, const AVERAGING_PARAMS& avgParams )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sKey;

        // Added in 1.0.5.4
        // Method forced to be Exponential
        // Param not saved - is based on averaging level
        sKey.printf( m_AvgSettingLevel.w_str(), AvgTypeName[avgType] );
        regIni->WriteInteger( m_SettingsSection, sKey, avgParams.avgLevel );

        sKey.printf( m_AvgSettingVariance.w_str(), AvgTypeName[avgType] );
        regIni->WriteInteger( m_SettingsSection, sKey, (int)avgParams.absVariance );

        delete regIni;
    }
}


bool GetInterlockEn( void )
{
   return m_interlockEnable;
}


void SaveInterlockEn( bool bInterlockEn )
{
    m_interlockEnable = bInterlockEn;
}


void GetPDSSwitchThresholds( int& iPDSOnThresh, int& iPDSOffThresh )
{
    iPDSOnThresh  = 0x0010;
    iPDSOffThresh = 0x0E00;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        iPDSOnThresh  = regIni->ReadInteger( m_SettingsSection, m_PDSOnThreshold,  iPDSOnThresh  );
        iPDSOffThresh = regIni->ReadInteger( m_SettingsSection, m_PDSOffThreshold, iPDSOffThresh );

        delete regIni;
    }
}


void GetGraphAxisSpan( int unitsOfMeasure, GRAPH_AXIS_VALUES& params )
{
    // Assign default values
    params.turns      = 10;
    params.torque     = 16000;
    params.RPM        = 40;
    params.tension    = 10000;
    params.time       = 30;
    params.bAutoCalTq = true;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_TurnsAxisDefault.w_str(), unitsOfMeasure );
        params.turns = regIni->ReadInteger( m_SettingsSection, sKey, params.turns );

        sKey.printf( m_TorqueAxisDefault.w_str(), unitsOfMeasure );
        params.torque = regIni->ReadInteger( m_SettingsSection, sKey, params.torque );

        sKey.printf( m_RPMAxisDefault.w_str(), unitsOfMeasure );
        params.RPM = regIni->ReadInteger( m_SettingsSection, sKey, params.RPM );

        sKey.printf( m_TenAxisDefault.w_str(), unitsOfMeasure );
        params.tension = regIni->ReadInteger( m_SettingsSection, sKey, params.tension );

        sKey.printf( m_TimeAxisDefault.w_str(), unitsOfMeasure );
        params.time = regIni->ReadInteger( m_SettingsSection, sKey, params.time );

        sKey.printf( m_TorqueAxisAutoCal.w_str(), unitsOfMeasure );
        params.bAutoCalTq = regIni->ReadBool( m_SettingsSection, sKey, params.bAutoCalTq );

        delete regIni;
    }
}


void SaveGraphAxisSpan( int unitsOfMeasure, const GRAPH_AXIS_VALUES& params )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_TorqueAxisDefault.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, params.torque );

        sKey.printf( m_TimeAxisDefault.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, params.time );

        sKey.printf( m_RPMAxisDefault.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, params.RPM );

        sKey.printf( m_TenAxisDefault.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, params.tension );

        sKey.printf( m_TurnsAxisDefault.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, params.turns );

        sKey.printf( m_TorqueAxisAutoCal.w_str(), unitsOfMeasure );
        regIni->WriteBool( m_SettingsSection, sKey, params.bAutoCalTq );

        delete regIni;
    }
}


void GetGraphAxisIncrements( int unitsOfMeasure, GRAPH_AXIS_VALUES& params )
{
    // Assign default values
    params.turns      = 2;
    params.torque     = 2000;
    params.RPM        = 10;
    params.tension    = 1000;
    params.time       = 10;
    params.bAutoCalTq = false;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_TurnsAxisIncr.w_str(), unitsOfMeasure );
        params.turns = regIni->ReadInteger( m_SettingsSection, sKey, params.turns );

        sKey.printf( m_TorqueAxisIncr.w_str(), unitsOfMeasure );
        params.torque = regIni->ReadInteger( m_SettingsSection, sKey, params.torque );

        sKey.printf( m_RPMAxisIncr.w_str(), unitsOfMeasure );
        params.RPM = regIni->ReadInteger( m_SettingsSection, sKey, params.RPM );

        sKey.printf( m_TenAxisIncr.w_str(), unitsOfMeasure );
        params.tension = regIni->ReadInteger( m_SettingsSection, sKey, params.tension );

        sKey.printf( m_TimeAxisIncr.w_str(), unitsOfMeasure );
        params.time = regIni->ReadInteger( m_SettingsSection, sKey, params.time );

        delete regIni;
    }
}


void SaveGraphAxisIncrements( int unitsOfMeasure, const GRAPH_AXIS_VALUES& params )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_TorqueAxisIncr.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, params.torque );

        sKey.printf( m_TimeAxisIncr.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, params.time );

        sKey.printf( m_RPMAxisIncr.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, params.RPM );

        sKey.printf( m_TenAxisIncr.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, params.tension );

        sKey.printf( m_TurnsAxisIncr.w_str(), unitsOfMeasure );
        regIni->WriteInteger( m_SettingsSection, sKey, params.turns );

        delete regIni;
    }
}


UnicodeString GetSysAdminPassword( void )
{
    // Initialize return result with default password
    UnicodeString sPassword( "tesco" );

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TMemoryStream* pStream = new TMemoryStream();

        if( pStream != NULL )
        {
            int bytesRead = regIni->ReadBinaryStream( m_SettingsSection, m_Password, pStream );

            if( bytesRead > 0 )
            {
                // Decode the password. Byte values are xor'd with 0x85
                AnsiString sTemp;
                sTemp.SetLength( bytesRead );

                BYTE* pData = (BYTE*)( pStream->Memory );

                for( int iByte = 0; iByte < bytesRead; iByte++ )
                    sTemp[iByte+1] = (char)( pData[iByte] ^ 0x85 );

                sPassword = sTemp;
            }

            delete pStream;

        }

        delete regIni;
    }

    return sPassword;
}


void SaveSysAdminPassword( const String& newPW )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TMemoryStream* pStream = new TMemoryStream();

        if( pStream != NULL )
        {
            AnsiString sTemp( newPW );

            pStream->SetSize( sTemp.Length() );

            BYTE* pData = (BYTE*)( pStream->Memory );

            for( int iByte = 0; iByte < sTemp.Length(); iByte++ )
                pData[iByte] = 0x85 ^ sTemp[iByte+1];

            pStream->Position = 0;

            regIni->WriteBinaryStream( m_SettingsSection, m_Password, pStream );

            delete pStream;
        }

        delete regIni;
    }
}


void GetCommSettings( DEV_COMM_TYPE devType, COMMS_CFG& commCfg )
{
    // Init defaults
    commCfg.portType  = CT_UNUSED;
    commCfg.portName  = "";
    commCfg.portParam = 0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sPortTypeKey;
        UnicodeString sPortNameKey;
        UnicodeString sPortParamKey;

        sPortTypeKey.printf ( m_portType.w_str(),  devType );
        sPortNameKey.printf ( m_portName.w_str(),  devType );
        sPortParamKey.printf( m_portParam.w_str(), devType );

        commCfg.portType  = (COMM_TYPE) regIni->ReadInteger( m_CommsSection, sPortTypeKey,  commCfg.portType );
        commCfg.portName  =             regIni->ReadString ( m_CommsSection, sPortNameKey,  commCfg.portName );
        commCfg.portParam =             regIni->ReadInteger( m_CommsSection, sPortParamKey, commCfg.portParam );

        delete regIni;
    }
}


void SaveCommSettings( DEV_COMM_TYPE devType, const COMMS_CFG& commCfg )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sPortTypeKey;
        UnicodeString sPortNameKey;
        UnicodeString sPortParamKey;

        sPortTypeKey.printf ( m_portType.w_str(),  devType );
        sPortNameKey.printf ( m_portName.w_str(),  devType );
        sPortParamKey.printf( m_portParam.w_str(), devType );

        regIni->WriteInteger( m_CommsSection, sPortTypeKey,  commCfg.portType );
        regIni->WriteString ( m_CommsSection, sPortNameKey,  commCfg.portName );
        regIni->WriteInteger( m_CommsSection, sPortParamKey, commCfg.portParam );

        delete regIni;
    }
}


bool GetAutoAssignCommPorts( void )
{
    // Init defaults
    bool bAutoAssignPorts = true;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bAutoAssignPorts = regIni->ReadBool( m_CommsSection, m_autoAssignPorts, bAutoAssignPorts );

        delete regIni;
    }

    return bAutoAssignPorts;
}


void SaveAutoAssignCommPorts( bool bAutoAssignPorts )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool( m_CommsSection, m_autoAssignPorts, bAutoAssignPorts );

        delete regIni;
    }
}


bool GetDataLogging( DATA_LOG_TYPE logType, String& logFileName )
{
    // Non-persistent property
    logFileName = m_logFileName[logType];

    return m_loggingEnabled[logType];
}


void SaveDataLogging( DATA_LOG_TYPE logType, bool loggingOn, const String& logFileName )
{
    // Non-persistent property
    m_logFileName[logType]    = logFileName;
    m_loggingEnabled[logType] = loggingOn;
}


int GetRPMAvgingValue( void )
{
    int rpmAvgValue = 1;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        rpmAvgValue = regIni->ReadInteger( m_SettingsSection, m_RPMAveraging, rpmAvgValue );
        delete regIni;
    }

    return rpmAvgValue;
}


void SaveRPMAvgingValue( int newValue )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_SettingsSection, m_RPMAveraging, newValue );
        delete regIni;
    }
}


float GetConnMinTurnsValue( void )
{
    float connMinTurns = 1.0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        connMinTurns = regIni->ReadFloat( m_SettingsSection, m_ConnMinTurns, connMinTurns );
        delete regIni;
    }

    return connMinTurns;
}


void SaveConnMinTurnsValue( float newValue )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteFloat( m_SettingsSection, m_ConnMinTurns, newValue );
        delete regIni;
    }
}


void GetDialogPos( DIALOG_TYPE aDlg, int& top, int& left, int& iHeight, int& iWidth )
{
    // Caller is assumed to initialize pased params to default values
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sKey;

        sKey.printf( m_dlgPosTopEntry.w_str(), aDlg );
        top = regIni->ReadInteger( m_DialogPosSection, sKey, top );

        sKey.printf( m_dlgPosLeftEntry.w_str(), aDlg );
        left = regIni->ReadInteger( m_DialogPosSection, sKey, left );

        sKey.printf( m_dlgPosHeightEntry.w_str(), aDlg );
        iHeight = regIni->ReadInteger( m_DialogPosSection, sKey, iHeight );

        sKey.printf( m_dlgPosWidthEntry.w_str(), aDlg );
        iWidth = regIni->ReadInteger( m_DialogPosSection, sKey, iWidth );

        delete regIni;
    }
}


void SaveDialogPos( DIALOG_TYPE aDlg, int top, int left, int iHeight, int iWidth )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;

        sKey.printf( m_dlgPosTopEntry.w_str(), aDlg );
        regIni->WriteInteger( m_DialogPosSection, sKey, top );

        sKey.printf( m_dlgPosLeftEntry.w_str(), aDlg );
        regIni->WriteInteger( m_DialogPosSection, sKey, left );

        sKey.printf( m_dlgPosHeightEntry.w_str(), aDlg );
        regIni->WriteInteger( m_DialogPosSection, sKey, iHeight );

        sKey.printf( m_dlgPosWidthEntry.w_str(), aDlg );
        regIni->WriteInteger( m_DialogPosSection, sKey, iWidth );

        delete regIni;
    }
}


void GetRecentNameList( RECENT_NAME_LIST aList, TStringList* nameList )
{
    if( nameList == NULL )
        return;

    nameList->Clear();
    nameList->Sorted = false;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        UnicodeString sKey;
        sKey.printf( m_RecentNameSection.w_str(), aList );

        for( int iItem = 0; iItem < 30; iItem++ )
        {
           UnicodeString sItem;
           sItem.printf( m_recentNameItem.w_str(), iItem );

           UnicodeString sEntry = regIni->ReadString( sKey, sItem, L"" );

            if( sEntry.Length() == 0 )
                break;

            nameList->Add( sEntry );
        }

        delete regIni;
    }
}


void SaveRecentName( RECENT_NAME_LIST aList, const UnicodeString newName )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        TStringList* currNames = new TStringList;

        // Get current name list on registry for a section
        GetRecentNameList( aList, currNames );

        // If the newName is in the list, delete it.
        int currIndex = currNames->IndexOf( newName );

        if( currIndex >= 0 )
            currNames->Delete( currIndex );

        // Insert to the top of list
        currNames->Insert( 0, newName );

        // Make sure we have no more thean the max items in the list
        const int MaxNames = 30;

        while( currNames->Count > MaxNames )
            currNames->Delete( currNames->Count - 1 );

        // Write the list to section. Erase any current entries first
        UnicodeString sKey;
        sKey.printf( m_RecentNameSection.w_str(), aList );

        regIni->EraseSection( sKey );

        for( int iItem = 0; iItem < currNames->Count; iItem++ )
        {
            UnicodeString sItem;
            sItem.printf( m_recentNameItem.w_str(), iItem );

            regIni->WriteString( sKey, sItem, currNames->Strings[iItem] );
        }

        delete currNames;
        delete regIni;
    }
}


bool GetForceBackup( String& sDir )
{
    bool bResult = false;
    sDir         = "";

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bResult = regIni->ReadBool  ( m_SettingsSection, m_ForceBackupSave,    bResult );
        sDir    = regIni->ReadString( m_SettingsSection, m_ForceBackupSaveDir, sDir    );

        delete regIni;
    }

    return bResult;
}


void SaveForceBackup( bool bNewValue, String sDir )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool  ( m_SettingsSection, m_ForceBackupSave,    bNewValue );
        regIni->WriteString( m_SettingsSection, m_ForceBackupSaveDir, sDir      );

        delete regIni;
    }
}


bool GetPDSMonEnabled( void )
{
    bool bResult = false;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bResult = regIni->ReadBool( m_SettingsSection, m_PDSMonEnabled, bResult );

        delete regIni;
    }

    return bResult;
}


void SavePDSMonEnabled( bool bIsEnabled )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool( m_SettingsSection, m_PDSMonEnabled, bIsEnabled );

        delete regIni;
    }
}


eBR3OutFcn GetBaseRadioOut3Fcn( void )
{
    eBR3OutFcn eResult = eBR3Out_PDSAlarm;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        eResult = (eBR3OutFcn) regIni->ReadInteger( m_SettingsSection, m_BROut3Fcn, (int)eResult );

        delete regIni;
    }

    return eResult;
}


void SaveBaseRadioOut3Fcn( eBR3OutFcn eNewFcn )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_SettingsSection, m_BROut3Fcn, (int)eNewFcn );

        delete regIni;
    }
}


void GetTempCompParams( TEMP_COMP_PARAMS& tcParams )
{
    // Set defaults first
    tcParams.bApplyTempComp       = true;
    tcParams.fRisingSlopeThresh   = 2.0;
    tcParams.fRisingSlopeFactor   = -250.0;
    tcParams.fFallingSlopeThresh  = 0.0;
    tcParams.fFallingSlopeFactor  = -450.0;
    tcParams.fRTDAvgAlpha         = 0.0017;
    tcParams.fRTDAvgMaxVar        = 0.0;    // 0.0 means max variance checking disabled
    tcParams.bApplyRTDSlopeComp   = true;
    tcParams.fRTDSlopeAlpha       = 0.1;
    tcParams.fRTDSlopeMaxVar      = 0.0;    // 0.0 means max variance checking disabled
    tcParams.fRTDSlopeNegThresh   = -0.01;
    tcParams.fRTDSlopePosThresh   = 0.01;
    tcParams.fRTDSlopeFactor      = -100000.0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        // Legacy upgrade: if the Version entry is not 2, then erase the
        // whole section and re-write it with default values.
        if( regIni->ReadInteger( m_TempCompSection, m_tcVersion, 0 ) != CURR_RTD_TC_VER )
        {
            // This is a legacy ini file. Erase the section then write defaults for all values
            regIni->EraseSection( m_TempCompSection );

            regIni->WriteInteger( m_TempCompSection, m_tcVersion,          CURR_RTD_TC_VER               );
            regIni->WriteBool   ( m_TempCompSection, m_tcApplyTempComp,    tcParams.bApplyTempComp       );
            regIni->WriteString ( m_TempCompSection, m_tcRiseSlopeThresh,  tcParams.fRisingSlopeThresh   );
            regIni->WriteString ( m_TempCompSection, m_tcRiseSlopeFactor,  tcParams.fRisingSlopeFactor   );
            regIni->WriteString ( m_TempCompSection, m_tcFallSlopeThresh,  tcParams.fFallingSlopeThresh  );
            regIni->WriteString ( m_TempCompSection, m_tcFallSlopeFactor,  tcParams.fFallingSlopeFactor  );
            regIni->WriteString ( m_TempCompSection, m_tcRTDAlpha,         tcParams.fRTDAvgAlpha         );
            regIni->WriteString ( m_TempCompSection, m_tcRTDMaxVar,        tcParams.fRTDAvgMaxVar        );
            regIni->WriteBool   ( m_TempCompSection, m_tcApplySlopeComp,   tcParams.bApplyRTDSlopeComp   );
            regIni->WriteString ( m_TempCompSection, m_tcRTDSlopeAlpha,    tcParams.fRTDSlopeAlpha       );
            regIni->WriteString ( m_TempCompSection, m_tcRTDSlopMaxVar,    tcParams.fRTDSlopeMaxVar      );
            regIni->WriteString ( m_TempCompSection, m_tcRTDSlopeFactor,   tcParams.fRTDSlopeFactor      );
            regIni->WriteString ( m_TempCompSection, m_tcRTDSlopePThresh,  tcParams.fRTDSlopePosThresh   );
            regIni->WriteString ( m_TempCompSection, m_tcRTDSlopeNThresh,  tcParams.fRTDSlopeNegThresh   );
        }

        tcParams.bApplyTempComp     = regIni->ReadBool( m_TempCompSection, m_tcApplyTempComp,  tcParams.bApplyTempComp     );
        tcParams.bApplyRTDSlopeComp = regIni->ReadBool( m_TempCompSection, m_tcApplySlopeComp, tcParams.bApplyRTDSlopeComp );

        tcParams.fRisingSlopeThresh  = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcRiseSlopeThresh,  tcParams.fRisingSlopeThresh   ), tcParams.fRisingSlopeThresh   );
        tcParams.fRisingSlopeFactor  = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcRiseSlopeFactor,  tcParams.fRisingSlopeFactor   ), tcParams.fRisingSlopeFactor   );
        tcParams.fFallingSlopeThresh = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcFallSlopeThresh,  tcParams.fFallingSlopeThresh  ), tcParams.fFallingSlopeThresh  );
        tcParams.fFallingSlopeFactor = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcFallSlopeFactor,  tcParams.fFallingSlopeFactor  ), tcParams.fFallingSlopeFactor  );
        tcParams.fRTDAvgAlpha        = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcRTDAlpha,         tcParams.fRTDAvgAlpha         ), tcParams.fRTDAvgAlpha         );
        tcParams.fRTDAvgMaxVar       = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcRTDMaxVar,        tcParams.fRTDAvgMaxVar        ), tcParams.fRTDAvgMaxVar        );
        tcParams.fRTDSlopeAlpha      = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcRTDSlopeAlpha,    tcParams.fRTDSlopeAlpha       ), tcParams.fRTDSlopeAlpha       );
        tcParams.fRTDSlopeMaxVar     = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcRTDSlopMaxVar,    tcParams.fRTDSlopeMaxVar      ), tcParams.fRTDSlopeMaxVar      );
        tcParams.fRTDSlopeFactor     = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcRTDSlopeFactor,   tcParams.fRTDSlopeFactor      ), tcParams.fRTDSlopeFactor      );
        tcParams.fRTDSlopePosThresh  = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcRTDSlopePThresh,  tcParams.fRTDSlopePosThresh   ), tcParams.fRTDSlopePosThresh   );
        tcParams.fRTDSlopeNegThresh  = StrToFloatDef( regIni->ReadString( m_TempCompSection, m_tcRTDSlopeNThresh,  tcParams.fRTDSlopeNegThresh   ), tcParams.fRTDSlopeNegThresh   );

        // 1v059: tcParams.fRTDSlopeFactor must always be negative. If it is positive,
        // then flip its value
        if( tcParams.fRTDSlopeFactor > 0 )
            tcParams.fRTDSlopeFactor = -tcParams.fRTDSlopeFactor;

        delete regIni;
    }
}


bool GetTempCompEnabled( void )
{
    TEMP_COMP_PARAMS tcParams;
    GetTempCompParams( tcParams );

    return tcParams.bApplyTempComp;
}


void SaveTempCompEnabled( bool bIsEnabled )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool( m_TempCompSection, m_tcApplyTempComp, bIsEnabled );

        delete regIni;
    }
}


float GetMinBattLevel( void )
{
    float fResult = 3.3;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        fResult = StrToFloatDef( regIni->ReadString( m_SettingsSection, m_MinBattLevel, fResult ), fResult );

        delete regIni;
    }

    return fResult;
}


DWORD GetRadioChanWaitTimeout( void )
{
    // Init defaults
    DWORD dwTimeout = 30000;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        dwTimeout = regIni->ReadInteger( m_CommsSection, m_radioChanTimeout, dwTimeout );

        delete regIni;
    }

    return dwTimeout;
}


void SaveRadioChanWaitTimeout( DWORD dwNewTimeout )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_CommsSection, m_radioChanTimeout, dwNewTimeout );

        delete regIni;
    }
}


int GetDefaultRadioChan( void )
{
    // Init defaults
    int iDefaultChan = 25;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        iDefaultChan = regIni->ReadInteger( m_CommsSection, m_defaultRadioChan, iDefaultChan );

        delete regIni;
    }

    return iDefaultChan;
}


void GetBatteryThresholds( BATT_THRESH_VALS& battThresh )
{
    // Set defaults
    battThresh.fReadingValidThresh = 2.50;   // Battery readings below this value are ignored, in V
    battThresh.fMinOperValue       = 3.35;   // Operation not allowed below this threshold, in V
    battThresh.fGoodThresh         = 3.65;   // Battery is in a good state at or above this, in V

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        battThresh.fReadingValidThresh = regIni->ReadFloat( m_SettingsSection, m_BattValidThresh, battThresh.fReadingValidThresh );
        battThresh.fMinOperValue       = regIni->ReadFloat( m_SettingsSection, m_BattMinOperV,    battThresh.fMinOperValue       );
        battThresh.fGoodThresh         = regIni->ReadFloat( m_SettingsSection, m_BattGoodV,       battThresh.fGoodThresh         );

        delete regIni;
    }
}


void SaveBatteryThresholds( const BATT_THRESH_VALS& battThresh )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteFloat( m_SettingsSection, m_BattValidThresh, battThresh.fReadingValidThresh );
        regIni->WriteFloat( m_SettingsSection, m_BattMinOperV,    battThresh.fMinOperValue       );
        regIni->WriteFloat( m_SettingsSection, m_BattGoodV,       battThresh.fGoodThresh         );

        delete regIni;
    }
}


void GetWitsPortsSettings ( COMMS_CFG& commCfg )
{
    commCfg.portType  = CT_UNUSED;
    commCfg.portName  = "1";
    commCfg.portParam = 0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        commCfg.portType  = (COMM_TYPE) regIni->ReadInteger( m_WitsSection, m_WitsPortType,  commCfg.portType  );
        commCfg.portName  = regIni->ReadString ( m_WitsSection, m_WitsPortName,  commCfg.portName  );
        commCfg.portParam = regIni->ReadInteger( m_WitsSection, m_WitsPortParam, commCfg.portParam );

        delete regIni;
    }
}


void SaveWitsPortsSettings( const COMMS_CFG& commCfg )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteInteger( m_WitsSection, m_WitsPortType,  commCfg.portType  );
        regIni->WriteString ( m_WitsSection, m_WitsPortName,  commCfg.portName  );
        regIni->WriteInteger( m_WitsSection, m_WitsPortParam, commCfg.portParam );

        delete regIni;
    }
}

void GetWitsRfcHdrSettings ( bool& bRfcEnabled, int& iRfcCode, String& sRfcData )
{
    bRfcEnabled = false;
    iRfcCode    = 0;
    sRfcData    = "";

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bRfcEnabled = regIni->ReadBool   ( m_WitsSection, m_WitsRfcHdr,     bRfcEnabled );
        iRfcCode    = regIni->ReadInteger( m_WitsSection, m_WitsRfcHdrCode, iRfcCode    );
        sRfcData    = regIni->ReadString ( m_WitsSection, m_WitsRfcHdrData, sRfcData    );

        delete regIni;
    }
}


void SaveWitsRfcHdrSettings( const bool bRfcEnabled, const int iRfcCode, const String sRfcData )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool   ( m_WitsSection, m_WitsRfcHdr,     bRfcEnabled );
        regIni->WriteInteger( m_WitsSection, m_WitsRfcHdrCode, iRfcCode    );
        regIni->WriteString ( m_WitsSection, m_WitsRfcHdrData, sRfcData    );

        delete regIni;
    }
}


void GetWitsStreamHdrSettings( bool& bStreamEnabled, int& iStreamCode, String& sStreamData )
{
    bStreamEnabled = false;
    iStreamCode    = 0;
    sStreamData    = "";

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bStreamEnabled = regIni->ReadBool   ( m_WitsSection, m_WitsStreamHdr,     bStreamEnabled );
        iStreamCode    = regIni->ReadInteger( m_WitsSection, m_WitsStreamHdrCode, iStreamCode    );
        sStreamData    = regIni->ReadString ( m_WitsSection, m_WitsStreamHdrData, sStreamData    );

        delete regIni;
    }
}


void SaveWitsStreamHdrSettings( const bool bStreamEnabled, const int iStreamCode, const String sStreamData )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool   ( m_WitsSection, m_WitsStreamHdr,     bStreamEnabled );
        regIni->WriteInteger( m_WitsSection, m_WitsStreamHdrCode, iStreamCode    );
        regIni->WriteString ( m_WitsSection, m_WitsStreamHdrData, sStreamData    );

        delete regIni;
    }
}


int GetWitsFieldSettings( int iWitsPktType, WITS_FIELD_SETTING* fieldList, const int iNumFields )
{
    int iFieldsParsed = 0;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        // First get the number of field entries for this type of packet
        String sKey;
        sKey.printf( m_WitsFieldCount.w_str(), iWitsPktType );

        int iNumberOfEntries = regIni->ReadInteger( m_WitsSection, sKey, 0 );

        // If the fieldList is NULL, then all the user wants to know if how
        // many entries there are - return that now
        if( fieldList == NULL )
        {
            delete regIni;
            return iNumberOfEntries;
        }

        // Adjust the number of fields to report based on the max the user is asking for
        if( iNumberOfEntries > iNumFields )
            iNumberOfEntries = iNumFields;

        // Set the return array to defaults to start
        for( int iField = 0; iField < iNumberOfEntries; iField++ )
        {
            fieldList[iField].bEnabled   = false;
            fieldList[iField].iCode      = 0;
            fieldList[iField].iFieldEnum = 0;
        }

        // Loop here reading entries. Each field entry is a triplet of values:
        // the field enum, its WITS code, and whether it is enabled or not
        int iFieldsParsed = 0;

        TStringList* itemsList = new TStringList();

        for( int iField = 0; iField < iNumberOfEntries; iField++ )
        {
            sKey.printf( m_WitsFieldInfo.w_str(), iWitsPktType, iField );

            String sInfo = regIni->ReadString( m_WitsSection, sKey, "" );

            if( ConvertDelimitedString( sInfo, itemsList, "," ) == 3 )
            {
                fieldList[iFieldsParsed].iFieldEnum = itemsList->Strings[0].ToIntDef( 0 );
                fieldList[iFieldsParsed].iCode      = itemsList->Strings[1].ToIntDef( 0 );
                fieldList[iFieldsParsed].bEnabled   = ( itemsList->Strings[2].CompareIC( "1" ) == 0 );

                iFieldsParsed++;
            }
        }

        delete itemsList;

        delete regIni;
    }

    return iFieldsParsed;
}


void SaveWitsFieldSettings( const int iWitsPktType, const WITS_FIELD_SETTING* fieldList, const int iNumEntries )
{
    if( fieldList == NULL )
        return;

    if( iNumEntries <= 0 )
        return;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        String sKey;

        // Loop through the set of field entries, creating an entry triplet for each field
        for( int iCurrEntry = 0; iCurrEntry < iNumEntries; iCurrEntry++ )
        {
            String sFieldInfo;
            sFieldInfo.printf( L"%d,%d,%d", fieldList[iCurrEntry].iFieldEnum, fieldList[iCurrEntry].iCode, fieldList[iCurrEntry].bEnabled );

            sKey.printf( m_WitsFieldInfo.w_str(), iWitsPktType, iCurrEntry );

            regIni->WriteString( m_WitsSection, sKey, sFieldInfo );
        }

        // Save the number of values written
        sKey.printf( m_WitsFieldCount.w_str(), iWitsPktType );
        regIni->WriteInteger( m_WitsSection, sKey, iNumEntries );

        delete regIni;
    }
}


AnsiString FormatWITSCode( int iCode )
{
    // Special handler for WITS codes
    AnsiString sCode;
    sCode.printf( "%04d", iCode );

    return sCode;
}


void GetAutoRecSettings( AUTO_REC_SETTINGS& arSettings )
{
    arSettings.bAutoRecEnabled   = false;
    arSettings.minRPMToStart     = 10;   // TODO - get 'real' default from Brian
    arSettings.minTorqueToStart  = 2500; // Also note, might want different defaults based on current UoM
    arSettings.stopTime          = 5;
    arSettings.crossThreadThresh = 12000;
    arSettings.bAlarmOnError     = true;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        arSettings.bAutoRecEnabled   = regIni->ReadBool   ( m_AutoRecSection, m_AREnabled,       arSettings.bAutoRecEnabled   );
        arSettings.minRPMToStart     = regIni->ReadInteger( m_AutoRecSection, m_ARMinRPM,        arSettings.minRPMToStart     );
        arSettings.minTorqueToStart  = regIni->ReadInteger( m_AutoRecSection, m_ARMinTorque,     arSettings.minTorqueToStart  );
        arSettings.stopTime          = regIni->ReadInteger( m_AutoRecSection, m_ARStopTime,      arSettings.stopTime          );
        arSettings.crossThreadThresh = regIni->ReadInteger( m_AutoRecSection, m_ARXThreadThresh, arSettings.crossThreadThresh );
        arSettings.bAlarmOnError     = regIni->ReadBool   ( m_AutoRecSection, m_ARAlarmOnError,  arSettings.bAlarmOnError     );

        delete regIni;
    }

    // When getting settings, also update our cached copy of the enabled flag
    m_bsAutoRecEn = arSettings.bAutoRecEnabled ? eBS_On : eBS_Off;
}


void SaveAutoRecSettings( const AUTO_REC_SETTINGS& arSettings )
{
    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        regIni->WriteBool   ( m_AutoRecSection, m_AREnabled,       arSettings.bAutoRecEnabled   );
        regIni->WriteInteger( m_AutoRecSection, m_ARMinRPM,        arSettings.minRPMToStart     );
        regIni->WriteInteger( m_AutoRecSection, m_ARMinTorque,     arSettings.minTorqueToStart  );
        regIni->WriteInteger( m_AutoRecSection, m_ARStopTime,      arSettings.stopTime          );
        regIni->WriteInteger( m_AutoRecSection, m_ARXThreadThresh, arSettings.crossThreadThresh );
        regIni->WriteBool   ( m_AutoRecSection, m_ARAlarmOnError,  arSettings.bAlarmOnError     );

        delete regIni;
    }

    // When saving, update our cached copy of the enabled flag too
    m_bsAutoRecEn = arSettings.bAutoRecEnabled ? eBS_On : eBS_Off;
}


bool GetAutoRecordEnabled( void )
{
    // Return the cached version, if value is known
    if( m_bsAutoRecEn == eBS_On )
        return true;
    else if( m_bsAutoRecEn == eBS_Off )
        return false;

    // Don't know current state - get it from the registry
    bool bEnabled = false;

    TRegistryIniFile* regIni = CreateRegIni();

    if( regIni != NULL )
    {
        bEnabled = regIni->ReadBool( m_AutoRecSection, m_AREnabled, bEnabled );

        delete regIni;
    }

    // Update our cache copy, then return
    m_bsAutoRecEn = bEnabled ? eBS_On : eBS_Off;

    return bEnabled;
}


//
//  Private Functions
//

TRegistryIniFile* CreateRegIni( void )
{
    // Caller must free the created object
    return new TRegistryIniFile( String( "Software\\Tesco\\WTTTS Manager\\V" ) + IntToStr( GetApplMajorVer() ) );
}

