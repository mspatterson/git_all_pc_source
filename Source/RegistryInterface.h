#ifndef RegistryInterfaceH
#define RegistryInterfaceH

    #include "SerialPortUtils.h"
    #include "Averaging.h"

    typedef enum {
        SDT_NEW,       // First time this program has been run
        SDT_GOOD,      // Program has been run before, and closed properly
        SDT_CRASH,     // Program has been run before, but not closed properly
        NBR_SHUT_DOWN_TYPES
    } SHUT_DOWN_TYPE;

    SHUT_DOWN_TYPE GetLastShutdown( void );
    void           SetProgramInUse( void );
    void           SetProgramShutdownGood( void );

    DWORD          GetProcessPriority( void );
    void           SetProcessPriority( DWORD newPriority );

    UnicodeString  GetJobDataDir( void );
    void           SetJobDataDir( UnicodeString newDir );

    void           GetMostRecentJobsList( TStrings* slJobList );
    UnicodeString  GetMostRecentJob( void );
    void           AddToMostRecentJobsList( UnicodeString sNewJob );

    int            GetDefaultUnitsOfMeasure( void );
    void           SetDefaultUnitsOfMeasure( int newDefaultUOM );

    // Control size helpers
    typedef enum {
        CN_MAIN_PG_CTRL,
        CN_MAIN_GRAPH_HEIGHT,
        NBR_CTRL_NAMES
    } CTRL_NAME;

    int            GetCtrlSize( CTRL_NAME aCtrl );
    void           SaveCtrlSize( CTRL_NAME aCtrl, int newSize );
                     // GetCtrlSize() returns -1 if the control size has
                     // not been previously saved to the registry

    int            GetShoulderBoxSize( void );
    void           SaveShoulderBoxSize( int newSize );

    bool           GetShowShldrCursors( void );
    void           SaveShowShldrCursors( bool newSetting );

    void           GetGraphColors ( TColor& rpmColor, TColor& torqueColor, TColor& tenColor );
    void           SaveGraphColors( TColor  rpmColor, TColor  torqueColor, TColor  tenColor );

    bool           GetNagOnAutoShoulderOverride( void );
    void           SaveNagOnAutoShoulderOverride( bool newValue );

    void           GetCDSSlipsSettings ( int unitsOfMeasure, int& cdsRel, int& cdsslipLock, int& cdsLockSlipsRel );
    void           SaveCDSSlipsSettings( int unitsOfMeasure, int  cdsRel, int  cdsslipLock, int  cdsLockSlipsRel );
                     // CDS and Slips threshold functions

    int            GetInterlockReleaseTime( void );
    void           SaveInterlockReleaseTime( int newValue );
                     // Min time at release threshold before can release

    bool           GetInterlockEn( void );
    void           SaveInterlockEn( bool bInterlockEn );
                     // Interlock enabled state

    void           GetPDSSwitchThresholds( int& iPDSOnThresh, int& iPDSOffThresh );
                     // PDS switch thresholds

    typedef enum {
        eBR3Out_PDSAlarm,
        eBR3Out_AutoRecAlarm
    } eBR3OutFcn;

    eBR3OutFcn     GetBaseRadioOut3Fcn( void );
    void           SaveBaseRadioOut3Fcn( eBR3OutFcn eNewFcn );


    typedef enum {
        AT_TORQUE,
        AT_TENSION,
        NBR_AVG_TYPES
    } AVG_TYPE;

    // Averaging Level Properties
    typedef struct {
        float      param;
        float      absVariance;
    } AVG_LEVEL_PROPS;

    void GetAveragingSettings ( AVG_TYPE avgType,       AVERAGING_PARAMS& avgParams );
    void SaveAveragingSettings( AVG_TYPE avgType, const AVERAGING_PARAMS& avgParams );


    typedef struct {
        int turns;
        int torque;
        int RPM;
        int time;
        int tension;
        bool bAutoCalTq;
    } GRAPH_AXIS_VALUES;

    void           GetGraphAxisSpan ( int unitsOfMeasure,       GRAPH_AXIS_VALUES& params );
    void           SaveGraphAxisSpan( int unitsOfMeasure, const GRAPH_AXIS_VALUES& params );
                     // Helper functions to get / save graph axis span values.
                     // The unitsOfMeasure is an integer value representing the units
                     // of measure to which the params pertain.

    void           GetGraphAxisIncrements ( int unitsOfMeasure,       GRAPH_AXIS_VALUES& params );
    void           SaveGraphAxisIncrements( int unitsOfMeasure, const GRAPH_AXIS_VALUES& params );
                     // Helper functions to get / save graph axis extention params.
                     // The unitsOfMeasure is an integer value representing the units
                     // of measure to which the params pertain.



    // Misc functions
    UnicodeString  GetSysAdminPassword( void );
    void           SaveSysAdminPassword( const String& newPW );

    // Comms settings
    typedef enum {
        DT_WTTTS,
        DT_LEGACY_SUB,
        DT_NULL,
        NBR_DEVICE_TYPES
    } DEVICE_TYPE;

    extern const String DeviceTypeText[NBR_DEVICE_TYPES];

    typedef enum {
        DCT_WTTTS,
        DCT_BASE,
        DCT_MGMT,
        NBR_DEV_COMM_TYPES
    } DEV_COMM_TYPE;

    void           GetCommSettings ( DEV_COMM_TYPE devType, COMMS_CFG& commCfg );
    void           SaveCommSettings( DEV_COMM_TYPE devType, const COMMS_CFG& commCfg );

    bool           GetAutoAssignCommPorts ( void );
    void           SaveAutoAssignCommPorts( bool bAutoAssignPorts );

    typedef enum {
        DLT_RAW_DATA,
        DLT_CAL_DATA,
        DLT_RFC_DATA,
        DLT_CHART_DATA,
        NBR_DATA_LOG_TYPES
    } DATA_LOG_TYPE;

    bool           GetDataLogging ( DATA_LOG_TYPE logType, String& logFileName );
    void           SaveDataLogging( DATA_LOG_TYPE logType, bool loggingOn, const String& logFileName );

    int            GetRPMAvgingValue( void );
    void           SaveRPMAvgingValue( int newValue );

    float          GetConnMinTurnsValue( void );
    void           SaveConnMinTurnsValue( float newValue );

    typedef enum {
        DT_CONN_DLG,
        DT_CIRC_DLG,
        DT_HARDWARE_DLG
    } DIALOG_TYPE;

    void SaveDialogPos( DIALOG_TYPE aDlg, int top, int  left, int  iHeight, int  iWidth );
    void GetDialogPos( DIALOG_TYPE aDlg, int& top, int& left, int& iHeight, int& iWidth );

    typedef enum {
        RNL_CLIENT_REP,
        RNL_TESCO_REP,
        RNL_THREAD_REP,
        NBR_RECENT_NAME_LISTS
    } RECENT_NAME_LIST;

    void GetRecentNameList( RECENT_NAME_LIST aList, TStringList* nameList );
    void SaveRecentName( RECENT_NAME_LIST aList, const UnicodeString newName );

    bool GetForceBackup( String& sDir );
    void SaveForceBackup( bool bNewValue, String sDir );

    bool GetPDSMonEnabled( void );
    void SavePDSMonEnabled( bool bIsEnabled );

    typedef struct {
        bool  bApplyTempComp;
        float fRisingSlopeThresh;
        float fRisingSlopeFactor;
        float fFallingSlopeThresh;
        float fFallingSlopeFactor;
        float fRTDAvgAlpha;
        float fRTDAvgMaxVar;
        bool  bApplyRTDSlopeComp;
        float fRTDSlopeAlpha;
        float fRTDSlopeMaxVar;
        float fRTDSlopeNegThresh;
        float fRTDSlopePosThresh;
        float fRTDSlopeFactor;
    } TEMP_COMP_PARAMS;

    void GetTempCompParams( TEMP_COMP_PARAMS& tcParams );

    bool GetTempCompEnabled( void );
    void SaveTempCompEnabled( bool bIsEnabled );

    float GetMinBattLevel( void );

    DWORD GetRadioChanWaitTimeout( void );
    void  SaveRadioChanWaitTimeout( DWORD dwNewTimeout );

    int   GetDefaultRadioChan( void );

    typedef struct {
        float fReadingValidThresh;   // Battery readings below this value are ignored, in V
        float fMinOperValue;         // Operation not allowed below this threshold, in V
        float fGoodThresh;           // Battery is in a good state at or above this, in V
    } BATT_THRESH_VALS;

    void GetBatteryThresholds( BATT_THRESH_VALS& battThresh );
    void SaveBatteryThresholds( const BATT_THRESH_VALS& battThresh );

    // WITS Struct/Gets/Saves
    typedef struct {
        bool bEnabled;
        int iCode;
        int iFieldEnum;
    } WITS_FIELD_SETTING;

    void GetWitsPortsSettings ( COMMS_CFG& commCfg );
    void SaveWitsPortsSettings( const COMMS_CFG& commCfg );

    void GetWitsRfcHdrSettings ( bool& bRfcEnabled, int& iRfcCode, String& sRfcData );
    void SaveWitsRfcHdrSettings( const bool bRfcEnabled, const int iRfcCode, const String sRfcData );

    void GetWitsStreamHdrSettings ( bool& bStreamEnabled, int& iStreamCode, String& sStreamData );
    void SaveWitsStreamHdrSettings( const bool bStreamEnabled, const int iStreamCode, const String sStreamData );

    int  GetWitsFieldSettings ( int iWitsType, WITS_FIELD_SETTING* fieldList, const int iNumFields );
    void SaveWitsFieldSettings( const int iWitsType, const WITS_FIELD_SETTING* fieldList, const int iNumEntries );

    AnsiString FormatWITSCode( int iCode );

    // Auto-recording
    typedef struct {
        bool bAutoRecEnabled;
        int  minRPMToStart;
        int  minTorqueToStart;
        int  stopTime;
        int  crossThreadThresh;
        bool bAlarmOnError;
    } AUTO_REC_SETTINGS;

    void GetAutoRecSettings( AUTO_REC_SETTINGS& arSettings );
    void SaveAutoRecSettings( const AUTO_REC_SETTINGS& arSettings );

    bool GetAutoRecordEnabled( void );

#endif
