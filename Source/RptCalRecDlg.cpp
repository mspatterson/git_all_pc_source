#include <vcl.h>
#pragma hdrstop

#include "RptCalRecDlg.h"
#include "Applutils.h"
#include "RptHelpers.h"

#pragma package(smart_init)
#pragma link "frxClass"
#pragma resource "*.dfm"


TRptCalRecForm *RptCalRecForm;


__fastcall TRptCalRecForm::TRptCalRecForm(TComponent* Owner) : TForm(Owner)
{
}


void TRptCalRecForm::ShowReport( TJob* currJob, int start, int finish )
{
    // Validate current job.
    if( currJob == NULL )
        return;

    m_pJob = currJob;

    // Validate start and end record numbers. These numbers are 1-based
    if( ( start < 1 ) || ( finish < 1 ) )
    {
        MessageDlg( "There are no Calibration Records to report.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    // If the last calibration requested is too large, just set it to the
    // last calibration in the job.
    if( finish > currJob->NbrCalibrationRecs )
        finish = currJob->NbrCalibrationRecs;

    if( finish < start )
        return;

    // Initialize job name memos - these never change
    RptSetMemoText( currJob, CalRecReport, "ClientNameMemo1", currJob->Client );
    RptSetMemoText( currJob, CalRecReport, "JobLocnMemo1",    currJob->Location );
    RptSetMemoText( currJob, CalRecReport, "JobDateMemo",     currJob->DateTimeString() );

        // Set number of records to read
    CalRecDataSet->RangeEndCount = finish - start + 1;
    CalRecDataSet->RangeEnd      = reCount;

    // Build the list of section recs. We are always guaranteed to be showing
    // at least one section
    int nbrCalRecs = finish - start + 1;

    m_rptStartRec = start;

    m_calRecs = new CALIBRATION_REC[nbrCalRecs];

    // Calibration records are always stored sequentially in job manager, starting
    // with record 1. Therefore, a calibration's index is its calibration nbr - 1.
    for( int iCal = 0; iCal < nbrCalRecs; iCal++ )
        currJob->GetCalibrationRec( iCal + start - 1, m_calRecs[iCal] );

    // Preview the report
    try
    {
        CalRecReport->ShowReport( true );
    }
    catch( ... )
    {
    }

    delete [] m_calRecs;
}


void TRptCalRecForm::ShowReport( TJob* currJob )
{
    // Display report for all calibration
    ShowReport( currJob, 1, currJob->NbrCalibrationRecs );
}


void __fastcall TRptCalRecForm::CalRecDataSetGetValue(const UnicodeString VarName, Variant &Value)
{
    if( VarName == "Cal_calRecNbr" )
        Value = CalRecDataSet->RecNo + m_rptStartRec;
    else
        RptGetCalRecField( m_pJob, m_calRecs[CalRecDataSet->RecNo], VarName, Value );
}


void __fastcall TRptCalRecForm::CalRecReportPreview(TObject *Sender)
{
    TfrxReport* frxReport = (TfrxReport*)Sender;
    frxReport->PreviewForm->BorderIcons = TBorderIcons() << biSystemMenu << biMaximize;
}

