#ifndef RptCalRecDlgH
#define RptCalRecDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "frxClass.hpp"

#include "JobManager.h"


class TRptCalRecForm : public TForm
{
__published:    // IDE-managed Components
    TfrxReport *CalRecReport;
    TfrxUserDataSet *CalRecDataSet;
    void __fastcall CalRecReportPreview(TObject *Sender);
    void __fastcall CalRecDataSetGetValue(const UnicodeString VarName, Variant &Value);

private:    // User declarations
    CALIBRATION_REC* m_calRecs;
    int              m_rptStartRec;
    TJob*            m_pJob;

public:        // User declarations
    __fastcall TRptCalRecForm(TComponent* Owner);
    void ShowReport( TJob* currJob, int start, int finish );
    void ShowReport( TJob* currJob );
};
//---------------------------------------------------------------------------
extern PACKAGE TRptCalRecForm *RptCalRecForm;
//---------------------------------------------------------------------------
#endif
