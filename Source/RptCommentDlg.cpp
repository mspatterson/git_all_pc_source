#include <vcl.h>
#pragma hdrstop

#include "RptCommentDlg.h"
#include "Applutils.h"
#include "RptHelpers.h"

#pragma package(smart_init)
#pragma link "frxClass"
#pragma resource "*.dfm"


TCommentReportForm *CommentReportForm;


__fastcall TCommentReportForm::TCommentReportForm(TComponent* Owner): TForm(Owner)
{
}


void TCommentReportForm::ShowReport( TJob* currJob )
{
    // Validate current job.
    if( currJob == NULL )
        return;

    // Remember the job
    m_currJob = currJob;

    // Set number of records to display. If there are no comments,
    // set a flag and force one 'record' to be displayed.
    if( currJob->NbrMainComments > 0 )
    {
        m_haveRecs = true;

        frxCommentDataset->RangeEndCount = currJob->NbrMainComments;
    }
    else
    {
        m_haveRecs = false;

        frxCommentDataset->RangeEndCount = 1;
    }

    frxCommentDataset->RangeEnd = reCount;

    // Initialize job name memos - these never change
    RptSetMemoText( currJob, CommentReport, "ClientNameMemo", currJob->Client );
    RptSetMemoText( currJob, CommentReport, "JobLocnMemo",    currJob->Location );
    RptSetMemoText( currJob, CommentReport, "JobDateMemo",    currJob->DateTimeString() );

    // preview the report
    CommentReport->ShowReport( true );
}


void __fastcall TCommentReportForm::frxCommentDatasetGetValue(const UnicodeString VarName, Variant &Value)
{
    if( m_haveRecs )
    {
        MAIN_COMMENT comment;

        if( m_currJob->GetMainComment( frxCommentDataset->RecNo, comment ) )
        {
            if( VarName == "CommentDate" )
                Value = m_currJob->DateTimeString();
            else if( VarName == "CommentText" )
                Value = comment.entryText;
            else
                Value = "Unknown field: " + VarName;
        }
        else
        {
            Value = "Bad rec nbr " + IntToStr( frxCommentDataset->RecNo );
        }
    }
    else
    {
        if( VarName == "CommentDate" )
            Value = "";
        else if( VarName == "CommentText" )
            Value = "(no comments)";
        else
            Value = "Unknown field: " + VarName;
    }
}


void __fastcall TCommentReportForm::CommentReportPreview(TObject *Sender)
{
    TfrxReport* frxReport = (TfrxReport*)Sender;
    frxReport->PreviewForm->BorderIcons = TBorderIcons() << biSystemMenu << biMaximize;
}

