//---------------------------------------------------------------------------
#ifndef RptCommentDlgH
#define RptCommentDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "frxClass.hpp"
#include "JobManager.h"
//---------------------------------------------------------------------------
class TCommentReportForm : public TForm
{
__published:    // IDE-managed Components
    TfrxReport *CommentReport;
    TfrxUserDataSet *frxCommentDataset;
    void __fastcall frxCommentDatasetGetValue(const UnicodeString VarName, Variant &Value);
    void __fastcall CommentReportPreview(TObject *Sender);


private:
     TJob* m_currJob;
     bool  m_haveRecs;

public:
    __fastcall TCommentReportForm(TComponent* Owner);
    void ShowReport( TJob* currJob );
};
//---------------------------------------------------------------------------
extern PACKAGE TCommentReportForm *CommentReportForm;
//---------------------------------------------------------------------------
#endif
