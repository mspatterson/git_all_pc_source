#include <vcl.h>
#pragma hdrstop

#include <Clipbrd.hpp>

#include "RptConnDlg.h"
#include "Applutils.h"
#include "RptHelpers.h"
#include "Messages.h"
#include "RptStatsDlg.h"

#pragma package(smart_init)
#pragma link "frxClass"
#pragma resource "*.dfm"


TRptConnForm *RptConnForm;


// Items to be disabled when there's no shoulder
String sShoulderItems[NBR_SHLDR_ITEMS] =
{
    "ShoulderGB",
    "ShoulderMemo",
    "ShoulderTorqueMemo",
    "ShoulderMinTorqueMemo",
    "ShoulderMaxTorqueMemo",
    "ShoulderTurnsMemo",
    "ShoulderTurnRangeMemo",
    "ShoulderTorqueData",
    "ShoulderTurnsData",
    "ConnDataSetConn_SecShlderMinTqe",
    "ConnDataSetConn_SecShlderMaxTqe",
    "ConnDataSetConn_SecShoulderPostShldrTurns",
};


// The number of connections to base the progress bar off of
int m_numConnections = 1;


//
// Class Implementation
//

__fastcall TRptConnForm::TRptConnForm(TComponent* Owner) : TForm(Owner)
{
}


void  TRptConnForm::ShowReport( TJob* currJob, int start, int finish )
{
    // Display report for range from start point to finish point
    // validate current job. Passed connection range is 1-based.
    if( currJob == NULL )
        return;

    if( start < 1 )
        return;

    // validate start session and finish session.
    if( finish < 1 )
        return;

    if( finish < start )
        return;

    // Initialize job name memos - these never change
    RptSetMemoText( currJob, ConnReport, "ClientNameMemo1", currJob->Client );
    RptSetMemoText( currJob, ConnReport, "ClientNameMemo2", currJob->Client );
    RptSetMemoText( currJob, ConnReport, "JobLocnMemo1",    currJob->Location );
    RptSetMemoText( currJob, ConnReport, "JobLocnMemo2",    currJob->Location );
    RptSetMemoText( currJob, ConnReport, "JobDateMemo",     currJob->DateTimeString() );

    // Initialize the report units of measure memos
    RptSetMemoTextToUnits( currJob, ConnReport, "ShoulderMemo",   "Shoulder", MT_TORQUE_SHORT );
    RptSetMemoTextToUnits( currJob, ConnReport, "TorqueMemo",     "Torque",   MT_TORQUE_SHORT );
    RptSetMemoTextToUnits( currJob, ConnReport, "ConnLengthMemo", "Length",   MT_DIST1_SHORT  );

    // Determine the number of connection records that will be displayed.
    // The range passed is a range of connection numbers, but there can
    // be multiple records per connection number. We want to display
    // connections in order, and because connections can be removed
    // connection numbers are necessarily in order in job manager. So,
    // create a list to contain the index numbers of the connections
    // to be displayed. By scanning through all connections by increasing
    // connection number, the indices in this list will guarantee that
    // connections are displayed in ascending order.
    TList* connIndexList = new TList();

    for( int iRec = 0; iRec < currJob->NbrConnRecs; iRec++ )
    {
        CONNECTION_INFO connInfo;

        if( currJob->GetConnection( iRec, connInfo ) )
        {
            if( ( connInfo.connNbr >= start ) && ( connInfo.connNbr <= finish ) )
                connIndexList->Add( (void*)iRec );
        }
    }

    // The number of items in the list is the number of recs to be displayed.
    if( connIndexList->Count == 0 )
    {
        delete connIndexList;

        MessageDlg( "There are no connection records in the requested range.", mtInformation, TMsgDlgButtons() << mbOK, 0 );

        return;
    }

    // Set number of records to read
    ConnDataSet->RangeEndCount = connIndexList->Count;
    ConnDataSet->RangeEnd      = reCount;

    m_numConnections = connIndexList->Count;

    // Now get all the data we need
    m_connRecs = new CONNECTION_RECORD[connIndexList->Count];

    for( int iItem = 0; iItem < connIndexList->Count; iItem++ )
    {
        int connIndexNbr = (int)( connIndexList->Items[ iItem ] );

        currJob->GetConnection( connIndexNbr, m_connRecs[iItem].connInfo );

        // Search for the requested section number in the section records.
        // Section records are always stored sequentially starting at section 1,
        // so a section's index is just its number - 1.
        int sectIndex = m_connRecs[iItem].connInfo.sectionNbr - 1;

        currJob->GetSectionRec( sectIndex, m_connRecs[iItem].connSectInfo );
    }

    // Sort the records in ascending connection number order. The connection
    // records will roughly be in order, so there's no need to have an
    // optimized sort routine.
    bool bSorted = false;

    while( !bSorted )
    {
        // Assume we'll be sorted to start
        bSorted = true;

        for( int iRec = 0; iRec < m_numConnections - 1; iRec++ )
        {
            CONNECTION_RECORD cr1 = m_connRecs[iRec];
            CONNECTION_RECORD cr2 = m_connRecs[iRec+1];

            if( cr1.connInfo.connNbr > cr2.connInfo.connNbr )
            {
                // Out of order - swap recs
                m_connRecs[iRec]   = cr2;
                m_connRecs[iRec+1] = cr1;

                bSorted = false;
            }
            else if( cr1.connInfo.connNbr == cr2.connInfo.connNbr )
            {
                // If connection numbers are the same, sort by creation ID
                // which will reflect the order the connection was peformed in
                if( cr1.connInfo.creationNbr > cr2.connInfo.creationNbr )
                {
                    // Out of order - swap recs
                    m_connRecs[iRec]   = cr2;
                    m_connRecs[iRec+1] = cr1;

                    bSorted = false;
                }
            }
        }
    }

    // Preview the report
    try
    {
        ConnReport->ShowReport( true );
    }
    catch( ... )
    {
    }

    // Clean-up
    delete [] m_connRecs;
    delete connIndexList;
}


void  TRptConnForm::ShowReport( TJob* currJob )
{
    ShowReport( currJob, 1, 0x7FFFFFFF );
}


void __fastcall TRptConnForm::ConnDataSetGetValue(const UnicodeString VarName, Variant &Value)
{
    switch( RptGetFieldType( VarName ) )
    {
       case RFT_SECTION:
           RptGetSectionRecField( m_connRecs[ConnDataSet->RecNo].connSectInfo, m_connRecs[ConnDataSet->RecNo].connInfo.pkTorquePt, VarName, Value );
           break;

       case RFT_CONNECTION:
           RptGetConnRecField( m_connRecs[ConnDataSet->RecNo].connInfo, VarName, Value );
           break;

       default:
          Value = "Unknown field " + VarName;
          break;
    }
}


void __fastcall TRptConnForm::ConnReportBeforePrint(TfrxReportComponent *Sender)
{
    // Print connection graph form cached connection
    // Need to print connection graph here
    if( Sender->Name == "ConnGraphPicture" )
    {
        int connCreationNbr = m_connRecs[ConnDataSet->RecNo].connInfo.creationNbr;

        // Tell the main form to render the graph and copy it to the clipboard
        Application->MainForm->Perform( WM_COPY_CONN_TO_CLIP, connCreationNbr, 0 );

        if( Clipboard()->HasFormat( CF_ENHMETAFILE ) )
        {
            // Now copy the clipboard contents into the report graph picture
            TfrxPictureView* graphPicView = (TfrxPictureView*)Sender;

            //graphPicView->Picture->LoadFromClipboardFormat( CF_ENHMETAFILE, Clipboard()->GetAsHandle( CF_METAFILEPICT ), 0 );
            graphPicView->Picture->Metafile->Assign( Clipboard() );
        }
    }

    // Iterate through the shoulder items
    for( int iItem = 0; iItem < NBR_SHLDR_ITEMS; iItem++ )
    {
        if( Sender->Name == sShoulderItems[iItem] )
        {
            // Check if there's no shoulder
            if( !m_connRecs[ConnDataSet->RecNo].connSectInfo.hasShoulder )
                Sender->Visible = false;
            else
                Sender->Visible = true;

            break;
        }
    }
}


void __fastcall TRptConnForm::ConnReportPreview(TObject *Sender)
{
    TfrxReport* frxReport = (TfrxReport*)Sender;
    frxReport->PreviewForm->BorderIcons = TBorderIcons() << biSystemMenu << biMaximize;
}


// Create the progress dialog that will track form creation
void __fastcall TRptConnForm::ConnReportProgressStart(TfrxReport *Sender, TfrxProgressType ProgressType, int Progress)
{
    ProgressDlg->ShowProgressForm( "Generating Report", false );
}


// Update the progress regarding creating the form
void __fastcall TRptConnForm::ConnReportProgress(TfrxReport *Sender, TfrxProgressType ProgressType, int Progress)
{
     // Calculate a estimate progress based on the number of connections
     int iPercentProgress = (int)( ( (float) Progress / (float) m_numConnections ) * 100 ) % 101;

     ProgressDlg->UpdateProgress( iPercentProgress );
}


// Destroy the progress dialog tracks form creation
void __fastcall TRptConnForm::ConnReportProgressStop(TfrxReport *Sender, TfrxProgressType ProgressType, int Progress)
{
    ProgressDlg->CloseProgressForm();
}
