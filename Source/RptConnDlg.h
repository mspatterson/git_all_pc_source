//---------------------------------------------------------------------------
#ifndef RptConnDlgH
#define RptConnDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "frxClass.hpp"
#include "JobManager.h"
#include "ProgressDialog.h"
//---------------------------------------------------------------------------
class TRptConnForm : public TForm
{
__published: // IDE-managed Components
    TfrxUserDataSet *ConnDataSet;
    TfrxReport *ConnReport;
    void __fastcall ConnDataSetGetValue(const UnicodeString VarName, Variant &Value);
    void __fastcall ConnReportBeforePrint(TfrxReportComponent *Sender);
    void __fastcall ConnReportPreview(TObject *Sender);
    void __fastcall ConnReportProgress(TfrxReport *Sender, TfrxProgressType ProgressType,
          int Progress);
    void __fastcall ConnReportProgressStart(TfrxReport *Sender, TfrxProgressType ProgressType,
          int Progress);
    void __fastcall ConnReportProgressStop(TfrxReport *Sender, TfrxProgressType ProgressType,
          int Progress);


private:
    #define NBR_SHLDR_ITEMS     12

    // Define a super-structure that contains all information required for
    // each record of this report.
    typedef struct {
        SECTION_INFO    connSectInfo;
        CONNECTION_INFO connInfo;
    } CONNECTION_RECORD;

    CONNECTION_RECORD* m_connRecs;

public:
    __fastcall TRptConnForm(TComponent* Owner);
    void ShowReport( TJob* currJob, int start, int Finish );
    void ShowReport( TJob* currJob );
    void GetConnectionMaxTorque( TJob* currJob, int connCreationNbr, float& maxTorque, int& turns );
};
//---------------------------------------------------------------------------
extern PACKAGE TRptConnForm *RptConnForm;
//---------------------------------------------------------------------------
#endif
