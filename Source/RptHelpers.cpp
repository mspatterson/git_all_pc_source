#include <vcl.h>
#pragma hdrstop

#include "RptHelpers.h"
#include "Applutils.h"

#pragma package(smart_init)


static const String FieldTypeIdentifiers[NBR_RPT_FIELD_TYPES] = {
    "Job_",
    "Section_",
    "Conn_",
    "Cal_",
};


//
// Public Functions
//

RPT_FIELD_TYPE RptGetFieldType( const String &VarName )
{
    // Check the prefix of the passed string for a match with one
    // of our known prefixes.
    for( int iType = 0; iType < NBR_RPT_FIELD_TYPES; iType++ )
    {
        if( VarName.Pos( FieldTypeIdentifiers[iType] ) == 1 )
            return (RPT_FIELD_TYPE)iType;
    }

    // Fall-through means we don't know this field type
    return RFT_UNKNOWN;
}


void RptGetJobField( TJob* currJob, const String &VarName, Variant &Value )
{
    // Returns the requested value from the job
    if( VarName == "Job_Client" )
        Value = currJob->Client;
    else if( VarName == "Job_Location" )
        Value = currJob->Location;
    else if( VarName == "Job_CreationDate" )
        Value = currJob->DateTimeString();
    else
        Value = "unknown field " + VarName;
}


void RptGetConnRecField( const CONNECTION_INFO& connRec, const String &VarName, Variant &Value )
{
    // Returns the requested value from the connection record.
    if( VarName == "Conn_CreateNbr" )
        Value = connRec.creationNbr;
    else if( VarName == "Conn_ConnNbr" )
        Value = connRec.connNbr;
    else if( VarName == "Conn_SeqNbr" )
        Value = connRec.seqNbr;
    else if( VarName == "Conn_SectNbr" )
        Value = connRec.sectionNbr;
    else if( VarName == "Conn_Length" )
        Value = FloatToStrF( connRec.length, ffFixed, 7, 2 );
    else if( VarName == "Conn_Date" )
        Value = connRec.dateTime.FormatString( "yyyy-mm-dd hh:nn:ss" );   // Note - we expect conn rec time to have been converted to local time
    else if( VarName == "Conn_Status" )
        Value = ConnResultText[connRec.crResult];
    else if( VarName == "Conn_ShldrSet" )
        Value = connRec.shoulderSet;
    else if( VarName == "Conn_ShldrPt" )
    {
        if( connRec.shoulderSet )
            Value = FloatToStrF( connRec.shoulderPt, ffFixed, 7, 3 );
        else
            Value = "No Shoulder";
    }
    else if( VarName == "Conn_ShldrTq" )
        Value = FloatToStrF( connRec.shoulderTq, ffFixed, 7, 3 );
    else if( VarName == "Conn_Comment" )
        Value = connRec.comment;
    else if( VarName == "Conn_PeakTorque" )
        Value = FloatToStrF( connRec.pkTorque, ffFixed, 7, 3 );
    else if( VarName == "Conn_PeakTorqueTurns" )
        Value = FloatToStrF( connRec.pkTorquePt, ffFixed, 7, 3 );
    else if( VarName == "Conn_BattVolts" )
    {
        if( connRec.battVolts == 0.0 )
            Value = "";
        else
            Value = FloatToStrF( connRec.battVolts, ffFixed, 7, 3 );
    }
    else if( VarName == "Conn_CaliRecNbr" )
    {
        if( connRec.calRecNbr == 0 )
            Value = "(no record)";
        else
            Value = IntToStr( connRec.calRecNbr );
    }
    else
        Value = "unknown field " + VarName;
}


void RptGetSectionRecField( const SECTION_INFO& sectRec, float peakTorqueTurns, const String &VarName, Variant &Value )
{
    // Returns the requested value from the section record.
    if( VarName == "Section_SectionNbr" )
        Value = sectRec.sectionNbr;
    else if( VarName == "Section_CasingMfg" )
        Value = sectRec.casingMfgr;
    else if( VarName == "Section_Customer" )
        Value = sectRec.clientRep;
    else if( VarName == "Section_TescoTech" )
        Value = sectRec.tescoTech;
    else if( VarName == "Section_ThreadRep" )
        Value = sectRec.threadRep;
    else if( VarName == "Section_CasingName" )
        Value = sectRec.casingName;
    else if( VarName == "Section_CasingType" )
        Value = sectRec.casingType;
    else if( VarName == "Section_CasingGrade" )
        Value = sectRec.casingGrade;
    else if( VarName == "Section_CasingDia" )
        Value = sectRec.casingDia;
    else if( VarName == "Section_Casingweight" )
        Value = sectRec.casingWeight;
    else if( VarName == "Section_HasShoulder" )
        Value = sectRec.hasShoulder;
    else if( VarName == "Section_ShoulderPostShldrTurns" )
        Value = FloatToStrF( sectRec.shoulderPostTurns, ffFixed, 7, 3 );
    else if( VarName == "Section_ShoulderMaxTorque" )
        Value = FloatToStrF( sectRec.shoulderMaxTorque, ffFixed, 7, 3 );
    else if( VarName == "Section_ShoulderMinTorque" )
        Value = FloatToStrF( sectRec.shoulderMinTorque, ffFixed, 7, 3 );
    else if( VarName == "Section_ShoulderDelta" )
        Value = FloatToStrF( sectRec.shoulderDelta, ffFixed, 7, 3 );
    else if( VarName == "Section_ShoulderPostShldrTurnsRange" )
        Value = FloatToStrF( peakTorqueTurns - sectRec.shoulderPostTurns - sectRec.shoulderDelta, ffFixed, 7, 3 ) + " - " + FloatToStrF( peakTorqueTurns - sectRec.shoulderPostTurns + sectRec.shoulderDelta, ffFixed, 7, 3  );
    // This was removed when the Shouldering alg was changed, should eventually remove it
    else if( VarName == "Section_ShoulderAvgTorqueOffset" )   // TODO - where should the 'avg' values come from?
        Value = 0;
    else if( VarName == "Section_ShoulderAvgStartTurns" )
        Value = 0;
    else if( VarName == "Section_ShoulderAvgEndTurns" )
        Value = 0;
    else if( VarName == "Section_ShoulderAutoShldrEnabled" )
        Value = sectRec.autoShldrEnabled;
    else if( VarName == "Section_PeakTorqueTargetMaxTorgue" )
        Value = FloatToStrF( sectRec.peakTargetMaxTorque, ffFixed, 7, 3 );
    else if( VarName == "Section_PeakTorqueTargetOptTorque" )
        Value = FloatToStrF( sectRec.peakTargetOptTorque, ffFixed, 7, 3 );
    else if( VarName == "Section_PeakTorqueTargetTurns" )
        Value = FloatToStrF( sectRec.peakTargetTurns, ffFixed, 7, 3 );
    else if( VarName == "Section_PeakTorqueTargetDelta" )
        Value = FloatToStrF( sectRec.peakTargetDelta, ffFixed, 7, 3 );
    else if( VarName == "Section_PeakTorqueTargetHold" )
        Value = FloatToStrF( sectRec.peakTargetHold, ffFixed, 7, 1 );
    else if( VarName == "Section_PeakTorqueTargetMinTorque" )
        Value = FloatToStrF( sectRec.peakTargetMinTorque, ffFixed, 7, 3 );
    else if( VarName == "Section_PeakTorqueTargetOvershoot" )
        Value = FloatToStrF( sectRec.peakTargetOvershoot, ffFixed, 7, 0 );
    else if( VarName == "Section_PeakTorqueTargetTurnsRange" )
        Value = FloatToStrF( sectRec.peakTargetTurns - sectRec.peakTargetDelta, ffFixed, 7, 3 ) + " - " + FloatToStrF( sectRec.peakTargetTurns + sectRec.peakTargetDelta, ffFixed, 7, 3 );
    else
        Value = "unknown field " + VarName;
}


void RptGetCalRecField( TJob* currJob, const CALIBRATION_REC& calRec, const String &VarName, Variant &Value )
{
    // Returns the requested value from the calibration record.
    if( VarName == "Cal_creationNbr" )
        Value = calRec.creationNbr;
    else if( VarName == "Cal_housingSN" )
        Value = calRec.housingSN;
    else if( VarName == "Cal_fwRev" )
        Value = calRec.fwRev;
    else if( VarName == "Cal_cfgStraps" )
        Value = calRec.cfgStraps;
    else if( VarName == "Cal_mfgSN" )
        Value = calRec.mfgSN;
    else if( VarName == "Cal_mfgInfo" )
        Value = calRec.mfgInfo;
    else if( VarName == "Cal_mfgDate" )
        Value = calRec.mfgDate;
    else if( VarName == "Cal_torque000Offset" )
        Value = calRec.torque000Offset;
    else if( VarName == "Cal_torque000Span" )
        Value = calRec.torque000Span;
    else if( VarName == "Cal_torque180Offset" )
        Value = calRec.torque180Offset;
    else if( VarName == "Cal_torque180Span" )
        Value = calRec.torque180Span;
    else if( VarName == "Cal_tension000Offset" )
        Value = calRec.tension000Offset;
    else if( VarName == "Cal_tension000Span" )
        Value = calRec.tension000Span;
    else if( VarName == "Cal_tension090Offset" )
        Value = calRec.tension090Offset;
    else if( VarName == "Cal_tension090Span" )
        Value = calRec.tension090Span;
    else if( VarName == "Cal_tension180Offset" )
        Value = calRec.tension180Offset;
    else if( VarName == "Cal_tension180Span" )
        Value = calRec.tension180Span;
    else if( VarName == "Cal_tension270Offset" )
        Value = calRec.tension270Offset;
    else if( VarName == "Cal_tension270Span" )
        Value = calRec.tension270Span;
    else if( VarName == "Cal_xTalkTqTq" )
        Value = calRec.xTalkTqTq;
    else if( VarName == "Cal_xTalkTqTen" )
        Value = calRec.xTalkTqTen;
    else if( VarName == "Cal_xTalkTenTq" )
        Value = calRec.xTalkTenTq;
    else if( VarName == "Cal_xTalkTenTen" )
        Value = calRec.xTalkTenTen;
    else if( VarName == "Cal_gyroOffset" )
        Value = calRec.gyroOffset;
    else if( VarName == "Cal_gyroSpan" )
        Value = calRec.gyroSpan;
    else if( VarName == "Cal_pressureOffset" )
        Value = calRec.pressureOffset;
    else if( VarName == "Cal_pressureSpan" )
        Value = calRec.pressureSpan;
    else if( VarName == "Cal_battCapacity" )
        Value = calRec.battCapacity;
    else if( VarName == "Cal_battType" )
        Value = calRec.battType;
    else if( VarName == "Cal_lastCalDate" )
        Value = calRec.lastCalDate;
    else if( VarName == "Cal_connList" )
        Value = currJob->GetCalRecConnList( calRec.creationNbr );
    else
        Value = "unknown field " + VarName;
}


bool RptSetMemoTextToUnits( TJob* currJob, TfrxReport* aRpt, String memoName, String prefaceText, MEASURE_TYPE mt )
{
    if( currJob == NULL )
        return false;

    if( aRpt == NULL )
        return false;

    TfrxMemoView* unitsMemo = dynamic_cast <TfrxMemoView *>( aRpt->FindObject( memoName ) );

    if( unitsMemo == NULL )
        return false;

    // Clear the memo first. If the preface text is not blank, add a blank
    // between that text and the units text.
    unitsMemo->Lines->Clear();

    String measureText = "(" + GetUnitsText( currJob->Units, mt ) + ")";

    if( prefaceText.Length() == 0 )
        unitsMemo->Memo->Add( measureText );
    else
        unitsMemo->Memo->Add( prefaceText + " " + measureText );

    return true;
}


bool RptSetMemoText( TJob* currJob, TfrxReport* aRpt, String memoName, String text )
{
    if( currJob == NULL )
        return false;

    if( aRpt == NULL )
        return false;

    TfrxMemoView* unitsMemo = dynamic_cast <TfrxMemoView *>( aRpt->FindObject( memoName ) );

    if( unitsMemo == NULL )
        return false;

    // Clear the memo, then add the text
    unitsMemo->Lines->Clear();
    unitsMemo->Memo->Add( text );

    return true;
}


void SetGraphAxisMinMax( TChartAxis* anAxis, float newMin, float newMax )
{
    // Sets the passed graph's axis min / max values in the correct order to
    // prevent exceptions if newMin > old max.

    // Sanity check: make sure new values being passed are correctly ordered
    if( newMin > newMax )
    {
        float temp = newMin;
        newMin = newMax;
        newMax = temp;
    }

    // If the new min is greater than the current max, need to set max first.
    if( newMin > anAxis->Maximum )
    {
        anAxis->Maximum = newMax;
        anAxis->Minimum = newMin;
    }
    else
    {
        anAxis->Minimum = newMin;
        anAxis->Maximum = newMax;
    }
}

