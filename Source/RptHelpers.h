#ifndef RptHelpersH
#define RptHelpersH

#include <VCLTee.Chart.hpp>

#include "frxClass.hpp"

#include "JobManager.h"

    typedef enum
    {
       RFT_UNKNOWN = -1,
       RFT_JOB = 0,
       RFT_SECTION,
       RFT_CONNECTION,
       RFT_CAL,
       NBR_RPT_FIELD_TYPES
    } RPT_FIELD_TYPE;

    RPT_FIELD_TYPE RptGetFieldType( const String &VarName );

    void RptGetJobField( TJob* currJob, const String &VarName, Variant &Value );
        // Returns the requested value from the job

    void RptGetConnRecField( const CONNECTION_INFO& connRec, const String &VarName, Variant &Value );
        // Returns the requested value from the connection record.

    void RptGetSectionRecField( const SECTION_INFO& sectRec, float peakTorqueTurns, const String &VarName, Variant &Value );
        // Returns the requested value from the section record.

    void RptGetCalRecField( TJob* currJob, const CALIBRATION_REC& calRec, const String &VarName, Variant &Value );
        // Returns the requested value from the calibration record.

    bool RptSetMemoTextToUnits( TJob* currJob, TfrxReport* aRpt, String memoName, String prefaceText, MEASURE_TYPE mt );
        // Sets the value of a TfrxMemoView to the units of measure text passed. Returns
        // false if NULL passed for any of the pointers or if the named memo cannot
        // be found.

    bool RptSetMemoText( TJob* currJob, TfrxReport* aRpt, String memoName, String text );
        // Sets the value of a TfrxMemoView to the text passed. Returns false if NULL
        // passed for any of the pointers or if the named memo cannot be found.

    void SetGraphAxisMinMax( TChartAxis* anAxis, float newMin, float newMax );
        // Sets the passed graph's axis min / max values in the correct order to
        // prevent exceptions if newMin > old max.

#endif
