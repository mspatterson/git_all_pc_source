#include <vcl.h>
#pragma hdrstop

#include "RptJobHardwareDlg.h"
#include "Applutils.h"
#include "RptHelpers.h"

#pragma package(smart_init)
#pragma link "frxClass"
#pragma resource "*.dfm"


TJobHardwareReportForm *JobHardwareReportForm;


__fastcall TJobHardwareReportForm::TJobHardwareReportForm(TComponent* Owner) : TForm(Owner)
{
}


void TJobHardwareReportForm::ShowReport( TJob* currJob )
{
    // Validate current job.
    if( currJob == NULL )
        return;

    // Remember the job
    m_currJob = currJob;

    // Set number of records to read
    JobHardwareDataSet->RangeEndCount = 1;
    JobHardwareDataSet->RangeEnd = reCount;

    // Initialize job name memos - these never change
    RptSetMemoText( currJob, JobHardwareReport, "ClientNameMemo", currJob->Client );
    RptSetMemoText( currJob, JobHardwareReport, "JobLocnMemo",    currJob->Location );
    RptSetMemoText( currJob, JobHardwareReport, "JobDateMemo",    currJob->DateTimeString( currJob->StartTime ) );

    // preview the report
    JobHardwareReport->ShowReport( true );
}


void __fastcall TJobHardwareReportForm::JobHardwareDataSetGetValue(const UnicodeString VarName, Variant &Value)
{
    // To get device information
    RptGetDeviceField( m_currJob, VarName, Value );
}


void __fastcall TJobHardwareReportForm::JobHardwareReportPreview(TObject *Sender)
{
    TfrxReport* frxReport = (TfrxReport*)Sender;
    frxReport->PreviewForm->BorderIcons = TBorderIcons() << biSystemMenu << biMaximize;
}

