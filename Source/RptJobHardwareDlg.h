//---------------------------------------------------------------------------

#ifndef RptJobHardwareDlgH
#define RptJobHardwareDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "frxClass.hpp"
#include "JobManager.h"
//---------------------------------------------------------------------------
class TJobHardwareReportForm : public TForm
{
__published:	// IDE-managed Components
    TfrxReport *JobHardwareReport;
    TfrxUserDataSet *JobHardwareDataSet;
    void __fastcall JobHardwareReportPreview(TObject *Sender);
    void __fastcall JobHardwareDataSetGetValue(const UnicodeString VarName, Variant &Value);

private:	// User declarations
    TJob* m_currJob;

public:		// User declarations
    __fastcall TJobHardwareReportForm(TComponent* Owner);
    void ShowReport( TJob* currJob );
};
//---------------------------------------------------------------------------
extern PACKAGE TJobHardwareReportForm *JobHardwareReportForm;
//---------------------------------------------------------------------------
#endif
