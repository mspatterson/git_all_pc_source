#include <vcl.h>
#pragma hdrstop

#include "RptSecSummaryDlg.h"
#include "Applutils.h"
#include "RptHelpers.h"

#pragma package(smart_init)
#pragma link "frxClass"
#pragma resource "*.dfm"


TRptSecSummaryForm *RptSecSummaryForm;


__fastcall TRptSecSummaryForm::TRptSecSummaryForm(TComponent* Owner) : TForm(Owner)
{
}


void TRptSecSummaryForm::ShowReport( TJob* currJob, int start, int finish )
{
    // Validate current job.
    if( currJob == NULL )
        return;

    // Validate start and end section. Section numbers are 1-based.
    if( ( start < 1 ) || ( finish < 1 ) )
        return;

    // If the last section requested is too large, just set it to the
    // last section in the job.
    if( finish > currJob->NbrSections )
        finish = currJob->NbrSections;

    if( finish < start )
        return;

    // Set number of records to read
    SecSummaryDataSet->RangeEndCount = finish - start + 1;
    SecSummaryDataSet->RangeEnd      = reCount;

    // Initialize job name memos - these never change
    RptSetMemoText( currJob, SecSummaryReport, "ClientNameMemo", currJob->Client );
    RptSetMemoText( currJob, SecSummaryReport, "JobLocnMemo",    currJob->Location );
    RptSetMemoText( currJob, SecSummaryReport, "JobDateMemo",    currJob->DateTimeString() );

    // Initialize the report units of measure memos
    RptSetMemoTextToUnits( currJob, SecSummaryReport, "HdrMemoDiaUnits", "", MT_DIST2_SHORT  );
    RptSetMemoTextToUnits( currJob, SecSummaryReport, "HdrMemoWtUnits",  "", MT_WEIGHT_SHORT );
    RptSetMemoTextToUnits( currJob, SecSummaryReport, "HdrMemoTqUnits",  "", MT_TORQUE_SHORT );

    // Build the list of section recs. We are always guaranteed to be showing
    // at least one section
    int nbrSectRecs = finish - start + 1;

    m_sectRecs = new SECTION_INFO[nbrSectRecs];

    // Section records are always stored sequentially in job manager, starting
    // with section 1. Therefore, a section's index is its section nbr - 1.
    for( int iSect = 0; iSect < nbrSectRecs; iSect++ )
        currJob->GetSectionRec( iSect + start - 1, m_sectRecs[iSect] );

    // Preview the report
    try
    {
        SecSummaryReport->ShowReport( true );
    }
    catch( ... )
    {
    }

    // Clean-up
    delete [] m_sectRecs;
}


void TRptSecSummaryForm::ShowReport( TJob* currJob )
{
    // To show all sections on the report
    ShowReport( currJob, 1, currJob->NbrSections );
}


void __fastcall TRptSecSummaryForm::SecSummaryDataSetGetValue(const UnicodeString VarName, Variant &Value)
{
     RptGetSectionRecField( m_sectRecs[SecSummaryDataSet->RecNo], 0, VarName, Value );
}


void __fastcall TRptSecSummaryForm::SecSummaryReportPreview(TObject *Sender)
{
    TfrxReport* frxReport = (TfrxReport*)Sender;
    frxReport->PreviewForm->BorderIcons = TBorderIcons() << biSystemMenu << biMaximize;
}

