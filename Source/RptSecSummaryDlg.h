//---------------------------------------------------------------------------
#ifndef RptSecSummaryDlgH
#define RptSecSummaryDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "frxClass.hpp"
#include "JobManager.h"
//---------------------------------------------------------------------------
class TRptSecSummaryForm : public TForm
{
__published:    // IDE-managed Components
    TfrxUserDataSet *SecSummaryDataSet;
    TfrxReport *SecSummaryReport;
    void __fastcall SecSummaryDataSetGetValue(const UnicodeString VarName, Variant &Value);
    void __fastcall SecSummaryReportPreview(TObject *Sender);

private:
     SECTION_INFO* m_sectRecs;

public:
    __fastcall TRptSecSummaryForm(TComponent* Owner);
    void ShowReport( TJob* currJob, int start, int finish );
    void ShowReport( TJob* currJob );
};
//---------------------------------------------------------------------------
extern PACKAGE TRptSecSummaryForm *RptSecSummaryForm;
//---------------------------------------------------------------------------
#endif
