#include <vcl.h>
#pragma hdrstop

#include "RptSectionsDlg.h"
#include "Applutils.h"
#include "RptHelpers.h"

#pragma package(smart_init)
#pragma link "frxClass"
#pragma link "frxChBox"
#pragma resource "*.dfm"


TRptSectionsForm *RptSectionsForm;


__fastcall TRptSectionsForm::TRptSectionsForm(TComponent* Owner) : TForm(Owner)
{
}


void TRptSectionsForm::ShowReport( TJob* currJob, int start, int finish )
{
    // Validate current job.
    if( currJob == NULL )
        return;

    // Validate start and end section. Section numbers are 1-based.
    if( ( start < 1 ) || ( finish < 1 ) )
        return;

    // If the last section requested is too large, just set it to the
    // last section in the job.
    if( finish > currJob->NbrSections )
        finish = currJob->NbrSections;

    if( finish < start )
        return;

    // Save the job
    m_currJob = currJob;

    // Set number of records to read
    SectionsDataSet->RangeEndCount = finish - start + 1;
    SectionsDataSet->RangeEnd      = reCount;

    // Initialize job name memos - these never change
    RptSetMemoText( currJob, SectionsReport, "ClientNameMemo1", currJob->Client );
    RptSetMemoText( currJob, SectionsReport, "ClientNameMemo2", currJob->Client );
    RptSetMemoText( currJob, SectionsReport, "JobLocnMemo1",    currJob->Location );
    RptSetMemoText( currJob, SectionsReport, "JobLocnMemo2",    currJob->Location );
    RptSetMemoText( currJob, SectionsReport, "JobDateMemo",     currJob->DateTimeString() );

    // Initialize the report units of measure memos
    RptSetMemoTextToUnits( currJob, SectionsReport, "DiameterMemo",       "Diameter",           MT_DIST2_SHORT  );
    RptSetMemoTextToUnits( currJob, SectionsReport, "WeightMemo",         "Weight",             MT_WEIGHT_SHORT );
    RptSetMemoTextToUnits( currJob, SectionsReport, "ShoulderTargetMemo", "Shoulder Target",    MT_TORQUE_SHORT );
    RptSetMemoTextToUnits( currJob, SectionsReport, "TorqueTargetMemo",   "Peak Torque Target", MT_TORQUE_SHORT );

    // Build the list of section recs. We are always guaranteed to be showing
    // at least one section
    int nbrSectRecs = finish - start + 1;

    m_sectRecs = new SECTION_INFO[nbrSectRecs];

    // Section records are always stored sequentially in job manager, starting
    // with section 1. Therefore, a section's index is its section nbr - 1.
    for( int iSect = 0; iSect < nbrSectRecs; iSect++ )
        currJob->GetSectionRec( iSect + start - 1, m_sectRecs[iSect] );

    // Preview the report
    try
    {
        SectionsReport->ShowReport( true );
    }
    catch( ... )
    {
    }

    // Clean-up
    delete [] m_sectRecs;
}


void TRptSectionsForm::ShowReport( TJob* currJob )
{
    //  To show all tasks on the report
    ShowReport( currJob, 1, currJob->NbrSections );
}


void __fastcall TRptSectionsForm::SectionsDataSetGetValue(const UnicodeString VarName, Variant &Value)
{
     // This report has a custom field - check for that first
     if( VarName == "Custom_ConnList" )
     {
        // Get all connections for the section
        int iSect = m_sectRecs[SectionsDataSet->RecNo].sectionNbr;

        UnicodeString connNbrs;

        for( int iConn = 0; iConn < m_currJob->NbrConnRecs; iConn++ )
        {
            CONNECTION_INFO connectionInfo;
            m_currJob->GetConnection( iConn, connectionInfo );

            if( connectionInfo.sectionNbr == iSect )
            {
                // Save all connections, but only for passed connections
                if( ConnGood( &connectionInfo ) )
                {
                    if( connNbrs.Length() == 0 )
                        connNbrs = IntToStr( connectionInfo.connNbr );
                    else
                        connNbrs = connNbrs + ", " + IntToStr( connectionInfo.connNbr );
                }
            }
        }

        // Assign connections to return var
        if( connNbrs.Length() > 0 )
            Value = connNbrs;
        else
            Value = "(No Connections)";
     }
     else
     {
         RptGetSectionRecField( m_sectRecs[SectionsDataSet->RecNo], 0, VarName, Value );
     }
}


void __fastcall TRptSectionsForm::SectionsReportPreview(TObject *Sender)
{
    TfrxReport* frxReport = (TfrxReport*)Sender;
    frxReport->PreviewForm->BorderIcons = TBorderIcons() << biSystemMenu << biMaximize;
}


