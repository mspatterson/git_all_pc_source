//---------------------------------------------------------------------------
#ifndef RptSectionsDlgH
#define RptSectionsDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "frxClass.hpp"
#include "JobManager.h"
#include "frxChBox.hpp"
//---------------------------------------------------------------------------
class TRptSectionsForm : public TForm
{
__published:    // IDE-managed Components
    TfrxReport *SectionsReport;
    TfrxUserDataSet *SectionsDataSet;
    void __fastcall SectionsDataSetGetValue(const UnicodeString VarName, Variant &Value);
    void __fastcall SectionsReportPreview(TObject *Sender);

private:
     TJob*         m_currJob;
     SECTION_INFO* m_sectRecs;

public:
    __fastcall TRptSectionsForm(TComponent* Owner);
    void ShowReport( TJob* currJob, int start, int finish );
    void ShowReport( TJob* currJob );
};
//---------------------------------------------------------------------------
extern PACKAGE TRptSectionsForm *RptSectionsForm;
//---------------------------------------------------------------------------
#endif
