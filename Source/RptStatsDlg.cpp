#include <vcl.h>
#pragma hdrstop

#include <Math.hpp>

#include "RptStatsDlg.h"
#include "RptHelpers.h"
#include "RptStatsRptDlg.h"
#include "UnitsOfMeasure.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TRptStatsForm *RptStatsForm;


static const int NbrTorqueBins = 11;
static const int NbrTurnBins   = 11;

static const FinalTorqueBinStart[NBR_UNITS_OF_MEASURE] = { 10000, 8000 };
static const SlderTorqueBinStart[NBR_UNITS_OF_MEASURE] = { 0,     0 };
static const DeltaTorqueBinStart[NBR_UNITS_OF_MEASURE] = { 10000, 8000 };

static float RoundDown( float aValue, float multiple )
{
    float result = 0.0;

    while( result + multiple < aValue )
        result += multiple;

    return result;
}

static float RoundUp( float aValue, float multiple )
{
    float result = RoundDown( aValue, multiple );

    return result + multiple;
}


//
// Class Implementation
//

__fastcall TRptStatsForm::TRptStatsForm(TComponent* Owner) : TForm(Owner)
{
}


void TRptStatsForm::ShowPlots( TJob* currJob )
{
    // validate current job.
    if( currJob == NULL )
        return;

    // validate connection records
    if( currJob->NbrConnRecs == 0 )
        return;

    // Clean up previous data
    FinalTorqueSeries->Clear();
    ShldrTorqueSeries->Clear();
    DeltaTorqueSeries->Clear();
    FinalTurnsSeries->Clear();
    ShldrTurnsSeries->Clear();
    DeltaTurnsSeries->Clear();

    // Remember job and set dialog caption
    m_currJob = currJob;

    Caption = "Summary Report for " + m_currJob->Client + ", " + m_currJob->Location;

    // Setup graph captions
    UnicodeString tqUnitsText = " (" + GetUnitsText( m_currJob->Units, MT_TORQUE_SHORT ) + ")";
    UnicodeString tqVertTitle = "Torque" + tqUnitsText;

    FinTqeVsFinturnsChart->LeftAxis->Title->Caption     = tqVertTitle;
    ShldrTqeVsShldrTurnsChart->LeftAxis->Title->Caption = tqVertTitle;
    DeltaTqeVsDeltaTrunsChart->LeftAxis->Title->Caption = tqVertTitle;

    FinalTorqueChart->BottomAxis->Title->Caption = "Final Torque"    + tqUnitsText;
    ShldrTorqueChart->BottomAxis->Title->Caption = "Shoulder Torque" + tqUnitsText;
    DeltaTorqueChart->BottomAxis->Title->Caption = "Delta Torque"    + tqUnitsText;

    // Initialize stats variables
    float finTorqueTotal   = 0.0;
    float finTurnTotal     = 0.0;
    float shldrTorqueTotal = 0.0;
    float shldrTurnTotal   = 0.0;

    int nbrCount = 0;
    int nbrFail  = 0;

    BIN_ITEM finTorqueBins[NbrTorqueBins];
    BIN_ITEM shldrTorqueBins[NbrTorqueBins];
    BIN_ITEM deltaTorqueBins[NbrTorqueBins];
    BIN_ITEM finTurnsBins[NbrTurnBins];
    BIN_ITEM shldrTurnsBins[NbrTurnBins];
    BIN_ITEM deltaTurnsBins[NbrTurnBins];

    // To set the ranage of bins and number of count
    InitBinArray( finTorqueBins,   NbrTorqueBins, FinalTorqueBinStart[m_currJob->Units], 1000 );
    InitBinArray( shldrTorqueBins, NbrTorqueBins, SlderTorqueBinStart[m_currJob->Units],  200 );
    InitBinArray( deltaTorqueBins, NbrTorqueBins, SlderTorqueBinStart[m_currJob->Units], 1000 );
    InitBinArray( finTurnsBins,    NbrTurnBins,   7.0,   0.5 );
    InitBinArray( shldrTurnsBins,  NbrTurnBins,   5.0,   0.5 );
    InitBinArray( deltaTurnsBins,  NbrTurnBins,   1.0,   0.5 );

    InitValueRange( m_finalTurnsRange );
    InitValueRange( m_shldrTurnsRange );
    InitValueRange( m_deltaTurnsRange );

    InitValueRange( m_finalTqRange );
    InitValueRange( m_shldrTqRange );
    InitValueRange( m_deltaTqRange );

    // Loop through Job list to retrieve all max torque and max turns for each
    // Item on connection records
    for( int iConn = 0; iConn < currJob->NbrConnRecs; iConn++ )
    {
        CONNECTION_INFO currConn;
        currJob->GetConnection( iConn, currConn );

        // retrieve Torque and Turns form passed connections
        if( ( currConn.crResult == CR_PASSED ) || ( currConn.crResult == CR_FORCE_PASSED ) )
        {
            // Update number of count of Bins
            UpdateBinArray( finTorqueBins,   NbrTorqueBins, currConn.pkTorque   );
            UpdateBinArray( shldrTorqueBins, NbrTorqueBins, currConn.shoulderTq );
            UpdateBinArray( deltaTorqueBins, NbrTorqueBins, ( currConn.shoulderTq - currConn.shoulderTq ) );
            UpdateBinArray( finTurnsBins,    NbrTurnBins,   currConn.pkTorquePt );
            UpdateBinArray( shldrTurnsBins,  NbrTurnBins,   currConn.shoulderPt );
            UpdateBinArray( deltaTurnsBins,  NbrTurnBins,   ( currConn.pkTorquePt - currConn.shoulderPt ) );

            // Print Summary plots
            PlotSummary( currConn, currConn.pkTorquePt, currConn.shoulderTq );

            // Sum of final torque and final turns
            finTorqueTotal += currConn.pkTorque;
            finTurnTotal   += currConn.pkTorquePt;
        
            // Sum of shoulder touque and shoulder turns
            shldrTorqueTotal += currConn.shoulderTq;
            shldrTurnTotal   += currConn.shoulderPt;
        
            // Total number of passed count
            ++nbrCount;
        }
        else if( ( currConn.crResult == CR_FAILED ) || ( currConn.crResult == CR_FORCE_FAILED ) )
        {
            // number of failed connections
            ++nbrFail;
        }
    }

    // Print Torque plots
    PlotTorque( finTorqueBins, shldrTorqueBins, deltaTorqueBins, NbrTorqueBins );

    // Print Turns plots
    PlotTurns( finTurnsBins, shldrTurnsBins, deltaTurnsBins, NbrTurnBins );

    // Adjust the graph x-axis min / max values
    SetGraphAxisMinMax( FinTqeVsFinturnsChart->BottomAxis,     Floor( m_finalTurnsRange.min ), Ceil( m_finalTurnsRange.max ) );
    SetGraphAxisMinMax( ShldrTqeVsShldrTurnsChart->BottomAxis, Floor( m_shldrTurnsRange.min ), Ceil( m_shldrTurnsRange.max ) );
    SetGraphAxisMinMax( DeltaTqeVsDeltaTrunsChart->BottomAxis, Floor( m_deltaTurnsRange.min ), Ceil( m_deltaTurnsRange.max ) );

    // Adjust graph y-axis min / max values
    SetGraphAxisMinMax( FinTqeVsFinturnsChart->LeftAxis,     RoundDown( m_finalTqRange.min, 1000 ), RoundUp( m_finalTqRange.max, 1000 ) );
    SetGraphAxisMinMax( ShldrTqeVsShldrTurnsChart->LeftAxis, RoundDown( m_shldrTqRange.min, 1000 ), RoundUp( m_shldrTqRange.max, 1000 ) );
    SetGraphAxisMinMax( DeltaTqeVsDeltaTrunsChart->LeftAxis, RoundDown( m_deltaTqRange.min, 1000 ), RoundUp( m_deltaTqRange.max, 1000 ) );

    FinalTorqueChart->BottomAxis->Increment = 1000;
    ShldrTorqueChart->BottomAxis->Increment = 200;
    DeltaTorqueChart->BottomAxis->Increment = 1000;

    // Averagers
    float finTqeAvg          = CalcAvg( finTorqueTotal,   nbrCount );
    float finTurnsAvg        = CalcAvg( finTurnTotal,     nbrCount );
    float shldrTqeAvg        = CalcAvg( shldrTorqueTotal, nbrCount );
    float shldrTurnsAvg      = CalcAvg( shldrTurnTotal,   nbrCount );
    float deltaShldrTqeAvg   = finTqeAvg   - shldrTqeAvg;
    float deltaShldrTurnsAvg = finTurnsAvg - shldrTurnsAvg;

    // Show the report
    SumChartPgCtrl->ActivePageIndex = 0;

    ShowModal();
}


void TRptStatsForm::InitBinArray( BIN_ITEM binArray[], int nbrArrayItems, float firstBinStart, float binStepValue )
{
    // Initializes an array of BIN_ITEM structures
    for( int i = 0; i < nbrArrayItems; i++ )
    {
        binArray[i].rangeStart = firstBinStart;
        binArray[i].rangeEnd   = firstBinStart + binStepValue;
        binArray[i].itemCount = 0;

        firstBinStart +=  binStepValue;
    }
}


void TRptStatsForm::UpdateBinArray( BIN_ITEM binArray[], int nbrArrayItems, float currValue )
{
    // Updates an array of BIN_ITEM structures. Check for the value
    // being below or above the bin range first.
    if( currValue < binArray[0].rangeStart )
    {
        binArray[0].itemCount += 1;
        return;
    }

    if( currValue >= binArray[nbrArrayItems-1].rangeStart )
    {
        binArray[nbrArrayItems-1].itemCount += 1;
        return;
    }

    // Value falls in the range of bins
    for( int i = 0; i < nbrArrayItems; i++ )
    {
        if( ( currValue >= binArray[i].rangeStart ) && ( currValue < binArray[i].rangeEnd ) )
        {
            binArray[i].itemCount += 1;
            break;
        }
    }
}


float TRptStatsForm::CalcAvg( float total, int count )
{
    // Calculate average - watch for divide by zero
    if( count > 0 )
        return ( total / count );
    else
        return 0;
}


void TRptStatsForm::PlotSummary( const CONNECTION_INFO& currConn, float turns, float torque )
{
    // Print summary plots
    FinTqeVsFinTurnsSeries->AddXY( turns, torque );
    shldrTqeVsShldrTurnsSeries->AddXY( currConn.shoulderPt, currConn.shoulderTq );
    DeltaTqeVsDeltaTurnsSeries->AddXY( turns - currConn.shoulderPt, torque - currConn.shoulderTq );

    // Update turns min/max ranges
    UpdateValueRange( m_finalTurnsRange, turns );
    UpdateValueRange( m_shldrTurnsRange, currConn.shoulderPt );
    UpdateValueRange( m_deltaTurnsRange, turns - currConn.shoulderPt );

    // Update torque min/max values
    UpdateValueRange( m_finalTqRange, torque );
    UpdateValueRange( m_shldrTqRange, currConn.shoulderTq );
    UpdateValueRange( m_deltaTqRange, torque - currConn.shoulderTq );
}


void TRptStatsForm::PlotTorque( BIN_ITEM finTqeBinArray[], BIN_ITEM shldrTqeBinArray[], BIN_ITEM deltaTqeBinArray[], int nbrArrayItems )
{
    // Add value to torque series
    for( int i = 0; i < nbrArrayItems; i++ )
    {
         FinalTorqueSeries->AddXY( ( ( finTqeBinArray[i].rangeEnd + finTqeBinArray[i].rangeStart ) / 2 ),
                                       finTqeBinArray[i].itemCount );

         ShldrTorqueSeries->AddXY( ( ( shldrTqeBinArray[i].rangeEnd + shldrTqeBinArray[i].rangeStart ) / 2 ),
                                       shldrTqeBinArray[i].itemCount );

         DeltaTorqueSeries->AddXY( ( ( deltaTqeBinArray[i].rangeEnd + deltaTqeBinArray[i].rangeStart ) / 2 ),
                                       deltaTqeBinArray[i].itemCount );
    }
}


void TRptStatsForm::PlotTurns( BIN_ITEM finTurnsBinArray[], BIN_ITEM shldrTurnsBinArray[], BIN_ITEM deltaTurnsBinArray[], int nbrArrayItems )
{
    // Add value to turns series
    for( int i = 0; i < nbrArrayItems; i++ )
    {
        FinalTurnsSeries->AddXY( ( ( finTurnsBinArray[i].rangeEnd + finTurnsBinArray[i].rangeStart ) / 2 ),
                                     finTurnsBinArray[i].itemCount );

        ShldrTurnsSeries->AddXY( ( ( shldrTurnsBinArray[i].rangeEnd + shldrTurnsBinArray[i].rangeStart ) / 2 ),
                                     shldrTurnsBinArray[i].itemCount );

        DeltaTurnsSeries->AddXY( ( ( deltaTurnsBinArray[i].rangeEnd + deltaTurnsBinArray[i].rangeStart ) / 2 ),
                                     deltaTurnsBinArray[i].itemCount );
    }
}


void __fastcall TRptStatsForm::PrintBtnClick(TObject *Sender)
{
    RptStatsRptForm->ShowReport( m_currJob, this );
}


void __fastcall TRptStatsForm::WMCopyGraphToClip( TMessage &Message )
{
    // Message sent by child forms to force the display of a connection
    // and then copying of the graph to the clipboard as a WMF image.

    // First, clear the clipboard
    EmptyClipboard();

    // Now select the chart to copy
    TChart* aChart = NULL;

    switch( Message.WParam )
    {
        case 0:  aChart = FinTqeVsFinturnsChart;      break;
        case 1:  aChart = ShldrTqeVsShldrTurnsChart;  break;
        case 2:  aChart = DeltaTqeVsDeltaTrunsChart;  break;
        case 3:  aChart = FinalTorqueChart;           break;
        case 4:  aChart = ShldrTorqueChart;           break;
        case 5:  aChart = DeltaTorqueChart;           break;
        case 6:  aChart = FinalTurnsChart;            break;
        case 7:  aChart = ShldrTurnsChart;            break;
        case 8:  aChart = DeltaTurnsChart;            break;
    }

    if( aChart != NULL )
    {
        aChart->BufferedDisplay = false;
        aChart->CopyToClipboardMetafile( true );
        aChart->BufferedDisplay = true;
    }
}


void TRptStatsForm::InitValueRange( VALUE_RANGE& aRange )
{
    aRange.min = 9999999;
    aRange.max = -9999999;
}


void TRptStatsForm::UpdateValueRange( VALUE_RANGE& aRange, float aValue )
{
    if( aValue < aRange.min )
        aRange.min = aValue;
    else if( aValue > aRange.max )
        aRange.max = aValue;
}

