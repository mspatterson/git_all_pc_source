object RptStatsForm: TRptStatsForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Summary Charts'
  ClientHeight = 745
  ClientWidth = 762
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poPrintToFit
  DesignSize = (
    762
    745)
  PixelsPerInch = 96
  TextHeight = 13
  object SumChartPgCtrl: TPageControl
    Left = 8
    Top = 9
    Width = 746
    Height = 696
    ActivePage = ByTurnsSheet
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
    object SummarySheet: TTabSheet
      Caption = 'Summary'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        738
        668)
      object FinTqeVsFinturnsChart: TChart
        Left = 12
        Top = 9
        Width = 718
        Height = 210
        LeftWall.Visible = False
        Legend.Visible = False
        Title.Text.Strings = (
          'Final Torque vs Final Turns')
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.ExactDateTime = False
        BottomAxis.Increment = 0.500000000000000000
        BottomAxis.LabelsSeparation = 0
        BottomAxis.LabelStyle = talValue
        BottomAxis.Maximum = 10.000000000000000000
        BottomAxis.Title.Caption = 'Final Turns'
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.LabelStyle = talValue
        LeftAxis.Maximum = 13800.000000000000000000
        LeftAxis.Minimum = 12800.000000000000000000
        LeftAxis.Title.Caption = '(Torque ft-lb)'
        View3D = False
        TabOrder = 0
        Anchors = [akLeft, akTop, akRight]
        PrintMargins = (
          15
          32
          15
          32)
        ColorPaletteIndex = 13
        object FinTqeVsFinTurnsSeries: TPointSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          Title = 'FinalTqeVsFinalTurns'
          ClickableLine = False
          Pointer.Brush.Gradient.EndColor = 10708548
          Pointer.Gradient.EndColor = 10708548
          Pointer.InflateMargins = True
          Pointer.Style = psDiagCross
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {0000000000}
        end
      end
      object ShldrTqeVsShldrTurnsChart: TChart
        Left = 12
        Top = 225
        Width = 718
        Height = 210
        Legend.Visible = False
        Title.Text.Strings = (
          'Shoulder Torque vs Shoulder Turns')
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.ExactDateTime = False
        BottomAxis.Increment = 0.500000000000000000
        BottomAxis.LabelStyle = talValue
        BottomAxis.Maximum = 10.000000000000000000
        BottomAxis.Title.Caption = 'Shoulder Turns'
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Maximum = 4000.000000000000000000
        LeftAxis.Minimum = 1800.000000000000000000
        LeftAxis.Title.Caption = '( Torque ft-lb)'
        View3D = False
        TabOrder = 1
        Anchors = [akLeft, akTop, akRight]
        ColorPaletteIndex = 13
        object shldrTqeVsShldrTurnsSeries: TPointSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          ClickableLine = False
          Pointer.Brush.Gradient.EndColor = 10708548
          Pointer.Gradient.EndColor = 10708548
          Pointer.InflateMargins = True
          Pointer.Style = psDiagCross
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {0000000000}
        end
      end
      object DeltaTqeVsDeltaTrunsChart: TChart
        Left = 12
        Top = 441
        Width = 718
        Height = 210
        Legend.Visible = False
        Title.Text.Strings = (
          'Delta Torque vs Delta Turns')
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.ExactDateTime = False
        BottomAxis.Increment = 0.500000000000000000
        BottomAxis.Maximum = 2.500000000000000000
        BottomAxis.MaximumOffset = 1
        BottomAxis.Minimum = 1.500000000000000000
        BottomAxis.Title.Caption = 'Delta Turns'
        LeftAxis.Automatic = False
        LeftAxis.AutomaticMaximum = False
        LeftAxis.AutomaticMinimum = False
        LeftAxis.Maximum = 11500.000000000000000000
        LeftAxis.Minimum = 8000.000000000000000000
        LeftAxis.Title.Caption = '( Torque ft-lb )'
        View3D = False
        TabOrder = 2
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColorPaletteIndex = 13
        object DeltaTqeVsDeltaTurnsSeries: TPointSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          ClickableLine = False
          Pointer.Brush.Gradient.EndColor = 10708548
          Pointer.Gradient.EndColor = 10708548
          Pointer.InflateMargins = True
          Pointer.Style = psDiagCross
          Pointer.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
          Data = {0000000000}
        end
      end
    end
    object ByTorqueSheet: TTabSheet
      Caption = 'By Torque'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        738
        668)
      object FinalTorqueChart: TChart
        Left = 12
        Top = 9
        Width = 718
        Height = 210
        Legend.Visible = False
        Title.Text.Strings = (
          '')
        BottomAxis.ExactDateTime = False
        BottomAxis.Grid.Visible = False
        BottomAxis.Increment = 1000.000000000000000000
        BottomAxis.LabelsSeparation = 0
        BottomAxis.LabelStyle = talValue
        BottomAxis.MinorTicks.Visible = False
        BottomAxis.TickOnLabelsOnly = False
        BottomAxis.Title.Caption = 'Final Torque  (ft-lb)'
        LeftAxis.LabelStyle = talValue
        View3D = False
        TabOrder = 0
        Anchors = [akLeft, akTop, akRight]
        ColorPaletteIndex = 13
        object FinalTorqueSeries: TBarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = True
          XValues.Name = 'X'
          XValues.Order = loNone
          YValues.Name = 'Bar'
          YValues.Order = loNone
          Data = {0000000000}
        end
      end
      object ShldrTorqueChart: TChart
        Left = 17
        Top = 225
        Width = 718
        Height = 210
        LeftWall.Visible = False
        Legend.Visible = False
        Title.Text.Strings = (
          '')
        BottomAxis.ExactDateTime = False
        BottomAxis.Increment = 1000.000000000000000000
        BottomAxis.LabelsSeparation = 0
        BottomAxis.MinorTicks.Visible = False
        BottomAxis.Title.Caption = 'Shoulder Torque ( ft-lb )'
        View3D = False
        TabOrder = 1
        Anchors = [akLeft, akTop, akRight]
        ColorPaletteIndex = 13
        object ShldrTorqueSeries: TBarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
          Data = {0000000000}
        end
      end
      object DeltaTorqueChart: TChart
        Left = 12
        Top = 441
        Width = 718
        Height = 210
        Legend.Visible = False
        Title.Text.Strings = (
          '')
        BottomAxis.ExactDateTime = False
        BottomAxis.Increment = 500.000000000000000000
        BottomAxis.LabelsSeparation = 0
        BottomAxis.MinorTicks.Visible = False
        BottomAxis.Title.Caption = 'Delta Torque ( ft-lb )'
        View3D = False
        TabOrder = 2
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColorPaletteIndex = 13
        object DeltaTorqueSeries: TBarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
          Data = {0000000000}
        end
      end
    end
    object ByTurnsSheet: TTabSheet
      Caption = 'By Turns'
      ImageIndex = 2
      DesignSize = (
        738
        668)
      object FinalTurnsChart: TChart
        Left = 12
        Top = 10
        Width = 718
        Height = 210
        Legend.Visible = False
        Title.Text.Strings = (
          '')
        BottomAxis.ExactDateTime = False
        BottomAxis.Increment = 0.500000000000000000
        BottomAxis.LabelsSeparation = 0
        BottomAxis.Title.Caption = 'Final Turns'
        View3D = False
        TabOrder = 0
        Anchors = [akLeft, akTop, akRight]
        ColorPaletteIndex = 13
        object FinalTurnsSeries: TBarSeries
          BarBrush.BackColor = clDefault
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
          Data = {0000000000}
        end
      end
      object DeltaTurnsChart: TChart
        Left = 12
        Top = 442
        Width = 718
        Height = 210
        Legend.Visible = False
        Title.Text.Strings = (
          ''
          '')
        BottomAxis.ExactDateTime = False
        BottomAxis.Increment = 0.500000000000000000
        BottomAxis.LabelsSeparation = 0
        BottomAxis.MaximumOffset = 1
        BottomAxis.Title.Caption = 'Delta Turns'
        View3D = False
        TabOrder = 1
        Anchors = [akLeft, akTop, akRight, akBottom]
        ColorPaletteIndex = 13
        object DeltaTurnsSeries: TBarSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Emboss.Color = 8487297
          Marks.Shadow.Color = 8553090
          Marks.Visible = True
          Title = 'DeltaTurnsSeries'
          BarWidthPercent = 75
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
          Data = {0000000000}
        end
      end
      object ShldrTurnsChart: TChart
        Left = 12
        Top = 226
        Width = 718
        Height = 210
        Legend.Visible = False
        Title.Text.Strings = (
          '')
        BottomAxis.ExactDateTime = False
        BottomAxis.Increment = 0.500000000000000000
        BottomAxis.LabelsSeparation = 0
        BottomAxis.Title.Caption = 'Shoulder Turns'
        View3D = False
        TabOrder = 2
        Anchors = [akLeft, akTop, akRight]
        ColorPaletteIndex = 13
        object ShldrTurnsSeries: TBarSeries
          BarBrush.BackColor = clDefault
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Emboss.Color = 8487297
          Marks.Shadow.Color = 8487297
          Marks.Visible = True
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Bar'
          YValues.Order = loNone
          Data = {0000000000}
        end
      end
    end
  end
  object PrintBtn: TButton
    Left = 679
    Top = 714
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Print'
    TabOrder = 1
    OnClick = PrintBtnClick
  end
  object PrinterSetupDlg: TPrinterSetupDialog
    Left = 696
    Top = 8
  end
end
