//---------------------------------------------------------------------------
#ifndef RptStatsDlgH
#define RptStatsDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Tabs.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.Series.hpp>
#include <Vcl.Dialogs.hpp>
#include "JobManager.h"
#include "Messages.h"
//---------------------------------------------------------------------------
class TRptStatsForm : public TForm
{
__published:    // IDE-managed Components
    TPageControl *SumChartPgCtrl;
    TTabSheet *SummarySheet;
    TTabSheet *ByTorqueSheet;
    TTabSheet *ByTurnsSheet;
    TChart *FinalTurnsChart;
    TChart *DeltaTurnsChart;
    TChart *ShldrTurnsChart;
    TChart *FinalTorqueChart;
    TChart *ShldrTorqueChart;
    TChart *DeltaTorqueChart;
    TChart *FinTqeVsFinturnsChart;
    TChart *ShldrTqeVsShldrTurnsChart;
    TChart *DeltaTqeVsDeltaTrunsChart;
    TPointSeries *shldrTqeVsShldrTurnsSeries;
    TPointSeries *DeltaTqeVsDeltaTurnsSeries;
    TBarSeries *FinalTorqueSeries;
    TBarSeries *FinalTurnsSeries;
    TBarSeries *ShldrTurnsSeries;
    TPointSeries *FinTqeVsFinTurnsSeries;
    TBarSeries *ShldrTorqueSeries;
    TBarSeries *DeltaTorqueSeries;
    TBarSeries *DeltaTurnsSeries;
    TButton *PrintBtn;
    TPrinterSetupDialog *PrinterSetupDlg;
    void __fastcall PrintBtnClick(TObject *Sender);

private:

    TJob* m_currJob;

    typedef struct
    {
        float min;
        float max;
    } VALUE_RANGE;

    VALUE_RANGE m_finalTurnsRange;
    VALUE_RANGE m_shldrTurnsRange;
    VALUE_RANGE m_deltaTurnsRange;

    VALUE_RANGE m_finalTqRange;
    VALUE_RANGE m_shldrTqRange;
    VALUE_RANGE m_deltaTqRange;

    void InitValueRange( VALUE_RANGE& aRange );
    void UpdateValueRange( VALUE_RANGE& aRange, float aValue );

    typedef struct
    {
        float rangeStart;
        float rangeEnd;
        int itemCount;
    } BIN_ITEM;

    void  GetBarchart( float* maxTorque, float* maxTurns );

    void  PlotSummary( const CONNECTION_INFO& currConn, float m_Turns, float m_Torque );
    void  PlotTorque( BIN_ITEM finTqeBinArray[], BIN_ITEM shldrTqeBinArray[], BIN_ITEM deltaTqeBinArray[], int nbrArrayItems );
    void  PlotTurns( BIN_ITEM finTurnsBinArray[], BIN_ITEM shldrTurnsBinArray[], BIN_ITEM deltaTurnsBinArray[], int nbrArrayItems );

    void  InitBinArray( BIN_ITEM binArray[], int nbrArrayItems, float firstBinStart, float binStepValue );
    void  UpdateBinArray( BIN_ITEM binArray[], int nbrArrayItems, float currValue );

    float CalcAvg( float total, int count );

    void __fastcall WMCopyGraphToClip( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_COPY_GRAPH_TO_CLIP, TMessage, WMCopyGraphToClip )
    END_MESSAGE_MAP(TForm)

public:
    __fastcall TRptStatsForm(TComponent* Owner);

    void ShowPlots( TJob* currJob );
};

//---------------------------------------------------------------------------
extern PACKAGE TRptStatsForm *RptStatsForm;
//---------------------------------------------------------------------------
#endif
