#include <vcl.h>
#pragma hdrstop

#include <Clipbrd.hpp>

#include "RptStatsRptDlg.h"
#include "Messages.h"
#include "RptHelpers.h"

#pragma package(smart_init)
#pragma link "frxClass"
#pragma resource "*.dfm"


TRptStatsRptForm *RptStatsRptForm;


__fastcall TRptStatsRptForm::TRptStatsRptForm(TComponent* Owner) : TForm(Owner)
{
}

void TRptStatsRptForm::ShowReport( TJob* currJob, TForm* graphsForm )
{
    // Validate passed parameters
    if( currJob == NULL )
        return;

    if( graphsForm == NULL )
        return;

    m_currJob   = currJob;
    m_graphForm = graphsForm;

    // Initialize job name memos - these never change
    RptSetMemoText( m_currJob, GraphReport, "ClientNameMemo1", m_currJob->Client );
    RptSetMemoText( m_currJob, GraphReport, "JobLocnMemo1",    m_currJob->Location );
    RptSetMemoText( m_currJob, GraphReport, "JobDateMemo",     m_currJob->DateTimeString() );

    // The dataset always has three records
    GraphDataSet->RangeEndCount = 3;
    GraphDataSet->RangeEnd      = reCount;

    GraphReport->ShowReport( true );
}


void __fastcall TRptStatsRptForm::GraphReportBeforePrint(TfrxReportComponent *Sender)
{
    // Print Final Torque graph from the RptStatsDlg. The dataset has three
    // records, each of which corresponds to a tab on the RptStatsDlg. Then
    // on each tab there are three graphs.
    int whichGraph = -1;

    if( Sender->Name == "FinalPicture" )
        whichGraph = 3 * GraphDataSet->RecNo;
    else if( Sender->Name == "ShoulderPicture" )
        whichGraph = 3 * GraphDataSet->RecNo + 1;
    else if( Sender->Name == "DeltaPicture" )
        whichGraph = 3 * GraphDataSet->RecNo + 2;

    if( whichGraph >= 0 )
    {
        m_graphForm->Perform( WM_COPY_GRAPH_TO_CLIP, whichGraph, 0 );

        if( Clipboard()->HasFormat( CF_ENHMETAFILE ) )
        {
            // Now copy the clipboard contents into the report graph picture
            TfrxPictureView* graphPicView = (TfrxPictureView*)Sender;

            graphPicView->Picture->Metafile->Assign( Clipboard() );
        }
    }
}


void __fastcall TRptStatsRptForm::GraphDataSetGetValue(const UnicodeString VarName, Variant &Value)
{
    if( VarName == "graph_heading_title" )
    {
        switch( GraphDataSet->RecNo )
        {
            case 0:   Value = "Summary";    break;
            case 1:   Value = "By Torque";  break;
            case 2:   Value = "By Turns";   break;
            default:  Value = "Bad rec nbr " + IntToStr( GraphDataSet->RecNo );  break;
        }

    }
    else
    {
        Value = "Unknown field: " + VarName;
    }
}


void __fastcall TRptStatsRptForm::GraphReportPreview(TObject *Sender)
{
    TfrxReport* frxReport = (TfrxReport*)Sender;
    frxReport->PreviewForm->BorderIcons = TBorderIcons() << biSystemMenu << biMaximize;
}

