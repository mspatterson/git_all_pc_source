//---------------------------------------------------------------------------
#ifndef RptStatsRptDlgH
#define RptStatsRptDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "frxClass.hpp"
#include "JobManager.h"
//---------------------------------------------------------------------------
class TRptStatsRptForm : public TForm
{
__published:    // IDE-managed Components
    TfrxReport *GraphReport;
    TfrxUserDataSet *GraphDataSet;
    void __fastcall GraphReportBeforePrint(TfrxReportComponent *Sender);
    void __fastcall GraphDataSetGetValue(const UnicodeString VarName, Variant &Value);
    void __fastcall GraphReportPreview(TObject *Sender);

private:
     TJob*  m_currJob;
     TForm* m_graphForm;

public:// User declarations
    __fastcall TRptStatsRptForm(TComponent* Owner);
    void ShowReport( TJob* currJob, TForm* graphsForm );
};
//---------------------------------------------------------------------------
extern PACKAGE TRptStatsRptForm *RptStatsRptForm;
//---------------------------------------------------------------------------
#endif
