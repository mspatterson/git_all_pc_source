//************************************************************************
//
//  SubDeviceBaseClass.h: base class for all WTTS-like devices. This is
//       an abstract class. Therefore, descendent classes will need to
//       implemement a number of abstract functions.
//
//************************************************************************
#include <vcl.h>
#pragma hdrstop

#include "SubDeviceBaseClass.h"

#pragma package(smart_init)


//
// Private Declarations
//


//
// Class Implementation
//

const UnicodeString TSubDevice::WitsFieldCaptions[eWF_NbrWITSFields] = {
    "Data Source",
    "RPM",
    "Torque 045",
    "Torque 225",
    "Tension 000",
    "Tension 090",
    "Tension 180",
    "Tension 270",
    "Compass X",
    "Compass Y",
    "Compass Z",
    "Acceleration X",
    "Acceleration Y",
    "Acceleration Z",
    "Battery Level",
    "Header Sequence Number",
    "Header Timestamp"
};


__fastcall TSubDevice::TSubDevice( DEVICE_TYPE itsType )
{
    m_devType = itsType;

    ClearCommStats();

    m_port.portType       = CT_UNUSED;
    m_port.connResult     = CCommObj::ERR_NONE;
    m_port.connResultText = "(closed)";

    m_portLost  = false;
    m_linkState = LS_CLOSED;

    // UDP control vars
    m_bUsingUDP  = false;
    m_sTTDevAddr = "";
    m_wTTDevPort = 0;

    // Set time param defaults
    m_timeParams.rfcRate        = DEFAULT_RFC_RATE;
    m_timeParams.rfcTimeout     = DEFAULT_RFC_TIMEOUT;
    m_timeParams.streamRate     = DEFAULT_STREAM_RATE;
    m_timeParams.streamTimeout  = DEFAULT_STREAM_TIMEOUT;
    m_timeParams.pairingTimeout = DEFAULT_PAIRING_TIMEOUT;

    // Set the default packet timeout
    m_tPacketTimeout = DEFAULT_PACKET_TIMEOUT;

    // Only newer units have a device ID
    m_devID = "";

    // Set default averaging params
    m_avgTension = new TNoAverage();
    m_avgTorque  = new TNoAverage();
    m_avgRPM     = new TNoAverage();

    // Unknown battery level default
    m_battLevel = eBL_Unknown;

    // Set all fields to be "0" to ensure no blanks are ever sent
    for( int iField = 0; iField < eWF_NbrWITSFields; iField++ )
        m_WitsFieldValues[iField] = "0";

}


UnicodeString TSubDevice::GetDevTypeText( void )
{
    return DeviceTypeText[m_devType];
}


bool TSubDevice::GetDevIsRecving( void )
{
    if( time( NULL ) > m_port.stats.tLastPkt + m_tPacketTimeout )
        return false;

    return true;
}


bool TSubDevice::GetIsConnected( void )
{
    // Legacy sub only uses the data port
    if( m_port.portObj == NULL )
        return false;

    return m_port.portObj->isConnected;
}


bool TSubDevice::Connect( const COMMS_CFG& portCfg )
{
    if( portCfg.portType == CT_UNUSED )
    {
        m_port.connResultText = "port settings not specified";
        return false;
    }

    if( !CreateCommPort( portCfg, m_port ) )
        return false;

    // Note if we're using UDP
    if( portCfg.portType == CT_UDP )
        m_bUsingUDP = true;

    return true;
}


void TSubDevice::Disconnect( void )
{
    // Base class only disconnects and releases any created objects
    ReleaseCommPort( m_port );

    m_linkState = LS_CLOSED;
}


void TSubDevice::GetCommStats( COMM_STATS& commStats )
{
    commStats.deviceType = m_devType;
    commStats.portStats  = m_port.stats;
}


void TSubDevice::ClearCommStats( void )
{
    // Only the count members of the stats gets updated
    ClearPortStats( m_port.stats );
}


bool TSubDevice::InitPropertyList( TStringList* pCaptions, TStringList* pValues, const UnicodeString captionList[], int nbrCaptions )
{
    // Helper function that initializes the passed string lists used
    // to populate property editors.
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( !IsConnected )
        return false;

    // Add all of the captions first, with blank
    for( int iItem = 0; iItem < nbrCaptions; iItem++ )
    {
        pCaptions->Add( captionList[iItem] );
        pValues->Add( "" );
    }

    return true;
}


WORD TSubDevice::GetTimeParam( WTTTS_SET_RATE_TYPE aType )
{
    switch( aType )
    {
        case SRT_RFC_RATE:        return m_timeParams.rfcRate;
        case SRT_RFC_TIMEOUT:     return m_timeParams.rfcTimeout;
        case SRT_STREAM_RATE:     return m_timeParams.streamRate;
        case SRT_STREAM_TIMEOUT:  return m_timeParams.streamTimeout;
        case SRT_PAIR_TIMEOUT:    return m_timeParams.pairingTimeout;
    }

    // Fall through means unknown type passed
    return 0;
}


void TSubDevice::SetTimeParam( WTTTS_SET_RATE_TYPE aType, WORD newValue )
{
    switch( aType )
    {
        case SRT_RFC_RATE:        m_timeParams.rfcRate        = newValue;  break;
        case SRT_RFC_TIMEOUT:     m_timeParams.rfcTimeout     = newValue;  break;
        case SRT_STREAM_RATE:     m_timeParams.streamRate     = newValue;  break;
        case SRT_STREAM_TIMEOUT:  m_timeParams.streamTimeout  = newValue;  break;
        case SRT_PAIR_TIMEOUT:    m_timeParams.pairingTimeout = newValue;  break;
    }
}


void TSubDevice::RefreshSettings( void )
{
    // Called to refresh any registry-based settings. Causes those settings
    // to be re-read from the registry.
    BATT_THRESH_VALS battThresh;
    GetBatteryThresholds( battThresh );

    m_batteryLevel.fValidThresh   = battThresh.fReadingValidThresh;
    m_batteryLevel.fMinOperThresh = battThresh.fMinOperValue;
    m_batteryLevel.fGoodThresh    = battThresh.fGoodThresh;
}


void TSubDevice::SetAveraging( AVG_TYPE atType, const AVERAGING_PARAMS& avgParams )
{
    // Create the new averaging object first. We'll assign it to the correct
    // class var after that.
    TAverage* newAvgObj;

    switch( avgParams.avgMethod )
    {
        case AM_BOXCAR:      newAvgObj = new TMovingAverage( (int)( avgParams.param + 0.5 ), avgParams.absVariance );  break;
        case AM_EXPONENTIAL: newAvgObj = new TExpAverage( avgParams.param, avgParams.absVariance );                    break;
        default:             newAvgObj = new TNoAverage();  break;
    }

    switch( atType )
    {
        case AT_TENSION:

            if( m_avgTension != NULL )
                delete m_avgTension;

            m_avgTension = newAvgObj;

            break;

        case AT_TORQUE:

            if( m_avgTorque != NULL )
                delete m_avgTorque;

            m_avgTorque = newAvgObj;

            break;

        default:

            // Should never happen, but if it does delete the newly created
            // object to prevent a memory leak.
            delete newAvgObj;
    }
}


bool TSubDevice::GetLastAvgData( float& avgTorque, float& avgTension, int& avgRPM )
{
    // Returns the last avg value for the two parameters in the function.
    // Depending on the device type that is instantiated, this data could
    // come from idle packets or from streaming packets. Returns true if
    // both averages are valid.
    avgTorque  = m_avgTorque->AverageValue;
    avgTension = m_avgTension->AverageValue;
    avgRPM     = m_avgRPM->AverageValue;

    return( m_avgTorque->IsValid && m_avgTension->IsValid && m_avgRPM->IsValid );
}


void TSubDevice::SetPDSSwitchThresholds( int iOnThresh, int iOffThresh )
{
    m_pdsOnThresh  = iOnThresh;
    m_pdsOffThresh = iOffThresh;
}


bool TSubDevice::GetPDSClosed( void )
{
    // Default action is to report the switch is not closed
    return false;
}


bool TSubDevice::GetPDSOpen( void )
{
    // Default action is to report the switch is not open
    return false;
}


void TSubDevice::GetLastTempCompValues( TEMP_COMP_VALUES& tcvValues )
{
    // Default behaviour: clear the returned values
    tcvValues.fRTDSlope            = 0.0;
    tcvValues.fFirstOrderOffset    = 0.0;
    tcvValues.fRTDSlopeROC         = 0.0;
    tcvValues.fTotalTensionOffset  = 0.0;
}


String TSubDevice::GetWitsFieldValue( WITS_FIELDS eFieldNumber )
{
    // Ensure we're in range
    if( eFieldNumber < eWF_NbrWITSFields )
        return m_WitsFieldValues[eFieldNumber];

    //Fall-through means bad input, return "0"
    return "0";
}

