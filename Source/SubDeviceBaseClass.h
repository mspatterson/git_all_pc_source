//************************************************************************
//
//  SubDeviceBaseClass.h: base class for all WTTS-like devices. This is
//       an abstract class. Therefore, descendent classes will need to
//       implemement a number of the abstract functions declared below.
//
//************************************************************************

#ifndef SubDeviceBaseClassH
#define SubDeviceBaseClassH

    #include "CommObj.h"
    #include "WTTTSDefs.h"
    #include "WTTTSProtocolUtils.h"
    #include "SerialPortUtils.h"
    #include "Averaging.h"
    #include "RegistryInterface.h"


    typedef struct {
        DEVICE_TYPE   deviceType;
        PORT_STATS    portStats;
    } COMM_STATS;


    class TSubDevice : public TObject {

      public:

        typedef enum {
            eBL_Unknown,
            eBL_Low,
            eBL_Med,
            eBL_OK,
            eNbrBattLevels
        } BatteryLevelType;

        typedef enum {
            eWF_DataSource,
            eWF_RPM,
            eWF_Torque045,
            eWF_Torque225,
            eWF_Tension000,
            eWF_Tension090,
            eWF_Tension180,
            eWF_Tension270,
            eWF_CompassX,
            eWF_CompassY,
            eWF_CompassZ,
            eWF_AccelX,
            eWF_AccelY,
            eWF_AccelZ,
            eWF_BattLevel,
            eWF_PktHdrSeqNbr,
            eWF_PktHdrTimestamp,
            eWF_NbrWITSFields
        } WITS_FIELDS;

        typedef enum {
            eWPT_None,
            eWPT_RFC,
            eWPT_Stream,
            eWPT_NbrPktTypes
        } WITS_PACKET_TYPE;

        typedef enum {
            LS_CLOSED,         // Comm port closed
            LS_HUNTING,        // Waiting for first RFC command from sub
            LS_CFG_DOWNLOAD,   // Downloading config data from device
            LS_CFG_UPLOAD,     // Uploading config data to device
            LS_IDLE,           // Idle, responding to sub RFC requests with 'no command'
            LS_STARTING,       // Waiting for data streaming to start
            LS_STREAMING,      // Streaming data samples
            LS_STOPPING,       // Stopping data stream
            NBR_LINK_STATES
        } LINK_STATE;

      protected:

        DEVICE_TYPE   m_devType;
        DEVICE_PORT   m_port;

        LINK_STATE    m_linkState;

        bool          m_portLost;

        TNotifyEvent  m_onRFCRcvd;
        TNotifyEvent  m_onStreamRcvd;

        // We use different comm object routines for UDP comms - these
        // flags track that info
        bool          m_bUsingUDP;    // True if we're using UDP comms
        AnsiString    m_sTTDevAddr;   // If using UDP, this is the IP addr from which the last event was received
        WORD          m_wTTDevPort;   // If using UDP, this is the port from which the last event was received

        TAverage*     m_avgTension;
        TAverage*     m_avgTorque;
        TAverage*     m_avgRPM;

        time_t        m_tPacketTimeout;

        int           m_pdsOnThresh;
        int           m_pdsOffThresh;

        String        m_devID;

        String        m_WitsFieldValues[eWF_NbrWITSFields];


                __fastcall  TSubDevice( DEVICE_TYPE itsType );
                            // Can only construct descendant classes

                String  GetDevTypeText( void );

        virtual String  GetDevStatus( void ) = 0;
        virtual bool    GetDevIsIdle( void ) = 0;
        virtual bool    GetDevIsStreaming( void ) = 0;
        virtual bool    GetDevIsCalibrated( void ) = 0;

        virtual bool    GetDevIsRecving( void );
        virtual bool    GetIsConnected( void );

        virtual String  GetDevHousingSN( void ) { return ""; }

        virtual String  GetPortDesc( void )  { return m_port.stats.portDesc; }

        virtual bool    GetCanStart( void ) = 0;

        virtual int     GetConfigUploadProgress( void ) { return 0; }

                bool    InitPropertyList( TStringList* pCaptions, TStringList* pValues, const UnicodeString captionList[], int nbrCaptions );

        // Battery averaging
        struct {
            float        fLastBattVolts;
            int          iLastBattUsed;
            float        fValidThresh;
            float        fMinOperThresh;
            float        fGoodThresh;
            TExpAverage* avgBattVolts;
            TExpAverage* avgBattUsed;
        } m_batteryLevel;

        // The following structure stores the timing values for the device.
        // Note that this structure is designed with the WTTTS device in mind.
        // The legacy device class can interpret these values best it can.
        typedef struct {
            WORD rfcRate;                // Sets RFC tx rate, in msecs
            WORD rfcTimeout;             // Sets how long the WTTTS receiver remains active after sending a RFC
            WORD streamRate;             // Sets the rate at which stream packets are reported
            WORD streamTimeout;          // Sets how long the WTTTS will send stream packets before automatically stopping
            WORD pairingTimeout;         // Sets how long before the WTTTS enters 'deep sleep' if it can't connect
        } TIME_PARAMS;

        TIME_PARAMS m_timeParams;

        WORD GetTimeParam( WTTTS_SET_RATE_TYPE aType );
        void SetTimeParam( WTTTS_SET_RATE_TYPE aType, WORD newValue );

        BatteryLevelType m_battLevel;

        virtual BatteryLevelType GetBattLevel( void ) { return m_battLevel; }

      public:

        virtual __fastcall ~TSubDevice() { Disconnect(); }

        __property DEVICE_TYPE   DeviceType         = { read = m_devType };
        __property String        DeviceTypeAsText   = { read = GetDevTypeText };
        __property String        DeviceStatus       = { read = GetDevStatus };
        __property bool          DeviceIsIdle       = { read = GetDevIsIdle };
        __property bool          DeviceIsStreaming  = { read = GetDevIsStreaming };
        __property bool          DeviceIsCalibrated = { read = GetDevIsCalibrated };
        __property bool          DeviceIsRecving    = { read = GetDevIsRecving };
        __property String        DeviceID           = { read = m_devID };
        __property String        DeviceHousingSN    = { read = GetDevHousingSN };

        __property LINK_STATE    DeviceLinkState    = { read = m_linkState };

        virtual bool Connect( const COMMS_CFG& portCfg );
        virtual void Disconnect( void );

        __property bool             IsConnected   = { read = GetIsConnected };
        __property UnicodeString    ConnectResult = { read = m_port.connResultText };
        __property bool             PortLost      = { read = m_portLost };
        __property UnicodeString    PortDesc      = { read = GetPortDesc };

        __property TNotifyEvent     OnRFCRcvd     = { read = m_onRFCRcvd,    write = m_onRFCRcvd    };
        __property TNotifyEvent     OnStreamRcvd  = { read = m_onStreamRcvd, write = m_onStreamRcvd };

        __property WORD             ParamRate[WTTTS_SET_RATE_TYPE] = { read = GetTimeParam, write = SetTimeParam };

        __property bool             CanStartDataCollection = { read = GetCanStart };

        __property int              ConfigUploadProgress   = { read = GetConfigUploadProgress };

        __property BatteryLevelType BatteryLevel = { read = GetBattLevel };

        virtual bool StartDataCollection( WORD streamInterval /*msecs*/ ) = 0;
        virtual bool StopDataCollection( void ) = 0;
            // Starts streaming mode at the interval passed, and stops streaming mode

        virtual bool StartCirculatingMode( WORD wInterval /*msecs*/ ) = 0;
        virtual bool StopCirculatingMode( void ) = 0;
            // Starts circulating mode at the interval passed, and stops circ mode

        virtual bool GetNextSample( WTTTS_READING& nextSample, int& rpm ) = 0;
            // Returns a calibrated torque / turns packet

        typedef struct {
            int torque045;
            int torque225;
            int torque045_R3;
            int torque225_R3;
            int tension000;
            int tension090;
            int tension180;
            int tension270;
            int compassX;
            int compassY;
            int compassZ;
            int accelX;
            int accelY;
            int accelZ;
            int gyro;
        } DEVICE_RAW_READING;

        typedef struct {
            int   torque045;
            int   torque225;
            int   tension000;
            int   tension090;
            int   tension180;
            int   tension270;
            float compassX;
            float compassY;
            float compassZ;
            float accelX;
            float accelY;
            float accelZ;
            int   gyro;     // in micro-turns
        } DEVICE_CAL_READING;

        virtual bool GetLastDataRaw( DWORD& msecs, BYTE& seqNbr, DEVICE_RAW_READING& lastPkt ) = 0;
        virtual bool GetLastDataCal( DWORD& msecs, BYTE& seqNbr, DEVICE_CAL_READING& lastPkt ) = 0;
            // Returns the last packet of stream data as either raw or calibrated data.

        void RefreshSettings( void );
            // Called to refresh any registry-based settings. Causes those settings
            // to be re-read from the registry.

        void SetAveraging( AVG_TYPE atType, const AVERAGING_PARAMS& avgParams );
            // Updates the passed averaging type to use the new averagin method.

        virtual bool GetLastAvgData( float& avgTorque, float& avgTension, int& avgRPM );
            // Returns the last avg value for the two parameters in the function.
            // Depending on the device type that is instantiated, this data could
            // come from idle packets or from streaming packets.

        void SetPDSSwitchThresholds( int iOnThresh, int iOffThresh );
            // Sets the thresholds for determining if the PDS switch is on or off

        virtual bool GetPDSClosed( void );
        virtual bool GetPDSOpen( void );
            // Returns the state of the PDS switch. Two functiona are implemented
            // as that allows for hysteresis be implemented

        typedef struct {
            float fRTDSlope;
            float fFirstOrderOffset;
            float fRTDSlopeROC;
            float fTotalTensionOffset;
        } TEMP_COMP_VALUES;

        virtual void GetLastTempCompValues( TEMP_COMP_VALUES& tcvValues );
            // Just applicable to a TesTORK device, but we have to declare it in
            // the base class to make the rest of the code easy to work with.

        virtual bool Update( void ) = 0;
            // Descendant classes must implement this function. It will get called
            // regularly to allow the class to receive and process information.

        virtual void GetCommStats( COMM_STATS& commStats );
        virtual void ClearCommStats( void );

        virtual bool ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues ) = 0;
        virtual bool GetLastStatusPkt( time_t& tLastPkt, DEVICE_RAW_READING& lastPkt ) = 0;
            // Descendant classes must implement population of the passed strings
            // with status packet values. When calling GetLastStatusPkt(), the
            // values returned are raw readings. If a device does not report a status
            // packet, then its implementation if this function must return false.

        virtual bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues ) = 0;
        virtual bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues ) = 0;
        virtual bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues ) = 0;
            // Calibration support routines. The get 'raw' function returns data as
            // binary bytes. The 'formatted' get/set functions provide access to
            // data as named values.

        virtual bool ReloadCalDataFromDevice( void ) = 0;
            // Causes the device to clear it internal cached cal data and
            // reload the data from the device.

        virtual bool WriteCalDataToDevice( void ) = 0;
            // Writes the calibration data currently cached in the software
            // to the WTTTS device.

        virtual bool WriteHousingSN( const String& sNewHousingSN ) { return false; }
            // If supported by a derived device, writes the passed
            // housing serial number to it. Note: no status information
            // is reported while the SN is updated; client code will have
            // to poll the Device status and Housing SN property to see
            // if / when the update is done.

        virtual bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo ) = 0;
            // All devices must provide a way to report back the above hw info

        virtual bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo ) = 0;
            // All devices must report back battery information

        static const String WitsFieldCaptions[];

        virtual String GetWitsFieldValue( WITS_FIELDS eFieldNumber );

      private:

    };

#endif
