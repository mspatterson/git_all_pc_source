#include <vcl.h>
#pragma hdrstop

//
// Stub class when no device is loaded
//

#include "SubDeviceNone.h"
#include "ApplUtils.h"

#pragma package(smart_init)


//
// Class Implementation
//

__fastcall TWTTTSNullDevice::TWTTTSNullDevice( void ) : TSubDevice( DT_NULL )
{
    // Init port object
    m_port.portType = CT_UNUSED;
    m_port.portObj  = NULL;

    m_port.stats.portType  = CT_UNUSED;
    m_port.stats.portDesc  = "N/A";

    ClearPortStats( m_port.stats );
}


__fastcall TWTTTSNullDevice::~TWTTTSNullDevice()
{
}


bool TWTTTSNullDevice::Connect( const COMMS_CFG& portCfg )
{
    // Allow connect to always succeed
    return true;
}


bool TWTTTSNullDevice::StartDataCollection( WORD streamInterval /*msecs*/ )
{
    // Null device cannot collect data
    return false;
}


bool TWTTTSNullDevice::StopDataCollection( void )
{
    // Cannot stop data collection
    return false;
}


bool TWTTTSNullDevice::StartCirculatingMode( WORD wInterval /*msecs*/ )
{
    // Cannot start this mode
    return false;
}


bool TWTTTSNullDevice::StopCirculatingMode( void )
{
    // Cannot stop this mode
    return false;
}


bool TWTTTSNullDevice::GetNextSample( WTTTS_READING& nextSample, int& rpm )
{
    // Cannot perform this operation with a NULL device
    return false;
}


bool TWTTTSNullDevice::Update( void )
{
    // Set the last packet received to now
    m_port.stats.tLastPkt = time( NULL );

    // No update required of Null device
    return true;
}


typedef enum {
    SI_DEV_STATE,
    NBR_STATUS_ITEMS
} STATUS_ITEM;

static const UnicodeString statusCaption[NBR_STATUS_ITEMS] = {
    "Link State",
};

bool TWTTTSNullDevice::ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, statusCaption, NBR_STATUS_ITEMS ) )
        return false;

    // Always show the device state
    pValues->Strings[SI_DEV_STATE] = GetDevStatus();

    return true;
}


bool TWTTTSNullDevice::GetLastStatusPkt( time_t& tLastPkt, DEVICE_RAW_READING& lastPkt )
{
    tLastPkt = 0;

    memset( &lastPkt, 0, sizeof( DEVICE_RAW_READING ) );

    return false;
}


bool TWTTTSNullDevice::GetLastDataRaw( DWORD& msecs, BYTE& seqNbr, DEVICE_RAW_READING& lastPkt )
{
    msecs  = 0;
    seqNbr = 0;

    memset( &lastPkt, 0, sizeof( DEVICE_RAW_READING ) );

    return false;
}


bool TWTTTSNullDevice::GetLastDataCal( DWORD& msecs, BYTE& seqNbr, DEVICE_CAL_READING& lastPkt )
{
    msecs  = 0;
    seqNbr = 0;

    memset( &lastPkt, 0, sizeof( DEVICE_CAL_READING ) );

    return false;
}


UnicodeString TWTTTSNullDevice::GetDevStatus( void )
{
    return "N/A";
}


bool TWTTTSNullDevice::GetCanStart( void )
{
    // Null device cannot start
    return false;
}


bool TWTTTSNullDevice::GetDevIsIdle( void )
{
    // Null device is always idle
    return true;
}


bool TWTTTSNullDevice::GetDevIsStreaming( void )
{
    // Null device is never streaming
    return false;
}


bool TWTTTSNullDevice::GetDevIsCalibrated( void )
{
    // Null device is never calibrated
    return false;
}


//
// WTTTS Onboard Configuration Data Support Functions
//

typedef enum {
    CI_VERSION,
    CI_REVISION,
    CI_SERIAL_NBR,
    CI_MFG_INFO,
    CI_MFG_DATE,
    CI_TORQUE000_OFFSET,
    CI_TORQUE000_SPAN,
    CI_TORQUE180_OFFSET,
    CI_TORQUE180_SPAN,
    CI_TENSION000_OFFSET,
    CI_TENSION000_SPAN,
    CI_TENSION090_OFFSET,
    CI_TENSION090_SPAN,
    CI_TENSION180_OFFSET,
    CI_TENSION180_SPAN,
    CI_TENSION270_OFFSET,
    CI_TENSION270_SPAN,
    CI_GYRO_OFFSET,
    CI_GYRO_SPAN,
    CI_PRESSURE_OFFSET,
    CI_PRESSURE_SPAN,
    NBR_CALIBRATION_ITEMS
} CALIBRATION_ITEM;

static const UnicodeString calFactorCaption[NBR_CALIBRATION_ITEMS] = {
    "Cal Version",
    "Cal Revision",
    "Serial Number",
    "Manufacture Info",
    "Manufacture Date",
    "Torque 000 Offset",
    "Torque 000 Span",
    "Torque 180 Offset",
    "Torque 180 Span",
    "Tension 000 Offset",
    "Tension 000 Span",
    "Tension 090 Offset",
    "Tension 090 Span",
    "Tension 180 Offset",
    "Tension 180 Span",
    "Tension 270 Offset",
    "Tension 270 Span",
    "Gyro Offset",
    "Gyro Span",
    "Pressure Offset",
    "Pressure Span",
};

// Each area of memory is represented by its own structure. The first 8 pages
// are for manufacturing and other 'public' information. The second 8 pages
// are reserved for internal use by the WTTTS microcontroller. The next 15
// pages are available for host-controlled calibration data. The last page
// contains host-controlled calibration data version control.

#define CURR_CAL_VERSION  1

typedef struct {
    float torque000_Offset;
    float torque000_Span;
    float torque180_Offset;
    float torque180_Span;
    float tension000_Offset;
    float tension000_Span;
    float tension090_Offset;
    float tension090_Span;
    float tension180_Offset;
    float tension180_Span;
    float tension270_Offset;
    float tension270_Span;
    float gyro_Offset;
    float gyro_Span;
    float pressure_Offset;
    float pressure_Span;
} WTTTS_CAL_DATA_STRUCT_V1;


static const WTTTS_CAL_DATA_STRUCT_V1 defaultWTTTSCalData = {
    /* torque000_Offset  */  0.0,
    /* torque000_Span    */  1.0,
    /* torque180_Offset  */  0.0,
    /* torque180_Span    */  1.0,
    /* tension000_Offset */  0.0,
    /* tension000_Span   */  1.0,
    /* tension090_Offset */  0.0,
    /* tension090_Span   */  1.0,
    /* tension180_Offset */  0.0,
    /* tension180_Span   */  1.0,
    /* tension270_Offset */  0.0,
    /* tension270_Span   */  1.0,
    /* gyro_Offset       */  0.0,
    /* gyro_Span         */  1.0,
    /* pressure_Offset   */  0.0,
    /* pressure_Span     */  1.0,
};


bool TWTTTSNullDevice::GetCalDataRaw( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    return true;
}


bool TWTTTSNullDevice::GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, calFactorCaption, NBR_CALIBRATION_ITEMS ) )
        return false;

    return true;
}


bool TWTTTSNullDevice::SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    // Cannot set cal data for Null device
    return false;
}


bool TWTTTSNullDevice::GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo )
{
    // Null devices have no hw info
    return false;
}


bool TWTTTSNullDevice::GetDeviceBatteryInfo( BATTERY_INFO& battInfo )
{
    // Null devices have no battery info
    return false;
}


bool TWTTTSNullDevice::ReloadCalDataFromDevice( void )
{
    // Null device cannot reload cal data
    return false;
}


bool TWTTTSNullDevice::WriteCalDataToDevice( void )
{
    // Null device cannot write cal data to device
    return false;
}

