#ifndef SubDeviceNoneH
#define SubDeviceNoneH

    //
    // Class Declaration for new WTTTS Sub handler
    //

    #include "SubDeviceBaseClass.h"

    class TWTTTSNullDevice: public TSubDevice {

      protected:

        UnicodeString GetDevStatus( void );
        bool          GetCanStart( void );
        bool          GetDevIsIdle( void );
        bool          GetDevIsStreaming( void );
        bool          GetDevIsCalibrated( void );

      public:

        __fastcall  TWTTTSNullDevice( void );
        __fastcall ~TWTTTSNullDevice();

        bool Connect( const COMMS_CFG& portCfg );

        bool StartDataCollection( WORD streamInterval /*msecs*/ );
        bool StopDataCollection( void );

        bool StartCirculatingMode( WORD wInterval /*msecs*/ );
        bool StopCirculatingMode( void );

        bool GetNextSample( WTTTS_READING& nextSample, int& rpm );

        bool Update( void );

        bool ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues );
        bool GetLastStatusPkt( time_t& tLastPkt, DEVICE_RAW_READING& lastPkt );

        bool GetLastDataRaw( DWORD& msecs, BYTE& seqNbr, DEVICE_RAW_READING& lastPkt );
        bool GetLastDataCal( DWORD& msecs, BYTE& seqNbr, DEVICE_CAL_READING& lastPkt );

        bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues );
        bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
        bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );

        bool ReloadCalDataFromDevice( void );
        bool WriteCalDataToDevice( void );

        bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo );

        bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo );

      private:

    };

#endif
