#include <vcl.h>
#pragma hdrstop

//
// Device Class implementation for legacy WTTS device.
//

#include "SubDeviceWTTS.h"
#include "ApplUtils.h"

#pragma package(smart_init)


//
// Private Declarations
//

static const int RxBuffSize = 200;

static bool IsHexChar( BYTE aChar )
{
    if( ( aChar >= '0' ) && ( aChar <= '9' ) )
        return true;

    if( ( aChar >= 'A' ) && ( aChar <= 'F' ) )
        return true;

    if( ( aChar >= 'a' ) && ( aChar <= 'f' ) )
        return true;

    // Fall through means we don't have a hex char
    return false;
}


//
// Class Implementation
//

__fastcall TLegacySub::TLegacySub( void ) : TSubDevice( DT_LEGACY_SUB )
{
    m_streamingData = false;

    m_rxBuffer  = new BYTE[RxBuffSize];
    m_buffCount = 0;
}


__fastcall TLegacySub::~TLegacySub()
{
    if( m_rxBuffer != NULL )
        delete [] m_rxBuffer;
}


bool TLegacySub::Connect( const COMMS_CFG& portCfg )
{
    // Let base class do its work
    if( !TSubDevice::Connect( portCfg ) )
        return false;

    // Do other init work now
    return true;
}


bool TLegacySub::StartDataCollection( WORD streamInterval /*msecs*/ )
{
    // This type of device is always generating data. When a client wants to
    // start data collection, just start adding reports to the queue.
    if( !IsConnected )
        return false;

    /* to do: could fake an output rate based on the passed stream interval */
    /* to do: need to capture current rotation value as a rotation 'start point */

    m_haveTorque   = false;
    m_haveTension  = false;
    m_haveRotation = false;

    m_startTime     = GetTickCount();
    m_streamingData = true;

    return true;
}


bool TLegacySub::StopDataCollection( void )
{
    m_streamingData = false;

    // Clear last packet indication
    m_haveTorque   = false;
    m_haveTension  = false;
    m_haveRotation = false;

    return true;
}


bool TLegacySub::StartCirculatingMode( WORD wInterval /*msecs*/ )
{
    // Legacy sub didn't have this mode. Just start streaming
    return StartDataCollection( wInterval );
}


bool TLegacySub::StopCirculatingMode( void )
{
    // Legacy sub didn't have this mode. Just stop streaming
    return StopDataCollection();
}


bool TLegacySub::GetNextSample( WTTTS_READING& nextSample, int& rpm )
{
    if( HavePacket() )
    {
        nextSample.msecs    = (WORD)TimeSince( m_startTime );
        nextSample.rotation = m_lastRotation;
        nextSample.torque   = m_lastTorque;
        nextSample.tension  = m_lastTension;

        // RPM not reported by legacy device
        rpm = 0;

        // Clear last packet indication
        m_haveTorque   = false;
        m_haveTension  = false;
        m_haveRotation = false;

        return true;
    }

    // Fall through means we have no packet
    return false;
}


bool TLegacySub::Update( void )
{
    // Update our state machine here
    if( !IsConnected )
        return false;

    if( !m_streamingData )
    {
        // Not currently streaming - flush any data
        m_port.portObj->CommRecv( m_rxBuffer, RxBuffSize );
        return true;
    }

    DWORD bytesRxd = m_port.portObj->CommRecv( &(m_rxBuffer[m_buffCount]), RxBuffSize - m_buffCount );

    if( bytesRxd > 0 )
    {
        IncStat( m_port.stats.bytesRecv, bytesRxd );
        m_lastRxTime = GetTickCount();
    }

    m_buffCount += bytesRxd;

    // Extract subfields until we have a packet. Note that if we have a packet
    // pending, we'll keep it waiting for GetNextSample() to be called.
    while( HaveField() )
    {
        // We have a field. Check if this has completed a packet
        if( HavePacket() )
        {
            m_port.stats.tLastPkt = time( NULL );
            IncStat( m_port.stats.respRecv );

            break;
        }
    }

    // Flush any data where the received data has gone stale
    if( HaveTimeout( m_lastRxTime, 500 ) )
        m_buffCount = 0;

    return true;
}


typedef enum {
    SI_LAST_PKT_TIME,
    SI_LAST_TORQUE,
    SI_LAST_TENSION,
    SI_LAST_ROTATION,
    SI_BATT_LEVEL,
    NBR_STATUS_ITEMS
} STATUS_ITEM;

static const UnicodeString statusCaption[NBR_STATUS_ITEMS] = {
    "Last Pkt Time",
    "Torque",
    "Tension",
    "Rotation",
    "Battery Level"
};


bool TLegacySub::ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, statusCaption, NBR_STATUS_ITEMS ) )
        return false;

    // Make sure we have data to report
    if( m_lastFieldTime == 0 )
        return true;

    // Don't get RFC's with the legacy device, so just populate with the
    // last data received
    pValues->Strings[SI_LAST_PKT_TIME]  = LocalTimeString( m_lastFieldTime );
    pValues->Strings[SI_LAST_TORQUE]    = IntToStr( m_lastTorque );
    pValues->Strings[SI_LAST_TENSION]   = IntToStr( m_lastTension );
    pValues->Strings[SI_LAST_ROTATION]  = IntToStr( m_lastRotation );
    pValues->Strings[SI_BATT_LEVEL]     = IntToStr( m_battLevel );

    return true;
}


bool TLegacySub::GetLastStatusPkt( time_t& tLastPkt, DEVICE_RAW_READING& lastPkt )
{
    memset( &lastPkt, 0, sizeof( DEVICE_RAW_READING ) );

    tLastPkt = m_lastFieldTime;

    // Populate only those fields that the legacy device supports
    lastPkt.torque045  = m_lastTorque;
    lastPkt.torque225  = m_lastTorque;
    lastPkt.tension000 = m_lastTension;
    lastPkt.tension090 = m_lastTension;
    lastPkt.tension180 = m_lastTension;
    lastPkt.tension270 = m_lastTension;

    return true;
}


bool TLegacySub::GetLastDataRaw( DWORD& msecs, BYTE& seqNbr, DEVICE_RAW_READING& lastPkt )
{
    // Legacy device does not report raw data, only calibrated data.
    // Fill the packet with the data.
    memset( &lastPkt, 0, sizeof( DEVICE_RAW_READING ) );

    msecs = m_lastFieldTime * 1000;

    // Sequence number not reported by legacy device
    seqNbr = 0;

    // Populate only those fields that the legacy device supports
    lastPkt.torque045  = m_lastTorque;
    lastPkt.torque225  = m_lastTorque;
    lastPkt.tension000 = m_lastTension;
    lastPkt.tension090 = m_lastTension;
    lastPkt.tension180 = m_lastTension;
    lastPkt.tension270 = m_lastTension;

    return true;
}


bool TLegacySub::GetLastDataCal( DWORD& msecs, BYTE& seqNbr, DEVICE_CAL_READING& lastPkt )
{
    // Fill the packet with the data.
    memset( &lastPkt, 0, sizeof( DEVICE_RAW_READING ) );

    msecs = m_lastFieldTime * 1000;

    // Sequence number not reported by legacy device
    seqNbr = 0;

    // Populate only those fields that the legacy device supports
    lastPkt.torque045  = m_lastTorque;
    lastPkt.torque225  = m_lastTorque;
    lastPkt.tension000 = m_lastTension;
    lastPkt.tension090 = m_lastTension;
    lastPkt.tension180 = m_lastTension;
    lastPkt.tension270 = m_lastTension;

    return true;
}


bool TLegacySub::GetCalDataRaw( TStringList* pCaptions, TStringList* pValues )
{
    // Legacy device does not expose data calibration memory
    if( pCaptions != NULL )
        pCaptions->Clear();

    if( pValues != NULL )
        pValues->Clear();

    return true;
}


bool TLegacySub::GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    // Legacy device does not expose data calibration memory
    if( pCaptions != NULL )
        pCaptions->Clear();

    if( pValues != NULL )
        pValues->Clear();

    return true;
}


bool TLegacySub::SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    // Legacy device does not expose data calibration memory
    if( pCaptions != NULL )
        pCaptions->Clear();

    if( pValues != NULL )
        pValues->Clear();

    return true;
}


bool TLegacySub::ReloadCalDataFromDevice( void )
{
    // Legacy device does not support reading of cached data
    return false;
}


bool TLegacySub::WriteCalDataToDevice( void )
{
    // Legacy device does not support writing of cached data to it
    return false;
}


bool TLegacySub::GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo )
{
    // Legacy device does not support hw info
    devHWInfo.housingSN = "CT 3000";
    devHWInfo.fwRev     = "(n/a)";
    devHWInfo.cfgStraps = "(n/a)";
    devHWInfo.mfgSN     = "(n/a)";
    devHWInfo.mfgInfo   = "(n/a)";
    devHWInfo.mfgDate   = "(n/a)";

    return false;
}


bool TLegacySub::GetDeviceBatteryInfo( BATTERY_INFO& battInfo )
{
    // Legacy device does not support battery info
    return false;
}


bool TLegacySub::HavePacket( void )
{
    return( m_haveTorque && m_haveTension && m_haveRotation );
}


bool TLegacySub::HaveField( void )
{
    // Examine the buffer. If enough bytes exist for a field, check for one.
    // Each field has the same format: a type byte, 4 bytes of hex digits,
    // and a one-byte binary value of battery level.
    const int fieldLength = 1 + 4 + 1;

    AnsiString sValue;
    sValue.SetLength( 5 );
    sValue[1] = '$';

    while( m_buffCount >= fieldLength )
    {
        if( ( m_rxBuffer[0] == 'Q' ) || ( m_rxBuffer[0] == 'R' ) || ( m_rxBuffer[0] == 'T' ) )
        {
            // Have a matching ident byte. See if the next four bytes are hex chars
            if( IsHexChar( m_rxBuffer[1] ) && IsHexChar( m_rxBuffer[2] ) && IsHexChar( m_rxBuffer[3] ) && IsHexChar( m_rxBuffer[4] ) )
            {
                // Looks like we have a valid field. Extract the field value first.
                // We'll be extracting a short int. If the high order bit (D15) is
                // set, sign-extend the value to make it a negative value.
                sValue[2] = (char)m_rxBuffer[1];
                sValue[3] = (char)m_rxBuffer[2];
                sValue[4] = (char)m_rxBuffer[3];
                sValue[5] = (char)m_rxBuffer[4];

                int fieldValue = sValue.ToIntDef( 0 );

                if( ( fieldValue & 0x8000 ) != 0 )
                    fieldValue |= 0xFFFF0000;

                if( m_rxBuffer[0] == 'Q' )
                {
                    m_haveTorque = true;
                    m_lastTorque = fieldValue;

                    m_avgTorque->AddValue( fieldValue );
                }
                else if( m_rxBuffer[0] == 'R' )
                {
                    m_haveRotation = true;
                    m_lastRotation = fieldValue * 1000;  // Convert milliturns to microturns
                }
                else
                {
                    m_haveTension = true;
                    m_lastTension = fieldValue;

                    m_avgTension->AddValue( fieldValue );
                }

                // Battery level is just a binary byte value
                m_battLevel = m_rxBuffer[5];

                m_lastFieldTime = time( NULL );

                ShiftBufferDown( m_rxBuffer, m_buffCount, 1 );

                return true;
            }
        }

        // Shift out the number of bytes processed
        ShiftBufferDown( m_rxBuffer, m_buffCount, 1 );
    }

    // Fall through means no field was found
    return false;
}


UnicodeString TLegacySub::GetDevStatus( void )
{
    // The legacy sub doesn't have any internal states. It is either
    // always on or off
    if( IsConnected )
        return "Connected";
    else
        return "Not connected";
}


bool TLegacySub::GetCanStart( void )
{
    // The legacy WTTS device can always accept a start command
    return true;
}


bool TLegacySub::GetDevIsIdle( void )
{
    // The legacy WTTS device can always accept a command
    return true;
}


bool TLegacySub::GetDevIsStreaming( void )
{
    // The legacy WTTS device is always streaming
    return true;
}


bool TLegacySub::GetDevIsCalibrated( void )
{
    // The legacy WTTS device is calibrated
    return true;
}

