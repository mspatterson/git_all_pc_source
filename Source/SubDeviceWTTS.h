#ifndef SubDeviceWTTSH
#define SubDeviceWTTSH

    //
    // Class Declaration for new legacy WTTS handler
    //

    #include "SubDeviceBaseClass.h"

    class TLegacySub: public TSubDevice {

      protected:

        UnicodeString GetDevStatus( void );
        bool          GetDevIsIdle( void );
        bool          GetCanStart( void );
        bool          GetDevIsStreaming( void );
        bool          GetDevIsCalibrated( void );

      public:

        __fastcall TLegacySub( void );
        virtual __fastcall ~TLegacySub();

        bool Connect( const COMMS_CFG& portCfg );

        bool StartDataCollection( WORD streamInterval /*msecs*/ );
        bool StopDataCollection( void );

        bool StartCirculatingMode( WORD wInterval /*msecs*/ );
        bool StopCirculatingMode( void );

        bool GetNextSample( WTTTS_READING& nextSample, int& rpm );

        bool Update( void );

        bool ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues );
        bool GetLastStatusPkt( time_t& tLastPkt, DEVICE_RAW_READING& lastPkt );

        bool GetLastDataRaw( DWORD& msecs, BYTE& seqNbr, DEVICE_RAW_READING& lastPkt );
        bool GetLastDataCal( DWORD& msecs, BYTE& seqNbr, DEVICE_CAL_READING& lastPkt );

        bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues );
        bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
        bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );

        bool ReloadCalDataFromDevice( void );
        bool WriteCalDataToDevice( void );

        bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo );

        bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo );

      private:

        bool   m_streamingData;
        DWORD  m_startTime;

        bool   m_haveTorque;
        bool   m_haveTension;
        bool   m_haveRotation;

        int    m_lastTorque;
        int    m_lastTension;
        int    m_lastRotation;
        int    m_battLevel;

        time_t m_lastFieldTime;

        BYTE*  m_rxBuffer;
        DWORD  m_buffCount;
        DWORD  m_lastRxTime;

        bool   HavePacket( void );
        bool   HaveField( void );
    };

#endif
