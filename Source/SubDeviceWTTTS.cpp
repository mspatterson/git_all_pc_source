#include <vcl.h>
#pragma hdrstop

//
// Device class for new WTTTS device.
//

#include "SubDeviceWTTTS.h"
#include "CUDPPort.h"
#include "ApplUtils.h"
#include "TypeDefs.h"

#pragma package(smart_init)



//
// Private Functions
//

static const DWORD  RxBuffLen  = 2048;

static const WORD   StartupRFCInterval    =  100;   // When initializing link, start with fast RFC
static const WORD   StartupRFCTimeout     =  250;   // rate with a long response timeout.
static const WORD   StandbyRFCInterval    = 1000;
static const WORD   StandbyRFCTimeout     =  100;
static const WORD   CircingRFCInterval    = 1000;
static const WORD   CircingRFCTimeout     =  100;
static const WORD   StreamTimeoutInterval =  120;   // WTTTS will stop streaming this many seconds after last start command

static const DWORD  MSecsPerPktTick       =   10;   // Each count in the pkt header equals this many msecs

static const int    PktSanityTolerance    =    2;   // Ensures packets are sequential

static const String EmptyValue( "---" );


static float DoSpanOffsetCal( int rawValue, int offset, float span )
{
    float fResult = 0.0;

    try
    {
        fResult = (float)(rawValue - offset ) * span;
    }
    catch( ... )
    {
    }

    return fResult;
}


//
// WTTTS Onboard Configuration Data Definitions
//

// Each area of memory is represented by its own structure. The first 8 pages
// are for manufacturing and other 'public' information. The second 8 pages
// are reserved for internal use by the WTTTS microcontroller. The next 15
// pages are available for host-controlled calibration data. The last page
// contains host-controlled calibration data version control.

#define CURR_CAL_VERSION  2
#define CURR_CAL_REV      2

typedef struct {
    float torque045_Offset;
    float torque045_Span;
    float torque225_Offset;
    float torque225_Span;
    float tension000_Offset;
    float tension000_Span;
    float tension090_Offset;
    float tension090_Span;
    float tension180_Offset;
    float tension180_Span;
    float tension270_Offset;
    float tension270_Span;
    float gyro_Offset;
    float gyro_Span;
    float pressure_Offset;
    float pressure_Span;
    WORD  battCapacity;          // Introduced in V2, in mAh
    BYTE  battType;              // Introduced in V2, 0 = unknown, 1 = Li, 2 = NiMH
    BYTE  rfu;                   // Introduced in V2, reserved for future use, init'd to 0
} WTTTS_CAL_DATA_STRUCT_V2_0;

typedef struct {
    int   torque045_Offset;      // In 2v1, the offset values were converted from floats to ints
    float torque045_Span;
    int   torque225_Offset;
    float torque225_Span;
    int   tension000_Offset;
    float tension000_Span;
    int   tension090_Offset;
    float tension090_Span;
    int   tension180_Offset;
    float tension180_Span;
    int   tension270_Offset;
    float tension270_Span;
    int   gyro_Offset;
    float gyro_Span;
    int   pressure_Offset;
    float pressure_Span;
    WORD  battCapacity;
    BYTE  battType;
    BYTE  rfu1;
} WTTTS_CAL_DATA_STRUCT_V2_1;

typedef struct {
    int   torque045_Offset;
    float torque045_Span;
    int   torque225_Offset;
    float torque225_Span;
    int   tension000_Offset;
    float tension000_Span;
    int   tension090_Offset;
    float tension090_Span;
    int   tension180_Offset;
    float tension180_Span;
    int   tension270_Offset;
    float tension270_Span;
    int   gyro_Offset;
    float gyro_Span;
    int   pressure_Offset;
    float pressure_Span;
    WORD  battCapacity;
    BYTE  battType;
    BYTE  rfu1;
    float xTalk_TqTq;
    float xTalk_TqTen;
    float xTalk_TenTq;
    float xTalk_TenTen;
    DWORD rfu2;
    DWORD rfu3;
    DWORD rfu4;
} WTTTS_CAL_DATA_STRUCT_V2_2;

static const WTTTS_CAL_DATA_STRUCT_V2_2 defaultWTTTSCalData = {
    /* torque000_Offset  */    0,
    /* torque000_Span    */  1.0,
    /* torque180_Offset  */    0,
    /* torque180_Span    */  1.0,
    /* tension000_Offset */    0,
    /* tension000_Span   */  1.0,
    /* tension090_Offset */    0,
    /* tension090_Span   */  1.0,
    /* tension180_Offset */    0,
    /* tension180_Span   */  1.0,
    /* tension270_Offset */    0,
    /* tension270_Span   */  1.0,
    /* gyro_Offset       */    0,
    /* gyro_Span         */  1.0,
    /* pressure_Offset   */    0,
    /* pressure_Span     */  1.0,
    /* battery capacity  */    0,     // in mAh
    /* battery type      */    0,     // 0 = unknown battery type
    /* RFU1              */    0,
    /* XTalk_TqTq        */  1.0,
    /* XTalk_TqTen       */  0.0,
    /* XTalk_TenTq       */  0.0,
    /* XTalk_TenTen      */  1.0,
    /* RFU 2             */  0.0,
    /* RFU 3             */  0.0,
    /* RFU 4             */  0.0,
};


//
// Class Implementation
//

const UnicodeString TWTTTSSub::CalFactorCaptions[NBR_CALIBRATION_ITEMS] = {
    "Cal Version",
    "Cal Revision",
    "Serial Number",
    "Manufacture Info",
    "Manufacture Date",
    "Calibration Date",
    "Torque 000 Offset",
    "Torque 000 Span",
    "Torque 180 Offset",
    "Torque 180 Span",
    "Tension 000 Offset",
    "Tension 000 Span",
    "Tension 090 Offset",
    "Tension 090 Span",
    "Tension 180 Offset",
    "Tension 180 Span",
    "Tension 270 Offset",
    "Tension 270 Span",
    "Torque-Torque Crosstalk",
    "Torque-Tension Crosstalk",
    "Tension-Torque Crosstalk",
    "Tension-Tension Crosstalk",
    "Gyro Offset",
    "Gyro Span",
    "Pressure Offset",
    "Pressure Span",
    "Battery Type",
    "Battery Capacity",
};


__fastcall TWTTTSSub::TWTTTSSub( void ) : TSubDevice( DT_WTTTS )
{
    // Init state vars
    m_linkState = LS_CLOSED;

    // Init rx control vars
    m_rxBuffer    = new BYTE[RxBuffLen];
    m_rxBuffCount = 0;

    memset( &m_lastRFC,     0, sizeof( m_lastRFC     ) );
    memset( &m_currReading, 0, sizeof( m_currReading ) );

    // Init command cache
    for( int iCachedItem = 0; iCachedItem < NBR_SUB_CMD_TYPES; iCachedItem++ )
        InitWTTTSDataCmdPkt( (SUB_CMD_TYPE)iCachedItem );

    // Init temp comp values. For now these are read from the registry
    // and only on boot
    GetTempCompParams( m_tempComp.params );

    m_tempComp.avgRTD      = new TExpAverage( m_tempComp.params.fRTDAvgAlpha,   m_tempComp.params.fRTDAvgMaxVar   );
    m_tempComp.avgRTDSlope = new TExpAverage( m_tempComp.params.fRTDSlopeAlpha, m_tempComp.params.fRTDSlopeMaxVar );

    ClearTempComp();

    // Init battery averaging - averaging is hard-coded for now
    m_batteryLevel.fLastBattVolts = 0.0;
    m_batteryLevel.iLastBattUsed  = 0;
    m_batteryLevel.avgBattVolts   = new TExpAverage( 10, 0 );
    m_batteryLevel.avgBattUsed    = new TExpAverage( 10, 0 );
    m_batteryLevel.fValidThresh   = 0.0;   // Will be updated by RefreshSettings() call below
    m_batteryLevel.fMinOperThresh = 0.0;   // Will be updated by RefreshSettings() call below
    m_batteryLevel.fGoodThresh    = 0.0;   // Will be updated by RefreshSettings() call below

    // Clear other device info
    ClearConfigurationData();
    ClearHardwareInfo();

    // Refresh Settings
    RefreshSettings();
}


__fastcall TWTTTSSub::~TWTTTSSub()
{
    Disconnect();

    if( m_rxBuffer != NULL )
        delete [] m_rxBuffer;

    // Free temp comp averages
    delete m_tempComp.avgRTD;
    delete m_tempComp.avgRTDSlope;

    // Free battery averages
    delete m_batteryLevel.avgBattVolts;
    delete m_batteryLevel.avgBattUsed;
}


bool TWTTTSSub::Connect( const COMMS_CFG& portCfg )
{
    // Reset our state first
    m_linkState = LS_CLOSED;

    // Let base class do its work
    if( !TSubDevice::Connect( portCfg ) )
        return false;

    // Once the physical connection is established, we go into a hunt
    // mode. In this mode, we are looking for any type of response
    // from the sub. This tells us we have the correct physical link.
    m_linkState = LS_HUNTING;

    m_streamDiscardCount = 2;

    ClearConfigurationData();
    ClearHardwareInfo();

    // Set the initial timing parameters so that the configuration download
    // happens quickly.
    m_timeParams.rfcRate       = StartupRFCInterval;
    m_timeParams.rfcTimeout    = StartupRFCTimeout;
    m_timeParams.streamTimeout = StreamTimeoutInterval;

    return true;
}


bool TWTTTSSub::StartDataCollection( WORD streamInterval /*msecs*/ )
{
    // If we are already starting or running, return immediately.
    if( ( m_linkState == LS_STARTING ) || ( m_linkState == LS_STREAMING ) )
        return true;

    // Otherwise, device must be in idle state to process this command
    if( m_linkState != LS_IDLE )
        return false;

    // Validate stream interval
    if( streamInterval == 0 )
        return false;

    // Fall through means we are in a state to start data collection.
    // Set our state and let the Update() handler send the command.
    m_timeParams.streamRate = streamInterval;

    m_linkState = LS_STARTING;

    m_streamDiscardCount = 2;

    return true;
}


bool TWTTTSSub::StopDataCollection( void )
{
    // If we are already idle or stopping, work is done
    if( ( m_linkState == LS_STOPPING ) || ( m_linkState == LS_IDLE ) )
        return true;

    // Otherwise, device must be streaming to accept this command
    // Or starting! - Otherwise we can get stuck in streaming when the user has
    // ordered a stopstreaming command
    if( ( m_linkState != LS_STREAMING ) && ( m_linkState != LS_STARTING ) )
        return false;

    // Fall through means we are in a state to stop data collection.
    // Set our state and let the Update() handler send the command.
    m_timeParams.rfcRate    = StandbyRFCInterval;
    m_timeParams.rfcTimeout = StandbyRFCTimeout;

    // Throw away the first couple of RFC packets when stopping
    m_streamDiscardCount = 2;

    m_linkState = LS_STOPPING;

    return true;
}


bool TWTTTSSub::StartCirculatingMode( WORD wInterval /*msecs*/ )
{
    // Circulation mode uses RFCs. All we need to do is set the time
    // params and reset the rotation count

    // First, can only start circulating if we are idle
    if( m_linkState != LS_IDLE )
        return false;

    // Use default timing params for now. That means the
    // WInterval param is ignored.
    m_timeParams.rfcRate    = CircingRFCInterval;
    m_timeParams.rfcTimeout = CircingRFCTimeout;

    // RFCs are used in circulating mode. Reset RFC tracking params
    m_lastRFC.havePrevRFC = false;
    m_lastRFC.rotation    = 0;
    m_lastRFC.fAccum      = 0.0;

    m_msecsInMode = 0;

    return true;
}


bool TWTTTSSub::StopCirculatingMode( void )
{
    // Can only accept this command if we were idle (which is
    // also the circulating state)
    if( m_linkState != LS_IDLE )
        return false;

    m_timeParams.rfcRate    = StandbyRFCInterval;
    m_timeParams.rfcTimeout = StandbyRFCTimeout;

    return true;
}


bool TWTTTSSub::GetNextSample( WTTTS_READING& nextSample, int& rpm )
{
    // Ensure that we only report a sample in states where a sample
    // is valid
    switch( m_linkState )
    {
        case LS_IDLE:
            // Can report samples, but must be from RFC packets
            if( m_currReading.eSource != eRS_RFC )
            {
                m_currReading.haveReading = false;
                return false;
            }
            break;

        case LS_STREAMING:
            // Can report samples, but must be from stream packets
            if( m_currReading.eSource != eRS_Stream )
            {
                m_currReading.haveReading = false;
                return false;
            }
            break;

        default:
            // In any other state, cannot report sample data
            m_currReading.haveReading = false;
            return false;
    }

    // Fall through means we could have a potential sample. Check for one
    if( !m_currReading.haveReading )
        return false;

    // Apply crosstalk compensation
    float preCompTorque  = m_currReading.wtttsReading.torque;
    float preCompTension = m_currReading.wtttsReading.tension;

    WTTTS_CAL_DATA_STRUCT_V2_2* pCalData = (WTTTS_CAL_DATA_STRUCT_V2_2*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    m_currReading.wtttsReading.torque  = ( preCompTorque * pCalData->xTalk_TqTq  ) + ( preCompTension * pCalData->xTalk_TqTen  );
    m_currReading.wtttsReading.tension = ( preCompTorque * pCalData->xTalk_TenTq ) + ( preCompTension * pCalData->xTalk_TenTen );

    nextSample = m_currReading.wtttsReading;
    rpm        = m_currReading.rpm;

    m_currReading.haveReading = false;

    return true;
}


bool TWTTTSSub::Update( void )
{
    // Update our state machine here. First, if we have no port, there is
    // nothing to do
    if( m_linkState == LS_CLOSED )
        return false;

    // The nature of the protocol is such that we need to process any requests
    // or events from the device first.
    DWORD bytesRxd;

    if( m_bUsingUDP )
        bytesRxd = ((CUDPPort*)m_port.portObj)->RecvFrom( &(m_rxBuffer[m_rxBuffCount]), RxBuffLen - m_rxBuffCount, m_sTTDevAddr, m_wTTDevPort );
    else
        bytesRxd = m_port.portObj->CommRecv( &(m_rxBuffer[m_rxBuffCount]), RxBuffLen - m_rxBuffCount );

    if( bytesRxd > 0 )
    {
        m_lastRxByteTime = GetTickCount();

        IncStat( m_port.stats.bytesRecv, bytesRxd );

        m_rxBuffCount += bytesRxd;
    }
    else
    {
        // No data received - check if any bytes in the buffer have gone stale
        if( HaveTimeout( m_lastRxByteTime, 250 ) )
            m_rxBuffCount = 0;
    }

    // After checking for received data, confirm the port is still open
    if( m_port.portObj->portLost )
    {
        // This is not good!
        m_portLost = true;
        Disconnect();

        m_linkState = LS_CLOSED;

        // Reset averaging data
        m_avgTorque->Reset();
        m_avgTension->Reset();

        return false;
    }

    // Have the parser check for any responses from the unit.
    bool bRFCPending = false;

    WTTTS_PKT_HDR      pktHdr;
    TESTORK_DATA_UNION pktData;

    if( HaveTesTORKRespPkt( m_rxBuffer, m_rxBuffCount, pktHdr, pktData ) )
    {
        IncStat( m_port.stats.respRecv );

        m_port.stats.tLastPkt = time( NULL );
        m_lastRxPktTime       = GetTickCount();

        // Process the received packet. Firstly, if we are in the hunting
        // state, receiving a packet means we are on the right channel.
        // The next thing we have to do is download the cal table.
        if( m_linkState == LS_HUNTING )
        {
            m_linkState = LS_CFG_DOWNLOAD;

            m_currCfgReqCycle = 1;
        }

        if( pktHdr.pktType == WTTTS_RESP_STREAM_DATA )
        {
            // We are receiving stream data. This is valid in certain states
            m_lastStreamDataTime = GetTickCount();

            DWORD elapsedTicks;

            switch( m_linkState )
            {
                case LS_IDLE:
                case LS_CFG_DOWNLOAD:
                case LS_CFG_UPLOAD:
                case LS_STOPPING:
                    // In any of these states, receiving streaming data is
                    // not valid. Immediately send a stop command.
                    SendSubCommand( SCT_STOP_DATA_COLLECTION );
                    break;

                case LS_STARTING:
                    // We throw away a certain number of packets when starting
                    // in this state. Check for that first
                    if( m_streamDiscardCount > 0 )
                    {
                        m_streamDiscardCount -= 1;
                        break;
                    }

                    // This response is expected in this state. Reset our state
                    // vars and then fall through to streaming state processing
                    m_linkState = LS_STREAMING;

                    m_msecsInMode = 0;

                    memset( &m_lastGyroValues,    0, sizeof( GYRO_VALUES ) );
                    memset( &m_devRawReading,     0, sizeof( DEVICE_RAW_READING ) );
                    memset( &m_devCalReading,     0, sizeof( DEVICE_CAL_READING ) );
                    memset( &m_streamingRotnRecs, 0, sizeof( STREAM_MODE_ROTN_REC ) * MAX_NBR_STREAM_MODE_ROTN_RECS );

                    // Fall through...

                case LS_STREAMING:

                    // It is possible to receive repeat packets over the air.
                    // So if the current sequence number is the same as the
                    // last one, ignore this packet.
                    if( pktHdr.seqNbr == m_lastPktHdr.seqNbr )
                        break;

                    elapsedTicks   = CalcElapsedTicks( m_lastPktHdr.timeStamp, pktHdr.timeStamp );
                    m_msecsInMode += MSecsPerPktTick * elapsedTicks;

                    // Determine how many packets have elapsed since the last
                    // packet was received
                    int pktCount;

                    if( pktHdr.seqNbr >= m_lastPktHdr.seqNbr )
                        pktCount = (int)( pktHdr.seqNbr - m_lastPktHdr.seqNbr );
                    else
                        pktCount = 256 - (int)m_lastPktHdr.seqNbr + pktHdr.seqNbr;

                    // Discard the packet(s) if they fail a sanity check
                    int sanity = abs( elapsedTicks - pktCount );

                    // Convert the raw values to calibrated values if
                    // they pass the sanity check
                    if( sanity <= PktSanityTolerance )
                    {
                        CreateCalibratedReading( pktData.streamData, MSecsPerPktTick * elapsedTicks, pktCount );
                        LogCalibratedPacket( pktHdr, m_devCalReading );
                        UpdateAveraging();

                        if( m_onStreamRcvd )
                            m_onStreamRcvd( this );
                    }

                    // We need to periodically send start streaming commands
                    // in this state. We do this at 4x the timeout interval
                    if( time( NULL ) >= m_lastStreamStartTime + StreamTimeoutInterval / 4 )
                    {
                        SendSubCommand( SCT_START_DATA_COLLECTION );
                        m_lastStreamStartTime = time( NULL );
                    }

                    break;
            }

            // Pass the packet info to the logging routine. It will check if
            // logging is enabled.
            LogRawStreamPacket( pktHdr, pktData.streamData );
        }
        else if( pktHdr.pktType == WTTTS_RESP_VER_PKT )
        {
            // Save the version info
            m_cfgStraps =  pktData.verPkt.hardwareSettings;

            memcpy( m_firmwareVer, pktData.verPkt.fwVer, WTTTS_FW_VER_LEN );
        }
        else if( pktHdr.pktType == WTTTS_RESP_CFG_DATA )
        {
            // This is valid only in the upload or download cfg states.
            // If downloading cfg data, save the data and mark the page
            // as such. If uploading, this is an ack to a previous
            // 'set cfg' command - verify the response.
            if( m_linkState == LS_CFG_DOWNLOAD )
            {
                if( pktData.cfgData.pageNbr < NBR_WTTTS_CFG_PAGES )
                {
                    m_cfgPgStatus[pktData.cfgData.pageNbr].havePage = true;

                    int pageOffset = pktData.cfgData.pageNbr * NBR_BYTES_PER_WTTTS_PAGE;

                    memcpy( &( m_rawCfgData[pageOffset] ), pktData.cfgData.pageData, NBR_BYTES_PER_WTTTS_PAGE );
                }
            }
            else if( m_linkState == LS_CFG_UPLOAD )
            {
                // The WTTTS will have echoed back in response to a previous
                // 'set' packet command. If the values echoed back match
                // the values in the table, then that item has been
                // successfully uploaded. First, check the response code.
                if( pktData.cfgData.result != WTTTS_PG_RESULT_SUCCESS )
                {
                    // WTTTS is indicating we cannot download this page.
                    // Clear the download pending bit and move on.
                    m_cfgPgStatus[pktData.cfgData.pageNbr].setPending = false;
                }
                else if( pktData.cfgData.pageNbr < NBR_WTTTS_CFG_PAGES )
                {
                    // WTTTS liked the page number. If our cached data matches
                    // the data echoed back, this page has been updated.
                    int   pageOffset = pktData.cfgData.pageNbr * NBR_BYTES_PER_WTTTS_PAGE;
                    BYTE* pgPointer  = &( m_rawCfgData[pageOffset] );

                    if( memcmp( pgPointer, pktData.cfgData.pageData, NBR_BYTES_PER_WTTTS_PAGE ) == 0 )
                        m_cfgPgStatus[pktData.cfgData.pageNbr].setPending = false;
                }
            }
        }
        else if( ( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_V1 ) || ( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_V2 ) )
        {
            // The WTTTS has requested a command from us. If we have
            // none pending, send a 'no command' response back.
            m_lastRFC.dwRFCTick = GetTickCount();
            m_lastRFC.tRFCTime  = time( NULL );

            // Before saving the RFC packet, check if we have a new device attached.
            // If so, we need to download its cal data (and will need to advise
            // clients that the device changed)
            if( pktHdr.pktType == WTTTS_RESP_REQ_FOR_CMD_V2 )
            {
                String sIDInRFC = DeviceIDToStr( pktData.rfcPktV2.deviceID );

                if( sIDInRFC.CompareIC( m_devID ) != 0 )
                {
                    // TODO: figure out how to advise clients of change

                    // Reload config data. This will only succeed if we are in an idle
                    // state. But that should be the case if we're receiving RFC's
                    ReloadCalDataFromDevice();
                    ClearBatteryVolts();
                    ClearTempComp();

                    m_devID = sIDInRFC;
                }
            }

            ConvertRFCPktToGeneric( m_lastRFC.rfcPkt, pktHdr, pktData );
            LogRawRFCPacket( pktHdr, m_lastRFC.rfcPkt );

            bRFCPending = true;

            // We throw away a certain number of packets when stopping, since
            // these can contain junk data that will throw off the averaging
            if( m_streamDiscardCount > 0 )
            {
                m_streamDiscardCount--;
            }
            else
            {
                // After the TesTORK has gone to sleep, after wakeup it is
                // possible for the first few RFC values to be invalid.
                // Discard those.
                if( ( m_lastRFC.rfcPkt.tension000 == 0x7FFFFF )
                      &&
                    ( m_lastRFC.rfcPkt.tension090 == 0x7FFFFF )
                      &&
                    ( m_lastRFC.rfcPkt.tension180 == 0x7FFFFF )
                      &&
                    ( m_lastRFC.rfcPkt.tension270 == 0x7FFFFF )
                  )
                {
                    // Invalid data - do not average
                }
                else
                {
                    // Valid data - okay to average
                    UpdateTempComp( pktHdr );
                    CreateCalibratedReading( m_lastRFC.rfcPkt, pktHdr );
                    UpdateAveraging();
                    UpdateBatteryVolts();

                    if( m_onRFCRcvd )
                        m_onRFCRcvd( this );
                }
            }

            // After the above is done, note that we've received an RFC
            m_lastRFC.havePrevRFC      = true;
            m_lastRFC.prevRFCTimestamp = pktHdr.timeStamp;
        }

        // After all the above processing is done, save this packet's header
        m_lastPktHdr = pktHdr;
    }

    // If a RFC is pending, check that the time params are up to date. If they
    // are not, a command will be sent to update those first.
    if( bRFCPending )
        bRFCPending = TimeParamsGood();

    // Now update the state machine
    switch( m_linkState )
    {
        case LS_IDLE:
            // If we have a RFC timeout, could have lost the link. Could also
            // scale back RFC interval based on the last time we got a command
            // from client modules.
            /* to do */
            break;

        case LS_HUNTING:
            // We are currently waiting to receive a command from the sub.
            // Packet reception is captured in receive processing above,
            // so there is nothing to do here.
            break;

        case LS_CFG_DOWNLOAD:
            // We are downloading the calibration table. Check if we are
            // done - switch to idle if so.
            if( bRFCPending )
            {
                bool bNeedPages = false;

                // Loop until we send a config request or until we know we have
                // all config pages
                while( true )
                {
                    // Scan the table for any pending entries. If there is
                    // one, request it. Otherwise, switch to the idle state.
                    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
                    {
                        if( m_cfgPgStatus[iPg].havePage == false )
                        {
                            // We still need a page
                            bNeedPages = true;

                            // Send a request for this page if we haven't already
                            // done so in this page request cycle
                            if ( m_cfgPgStatus[iPg].lastCfgReqCycle < m_currCfgReqCycle )
                            {
                                TESTORK_DATA_UNION pktPayload;
                                pktPayload.cfgRequest.pageNbr = iPg;

                                SendSubCommand( SCT_GET_CFG_PAGE, &pktPayload );

                                m_cfgPgStatus[iPg].lastCfgReqCycle = m_currCfgReqCycle;
                                bRFCPending = false;
                                break;
                            }
                        }
                    }

                    // Check if we've sent a request
                    if( !bRFCPending )
                        break;

                    // Check if we have all pages
                    if( !bNeedPages )
                        break;

                    // Step the page request cycle and start through the loop again.
                    // This will allow us to re-sent requests for pages that have
                    // not yet been received for some reason.
                    m_currCfgReqCycle += 1;
                }

                // If the RFC is pending, it means all pages have been downloaded.
                // Send the 'get version' command, and also put the link into the
                // idle state
                if( bRFCPending )
                {
                    // Calibration data all downloaded. Check the version of the
                    // cal data, and update if necessary.
                    CheckCalDataVersion();

                    // Send a request to get the verison info from the WTTTS.
                    // We don't implement a state for this, as the response
                    // consists of a single packet only.
                    SendSubCommand( SCT_REQUEST_VER_INFO );
                    m_linkState = LS_IDLE;

#if DISCARD_RFU_ON_POWER_UP
                    // On first power up, the TesTORK will have had its sensors
                    // turned off. Skip some extra RFC packets to allow the sensors
                    // to power up
                    m_streamDiscardCount = 6;
#endif

                    // Also slow down the RFC rate
                    m_timeParams.rfcRate    = StandbyRFCInterval;
                    m_timeParams.rfcTimeout = StandbyRFCTimeout;
                }
            }
            break;

        case LS_CFG_UPLOAD:
            // We are uploading the calibration table. Check if we are
            // done - switch to idle if so.
            if( bRFCPending )
            {
                // Scan the table for any pending entries. If there is
                // one, request it. Otherwise, switch to the idle state.
                for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
                {
                    if( m_cfgPgStatus[iPg].setPending )
                    {
                        TESTORK_DATA_UNION pktPayload;

                        pktPayload.cfgData.pageNbr = iPg;
                        pktPayload.cfgData.result  = 0;

                        int   pageOffset = iPg * NBR_BYTES_PER_WTTTS_PAGE;
                        BYTE* pgPointer  = &( m_rawCfgData[pageOffset] );

                        memcpy( pktPayload.cfgData.pageData, pgPointer, NBR_BYTES_PER_WTTTS_PAGE );

                        SendSubCommand( SCT_SET_CFG_PAGE, &pktPayload );

                        bRFCPending = false;

                        break;
                    }
                }

                // If the RFC is pending, it means all pages have been set. Switch
                // to the idle state.
                if( bRFCPending )
                {
                    m_linkState = LS_IDLE;

                    // Also slow down the RFC rate
                    m_timeParams.rfcRate    = StandbyRFCInterval;
                    m_timeParams.rfcTimeout = StandbyRFCTimeout;
                }
            }
            break;

        case LS_STARTING:
            // When starting, on each RFC send a start streaming command
            if( bRFCPending )
            {
                SendSubCommand( SCT_START_DATA_COLLECTION );
                m_lastStreamStartTime = time( NULL );
            }
            break;

        case LS_STOPPING:
            // If we've received a RFC, then the streaming data has stopped
            if( bRFCPending )
                m_linkState = LS_IDLE;

            break;

        case LS_STREAMING:
            // Nothing extra needs to be done in this state
            break;
    }

    // If at this point the RFC is still pending, send a 'no command' response
    if( bRFCPending )
        SendSubCommand( SCT_NO_COMMAND );

    // Check for data loss. If we have a timeout, must revert to the hunting state.
    // If we are here, data loss won't be because we lost the port. Rather, the
    // TesTORK must have gone offline.
    if( !DeviceIsRecving )
    {
        // Do the same work here as we do on in the Connect() method,
        // except for the actual opening of the comm port.
        m_linkState = LS_HUNTING;

        m_streamDiscardCount = 2;

        ClearConfigurationData();
        ClearHardwareInfo();

        // Set the initial timing parameters so that the configuration download
        // happens quickly.
        m_timeParams.rfcRate       = StartupRFCInterval;
        m_timeParams.rfcTimeout    = StartupRFCTimeout;
        m_timeParams.streamTimeout = StreamTimeoutInterval;
    }

    // Finally, data values are only valid in certain states. In any other
    // state the averaged data values must be reset. Don't update the averages
    // here (that happens above with the reception of each packet). Only
    // reset the averaging if required.
    switch( m_linkState )
    {
        case LS_IDLE:
        case LS_STARTING:
        case LS_STOPPING:
        case LS_STREAMING:
            // UpdateAveraging() would have been called as each packet
            // is received - don't need to do anything.
            break;

        default:
            // In any other state, data from the tool is not valid.
            // Reset the data averages
            m_avgTorque->Reset();
            m_avgTension->Reset();
            break;
    }

    return true;
}


typedef enum {
    SI_DEV_STATE,
    SI_LAST_RFC_TIME,
    SI_RFC_TYPE,
    SI_TEMPERATURE,
    SI_BATT_VOLTS,
    SI_BATT_USED,
    SI_BATT_TYPE,
    SI_PDS_READING,
    SI_RPM,
    SI_LAST_RESET,
    SI_RF_CHAN,
    SI_CURR_MODE,
    SI_RFC_RATE,
    SI_RFC_TIMEOUT,
    SI_STREAM_RATE,
    SI_STREAM_TIMEOUT,
    SI_PAIR_TIMEOUT,
    SI_TORQUE_045,
    SI_TORQUE_225,
    SI_TORQUE_045_R3,
    SI_TORQUE_225_R3,
    SI_TENSION_000,
    SI_TENSION_090,
    SI_TENSION_180,
    SI_TENSION_270,
    SI_COMPASS_X,
    SI_COMPASS_Y,
    SI_COMPASS_Z,
    SI_ACCEL_X,
    SI_ACCEL_Y,
    SI_ACCEL_Z,
    SI_RTD_READING,
    SI_DEV_ID,
    SI_TC_COMP_ON,
    SI_TC_RTD_ALPHA,
    SI_TC_RTD_MAXVAR,
    SI_TC_MPOS_THRESH,
    SI_TC_MPOS_FACTOR,
    SI_TC_MNEG_THRESH,
    SI_TC_MNEG_FACTOR,
    SI_TC_SLOPE_COMP,
    SI_TC_SLOPE_ALPHA,
    SI_TC_SLOPE_MAXVAR,
    SI_TC_SLOPE_FACTOR,
    NBR_STATUS_ITEMS
} STATUS_ITEM;

static const String statusCaption[NBR_STATUS_ITEMS] = {
    "Link State",
    "Last RFC",
    "RFC Type",
    "Temperature",
    "Battery Volts",
    "Battery Used",
    "Battery Type",
    "PDS Reading",
    "RPM",
    "Last Reset",
    "RF Channel",
    "Current Mode",
    "RFC Rate",
    "RFC Timeout",
    "Stream Rate",
    "Stream Timeout",
    "Pairing Timeout",
    "Torque 045",
    "Torque 225",
    "Torque 045 (R3)",
    "Torque 225 (R3)",
    "Tension 000",
    "Tension 090",
    "Tension 180",
    "Tension 270",
    "Compass X",
    "Compass Y",
    "Compass Z",
    "Accel X",
    "Accel Y",
    "Accel Z",
    "RTD Temp",
    "Device ID",
    "TC Comp On",
    "TC RTD Alpha",
    "TC RTD Max Var",
    "TC M+ Thresh",
    "TC M+ Factor",
    "TC M- Thresh",
    "TC M- Factor",
    "TC Slope Comp",
    "TC Slope Alpha",
    "TC Slope Max Var",
    "TC Slope Factor",
};


bool TWTTTSSub::ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, statusCaption, NBR_STATUS_ITEMS ) )
        return false;

    // Always show the device state. Show simulation differently though
    if( ( m_port.portType == CT_UDP ) && ( m_linkState == LS_IDLE ) )
        pValues->Strings[SI_DEV_STATE] = "Simulation (Save Disabled)";
    else
        pValues->Strings[SI_DEV_STATE] = GetDevStatus();

    // Make sure we have a RFC packet
    if( m_lastRFC.tRFCTime == 0 )
        return true;

    // Have a RFC - populate the value string list
    pValues->Strings[SI_LAST_RFC_TIME]  = LocalTimeString( m_lastRFC.tRFCTime );
    pValues->Strings[SI_TEMPERATURE]    = FloatToStrF( m_lastRFC.rfcPkt.fTemperature, ffFixed, 7, 1 );
    pValues->Strings[SI_BATT_VOLTS]     = FloatToStrF( m_batteryLevel.avgBattVolts->AverageValue, ffFixed, 7, 2 );
    pValues->Strings[SI_BATT_USED]      = IntToStr( (int)( m_batteryLevel.avgBattUsed->AverageValue ) );
    pValues->Strings[SI_RFC_RATE]       = IntToStr( m_lastRFC.rfcPkt.rfcRate )        + " msecs";
    pValues->Strings[SI_RFC_TIMEOUT]    = IntToStr( m_lastRFC.rfcPkt.rfcTimeout )     + " msecs";
    pValues->Strings[SI_STREAM_RATE]    = IntToStr( m_lastRFC.rfcPkt.streamRate )     + " msecs";
    pValues->Strings[SI_STREAM_TIMEOUT] = IntToStr( m_lastRFC.rfcPkt.streamTimeout )  + " secs";
    pValues->Strings[SI_PAIR_TIMEOUT]   = IntToStr( m_lastRFC.rfcPkt.pairingTimeout ) + " secs";
    pValues->Strings[SI_BATT_TYPE]      = GetBattTypeText( (BATTERY_TYPE)m_lastRFC.rfcPkt.battType );

    switch( m_lastRFC.rfcPkt.lastReset )
    {
        case 1:   pValues->Strings[SI_LAST_RESET] = "Power-up";   break;
        case 2:   pValues->Strings[SI_LAST_RESET] = "WDT";        break;
        case 3:   pValues->Strings[SI_LAST_RESET] = "Brown out";  break;
        default:  pValues->Strings[SI_LAST_RESET] = "Type " + IntToStr( m_lastRFC.rfcPkt.lastReset );  break;
    }

    switch( m_lastRFC.rfcPkt.currMode )
    {
        case 0:   pValues->Strings[SI_CURR_MODE] = "Entering deep sleep";  break;
        case 1:   pValues->Strings[SI_CURR_MODE] = "Normal";               break;
        case 2:   pValues->Strings[SI_CURR_MODE] = "Low power";            break;
        default:  pValues->Strings[SI_CURR_MODE] = "Type " + IntToStr( m_lastRFC.rfcPkt.lastReset );  break;
    }

    pValues->Strings[SI_PDS_READING] = IntToStr( m_lastRFC.rfcPkt.pdsSwitch );
    pValues->Strings[SI_RPM]         = IntToStr( m_lastRFC.rfcPkt.rpm );
    pValues->Strings[SI_RF_CHAN]     = IntToStr( m_lastRFC.rfcPkt.rfChannel );

    pValues->Strings[SI_TORQUE_045]  = IntToStr( m_lastRFC.rfcPkt.torque045 );
    pValues->Strings[SI_TORQUE_225]  = IntToStr( m_lastRFC.rfcPkt.torque225 );
    pValues->Strings[SI_TENSION_000] = IntToStr( m_lastRFC.rfcPkt.tension000 );
    pValues->Strings[SI_TENSION_090] = IntToStr( m_lastRFC.rfcPkt.tension090 );
    pValues->Strings[SI_TENSION_180] = IntToStr( m_lastRFC.rfcPkt.tension180 );
    pValues->Strings[SI_TENSION_270] = IntToStr( m_lastRFC.rfcPkt.tension270 );

    pValues->Strings[SI_COMPASS_X] = IntToStr( m_lastRFC.rfcPkt.compassX );
    pValues->Strings[SI_COMPASS_Y] = IntToStr( m_lastRFC.rfcPkt.compassY );
    pValues->Strings[SI_COMPASS_Z] = IntToStr( m_lastRFC.rfcPkt.compassZ );
    pValues->Strings[SI_ACCEL_X]   = IntToStr( m_lastRFC.rfcPkt.accelX );
    pValues->Strings[SI_ACCEL_Y]   = IntToStr( m_lastRFC.rfcPkt.accelY );
    pValues->Strings[SI_ACCEL_Z]   = IntToStr( m_lastRFC.rfcPkt.accelZ );

    // Temp for now - may delete these
    pValues->Strings[SI_TC_RTD_ALPHA]    = FloatToStrF( m_tempComp.params.fRTDAvgAlpha,        ffFixed, 7, 3 );
    pValues->Strings[SI_TC_RTD_MAXVAR]   = FloatToStrF( m_tempComp.params.fRTDAvgMaxVar,       ffFixed, 7, 3 );
    pValues->Strings[SI_TC_MPOS_THRESH]  = FloatToStrF( m_tempComp.params.fRisingSlopeThresh,  ffFixed, 7, 3 );
    pValues->Strings[SI_TC_MPOS_FACTOR]  = FloatToStrF( m_tempComp.params.fRisingSlopeFactor,  ffFixed, 7, 3 );
    pValues->Strings[SI_TC_MNEG_THRESH]  = FloatToStrF( m_tempComp.params.fFallingSlopeThresh, ffFixed, 7, 3 );
    pValues->Strings[SI_TC_MNEG_FACTOR]  = FloatToStrF( m_tempComp.params.fFallingSlopeFactor, ffFixed, 7, 3 );
    pValues->Strings[SI_TC_SLOPE_ALPHA]  = FloatToStrF( m_tempComp.params.fRTDSlopeAlpha,      ffFixed, 7, 3 );
    pValues->Strings[SI_TC_SLOPE_MAXVAR] = FloatToStrF( m_tempComp.params.fRTDSlopeMaxVar,     ffFixed, 7, 3 );
    pValues->Strings[SI_TC_SLOPE_FACTOR] = FloatToStrF( m_tempComp.params.fRTDSlopeFactor,     ffFixed, 7, 3 );
    pValues->Strings[SI_TC_COMP_ON]      = m_tempComp.params.bApplyTempComp     ? String( "Yes" ) : String( "No" );
    pValues->Strings[SI_TC_SLOPE_COMP]   = m_tempComp.params.bApplyRTDSlopeComp ? String( "Yes" ) : String( "No" );

    // The following fields are only present in a V2 packet
    if( m_lastRFC.rfcPkt.rfcV2DataPresent )
    {
        pValues->Strings[SI_RFC_TYPE] = "V2";

        pValues->Strings[SI_TORQUE_045_R3] = IntToStr( m_lastRFC.rfcPkt.torque045_R3 );
        pValues->Strings[SI_TORQUE_225_R3] = IntToStr( m_lastRFC.rfcPkt.torque225_R3 );
        pValues->Strings[SI_DEV_ID]        = m_devID;

        // An RTD reading of 0 means it is not connected
        if( m_lastRFC.rfcPkt.rtdReading == 0 )
            pValues->Strings[SI_RTD_READING] = "Disconnected";
        else
            pValues->Strings[SI_RTD_READING] = IntToStr( m_lastRFC.rfcPkt.rtdReading );
    }
    else
    {
        pValues->Strings[SI_RFC_TYPE] = "V1";

        pValues->Strings[SI_TORQUE_045_R3] = EmptyValue;
        pValues->Strings[SI_TORQUE_225_R3] = EmptyValue;
        pValues->Strings[SI_RTD_READING]   = EmptyValue;
        pValues->Strings[SI_DEV_ID]        = EmptyValue;
    }

    return true;
}


bool TWTTTSSub::GetLastStatusPkt( time_t& tLastPkt, DEVICE_RAW_READING& lastPkt )
{
    memset( &lastPkt, 0, sizeof( DEVICE_RAW_READING ) );

    tLastPkt = m_lastRFC.tRFCTime;

    lastPkt.torque045    = m_lastRFC.rfcPkt.torque045;
    lastPkt.torque225    = m_lastRFC.rfcPkt.torque225;
    lastPkt.torque045_R3 = m_lastRFC.rfcPkt.torque045_R3;
    lastPkt.torque225_R3 = m_lastRFC.rfcPkt.torque225_R3;
    lastPkt.tension000   = m_lastRFC.rfcPkt.tension000;
    lastPkt.tension090   = m_lastRFC.rfcPkt.tension090;
    lastPkt.tension180   = m_lastRFC.rfcPkt.tension180;
    lastPkt.tension270   = m_lastRFC.rfcPkt.tension270;
    lastPkt.compassX     = m_lastRFC.rfcPkt.compassX;
    lastPkt.compassY     = m_lastRFC.rfcPkt.compassY;
    lastPkt.compassZ     = m_lastRFC.rfcPkt.compassZ;
    lastPkt.accelX       = m_lastRFC.rfcPkt.accelX;
    lastPkt.accelY       = m_lastRFC.rfcPkt.accelY;
    lastPkt.accelZ       = m_lastRFC.rfcPkt.accelZ;

    return true;
}


bool TWTTTSSub::GetLastDataRaw( DWORD& msecs, BYTE& seqNbr, DEVICE_RAW_READING& lastPkt )
{
    memset( &lastPkt, 0, sizeof( DEVICE_RAW_READING ) );

    lastPkt = m_devRawReading;

    msecs  = m_msecsInMode;
    seqNbr = m_lastPktHdr.seqNbr;

    return true;
}


bool TWTTTSSub::GetLastDataCal( DWORD& msecs, BYTE& seqNbr, DEVICE_CAL_READING& lastPkt )
{
    memset( &lastPkt, 0, sizeof( DEVICE_CAL_READING ) );

    lastPkt = m_devCalReading;

    msecs  = m_msecsInMode;
    seqNbr = m_lastPktHdr.seqNbr;

    return true;
}


bool TWTTTSSub::GetLastAvgData( float& avgTorque, float& avgTension, int& avgRPM )
{
    // In some link states, we won't have average data to report.
    // Check for that first.
    switch( m_linkState )
    {
        case LS_CLOSED:
        case LS_HUNTING:
        case LS_CFG_DOWNLOAD:
        case LS_CFG_UPLOAD:
            // Cannot report average values in these states
            m_avgTension->Reset();
            m_avgTorque->Reset();
            m_avgRPM->Reset();
            break;
    }

    return TSubDevice::GetLastAvgData( avgTorque, avgTension, avgRPM );
}


void TWTTTSSub::InitWTTTSDataCmdPkt( SUB_CMD_TYPE cmdType )
{
    // Clear the memory area first
    memset( &( m_cachedCmds[cmdType] ), 0, sizeof( CACHED_CMD_PKT ) );

    // Determine the packet type byte, and payload length (if any)
    BYTE* byBuff = m_cachedCmds[cmdType].byTxData;

    WTTTS_PKT_HDR* pHdr  = (WTTTS_PKT_HDR*)byBuff;

    // Setup header defaults
    pHdr->pktHdr    = TESTORK_HDR_TX;
    pHdr->pktType   = 0;
    pHdr->seqNbr    = 0;
    pHdr->timeStamp = 0;
    pHdr->dataLen   = 0;

    // Now initialize specific commands
    switch( cmdType )
    {
        case SCT_NO_COMMAND:
            pHdr->pktType = WTTTS_CMD_NO_CMD;
            pHdr->dataLen = 0;
            break;

        case SCT_START_DATA_COLLECTION:
            pHdr->pktType = WTTTS_CMD_START_STREAM;
            pHdr->dataLen = 0;
            break;

        case SCT_STOP_DATA_COLLECTION:
            pHdr->pktType = WTTTS_CMD_STOP_STREAM;
            pHdr->dataLen = 0;
            break;

        case SCT_REQUEST_VER_INFO:
            pHdr->pktType = WTTTS_CMD_QUERY_VER;
            pHdr->dataLen = 0;
            break;

        case SCT_GET_CFG_PAGE:
            pHdr->pktType = WTTTS_CMD_GET_CFG_DATA;
            pHdr->dataLen = sizeof( WTTTS_CFG_ITEM );
            break;

        case SCT_SET_CFG_PAGE:
            pHdr->pktType = WTTTS_CMD_SET_CFG_DATA;
            pHdr->dataLen = sizeof( WTTTS_CFG_DATA );
            break;

        case SCT_SET_RATE_CMD:
            pHdr->pktType = WTTTS_CMD_SET_RATE;
            pHdr->dataLen = sizeof( WTTTS_RATE_PKT );
            break;

        case SCT_ENTER_DEEP_SLEEP:
            pHdr->pktType = WTTTS_CMD_ENTER_DEEP_SLEEP;
            pHdr->dataLen = 0;
            break;

        case SCT_SET_RF_CHANNEL:
            pHdr->pktType = WTTTS_CMD_SET_RF_CHANNEL;
            pHdr->dataLen = sizeof( WTTTS_SET_CHAN_PKT );
            break;

        default:
            // Don't know this command!
            return;
    }

    // If we don't need a payload, can finish initializing the command.
    // Otherwise, we have to finish the command each time it is sent.
    if( pHdr->dataLen == 0 )
    {
        m_cachedCmds[cmdType].needsPayload = false;
        m_cachedCmds[cmdType].cmdLen       = MIN_TESTORK_PKT_LEN;

        BYTE byCRC = CalcWTTTSChecksum( byBuff, sizeof( WTTTS_PKT_HDR ) );

        byBuff[ sizeof( WTTTS_PKT_HDR ) ] = byCRC;
    }
    else
    {
        m_cachedCmds[cmdType].needsPayload = true;
        m_cachedCmds[cmdType].cmdLen       = MIN_TESTORK_PKT_LEN + pHdr->dataLen;
    }
}


bool TWTTTSSub::SendSubCommand( SUB_CMD_TYPE aCmd, const TESTORK_DATA_UNION* pPayload )
{
    if( !IsConnected )
        return false;

    // If the command needs a payload, add it on
    if( m_cachedCmds[aCmd].needsPayload )
    {
        if( pPayload == NULL )
            return false;

        BYTE* txBuff   = m_cachedCmds[aCmd].byTxData;
        int   crcCount = m_cachedCmds[aCmd].cmdLen - 1;

        TESTORK_DATA_UNION* pData = (TESTORK_DATA_UNION*)&( txBuff[ sizeof( WTTTS_PKT_HDR ) ] );

        memcpy( pData, pPayload, sizeof( TESTORK_DATA_UNION ) );

        txBuff[crcCount] = CalcWTTTSChecksum( txBuff, crcCount );
    }

    BYTE* pBuff       = m_cachedCmds[aCmd].byTxData;
    DWORD bytesToSend = m_cachedCmds[aCmd].cmdLen;

    DWORD bytesSent;

    if( m_bUsingUDP )
        bytesSent = ((CUDPPort*)m_port.portObj)->SendTo( pBuff, bytesToSend, m_sTTDevAddr, m_wTTDevPort );
    else
        bytesSent = m_port.portObj->CommSend( pBuff, bytesToSend );

    IncStat( m_port.stats.cmdsSent );
    IncStat( m_port.stats.bytesSent, bytesSent );

    return( bytesSent == bytesToSend );
}


String TWTTTSSub::GetDevStatus( void )
{
    switch( m_linkState )
    {
        case LS_CLOSED:        return "Port closed";
        case LS_HUNTING:       return "Hunting";
        case LS_CFG_DOWNLOAD:  return "Downloading cfg data";
        case LS_CFG_UPLOAD:    return "Uploading cfg data";
        case LS_IDLE:          return "Idle";
        case LS_STARTING:      return "Starting streaming";
        case LS_STREAMING:     return "Streaming";
        case LS_STOPPING:      return "Stopping streaming";
    }

    // Fall through means unknown state!
    return "State " + IntToStr( m_linkState );
}


String TWTTTSSub::GetDevHousingSN( void )
{
    // Check to see if we have a valid Housing SN in the manufacturing area
    WTTTS_HOUSING_SN_STRUCT* pSNStruct = (WTTTS_HOUSING_SN_STRUCT*)&( m_rawCfgData[ HOUSING_SN_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    if( ( pSNStruct->byHousingSN[0] == 0xFF ) || ( pSNStruct->byHousingSN[0] == 0x00 ) )
        return "";

    // Could have what appears to be a valid SN. Copy byte for byte until either a
    // 0xFF or 0x00 is encountered
    const int MaxHousingSNLen = NBR_BYTES_PER_WTTTS_PAGE * HOUSING_SN_STRUCT_PAGES;

    // Overallocate a temp array by one byte to be sure the string is null terminated.
    char szTemp[MaxHousingSNLen + 1] = { 0 };

    for( int iByte = 0; iByte < MaxHousingSNLen; iByte++ )
    {
        if( ( pSNStruct->byHousingSN[iByte] == 0xFF ) || ( pSNStruct->byHousingSN[iByte] == 0x00 ) )
            break;

        szTemp[iByte] = pSNStruct->byHousingSN[iByte];
    }

    return szTemp;
}


bool TWTTTSSub::GetCanStart( void )
{
    // The WTTTS device can start only if it is in the idle state,
    // or if it has already started
    switch( m_linkState )
    {
        case LS_IDLE:
        case LS_STARTING:
        case LS_STREAMING:
            return true;
    }

    // Fall through means we can't currently start
    return false;
}


bool TWTTTSSub::GetDevIsIdle( void )
{
    return( m_linkState == LS_IDLE );
}


bool TWTTTSSub::GetDevIsStreaming( void )
{
    return( m_linkState == LS_STREAMING );
}


bool TWTTTSSub::GetDevIsCalibrated( void )
{
    // Return true if all cal pages have been downloaded
    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        if( !m_cfgPgStatus[iPg].havePage )
            return false;
    }

    // Fall through means we have config data
    return true;
}


int TWTTTSSub::GetConfigUploadProgress( void )
{
    // Config upload progress is based on the host-writable pages (not all pages)
    int iTotalHostPgs = NBR_WTTTS_CFG_PAGES - WTTTS_CFG_FIRST_HOST_PAGE;

    int iPgsPending = 0;

    for( int iPg = WTTTS_CFG_FIRST_HOST_PAGE; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        if( m_cfgPgStatus[iPg].setPending )
            iPgsPending++;
    }

    return 100 - 100 * iPgsPending / iTotalHostPgs;
}


void TWTTTSSub::UpdateAveraging( void )
{
    // Get the call data, as it's used in both cases
    WTTTS_CAL_DATA_STRUCT_V2_2* pCalData = (WTTTS_CAL_DATA_STRUCT_V2_2*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    // Update the averaging for tension and torque values. In the idle
    // state, use RFC values. In the streaming state, use the streaming
    // data.
    if( ( m_linkState == LS_IDLE ) || ( m_linkState == LS_STARTING ) )
    {
        // Calculate the avg calibrated torque and tension values
        float avgTorque  = ( m_rfcCalReading.calTorque045 + m_rfcCalReading.calTorque225 ) / 2.0;
        float avgTension = ( m_rfcCalReading.calTension000 + m_rfcCalReading.calTension090 + m_rfcCalReading.calTension180 + m_rfcCalReading.calTension270 ) / 4.0;

        // Compansate for Crosstalk
        float compdTorque  = ( avgTorque * pCalData->xTalk_TqTq  ) + ( avgTension * pCalData->xTalk_TqTen );
        float compdTension = ( avgTorque * pCalData->xTalk_TenTq ) + ( avgTension * pCalData->xTalk_TenTen );

        // Add average values to queue
        m_avgTorque->AddValue( compdTorque );
        m_avgTension->AddValue( compdTension );
        m_avgRPM->AddValue( m_lastRFC.rfcPkt.rpm );

        // Populate WitsFieldValues Array based on RFC data
        // <TODO> Send all field values!
        m_WitsFieldValues[eWF_DataSource]     .printf( L"%d", TSubDevice::eWPT_RFC );
        m_WitsFieldValues[eWF_RPM]            .printf( L"0.0" );  // FLOAT
        m_WitsFieldValues[eWF_Torque045]      .printf( L"%d", m_rfcCalReading.calTorque045 );
        m_WitsFieldValues[eWF_Torque225]      .printf( L"%d", m_rfcCalReading.calTorque225 );
        m_WitsFieldValues[eWF_Tension000]     .printf( L"%d", m_rfcCalReading.calTension000 );
        m_WitsFieldValues[eWF_Tension090]     .printf( L"%d", m_rfcCalReading.calTension090 );
        m_WitsFieldValues[eWF_Tension180]     .printf( L"%d", m_rfcCalReading.calTension180 );
        m_WitsFieldValues[eWF_Tension270]     .printf( L"%d", m_rfcCalReading.calTension270 );
        m_WitsFieldValues[eWF_CompassX]       .printf( L"0.0" );
        m_WitsFieldValues[eWF_CompassY]       .printf( L"0.0" );
        m_WitsFieldValues[eWF_CompassZ]       .printf( L"0.0" );
        m_WitsFieldValues[eWF_AccelX]         .printf( L"0.0" );
        m_WitsFieldValues[eWF_AccelY]         .printf( L"0.0" );
        m_WitsFieldValues[eWF_AccelZ]         .printf( L"0.0" );
        m_WitsFieldValues[eWF_BattLevel]      .printf( L"0.0" );  // FLOAT
        m_WitsFieldValues[eWF_PktHdrSeqNbr]   .printf( L"%d", m_lastPktHdr.seqNbr );
        m_WitsFieldValues[eWF_PktHdrTimestamp].printf( L"%d", m_lastPktHdr.timeStamp );
    }
    else if( ( m_linkState == LS_STREAMING ) || ( m_linkState == LS_STOPPING ) )
    {
        // We are receiving streaming data - report the last calibrated values
        // from that packet. Note that temp comp is applied to the calibrated
        // values at the time they are calculated
        int avgTorque  = ( m_devCalReading.torque045 + m_devCalReading.torque225 ) / 2;
        int avgTension = ( m_devCalReading.tension000 + m_devCalReading.tension090 + m_devCalReading.tension180 + m_devCalReading.tension270 ) / 4;

        // Calculate xTalk Comp values
        float compdTorque  = ( avgTorque * pCalData->xTalk_TqTq  ) + ( avgTension * pCalData->xTalk_TqTen );
        float compdTension = ( avgTorque * pCalData->xTalk_TenTq ) + ( avgTension * pCalData->xTalk_TenTen );

        m_avgTorque->AddValue( compdTorque );
        m_avgTension->AddValue( compdTension );
        m_avgRPM->AddValue( m_currReading.rpm );

        // Populate WitsFieldValues Array based on Stream data
        // <TODO> Send all field values!
        m_WitsFieldValues[TSubDevice::eWF_DataSource]     .printf( L"%.2f", TSubDevice::eWPT_Stream );
        m_WitsFieldValues[TSubDevice::eWF_RPM]            .printf( L"0.0" );
        m_WitsFieldValues[TSubDevice::eWF_Torque045]      .printf( L"%.2f", m_devCalReading.torque045 );
        m_WitsFieldValues[TSubDevice::eWF_Torque225]      .printf( L"%.2f", m_devCalReading.torque225 );
        m_WitsFieldValues[TSubDevice::eWF_Tension000]     .printf( L"%.2f", m_devCalReading.tension000 );
        m_WitsFieldValues[TSubDevice::eWF_Tension090]     .printf( L"%.2f", m_devCalReading.tension090 );
        m_WitsFieldValues[TSubDevice::eWF_Tension180]     .printf( L"%.2f", m_devCalReading.tension180 );
        m_WitsFieldValues[TSubDevice::eWF_Tension270]     .printf( L"%.2f", m_devCalReading.tension270 );
        m_WitsFieldValues[TSubDevice::eWF_CompassX]       .printf( L"0.0" );
        m_WitsFieldValues[TSubDevice::eWF_CompassY]       .printf( L"0.0" );
        m_WitsFieldValues[TSubDevice::eWF_CompassZ]       .printf( L"0.0" );
        m_WitsFieldValues[TSubDevice::eWF_AccelX]         .printf( L"0.0" );
        m_WitsFieldValues[TSubDevice::eWF_AccelY]         .printf( L"0.0" );
        m_WitsFieldValues[TSubDevice::eWF_AccelZ]         .printf( L"0.0" );
        m_WitsFieldValues[TSubDevice::eWF_BattLevel]      .printf( L"0.0" );
        m_WitsFieldValues[TSubDevice::eWF_PktHdrSeqNbr]   .printf( L"%d",   m_lastPktHdr.seqNbr );
        m_WitsFieldValues[TSubDevice::eWF_PktHdrTimestamp].printf( L"%.2f", m_lastPktHdr.timeStamp );
    }
}


void TWTTTSSub::ClearTempComp( void )
{
    m_tempComp.avgRTD->Reset();
    m_tempComp.avgRTDSlope->Reset();

    m_tempComp.tLastRFCPkt      = 0;
    m_tempComp.lastRFCSeqNbr    = 0;
    m_tempComp.lastRFCTimeStamp = 0;
    m_tempComp.iLastRTDValue    = 0;
    m_tempComp.fLastRTDROC      = 0.0;    // First order: RTD rate of change
    m_tempComp.fLastRTDSlopeROC = 0.0;    // Second order: RTD slope rate of change
    m_tempComp.fInitialOffset   = 0.0;    // First order offset calculation
    m_tempComp.fTotalOffset     = 0.0;    // First order + second order offset calc
}


// Uncomment the following line to show intermediate temp comp vars
// in the debug messages window
//#define SHOW_TC_VARS_IN_DEBUG_WINDOW

void TWTTTSSub::UpdateTempComp( const WTTTS_PKT_HDR& pktHdr )
{
    // Only update RTD averaging if we are in the idle state
    if( m_linkState != LS_IDLE )
        return;

    // If the temp comp average has not yet been given a value,
    // add the current value now and exit
    if( !m_tempComp.avgRTD->IsValid )
    {
        m_tempComp.avgRTD->AddValue( m_lastRFC.rfcPkt.rtdReading );

        m_tempComp.tLastRFCPkt      = time( NULL );
        m_tempComp.lastRFCSeqNbr    = pktHdr.seqNbr;
        m_tempComp.lastRFCTimeStamp = pktHdr.timeStamp;
        m_tempComp.iLastRTDValue    = m_lastRFC.rfcPkt.rtdReading;
        m_tempComp.fLastRTDROC      = 0.0;
        m_tempComp.fLastRTDSlopeROC = 0.0;
        m_tempComp.fInitialOffset   = 0.0;
        m_tempComp.fTotalOffset     = 0.0;

        #ifdef SHOW_TC_VARS_IN_DEBUG_WINDOW
            /* Debug */
            String sInfo;
            sInfo.printf( L"TC Avg Invalid: seq %d timestamp %d time %d lastRTD %d RTDAvg %f RTDmAvg %f RTDROC %f mRTDROC %f initoff %f finaloff %f",
                          m_tempComp.lastRFCSeqNbr, m_tempComp.lastRFCTimeStamp, m_tempComp.tLastRFCPkt,
                          m_tempComp.iLastRTDValue, m_tempComp.avgRTD->AverageValue, m_tempComp.avgRTDSlope->AverageValue,
                          m_tempComp.fLastRTDROC, m_tempComp.fLastRTDSlopeROC, m_tempComp.fInitialOffset, m_tempComp.fTotalOffset );

            OutputDebugString( sInfo.w_str() );
        #endif

        return;
    }

    // It is possible for duplicate RFC packets to be received. If
    // this is a duplicate, ignore it
    if( ( pktHdr.seqNbr == m_tempComp.lastRFCSeqNbr ) && ( pktHdr.timeStamp == m_tempComp.lastRFCTimeStamp ) )
        return;

    // The temp comp has two implicit assumptions:
    //
    //   1. The slope factors implicitly depending on RFC values being received
    //      once per second. If the RFC values come in slower or faster, then
    //      the temp comp factors will have to be adjusted.
    //
    //   2. The slope factors are implicitly tied to the tension span. So while
    //      a set of temp comp factors may work well for one TesTORK, them may
    //      not for another TesTORK whose tension gauges have a different span.
    //      This could be dealt with by specifying temp comp factors to work
    //      with a nominal tension span of 1, and then normalizing the factors
    //      after the calibration values have been downloaded from the TesTORK.

    int iEndRTDVal = m_lastRFC.rfcPkt.rtdReading;

    // Step one: determine the amount of elapsed time. The timestamp member in
    // the pkt header is a word-sized var representing 10's of msecs. If more
    // time in seconds has elapsed than that var can represent, use the elapsed
    // time in seconds as our basis
    const int MaxHdrTimestampDuration = 0x00010000 * 10 /*msecs per tick*/;

    DWORD dwElapsedMsecs = 0;

    if( time( NULL ) - m_tempComp.tLastRFCPkt >= MaxHdrTimestampDuration )
    {
        dwElapsedMsecs = ( time( NULL ) - m_tempComp.tLastRFCPkt ) * 1000 /*msecs/sec*/;
    }
    else
    {
        // Elapsed time is such that packet header timestamps can be used.
        if( pktHdr.timeStamp > m_tempComp.lastRFCTimeStamp )
            dwElapsedMsecs = ( pktHdr.timeStamp - m_tempComp.lastRFCTimeStamp ) * 10 /*msecs/tick*/;
        else
            dwElapsedMsecs = ( pktHdr.timeStamp + ( 0xFFFF - m_tempComp.lastRFCTimeStamp ) ) * 10 /*msecs/tick*/;
    }

#ifdef SHOW_TC_VARS_IN_DEBUG_WINDOW
    /* Debug */
    String sEntryMsg;
    sEntryMsg.printf( L"Entering TC Loop with %u elapsed msecs", dwElapsedMsecs );

    OutputDebugString( sEntryMsg.w_str() );
#endif

    // Accept an elapsed time that is 'around' one second. Skip this packet if the
    // elapsed time is less than that, and iterate multiple times through the loop
    // if greater than that.
    const DWORD MinRFCInterval =  800;  // msecs
    const DWORD MaxRFCInterval = 1200;  // msecs

    while( dwElapsedMsecs > MinRFCInterval )
    {
        // If this is our last iteration, always use the end RTD value currently
        // being reported. Otherwise, estimate an end RTD value based on the slope.
        int iCurrRTDVal;

        if( dwElapsedMsecs < MaxRFCInterval )
        {
            iCurrRTDVal = iEndRTDVal;
        }
        else
        {
            // We've been missing RFC values for some reason. Assume the temperature
            // has changed in a straight-line fashion since the last reading. The
            // reset values must be calculated as if they happened one second ago.
            // Because we are doing straight line extrapolation, the second order
            // factor will be zero (eg, the slope value was not changing)
            float fRTDSlope = (float)( iEndRTDVal - m_tempComp.iLastRTDValue ) / (float)dwElapsedMsecs;

            iCurrRTDVal = m_tempComp.iLastRTDValue + (int)( fRTDSlope * 1000.0 /*msecs*/ );
        }

        // Determine the slop average first
        float fLastRTDVal = m_tempComp.avgRTD->AverageValue;

        m_tempComp.avgRTD->AddValue( iCurrRTDVal );

        m_tempComp.fLastRTDROC = m_tempComp.avgRTD->AverageValue - fLastRTDVal;

        // Create an offset value if the sample to sample change exceeds either threshold
        if( m_tempComp.fLastRTDROC > m_tempComp.params.fRisingSlopeThresh )
            m_tempComp.fInitialOffset = m_tempComp.fLastRTDROC * m_tempComp.params.fRisingSlopeFactor;
        else if( m_tempComp.fLastRTDROC < m_tempComp.params.fFallingSlopeThresh )
            m_tempComp.fInitialOffset = m_tempComp.fLastRTDROC * m_tempComp.params.fFallingSlopeFactor;
        else
            m_tempComp.fInitialOffset = 0;

        // Apply 2nd order compenstation if enabled
        if( m_tempComp.params.bApplyRTDSlopeComp )
        {
            // Make sure the slope average is primed. If not, just the add value
            if( !m_tempComp.avgRTDSlope->IsValid )
                m_tempComp.avgRTDSlope->AddValue( m_tempComp.fLastRTDROC );

            // Calc slope of slope now
            float fLastAvgRTDSlope = m_tempComp.avgRTDSlope->AverageValue;

            m_tempComp.avgRTDSlope->AddValue( m_tempComp.fLastRTDROC );

            m_tempComp.fLastRTDSlopeROC = m_tempComp.avgRTDSlope->AverageValue - fLastAvgRTDSlope;

            if( m_tempComp.fLastRTDSlopeROC > m_tempComp.params.fRTDSlopePosThresh )
                m_tempComp.fTotalOffset = m_tempComp.fInitialOffset + m_tempComp.fLastRTDSlopeROC * m_tempComp.params.fRTDSlopeFactor;
            else if( m_tempComp.fLastRTDSlopeROC < m_tempComp.params.fRTDSlopeNegThresh )
                m_tempComp.fTotalOffset = m_tempComp.fInitialOffset + m_tempComp.fLastRTDSlopeROC * m_tempComp.params.fRTDSlopeFactor;
            else
                m_tempComp.fTotalOffset = m_tempComp.fInitialOffset;
        }
        else
        {
            // No second order comp. Total comp is just the initial comp
            m_tempComp.fLastRTDSlopeROC = 0.0;
            m_tempComp.fTotalOffset     = m_tempComp.fInitialOffset;
        }

        // Okay to update our tracking vars now
        m_tempComp.iLastRTDValue    = iCurrRTDVal;
        m_tempComp.tLastRFCPkt      = time( NULL );
        m_tempComp.lastRFCSeqNbr    = pktHdr.seqNbr;
        m_tempComp.lastRFCTimeStamp = pktHdr.timeStamp;

    #ifdef SHOW_TC_VARS_IN_DEBUG_WINDOW
        /* Debug */
        String sInfo;
        sInfo.printf( L"TC Vals: seq %d timestamp %d time %d lastRTD %d RTDAvg %f RTDmAvg %f RTDROC %f mRTDROC %f initoff %f finaloff %f",
                      m_tempComp.lastRFCSeqNbr, m_tempComp.lastRFCTimeStamp, m_tempComp.tLastRFCPkt,
                      m_tempComp.iLastRTDValue, m_tempComp.avgRTD->AverageValue, m_tempComp.avgRTDSlope->AverageValue,
                      m_tempComp.fLastRTDROC, m_tempComp.fLastRTDSlopeROC, m_tempComp.fInitialOffset, m_tempComp.fTotalOffset );

        OutputDebugString( sInfo.w_str() );
    #endif

        // Decrement elapsed time
        if( dwElapsedMsecs > 1000 )
            dwElapsedMsecs -= 1000;
        else
            dwElapsedMsecs = 0;
    }
}


void TWTTTSSub::ClearBatteryVolts( void )
{
    m_batteryLevel.avgBattVolts->Reset();
    m_batteryLevel.avgBattUsed->Reset();
}


void TWTTTSSub::UpdateBatteryVolts( void )
{
    // Assumes that an RFC packet was just received
    if( m_lastRFC.rfcPkt.fBattVoltage >= m_batteryLevel.fValidThresh )
    {
        m_batteryLevel.fLastBattVolts = m_lastRFC.rfcPkt.fBattVoltage;
        m_batteryLevel.iLastBattUsed  = m_lastRFC.rfcPkt.battUsed;

        m_batteryLevel.avgBattVolts->AddValue( m_lastRFC.rfcPkt.fBattVoltage );
        m_batteryLevel.avgBattUsed->AddValue ( m_lastRFC.rfcPkt.battUsed );

        if( m_batteryLevel.avgBattVolts->AverageValue < m_batteryLevel.fMinOperThresh )
            m_battLevel = eBL_Low;
        else if( m_batteryLevel.avgBattVolts->AverageValue < m_batteryLevel.fGoodThresh )
            m_battLevel = eBL_Med;
        else
            m_battLevel = eBL_OK;
    }
}


void TWTTTSSub::ClearHardwareInfo( void )
{
    m_cfgStraps = 0;
    memset( m_firmwareVer, 0, WTTTS_FW_VER_LEN );
}


bool TWTTTSSub::TimeParamsGood( void )
{
    // Check that the current time params match those in the last RFC packet.
    // Only call this function if a RFC has been received and no other command
    // has been sent. Return true if all timing params match, otherwise
    // return false.
    TESTORK_DATA_UNION pktUnion;
    memset( &pktUnion, 0, sizeof( TESTORK_DATA_UNION ) );

    bool paramsGood = true;

    if( m_lastRFC.rfcPkt.rfcRate != m_timeParams.rfcRate )
    {
        pktUnion.ratePkt.rateType = SRT_RFC_RATE;
        pktUnion.ratePkt.newValue = m_timeParams.rfcRate;

        paramsGood = false;
    }
    else if( m_lastRFC.rfcPkt.rfcTimeout != m_timeParams.rfcTimeout )
    {
        pktUnion.ratePkt.rateType = SRT_RFC_TIMEOUT;
        pktUnion.ratePkt.newValue = m_timeParams.rfcTimeout;

        paramsGood = false;
    }
    else if( m_lastRFC.rfcPkt.streamRate != m_timeParams.streamRate )
    {
        pktUnion.ratePkt.rateType = SRT_STREAM_RATE;
        pktUnion.ratePkt.newValue = m_timeParams.streamRate;

        paramsGood = false;
    }
    else if( m_lastRFC.rfcPkt.streamTimeout != m_timeParams.streamTimeout )
    {
        pktUnion.ratePkt.rateType = SRT_STREAM_TIMEOUT;
        pktUnion.ratePkt.newValue = m_timeParams.streamTimeout;

        paramsGood = false;
    }
    else if( m_lastRFC.rfcPkt.pairingTimeout != m_timeParams.pairingTimeout )
    {
        pktUnion.ratePkt.rateType = SRT_PAIR_TIMEOUT;
        pktUnion.ratePkt.newValue = m_timeParams.pairingTimeout;

        paramsGood = false;
    }

    if( paramsGood )
        return true;

    // Fall through means we have to change a rate
    SendSubCommand( SCT_SET_RATE_CMD, &pktUnion );

    return false;
}


//
// WTTTS Onboard Configuration Data Support Functions
//

bool TWTTTSSub::GetCalDataRaw( TStringList* pCaptions, TStringList* pValues )
{
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    pCaptions->Clear();
    pValues->Clear();

    if( !IsConnected )
        return false;

    // Report the raw data as array of pages
    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        UnicodeString pageDataStr;

        BYTE* pData = &( m_rawCfgData[ iPg * NBR_BYTES_PER_WTTTS_PAGE ] );

        for( int iByte = 0; iByte < NBR_BYTES_PER_WTTTS_PAGE; iByte++ )
            pageDataStr = pageDataStr + IntToHex( pData[iByte], 2 ) + " ";

        pCaptions->Add( "Page " + IntToStr( iPg ) );
        pValues->Add( pageDataStr );
    }

    return true;
}


static UnicodeString CharsToStr( const char* szString, int maxLength )
{
    char* szTemp = new char[maxLength];

    memset( szTemp, 0, maxLength );

    for( int iChar = 0; iChar < maxLength - 1; iChar++ )
    {
        if( szString[iChar] == '\0' )
            break;

        szTemp[iChar] = szString[iChar];
    }

    UnicodeString sResult = szTemp;

    delete [] szTemp;

    return sResult;
}


static UnicodeString MfgDateToStr( BYTE relYear, BYTE month )
{
    int year = 2000 + (int)relYear;

    AnsiString sMonth;

    switch( month )
    {
        case 0:   sMonth = "Jan";  break;
        case 1:   sMonth = "Feb";  break;
        case 2:   sMonth = "Mar";  break;
        case 3:   sMonth = "Apr";  break;
        case 4:   sMonth = "May";  break;
        case 5:   sMonth = "Jun";  break;
        case 6:   sMonth = "Jul";  break;
        case 7:   sMonth = "Aug";  break;
        case 8:   sMonth = "Sep";  break;
        case 9:   sMonth = "Oct";  break;
        case 10:  sMonth = "Nov";  break;
        case 11:  sMonth = "Dec";  break;
        default:  sMonth = "Month " + IntToStr( month );
    }

    AnsiString sResult;
    sResult.printf( "%d %s", year, sMonth.c_str() );

    return UnicodeString( sResult );
}


bool TWTTTSSub::GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo )
{
    WTTTS_MFG_DATA_STRUCT* pMfgData = (WTTTS_MFG_DATA_STRUCT*)m_rawCfgData;

    const String notSet( "(not set)" );

    if( pMfgData->devSN != 0xFFFF )
        devHWInfo.mfgSN = IntToStr( pMfgData->devSN );
    else
        devHWInfo.mfgSN = notSet;

    if( pMfgData->szMfgInfo[0] != (char)0xFF )
        devHWInfo.mfgInfo = CharsToStr( pMfgData->szMfgInfo, 8 );
    else
        devHWInfo.mfgInfo = notSet;

    if( pMfgData->yearOfMfg != 0xFF )
        devHWInfo.mfgDate = MfgDateToStr( pMfgData->yearOfMfg, pMfgData->monthOfMfg );
    else
        devHWInfo.mfgDate = notSet;

    if( m_firmwareVer[0] != 0xFF )
        devHWInfo.fwRev = IntToStr( m_firmwareVer[0] ) + "." + IntToStr( m_firmwareVer[1] ) + "." + IntToStr( m_firmwareVer[2] ) + "." + IntToStr( m_firmwareVer[3] );
    else
        devHWInfo.fwRev = notSet;

    devHWInfo.cfgStraps = IntToHex( m_cfgStraps, 2 );
    devHWInfo.housingSN = GetDevHousingSN();

    return true;
}


bool TWTTTSSub::GetDeviceBatteryInfo( BATTERY_INFO& battInfo )
{
    if( !m_batteryLevel.avgBattVolts->IsValid )
        return false;

    WTTTS_CAL_DATA_STRUCT_V2_2* pCalData = (WTTTS_CAL_DATA_STRUCT_V2_2*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    BATTERY_TYPE battType = (BATTERY_TYPE)m_lastRFC.rfcPkt.battType;

    battInfo.battType        = battType;
    battInfo.battTypeText    = GetBattTypeText( battType );
    battInfo.initialCapacity = pCalData->battCapacity;
    battInfo.currVolts       = m_batteryLevel.fLastBattVolts;
    battInfo.currUsage       = m_batteryLevel.iLastBattUsed;
    battInfo.avgVolts        = m_batteryLevel.avgBattVolts->AverageValue;
    battInfo.avgUsage        = (int)( m_batteryLevel.avgBattUsed->AverageValue );

    return true;
}


bool TWTTTSSub::GetPDSClosed( void )
{
    if( m_lastRFC.rfcPkt.pdsSwitch < m_pdsOnThresh )
        return true;
    else
        return false;
}


bool TWTTTSSub::GetPDSOpen( void )
{
    if( m_lastRFC.rfcPkt.pdsSwitch > m_pdsOffThresh )
        return true;
    else
        return false;
}


void TWTTTSSub::GetLastTempCompValues( TEMP_COMP_VALUES& tcvValues )
{
    tcvValues.fRTDSlope           = m_tempComp.fLastRTDROC;
    tcvValues.fRTDSlopeROC        = m_tempComp.fLastRTDSlopeROC;
    tcvValues.fFirstOrderOffset   = m_tempComp.fInitialOffset;
    tcvValues.fTotalTensionOffset = m_tempComp.fTotalOffset;
}


bool TWTTTSSub::GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    if( !InitPropertyList( pCaptions, pValues, CalFactorCaptions, NBR_CALIBRATION_ITEMS ) )
        return false;

    WTTTS_MFG_DATA_STRUCT*      pMfgData = (WTTTS_MFG_DATA_STRUCT*)m_rawCfgData;
    WTTTS_CAL_DATA_STRUCT_V2_2* pCalData = (WTTTS_CAL_DATA_STRUCT_V2_2*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*       pVerData = (WTTTS_CAL_VER_STRUCT*)      &( m_rawCfgData[ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    for( int iItem = 0; iItem < NBR_CALIBRATION_ITEMS; iItem++ )
    {
        UnicodeString sValue;

        switch( iItem )
        {
            case CI_VERSION:           sValue = IntToStr    ( pVerData->byCalVerNbr );     break;
            case CI_REVISION:          sValue = IntToStr    ( pVerData->byCalRevNbr );     break;
            case CI_SERIAL_NBR:        sValue = IntToStr    ( pMfgData->devSN );           break;
            case CI_MFG_INFO:          sValue = CharsToStr  ( pMfgData->szMfgInfo, 8 );    break;
            case CI_MFG_DATE:          sValue = MfgDateToStr( pMfgData->yearOfMfg, pMfgData->monthOfMfg );      break;
            case CI_TORQUE000_OFFSET:  sValue = IntToStr    ( pCalData->torque045_Offset );                      break;
            case CI_TORQUE000_SPAN:    sValue = FloatToStrF ( pCalData->torque045_Span,    ffExponent, 7, 2 );   break;
            case CI_TORQUE180_OFFSET:  sValue = IntToStr    ( pCalData->torque225_Offset );                      break;
            case CI_TORQUE180_SPAN:    sValue = FloatToStrF ( pCalData->torque225_Span,    ffExponent, 7, 2 );   break;
            case CI_TENSION000_OFFSET: sValue = IntToStr    ( pCalData->tension000_Offset );                     break;
            case CI_TENSION000_SPAN:   sValue = FloatToStrF ( pCalData->tension000_Span,   ffExponent, 7, 2 );   break;
            case CI_TENSION090_OFFSET: sValue = IntToStr    ( pCalData->tension090_Offset );                     break;
            case CI_TENSION090_SPAN:   sValue = FloatToStrF ( pCalData->tension090_Span,   ffExponent, 7, 2 );   break;
            case CI_TENSION180_OFFSET: sValue = IntToStr    ( pCalData->tension180_Offset );                     break;
            case CI_TENSION180_SPAN:   sValue = FloatToStrF ( pCalData->tension180_Span,   ffExponent, 7, 2 );   break;
            case CI_TENSION270_OFFSET: sValue = IntToStr    ( pCalData->tension270_Offset );                     break;
            case CI_TENSION270_SPAN:   sValue = FloatToStrF ( pCalData->tension270_Span,   ffExponent, 7, 2 );   break;
            case CI_XTALK_TQ_TQ:       sValue = FloatToStrF ( pCalData->xTalk_TqTq,        ffExponent, 7, 2 );   break;
            case CI_XTALK_TQ_TEN:      sValue = FloatToStrF ( pCalData->xTalk_TqTen,       ffExponent, 7, 2 );   break;
            case CI_XTALK_TEN_TQ:      sValue = FloatToStrF ( pCalData->xTalk_TenTq,       ffExponent, 7, 2 );   break;
            case CI_XTALK_TEN_TEN:     sValue = FloatToStrF ( pCalData->xTalk_TenTen,      ffExponent, 7, 2 );   break;
            case CI_GYRO_OFFSET:       sValue = IntToStr    ( pCalData->gyro_Offset );                           break;
            case CI_GYRO_SPAN:         sValue = FloatToStrF ( pCalData->gyro_Span,         ffExponent, 7, 2 );   break;
            case CI_PRESSURE_OFFSET:   sValue = IntToStr    ( pCalData->pressure_Offset );                       break;
            case CI_PRESSURE_SPAN:     sValue = FloatToStrF ( pCalData->pressure_Span,     ffExponent, 7, 2 );   break;
            case CI_BATTERY_CAPACITY:  sValue = IntToStr    ( pCalData->battCapacity );                          break;
            case CI_BATTERY_TYPE:      sValue = GetBattTypeText( (BATTERY_TYPE)pCalData->battType );            break;

            case CI_CALIBRATION_DATE:
                if( pVerData->calYear != 0xFF )
                {
                    sValue.printf( L"%d/%02d/%02d", (int)pVerData->calYear + 2000,
                                                    (int)pVerData->calMonth,
                                                    (int)pVerData->calDay );
                }
                else
                {
                    sValue = "----/--/--";
                }
                break;
        }

        pValues->Strings[iItem] = sValue;
    }

    return true;
}


bool TWTTTSSub::SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues )
{
    // Although the user can try to pass values for all items in configuration
    // memory, we only change the values of items in the host config area.
    // Note that this function only updates the data cached in the host memory
    // area - it does not commit the data to the device.
    if( ( pCaptions == NULL ) || ( pValues == NULL ) )
        return false;

    WTTTS_CAL_DATA_STRUCT_V2_2* pCalData = (WTTTS_CAL_DATA_STRUCT_V2_2*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*       pVerData = (WTTTS_CAL_VER_STRUCT*)      &( m_rawCfgData[ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    try
    {
        for( int iParam = 0; iParam < pCaptions->Count; iParam++ )
        {
            String sKey   = pCaptions->Strings[iParam];
            String sValue = pValues->Strings[iParam];

            for( int paramIndex = 0; paramIndex < NBR_CALIBRATION_ITEMS; paramIndex++ )
            {
                if( sKey == CalFactorCaptions[paramIndex] )
                {
                    switch( paramIndex )
                    {
                        case CI_REVISION:           pVerData->byCalRevNbr       = sValue.ToInt();     break;
                        case CI_TORQUE000_OFFSET:   pCalData->torque045_Offset  = sValue.ToInt();     break;
                        case CI_TORQUE000_SPAN:     pCalData->torque045_Span    = sValue.ToDouble();  break;
                        case CI_TORQUE180_OFFSET:   pCalData->torque225_Offset  = sValue.ToInt();     break;
                        case CI_TORQUE180_SPAN:     pCalData->torque225_Span    = sValue.ToDouble();  break;
                        case CI_TENSION000_OFFSET:  pCalData->tension000_Offset = sValue.ToInt();     break;
                        case CI_TENSION000_SPAN:    pCalData->tension000_Span   = sValue.ToDouble();  break;
                        case CI_TENSION090_OFFSET:  pCalData->tension090_Offset = sValue.ToInt();     break;
                        case CI_TENSION090_SPAN:    pCalData->tension090_Span   = sValue.ToDouble();  break;
                        case CI_TENSION180_OFFSET:  pCalData->tension180_Offset = sValue.ToInt();     break;
                        case CI_TENSION180_SPAN:    pCalData->tension180_Span   = sValue.ToDouble();  break;
                        case CI_TENSION270_OFFSET:  pCalData->tension270_Offset = sValue.ToInt();     break;
                        case CI_TENSION270_SPAN:    pCalData->tension270_Span   = sValue.ToDouble();  break;
                        case CI_XTALK_TQ_TQ:        pCalData->xTalk_TqTq        = sValue.ToDouble();  break;
                        case CI_XTALK_TQ_TEN:       pCalData->xTalk_TqTen       = sValue.ToDouble();  break;
                        case CI_XTALK_TEN_TQ:       pCalData->xTalk_TenTq       = sValue.ToDouble();  break;
                        case CI_XTALK_TEN_TEN:      pCalData->xTalk_TenTen      = sValue.ToDouble();  break;
                        case CI_GYRO_OFFSET:        pCalData->gyro_Offset       = sValue.ToInt();     break;
                        case CI_GYRO_SPAN:          pCalData->gyro_Span         = sValue.ToDouble();  break;
                        case CI_PRESSURE_OFFSET:    pCalData->pressure_Offset   = sValue.ToInt();     break;
                        case CI_PRESSURE_SPAN:      pCalData->pressure_Span     = sValue.ToDouble();  break;
                        case CI_BATTERY_CAPACITY:   pCalData->battCapacity      = sValue.ToInt();     break;
                        case CI_BATTERY_TYPE:       pCalData->battType          = GetBattTypeEnum( sValue ); break;
                    }

                    // Can break out of search loop when an item is found
                    break;
                }
            }
        }
    }
    catch( ... )
    {
        // A catch means either the pValues array was insufficiently populated
        // or one of the entries was an invalid value.
        return false;
    }

    // Fall through means success - update the current cal version
    pVerData->byCalVerNbr = CURR_CAL_VERSION;
    pVerData->byCalRevNbr = CURR_CAL_REV;

    return true;
}


bool TWTTTSSub::ReloadCalDataFromDevice( void )
{
    if( !IsConnected )
        return false;

    // Can only start the upload to device if we are currently idle
    if( m_linkState != LS_IDLE )
        return false;

    // Clear the configuration data. When doing this, we also reset averages
    ClearConfigurationData();

    m_avgTorque->Reset();
    m_avgTension->Reset();

    // Set the link state, and bump up the RFC timers for the data exchange
    m_timeParams.rfcRate    = StartupRFCInterval;
    m_timeParams.rfcTimeout = StartupRFCTimeout;

    m_linkState = LS_CFG_DOWNLOAD;

    m_currCfgReqCycle = 1;

    return true;
}


bool TWTTTSSub::WriteCalDataToDevice( void )
{
    if( !IsConnected )
        return false;

    // Can only start the upload to device if we are currently idle
    if( m_linkState != LS_IDLE )
        return false;

    for( int iPg = WTTTS_CFG_FIRST_HOST_PAGE; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
        m_cfgPgStatus[iPg].setPending = true;

    // Reset averages when uploading config data
    m_avgTorque->Reset();
    m_avgTension->Reset();

    // Set the link state, and bump up the RFC timers for the data exchange
    m_timeParams.rfcRate    = StartupRFCInterval;
    m_timeParams.rfcTimeout = StartupRFCTimeout;

    m_linkState = LS_CFG_UPLOAD;

    return true;
}


bool TWTTTSSub::WriteHousingSN( const String& sNewHousingSN )
{
    // Writes the passed housing serial number to it. No status
    // information is reported while the SN is updated; client
    // code will have to poll the Device status and Housing SN
    // property to see if / when the update is done.
    if( !IsConnected )
        return false;

    // Can only start the upload to device if we are currently idle
    if( m_linkState != LS_IDLE )
        return false;

    // Put the passed SN into memory. A blank SN is not allowed.
    if( sNewHousingSN.Length() == 0 )
        return false;

    // Copy the SN into the area now. First clear any existing data
    AnsiString asNewSN = sNewHousingSN;

    WTTTS_HOUSING_SN_STRUCT* pSNStruct = (WTTTS_HOUSING_SN_STRUCT*)&( m_rawCfgData[ HOUSING_SN_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    memset( pSNStruct, 0, HOUSING_SN_STRUCT_PAGES * NBR_BYTES_PER_WTTTS_PAGE );

    // Do a safe copy of the new SN. If it is too long, it will be truncated.
    // Must leave at least one byte for a null terminator
    int iSNLength = asNewSN.Length();

    if( iSNLength > HOUSING_SN_STRUCT_PAGES * NBR_BYTES_PER_WTTTS_PAGE - 1 )
        iSNLength = HOUSING_SN_STRUCT_PAGES * NBR_BYTES_PER_WTTTS_PAGE - 1;

    memcpy( pSNStruct->byHousingSN, asNewSN.c_str(), iSNLength );

    // Copy the new housing SN to the pages to be uploaded
    for( int iPgOffset = 0; iPgOffset < HOUSING_SN_STRUCT_PAGES; iPgOffset++ )
        m_cfgPgStatus[HOUSING_SN_STRUCT_PAGE + iPgOffset].setPending = true;

    // Set the link state, and bump up the RFC timers for the data exchange
    m_timeParams.rfcRate    = StartupRFCInterval;
    m_timeParams.rfcTimeout = StartupRFCTimeout;

    m_linkState = LS_CFG_UPLOAD;

    return true;
}


void TWTTTSSub::ClearConfigurationData( void )
{
    // Set all pages to the EEPROM 'empty' value first (0xFF)
    for( int iPg = 0; iPg < NBR_WTTTS_CFG_PAGES; iPg++ )
    {
        m_cfgPgStatus[iPg].havePage        = false;
        m_cfgPgStatus[iPg].lastCfgReqCycle = 0;
        m_cfgPgStatus[iPg].setPending      = false;

        memset( m_rawCfgData, 0xFF, NBR_WTTTS_CFG_PAGES * NBR_BYTES_PER_WTTTS_PAGE );
    }

    m_currCfgReqCycle = 0;

    // Call CheckCalDataVersion next - this will initialize our local
    // memory to default values while we wait for the config data
    // to be downloaded.
    CheckCalDataVersion();
}


void TWTTTSSub::CheckCalDataVersion( void )
{
    WTTTS_CAL_DATA_STRUCT_V2_2* pCalData2v2 = (WTTTS_CAL_DATA_STRUCT_V2_2*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_DATA_STRUCT_V2_1* pCalData2v1 = (WTTTS_CAL_DATA_STRUCT_V2_1*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_DATA_STRUCT_V2_0* pCalData2v0 = (WTTTS_CAL_DATA_STRUCT_V2_0*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );
    WTTTS_CAL_VER_STRUCT*       pVerData    = (WTTTS_CAL_VER_STRUCT*)      &( m_rawCfgData[ CAL_VER_STRUCT_PAGE  * NBR_BYTES_PER_WTTTS_PAGE ] );

    // Upgrade the cal struct, from oldest rev to newest
    if( pVerData->byCalVerNbr == 1 )
    {
        // Original version. Initialize members introduced in V2
        pCalData2v1->battType     = 0; // unknown battery type
        pCalData2v1->battCapacity = 0; // mAh
        pCalData2v1->rfu1         = 0;

        pVerData->byCalVerNbr = 2;
        pVerData->byCalRevNbr = 0;
    }

    if( pVerData->byCalVerNbr == CURR_CAL_VERSION )
    {
        // This is the current version - check for a revision update
        if( pVerData->byCalRevNbr == 0 )
        {
            // Convert all offset values from floats to ints
            pCalData2v1->torque045_Offset  = pCalData2v0->torque045_Offset;
            pCalData2v1->torque225_Offset  = pCalData2v0->torque225_Offset;
            pCalData2v1->tension000_Offset = pCalData2v0->tension000_Offset;
            pCalData2v1->tension090_Offset = pCalData2v0->tension090_Offset;
            pCalData2v1->tension180_Offset = pCalData2v0->tension180_Offset;
            pCalData2v1->tension270_Offset = pCalData2v0->tension270_Offset;
            pCalData2v1->gyro_Offset       = pCalData2v0->gyro_Offset;
            pCalData2v1->pressure_Offset   = pCalData2v0->pressure_Offset;

            pVerData->byCalRevNbr = 1;
        }

        if( pVerData->byCalRevNbr == 1 )
        {
            // Add defaults for crosstalk values
            pCalData2v2->xTalk_TqTq        = 1.0;
            pCalData2v2->xTalk_TqTen       = 0.0;
            pCalData2v2->xTalk_TenTq       = 0.0;
            pCalData2v2->xTalk_TenTen      = 1.0;

            pVerData->byCalRevNbr = 2;
        }
    }

    // Check for the current version. If we're at it, we are done.
    if( ( pVerData->byCalVerNbr == CURR_CAL_VERSION )
          &&
        ( pVerData->byCalRevNbr == CURR_CAL_REV )
      )
    {
        return;
    }

    // Fall through means we don't know this cal struct. Init it to default values
    memcpy( pCalData2v2, &defaultWTTTSCalData, sizeof( WTTTS_CAL_DATA_STRUCT_V2_2 ) );

    pVerData->byCalVerNbr = CURR_CAL_VERSION;
    pVerData->byCalRevNbr = CURR_CAL_REV;
}


void TWTTTSSub::CreateCalibratedReading( const WTTTS_STREAM_DATA& streamData, DWORD elapsedMsecs, int nbrPkts )
{
    WTTTS_CAL_DATA_STRUCT_V2_2* pCalData = (WTTTS_CAL_DATA_STRUCT_V2_2*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    // Capture the data values as reported in the streaming packet
    m_devRawReading.torque045  = TriByteToInt    ( streamData.torque045  );
    m_devRawReading.torque225  = TriByteToInt    ( streamData.torque225  );
    m_devRawReading.tension000 = TriByteToInt    ( streamData.tension000 );
    m_devRawReading.tension090 = TriByteToInt    ( streamData.tension090 );
    m_devRawReading.tension180 = TriByteToInt    ( streamData.tension180 );
    m_devRawReading.tension270 = TriByteToInt    ( streamData.tension270 );
    m_devRawReading.compassX   = BiByteToShortInt( streamData.compassX );
    m_devRawReading.compassY   = BiByteToShortInt( streamData.compassY );
    m_devRawReading.compassZ   = BiByteToShortInt( streamData.compassZ );
    m_devRawReading.accelX     = BiByteToShortInt( streamData.accelX );
    m_devRawReading.accelY     = BiByteToShortInt( streamData.accelY );
    m_devRawReading.accelZ     = BiByteToShortInt( streamData.accelZ );
    m_devRawReading.gyro       = QuadByteToInt   ( streamData.gyro );

    // Torque: convert both torque values, then report the average of the two
    m_devCalReading.torque045 = DoSpanOffsetCal( m_devRawReading.torque045, pCalData->torque045_Offset, pCalData->torque045_Span );
    m_devCalReading.torque225 = DoSpanOffsetCal( m_devRawReading.torque225, pCalData->torque225_Offset, pCalData->torque225_Span );

    // Convert tension readings, including temp comp in the final value
    m_devCalReading.tension000 = DoSpanOffsetCal( m_devRawReading.tension000, pCalData->tension000_Offset, pCalData->tension000_Span );
    m_devCalReading.tension090 = DoSpanOffsetCal( m_devRawReading.tension090, pCalData->tension090_Offset, pCalData->tension090_Span );
    m_devCalReading.tension180 = DoSpanOffsetCal( m_devRawReading.tension180, pCalData->tension180_Offset, pCalData->tension180_Span );
    m_devCalReading.tension270 = DoSpanOffsetCal( m_devRawReading.tension270, pCalData->tension270_Offset, pCalData->tension270_Span );

    // Apply temp comp if enabled
    if( m_tempComp.params.bApplyTempComp )
    {
        m_devCalReading.tension000 += m_tempComp.fTotalOffset;
        m_devCalReading.tension090 += m_tempComp.fTotalOffset;
        m_devCalReading.tension180 += m_tempComp.fTotalOffset;
        m_devCalReading.tension270 += m_tempComp.fTotalOffset;
    }

    // Currently there are no cal factors stored for compass or accelerometer readings.
    // Report the raw readings as cal'd readings for now.
    m_devCalReading.compassX   = m_devRawReading.compassX;
    m_devCalReading.compassY   = m_devRawReading.compassY;
    m_devCalReading.compassZ   = m_devRawReading.compassZ;

    m_devCalReading.accelX     = m_devRawReading.accelX;
    m_devCalReading.accelY     = m_devRawReading.accelY;
    m_devCalReading.accelZ     = m_devRawReading.accelZ;

    // Convert the gyro reading to a position. The gyro cal offset value
    // is in raw ticks per gyro sample
    if( elapsedMsecs > 0 )
    {
        try
        {
            // Determine the change in the gyro reading. Watch for a
            // reading wrap. Equality with zero can be ignored here,
            // as we are looking for counts that have wrapped around
            // either from the max negative value to max positive,
            // or max positive around to max negative. Use 1/2 the
            // most positive and most negative values as thresholds
            // for this test.
            int posWrapThresh = 0x3FFFFFFF;
            int negWrapThresh = 0xC0000000;

            int deltaGyro;

            if( ( m_devRawReading.gyro > posWrapThresh )
                  &&
                ( m_lastGyroValues.rawReading < negWrapThresh )
              )
            {
                // Have wrapped from negative values to positive in this case
                deltaGyro = ( 0x7FFFFFFF - m_devRawReading.gyro ) + ( m_lastGyroValues.rawReading - 0x80000000 ) + 1;
            }
            else if( ( m_devRawReading.gyro < negWrapThresh )
                       &&
                     ( m_lastGyroValues.rawReading > posWrapThresh )
                   )
            {
                // Have wrapped from positive values to negative in this case
                deltaGyro = -( ( 0x7FFFFFFF - m_lastGyroValues.rawReading ) + ( m_devRawReading.gyro - 0x80000000 ) + 1 );
            }
            else
            {
                // Both readings have the same sign, so can do a
                // straight subtraction in this case
                deltaGyro = m_devRawReading.gyro - m_lastGyroValues.rawReading;
            }

#ifdef NEW_GYRO_METHOD
            // In this firmware, the accelX value is the number of gyro
            // readings contained in the rawReading member. Multiply that
            // by the number of packets this reading represents, in case
            // we've missed a packet.
            int nbrReadings = m_devRawReading.accelX * nbrPkts;

            // Net off the offset value
            int actDelta = deltaGyro - nbrReadings * pCalData->gyro_Offset;

            // The number of new micro-turns is the adjusted gyro value * scale factor
            int newCounts = (int)( (float)actDelta * pCalData->gyro_Span );

#else
            // Current firmware does not report number of A/D readings
            // made per packet. The gyro offset is per 10 msec packet.
            int gyroOffset = (int)elapsedMsecs / 10 * pCalData->gyro_Offset;

            // Net off the offset value
            int actDelta = deltaGyro - gyroOffset;

            // The number of new micro-turns is the adjusted gyro value * scale factor
            int newCounts = (int)( (float)actDelta * pCalData->gyro_Span );

#endif

            // The new rotation position is the last rotation plus the new count
            m_lastGyroValues.calReading += newCounts;

        }
        catch( ... )
        {
        }
    }

    // Save updated values
    m_lastGyroValues.rawReading = m_devRawReading.gyro;
    m_devCalReading.gyro        = m_lastGyroValues.calReading;

    // Save the new counts and the elapsed time in the RPM struct. Shift
    // all entries down one position and put this data in the last slot.
    for( int iSlot = 0; iSlot < MAX_NBR_STREAM_MODE_ROTN_RECS - 1; iSlot++ )
        m_streamingRotnRecs[iSlot] = m_streamingRotnRecs[iSlot+1];

    m_streamingRotnRecs[MAX_NBR_STREAM_MODE_ROTN_RECS - 1].dwElapsedMsecs = m_msecsInMode;
    m_streamingRotnRecs[MAX_NBR_STREAM_MODE_ROTN_RECS - 1].iGyroCount     = m_devCalReading.gyro;

    // Update the current reading, but only if we are in streaming mode
    if( m_linkState == LS_STREAMING )
    {
        m_currReading.wtttsReading.msecs    = m_msecsInMode;
        m_currReading.wtttsReading.rotation = m_devCalReading.gyro;
        m_currReading.wtttsReading.torque   = ( m_devCalReading.torque045 + m_devCalReading.torque225 ) / 2;
        m_currReading.wtttsReading.tension  = ( m_devCalReading.tension000 + m_devCalReading.tension090 + m_devCalReading.tension180 + m_devCalReading.tension270 ) / 4;

        // Calc the current RPM. To smooth out jitter, we look at the data over
        // a 1/2 second period from the most current record (at the end of the
        // array) looking backwards. Note that at this point we'll always have
        // at least one record in the array
        DWORD dwPastMsecs = 0;
        int   iPastGyro   = 0;

        for( int iSlot = MAX_NBR_STREAM_MODE_ROTN_RECS - 2; iSlot >= 0; iSlot-- )
        {
            // Break if the slot is empty
            if( m_streamingRotnRecs[iSlot].dwElapsedMsecs == 0 )
                break;

            // Save the slot values
            dwPastMsecs = m_streamingRotnRecs[iSlot].dwElapsedMsecs;
            iPastGyro   = m_streamingRotnRecs[iSlot].iGyroCount;

            // Break if we've accumulated enough data
            if( m_streamingRotnRecs[MAX_NBR_STREAM_MODE_ROTN_RECS-1].dwElapsedMsecs - dwPastMsecs >= 500 )
                break;
        }

        // Calc RPM. Gyro value is in microturns, so need to divide that by 1000000
        // to get turns. Time is in msecs, so need to convert that to minutes
        //   RPM = MicroTurns / 1000000 / ( msecs / 60000 )
        //       = MicroTurns / 1000000 / msecs * 60000
        //       = MicroTurns * 60000 / 1000000  / msecs
        //       = MicroTurns * 6 / 100  / msecs
        float fRPM = 0;

        if( dwPastMsecs != 0 )
        {
            float fDeltaTurns = (float)( m_streamingRotnRecs[MAX_NBR_STREAM_MODE_ROTN_RECS-1].iGyroCount - iPastGyro );
            float fDeltaTime  = (float)( m_streamingRotnRecs[MAX_NBR_STREAM_MODE_ROTN_RECS-1].dwElapsedMsecs - dwPastMsecs );

            // RPM is always a positive number
            if( fDeltaTime > 0.0 )
                fRPM = fabs( fDeltaTurns * 6.0 / 100.0 / fDeltaTime );
        }

        m_currReading.rpm = (int)( fRPM + 0.5 );

        m_currReading.eSource     = eRS_Stream;
        m_currReading.haveReading = true;
    }
}


void TWTTTSSub::CreateCalibratedReading( const GENERIC_RFC_PKT& rfcData, const WTTTS_PKT_HDR& pktHdr )
{
     // Create Cal Reading for RFC packet. Get the calibration data
    WTTTS_CAL_DATA_STRUCT_V2_2* pCalData = (WTTTS_CAL_DATA_STRUCT_V2_2*)&( m_rawCfgData[ CAL_DATA_STRUCT_PAGE * NBR_BYTES_PER_WTTTS_PAGE ] );

    // Calibrate the torque and tension values
    float calTorque045  = DoSpanOffsetCal( rfcData.torque045,  pCalData->torque045_Offset,  pCalData->torque045_Span );
    float calTorque225  = DoSpanOffsetCal( rfcData.torque225,  pCalData->torque225_Offset,  pCalData->torque225_Span );
    float calTension000 = DoSpanOffsetCal( rfcData.tension000, pCalData->tension000_Offset, pCalData->tension000_Span );
    float calTension090 = DoSpanOffsetCal( rfcData.tension090, pCalData->tension090_Offset, pCalData->tension090_Span );
    float calTension180 = DoSpanOffsetCal( rfcData.tension180, pCalData->tension180_Offset, pCalData->tension180_Span );
    float calTension270 = DoSpanOffsetCal( rfcData.tension270, pCalData->tension270_Offset, pCalData->tension270_Span );

    // Apply temp comp if enabled
    if( m_tempComp.params.bApplyTempComp )
    {
        calTension000 += m_tempComp.fTotalOffset;
        calTension090 += m_tempComp.fTotalOffset;
        calTension180 += m_tempComp.fTotalOffset;
        calTension270 += m_tempComp.fTotalOffset;
    }

    // Build our rfcCalReadings
    m_rfcCalReading.calTorque045  = calTorque045;
    m_rfcCalReading.calTorque225  = calTorque225;
    m_rfcCalReading.calTension000 = calTension000;
    m_rfcCalReading.calTension090 = calTension090;
    m_rfcCalReading.calTension180 = calTension180;
    m_rfcCalReading.calTension270 = calTension270;

    // RFC data is reported as a sample only when we are in idle mode and when
    // we have received a previous RFC in this state
    if( ( m_linkState == LS_IDLE ) && m_lastRFC.havePrevRFC )
    {
        // Calculate the elapsed time. If it is zero, then we've received
        // a duplicate packet (possible with the radio comms being used)
        DWORD elapsedTicks = CalcElapsedTicks( m_lastRFC.prevRFCTimestamp, pktHdr.timeStamp );

        if( elapsedTicks != 0 )
        {
            m_msecsInMode += MSecsPerPktTick * elapsedTicks;

            // Calculate the rotation. Note that the value reported is in microturns
            // and stored as an int. Therefore, the max turns that can be reported
            // is 2,400 before the value will wrap around to a negative number.
            int iNewRotation = m_lastRFC.rotation;

            // Convert to microturns per millisecond. So mul by 1000000, then div by 60000
            float fmTurnspmSecond = (float)rfcData.rpm * ( 1000000.0 / 60000.0 );

            // Add the change in rotation to our old rotation
            m_lastRFC.fAccum += ( fmTurnspmSecond * elapsedTicks * MSecsPerPktTick );

            // Take any full steps out of the accumumlater now
            int iNewSteps = (int)m_lastRFC.fAccum;

            iNewRotation     += iNewSteps;
            m_lastRFC.fAccum -= iNewSteps;

            // Average the Cal readings
            float fAvgTorque  = ( m_rfcCalReading.calTorque045 + m_rfcCalReading.calTorque225 ) / 2;
            float fAvgTension = ( m_rfcCalReading.calTension000 + m_rfcCalReading.calTension090 + m_rfcCalReading.calTension180 + m_rfcCalReading.calTension270 ) / 4;

            // Update the lastRFC
            m_lastRFC.rotation = iNewRotation;

            // Update the current reading
            m_currReading.wtttsReading.msecs    = m_msecsInMode;
            m_currReading.wtttsReading.rotation = iNewRotation;
            m_currReading.wtttsReading.torque   = fAvgTorque;
            m_currReading.wtttsReading.tension  = fAvgTension;

            // Make an int out of the RPM byte. Chances that the RPM will be
            // > 127 are slim, but just in case use an XFER buff so that bit
            // D7 doesn't get sign-extended across
            XFER_BUFFER xferBuff;

            xferBuff.iData = 0;
            xferBuff.byData[0] = rfcData.rpm;

            m_currReading.rpm = xferBuff.iData;

            m_currReading.eSource     = eRS_RFC;
            m_currReading.haveReading = true;
        }
    }
}


//
// Data Logging
//

void TWTTTSSub::LogRawStreamPacket( const WTTTS_PKT_HDR& pktHdr, const WTTTS_STREAM_DATA& streamData )
{
    // Write the record to file as text, if logging is enabled
    UnicodeString logFileName;

    if( !GetDataLogging( DLT_RAW_DATA, logFileName ) )
        return;

    TFileStream* logStream = NULL;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            AnsiString sHeader( "RxTime,SeqNbr,msecs,Torque045,Torque225,Tension000,Tension090,Tension180,Tension270,Gyro,CompassX,CompassY,CompassZ,AccelX,AccelY,AccelZ\r" );

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        // Convert all structure members first
        int   iTorque045  = TriByteToInt    ( streamData.torque045 );
        int   iTorque225  = TriByteToInt    ( streamData.torque225 );
        int   iTension000 = TriByteToInt    ( streamData.tension000 );
        int   iTension090 = TriByteToInt    ( streamData.tension090 );
        int   iTension180 = TriByteToInt    ( streamData.tension180 );
        int   iTension270 = TriByteToInt    ( streamData.tension270 );
        int   iRotation   = QuadByteToInt   ( streamData.gyro );
        INT16 siCompassX  = BiByteToShortInt( streamData.compassX );
        INT16 siCompassY  = BiByteToShortInt( streamData.compassY );
        INT16 siCompassZ  = BiByteToShortInt( streamData.compassZ );
        INT16 siAccelX    = BiByteToShortInt( streamData.accelX );
        INT16 siAccelY    = BiByteToShortInt( streamData.accelY );
        INT16 siAccelZ    = BiByteToShortInt( streamData.accelZ );

        AnsiString sTime = Time().TimeString();

        AnsiString sLine;
        sLine.printf( "%s,%hu,%u,%i,%i,%i,%i,%i,%i,%i,%hi,%hi,%hi,%hi,%hi,%hi\r",
                      sTime.c_str(), pktHdr.seqNbr, pktHdr.timeStamp * MSecsPerPktTick,
                      iTorque045, iTorque225, iTension000, iTension090, iTension180, iTension270,
                      iRotation, siCompassX, siCompassY, siCompassZ, siAccelX, siAccelY, siAccelZ );

        logStream->Write( sLine.c_str(), sLine.Length() );
    }
    catch( ... )
    {
    }

    if( logStream != NULL )
        delete logStream;
}


void TWTTTSSub::LogCalibratedPacket( const WTTTS_PKT_HDR& pktHdr, const DEVICE_CAL_READING& devCalReading )
{
    // Write the record to file as text, if logging is enabled
    UnicodeString logFileName;

    if( !GetDataLogging( DLT_CAL_DATA, logFileName ) )
        return;

    TFileStream* logStream = NULL;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            AnsiString sHeader( "RxTime,SeqNbr,msecs,Torque045,Torque225,Tension000,Tension090,Tension180,Tension270,Gyro,CompassX,CompassY,CompassZ,AccelX,AccelY,AccelZ\r" );

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        AnsiString sTime = Time().TimeString();

        AnsiString sLine;
        sLine.printf( "%s,%hu,%u,%i,%i,%i,%i,%i,%i,%i,%f,%f,%f,%f,%f,%f\r",
                      sTime.c_str(), pktHdr.seqNbr, pktHdr.timeStamp * MSecsPerPktTick,
                      devCalReading.torque045, devCalReading.torque225,
                      devCalReading.tension000, devCalReading.tension090, devCalReading.tension180, devCalReading.tension270,
                      devCalReading.gyro,
                      devCalReading.compassX, devCalReading.compassY, devCalReading.compassZ,
                      devCalReading.accelX,   devCalReading.accelY,   devCalReading.accelZ );

        logStream->Write( sLine.c_str(), sLine.Length() );
    }
    catch( ... )
    {
    }

    if( logStream != NULL )
        delete logStream;
}


void TWTTTSSub::LogRawRFCPacket( const WTTTS_PKT_HDR& pktHdr, const GENERIC_RFC_PKT& rfcPkt )
{
    // Write the record to file as text, if logging is enabled
    String logFileName;

    if( !GetDataLogging( DLT_RFC_DATA, logFileName ) )
        return;

    TFileStream* logStream = NULL;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            AnsiString sHeader( "RxTime,SeqNbr,msecs,PDS,RTD,Temp,Torque045,Torque225,Tension000,Tension090,Tension180,Tension270,RPM,CompassX,CompassY,CompassZ,AccelX,AccelY,AccelZ\r" );

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        AnsiString sTime = Time().TimeString();
        int iRPM = (int)rfcPkt.rpm;

        AnsiString sLine;
        sLine.printf( "%s,%hu,%u,%i,%i,%f,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i,%i\r",
                      sTime.c_str(), pktHdr.seqNbr, pktHdr.timeStamp * MSecsPerPktTick,
                      rfcPkt.pdsSwitch, rfcPkt.rtdReading, rfcPkt.fTemperature,
                      rfcPkt.torque045, rfcPkt.torque225,
                      rfcPkt.tension000, rfcPkt.tension090, rfcPkt.tension180, rfcPkt.tension270,
                      iRPM,
                      rfcPkt.compassX, rfcPkt.compassY, rfcPkt.compassZ,
                      rfcPkt.accelX, rfcPkt.accelY, rfcPkt.accelZ );

        logStream->Write( sLine.c_str(), sLine.Length() );
    }
    catch( ... )
    {
    }

    if( logStream != NULL )
        delete logStream;
}

