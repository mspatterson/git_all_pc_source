#ifndef SubDeviceWTTTSH
#define SubDeviceWTTTSH

    //
    // Class Declaration for new WTTTS Sub handler
    //

    #include "SubDeviceBaseClass.h"

    class TWTTTSSub: public TSubDevice {

      protected:

        String GetDevStatus( void );
        String GetDevHousingSN( void );
        bool   GetCanStart( void );
        bool   GetDevIsIdle( void );
        bool   GetDevIsStreaming( void );
        bool   GetDevIsCalibrated( void );
        int    GetConfigUploadProgress( void );

        void   UpdateAveraging( void );
            // Call this function to update the tension and torque averages
            // after either a RFC or stream data packet is received.

      public:

        __fastcall  TWTTTSSub( void );
        __fastcall ~TWTTTSSub();

        bool Connect( const COMMS_CFG& portCfg );

        bool CheckPort( void );

        bool StartDataCollection( WORD streamInterval /*msecs*/ );
        bool StopDataCollection( void );

        bool StartCirculatingMode( WORD wInterval /*msecs*/ );
        bool StopCirculatingMode( void );

        bool GetNextSample( WTTTS_READING& nextSample, int& rpm );

        bool Update( void );

        bool ShowLastStatusPkt( TStringList* pCaptions, TStringList* pValues );
        bool GetLastStatusPkt( time_t& tLastPkt, DEVICE_RAW_READING& lastPkt );

        bool GetLastDataRaw( DWORD& msecs, BYTE& seqNbr, DEVICE_RAW_READING& lastPkt );
        bool GetLastDataCal( DWORD& msecs, BYTE& seqNbr, DEVICE_CAL_READING& lastPkt );

        bool GetLastAvgData( float& avgTorque, float& avgTension, int& avgRPM );

        typedef enum {
            CI_VERSION,
            CI_REVISION,
            CI_SERIAL_NBR,
            CI_MFG_INFO,
            CI_MFG_DATE,
            CI_CALIBRATION_DATE,
            CI_TORQUE000_OFFSET,
            CI_TORQUE000_SPAN,
            CI_TORQUE180_OFFSET,
            CI_TORQUE180_SPAN,
            CI_TENSION000_OFFSET,
            CI_TENSION000_SPAN,
            CI_TENSION090_OFFSET,
            CI_TENSION090_SPAN,
            CI_TENSION180_OFFSET,
            CI_TENSION180_SPAN,
            CI_TENSION270_OFFSET,
            CI_TENSION270_SPAN,
            CI_XTALK_TQ_TQ,
            CI_XTALK_TQ_TEN,
            CI_XTALK_TEN_TQ,
            CI_XTALK_TEN_TEN,
            CI_GYRO_OFFSET,
            CI_GYRO_SPAN,
            CI_PRESSURE_OFFSET,
            CI_PRESSURE_SPAN,
            CI_BATTERY_TYPE,
            CI_BATTERY_CAPACITY,
            NBR_CALIBRATION_ITEMS
        } CALIBRATION_ITEM;

        static const String CalFactorCaptions[NBR_CALIBRATION_ITEMS];

        bool GetCalDataRaw      ( TStringList* pCaptions, TStringList* pValues );
        bool GetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
        bool SetCalDataFormatted( TStringList* pCaptions, TStringList* pValues );
            // Note: users should not rely on the items in the above string lists
            // being in the same order as the calibration enum list above.

        bool ReloadCalDataFromDevice( void );
        bool WriteCalDataToDevice( void );

        bool WriteHousingSN( const String& sNewHousingSN );

        bool GetDeviceHWInfo( DEVICE_HW_INFO& devHWInfo );

        bool GetDeviceBatteryInfo( BATTERY_INFO& battInfo );

        virtual bool GetPDSClosed( void );
        virtual bool GetPDSOpen( void );
            // Returns the state of the PDS switch. Two functiona are implemented
            // as that allows for hysteresis be implemented

        virtual void GetLastTempCompValues( TEMP_COMP_VALUES& tcvValues );

      private:

        // WTTTS hardware info
        BYTE m_cfgStraps;
        BYTE m_firmwareVer[WTTTS_FW_VER_LEN];

        void ClearHardwareInfo( void );

        // Sub command management
        typedef enum {
            SCT_NO_COMMAND,
            SCT_START_DATA_COLLECTION,
            SCT_STOP_DATA_COLLECTION,
            SCT_REQUEST_VER_INFO,
            SCT_GET_CFG_PAGE,
            SCT_SET_CFG_PAGE,
            SCT_SET_RATE_CMD,
            SCT_ENTER_DEEP_SLEEP,
            SCT_SET_RF_CHANNEL,
            NBR_SUB_CMD_TYPES
        } SUB_CMD_TYPE;

        // Create an array of the sub commands. For no payload commands, the
        // cached data can be sent as is.
        typedef struct {
            bool  needsPayload;
            DWORD cmdLen;
            BYTE  byTxData[MAX_TESTORK_PKT_LEN];
        } CACHED_CMD_PKT;

        CACHED_CMD_PKT m_cachedCmds[NBR_SUB_CMD_TYPES];

        void InitWTTTSDataCmdPkt( SUB_CMD_TYPE cmdType );
        bool SendSubCommand( SUB_CMD_TYPE aCmd, const TESTORK_DATA_UNION* pPayload = NULL );

        // We have to periodically send restart messages when in streaming mode.
        // Need to record the last time we sent a start streaming command
        time_t m_lastStreamStartTime;

        // We sometimes want to discard messages from the device when it
        // starts or stops streaming
        int   m_streamDiscardCount;

        // Receive data vars
        BYTE* m_rxBuffer;              // received data holding buffer
        DWORD m_rxBuffCount;           // number of bytes currently in the buffer
        DWORD m_lastRxByteTime;        // time (tick count) of last byte received in buffer
        DWORD m_lastRxPktTime;         // time last valid packet of any type received from sub

        DWORD m_lastStreamDataTime;    // time (tick count) of last stream data packet
        DWORD m_msecsInMode;           // when streaming or circulating modes, the duration in the mode is tracked here (in msecs)

        #define MAX_NBR_STREAM_MODE_ROTN_RECS  10

        typedef struct {               // Struct to hold gyro counts while streaming to calc RPM
            DWORD dwElapsedMsecs;
            int   iGyroCount;
        } STREAM_MODE_ROTN_REC;

        STREAM_MODE_ROTN_REC m_streamingRotnRecs[MAX_NBR_STREAM_MODE_ROTN_RECS];

        WTTTS_PKT_HDR m_lastPktHdr;    // Header from the last packet

        typedef enum {
            eRS_RFC,
            eRS_Stream
        } eReadingSource;

        struct {
            eReadingSource eSource;       // Identifies the source of the stored reading (RFC or stream)
            WTTTS_READING  wtttsReading;  // Reading contents
            int            rpm;           // RPM at time of reading, always +ve value
            bool           haveReading;   // True if reading contents and source have been set
        } m_currReading;

        struct {
            DWORD            dwRFCTick;   // time (tick count) of last RFC from device
            time_t           tRFCTime;    // time_t of last RFC from device; 0 = don't have an RFC
            bool             havePrevRFC;       // Set to true when we have received a RFC after starting to circulate
            WORD             prevRFCTimestamp;  // Timestamp of prev RFC packet, valid if havePrevRFC == true
            GENERIC_RFC_PKT  rfcPkt;      // Converted form of RFC packet
            int              rotation;    // Rotational postion, in microturns, set in CreateCalRec
            float            fAccum;      // Accumulator for extra microturns, set in CreateCalRec
        } m_lastRFC;

        bool TimeParamsGood( void );

        // Temperature compensation support
        struct {
            BYTE             lastRFCSeqNbr;     // Incremented on each tx packet. Rolls over to zero at 255
            WORD             lastRFCTimeStamp;  // Packet time, in 10's of msec, since device power-up
            time_t           tLastRFCPkt;       // time of last RFC packet, in seconds
            int              iLastRTDValue;
            TExpAverage*     avgRTD;
            TExpAverage*     avgRTDSlope;
            float            fLastRTDROC;       // First order: RTD rate of change
            float            fLastRTDSlopeROC;  // Second order: RTD slope rate of change
            float            fInitialOffset;    // First order offset calculation
            float            fTotalOffset;      // First order + second order offset calc
            TEMP_COMP_PARAMS params;
        } m_tempComp;

        // Configuration data support
        BYTE m_rawCfgData[NBR_WTTTS_CFG_PAGES * NBR_BYTES_PER_WTTTS_PAGE];

        typedef struct {
            bool  havePage;
            int   lastCfgReqCycle;
            bool  setPending;
        } CFG_PAGE_STATUS;

        // Used to prevent multiple requests for each config page
        int m_currCfgReqCycle;

        CFG_PAGE_STATUS m_cfgPgStatus[NBR_WTTTS_CFG_PAGES];

        void ClearConfigurationData( void );
        void CheckCalDataVersion( void );

        typedef struct {
            int   rawReading;
            int   calReading;              // Last cal'd reading
        } GYRO_VALUES;

        GYRO_VALUES m_lastGyroValues;

        DEVICE_RAW_READING m_devRawReading;   // Record device's last reading as raw data
        DEVICE_CAL_READING m_devCalReading;   // Record device's last reading as cal data

        struct {
            float calTorque045;
            float calTorque225;
            float calTension000;
            float calTension090;
            float calTension180;
            float calTension270;
        } m_rfcCalReading;

        void CreateCalibratedReading( const WTTTS_STREAM_DATA& streamData, DWORD elapsedMsecs, int nbrPkts );
        void CreateCalibratedReading( const GENERIC_RFC_PKT& rfcData, const WTTTS_PKT_HDR& pktHdr );

        void ClearTempComp( void );
        void UpdateTempComp( const WTTTS_PKT_HDR& pktHdr );

        void ClearBatteryVolts( void );
        void UpdateBatteryVolts( void );

        // Logging
        void LogRawStreamPacket ( const WTTTS_PKT_HDR& pktHdr, const WTTTS_STREAM_DATA&  streamData );
        void LogCalibratedPacket( const WTTTS_PKT_HDR& pktHdr, const DEVICE_CAL_READING& devCalReading );
        void LogRawRFCPacket    ( const WTTTS_PKT_HDR& pktHdr, const GENERIC_RFC_PKT&    rfcPkt );
    };

#endif
