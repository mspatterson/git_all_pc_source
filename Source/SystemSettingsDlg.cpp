#include <vcl.h>
#pragma hdrstop

#include <FileCtrl.hpp>
#include <IniFiles.hpp>
#include <DateUtils.hpp>

#include "SystemSettingsDlg.h"
#include "RegistryInterface.h"
#include "UnitsOfMeasure.h"
#include "ZeroWTTTSDlg.h"
#include "NewBattDlg.h"
#include "EditWitsDlg.h"
#include "SubDeviceWTTTS.h"
#include "Messages.h"
#include "ApplUtils.h"
#include "TesTorkManagerPWGen.h"
#include "TSerializerDlg.h"
#include "ProgressDialog.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TSystemSettingsForm *SystemSettingsForm;


//
// Private Declarations
//

typedef enum{
    eWFC_Code,
    eWFC_Data,
    eWFC_NbrColumns
} WITS_FIELD_COLUMNS;


const int MIN_SAVE_TURNS = 3;


static bool IsValidInt( TEdit* anEdit, bool bMustBeGreaterThanZero = false )
{
    int iVal = 0;

    try
    {
        iVal = anEdit->Text.ToInt();
    }
    catch( ... )
    {
        return false;
    }

    if( bMustBeGreaterThanZero )
    {
        if( iVal <= 0 )
            return false;
    }

    // Fall through means value good
    return true;
}


static bool IsValidFloat( TEdit* anEdit )
{
    float fVal = 0.0;

    try
    {
        fVal = anEdit->Text.ToDouble();
    }
    catch( ... )
    {
    }

    return( fVal > 0.0 );
}


// The calibration load from file / save to file functions use the ini file format.
// The following constants define the section names in those files.
static const UnicodeString calIniDataSection( "Calibration Information" );

// Process Priority Combo settings
typedef struct {
    char*  szDesc;
    DWORD  procEnum;
} PROC_PRIORITY_ENTRY;

#define NBR_PROC_PRIORITIES 4

static const PROC_PRIORITY_ENTRY ProcPriorities[NBR_PROC_PRIORITIES] = {
    { "Normal",        NORMAL_PRIORITY_CLASS       },
    { "Above Normal",  ABOVE_NORMAL_PRIORITY_CLASS },
    { "High Priority", HIGH_PRIORITY_CLASS         },
    { "Real Time",     REALTIME_PRIORITY_CLASS     }
};


//
// Class Implementation
//

__fastcall TSystemSettingsForm::TSystemSettingsForm(TComponent* Owner) : TForm(Owner)
{
    // Init the comms sheet controls structs
    m_WTTTSCommsCtrls.pTypeCombo   = TesTORKPortTypeCombo;
    m_WTTTSCommsCtrls.pPortNbrEdit = TesTORKPortEdit;
    m_WTTTSCommsCtrls.pParamEdit   = TesTORKParamEdit;

    m_BaseCommsCtrls.pTypeCombo   = BaseRadioPortTypeCombo;
    m_BaseCommsCtrls.pPortNbrEdit = BaseRadioPortEdit;
    m_BaseCommsCtrls.pParamEdit   = BaseRadioParamEdit;

    m_MgmtCommsCtrls.pTypeCombo   = MgmtDevPortTypeCombo;
    m_MgmtCommsCtrls.pPortNbrEdit = MgmtDevPortEdit;
    m_MgmtCommsCtrls.pParamEdit   = MgmtDevParamEdit;

    // Init comm type combos - Also do this for the WITS comms combo
    TesTORKPortTypeCombo->Items->Clear();
    BaseRadioPortTypeCombo->Items->Clear();
    MgmtDevPortTypeCombo->Items->Clear();

    for( int iType = 0; iType < NBR_COMM_TYPES; iType++ )
    {
        TesTORKPortTypeCombo->Items->Add  ( CommTypeText[iType] );
        BaseRadioPortTypeCombo->Items->Add( CommTypeText[iType] );
        MgmtDevPortTypeCombo->Items->Add  ( CommTypeText[iType] );
    }

    TesTORKPortTypeCombo->ItemIndex   = CT_SERIAL;
    BaseRadioPortTypeCombo->ItemIndex = CT_SERIAL;
    MgmtDevPortTypeCombo->ItemIndex   = CT_SERIAL;

    // Init process priority combo
    ProcPriorityCombo->Items->Clear();

    for( int iItem = 0; iItem < NBR_PROC_PRIORITIES; iItem++ )
        ProcPriorityCombo->Items->Add( ProcPriorities[iItem].szDesc );

    ProcPriorityCombo->ItemIndex = 0;

    for( int iItem = 0; iItem < NBR_AVG_LEVELS; iItem++ )
    {
        TorqueCombo ->Items->Add( AvgLevelDesc[iItem] );
        TensionCombo->Items->Add( AvgLevelDesc[iItem] );
    }

    TorqueCombo ->ItemIndex = AL_MEDIUM;
    TensionCombo->ItemIndex = AL_MEDIUM;

    // Init WITS Comms
    WitsPortTypeCombo->Items->Clear();

    for( int iType = 0; iType < NBR_COMM_TYPES; iType++ )
        WitsPortTypeCombo->Items->Add( CommTypeText[iType] );

    WitsPortTypeCombo->ItemIndex = CT_UNUSED;
}


DWORD TSystemSettingsForm::ShowDlg( TCommPoller* pPoller, TJob* currJob )
{
    DWORD dwSettingsChanged = 0x0000;

    TSystemSettingsForm* currForm = new TSystemSettingsForm( NULL );

    currForm->PollObject = pPoller;
    currForm->CurrentJob = currJob;

    try
    {
        if( currForm->ShowModal() == mrOk )
            dwSettingsChanged = currForm->SettingsChanged;
    }
    __finally
    {
        delete currForm;
    }

    return dwSettingsChanged;
}


void __fastcall TSystemSettingsForm::FormShow(TObject *Sender)
{
    // Determine units of measure in effect at time of form show.
    // Get from the current job (if we have one) or the default
    // from the registry otherwise
    if( m_currJob == NULL )
        m_uomType = (UNITS_OF_MEASURE) GetDefaultUnitsOfMeasure();
    else
        m_uomType = m_currJob->Units;

    // On show, populate current settings
    LoadCommsPage();
    LoadCalPage();
    LoadMiscPage();
    LoadInterlockPage();
    LoadWitsPage();

    // Now force any click events here that influence the controls
    // on multiple pages
    BROut3FcnComboClick( BROut3FcnCombo );

    // Default settings changed to none
    m_dwSettingsChanged = 0x0000;

    PgCtrl->ActivePage        = CommsSheet;
    CalDataPgCtrl->ActivePage = DevMemorySheet;
    DevConnPgCtrl->ActivePage = WTTTSSheet;

    RFCPollTimer->Enabled = true;

    // Force a timer tick right away for immediate update
    RFCPollTimerTimer( RFCPollTimer );
}


void __fastcall TSystemSettingsForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Kill the comms object poll timer first
    RFCPollTimer->Enabled = false;

    // On close, if ModalResult == mrOK, then save current settings.
    // In this case, all settings are guaranteed to be valid.
    if( ModalResult == mrOk )
    {
        m_dwSettingsChanged = 0x0000;

        m_dwSettingsChanged |= SaveCommsPage();
        m_dwSettingsChanged |= SaveCalPage();
        m_dwSettingsChanged |= SaveMiscPage();
        m_dwSettingsChanged |= SaveInterlockPage();
        m_dwSettingsChanged |= SaveWitsPage();
    }
}


void __fastcall TSystemSettingsForm::OKBtnClick(TObject *Sender)
{
    // Validate settings
    TWinControl* invalidControl = CheckCommsPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = CommsSheet;
        ActiveControl = invalidControl;

        return;
    }

    invalidControl = CheckCalPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = CalSheet;
        ActiveControl = invalidControl;

        return;
    }

    invalidControl = CheckMiscPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = MiscSheet;
        ActiveControl = invalidControl;

        return;
    }

    invalidControl = CheckInterlockPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = InterlockSheet;
        ActiveControl = invalidControl;

        return;
    }

    invalidControl = CheckWitsPage();

    if( invalidControl != NULL )
    {
        PgCtrl->ActivePage = WitsSheet;
        ActiveControl = invalidControl;

        return;
    }

    // Values look good. Can close dialog
    ModalResult = mrOk;
}


void __fastcall TSystemSettingsForm::CommTypeClick(TObject *Sender)
{
    // There are two identical tabs of comm settings tab sheets.
    // Find the parent of the sheet of the Sender, then enable
    // all edits on that sheet whose Tag matches that of the
    // Sender. All other edits are disabled.
    TRadioButton* pRB = dynamic_cast<TRadioButton*>(Sender);

    if( pRB == NULL )
        return;

    TTabSheet* parentSheet = (TTabSheet*)( pRB->Parent );

    for( int iCtrl = 0; iCtrl < parentSheet->ControlCount; iCtrl++ )
    {
        TEdit* pEdit = dynamic_cast<TEdit*>(parentSheet->Controls[iCtrl]);

        if( pEdit == NULL )
            continue;

        if( pEdit->Tag == pRB->Tag )
        {
            pEdit->Enabled = true;
            pEdit->Color   = clWindow;
        }
        else
        {
            pEdit->Enabled = false;
            pEdit->Color   = clBtnFace;
        }
    }
}


void __fastcall TSystemSettingsForm::BrowseRawLogFileBtnClick(TObject *Sender)
{
    SaveLogDlg->Title = "Raw Stream Data Log File";

    if( SaveLogDlg->Execute() )
        RawLogFileNameEdit->Text = SaveLogDlg->FileName;
}


void __fastcall TSystemSettingsForm::BrowseCalLogFileBtnClick(TObject *Sender)
{
    SaveLogDlg->Title = "Calibrated Stream Data Log File";

    if( SaveLogDlg->Execute() )
        CalLogFileNameEdit->Text = SaveLogDlg->FileName;
}


void __fastcall TSystemSettingsForm::BrowseRFCLogFileBtnClick(TObject *Sender)
{
    SaveLogDlg->Title = "RFC Idle Data Log File";

    if( SaveLogDlg->Execute() )
        RFCLogFileNameEdit->Text = SaveLogDlg->FileName;
}


void __fastcall TSystemSettingsForm::BrowseChartLogFileBtnClick(TObject *Sender)
{
    SaveLogDlg->Title = "Calibrated Idle Data Log File";

    if( SaveLogDlg->Execute() )
        ChartLogFileNameEdit->Text = SaveLogDlg->FileName;
}


void __fastcall TSystemSettingsForm::SavePWBtnClick(TObject *Sender)
{
    // Password is saved with explicit button click, rather than
    // when the dialog is closed.
    UnicodeString currPW     = GetSysAdminPassword();
    UnicodeString backdoorPW = GeneratePassword();

    if( ( currPW != CurrPWEdit->Text ) && ( backdoorPW != CurrPWEdit->Text ) )
    {
        MessageDlg( "The current password you have entered is incorrect.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = CurrPWEdit;
        return;
    }

    if( NewPWEdit1->Text.Trim().Length() == 0 )
    {
        MessageDlg( "Your new password cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewPWEdit1;
        return;
    }

    if( NewPWEdit1->Text != NewPWEdit2->Text )
    {
        MessageDlg( "Your new passwords do not match.", mtError, TMsgDlgButtons() << mbOK, 0 );

        ActiveControl = NewPWEdit1;
        return;
    }

    // Password looks good. Save it and clear the boxes
    SaveSysAdminPassword( NewPWEdit1->Text );

    // Clear the edits of any text
    CurrPWEdit->Text = "";
    NewPWEdit1->Text = "";
    NewPWEdit2->Text = "";

    MessageDlg( "Password successfully changed.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
}


void __fastcall TSystemSettingsForm::BrowseJobDirBtnClick(TObject *Sender)
{
    String jobDir = JobDirEdit->Text;

    if( SelectDirectory( "Select Job Directory", "Desktop", jobDir, TSelectDirExtOpts() << sdNewUI << sdNewFolder, NULL ) )
    {
        // Check that the drive location is different than the current job
        if( ExtractFileDrive( jobDir ).CompareIC( ExtractFileDrive( BackupDirEdit->Text ) ) != 0 )
            JobDirEdit->Text = jobDir;
        else
            MessageDlg( "Job Directory must be in a different drive than the current Backup Directory", mtError, TMsgDlgButtons() << mbOK, 0 );
    }
}


void __fastcall TSystemSettingsForm::ListEditorSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect)
{
    // Called by read-only grids
    CanSelect = false;
}


void TSystemSettingsForm::SetCommsCtrls( const COMMS_CFG& commCfg, const COMMS_SHEET_CTRLS& sheetCtrls )
{
    if( ( commCfg.portType >= 0 ) && ( commCfg.portType < sheetCtrls.pTypeCombo->Items->Count ) )
        sheetCtrls.pTypeCombo->ItemIndex = commCfg.portType;
    else
        sheetCtrls.pTypeCombo->ItemIndex = CT_SERIAL;

    switch( commCfg.portType )
    {
        case CT_SERIAL:
        case CT_USB:
        case CT_UDP:
            sheetCtrls.pPortNbrEdit->Text = commCfg.portName;

            // Force a default value if the param value is 0
            if( commCfg.portParam == 0 )
            {
                if( commCfg.portType == CT_UDP )
                    sheetCtrls.pParamEdit->Text = "10000";
                else
                    sheetCtrls.pParamEdit->Text = "115200";
            }
            else
            {
                sheetCtrls.pParamEdit->Text = IntToStr( (int)commCfg.portParam );
            }

            // For now, enable enable both controls
            sheetCtrls.pPortNbrEdit->Enabled = true;
            sheetCtrls.pParamEdit->Enabled   = true;

            break;

        default:
            sheetCtrls.pPortNbrEdit->Text    = "1";
            sheetCtrls.pPortNbrEdit->Enabled = false;
            sheetCtrls.pParamEdit->Text      = "115200";
            sheetCtrls.pParamEdit->Enabled   = false;
            break;
    }
}


TWinControl* TSystemSettingsForm::GetCommsCtrls( COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls )
{
    // Assume default for now
    commsCfg.portType  = CT_UNUSED;
    commsCfg.portName  = "";
    commsCfg.portParam = 0;

    int iTestPort;
    int iTestParam;

    switch( sheetCtrls.pTypeCombo->ItemIndex )
    {
        case CT_SERIAL:

            iTestPort = sheetCtrls.pPortNbrEdit->Text.ToIntDef( 0 );

            if( iTestPort <= 0 )
            {
                MessageDlg( "Invalid Comm Port number.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pPortNbrEdit;
            }

            iTestParam = sheetCtrls.pParamEdit->Text.ToIntDef( 0 );

            if( iTestParam <= 0 )
            {
                MessageDlg( "Invalid serial Bit Rate value.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pParamEdit;
            }

            commsCfg.portType  = CT_SERIAL;
            commsCfg.portName  = IntToStr( iTestPort );
            commsCfg.portParam = iTestParam;

            break;

        case CT_UDP:

            if( sheetCtrls.pPortNbrEdit->Enabled && ( sheetCtrls.pPortNbrEdit->Text.Trim().Length() == 0 ) )
            {
                MessageDlg( "Destination IP cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pPortNbrEdit;
            }

            iTestParam = sheetCtrls.pParamEdit->Text.ToIntDef( 0 );

            if( ( iTestParam <= 0 ) || ( iTestParam > 65535 ) )
            {
                MessageDlg( "Invalid UDP port number.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pParamEdit;
            }

            commsCfg.portType  = CT_UDP;
            commsCfg.portName  = sheetCtrls.pPortNbrEdit->Text;
            commsCfg.portParam = iTestParam;

            break;

        case CT_USB:

            if( sheetCtrls.pPortNbrEdit->Text.Trim().Length() == 0 )
            {
                MessageDlg( "USB Port Description cannot be blank.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pPortNbrEdit;
            }

            iTestParam = sheetCtrls.pParamEdit->Text.ToIntDef( 0 );

            if( iTestParam <= 0 )
            {
                MessageDlg( "Invalid USB Bit Rate value.", mtError, TMsgDlgButtons() << mbOK, 0 );
                return sheetCtrls.pParamEdit;
            }

            commsCfg.portType  = CT_USB;
            commsCfg.portName  = sheetCtrls.pPortNbrEdit->Text;
            commsCfg.portParam = iTestParam;

            break;

        case CT_UNUSED:
            break;

        default:
            // Should never occur, but if it does return something to indicate an error
            MessageDlg( "TSystemSettingsForm::GetCommsCtrls() : invalid port type.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return sheetCtrls.pTypeCombo;
    }

    // Fall through means all values are valid
    return NULL;
}


void TSystemSettingsForm::LoadCommsPage( void )
{
    COMMS_CFG wtttsCommCfg;
    COMMS_CFG baseCommCfg;
    COMMS_CFG mgmtCommCfg;

    GetCommSettings( DCT_WTTTS, wtttsCommCfg );
    GetCommSettings( DCT_BASE,  baseCommCfg );
    GetCommSettings( DCT_MGMT,  mgmtCommCfg );

    m_OrigCommsSettings.devType      = DT_WTTTS;
    m_OrigCommsSettings.wtttsCommCfg = wtttsCommCfg;
    m_OrigCommsSettings.baseCommCfg  = baseCommCfg;
    m_OrigCommsSettings.mgmtCommCfg  = mgmtCommCfg;

    SetCommsCtrls( wtttsCommCfg, m_WTTTSCommsCtrls );
    SetCommsCtrls( baseCommCfg,  m_BaseCommsCtrls );
    SetCommsCtrls( mgmtCommCfg,  m_MgmtCommsCtrls );

    // Force a combo click to refresh the above to control sets
    BaseRadioPortTypeComboClick( BaseRadioPortTypeCombo );
    TesTORKPortTypeComboClick( TesTORKPortTypeCombo );

    // Get auto-assign options now
    bool bAutoAssignPorts = GetAutoAssignCommPorts();

    m_OrigCommsSettings.bAutoAssignPorts = bAutoAssignPorts;

    AutoAssignPortsCB->Checked = bAutoAssignPorts;

    // Data Logging. Note that we dont need to track data logging changes
    // in the original settings struct
    UnicodeString logFileName;

    EnableRawLogCB->Checked  = GetDataLogging( DLT_RAW_DATA, logFileName );
    RawLogFileNameEdit->Text = logFileName;

    EnableCalLogCB->Checked  = GetDataLogging( DLT_CAL_DATA, logFileName );
    CalLogFileNameEdit->Text = logFileName;

    EnableRFCLogCB->Checked  = GetDataLogging( DLT_RFC_DATA, logFileName );
    RFCLogFileNameEdit->Text = logFileName;

    EnableChartLogCB->Checked  = GetDataLogging( DLT_CHART_DATA, logFileName );
    ChartLogFileNameEdit->Text = logFileName;

    // Show current TesTORK radio channel number
    BRRadNbrComboClick( BRRadNbrCombo );

    // Load the BR4 function combo
    m_OrigCommsSettings.br3OutFcn = GetBaseRadioOut3Fcn();

    if( ( m_OrigCommsSettings.br3OutFcn >= 0 ) && ( m_OrigCommsSettings.br3OutFcn < BROut3FcnCombo->Items->Count ) )
        BROut3FcnCombo->ItemIndex = (int)m_OrigCommsSettings.br3OutFcn;
    else
        BROut3FcnCombo->ItemIndex = 0;
}


void TSystemSettingsForm::LoadCalPage( void )
{
    FormattedCalDataEditor->Strings->Clear();
    RawCalDataEditor->Strings->Clear();

    // Load raw data view first
    if( m_poller != NULL )
    {
       TStringList* pCaptions = new TStringList();
       TStringList* pValues   = new TStringList();

       if( m_poller->GetCalDataRaw( pCaptions, pValues ) )
       {
           for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
               RawCalDataEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
       }

       if( m_poller->GetCalDataFormatted( pCaptions, pValues ) )
       {
           for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
               FormattedCalDataEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
       }

       delete pCaptions;
       delete pValues;
    }

    // Can only zero readings and change battery if we have a poller and it is a real device.
    ZeroReadingsBtn->Enabled = ( ( m_poller != NULL ) && ( m_poller->DeviceType == DT_WTTTS ) );
    NewBattBtn->Enabled      = ZeroReadingsBtn->Enabled;

    // The WriteToDevBtn's tag is used to indicate if the user has
    // uploaded new data. Clear it on load.
    WriteToDevBtn->Tag = 0;
}


void TSystemSettingsForm::LoadMiscPage( void )
{
    // Clear password field edits
    CurrPWEdit->Text = "";
    NewPWEdit1->Text = "";
    NewPWEdit2->Text = "";

    // Load job data dir
    JobDirEdit->Text = GetJobDataDir();

    m_OrigMiscSettings.sJobDir = JobDirEdit->Text;

    SetJobStatusBtns();

    // Get process priority. Force default setting to start, but
    // override that when we find the priority in the list.
    ProcPriorityCombo->ItemIndex = 0;

    DWORD procPriority = GetProcessPriority();

    m_OrigMiscSettings.dwProcPriority = procPriority;

    for( int iItem = 0; iItem < NBR_PROC_PRIORITIES; iItem++ )
    {
        if( ProcPriorities[iItem].procEnum == procPriority )
        {
            ProcPriorityCombo->ItemIndex = iItem;
            break;
        }
    }

     // Connection Filtering
    ConnMinTurnsEdit->Text = FloatToStrF( GetConnMinTurnsValue(), ffFixed, 7, 1 );

    m_OrigMiscSettings.dMinTurns = GetConnMinTurnsValue();

    // AS Nag
    ASOverrideNagCB->Checked = GetNagOnAutoShoulderOverride();

    m_OrigMiscSettings.bASOverrideNag = ASOverrideNagCB->Checked;

    // Temp comp
    EnableTempCompCB->Checked = GetTempCompEnabled();

    m_OrigMiscSettings.bEnableTempComp = EnableTempCompCB->Checked;

    // Get the backup save settings
    String sBackupDir;
    bool bEnableBackup = GetForceBackup( sBackupDir );

    BackupDirEdit->Text   = sBackupDir;
    BackupSaveCB->Checked = bEnableBackup;

    m_OrigMiscSettings.sBackupDir       = sBackupDir;
    m_OrigMiscSettings.bForceBackupSave = bEnableBackup;

    // Load data averaging form registry. Format of the factor value
    // depends on the averaging type.
    AVERAGING_PARAMS avgParams;
    GetAveragingSettings( AT_TORQUE, avgParams );

    m_OrigMiscSettings.avgTQParams = avgParams;

    // Variance is shown as an integer 0-100 percent value
    TorqueAvgVarEdit->Text = FloatToStrF( avgParams.absVariance, ffFixed, 7, 0 );
    TorqueCombo->ItemIndex = avgParams.avgLevel;

    // Get tension settings now
    GetAveragingSettings( AT_TENSION, avgParams );
    m_OrigMiscSettings.avgTNParams = avgParams;

    // Variance is shown as an integer 0-100 percent value
    TensionAvgVarEdit->Text = FloatToStrF( avgParams.absVariance, ffFixed, 7, 0 );
    TensionCombo->ItemIndex = avgParams.avgLevel;

    // Load PDS settings.
    m_OrigMiscSettings.bPDSAlarmEn = GetPDSMonEnabled();

    PDSMonEnabledCB->Checked = m_OrigMiscSettings.bPDSAlarmEn;
}


void TSystemSettingsForm::LoadInterlockPage( void )
{
    // Populate labels with current job units of measure
    CDSRelUnitLB->Caption        = GetUnitsText( m_uomType, MT_WEIGHT_SHORT );
    CDSSlipLockedUnitLB->Caption = GetUnitsText( m_uomType, MT_WEIGHT_SHORT );
    CDSLockedUnitLB->Caption     = GetUnitsText( m_uomType, MT_WEIGHT_SHORT );
    ARTqStartUnitLB->Caption     = GetUnitsText( m_uomType, MT_TORQUE_SHORT );
    ARTqCrossedUnitLB->Caption   = GetUnitsText( m_uomType, MT_TORQUE_SHORT );

    int cdsRelsed;
    int cdsSlipsLocked;
    int cdslockSlipRel;

    // Get CDS and Slips from registry
    GetCDSSlipsSettings( m_uomType, cdsRelsed, cdsSlipsLocked, cdslockSlipRel );

    m_OrigInterlockSettings.iCDSRelsed      = cdsRelsed;
    m_OrigInterlockSettings.iCDSSlipsLocked = cdsSlipsLocked;
    m_OrigInterlockSettings.iCDSlockSlipRel = cdslockSlipRel;

    // populate values to TEdit
    CDSRelEdit->Text        = IntToStr( cdsRelsed );
    CDSSlipLockedEdit->Text = IntToStr( cdsSlipsLocked );
    CDSLockedEdit->Text     = IntToStr( cdslockSlipRel );

    // Get interlock release time from registry and populate edit
    MinRelTimeEdit->Text = IntToStr( GetInterlockReleaseTime() );

    m_OrigInterlockSettings.iReleaseTime = GetInterlockReleaseTime();

    // Get Interlock enable status
    InterlockEnCB->Checked = GetInterlockEn();

    m_OrigInterlockSettings.bIntLockEnabled = InterlockEnCB->Checked;

    // Load Auto-Record settings
    GetAutoRecSettings( m_OrigAutoRecSettings.arSettings );

    EnableARCB->Checked       = m_OrigAutoRecSettings.arSettings.bAutoRecEnabled;
    ARRPMToStartEdit->Text    = IntToStr( m_OrigAutoRecSettings.arSettings.minRPMToStart );
    ARTqToStartEdit->Text     = IntToStr( m_OrigAutoRecSettings.arSettings.minTorqueToStart );
    ARTimeToStopEdit->Text    = IntToStr( m_OrigAutoRecSettings.arSettings.stopTime );
    ARXThreadThreshEdit->Text = IntToStr( m_OrigAutoRecSettings.arSettings.crossThreadThresh );
    AutoRecAlarmEnCB->Checked = m_OrigAutoRecSettings.arSettings.bAlarmOnError;
}


void TSystemSettingsForm::LoadWitsPage( void )
{
    // WITS port settings
    COMMS_CFG commsCfg;
    GetWitsPortsSettings( commsCfg );

    WitsPortTypeCombo->ItemIndex = commsCfg.portType;
    WitsPortAddressEdit->Text    = commsCfg.portName;
    WitsParameterEdit->Text      = UIntToStr( (UINT) commsCfg.portParam );

    // Simulate a combo click to ensure the settings show correctly
    WitsPortTypeComboClick( WitsPortTypeCombo );

    // Now load RFC Header settings
    bool bRfcEnabled;
    int iRfcCode;
    String sRfcData;

    GetWitsRfcHdrSettings( bRfcEnabled, iRfcCode, sRfcData );

    WitsRFCHeaderCB->Checked = bRfcEnabled;
    WitsRFCCodeEdit->Text    = FormatWITSCode( iRfcCode );
    WitsRFCHeaderEdit->Text  = sRfcData;

    // Fake a click to get default values
    WitsRFCHeaderCBClick( WitsRFCHeaderCB );

    // Load Stream header settings
    bool bStreamEnabled;
    int iStreamCode;
    String sStreamData;

    GetWitsStreamHdrSettings( bStreamEnabled, iStreamCode, sStreamData );

    WitsStreamHeaderCB->Checked = bStreamEnabled;
    WitsStreamCodeEdit->Text    = FormatWITSCode( iStreamCode );
    WitsStreamHeaderEdit->Text  = sStreamData;

    // Fake a click to get default values
    WitsStreamHeaderCBClick( WitsStreamHeaderCB );

    // Finally set the Original settings structure
    m_OrigWitsSettings.portType          = commsCfg.portType;
    m_OrigWitsSettings.sPortName         = commsCfg.portName;
    m_OrigWitsSettings.dwPortParam       = commsCfg.portParam;
    m_OrigWitsSettings.bRfcHdrEnabled    = bRfcEnabled;
    m_OrigWitsSettings.iRfcHdrCode       = iRfcCode;
    m_OrigWitsSettings.sRfcHdrString     = sRfcData;
    m_OrigWitsSettings.bStreamHdrEnabled = bStreamEnabled;
    m_OrigWitsSettings.iStreamHdrCode    = iStreamCode;
    m_OrigWitsSettings.sStreamHdrString  = sStreamData;

    // Load WITS Rfc List
    WitsRfcListView->Clear();

    int iNumFields = GetWitsFieldSettings( TSubDevice::eWPT_RFC, NULL, NULL );

    WITS_FIELD_SETTING* fieldRfcList = new WITS_FIELD_SETTING[iNumFields];

    GetWitsFieldSettings( TSubDevice::eWPT_RFC, fieldRfcList, iNumFields );

    for( int iFieldEntry = 0; iFieldEntry < iNumFields; iFieldEntry++ )
    {
        TListItem* newItem = WitsRfcListView->Items->Add();

        newItem->Checked = fieldRfcList[iFieldEntry].bEnabled;
        newItem->Data    = (void*) fieldRfcList[iFieldEntry].iFieldEnum;
        newItem->SubItems->Add( FormatWITSCode( fieldRfcList[iFieldEntry].iCode ) );
        newItem->SubItems->Add( TSubDevice::WitsFieldCaptions[fieldRfcList[iFieldEntry].iFieldEnum] );
    }

    delete [] fieldRfcList;

    // Load WITS Stream List
    WitsStreamListView->Clear();

    iNumFields = GetWitsFieldSettings( TSubDevice::eWPT_Stream, NULL, NULL );

    WITS_FIELD_SETTING* fieldStreamList = new WITS_FIELD_SETTING[iNumFields];

    GetWitsFieldSettings( TSubDevice::eWPT_Stream, fieldStreamList, iNumFields );

    for( int iFieldEntry = 0; iFieldEntry < iNumFields; iFieldEntry++ )
    {
        TListItem* newItem = WitsStreamListView->Items->Add();

        newItem->Checked = fieldStreamList[iFieldEntry].bEnabled;
        newItem->Data    = (void*) fieldStreamList[iFieldEntry].iFieldEnum;
        newItem->SubItems->Add( FormatWITSCode( fieldStreamList[iFieldEntry].iCode ) );
        newItem->SubItems->Add( TSubDevice::WitsFieldCaptions[fieldStreamList[iFieldEntry].iFieldEnum] );
    }

    delete [] fieldStreamList;

    // Ensure button captions are refreshed and select first WITS tab
    WitsListPageControl->ActivePageIndex = 0;
    WitsListPageControlChange( WitsListPageControl );
}


TWinControl* TSystemSettingsForm::CheckCommsPage( void )
{
    COMMS_CFG temp;

    TWinControl* commsErrCtrl = GetCommsCtrls( temp, m_WTTTSCommsCtrls );

    if( commsErrCtrl != NULL )
    {
        DevConnPgCtrl->ActivePage = WTTTSSheet;
        return commsErrCtrl;
    }

    commsErrCtrl = GetCommsCtrls( temp, m_BaseCommsCtrls );

    if( commsErrCtrl != NULL )
    {
        DevConnPgCtrl->ActivePage = BaseRadioSheet;
        return commsErrCtrl;
    }

    if( EnableRawLogCB->Checked )
    {
        if( RawLogFileNameEdit->Text.Trim().Length() == 0 )
        {
            MessageDlg( "You must specify a file name to enable logging.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return RawLogFileNameEdit;
        }
    }

    if( EnableCalLogCB->Checked )
    {
        if( CalLogFileNameEdit->Text.Trim().Length() == 0 )
        {
            MessageDlg( "You must specify a file name to enable logging.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return CalLogFileNameEdit;
        }
    }

    if( EnableRFCLogCB->Checked )
    {
        if( RFCLogFileNameEdit->Text.Trim().Length() == 0 )
        {
            MessageDlg( "You must specify a file name to enable logging.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return RFCLogFileNameEdit;
        }
    }

    if( EnableChartLogCB->Checked )
    {
        if( ChartLogFileNameEdit->Text.Trim().Length() == 0 )
        {
            MessageDlg( "You must specify a file name to enable logging.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return ChartLogFileNameEdit;
        }
    }

    // Fall through means all page controls valid
    return NULL;
}


TWinControl* TSystemSettingsForm::CheckCalPage( void )
{
    // If a calibration has been loaded, but not written to the device,
    // warn the user.
    if( WriteToDevBtn->Tag == 1 )
    {
        int mrResult = MessageDlg( "You have loaded calibration data but not written the data to the device. "
                                   "If you exit now, the software will continue to use this calibration data, "
                                   "but the data stored on the device will be different."
                                   "\r  Press OK to continue with different calibrations."
                                   "\r  Press Cancel to reconsider.",
                                   mtWarning, TMsgDlgButtons() << mbOK << mbCancel, 0 );

        if( mrResult != mrOk )
            return FormattedCalDataEditor;
    }

    return NULL;
}


TWinControl* TSystemSettingsForm::CheckMiscPage( void )
{
    UnicodeString sInvalidValue( "You have entered an invalid value." );

    // Enforce that the job directory exists
    if( !DirectoryExists( JobDirEdit->Text ) )
    {
        MessageDlg( "You must specify a Job Directory.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return JobDirEdit;
    }

    if( !IsValidFloat( ConnMinTurnsEdit ) )
    {
        MessageDlg( sInvalidValue, mtError, TMsgDlgButtons() << mbOK, 0 );
        return ConnMinTurnsEdit;
    }

    if( ConnMinTurnsEdit->Text.ToDouble() > MIN_SAVE_TURNS )
    {
        MessageDlg( sInvalidValue + " Value must be " + IntToStr( MIN_SAVE_TURNS ) + " or less.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return ConnMinTurnsEdit;
    }

    if( TorqueAvgVarEdit->Text.ToIntDef( -1 ) < 0 )
    {
        MessageDlg( sInvalidValue, mtError, TMsgDlgButtons() << mbOK, 0 );
        return TorqueAvgVarEdit;
    }

    if( TensionAvgVarEdit->Text.ToIntDef( -1 ) < 0 )
    {
        MessageDlg( sInvalidValue, mtError, TMsgDlgButtons() << mbOK, 0 );
        return TensionAvgVarEdit;
    }

    return NULL;
}


TWinControl* TSystemSettingsForm::CheckInterlockPage( void )
{
    String sInvalidValue( "You have entered an invalid value." );

    for( int iCtrl = 0; iCtrl < SettingsGB->ControlCount; iCtrl++ )
    {
        TEdit* anEdit = dynamic_cast<TEdit*>( SettingsGB->Controls[iCtrl] );

        if( anEdit != NULL )
        {
            if( !IsValidInt( anEdit ) )
            {
                MessageDlg( sInvalidValue, mtError, TMsgDlgButtons() << mbOK, 0 );
                return anEdit;
            }
        }
    }

    // Secondary validation - for interlock release time
    if( MinRelTimeEdit->Text.ToIntDef( 0 ) <= 0 )
    {
        MessageDlg( sInvalidValue, mtError, TMsgDlgButtons() << mbOK, 0 );
        return  MinRelTimeEdit;
    }

    // Secondary validation: make sure all thresholds are correct relative
    // to each other. To have reached this point, the edits have valid
    // int values.
    int CDSRelThresh         = CDSRelEdit->Text.ToInt();
    int CDSSlipLockedThesh   = CDSSlipLockedEdit->Text.ToInt();
    int CDSLockSlipRelThresh = CDSLockedEdit->Text.ToInt();

    if( CDSRelThresh > CDSSlipLockedThesh )
    {
        MessageDlg( "CDS release tension must be less than CDS/Slips locked tension.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return CDSRelEdit;
    }

    if( CDSSlipLockedThesh > CDSLockSlipRelThresh )
    {
        MessageDlg( "CDS/Slips locked tension must be less than CDS locked tension.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return CDSSlipLockedEdit;
    }

    // Check Auto-Record settings
    for( int iCtrl = 0; iCtrl < AutoRecordGB->ControlCount; iCtrl++ )
    {
        TEdit* anEdit = dynamic_cast<TEdit*>( AutoRecordGB->Controls[iCtrl] );

        if( anEdit != NULL )
        {
            if( !IsValidInt( anEdit, true ) )
            {
                MessageDlg( sInvalidValue, mtError, TMsgDlgButtons() << mbOK, 0 );
                return anEdit;
            }
        }
    }

    return NULL;
}


TWinControl* TSystemSettingsForm::CheckWitsPage( void )
{

    // Check port details
    if( WitsPortAddressEdit->Text.IsEmpty() )
    {
        MessageDlg( "WITS Port/Address field may not be empty.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return WitsPortAddressEdit;
    }

    int iParam = StrToIntDef( WitsParameterEdit->Text, -1 );

    if( ( iParam < 0 ) && ( WitsPortTypeCombo->ItemIndex != CT_UNUSED ) )
    {
        MessageDlg( "WITS Parameter field must a positive integer.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return WitsRFCCodeEdit;
    }

    // Check RFC Header code & data
    if( WitsRFCHeaderCB->Checked )
    {
        int iHdrCode = StrToIntDef( WitsRFCCodeEdit->Text, 0 );

        if( iHdrCode < 1 || iHdrCode > 9999 )
        {
            MessageDlg( "WITS RFC Header code must be an integer value from 1 to 9999.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return WitsRFCCodeEdit;
        }

        if( WitsRFCCodeEdit->Text.Length() != 4 )
        {
            MessageDlg( "WITS RFC Header code must be four digits long.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return WitsRFCCodeEdit;
        }

        if( WitsRFCHeaderEdit->Text.IsEmpty() )
        {
            MessageDlg( "WITS RFC Header data may not be empty.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return WitsRFCHeaderEdit;
        }
    }

    // Check Stream Header code & data
    if( WitsStreamHeaderCB->Checked )
    {
        int iHdrCode = StrToIntDef( WitsStreamCodeEdit->Text, 0 );

        if( iHdrCode < 1 || iHdrCode > 9999 )
        {
            MessageDlg( "WITS Stream Header code must be an integer value from 1 to 9999.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return WitsStreamCodeEdit;
        }

        if( WitsStreamCodeEdit->Text.Length() != 4 )
        {
            MessageDlg( "WITS Stream  Header code must be four digits long.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return WitsStreamCodeEdit;
        }

        if( WitsStreamHeaderEdit->Text.IsEmpty() )
        {
            MessageDlg( "WITS Stream Header data may not be empty.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return WitsStreamHeaderEdit;
        }
    }

    return NULL;
}


DWORD TSystemSettingsForm::SaveCommsPage( void )
{
    bool bSettingsChanged = false;

    // All controls guaranteed to be valid with this function is called.

    // Save comms settings
    COMMS_CFG wtttsCommCfg;
    COMMS_CFG baseCommCfg;
    COMMS_CFG mgmtCommCfg;

    GetCommsCtrls( wtttsCommCfg, m_WTTTSCommsCtrls );
    GetCommsCtrls( baseCommCfg,  m_BaseCommsCtrls );
    GetCommsCtrls( mgmtCommCfg,  m_MgmtCommsCtrls );

    if( ( wtttsCommCfg.portType != m_OrigCommsSettings.wtttsCommCfg.portType ) || ( wtttsCommCfg.portName != m_OrigCommsSettings.wtttsCommCfg.portName ) || ( wtttsCommCfg.portParam != m_OrigCommsSettings.wtttsCommCfg.portParam ) )
        bSettingsChanged = true;

    if( ( baseCommCfg.portType != m_OrigCommsSettings.baseCommCfg.portType ) || ( baseCommCfg.portName != m_OrigCommsSettings.baseCommCfg.portName ) || ( baseCommCfg.portParam != m_OrigCommsSettings.baseCommCfg.portParam ) )
        bSettingsChanged = true;

    if( ( mgmtCommCfg.portType != m_OrigCommsSettings.mgmtCommCfg.portType ) || ( mgmtCommCfg.portName != m_OrigCommsSettings.mgmtCommCfg.portName ) || ( mgmtCommCfg.portParam != m_OrigCommsSettings.mgmtCommCfg.portParam ) )
        bSettingsChanged = true;

    SaveCommSettings( DCT_WTTTS, wtttsCommCfg );
    SaveCommSettings( DCT_BASE,  baseCommCfg );
    SaveCommSettings( DCT_MGMT,  mgmtCommCfg );

    if( AutoAssignPortsCB->Checked != m_OrigCommsSettings.bAutoAssignPorts )
        bSettingsChanged = true;

    SaveAutoAssignCommPorts( AutoAssignPortsCB->Checked );

    // Save raw logging settings. Note that data logging changes do not require
    // notification of settings being changed
    SaveDataLogging( DLT_RAW_DATA,   EnableRawLogCB->Checked,   RawLogFileNameEdit->Text   );
    SaveDataLogging( DLT_CAL_DATA,   EnableCalLogCB->Checked,   CalLogFileNameEdit->Text   );
    SaveDataLogging( DLT_RFC_DATA,   EnableRFCLogCB->Checked,   RFCLogFileNameEdit->Text   );
    SaveDataLogging( DLT_CHART_DATA, EnableChartLogCB->Checked, ChartLogFileNameEdit->Text );

    if( BROut3FcnCombo->ItemIndex != (int)m_OrigCommsSettings.br3OutFcn )
        bSettingsChanged = true;

    SaveBaseRadioOut3Fcn( (eBR3OutFcn)BROut3FcnCombo->ItemIndex );

    if( bSettingsChanged )
        return COMMS_SETTINGS_CHANGED;
    else
        return 0;
}


DWORD TSystemSettingsForm::SaveCalPage( void )
{
    bool bSettingsChanged = false;

    // Nothing to save on this page.

    if( bSettingsChanged )
        return CAL_SETTINGS_CHANGED;

    return 0;
}


DWORD TSystemSettingsForm::SaveMiscPage( void )
{
    bool bSettingsChanged = false;

    // All controls guaranteed to be valid with this function is called.

    if( JobDirEdit->Text != m_OrigMiscSettings.sJobDir )
        bSettingsChanged = true;

    // Save job data dir
    SetJobDataDir( JobDirEdit->Text );

    if( ProcPriorities[ ProcPriorityCombo->ItemIndex ].procEnum != m_OrigMiscSettings.dwProcPriority )
        bSettingsChanged = true;

    // Save process setting
    SetProcessPriority( ProcPriorities[ ProcPriorityCombo->ItemIndex ].procEnum );

    if( ConnMinTurnsEdit->Text.ToDouble() != m_OrigMiscSettings.dMinTurns )
        bSettingsChanged = true;

    // Connection filtering
    SaveConnMinTurnsValue( ConnMinTurnsEdit->Text.ToDouble() );

    if( ASOverrideNagCB->Checked != m_OrigMiscSettings.bASOverrideNag )
        bSettingsChanged = true;

    SaveNagOnAutoShoulderOverride( ASOverrideNagCB->Checked );

    // Auto-shoulder
    if( BackupDirEdit->Text != m_OrigMiscSettings.sBackupDir )
        bSettingsChanged = true;

    if( BackupSaveCB->Checked != m_OrigMiscSettings.bForceBackupSave )
        bSettingsChanged = true;

    SaveForceBackup( BackupSaveCB->Checked, BackupDirEdit->Text );

    // Save torque level setting
    AVERAGING_PARAMS avgParams;
    avgParams.avgLevel    = (AVG_LEVEL) TorqueCombo->ItemIndex;
    avgParams.absVariance = TorqueAvgVarEdit->Text.ToDouble();

    if( ( avgParams.avgLevel != m_OrigMiscSettings.avgTQParams.avgLevel ) || ( avgParams.absVariance != m_OrigMiscSettings.avgTQParams.absVariance ) )
        bSettingsChanged = true;

    SaveAveragingSettings( AT_TORQUE, avgParams );

    // Save tension level setting
    avgParams.avgLevel    = (AVG_LEVEL) TensionCombo->ItemIndex;
    avgParams.absVariance = TensionAvgVarEdit->Text.ToDouble();

    if( ( avgParams.avgLevel != m_OrigMiscSettings.avgTNParams.avgLevel ) || ( avgParams.absVariance != m_OrigMiscSettings.avgTNParams.absVariance ) )
        bSettingsChanged = true;

    SaveAveragingSettings( AT_TENSION, avgParams );

    // Save temp comp
    SaveTempCompEnabled( EnableTempCompCB->Checked );

    if( m_OrigMiscSettings.bEnableTempComp != EnableTempCompCB->Checked )
        bSettingsChanged = true;

    // Save PDS setting
    if( PDSMonEnabledCB->Checked != m_OrigMiscSettings.bPDSAlarmEn )
    {
        SavePDSMonEnabled( PDSMonEnabledCB->Checked );

        bSettingsChanged = true;
    }

    if( bSettingsChanged )
        return MISC_SETTINGS_CHANGED;

    return 0;
}


DWORD TSystemSettingsForm::SaveInterlockPage( void )
{
    DWORD dwChangedSettings = 0;

    bool bSettingsChanged = false;

    // Read current values - will be valid at this point
    int cdsRelsed      = CDSRelEdit->Text.ToInt();
    int cdsSlipsLocked = CDSSlipLockedEdit->Text.ToInt();
    int cdslockSlipRel = CDSLockedEdit->Text.ToInt();

    if( ( cdsRelsed != m_OrigInterlockSettings.iCDSRelsed ) || ( cdsSlipsLocked != m_OrigInterlockSettings.iCDSSlipsLocked ) || ( cdslockSlipRel != m_OrigInterlockSettings.iCDSlockSlipRel ) )
        bSettingsChanged = true;

    // Save CDS and Slips Settings to registry
    SaveCDSSlipsSettings( m_uomType, cdsRelsed, cdsSlipsLocked, cdslockSlipRel );

    if( MinRelTimeEdit->Text.ToInt() != m_OrigInterlockSettings.iReleaseTime  )
        bSettingsChanged = true;

    // Save min release time to registry
    SaveInterlockReleaseTime( MinRelTimeEdit->Text.ToInt() );

    if( InterlockEnCB->Checked != m_OrigInterlockSettings.bIntLockEnabled )
        bSettingsChanged = true;

    // Save interlock enable status
    SaveInterlockEn( InterlockEnCB->Checked );

    if( bSettingsChanged )
        dwChangedSettings |= INTERLOCK_SETTINGS_CHANGED;

    // Save Auto-Record settings - reset the settings changed flag to start
    bSettingsChanged = false;

    AUTO_REC_SETTINGS arNewSettings;

    arNewSettings.bAutoRecEnabled   = EnableARCB->Checked;
    arNewSettings.minRPMToStart     = ARRPMToStartEdit->Text.ToInt();
    arNewSettings.minTorqueToStart  = ARTqToStartEdit->Text.ToInt();
    arNewSettings.stopTime          = ARTimeToStopEdit->Text.ToInt();
    arNewSettings.crossThreadThresh = ARXThreadThreshEdit->Text.ToInt();
    arNewSettings.bAlarmOnError     = AutoRecAlarmEnCB->Checked;

    SaveAutoRecSettings( arNewSettings );

    bSettingsChanged |= ( arNewSettings.bAutoRecEnabled   != m_OrigAutoRecSettings.arSettings.bAutoRecEnabled );
    bSettingsChanged |= ( arNewSettings.minRPMToStart     != m_OrigAutoRecSettings.arSettings.minRPMToStart );
    bSettingsChanged |= ( arNewSettings.minTorqueToStart  != m_OrigAutoRecSettings.arSettings.minTorqueToStart );
    bSettingsChanged |= ( arNewSettings.stopTime          != m_OrigAutoRecSettings.arSettings.stopTime );
    bSettingsChanged |= ( arNewSettings.crossThreadThresh != m_OrigAutoRecSettings.arSettings.crossThreadThresh );
    bSettingsChanged |= ( arNewSettings.bAlarmOnError     != m_OrigAutoRecSettings.arSettings.bAlarmOnError );

    // Set the auto-rec changed flag if necessary
    if( bSettingsChanged )
        dwChangedSettings |= AUTO_REC_SETTINGS_CHANGED;

    return dwChangedSettings;
}


DWORD TSystemSettingsForm::SaveWitsPage( void )
{
    bool bSettingsChanged = false;

    // Port settings check
    COMMS_CFG commsCfg;
    commsCfg.portType  = (COMM_TYPE) WitsPortTypeCombo->ItemIndex;
    commsCfg.portName  = WitsPortAddressEdit->Text;
    commsCfg.portParam = StrToIntDef( WitsParameterEdit->Text, 0 );

    SaveWitsPortsSettings( commsCfg );

    if( ( commsCfg.portType != m_OrigWitsSettings.portType ) || ( commsCfg.portName != m_OrigWitsSettings.sPortName ) || ( commsCfg.portParam != m_OrigWitsSettings.dwPortParam ) )
        bSettingsChanged = true;

    // RFC settings check
    bool bRfcEnabled = WitsRFCHeaderCB->Checked;
    int iRfcCode     = StrToIntDef( WitsRFCCodeEdit->Text, 0 );
    String sRfcData  = WitsRFCHeaderEdit->Text;

    SaveWitsRfcHdrSettings( bRfcEnabled, iRfcCode, sRfcData );

    if( ( bRfcEnabled != m_OrigWitsSettings.bRfcHdrEnabled ) || ( iRfcCode != m_OrigWitsSettings.iRfcHdrCode ) || ( sRfcData != m_OrigWitsSettings.sRfcHdrString ) )
        bSettingsChanged = true;

    // Stream settings check
    bool bStreamEnabled = WitsStreamHeaderCB->Checked;
    int iStreamCode     = StrToIntDef( WitsStreamCodeEdit->Text, 0 );
    String sStreamData  = WitsStreamHeaderEdit->Text;

    SaveWitsStreamHdrSettings( bStreamEnabled, iStreamCode, sStreamData );

    if( ( bStreamEnabled != m_OrigWitsSettings.bStreamHdrEnabled ) || ( iStreamCode != m_OrigWitsSettings.iStreamHdrCode ) || ( sStreamData != m_OrigWitsSettings.sStreamHdrString ) )
        bSettingsChanged = true;

    // Save RFC List
    int iNumEntries = WitsRfcListView->Items->Count;

    WITS_FIELD_SETTING* fieldRfcSettings = new WITS_FIELD_SETTING[iNumEntries];

    // Loop through the listview, adding each to our array
    for( int iCurrEntry = 0; iCurrEntry < iNumEntries; iCurrEntry++ )
    {
        fieldRfcSettings[iCurrEntry].bEnabled   = WitsRfcListView->Items->Item[iCurrEntry]->Checked;
        fieldRfcSettings[iCurrEntry].iCode      = StrToInt( WitsRfcListView->Items->Item[iCurrEntry]->SubItems->Strings[eWFC_Code] );
        fieldRfcSettings[iCurrEntry].iFieldEnum = (int) WitsRfcListView->Items->Item[iCurrEntry]->Data;
    }

    // Save the RFC fields
    SaveWitsFieldSettings( TSubDevice::eWPT_RFC, fieldRfcSettings, iNumEntries );

    delete [] fieldRfcSettings;

    // Save Stream List
    iNumEntries = WitsStreamListView->Items->Count;

    WITS_FIELD_SETTING* fieldStreamSettings = new WITS_FIELD_SETTING[iNumEntries];

    // Loop through the listview, adding each to our array
    for( int iCurrEntry = 0; iCurrEntry < iNumEntries; iCurrEntry++ )
    {
        fieldStreamSettings[iCurrEntry].bEnabled   = WitsStreamListView->Items->Item[iCurrEntry]->Checked;
        fieldStreamSettings[iCurrEntry].iCode      = StrToInt( WitsStreamListView->Items->Item[iCurrEntry]->SubItems->Strings[eWFC_Code] );
        fieldStreamSettings[iCurrEntry].iFieldEnum = (int) WitsStreamListView->Items->Item[iCurrEntry]->Data;
    }

    // Save the Stream fields
    SaveWitsFieldSettings( TSubDevice::eWPT_Stream, fieldStreamSettings, iNumEntries );

    delete [] fieldStreamSettings;

    // Done saving WITS settings
    if( bSettingsChanged )
        return WITS_SETTINGS_CHANGED;

    return 0;
}


//
// Local Event Handlers
//

void __fastcall TSystemSettingsForm::RFCPollTimerTimer(TObject *Sender)
{
    // If we have a device, poll and display its last status
    if( m_poller != NULL )
    {
        TStringList* pCaptions = new TStringList();
        TStringList* pValues   = new TStringList();

        if( m_poller->GetLastStatusPktInfo( pCaptions, pValues ) )
        {
            for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
                LastRFCEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
        }

        // If either the Read From Dev or the Write To Dev buttons are
        // disabled and we the poller is idle, assume that a read/write
        // operation has completed. Reload the cal data in that case.
        if( m_poller->DeviceIsIdle )
        {
            if( !WriteToDevBtn->Enabled || !ReadFromDevBtn->Enabled )
                LoadCalPage();
        }

        // Update the cal page read/write to device buttons. These are
        // only enabled if the device is idle
        WriteToDevBtn->Enabled  = m_poller->DeviceIsIdle;
        ReadFromDevBtn->Enabled = m_poller->DeviceIsIdle;

        // Check for base radio status packet.
        if( m_poller->GetLastRadioStatusPktInfo( pCaptions, pValues ) )
        {
            for( int iItem = 0; iItem < pCaptions->Count; iItem++ )
                LastStatusEditor->Values[ pCaptions->Strings[iItem] ] = pValues->Strings[ iItem ];
        }

        delete pCaptions;
        delete pValues;
    }
    else
    {
        WriteToDevBtn->Enabled  = false;
        ReadFromDevBtn->Enabled = false;
    }
}


void __fastcall TSystemSettingsForm::SaveCalDatBtnClick(TObject *Sender)
{
    // Save the data as is in the current view.
    if( SaveCalDataDlg->Execute() )
    {
        TIniFile* pIniFile = new TIniFile( SaveCalDataDlg->FileName );

        pIniFile->EraseSection( calIniDataSection );

        // For each key in the editor, look for a corresponding section in the
        // ini file. If the section is not found, skip it.
        for( int iKey = 1; iKey <= FormattedCalDataEditor->Strings->Count; iKey++ )
        {
            UnicodeString sKey   = FormattedCalDataEditor->Keys[iKey];
            UnicodeString sValue = FormattedCalDataEditor->Values[sKey];

            pIniFile->WriteString( calIniDataSection, sKey, sValue );
        }

        pIniFile->UpdateFile();

        delete pIniFile;
    }
}


void __fastcall TSystemSettingsForm::LoadCalDatBtnClick(TObject *Sender)
{
    // Load the calibration data. Can only do this from the formatted view.
    if( CalDataPgCtrl->ActivePage != DevMemorySheet )
    {
        MessageDlg( "You can only load data in the Device Memory view.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    if( OpenCalDataDlg->Execute() )
    {
        TStringList* pCaptions = new TStringList();
        TStringList* pValues   = new TStringList();

        try
        {
            TIniFile* pIniFile = new TIniFile( OpenCalDataDlg->FileName );

            // For each key in the editor, look for a corresponding section in the
            // ini file. If the section is not found, skip it.
            for( int iKey = 1; iKey <= FormattedCalDataEditor->Strings->Count; iKey++ )
            {
                String sKey     = FormattedCalDataEditor->Keys[iKey];
                String newValue = pIniFile->ReadString( calIniDataSection, sKey, "" );

                if( newValue.Length() > 0 )
                {
                    FormattedCalDataEditor->Values[ sKey ] = newValue;
                }
            }

            delete pIniFile;

            // Now write the values to the software cache (not to the device yet though)
            for( int iKey = 1; iKey <= FormattedCalDataEditor->Strings->Count; iKey++ )
            {
                String sKey = FormattedCalDataEditor->Keys[iKey];

                pCaptions->Add( sKey );
                pValues->Add( FormattedCalDataEditor->Values[sKey] );
            }

            if( !m_poller->SetCalDataFormatted( pCaptions, pValues ) )
                Abort();
        }
        catch( ... )
        {
            MessageDlg( "Load calibration data failed.", mtError, TMsgDlgButtons() << mbOK, 0 );
        }

        delete pCaptions;
        delete pValues;

        // Regardless of the outcome, always refresh the cal data grids with
        // what is in the cache
        LoadCalPage();
    }
}


void __fastcall TSystemSettingsForm::WriteToDevBtnClick(TObject *Sender)
{
    // Write the data to the device. On success, set the 'write pending' indication
    if( m_poller->WriteCalDataToDevice() )
        WriteToDevBtn->Tag = 1;
    else
        MessageDlg( "Could not write calibration data to device.", mtError, TMsgDlgButtons() << mbOK, 0 );

    // Call the timer tick function to update the current status
    RFCPollTimerTimer( RFCPollTimer );
}


void __fastcall TSystemSettingsForm::ReadFromDevBtnClick(TObject *Sender)
{
    // On success, clear any 'write pending' indication
    if( m_poller->ReloadCalDataFromDevice() )
        WriteToDevBtn->Tag = 0;
    else
        MessageDlg( "Could not read calibration data from device.", mtError, TMsgDlgButtons() << mbOK, 0 );

    // Call the timer tick function to update the current status
    RFCPollTimerTimer( RFCPollTimer );
}


void __fastcall TSystemSettingsForm::ZeroReadingsBtnClick(TObject *Sender)
{
    // Poller cannot be NULL if this button was enabled
    TZeroWTTTSForm::ShowDlg( m_poller );
}


void __fastcall TSystemSettingsForm::NewBattBtnClick(TObject *Sender)
{
    BYTE battType;
    WORD capacity;

    if( TNewBattForm::ShowDlg( battType, capacity ) )
    {
        // We need only pass a list of the cal items that need to be updated to the comms mgr
        TStringList* pCaptions = new TStringList();
        TStringList* pValues   = new TStringList();

        pCaptions->Add( TWTTTSSub::CalFactorCaptions[ TWTTTSSub::CI_BATTERY_TYPE ] );
        pValues->Add  ( IntToStr( battType ) );

        pCaptions->Add( TWTTTSSub::CalFactorCaptions[ TWTTTSSub::CI_BATTERY_CAPACITY ] );
        pValues->Add  ( IntToStr( capacity ) );

        if( !m_poller->SetCalDataFormatted( pCaptions, pValues ) )
        {
            MessageDlg( "Could not set new battery information.", mtError, TMsgDlgButtons() << mbOK, 0 );

            delete pCaptions;
            delete pValues;

            return;
        }

        delete pCaptions;
        delete pValues;

        if( !m_poller->WriteCalDataToDevice() )
        {
            MessageDlg( "Could not write calibration data to device.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return;
        }

        // Now wait up to 30 seconds for the cal to upload. Device will return to idle
        // when upload is done.
        NewBattBtn->Enabled = false;
        Screen->Cursor = crHourGlass;

        TDateTime timeoutTime = IncSecond( Now(), 30 );

        while( !m_poller->DeviceIsIdle )
        {
            // Let the appl process its messages while in this loop
            Application->ProcessMessages();

            if( Now() > timeoutTime )
                break;
        }

        if( m_poller->DeviceIsIdle )
             MessageDlg( "New battery information successfully uploaded.", mtError, TMsgDlgButtons() << mbOK, 0 );
        else
             MessageDlg( "Upload of new battery information failed.", mtError, TMsgDlgButtons() << mbOK, 0 );

        Screen->Cursor = crDefault;
        NewBattBtn->Enabled = true;
    }
}


void __fastcall TSystemSettingsForm::CompleteJobBtnClick(TObject *Sender)
{
    // Main form takes care of this request
    Application->MainForm->Perform( WM_COMPLETE_JOB, 0, 0 );
    SetJobStatusBtns();
}


void __fastcall TSystemSettingsForm::ReopenJobBtnClick(TObject *Sender)
{
    // Main form takes care of this request
    Application->MainForm->Perform( WM_REOPEN_JOB, 0, 0 );
    SetJobStatusBtns();
}


void TSystemSettingsForm::SetJobStatusBtns( void )
{
    // We only use the current job to set the state of the job status buttons
    if( m_currJob == NULL )
    {
        CompleteJobBtn->Enabled = false;
        ReopenJobBtn->Enabled   = false;
    }
    else if( m_currJob->Completed )
    {
        CompleteJobBtn->Enabled = false;
        ReopenJobBtn->Enabled   = true;
    }
    else
    {
        CompleteJobBtn->Enabled = true;
        ReopenJobBtn->Enabled   = false;
    }
}


void __fastcall TSystemSettingsForm::SendChangeChanBtnClick(TObject *Sender)
{
    if( m_poller != NULL )
    {
        if( BRChanCombo->ItemIndex >= 0 )
        {
            switch( m_poller->SetRadioChannel( (eBaseRadioNbr)BRRadNbrCombo->ItemIndex, BRChanCombo->Items->Strings[ BRChanCombo->ItemIndex ].ToInt() ) )
            {
                case TBaseRadioDevice::SRCR_SUCCESS:
                    break;

                case TBaseRadioDevice::SRCR_NO_RADIO:
                    MessageDlg( "You cannot issue this command if you are not connected to a base radio.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    break;

                case TBaseRadioDevice::SRCR_IN_USE:
                    MessageDlg( "The selected channel is in use by another radio. Please try again with a different channel.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    break;

                default:
                    MessageDlg( "The command failed: there is a problem with the serial port.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    break;
            }
        }
    }
}


void __fastcall TSystemSettingsForm::AutoAssignPortsCBClick(TObject *Sender)
{
    // If we're auto-assigning ports, then disable everything
    if( AutoAssignPortsCB->Checked )
    {
        for( int iCtrl = 0; iCtrl < PortsGB->ControlCount; iCtrl++ )
        {
            TComboBox* pCombo = dynamic_cast<TComboBox*>( PortsGB->Controls[iCtrl] );

            if( pCombo != NULL )
            {
                // Disable the 'onclick' handler here so that it doesn't fire
                // and undo our work if the item index changes
                TNotifyEvent pOnComboClick = pCombo->OnClick;
                pCombo->OnClick = NULL;

                pCombo->ItemIndex = CT_SERIAL;  // serial
                pCombo->Enabled   = false;

                // Okay to restore click handler now
                pCombo->OnClick = pOnComboClick;
                continue;
            }

            TEdit* pEdit = dynamic_cast<TEdit*>( PortsGB->Controls[iCtrl] );

            if( pEdit != NULL )
                pEdit->Enabled   = false;

        }

        // Get the last saved settings
        COMMS_CFG radioCommCfg;
        COMMS_CFG deviceCommCfg;
        COMMS_CFG mgmtCommCfg;

        GetCommSettings( DCT_BASE,  radioCommCfg );
        GetCommSettings( DCT_WTTTS, deviceCommCfg );
        GetCommSettings( DCT_MGMT,  mgmtCommCfg );

        // Set the Port Numbers to that from the registry
        BaseRadioPortEdit->Text = radioCommCfg.portName;
        TesTORKPortEdit->Text   = deviceCommCfg.portName;
        MgmtDevPortEdit->Text   = mgmtCommCfg.portName;

        // If any of the texts are empty, give them a default value
        if( ( radioCommCfg.portName.IsEmpty() ) || ( deviceCommCfg.portName.IsEmpty() ) || ( mgmtCommCfg.portName.IsEmpty() ) )
        {
            BaseRadioPortEdit->Text = IntToStr( BASESTN_CONTROL_PORT_OFFSET + 1 );
            TesTORKPortEdit->Text   = IntToStr( BASESTN_TESTORK_RADIO_PORT_OFFSET + 1 );
            MgmtDevPortEdit->Text   = IntToStr( BASESTN_MGMT_PORT_OFFSET );
        }

        // When auto assigning, all ports are at 115200
        BaseRadioParamEdit->Text = "115200";
        TesTORKParamEdit->Text   = "115200";
        MgmtDevParamEdit->Text   = "115200";
    }
    else
    {
        // Not auto-assigning. Enable all edits
        for( int iCtrl = 0; iCtrl < PortsGB->ControlCount; iCtrl++ )
        {
            TComboBox* pCombo = dynamic_cast<TComboBox*>( PortsGB->Controls[iCtrl] );

            if( pCombo != NULL )
            {
                pCombo->Enabled = true;
                continue;
            }

            TEdit* pEdit = dynamic_cast<TEdit*>( PortsGB->Controls[iCtrl] );

            if( pEdit != NULL )
            {
                pEdit->Enabled = true;
                pEdit->Color   = clWindow;
                continue;
            }
        }
    }
}


void __fastcall TSystemSettingsForm::BROut3FcnComboClick(TObject *Sender)
{
    // Changing the Base Radio Out 4 function affects both the PDS
    // settings on the Misc page and the Auto-Record settings
    eBR3OutFcn eNewFcn = (eBR3OutFcn)( BROut3FcnCombo->ItemIndex );

    if( eNewFcn == eBR3OutFcn::eBR3Out_AutoRecAlarm )
    {
        AutoRecAlarmEnCB->Enabled = true;
        AutoRecAlarmEnCB->Checked = m_OrigAutoRecSettings.arSettings.bAlarmOnError;

        PDSMonEnabledCB->Enabled = false;
        PDSMonEnabledCB->Checked = false;
    }
    else
    {
        AutoRecAlarmEnCB->Enabled = false;
        AutoRecAlarmEnCB->Checked = false;

        PDSMonEnabledCB->Enabled = true;
        PDSMonEnabledCB->Checked = m_OrigMiscSettings.bPDSAlarmEn;
    }
}


void __fastcall TSystemSettingsForm::BrowseBackupDirBtnClick(TObject *Sender)
{
    String sBackupDir = JobDirEdit->Text;

    if( SelectDirectory( "Select Backup Directory", "Desktop", sBackupDir, TSelectDirExtOpts() << sdNewUI << sdNewFolder, NULL ) )
    {
        String tTest = ExtractFileDrive( sBackupDir );

        // Check that the drive location is different than the current job
        if( ExtractFileDrive( sBackupDir ).CompareIC( ExtractFileDrive( JobDirEdit->Text ) ) != 0 )
            BackupDirEdit->Text = sBackupDir;
        else
            MessageDlg( "Backup Directory must be in a different drive than the current Job Directory", mtError, TMsgDlgButtons() << mbOK, 0 );
    }
}


void __fastcall TSystemSettingsForm::BaseRadioPortTypeComboClick(TObject *Sender)
{
    if( BaseRadioPortTypeCombo->ItemIndex == CT_UNUSED )
    {
        BaseRadioPortEdit->Text     = "1";
        BaseRadioPortEdit->Enabled  = false;
        BaseRadioParamEdit->Enabled = false;
    }
    else if( BaseRadioPortTypeCombo->ItemIndex == CT_UDP )
    {
        BaseRadioPortEdit->Text     = "";
        BaseRadioParamEdit->Text    = "62220";
        BaseRadioPortEdit->Enabled  = false;
        BaseRadioParamEdit->Enabled = true;
    }
    else
    {
        BaseRadioPortEdit->Enabled  = true;
        BaseRadioParamEdit->Enabled = true;
    }
}


void __fastcall TSystemSettingsForm::TesTORKPortTypeComboClick(TObject *Sender)
{
    if( TesTORKPortTypeCombo->ItemIndex == CT_UNUSED )
    {
        TesTORKPortEdit->Text     = "1";
        TesTORKPortEdit->Enabled  = false;
        TesTORKParamEdit->Enabled = false;
    }
    else if( TesTORKPortTypeCombo->ItemIndex == CT_UDP )
    {
        TesTORKPortEdit->Text     = "";
        TesTORKParamEdit->Text    = "62222";
        TesTORKPortEdit->Enabled  = false;
        TesTORKParamEdit->Enabled = true;
    }
    else
    {
        TesTORKPortEdit->Enabled  = true;
        TesTORKParamEdit->Enabled = true;
    }
}


void __fastcall TSystemSettingsForm::MgmtDevPortTypeComboClick(TObject *Sender)
{
    if( MgmtDevPortTypeCombo->ItemIndex == CT_UNUSED )
    {
        MgmtDevPortEdit->Text     = "1";
        MgmtDevPortEdit->Enabled  = false;
        MgmtDevParamEdit->Enabled = false;
    }
    else if( TesTORKPortTypeCombo->ItemIndex == CT_UDP )
    {
        MgmtDevPortEdit->Text     = "127.0.0.1";
        MgmtDevParamEdit->Text    = "62224";
        MgmtDevPortEdit->Enabled  = true;
        MgmtDevParamEdit->Enabled = true;
    }
    else
    {
        MgmtDevPortEdit->Enabled  = true;
        MgmtDevParamEdit->Enabled = true;
    }
}


// When the Combobox is clicked, enable or disable the edits related
void __fastcall TSystemSettingsForm::WitsPortTypeComboClick(TObject *Sender)
{
    if( WitsPortTypeCombo->ItemIndex == CT_UNUSED )
    {
        WitsPortAddressEdit->Text    = "1";
        WitsPortAddressEdit->Enabled = false;
        WitsParameterEdit->Enabled   = false;
    }
    else if( WitsPortTypeCombo->ItemIndex == CT_UDP )
    {
        WitsPortAddressEdit->Text    = "127.0.0.1";
        WitsParameterEdit->Text      = "62226";
        WitsPortAddressEdit->Enabled = true;
        WitsParameterEdit->Enabled   = true;
    }
    else
    {
        WitsPortAddressEdit->Text    = "1";
        WitsParameterEdit->Text      = "460800";
        WitsPortAddressEdit->Enabled = true;
        WitsParameterEdit->Enabled   = true;
    }
}


// When the Header checkbox is clicked, enable or disable the edits related
void __fastcall TSystemSettingsForm::WitsRFCHeaderCBClick(TObject *Sender)
{
    if( WitsRFCHeaderCB->Checked )
    {
        WitsRFCCodeEdit->Enabled   = true;
        WitsRFCHeaderEdit->Enabled = true;

        WitsRFCCodeEdit->Color   = clWindow;
        WitsRFCHeaderEdit->Color = clWindow;
    }
    else
    {
        WitsRFCCodeEdit->Enabled   = false;
        WitsRFCHeaderEdit->Enabled = false;

        WitsRFCCodeEdit->Color   = clBtnFace;
        WitsRFCHeaderEdit->Color = clBtnFace;
    }
}


// When the Header checkbox is clicked, enable or disable the edits related
void __fastcall TSystemSettingsForm::WitsStreamHeaderCBClick(TObject *Sender)
{
    if( WitsStreamHeaderCB->Checked )
    {
        WitsStreamCodeEdit->Enabled   = true;
        WitsStreamHeaderEdit->Enabled = true;

        WitsStreamCodeEdit->Color   = clWindow;
        WitsStreamHeaderEdit->Color = clWindow;
    }
    else
    {
        WitsStreamCodeEdit->Enabled   = false;
        WitsStreamHeaderEdit->Enabled = false;

        WitsStreamCodeEdit->Color   = clBtnFace;
        WitsStreamHeaderEdit->Color = clBtnFace;
    }
}


// Create the EditsWitsDlg in Add mode
void __fastcall TSystemSettingsForm::WitsAddButtonClick(TObject *Sender)
{
    WITS_FIELD_SETTING fieldSetting;

    // Show the dialog - If it returns true, Add new values
    if( EditWitsForm->ShowAsAddDlg( fieldSetting ) )
    {
        // Get the currently used list
        TListView* currList;

        if( WitsListPageControl->ActivePage->Tag == TSubDevice::eWPT_Stream )
            currList = WitsStreamListView;
        else
            currList = WitsRfcListView;

        TListItem* newItem = currList->Items->Add();

        newItem->Checked = fieldSetting.bEnabled;
        newItem->Data    = (void*) fieldSetting.iFieldEnum;
        newItem->SubItems->Add( FormatWITSCode( fieldSetting.iCode ) );
        newItem->SubItems->Add( TSubDevice::WitsFieldCaptions[fieldSetting.iFieldEnum] );
    }
}


// Remove the selected item from the list
void __fastcall TSystemSettingsForm::WitsRemoveButtonClick(TObject *Sender)
{
    // Get the currently used list
    TListView* currList;

    if( WitsListPageControl->ActivePage->Tag == TSubDevice::eWPT_Stream )
        currList = WitsStreamListView;
    else
        currList = WitsRfcListView;

    // Ensure we have an item selected
    int iSelectedCount = currList->SelCount;

    if( iSelectedCount <= 0 )
        return;

    // Remove the item
    currList->Items->Delete( currList->ItemIndex );
}


// Create the EditsWitsDlg in Edit mode
void __fastcall TSystemSettingsForm::WitsEditClick(TObject *Sender)
{
    TListView* currList = dynamic_cast<TListView*>(Sender);

    if( currList == NULL )
        return;

    // Ensure we have an item selected
    int iSelectedCount = currList->SelCount;

    if( iSelectedCount <= 0 )
        return;

    // Get the index now that we know it exists
    TListItem* pCurrListItem = currList->Items->Item[currList->ItemIndex];

    WITS_FIELD_SETTING fieldSetting;

    fieldSetting.bEnabled   = pCurrListItem->Checked;
    fieldSetting.iCode      = StrToInt( pCurrListItem->SubItems->Strings[eWFC_Code] );
    fieldSetting.iFieldEnum = (int)pCurrListItem->Data;

    // Show the dialog - If it returns true, update values
    if( EditWitsForm->ShowAsEditDlg( fieldSetting ) )
    {
        pCurrListItem->Checked                      = fieldSetting.bEnabled;
        pCurrListItem->SubItems->Strings[eWFC_Code] = IntToStr( fieldSetting.iCode );
        pCurrListItem->SubItems->Strings[eWFC_Data] = TSubDevice::WitsFieldCaptions[fieldSetting.iFieldEnum];
        pCurrListItem->Data                         = (void*) fieldSetting.iFieldEnum;
    }
}


void __fastcall TSystemSettingsForm::WitsDuplicateBtnClick(TObject *Sender)
{
    // Get the currently used list
    String sWarning;

    TListView* srcList;
    TListView* destList;

    if( WitsListPageControl->ActivePage->Tag == TSubDevice::eWPT_Stream )
    {
        sWarning = "You are about to overwrite the RFC Data with the Stream Data.\r  Click Yes to proceed. \r  Click Cancel to reconsider.";
        srcList  = WitsStreamListView;
        destList = WitsRfcListView;
    }
    else
    {
        sWarning = "You are about to overwrite the Stream Data with the RFC Data.\r  Click Yes to proceed. \r  Click Cancel to reconsider.";
        srcList  = WitsRfcListView;
        destList = WitsStreamListView;
    }

    // Show the Confirmation Dialog
    int mrResult = MessageDlg( sWarning, mtWarning, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    // If the result is good, duplicate
    if( mrResult == mrYes )
        destList->Items->Assign( srcList->Items );
}


void __fastcall TSystemSettingsForm::WitsListPageControlChange(TObject *Sender)
{
    // On tab-change, re-write the Copy button caption
    if( WitsListPageControl->ActivePage->Tag == TSubDevice::eWPT_Stream )
        WitsDuplicateBtn->Caption = "Copy Stream Data";
    else
        WitsDuplicateBtn->Caption = "Copy RFC Data";
}


void __fastcall TSystemSettingsForm::SetHousingSN1Click(TObject *Sender)
{
    // This is a hidden menu click. Do not allow re-entry
    SetHousingSN1->Enabled = false;

    try
    {
        String sHousingSN = m_poller->DeviceHousingSN;

        if( TSerializerDlg::ShowDlg( this, sHousingSN ) )
        {
            // Send the new SN to the device. If accepted, the device
            // will immediately go into the config upload state.
            if( !m_poller->SetDeviceHousingSN( sHousingSN ) )
            {
                MessageDlg( "The Housing Serial Number could not be set. The device may not be in a state to accept this command.",
                            mtError, TMsgDlgButtons() << mbOK, 0 );

                return;
            }

            // The device will now be uploading... wait here for that to finish
            ProgressDlg->ShowProgressForm( "Updating Housing Serial Number", true );
            ProgressDlg->UpdateMsg( "Uploading new Serial Number..." );

            while( m_poller->DeviceLinkState == TSubDevice::LS_CFG_UPLOAD )
            {
                if( ProgressDlg->IsCancelled() )
                    break;

                Application->ProcessMessages();
            }

            // Finally, force a reload of the cal data from the device
            if( !m_poller->ReloadCalDataFromDevice() )
            {
                MessageDlg( "The Housing Serial Number was uploaded to the device, but the updated calibration information could not be downloaded. Please verify that the serial number was correctly programmed.",
                            mtError, TMsgDlgButtons() << mbOK, 0 );
            }
            else
            {
                ProgressDlg->UpdateMsg( "Downloading updated data..." );

                while( m_poller->DeviceLinkState == TSubDevice::LS_CFG_DOWNLOAD )
                {
                    if( ProgressDlg->IsCancelled() )
                        break;

                    // UpdateProgress() will call Appl->ProcessMsgs() for us
                    ProgressDlg->UpdateProgress( m_poller->ConfigUploadProgress );
                }

                if( ProgressDlg->IsCancelled() )
                {
                    MessageDlg( "The updating of the Housing Serial Number was cancelled. Please verify the correct SN is now displayed on the Hardware tab of the main form.",
                                mtError, TMsgDlgButtons() << mbOK, 0 );
                }
                else
                {
                    MessageDlg( "The Housing Serial Number was uploaded to the device. Please verify the correct SN is now displayed on the Hardware tab of the main form.",
                                mtConfirmation, TMsgDlgButtons() << mbOK, 0 );
                }
            }
        }
    }
    catch( ... )
    {
        MessageDlg( "An unhandled exception occurred. The Housing Serial Number may not have been updated.", mtError, TMsgDlgButtons() << mbOK, 0 );
    }

    // Be sure the progress dialog is hidden
    ProgressDlg->CloseProgressForm();

    // Can re-enable this click now
    SetHousingSN1->Enabled = true;
}


void __fastcall TSystemSettingsForm::BRRadNbrComboClick(TObject *Sender)
{
    // Repopulate the channel list when changing antennas.
    eBaseRadioNbr eSelRadio = (eBaseRadioNbr)( BRRadNbrCombo->ItemIndex );

    // If the selected radio is 1, then assume this is for the TesTORK system
    WIRELESS_SYSTEM_INFO wirelessInfo;

    if( eSelRadio == eBRN_1 )
        GetWirelessSystemInfo( eWS_TesTork, wirelessInfo );
    else
        GetWirelessSystemInfo( eWS_MPLS2, wirelessInfo );

    // Repopulate the combo
    BRChanCombo->Items->Clear();

    for( int iItem = 0; iItem < MAX_CHANS_PER_SYSTEM; iItem++ )
    {
        if( wirelessInfo.chanList[iItem] > 0 )
            BRChanCombo->Items->Add( IntToStr( wirelessInfo.chanList[iItem] ) );
    }

    // Now select the channel based on the currently active chan from the radio
    BRChanCombo->ItemIndex = -1;

    if( m_poller != NULL )
    {
        String sCurrChan = IntToStr( m_poller->GetRadioChannel( eSelRadio ) );

        for( int iEntry = 0; iEntry < BRChanCombo->Items->Count; iEntry++ )
        {
            if( BRChanCombo->Items->Strings[iEntry] == sCurrChan )
            {
                BRChanCombo->ItemIndex = iEntry;
                break;
            }
        }
    }
}

