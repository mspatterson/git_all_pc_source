object SystemSettingsForm: TSystemSettingsForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'TesTORK System Settings'
  ClientHeight = 496
  ClientWidth = 733
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = HiddenPopup
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  DesignSize = (
    733
    496)
  PixelsPerInch = 96
  TextHeight = 13
  object PgCtrl: TPageControl
    Left = 8
    Top = 8
    Width = 719
    Height = 449
    ActivePage = MiscSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
    object CommsSheet: TTabSheet
      Caption = 'Communications'
      DesignSize = (
        711
        421)
      object DevConnPgCtrl: TPageControl
        Left = 3
        Top = 3
        Width = 705
        Height = 415
        ActivePage = BaseRadioSheet
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object WTTTSSheet: TTabSheet
          Caption = 'TesTORK'
          DesignSize = (
            697
            387)
          object DataLoggingGB: TGroupBox
            Left = 351
            Top = 46
            Width = 339
            Height = 235
            Anchors = [akLeft, akBottom]
            Caption = ' Data Logging '
            TabOrder = 1
            object EnableRawLogCB: TCheckBox
              Left = 14
              Top = 24
              Width = 187
              Height = 17
              Caption = 'Raw Stream Data'
              TabOrder = 0
            end
            object RawLogFileNameEdit: TEdit
              Left = 13
              Top = 45
              Width = 273
              Height = 21
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 1
            end
            object BrowseRawLogFileBtn: TButton
              Left = 295
              Top = 42
              Width = 33
              Height = 25
              Caption = '...'
              TabOrder = 2
              OnClick = BrowseRawLogFileBtnClick
            end
            object EnableCalLogCB: TCheckBox
              Left = 14
              Top = 75
              Width = 187
              Height = 17
              Caption = 'Calibrated Stream Data'
              TabOrder = 3
            end
            object CalLogFileNameEdit: TEdit
              Left = 13
              Top = 96
              Width = 273
              Height = 21
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 4
            end
            object BrowseCalLogFileBtn: TButton
              Left = 295
              Top = 93
              Width = 33
              Height = 25
              Caption = '...'
              TabOrder = 5
              OnClick = BrowseCalLogFileBtnClick
            end
            object EnableRFCLogCB: TCheckBox
              Left = 14
              Top = 128
              Width = 187
              Height = 17
              Caption = 'Raw Idle Data'
              TabOrder = 6
            end
            object RFCLogFileNameEdit: TEdit
              Left = 13
              Top = 149
              Width = 273
              Height = 21
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 7
            end
            object BrowseRFCLogFileBtn: TButton
              Left = 295
              Top = 146
              Width = 33
              Height = 25
              Caption = '...'
              TabOrder = 8
              OnClick = BrowseRFCLogFileBtnClick
            end
            object EnableChartLogCB: TCheckBox
              Left = 14
              Top = 179
              Width = 187
              Height = 17
              Caption = 'Calibrated Idle Data'
              TabOrder = 9
            end
            object ChartLogFileNameEdit: TEdit
              Left = 13
              Top = 200
              Width = 273
              Height = 21
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 10
            end
            object BrowseChartLogFileBtn: TButton
              Left = 295
              Top = 197
              Width = 33
              Height = 25
              Caption = '...'
              TabOrder = 11
              OnClick = BrowseChartLogFileBtnClick
            end
          end
          object DiagnosticsGB: TGroupBox
            Left = 12
            Top = 12
            Width = 330
            Height = 371
            Anchors = [akLeft, akTop, akRight, akBottom]
            Caption = ' Diagnostics '
            TabOrder = 0
            DesignSize = (
              330
              371)
            object Label7: TLabel
              Left = 12
              Top = 16
              Width = 43
              Height = 13
              Caption = 'Last RFC'
            end
            object LastRFCEditor: TValueListEditor
              Left = 12
              Top = 32
              Width = 306
              Height = 328
              Anchors = [akLeft, akTop, akRight, akBottom]
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
              TabOrder = 0
              TitleCaptions.Strings = (
                'Property'
                'Value')
              OnSelectCell = ListEditorSelectCell
              ColWidths = (
                188
                112)
            end
          end
        end
        object BaseRadioSheet: TTabSheet
          Caption = 'Base Radio'
          ImageIndex = 1
          DesignSize = (
            697
            387)
          object BRDiagnosticsGB: TGroupBox
            Left = 12
            Top = 12
            Width = 330
            Height = 371
            Anchors = [akLeft, akTop, akRight, akBottom]
            Caption = ' Diagnostics '
            TabOrder = 0
            DesignSize = (
              330
              371)
            object Label37: TLabel
              Left = 12
              Top = 16
              Width = 54
              Height = 13
              Caption = 'Last Status'
            end
            object LastStatusEditor: TValueListEditor
              Left = 12
              Top = 32
              Width = 306
              Height = 328
              Anchors = [akLeft, akTop, akRight, akBottom]
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
              TabOrder = 0
              TitleCaptions.Strings = (
                'Property'
                'Value')
              OnSelectCell = ListEditorSelectCell
              ColWidths = (
                188
                112)
            end
          end
          object BRCmdGB: TGroupBox
            Left = 351
            Top = 12
            Width = 339
            Height = 66
            Caption = ' Commands '
            TabOrder = 1
            object Label39: TLabel
              Left = 12
              Top = 31
              Width = 74
              Height = 13
              Caption = 'Set Radio Chan'
            end
            object SendChangeChanBtn: TButton
              Left = 256
              Top = 25
              Width = 68
              Height = 25
              Caption = 'Send'
              TabOrder = 2
              OnClick = SendChangeChanBtnClick
            end
            object BRChanCombo: TComboBox
              Left = 199
              Top = 28
              Width = 51
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 1
              Text = '11'
              Items.Strings = (
                '11'
                '15'
                '20'
                '25')
            end
            object BRRadNbrCombo: TComboBox
              Left = 107
              Top = 28
              Width = 85
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 0
              Text = 'TesTORK'
              OnClick = BRRadNbrComboClick
              Items.Strings = (
                'TesTORK'
                'MPL')
            end
          end
          object BROut4GB: TGroupBox
            Left = 351
            Top = 84
            Width = 339
            Height = 66
            Caption = ' Output 3 Function '
            TabOrder = 2
            object Label5: TLabel
              Left = 12
              Top = 31
              Width = 79
              Height = 13
              Caption = 'Use Output 3 As'
            end
            object BROut3FcnCombo: TComboBox
              Left = 109
              Top = 28
              Width = 140
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 0
              Text = 'PDS Alarm'
              OnClick = BROut3FcnComboClick
              Items.Strings = (
                'PDS Alarm'
                'Auto-Record Alarm')
            end
            object BrTestOut4Btn: TButton
              Left = 256
              Top = 25
              Width = 68
              Height = 25
              Caption = 'Test'
              TabOrder = 1
            end
          end
        end
        object PortsSheet: TTabSheet
          Caption = 'Ports'
          ImageIndex = 3
          object PortsGB: TGroupBox
            Left = 12
            Top = 12
            Width = 445
            Height = 157
            Caption = ' Ports '
            TabOrder = 0
            object Label19: TLabel
              Left = 16
              Top = 67
              Width = 44
              Height = 13
              Caption = 'TesTORK'
            end
            object Label21: TLabel
              Left = 16
              Top = 95
              Width = 85
              Height = 13
              Caption = 'Management Port'
            end
            object Label22: TLabel
              Left = 232
              Top = 17
              Width = 69
              Height = 13
              Caption = 'Port / Address'
            end
            object Label23: TLabel
              Left = 338
              Top = 17
              Width = 50
              Height = 13
              Caption = 'Parameter'
            end
            object Label24: TLabel
              Left = 115
              Top = 17
              Width = 47
              Height = 13
              Caption = 'Port Type'
            end
            object Label20: TLabel
              Left = 16
              Top = 39
              Width = 53
              Height = 13
              Caption = 'Base Radio'
            end
            object AutoAssignPortsCB: TCheckBox
              Left = 115
              Top = 127
              Width = 169
              Height = 17
              Caption = 'Automatically Assign Ports'
              TabOrder = 9
              OnClick = AutoAssignPortsCBClick
            end
            object TesTORKPortTypeCombo: TComboBox
              Left = 115
              Top = 64
              Width = 95
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 3
              Text = 'Serial'
              OnClick = TesTORKPortTypeComboClick
              Items.Strings = (
                'Serial'
                'UDP'
                'No Device')
            end
            object MgmtDevPortTypeCombo: TComboBox
              Left = 115
              Top = 92
              Width = 95
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 6
              Text = 'Serial'
              OnClick = MgmtDevPortTypeComboClick
              Items.Strings = (
                'Serial'
                'UDP'
                'No Device')
            end
            object TesTORKPortEdit: TEdit
              Left = 232
              Top = 64
              Width = 85
              Height = 21
              TabOrder = 4
              Text = '1'
            end
            object TesTORKParamEdit: TEdit
              Left = 338
              Top = 64
              Width = 85
              Height = 21
              TabOrder = 5
              Text = '115200'
            end
            object MgmtDevPortEdit: TEdit
              Left = 232
              Top = 92
              Width = 85
              Height = 21
              TabOrder = 7
              Text = '1'
            end
            object MgmtDevParamEdit: TEdit
              Left = 338
              Top = 91
              Width = 85
              Height = 21
              TabOrder = 8
              Text = '115200'
            end
            object BaseRadioPortTypeCombo: TComboBox
              Left = 115
              Top = 36
              Width = 95
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 0
              Text = 'Serial'
              OnClick = BaseRadioPortTypeComboClick
              Items.Strings = (
                'Serial'
                'UDP'
                'No Device')
            end
            object BaseRadioPortEdit: TEdit
              Left = 232
              Top = 36
              Width = 85
              Height = 21
              TabOrder = 1
              Text = '1'
            end
            object BaseRadioParamEdit: TEdit
              Left = 338
              Top = 36
              Width = 85
              Height = 21
              TabOrder = 2
              Text = '115200'
            end
          end
        end
      end
    end
    object CalSheet: TTabSheet
      Caption = 'Calibration'
      ImageIndex = 1
      DesignSize = (
        711
        421)
      object CalDataPgCtrl: TPageControl
        Left = 8
        Top = 8
        Width = 433
        Height = 365
        ActivePage = DevMemorySheet
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        object DevMemorySheet: TTabSheet
          Caption = 'Device Memory'
          DesignSize = (
            425
            337)
          object FormattedCalDataEditor: TValueListEditor
            Left = 8
            Top = 8
            Width = 409
            Height = 323
            Anchors = [akLeft, akTop, akRight, akBottom]
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
            TabOrder = 0
            TitleCaptions.Strings = (
              'Property'
              'Value')
            OnSelectCell = ListEditorSelectCell
            ColWidths = (
              211
              192)
          end
        end
        object RawDataSheet: TTabSheet
          Caption = 'Raw Data View'
          ImageIndex = 1
          DesignSize = (
            425
            337)
          object RawCalDataEditor: TValueListEditor
            Left = 8
            Top = 8
            Width = 409
            Height = 323
            Anchors = [akLeft, akTop, akRight, akBottom]
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
            TabOrder = 0
            TitleCaptions.Strings = (
              'Page'
              'Data')
            OnSelectCell = ListEditorSelectCell
            ColWidths = (
              76
              327)
          end
        end
      end
      object LoadCalDatBtn: TButton
        Left = 114
        Top = 384
        Width = 99
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Load From File'
        TabOrder = 1
        OnClick = LoadCalDatBtnClick
      end
      object SaveCalDatBtn: TButton
        Left = 8
        Top = 384
        Width = 99
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Save To File'
        TabOrder = 2
        OnClick = SaveCalDatBtnClick
      end
      object WriteToDevBtn: TButton
        Left = 236
        Top = 384
        Width = 99
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Write To Device'
        TabOrder = 3
        OnClick = WriteToDevBtnClick
      end
      object ReadFromDevBtn: TButton
        Left = 342
        Top = 384
        Width = 99
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Read From Device'
        TabOrder = 4
        OnClick = ReadFromDevBtnClick
      end
      object ZeroReadingsBtn: TButton
        Left = 451
        Top = 40
        Width = 99
        Height = 25
        Caption = 'Zero Readings'
        TabOrder = 5
        OnClick = ZeroReadingsBtnClick
      end
      object NewBattBtn: TButton
        Left = 451
        Top = 75
        Width = 99
        Height = 25
        Caption = 'New Battery'
        TabOrder = 6
        OnClick = NewBattBtnClick
      end
    end
    object MiscSheet: TTabSheet
      Caption = 'Misc'
      ImageIndex = 3
      object JobDirGB: TGroupBox
        Left = 8
        Top = 8
        Width = 695
        Height = 54
        Caption = ' Job Directory '
        TabOrder = 0
        DesignSize = (
          695
          54)
        object JobDirEdit: TEdit
          Left = 14
          Top = 21
          Width = 629
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 0
        end
        object BrowseJobDirBtn: TButton
          Left = 651
          Top = 17
          Width = 33
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '...'
          TabOrder = 1
          OnClick = BrowseJobDirBtnClick
        end
      end
      object SysAdminPWGB: TGroupBox
        Left = 8
        Top = 70
        Width = 335
        Height = 139
        Caption = ' Sys Admin Password '
        TabOrder = 1
        object Label8: TLabel
          Left = 14
          Top = 24
          Width = 117
          Height = 13
          Caption = 'Enter current password:'
        end
        object Label9: TLabel
          Left = 14
          Top = 51
          Width = 102
          Height = 13
          Caption = 'Enter new password:'
        end
        object Label10: TLabel
          Left = 14
          Top = 78
          Width = 113
          Height = 13
          Caption = 'Confirm new password:'
        end
        object CurrPWEdit: TEdit
          Left = 148
          Top = 21
          Width = 171
          Height = 21
          PasswordChar = '*'
          TabOrder = 0
        end
        object NewPWEdit1: TEdit
          Left = 148
          Top = 48
          Width = 171
          Height = 21
          PasswordChar = '*'
          TabOrder = 1
        end
        object NewPWEdit2: TEdit
          Left = 148
          Top = 75
          Width = 171
          Height = 21
          PasswordChar = '*'
          TabOrder = 2
        end
        object SavePWBtn: TButton
          Left = 244
          Top = 103
          Width = 75
          Height = 25
          Caption = 'Save'
          TabOrder = 3
          OnClick = SavePWBtnClick
        end
      end
      object JobStatusGB: TGroupBox
        Left = 8
        Top = 215
        Width = 335
        Height = 55
        Caption = ' Job Status '
        TabOrder = 2
        object CompleteJobBtn: TButton
          Left = 12
          Top = 19
          Width = 95
          Height = 25
          Caption = 'Complete Job'
          TabOrder = 0
          OnClick = CompleteJobBtnClick
        end
        object ReopenJobBtn: TButton
          Left = 117
          Top = 19
          Width = 95
          Height = 25
          Caption = 'Re-Open Job'
          TabOrder = 1
          OnClick = ReopenJobBtnClick
        end
      end
      object PriorityGB: TGroupBox
        Left = 8
        Top = 276
        Width = 335
        Height = 53
        Caption = ' Process Priority '
        TabOrder = 3
        object ProcPriorityCombo: TComboBox
          Left = 11
          Top = 21
          Width = 199
          Height = 21
          Style = csDropDownList
          TabOrder = 0
        end
      end
      object ConnFilterGB: TGroupBox
        Left = 353
        Top = 70
        Width = 350
        Height = 54
        Caption = ' Connection Filtering Options '
        TabOrder = 5
        object Label36: TLabel
          Left = 14
          Top = 24
          Width = 120
          Height = 13
          Caption = 'Save all connections with'
        end
        object Label38: TLabel
          Left = 240
          Top = 24
          Width = 33
          Height = 13
          Caption = 'turn(s)'
        end
        object ConnMinTurnsEdit: TEdit
          Left = 160
          Top = 21
          Width = 73
          Height = 21
          NumbersOnly = True
          TabOrder = 0
          Text = '1'
        end
      end
      object AutoShlderOptGB: TGroupBox
        Left = 353
        Top = 130
        Width = 350
        Height = 46
        Caption = ' Auto-Shoulder Options '
        TabOrder = 6
        object ASOverrideNagCB: TCheckBox
          Left = 14
          Top = 21
          Width = 242
          Height = 17
          Caption = 'Warn user on override in Auto-Shoulder mode'
          TabOrder = 0
        end
      end
      object DataAveGB: TGroupBox
        Left = 353
        Top = 182
        Width = 350
        Height = 99
        Caption = ' Data Averaging '
        TabOrder = 7
        object TorqueLB: TLabel
          Left = 14
          Top = 38
          Width = 34
          Height = 13
          Caption = 'Torque'
        end
        object TensionLB: TLabel
          Left = 14
          Top = 67
          Width = 37
          Height = 13
          Caption = 'Tension'
        end
        object LevelLB: TLabel
          Left = 104
          Top = 16
          Width = 25
          Height = 13
          Caption = 'Level'
        end
        object Label17: TLabel
          Left = 197
          Top = 16
          Width = 41
          Height = 13
          Caption = 'Variance'
        end
        object TorqueCombo: TComboBox
          Left = 59
          Top = 35
          Width = 118
          Height = 21
          Style = csDropDownList
          TabOrder = 0
        end
        object TensionCombo: TComboBox
          Left = 59
          Top = 64
          Width = 118
          Height = 21
          Style = csDropDownList
          TabOrder = 2
        end
        object TorqueAvgVarEdit: TEdit
          Left = 188
          Top = 35
          Width = 63
          Height = 21
          TabOrder = 1
          Text = '0'
        end
        object TensionAvgVarEdit: TEdit
          Left = 188
          Top = 64
          Width = 63
          Height = 21
          TabOrder = 3
          Text = '0'
        end
      end
      object BackupSaveGB: TGroupBox
        Left = 8
        Top = 335
        Width = 335
        Height = 72
        Caption = ' Backup Save Directory '
        TabOrder = 4
        DesignSize = (
          335
          72)
        object BackupSaveCB: TCheckBox
          Left = 14
          Top = 47
          Width = 148
          Height = 17
          Caption = 'Force backup save on exit'
          TabOrder = 0
        end
        object BackupDirEdit: TEdit
          Left = 14
          Top = 20
          Width = 264
          Height = 21
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 1
        end
        object BrowseBackupDirBtn: TButton
          Left = 286
          Top = 16
          Width = 33
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '...'
          TabOrder = 2
          OnClick = BrowseBackupDirBtnClick
        end
      end
      object PDSMonitorGB: TGroupBox
        Left = 353
        Top = 339
        Width = 350
        Height = 46
        Caption = 'PDS Monitoring '
        TabOrder = 9
        object PDSMonEnabledCB: TCheckBox
          Left = 14
          Top = 21
          Width = 198
          Height = 17
          Caption = 'Enable PDS Monitoring'
          TabOrder = 0
        end
      end
      object TempCompGB: TGroupBox
        Left = 353
        Top = 287
        Width = 350
        Height = 46
        Caption = ' Temperature Compenstation '
        TabOrder = 8
        object EnableTempCompCB: TCheckBox
          Left = 14
          Top = 21
          Width = 198
          Height = 17
          Caption = 'Enable Temperature Compensation'
          TabOrder = 0
        end
      end
    end
    object InterlockSheet: TTabSheet
      Caption = 'Interlock / Auto-Record'
      ImageIndex = 3
      object SettingsGB: TGroupBox
        Left = 8
        Top = 8
        Width = 447
        Height = 138
        Caption = ' Interlock Settings '
        TabOrder = 0
        object CDSRelLB: TLabel
          Left = 14
          Top = 25
          Width = 176
          Height = 13
          Caption = 'CDS releases when tension less than'
        end
        object CDSSlipLockedLB: TLabel
          Left = 14
          Top = 52
          Width = 232
          Height = 13
          Caption = 'CDS and Slips locked when tension greater than '
        end
        object CDSRelUnitLB: TLabel
          Left = 398
          Top = 25
          Width = 19
          Height = 13
          Caption = 'Unit'
        end
        object CDSSlipLockedUnitLB: TLabel
          Left = 398
          Top = 52
          Width = 19
          Height = 13
          Caption = 'Unit'
        end
        object CDSLockedLB: TLabel
          Left = 14
          Top = 81
          Width = 276
          Height = 13
          Caption = 'CDS locked and Slips released when tension greater than '
        end
        object MinRelTimeLB: TLabel
          Left = 14
          Top = 109
          Width = 126
          Height = 13
          Caption = 'Wait time before releasing'
        end
        object MinRelTimeSecLB: TLabel
          Left = 398
          Top = 109
          Width = 29
          Height = 13
          Caption = '(secs)'
        end
        object CDSLockedUnitLB: TLabel
          Left = 398
          Top = 81
          Width = 19
          Height = 13
          Caption = 'Unit'
        end
        object CDSRelEdit: TEdit
          Left = 302
          Top = 22
          Width = 87
          Height = 21
          TabOrder = 0
        end
        object CDSSlipLockedEdit: TEdit
          Left = 302
          Top = 49
          Width = 87
          Height = 21
          TabOrder = 1
        end
        object CDSLockedEdit: TEdit
          Left = 302
          Top = 78
          Width = 87
          Height = 21
          TabOrder = 2
        end
        object MinRelTimeEdit: TEdit
          Left = 302
          Top = 106
          Width = 87
          Height = 21
          TabOrder = 3
        end
      end
      object InterlockEnableGB: TGroupBox
        Left = 8
        Top = 154
        Width = 447
        Height = 54
        Caption = ' Interlock Function  '
        TabOrder = 1
        object InterlockEnCB: TCheckBox
          Left = 14
          Top = 24
          Width = 121
          Height = 17
          Caption = 'Interlock Enabled'
          TabOrder = 0
        end
      end
      object AutoRecordGB: TGroupBox
        Left = 8
        Top = 216
        Width = 447
        Height = 193
        Caption = ' Auto Record Settings '
        TabOrder = 2
        object Label1: TLabel
          Left = 14
          Top = 51
          Width = 151
          Height = 13
          Caption = 'Minimum RPM to start recording'
        end
        object Label2: TLabel
          Left = 14
          Top = 78
          Width = 162
          Height = 13
          Caption = 'Minimum torque to start recording'
        end
        object Label3: TLabel
          Left = 14
          Top = 107
          Width = 241
          Height = 13
          Caption = 'Time below min RPM and torque to end connection'
        end
        object Label4: TLabel
          Left = 14
          Top = 135
          Width = 199
          Height = 13
          Caption = 'Max torque before cross-thread reported'
        end
        object ARTqStartUnitLB: TLabel
          Left = 398
          Top = 78
          Width = 19
          Height = 13
          Caption = 'Unit'
        end
        object Label11: TLabel
          Left = 398
          Top = 107
          Width = 29
          Height = 13
          Caption = '(secs)'
        end
        object ARTqCrossedUnitLB: TLabel
          Left = 398
          Top = 135
          Width = 19
          Height = 13
          Caption = 'Unit'
        end
        object ARRPMToStartEdit: TEdit
          Left = 302
          Top = 48
          Width = 87
          Height = 21
          TabOrder = 1
        end
        object ARTqToStartEdit: TEdit
          Left = 302
          Top = 75
          Width = 87
          Height = 21
          TabOrder = 2
        end
        object ARTimeToStopEdit: TEdit
          Left = 302
          Top = 104
          Width = 87
          Height = 21
          TabOrder = 3
        end
        object ARXThreadThreshEdit: TEdit
          Left = 302
          Top = 132
          Width = 87
          Height = 21
          TabOrder = 4
        end
        object AutoRecAlarmEnCB: TCheckBox
          Left = 14
          Top = 163
          Width = 199
          Height = 17
          Caption = 'Enable alarm on error'
          TabOrder = 5
        end
        object EnableARCB: TCheckBox
          Left = 14
          Top = 24
          Width = 191
          Height = 17
          Caption = 'Enable Auto-Record feature'
          TabOrder = 0
        end
      end
    end
    object WitsSheet: TTabSheet
      Caption = 'WITS'
      ImageIndex = 4
      DesignSize = (
        711
        421)
      object WitsPortsGB: TGroupBox
        Left = 8
        Top = 8
        Width = 339
        Height = 73
        Caption = ' Ports '
        TabOrder = 0
        object WitsPortTypeLB: TLabel
          Left = 56
          Top = 17
          Width = 47
          Height = 13
          Caption = 'Port Type'
        end
        object WitsPortAddressLB: TLabel
          Left = 165
          Top = 17
          Width = 69
          Height = 13
          Caption = 'Port / Address'
        end
        object WitsParamLB: TLabel
          Left = 256
          Top = 17
          Width = 50
          Height = 13
          Caption = 'Parameter'
        end
        object WitsLB: TLabel
          Left = 16
          Top = 39
          Width = 26
          Height = 13
          Caption = 'WITS'
        end
        object WitsPortTypeCombo: TComboBox
          Left = 56
          Top = 36
          Width = 95
          Height = 21
          TabOrder = 0
          Text = 'Serial'
          OnClick = WitsPortTypeComboClick
          Items.Strings = (
            'Serial'
            'UDP'
            'Unused')
        end
        object WitsPortAddressEdit: TEdit
          Left = 165
          Top = 36
          Width = 75
          Height = 21
          TabOrder = 1
          Text = '1'
        end
        object WitsParameterEdit: TEdit
          Left = 256
          Top = 36
          Width = 75
          Height = 21
          TabOrder = 2
          Text = '460800'
        end
      end
      object WitsHeaderGB: TGroupBox
        Left = 8
        Top = 87
        Width = 339
        Height = 179
        Caption = ' WITS Header '
        TabOrder = 1
        object WitsRFCCodeLB: TLabel
          Left = 16
          Top = 47
          Width = 25
          Height = 13
          Caption = 'Code'
        end
        object WitsRFCHeaderLB: TLabel
          Left = 122
          Top = 47
          Width = 35
          Height = 13
          Caption = 'Header'
        end
        object WitsStreamCodeLB: TLabel
          Left = 16
          Top = 124
          Width = 25
          Height = 13
          Caption = 'Code'
        end
        object WitsStreamHeaderLB: TLabel
          Left = 122
          Top = 124
          Width = 35
          Height = 13
          Caption = 'Header'
        end
        object WitsRFCHeaderCB: TCheckBox
          Left = 16
          Top = 24
          Width = 174
          Height = 17
          Caption = 'Send Header in RFC Packets'
          TabOrder = 0
          OnClick = WitsRFCHeaderCBClick
        end
        object WitsRFCCodeEdit: TEdit
          Left = 16
          Top = 66
          Width = 95
          Height = 21
          Enabled = False
          TabOrder = 1
          Text = '0000'
        end
        object WitsRFCHeaderEdit: TEdit
          Left = 122
          Top = 66
          Width = 209
          Height = 21
          Enabled = False
          TabOrder = 2
          Text = 'None'
        end
        object WitsStreamHeaderCB: TCheckBox
          Left = 16
          Top = 101
          Width = 174
          Height = 17
          Caption = 'Send Header in Stream Packets'
          TabOrder = 3
          OnClick = WitsStreamHeaderCBClick
        end
        object WitsStreamHeaderEdit: TEdit
          Left = 122
          Top = 143
          Width = 209
          Height = 21
          Enabled = False
          TabOrder = 5
          Text = 'None'
        end
        object WitsStreamCodeEdit: TEdit
          Left = 16
          Top = 143
          Width = 95
          Height = 21
          Enabled = False
          TabOrder = 4
          Text = '0000'
        end
      end
      object WitsListPageControl: TPageControl
        Left = 353
        Top = 8
        Width = 355
        Height = 359
        ActivePage = RfcSheet
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 2
        OnChange = WitsListPageControlChange
        object RfcSheet: TTabSheet
          Tag = 1
          Caption = 'RFC Data'
          DesignSize = (
            347
            331)
          object WitsRfcListView: TListView
            Left = 3
            Top = 3
            Width = 341
            Height = 325
            Anchors = [akLeft, akTop, akRight, akBottom]
            Checkboxes = True
            Columns = <
              item
                Caption = 'Enabled'
                Width = 55
              end
              item
                Caption = 'Code'
                Width = 55
              end
              item
                Caption = 'Data'
                Width = 174
              end>
            ReadOnly = True
            RowSelect = True
            TabOrder = 0
            ViewStyle = vsReport
            OnDblClick = WitsEditClick
          end
        end
        object StreamSheet: TTabSheet
          Tag = 2
          Caption = 'Stream Data'
          ImageIndex = 1
          DesignSize = (
            347
            331)
          object WitsStreamListView: TListView
            Left = 3
            Top = 3
            Width = 341
            Height = 325
            Anchors = [akLeft, akTop, akRight, akBottom]
            Checkboxes = True
            Columns = <
              item
                Caption = 'Enabled'
                Width = 55
              end
              item
                Caption = 'Code'
                Width = 55
              end
              item
                Caption = 'Data'
                Width = 174
              end>
            ReadOnly = True
            RowSelect = True
            TabOrder = 0
            ViewStyle = vsReport
            OnDblClick = WitsEditClick
          end
        end
      end
      object WitsAddButton: TButton
        Left = 469
        Top = 382
        Width = 75
        Height = 25
        Caption = 'Add'
        TabOrder = 4
        OnClick = WitsAddButtonClick
      end
      object WitsRemoveButton: TButton
        Left = 633
        Top = 382
        Width = 75
        Height = 25
        Caption = 'Remove'
        TabOrder = 6
        OnClick = WitsRemoveButtonClick
      end
      object WitsDuplicateBtn: TButton
        Left = 353
        Top = 382
        Width = 100
        Height = 25
        Caption = 'Copy RFC Data'
        TabOrder = 3
        OnClick = WitsDuplicateBtnClick
      end
      object WitsEditButton: TButton
        Left = 551
        Top = 382
        Width = 75
        Height = 25
        Caption = 'Edit'
        TabOrder = 5
        OnClick = WitsEditClick
      end
    end
  end
  object OKBtn: TButton
    Left = 560
    Top = 465
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'O&K'
    TabOrder = 1
    OnClick = OKBtnClick
  end
  object CancelBtn: TButton
    Left = 650
    Top = 465
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object SaveLogDlg: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All File (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Log File As...'
    Left = 607
  end
  object RFCPollTimer: TTimer
    Enabled = False
    OnTimer = RFCPollTimerTimer
    Left = 551
  end
  object OpenCalDataDlg: TOpenDialog
    DefaultExt = 'ini'
    Filter = 'Calibrated Data Files (*.ini)|*.ini|All Files (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Title = 'Load Calibration Data from File'
    Left = 503
  end
  object SaveCalDataDlg: TSaveDialog
    DefaultExt = 'ini'
    Filter = 'Calibrated Data Files (*.ini)|*.ini|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Calibration Data to File'
    Left = 447
  end
  object HiddenPopup: TPopupMenu
    Left = 656
    Top = 8
    object SetHousingSN1: TMenuItem
      Caption = 'Set Housing SN'
      ShortCut = 24648
      Visible = False
      OnClick = SetHousingSN1Click
    end
  end
end
