#ifndef SystemSettingsDlgH
#define SystemSettingsDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.Menus.hpp>
#include "JobManager.h"
#include "CommsMgr.h"


class TSystemSettingsForm : public TForm
{
__published:    // IDE-managed Components
    TPageControl *PgCtrl;
    TButton *OKBtn;
    TButton *CancelBtn;
    TTabSheet *CommsSheet;
    TTabSheet *CalSheet;
    TTabSheet *MiscSheet;
    TSaveDialog *SaveLogDlg;
    TGroupBox *JobDirGB;
    TEdit *JobDirEdit;
    TButton *BrowseJobDirBtn;
    TGroupBox *SysAdminPWGB;
    TLabel *Label8;
    TLabel *Label9;
    TLabel *Label10;
    TEdit *CurrPWEdit;
    TEdit *NewPWEdit1;
    TEdit *NewPWEdit2;
    TButton *SavePWBtn;
    TPageControl *CalDataPgCtrl;
    TTabSheet *DevMemorySheet;
    TTabSheet *RawDataSheet;
    TValueListEditor *FormattedCalDataEditor;
    TButton *LoadCalDatBtn;
    TButton *SaveCalDatBtn;
    TValueListEditor *RawCalDataEditor;
    TTimer *RFCPollTimer;
    TButton *WriteToDevBtn;
    TButton *ReadFromDevBtn;
    TOpenDialog *OpenCalDataDlg;
    TSaveDialog *SaveCalDataDlg;
    TGroupBox *JobStatusGB;
    TButton *CompleteJobBtn;
    TButton *ReopenJobBtn;
    TPageControl *DevConnPgCtrl;
    TTabSheet *WTTTSSheet;
    TTabSheet *BaseRadioSheet;
    TGroupBox *DataLoggingGB;
    TCheckBox *EnableRawLogCB;
    TEdit *RawLogFileNameEdit;
    TButton *BrowseRawLogFileBtn;
    TCheckBox *EnableCalLogCB;
    TEdit *CalLogFileNameEdit;
    TButton *BrowseCalLogFileBtn;
    TGroupBox *DiagnosticsGB;
    TLabel *Label7;
    TValueListEditor *LastRFCEditor;
    TGroupBox *BRDiagnosticsGB;
    TLabel *Label37;
    TGroupBox *BRCmdGB;
    TButton *SendChangeChanBtn;
    TComboBox *BRChanCombo;
    TLabel *Label39;
    TValueListEditor *LastStatusEditor;
    TButton *ZeroReadingsBtn;
    TComboBox *BRRadNbrCombo;
    TGroupBox *PriorityGB;
    TComboBox *ProcPriorityCombo;
    TGroupBox *ConnFilterGB;
    TLabel *Label36;
    TLabel *Label38;
    TEdit *ConnMinTurnsEdit;
    TGroupBox *AutoShlderOptGB;
    TCheckBox *ASOverrideNagCB;
    TTabSheet *InterlockSheet;
    TGroupBox *SettingsGB;
    TLabel *CDSRelLB;
    TLabel *CDSSlipLockedLB;
    TButton *NewBattBtn;
    TGroupBox *InterlockEnableGB;
    TCheckBox *InterlockEnCB;
    TGroupBox *DataAveGB;
    TLabel *TorqueLB;
    TLabel *TensionLB;
    TLabel *LevelLB;
    TComboBox *TorqueCombo;
    TComboBox *TensionCombo;
    TLabel *CDSRelUnitLB;
    TLabel *CDSSlipLockedUnitLB;
    TLabel *CDSLockedLB;
    TLabel *MinRelTimeLB;
    TLabel *MinRelTimeSecLB;
    TEdit *CDSRelEdit;
    TEdit *CDSSlipLockedEdit;
    TEdit *CDSLockedEdit;
    TEdit *MinRelTimeEdit;
    TLabel *CDSLockedUnitLB;
    TEdit *TorqueAvgVarEdit;
    TEdit *TensionAvgVarEdit;
    TTabSheet *PortsSheet;
    TGroupBox *PortsGB;
    TLabel *Label19;
    TCheckBox *AutoAssignPortsCB;
    TLabel *Label21;
    TLabel *Label22;
    TLabel *Label23;
    TComboBox *TesTORKPortTypeCombo;
    TComboBox *MgmtDevPortTypeCombo;
    TEdit *TesTORKPortEdit;
    TEdit *TesTORKParamEdit;
    TEdit *MgmtDevPortEdit;
    TEdit *MgmtDevParamEdit;
    TLabel *Label24;
    TLabel *Label20;
    TComboBox *BaseRadioPortTypeCombo;
    TEdit *BaseRadioPortEdit;
    TEdit *BaseRadioParamEdit;
    TGroupBox *BackupSaveGB;
    TCheckBox *BackupSaveCB;
    TEdit *BackupDirEdit;
    TButton *BrowseBackupDirBtn;
    TGroupBox *PDSMonitorGB;
    TCheckBox *PDSMonEnabledCB;
    TGroupBox *TempCompGB;
    TCheckBox *EnableTempCompCB;
    TCheckBox *EnableRFCLogCB;
    TEdit *RFCLogFileNameEdit;
    TButton *BrowseRFCLogFileBtn;
    TCheckBox *EnableChartLogCB;
    TEdit *ChartLogFileNameEdit;
    TButton *BrowseChartLogFileBtn;
    TTabSheet *WitsSheet;
    TGroupBox *WitsPortsGB;
    TLabel *WitsPortTypeLB;
    TLabel *WitsPortAddressLB;
    TLabel *WitsParamLB;
    TComboBox *WitsPortTypeCombo;
    TEdit *WitsPortAddressEdit;
    TEdit *WitsParameterEdit;
    TLabel *WitsLB;
    TGroupBox *WitsHeaderGB;
    TCheckBox *WitsRFCHeaderCB;
    TLabel *WitsRFCCodeLB;
    TLabel *WitsRFCHeaderLB;
    TEdit *WitsRFCCodeEdit;
    TEdit *WitsRFCHeaderEdit;
    TCheckBox *WitsStreamHeaderCB;
    TLabel *WitsStreamCodeLB;
    TLabel *WitsStreamHeaderLB;
    TEdit *WitsStreamHeaderEdit;
    TEdit *WitsStreamCodeEdit;
    TListView *WitsRfcListView;
    TButton *WitsAddButton;
    TButton *WitsRemoveButton;
    TPageControl *WitsListPageControl;
    TTabSheet *RfcSheet;
    TTabSheet *StreamSheet;
    TListView *WitsStreamListView;
    TButton *WitsDuplicateBtn;
    TButton *WitsEditButton;
    TPopupMenu *HiddenPopup;
    TMenuItem *SetHousingSN1;
    TGroupBox *AutoRecordGB;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TLabel *Label4;
    TLabel *ARTqStartUnitLB;
    TLabel *Label11;
    TLabel *ARTqCrossedUnitLB;
    TEdit *ARRPMToStartEdit;
    TEdit *ARTqToStartEdit;
    TEdit *ARTimeToStopEdit;
    TEdit *ARXThreadThreshEdit;
    TGroupBox *BROut4GB;
    TLabel *Label5;
    TComboBox *BROut3FcnCombo;
    TButton *BrTestOut4Btn;
    TCheckBox *AutoRecAlarmEnCB;
    TCheckBox *EnableARCB;
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall CommTypeClick(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall BrowseRawLogFileBtnClick(TObject *Sender);
    void __fastcall SavePWBtnClick(TObject *Sender);
    void __fastcall BrowseJobDirBtnClick(TObject *Sender);
    void __fastcall BrowseCalLogFileBtnClick(TObject *Sender);
    void __fastcall RFCPollTimerTimer(TObject *Sender);
    void __fastcall ListEditorSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
    void __fastcall SaveCalDatBtnClick(TObject *Sender);
    void __fastcall LoadCalDatBtnClick(TObject *Sender);
    void __fastcall WriteToDevBtnClick(TObject *Sender);
    void __fastcall ReadFromDevBtnClick(TObject *Sender);
    void __fastcall CompleteJobBtnClick(TObject *Sender);
    void __fastcall ReopenJobBtnClick(TObject *Sender);
    void __fastcall SendChangeChanBtnClick(TObject *Sender);
    void __fastcall ZeroReadingsBtnClick(TObject *Sender);
    void __fastcall NewBattBtnClick(TObject *Sender);
    void __fastcall AutoAssignPortsCBClick(TObject *Sender);
    void __fastcall BrowseBackupDirBtnClick(TObject *Sender);
    void __fastcall BaseRadioPortTypeComboClick(TObject *Sender);
    void __fastcall TesTORKPortTypeComboClick(TObject *Sender);
    void __fastcall MgmtDevPortTypeComboClick(TObject *Sender);
    void __fastcall BrowseRFCLogFileBtnClick(TObject *Sender);
    void __fastcall BrowseChartLogFileBtnClick(TObject *Sender);
    void __fastcall WitsRFCHeaderCBClick(TObject *Sender);
    void __fastcall WitsStreamHeaderCBClick(TObject *Sender);
    void __fastcall WitsPortTypeComboClick(TObject *Sender);
    void __fastcall WitsAddButtonClick(TObject *Sender);
    void __fastcall WitsRemoveButtonClick(TObject *Sender);
    void __fastcall WitsEditClick(TObject *Sender);
    void __fastcall WitsDuplicateBtnClick(TObject *Sender);
    void __fastcall WitsListPageControlChange(TObject *Sender);
    void __fastcall SetHousingSN1Click(TObject *Sender);
    void __fastcall BROut3FcnComboClick(TObject *Sender);
    void __fastcall BRRadNbrComboClick(TObject *Sender);

private:
    TCommPoller* m_poller;
    TJob*        m_currJob;

    DWORD        m_dwSettingsChanged;

    UNITS_OF_MEASURE m_uomType;

    typedef struct {
        TComboBox* pTypeCombo;
        TEdit*     pPortNbrEdit;
        TEdit*     pParamEdit;
    } COMMS_SHEET_CTRLS;

    COMMS_SHEET_CTRLS m_WTTTSCommsCtrls;
    COMMS_SHEET_CTRLS m_BaseCommsCtrls;
    COMMS_SHEET_CTRLS m_MgmtCommsCtrls;

    typedef struct {
        DEVICE_TYPE devType;
        COMMS_CFG   wtttsCommCfg;
        COMMS_CFG   baseCommCfg;
        COMMS_CFG   mgmtCommCfg;
        bool        bAutoAssignPorts;
        eBR3OutFcn  br3OutFcn;
    } COMMS_PAGE_SETTINGS;

    COMMS_PAGE_SETTINGS m_OrigCommsSettings;

    typedef struct {
        String           sJobDir;
        DWORD            dwProcPriority;
        double           dMinTurns;
        bool             bASOverrideNag;
        String           sBackupDir;
        bool             bForceBackupSave;
        bool             bEnableTempComp;
        AVERAGING_PARAMS avgTQParams;
        AVERAGING_PARAMS avgTNParams;
        bool             bPDSAlarmEn;
    } MISC_PAGE_SETTINGS;

    MISC_PAGE_SETTINGS m_OrigMiscSettings;

    typedef struct {
        int  iCDSRelsed;
        int  iCDSSlipsLocked;
        int  iCDSlockSlipRel;
        int  iReleaseTime;
        bool bIntLockEnabled;
    } INTERLOCK_PAGE_SETTINGS;

    INTERLOCK_PAGE_SETTINGS m_OrigInterlockSettings;

    typedef struct {
        AUTO_REC_SETTINGS arSettings;
    } AUTORECORD_PAGE_SETTINGS;

    AUTORECORD_PAGE_SETTINGS m_OrigAutoRecSettings;

    typedef struct {
        COMM_TYPE      portType;
        UnicodeString  sPortName;
        DWORD          dwPortParam;
        bool           bRfcHdrEnabled;
        int            iRfcHdrCode;
        String         sRfcHdrString;
        bool           bStreamHdrEnabled;
        int            iStreamHdrCode;
        String         sStreamHdrString;
    } WITS_PAGE_SETTINGS;

    WITS_PAGE_SETTINGS m_OrigWitsSettings;

    void         SetCommsCtrls( const COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls );
    TWinControl* GetCommsCtrls(       COMMS_CFG& commsCfg, const COMMS_SHEET_CTRLS& sheetCtrls );

    void LoadCommsPage( void );
    void LoadCalPage( void );
    void LoadMiscPage( void );
    void LoadInterlockPage( void );
    void LoadWitsPage( void );

    TWinControl* CheckCommsPage( void );
    TWinControl* CheckCalPage( void );
    TWinControl* CheckMiscPage( void );
    TWinControl* CheckInterlockPage( void );
    TWinControl* CheckWitsPage( void );

    // All controls must be valid when calling the following 'Save' functions
    DWORD SaveCommsPage( void );
    DWORD SaveCalPage( void );
    DWORD SaveMiscPage( void );
    DWORD SaveInterlockPage( void );
    DWORD SaveWitsPage( void );

    // Support functions
    void SetJobStatusBtns( void );

public:
    __fastcall TSystemSettingsForm(TComponent* Owner);

    __property TCommPoller* PollObject      = { read = m_poller,  write = m_poller  };
    __property TJob*        CurrentJob      = { read = m_currJob, write = m_currJob };
    __property DWORD        SettingsChanged = { read = m_dwSettingsChanged };

    // Further settings changed should be added by inceasing the number by a factor of 2 after every 4 entries
    typedef enum {
        COMMS_SETTINGS_CHANGED     = 0x0001,
        CAL_SETTINGS_CHANGED       = 0x0010,
        MISC_SETTINGS_CHANGED      = 0x0100,
        INTERLOCK_SETTINGS_CHANGED = 0x1000,
        AUTO_REC_SETTINGS_CHANGED  = 0x2000,
        WITS_SETTINGS_CHANGED      = 0x0002
    } SETTINGS_CHANGED;

    // This is not an auto-created form. Must call the static ShowDlg() method
    // to construct, show, and then destroy the dialog.
    static DWORD ShowDlg( TCommPoller* pPoller, TJob* currJob );

};

extern PACKAGE TSystemSettingsForm *SystemSettingsForm;

#endif
