#include <vcl.h>
#pragma hdrstop

#include "TAlarmDlg.h"
#include "TescoShared.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TAlarmDlg *AlarmDlg;


#define ET_ALARM_BIT  0x80000000


__fastcall TAlarmDlg::TAlarmDlg(TComponent* Owner) : TForm(Owner)
{
    m_pAckQueue = new TList();

    m_hwndMsg = NULL;
    m_msgNbr  = 0;
}


void __fastcall TAlarmDlg::FormDestroy(TObject *Sender)
{
    if( m_pAckQueue != NULL )
    {
        delete m_pAckQueue;
        m_pAckQueue = NULL;
    }
}


void TAlarmDlg::SetAckEventMessage( HWND hDestWindow, int uMsgNbr )
{
    // Call to set the message number of send to a window
    // when an event is ack'd. WParam will be an AckEventType
    // enum, LParam is not used.
    m_hwndMsg = hDestWindow;
    m_msgNbr  = uMsgNbr;
}


void TAlarmDlg::DisplayEvent( TAlarmDlg::EventType etType, bool bIsAlarm )
{
    // Queues the passed event for display and ack by the user.
    // The dialog appears immediately if there are no pending
    // events waiting ack. Otherwise, the event is queued for
    // display after all prior events have been ack'd

    // Just add the event to the queue. The timer tick will
    // pick this up and display the dialog. The alarm indication
    // will set the higher order bit - a hack indeed.
    DWORD dwItem = etType;

    if( bIsAlarm )
        dwItem |= ET_ALARM_BIT;

    m_pAckQueue->Add( (void*)dwItem );
}


String TAlarmDlg::EventShortDesc( EventType etType )
{
    // Returns a short description text for the event
    switch( etType )
    {
        case eET_Test:          return "Alarm Test";
        case eET_LowBattery:    return "Low TesTORK Battery";
        case eET_BadConnLength: return "Bad Connection Length";
        case eET_NoShoulder:    return "Shoulder Not Found";
        case eET_ConnFailed:    return "Failed Connection";
        case eET_NoInventory:   return "No Inventory";
        case eET_CrossThread:   return "Cross Thread Detected";
        case eET_BaseRadioLost: return "Comms with Base Radio lost";
        case eET_TesTORKLost:   return "Comms with TesTORK lost";
    }

    // Fall through means unknown type
    return "Event Type " + IntToStr( (int)etType );
}


bool TAlarmDlg::HaveEvents( void )
{
    // Returns true if one or more events are queued for ack'ing
    return( m_pAckQueue->Count > 0 );
}


void TAlarmDlg::Flush( void )
{
    // Call to flush the event queue of any pending events
    // and close the form if visible.
    m_pAckQueue->Clear();

    // Be sure we close the form on flush as well
    ModalResult = mrCancel;
}


void __fastcall TAlarmDlg::MesssagePaintBoxPaint(TObject *Sender)
{
    TCanvas* pCanvas = MesssagePaintBox->Canvas;

    // Increase the point size of the font beyond the default (parent) font
    pCanvas->Font->Size = 14;

    // Fill the label area and draw its text
    pCanvas->Brush->Color = clLtGray;
    pCanvas->Brush->Style = bsSolid;

    pCanvas->Pen->Color = clDkGray;
    pCanvas->Pen->Style = psSolid;
    pCanvas->Pen->Width = BorderWidth;

    pCanvas->Rectangle( TRect( 1, 1, MesssagePaintBox->Width, MesssagePaintBox->Height ) );

    const int BorderWidth = 8;

    RECT rCaption;
    rCaption.left   = BorderWidth;
    rCaption.top    = BorderWidth;
    rCaption.right  = MesssagePaintBox->Width - BorderWidth;
    rCaption.bottom = MesssagePaintBox->Height - BorderWidth;

    DrawText( pCanvas->Handle, m_sMessage.w_str(), m_sMessage.Length(), &rCaption, DT_WORDBREAK | DT_VCENTER | DT_CENTER );
}


void __fastcall TAlarmDlg::DisplayTimerTimer(TObject *Sender)
{
    // If we have no events, can return now
    if( m_pAckQueue->Count == 0 )
        return;

    // We have at least one event to show. Kill the timer
    // while we go modal.
    if( m_pAckQueue->Count > 0 )
    {
        DisplayTimer->Enabled = false;

        // Extract the next item. It will consist of an event type enum and
        // an alarm flag.
        DWORD dwCurrItem = (DWORD)( m_pAckQueue->Items[0] );

        EventType eCurrEvent = (EventType)( dwCurrItem & ~ET_ALARM_BIT );
        bool      bIsAlarm   = ( dwCurrItem & ET_ALARM_BIT ) ? true : false;

        switch( eCurrEvent )
        {
            case eET_Test:           m_sMessage = "Alarm system test.";  break;
            case eET_LowBattery:     m_sMessage = "The TesTORK battery voltage is low. Please replace the battery immediately.";  break;
            case eET_BadConnLength:  m_sMessage = "The connection Length is not valid.";  break;
            case eET_NoShoulder:     m_sMessage = "A shoulder could not be found.";  break;
            case eET_ConnFailed:     m_sMessage = "The connection has failed.";  break;
            case eET_NoInventory:    m_sMessage = "There is no casing inventory left. You will need to add inventory or disable Auto-Recording."; break;
            case eET_CrossThread:    m_sMessage = "A cross-thread condition has been detected.";  break;
            case eET_BaseRadioLost:  m_sMessage = "Communications with the Base Radio have been lost - system is scanning for a Base Radio.";  break;
            case eET_TesTORKLost:    m_sMessage = "Communications with the TesTORK have been lost - system is scanning for a TesTORK.";  break;
            default:                 m_sMessage = "Unknown event " + IntToStr( eCurrEvent ) + " reported";  break;
        }

        if( bIsAlarm )
        {
            Caption = "TesTORK Alarm";

            TitlePanel->Color = clTESCORed;
        }
        else
        {
            Caption = "TesTORK Event";

            TitlePanel->Color = clTESCOBlue;
        }

        // Show the dialog.
        ShowModal();

        // Okay to re-enable time
        DisplayTimer->Enabled = true;
    }
}


void __fastcall TAlarmDlg::AckBtnClick(TObject *Sender)
{
    // Tell the client this alarm was ack'd
    if( m_pAckQueue->Count > 0 )
    {
        DWORD dwCurrItem = (DWORD)( m_pAckQueue->Items[0] );

        EventType eCurrEvent = (EventType)( dwCurrItem & ~ET_ALARM_BIT );

        if( m_hwndMsg != NULL )
            ::PostMessage( m_hwndMsg, m_msgNbr, (UINT)eCurrEvent, 0 );

        // Delete this event from the queue
        m_pAckQueue->Delete( 0 );
    }

    ModalResult = mrOk;
}

