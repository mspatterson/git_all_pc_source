object AlarmDlg: TAlarmDlg
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'TesTORK Alarm'
  ClientHeight = 322
  ClientWidth = 543
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object TitlePanel: TPanel
    Left = 0
    Top = 0
    Width = 543
    Height = 73
    Align = alTop
    BevelEdges = [beLeft, beTop, beRight]
    BevelKind = bkFlat
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = 'TesTORK Alarm'
    Color = clRed
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
  end
  object MessagePanel: TPanel
    Left = 0
    Top = 73
    Width = 543
    Height = 249
    Align = alClient
    BevelKind = bkFlat
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = 'MessagePanel'
    Color = clWindow
    ParentBackground = False
    ShowCaption = False
    TabOrder = 1
    object MesssagePaintBox: TPaintBox
      Left = 16
      Top = 17
      Width = 505
      Height = 128
      OnPaint = MesssagePaintBoxPaint
    end
    object AckBtn: TButton
      Left = 107
      Top = 168
      Width = 313
      Height = 57
      Caption = 'Acknowledge'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = AckBtnClick
    end
  end
  object DisplayTimer: TTimer
    Interval = 500
    OnTimer = DisplayTimerTimer
    Left = 496
    Top = 24
  end
end
