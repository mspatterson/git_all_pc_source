//---------------------------------------------------------------------------
#ifndef TAlarmDlgH
#define TAlarmDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------

class TAlarmDlg : public TForm
{
__published:	// IDE-managed Components
    TPanel *TitlePanel;
    TPanel *MessagePanel;
    TButton *AckBtn;
    TPaintBox *MesssagePaintBox;
    TTimer *DisplayTimer;
    void __fastcall MesssagePaintBoxPaint(TObject *Sender);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall DisplayTimerTimer(TObject *Sender);
    void __fastcall AckBtnClick(TObject *Sender);

private:
    TList* m_pAckQueue;
    HWND   m_hwndMsg;
    int    m_msgNbr;

    String m_sMessage;

public:
    __fastcall TAlarmDlg(TComponent* Owner);

    void SetAckEventMessage( HWND hDestWindow, int uMsgNbr );
        // Call to set the message number of send to a window
        // when an event is ack'd. WParam will be an AckEventType
        // enum, LParam is not used.

    typedef enum {
       eET_Test,
       eET_LowBattery,
       eET_BadConnLength,
       eET_NoShoulder,
       eET_ConnFailed,
       eET_NoInventory,
       eET_CrossThread,
       eET_BaseRadioLost,
       eET_TesTORKLost,
       eET_NbrEventTypes
    } EventType;

    void DisplayEvent( EventType etType, bool bIsAlarm = false );
        // Queues the passed event for display and ack by the user.
        // The dialog appears immediately if there are no pending
        // events waiting ack. Otherwise, the event is queued for
        // display after all prior events have been ack'd

    String EventShortDesc( EventType etType );
        // Returns a short description text for the event

    bool HaveEvents( void );
        // Returns true if one or more events are queued for ack'ing

    void Flush( void );
        // Call to flush the event queue of any pending events
        // and close the form if visible.
};
//---------------------------------------------------------------------------
extern PACKAGE TAlarmDlg *AlarmDlg;
//---------------------------------------------------------------------------
#endif
