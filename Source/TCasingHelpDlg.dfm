object CasingHelpDlg: TCasingHelpDlg
  Left = 0
  Top = 0
  Caption = 'Importing a Casing File'
  ClientHeight = 425
  ClientWidth = 546
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    546
    425)
  PixelsPerInch = 96
  TextHeight = 13
  object CloseBtn: TButton
    Left = 463
    Top = 392
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    TabOrder = 0
    OnClick = CloseBtnClick
    ExplicitLeft = 444
    ExplicitTop = 352
  end
  object HelpEdit: TRichEdit
    Left = 8
    Top = 8
    Width = 530
    Height = 378
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      'Importing Casing Data'
      ''
      
        'You can define casing lengths before you start a job by importin' +
        'g a file containing the data. The file must '
      'conform with the following requirements.'
      ''
      
        'The file must be a text file - it cannot be a .xls or other appl' +
        'ication binary file.'
      ''
      
        'Each line in the input file will be displayed as a line in the d' +
        'isplay grid. The first lines in the file may be header '
      
        'lines - these lines are displayed but will not generate any casi' +
        'ng length entries.'
      ''
      
        'After the header lines you can have any number of casing length ' +
        'data lines. These lines must be in '#39'comma- '
      
        'separated'#39' format with the first item in each line being a casin' +
        'g length. The casing length can be given with '
      
        'any number of decimal digits, but the software will only display' +
        ' up to two significant decimal digits. A casing '
      
        'length must be a value greater than zero to be valid. If a line ' +
        'contains a valid casing length, that length will'
      
        'be displayed in the second column of the grid. However, if the l' +
        'ength is not valid then the text "???" will '
      
        'appear in that column. You cannot import a file if "???" appears' +
        ' in any row, and in that case you will have to '
      
        'edit the file in the software that originally created the file a' +
        'nd then re-import the file.'
      ''
      
        'Blank lines are not allowed in between casing length data lines.' +
        ' However, the importer will ignore any blank '
      'lines that are at the end of the file.'
      ''
      
        'The last line in the grid will display the total length of the c' +
        'asing data that was imported. If any row contains '
      
        'an invalid length, then the total length will display "???". How' +
        'ever, if all entries appear to be valid then the '
      
        'final row will show the total length. The total length shown her' +
        'e should match the expected total length of '
      
        'the string. If the length does not match, then review your origi' +
        'nal source file, edit if necessary, and then re-'
      'import it.')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
    Zoom = 100
    ExplicitWidth = 511
    ExplicitHeight = 369
  end
end
