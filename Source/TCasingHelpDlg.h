//---------------------------------------------------------------------------

#ifndef TCasingHelpDlgH
#define TCasingHelpDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
//---------------------------------------------------------------------------
class TCasingHelpDlg : public TForm
{
__published:	// IDE-managed Components
    TButton *CloseBtn;
    TRichEdit *HelpEdit;
    void __fastcall CloseBtnClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TCasingHelpDlg(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCasingHelpDlg *CasingHelpDlg;
//---------------------------------------------------------------------------
#endif
