#include <vcl.h>
#pragma hdrstop

#include "TCirculatingDlg.h"
#include "RegistryInterface.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TCirculatingDlg *CirculatingDlg;


static const String StartCircCaption( "Start Circulating (F1)" );
static const String StopCircCaption ( "Stop Circulating (F1)" );


__fastcall TCirculatingDlg::TCirculatingDlg(TComponent* Owner) : TForm(Owner)
{
    // Init start button props
    StartStopCircButton->Caption = StartCircCaption;
    StartStopCircButton->Tag     = eButtonTypeStart;
    StartStopCircButton->Enabled = false;

    // Set state
    CanStartCirc = false;
}


void TCirculatingDlg::SetEventCallbacks( TCircDlgEvent dlgCallback )
{
    m_circDlgEvent = dlgCallback;
}


void TCirculatingDlg::ShowForm( void )
{
    // Show the dialog modelessly
    Show();
}


void __fastcall TCirculatingDlg::FormShow(TObject *Sender)
{
    // Restore dialog position
    int top  = Top;
    int left = Left;
    int heightNotUsed = 0;
    int widthNotUsed  = 0;

    GetDialogPos( DT_CIRC_DLG, top, left, heightNotUsed, widthNotUsed );

    // Only set the position if it's on screen. Avoid issues with dual monitors
    if( top < 0 )
        Top = 10;
    else if( top < Screen->DesktopHeight - Height )
        Top = top;
    else
        Top = Screen->DesktopHeight - Height;

    if( left < 0 )
        Left = 10;
    else if( left < Screen->DesktopWidth - Width )
        Left = left;
    else
        Left = Screen->DesktopWidth - Width;

    // Enabled poll timer on show
    PollTimer->Enabled = true;
}


void __fastcall TCirculatingDlg::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    // Can't close while circulating
    if( StartStopCircButton->Tag == eButtonTypeStop )
    {
        MessageDlg( "You cannot close this form while circulation is in progress.", mtError, TMsgDlgButtons() << mbOK, 0 );
        CanClose = false;
    }
    else
    {
        CanClose = true;
    }
}


void __fastcall TCirculatingDlg::FormClose(TObject *Sender, TCloseAction &Action)
{
    // Disable poll timer on close
    PollTimer->Enabled = false;
}


void __fastcall TCirculatingDlg::WMExitMove( TMessage &Message )
{
    // Save form position, if visible
    if( Visible )
        SaveDialogPos( DT_CIRC_DLG, Top, Left, Height, Width );
}


void __fastcall TCirculatingDlg::CreateParams(Controls::TCreateParams &Params)
{
    // Add the WS_EX_APPWINDOW style to this form to cause its icon to be
    // be made present on the task bar
    TForm::CreateParams(Params);

    Params.ExStyle   = Params.ExStyle | WS_EX_APPWINDOW;
    Params.WndParent = ParentWindow;
}


void __fastcall TCirculatingDlg::SetCanStartCirc( bool bCanNowStart )
{
    // Set the system state. If we can't change the state because
    // we're currently circulating, we'll do that in the button click.
    if( StartStopCircButton->Tag == eButtonTypeStart )
    {
        // We're idle, can set button state
        StartStopCircButton->Enabled = bCanNowStart;
    }

    m_bCanStartCirc = bCanNowStart;
}


bool __fastcall TCirculatingDlg::GetIsCirculating( void )
{
    if( !Visible )
        return false;

    return ( StartStopCircButton->Tag == eButtonTypeStop );
}


bool __fastcall TCirculatingDlg::GetIsActive( void )
{
    // We are active if the poll timer is enabled
    return PollTimer->Enabled;
}


void __fastcall TCirculatingDlg::PollTimerTimer(TObject *Sender)
{
    // Poll timer is always enabled so that the comms status is up to date.
    // Elapsed time is only refreshed if we are circulating
    if( StartStopCircButton->Tag == eButtonTypeStop )
    {
        // Refresh the elapsed time
        TDateTime elapsedTime = Now() - m_StartTime;

        TimePanel->Caption = "Elapsed Time " + FormatDateTime( "hh:nn:ss", elapsedTime );
    }
}


void __fastcall TCirculatingDlg::StartStopCircButtonClick(TObject *Sender)
{
    if( StartStopCircButton->Tag == eButtonTypeStart )
    {
        // Handle Start Button Click here
        if( m_circDlgEvent( eCircEvt_StartCirc ) )
        {
            // A little hack here. Calling the circ event callback will
            // cause the CanStart property to get set to false, which
            // will disable the Start/Stop button - not a good thing.
            // So ensure the button is enabled here.
            StartStopCircButton->Caption = StopCircCaption;
            StartStopCircButton->Tag     = eButtonTypeStop;
            StartStopCircButton->Enabled = true;

            m_StartTime = Now();
        }
    }
    else if( StartStopCircButton->Tag == eButtonTypeStop )
    {
        // Handle Stop Button Click here
        if( m_circDlgEvent( eCircEvt_StopCirc ) )
        {
            StartStopCircButton->Caption = StartCircCaption;
            StartStopCircButton->Tag     = eButtonTypeStart;

            // Set the button state based on whether we can start circ again
            StartStopCircButton->Enabled = m_bCanStartCirc;
        }
    }
}


void __fastcall TCirculatingDlg::StartStopItemClick(TObject *Sender)
{
    // Attempt to start or stop the connection
    if( StartStopCircButton->Enabled )
        StartStopCircButtonClick( StartStopCircButton );
}


bool TCirculatingDlg::ProcessSample( const WTTTS_READING& nextReading, int rpm )
{
    // Auto-circulation Support. Returns true if reading should *not* be plotted
    return false;
}


