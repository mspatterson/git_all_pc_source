#ifndef TCirculatingDlgH
#define TCirculatingDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#include "JobManager.h"


// This dialog is an extension of the main form. It manages the circulating
// state and button states in this dialog, but must keep its state in sync
// with what is happening on the main form. Main provides a callback function
// that helps this dialog controls its state.
typedef enum {
    eCircEvt_StartCirc,
    eCircEvt_StopCirc,
    eCircEvt_NbrEvents
} eCircEvent;

typedef bool __fastcall (__closure *TCircDlgEvent)( eCircEvent anEvent );


class TCirculatingDlg : public TForm
{
__published:    // IDE-managed Components
    TGroupBox *NewConnGB;
    TPanel *TimePanel;
    TTimer *PollTimer;
    TButton *StartStopCircButton;
    TPopupMenu *HiddenPopUp;
    TMenuItem *StartStopItem;
    void __fastcall PollTimerTimer(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall StartStopCircButtonClick(TObject *Sender);
    void __fastcall CreateParams(Controls::TCreateParams &Params);
    void __fastcall StartStopItemClick(TObject *Sender);

  private:

    typedef enum{
        eButtonTypeStart,
        eButtonTypeStop
    } BUTTON_TYPE_TAG;

    TDateTime     m_StartTime;
    TCircDlgEvent m_circDlgEvent;

    bool __fastcall GetIsCirculating( void );
    bool __fastcall GetIsActive( void );

    bool m_bCanStartCirc;

    void __fastcall SetCanStartCirc( bool bCanNowStart );

  protected:

    void __fastcall WMExitMove( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_EXITSIZEMOVE, TMessage, WMExitMove )
    END_MESSAGE_MAP(TForm)

  public:
    __fastcall TCirculatingDlg(TComponent* Owner);

    // Set the event callbacks immediately after the form has been contructed
    void SetEventCallbacks( TCircDlgEvent dlgCallback );

    void ShowForm( void );

    __property bool IsCirculating = { read = GetIsCirculating };
    __property bool IsActive      = { read = GetIsActive };

    __property bool CanStartCirc  = { read = m_bCanStartCirc, write = SetCanStartCirc };

    // Auto-circulation Support. Returns true if reading should *not* be plotted
    bool ProcessSample( const WTTTS_READING& nextReading, int rpm );
};
//---------------------------------------------------------------------------
extern PACKAGE TCirculatingDlg *CirculatingDlg;
//---------------------------------------------------------------------------
#endif
