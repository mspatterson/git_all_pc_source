#include <vcl.h>
#pragma hdrstop

#include "THardwareStatusDlg.h"
#include "RegistryInterface.h"
#include "ApplUtils.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


THardwareStatusDlg *HardwareStatusDlg;


const String CommStatusGridKeys[NBR_STATS_GRID_ENTRIES] = {
    "Wired Device Status",
    "Network Status",
    "Device Type",
    "Device Status",
    "Housing Serial Nbr",
    "RSSI",
    "Ctrl Port Type",
    "Ctrl Port Desc",
    "Data Port Type",
    "Data Port Desc",
    "Mgmt Port Type",
    "Mgmt Port Desc",
    "Ctrl Port Cmds Tx",
    "Ctrl Port Bytes Tx",
    "Ctrl Port Resp Rx",
    "Ctrl Port Last Resp",
    "Ctrl Port Pkt Errs",
    "Ctrl Port Bytes Rx",
    "Data Port Cmds Tx",
    "Data Port Bytes Tx",
    "Data Port Resp Rx",
    "Data Port Last Resp",
    "Data Port Pkt Errs",
    "Data Port Bytes Rx",
    "Raw Stream Data Log",
    "Cal'd Stream Data Log",
    "Raw Idle Data Log",
    "Cal'd Idle Data Log",
    "Streaming Time",
    "Battery Type",
    "Battery Volts",
    "Battery Capacity",
    "Average Tension",
    "Average Torque",
    "RPM",
    "RTD Slope",
    "Initial Comp Offset",
    "RTD Slope ROC",
    "Total Comp Offset",
};


__fastcall THardwareStatusDlg::THardwareStatusDlg(TComponent* Owner) : TForm(Owner)
{
    for( int iItem = 0; iItem < NBR_STATS_GRID_ENTRIES; iItem++ )
        CommsStatusView->InsertRow( CommStatusGridKeys[iItem], L"", true );
}


void __fastcall THardwareStatusDlg::FormShow(TObject *Sender)
{
    // Restore dialog position
    int top    = Top;
    int left   = Left;
    int width  = Width;
    int height = Height;

    GetDialogPos( DT_HARDWARE_DLG, top, left, height, width );

    // Only set the position if it's on screen. Avoid issues with dual monitors
    if( top < 0 )
        Top = 10;
    else if( top < Screen->DesktopHeight - Height )
        Top = top;
    else
        Top = Screen->DesktopHeight - Height;

    if( left < 0 )
        Left = 10;
    else if( left < Screen->DesktopWidth - Width )
        Left = left;
    else
        Left = Screen->DesktopWidth - Width;

    if( ( height > 0 ) && ( height != Height ) )
        Height = height;

    if( ( width > 0 ) && ( width != Width ) )
        Width = width;
}


void __fastcall THardwareStatusDlg::WMExitMove( TMessage &Message )
{
    // Save form position, if visible
    if( Visible )
        SaveDialogPos( DT_HARDWARE_DLG, Top, Left, Height, Width );
}


void __fastcall THardwareStatusDlg::CloseBtnClick(TObject *Sender)
{
    // This dialog is shown modelessly, so just set it to not visible
    Visible = false;
}


void THardwareStatusDlg::UpdateStatus( TJob* pJob, TCommPoller* pDevicePoller )
{
    String sUnused( "---" );

    CommsStatusView->Values[ CommStatusGridKeys[SGE_WIRED_DEV_STAT] ] = pDevicePoller->ConnectResult;

    // If our current job is in Sim Mode, overwrite the wired status (We don't care about it anyway)
    if( pJob != NULL )
    {
        if( pJob->InSimMode )
            CommsStatusView->Values[ CommStatusGridKeys[SGE_WIRED_DEV_STAT] ] = "Simulation";
    }

    if( pDevicePoller->DeviceIsRecving )
        CommsStatusView->Values[ CommStatusGridKeys[SGE_NETWORK_STAT] ] = "Receiving";
    else
        CommsStatusView->Values[ CommStatusGridKeys[SGE_NETWORK_STAT] ] = "Not connected";

    // Populate unused entries first
    PORT_STATS radioStats;

    if( pDevicePoller->GetRadioStats( radioStats ) )
    {
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_PORT_TYPE] ]   = CommTypeText[radioStats.portType];
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_PORT_DESC] ]   = radioStats.portDesc;

        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_NBR_CMDS_TX] ] = IntToStr( (int)radioStats.cmdsSent );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_NBR_RESP_RX] ] = IntToStr( (int)radioStats.respRecv );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_PKT_ERRORS] ]  = IntToStr( (int)radioStats.pktErrors );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_BYTES_SENT] ]  = IntToStr( (int)radioStats.bytesSent );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_BYTES_RECV] ]  = IntToStr( (int)radioStats.bytesRecv );

        CommsStatusView->Values[ CommStatusGridKeys[SGE_RSSI] ] = IntToStr( pDevicePoller->TesTORKRSSI );

        if( radioStats.tLastPkt == 0 )
            CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_T_LAST_PKT] ] = sUnused;
        else
            CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_T_LAST_PKT] ] = LocalTimeString( radioStats.tLastPkt );
    }
    else
    {
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_PORT_TYPE] ]   = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_PORT_DESC] ]   = sUnused;

        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_NBR_CMDS_TX] ] = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_NBR_RESP_RX] ] = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_PKT_ERRORS] ]  = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_BYTES_SENT] ]  = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_BYTES_RECV] ]  = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CTRL_T_LAST_PKT] ]  = sUnused;
    }

    // Show Average Torque and Tension. These are always shown as integer values.
    // and tension is always shown in increments of 10.
    String sAvgTension = "";
    String sAvgTorque  = "";
    String sAvgRPM     = "";

    float avgTorque  = 0.0;
    float avgTension = 0.0;
    int   avgRPM     = 0;

    if( pDevicePoller->GetLastAvgData( avgTorque, avgTension, avgRPM ) )
    {
        if( avgTorque == 0.0 )
            sAvgTorque = "0";
        else if( avgTorque > 0.0 )
            sAvgTorque = IntToStr( (int)( avgTorque + 0.5 ) );
        else
            sAvgTorque = IntToStr( (int)( avgTorque - 0.5 ) );

        if( avgTension == 0.0 )
            sAvgTension = "0";
        else if( avgTension > 0.0 )
            sAvgTension = IntToStr( ( (int)( avgTension + 0.5 ) / 10 ) * 10 );
        else
            sAvgTension = IntToStr( ( (int)( avgTension - 0.5 ) / 10 ) * 10 );

        sAvgRPM = IntToStr( avgRPM );
    }

    CommsStatusView->Values[ CommStatusGridKeys[SGE_AVG_TENSION] ] = sAvgTension;
    CommsStatusView->Values[ CommStatusGridKeys[SGE_AVG_TORQUE]  ] = sAvgTorque;
    CommsStatusView->Values[ CommStatusGridKeys[SGE_RPM]         ] = sAvgRPM;

    // Populate non-comms related stats
    CommsStatusView->Values[ CommStatusGridKeys[SGE_DEV_TYPE] ]   = pDevicePoller->DeviceTypeText;
    CommsStatusView->Values[ CommStatusGridKeys[SGE_DEV_STATUS] ] = pDevicePoller->DeviceStatus;
    CommsStatusView->Values[ CommStatusGridKeys[SGE_HOUSING_SN] ] = pDevicePoller->DeviceHousingSN;

    // Populate logging state
    String sTemp;
    bool loggingOn = GetDataLogging( DLT_RAW_DATA, sTemp );

    if( loggingOn )
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RAW_LOGGING_STATE] ] = "Enabled";
    else
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RAW_LOGGING_STATE] ] = "Disabled";

    loggingOn = GetDataLogging( DLT_CAL_DATA, sTemp );

    if( loggingOn )
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CAL_LOGGING_STATE] ] = "Enabled";
    else
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CAL_LOGGING_STATE] ] = "Disabled";

    loggingOn = GetDataLogging( DLT_RFC_DATA, sTemp );

    if( loggingOn )
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RFC_LOGGING_STATE] ] = "Enabled";
    else
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RFC_LOGGING_STATE] ] = "Disabled";

    loggingOn = GetDataLogging( DLT_CHART_DATA, sTemp );

    if( loggingOn )
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CHART_LOGGING_STATE] ] = "Enabled";
    else
        CommsStatusView->Values[ CommStatusGridKeys[SGE_CHART_LOGGING_STATE] ] = "Disabled";

    // Get stats now
    COMM_STATS commStats;

    if( pDevicePoller->CommsGetStats( commStats ) )
    {
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_PORT_TYPE] ]   = CommTypeText[commStats.portStats.portType];
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_PORT_DESC] ]   = commStats.portStats.portDesc;

        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_NBR_CMDS_TX] ] = IntToStr( (int)commStats.portStats.cmdsSent );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_NBR_RESP_RX] ] = IntToStr( (int)commStats.portStats.respRecv );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_PKT_ERRORS] ]  = IntToStr( (int)commStats.portStats.pktErrors );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_BYTES_SENT] ]  = IntToStr( (int)commStats.portStats.bytesSent );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_BYTES_RECV] ]  = IntToStr( (int)commStats.portStats.bytesRecv );

        if( commStats.portStats.tLastPkt == 0 )
            CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_T_LAST_PKT] ] = "";
        else
            CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_T_LAST_PKT] ] = LocalTimeString( commStats.portStats.tLastPkt );
    }
    else
    {
        // Get stats failed - blank out all entries
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_PORT_TYPE] ]   = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_PORT_DESC] ]   = sUnused;

        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_NBR_CMDS_TX] ] = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_NBR_RESP_RX] ] = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_PKT_ERRORS] ]  = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_BYTES_SENT] ]  = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_BYTES_RECV] ]  = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_DATA_T_LAST_PKT] ]  = sUnused;
    }

    // Mgmt port only shows a couple of things
    PORT_STATS mgmtStats;

    if( pDevicePoller->MgmtGetStats( mgmtStats ) )
    {
        CommsStatusView->Values[ CommStatusGridKeys[SGE_MGMT_PORT_TYPE] ] = CommTypeText[mgmtStats.portType];
        CommsStatusView->Values[ CommStatusGridKeys[SGE_MGMT_PORT_DESC] ] = mgmtStats.portDesc;
    }
    else
    {
        CommsStatusView->Values[ CommStatusGridKeys[SGE_MGMT_PORT_TYPE] ] = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_MGMT_PORT_DESC] ] = sUnused;
    }

    // Show streaming time
    if( pDevicePoller->DeviceIsStreaming )
    {
        int streamTime = (int)pDevicePoller->StreamingTime;

        String sStreamTime;
        sStreamTime.printf( L"%d:%02d", streamTime / 60, streamTime % 60 );

        CommsStatusView->Values[ CommStatusGridKeys[SGE_STREAMING_TIME] ] = sStreamTime;
    }
    else
    {
        CommsStatusView->Values[ CommStatusGridKeys[SGE_STREAMING_TIME] ] = sUnused;
    }

    // Show battery info
    BATTERY_INFO battInfo;

    if( pDevicePoller->GetDeviceBatteryInfo( battInfo ) )
    {
        // Determine capacity left.
        String mAhLeft;

        if( battInfo.initialCapacity > battInfo.avgUsage )
            mAhLeft = IntToStr( battInfo.initialCapacity - battInfo.avgUsage );
        else
            mAhLeft = "(unknown)";

        CommsStatusView->Values[ CommStatusGridKeys[SGE_BATT_TYPE] ]     = battInfo.battTypeText;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_BATT_VOLTS] ]    = FloatToStrF( battInfo.avgVolts, ffFixed, 7, 2 );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_BATT_CAP_LEFT] ] = mAhLeft;
    }
    else
    {
        CommsStatusView->Values[ CommStatusGridKeys[SGE_BATT_TYPE] ]     = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_BATT_VOLTS] ]    = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_BATT_CAP_LEFT] ] = sUnused;
    }

    // Debug for RTD averaging
    TSubDevice::TEMP_COMP_VALUES tcValues;

    if( pDevicePoller->GetTempCompInfo( tcValues ) )
    {
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RTD_SLOPE] ]        = FloatToStrF( tcValues.fRTDSlope,    ffFixed, 7, 3 );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RTD_SLOPE_ROC] ]    = FloatToStrF( tcValues.fRTDSlopeROC, ffFixed, 7, 3 );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RTD_OFFSET] ]       = IntToStr( (int)tcValues.fFirstOrderOffset );
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RTD_TOTAL_OFFSET] ] = IntToStr( (int)tcValues.fTotalTensionOffset );
    }
    else
    {
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RTD_SLOPE] ]        = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RTD_SLOPE_ROC] ]    = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RTD_OFFSET] ]       = sUnused;
        CommsStatusView->Values[ CommStatusGridKeys[SGE_RTD_TOTAL_OFFSET] ] = sUnused;
    }
}


void THardwareStatusDlg::SyncDisplay( TValueListEditor* pDestVLE )
{
    // Syncs display items in the dest VLE with values from this dialog's VLE
    if( pDestVLE == NULL )
        return;

    // For each item in the dest VLE, assign the text from our VLE. Skip
    // over any items that are in the fixed rows though. The VCL hides the
    // FixedRows prop for a VLE, so just start at row 1.
    for( int iItem = 1; iItem < pDestVLE->RowCount; iItem++ )
    {
         String sKey = pDestVLE->Keys[iItem];

         pDestVLE->Values[sKey] = CommsStatusView->Values[sKey];
    }
}

