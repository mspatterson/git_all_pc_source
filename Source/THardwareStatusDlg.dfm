object HardwareStatusDlg: THardwareStatusDlg
  Left = 0
  Top = 0
  Caption = 'Hardware Status'
  ClientHeight = 455
  ClientWidth = 384
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesigned
  OnShow = FormShow
  DesignSize = (
    384
    455)
  PixelsPerInch = 96
  TextHeight = 13
  object CommsStatusView: TValueListEditor
    Left = 8
    Top = 8
    Width = 368
    Height = 409
    Anchors = [akLeft, akTop, akRight, akBottom]
    DrawingStyle = gdsGradient
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goThumbTracking]
    TabOrder = 0
    TitleCaptions.Strings = (
      'Item'
      'Value')
    ColWidths = (
      129
      233)
  end
  object CloseBtn: TButton
    Left = 301
    Top = 423
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    TabOrder = 1
    OnClick = CloseBtnClick
  end
end
