//---------------------------------------------------------------------------
#ifndef THardwareStatusDlgH
#define THardwareStatusDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include "JobManager.h"
#include "CommsMgr.h"
//---------------------------------------------------------------------------

//
// The following list of enums identify common titles for hardware status items
//

typedef enum {
    SGE_WIRED_DEV_STAT,
    SGE_NETWORK_STAT,
    SGE_DEV_TYPE,
    SGE_DEV_STATUS,
    SGE_HOUSING_SN,
    SGE_RSSI,
    SGE_CTRL_PORT_TYPE,
    SGE_CTRL_PORT_DESC,
    SGE_DATA_PORT_TYPE,
    SGE_DATA_PORT_DESC,
    SGE_MGMT_PORT_TYPE,
    SGE_MGMT_PORT_DESC,
    SGE_CTRL_NBR_CMDS_TX,
    SGE_CTRL_BYTES_SENT,
    SGE_CTRL_NBR_RESP_RX,
    SGE_CTRL_T_LAST_PKT,
    SGE_CTRL_PKT_ERRORS,
    SGE_CTRL_BYTES_RECV,
    SGE_DATA_NBR_CMDS_TX,
    SGE_DATA_BYTES_SENT,
    SGE_DATA_NBR_RESP_RX,
    SGE_DATA_T_LAST_PKT,
    SGE_DATA_PKT_ERRORS,
    SGE_DATA_BYTES_RECV,
    SGE_RAW_LOGGING_STATE,
    SGE_CAL_LOGGING_STATE,
    SGE_RFC_LOGGING_STATE,
    SGE_CHART_LOGGING_STATE,
    SGE_STREAMING_TIME,
    SGE_BATT_TYPE,
    SGE_BATT_VOLTS,
    SGE_BATT_CAP_LEFT,
    SGE_AVG_TENSION,
    SGE_AVG_TORQUE,
    SGE_RPM,
    SGE_RTD_SLOPE,
    SGE_RTD_OFFSET,
    SGE_RTD_SLOPE_ROC,
    SGE_RTD_TOTAL_OFFSET,
    NBR_STATS_GRID_ENTRIES
} STATS_GRID_ENTRY;


extern const String CommStatusGridKeys[NBR_STATS_GRID_ENTRIES];


class THardwareStatusDlg : public TForm
{
__published:	// IDE-managed Components
    TValueListEditor *CommsStatusView;
    TButton *CloseBtn;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall CloseBtnClick(TObject *Sender);

private:

protected:

    void __fastcall WMExitMove( TMessage &Message );

    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_EXITSIZEMOVE, TMessage, WMExitMove )
    END_MESSAGE_MAP(TForm)

public:		// User declarations
    __fastcall THardwareStatusDlg(TComponent* Owner);

    void UpdateStatus( TJob* pJob, TCommPoller* pDevicePoller );
        // Refreshes the display of status

    void SyncDisplay( TValueListEditor* pDestVLE );
        // Syncs display items in the dest VLE with values from this
        // dialog's VLE

};
//---------------------------------------------------------------------------
extern PACKAGE THardwareStatusDlg *HardwareStatusDlg;
//---------------------------------------------------------------------------
#endif
