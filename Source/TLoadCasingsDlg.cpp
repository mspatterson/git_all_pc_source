#include <vcl.h>
#pragma hdrstop

#include "TLoadCasingsDlg.h"
#include "TCasingHelpDlg.h"
#include "JobManager.h"
#include "ApplUtils.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TLoadCasingsDlg *LoadCasingsDlg;


typedef enum {
    eCGC_FileData,
    eCGC_CasingLength,
    eCGC_NbrCols
} eCasingGridCols;


const String BadCasingLength = "-1";


__fastcall TLoadCasingsDlg::TLoadCasingsDlg(TComponent* Owner) : TForm(Owner)
{
    // Set up grid basics
    CasingDataGrid->ColCount = eCGC_NbrCols;

    CasingDataGrid->Cells[eCGC_FileData][0]     = "File Data";
    CasingDataGrid->Cells[eCGC_CasingLength][0] = "Casing Length";

    const int CasingLenColWidth = 120;

    CasingDataGrid->ColWidths[eCGC_CasingLength] = CasingLenColWidth;
    CasingDataGrid->ColWidths[eCGC_FileData]     = CasingDataGrid->ClientWidth - CasingLenColWidth - CasingDataGrid->GridLineWidth;
}


bool TLoadCasingsDlg::ShowDlg( TComponent* Owner, UNITS_OF_MEASURE uom, String& sLoadedFile, String& sCasingList )
{
   // Casing lengths are returned sCasingList as a string of CSV
   // items. Each item is an integer representing a length in 100's
   // of the current units of measure. This string is only populated
   // if a valid file has been loaded. The file name loaded is returned
   // in sLoadedFile, and is returned whether or not the file name
   // was valid. This method returns true only if a file was loaded
   // that contained at least one casing length.
    bool bDlgOK = false;

    TLoadCasingsDlg* pDlg = new TLoadCasingsDlg( Owner );

    try
    {
        // Clear return list
        sCasingList = "";

        pDlg->m_uom = uom;

        // Reload the previously loaded file, if we had one and it
        // still exists
        if( ( sLoadedFile.Length() > 0 ) && FileExists( sLoadedFile ) )
            pDlg->CasingFileNameEdit->Text = sLoadedFile;
        else
            pDlg->CasingFileNameEdit->Text = "";

        if( pDlg->ShowModal() == mrOk )
        {
            sCasingList = pDlg->m_sCasingsList;
            bDlgOK = true;
        }
        else
        {
            sCasingList = "";
            bDlgOK = false;
        }

        // Whatever the last file name was that was loaded, is returned to the caller
        sLoadedFile = pDlg->CasingFileNameEdit->Text;
    }
    catch( ... )
    {
        bDlgOK = false;
    }

    delete pDlg;

    return bDlgOK;
}


void __fastcall TLoadCasingsDlg::FormShow(TObject *Sender)
{
    // Set initial button states
    OKBtn->Enabled    = false;
    CloseBtn->Enabled = true;

    // Setup grid
    CasingDataGrid->RowCount = CasingDataGrid->FixedRows + 1;

    for( int iRow = CasingDataGrid->FixedRows; iRow < CasingDataGrid->RowCount; iRow++ )
        CasingDataGrid->Rows[iRow]->Clear();

    // Clear any current casings in the return list
    m_sCasingsList = "";

    // If we've been given a file, reload it. Note that when re-loading a
    // file we don't re-nag a user with any errors as an error would have
    // been shown when the first loaded it.
    if( CasingFileNameEdit->Text.Length() > 0 )
    {
         String sErrMsgNotUsed;
         LoadCasingsFile( CasingFileNameEdit->Text, sErrMsgNotUsed );
    }
}


void __fastcall TLoadCasingsDlg::LoadCasingFileBtnClick(TObject *Sender)
{
     if( FileOpenDlg->Execute() )
     {
         String sErrMsg;

         eLoadCasingResult eResult = LoadCasingsFile( FileOpenDlg->FileName, sErrMsg );

         if( eResult != eLCR_Good )
             MessageDlg( sErrMsg, mtError, TMsgDlgButtons() << mbOK, 0 );
     }
}


void __fastcall TLoadCasingsDlg::ClearFileBtnClick(TObject *Sender)
{
    int confRes = MessageDlg( "Are you sure you want to clear all casing entries?"
                              "\r  Click Yes to proceed and clear all entries."
                              "\r  Click Cancel to reconsider.",
                              mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    if( confRes != mrYes )
        return;

    // Clear the current file name, then just call the form show handler
    // again as it will re-init the dlg
    CasingFileNameEdit->Text = "";

    FormShow( NULL );
}


TLoadCasingsDlg::eLoadCasingResult TLoadCasingsDlg::LoadCasingsFile( const String& sFileName, String& sErrMsg )
{
     // First, clear the current grid entries.
     CasingDataGrid->RowCount = CasingDataGrid->FixedRows + 1;
     CasingDataGrid->Rows[CasingDataGrid->FixedRows]->Clear();

     // Disable the OK button until we have a known good file
     OKBtn->Enabled    = false;
     CloseBtn->Enabled = true;

     // Clear any currently defined casings
     m_sCasingsList = "";
     m_iTotalLength = 0;
     m_iCasingCount = 0;

     sErrMsg = "";

     // Now try to load the file
     CasingFileNameEdit->Text = sFileName;

     if( !FileExists( sFileName ) )
     {
         sErrMsg = "The selected file could not be loaded.";
         return eLCR_BadFile;
     }

     TStringList* pFileStrings = new TStringList();

     try
     {
         pFileStrings->LoadFromFile( sFileName );
     }
     catch( ... )
     {
         sErrMsg = "The selected file could not be opened. Ensure it is not open in another application and try again.";

         delete pFileStrings;

         return eLCR_BadFile;
     }

     // Trim off any trailing blank lines
     while( pFileStrings->Count > 0 )
     {
         if( Trim( pFileStrings->Strings[pFileStrings->Count-1] ).Length() > 0 )
             break;

         pFileStrings->Delete( pFileStrings->Count - 1 );
     }

     // Make sure we have some lines to work with
     if( pFileStrings->Count == 0 )
     {
         sErrMsg = "The selected file has no data in it.";

         delete pFileStrings;

         return eLCR_BadContents;
     }

     // Set the row count in the grid to match the size of the loaded file.
     // Two extra rows are added: one fixed row for column headings and one
     // for the total length.
     CasingDataGrid->RowCount = CasingDataGrid->FixedRows + pFileStrings->Count + 1;

     // Now try to parse each input line
     bool bHaveFirstValidLine  = false;
     bool bRemainingLinesValid = true;

     double dMaxLength = TJob::MaxCasingLength( m_uom );

     TStringList* pLineStrings = new TStringList();

     for( int iLine = 0; iLine < pFileStrings->Count; iLine++ )
     {
         int iCurrRow = CasingDataGrid->FixedRows + iLine;

         CasingDataGrid->Cells[eCGC_FileData][iCurrRow] = pFileStrings->Strings[iLine];

         if( bHaveFirstValidLine )
             CasingDataGrid->Cells[eCGC_CasingLength][iCurrRow] = BadCasingLength;
         else
             CasingDataGrid->Cells[eCGC_CasingLength][iCurrRow] = "";

         pLineStrings->CommaText = Trim( pFileStrings->Strings[iLine] );

         if( pLineStrings->Count > 0 )
         {
             String sLength = pLineStrings->Strings[0];

             double dLength = -1.0;

             if( TryStrToFloat( sLength, dLength ) )
             {
                 if( ( dLength > 0.0 ) && ( dLength <= dMaxLength ) )
                 {
                     // We have a valid length. Note this.
                     bHaveFirstValidLine = true;

                     // Scale up the length to units of 100's
                     int iLength = (int)( dLength * 100 + 0.5 );

                     m_iTotalLength += iLength;

                     // Lengths are saved in the list in units of 100's of the current UOM
                     String sDisplayLength;
                     sDisplayLength.printf( L"%d.%02d", iLength / 100, iLength % 100 );

                     CasingDataGrid->Cells[eCGC_CasingLength][iCurrRow] = sDisplayLength;

                     m_iCasingCount++;

                     if( m_sCasingsList.Length() > 0 )
                         m_sCasingsList += ",";

                     m_sCasingsList += IntToStr( iLength );

                 }
                 else
                 {
                     // We have a number, but it is bogus. If we already
                     // have had good lines, then the file is bad
                     if( bHaveFirstValidLine )
                         bRemainingLinesValid = false;
                 }
             }
             else
             {
                 // Length text not valid. If we already
                 // have had good lines, then the file is bad
                 if( bHaveFirstValidLine )
                     bRemainingLinesValid = false;
             }
         }
     }

     // All string lines have been loaded. Now show the total length line
     int iTotalRow = CasingDataGrid->RowCount - 1;

     CasingDataGrid->Cells[eCGC_FileData][iTotalRow] = "TOTAL LENGTH";

     if( bHaveFirstValidLine && bRemainingLinesValid )
     {
         String sTotalLength;
         sTotalLength.printf( L"%d.%02d", m_iTotalLength / 100, m_iTotalLength % 100 );

         String sDistText = GetUnitsText( m_uom, MT_DIST1_LONG );

         CasingDataGrid->Cells[eCGC_CasingLength][iTotalRow] = sTotalLength + " " + sDistText;
     }
     else
     {
         CasingDataGrid->Cells[eCGC_CasingLength][iTotalRow] = BadCasingLength;
     }

     // Can get rid of input file now and line parser
     delete pFileStrings;
     delete pLineStrings;

     // Determine outcome
     eLoadCasingResult eReturnVal;

     if( !bHaveFirstValidLine )
     {
         eReturnVal = eLCR_BadContents;
         sErrMsg    = "The selected file has no valid casing lengths in it.";

         m_sCasingsList = "";
         m_iCasingCount = 0;
     }
     else if( !bRemainingLinesValid )
     {
         eReturnVal = eLCR_BadContents;
         sErrMsg    = "One or more lines do not have valid casing lengths.";

         m_sCasingsList = "";
         m_iCasingCount = 0;
     }
     else
     {
         // Input is good in this case
         eReturnVal = eLCR_Good;
         sErrMsg    = "";

         OKBtn->Enabled    = true;
         CloseBtn->Enabled = false;
     }

     return eReturnVal;
}


void __fastcall TLoadCasingsDlg::CasingDataGridDrawCell(TObject *Sender, int ACol,
          int ARow, TRect &Rect, TGridDrawState State)
{
    // Do our own cell drawing here, to prevent the cell with focus from
    // showing up as solid blue
    if( ( ARow == 0 ) || ( ( ACol == 0 ) && ( CasingDataGrid->FixedCols > 0 ) ) )
        CasingDataGrid->Canvas->Brush->Color = clBtnFace;
    else
        CasingDataGrid->Canvas->Brush->Color = clWindow;

    CasingDataGrid->Canvas->FillRect( Rect );

    // If we are on the casing length col and the length is -1, then
    // we have an invalid value
    String sText = CasingDataGrid->Cells[ACol][ARow];

    if( ( ACol == eCGC_CasingLength ) && ( sText == BadCasingLength ) )
    {
        sText = "???";
        CasingDataGrid->Canvas->Font->Color = clRed;
    }
    else
    {
        CasingDataGrid->Canvas->Font->Color = clWindowText;
    }

    if( ARow == CasingDataGrid->RowCount - 1 )
        CasingDataGrid->Canvas->Font->Style = TFontStyles() << fsBold;
    else
        CasingDataGrid->Canvas->Font->Style = TFontStyles();

    CasingDataGrid->Canvas->TextOut( Rect.Left + 2, Rect.Top + 4, sText );
}


void __fastcall TLoadCasingsDlg::HelpBtnClick(TObject *Sender)
{
    ShowModelessForm( CasingHelpDlg );
}


void __fastcall TLoadCasingsDlg::OKBtnClick(TObject *Sender)
{
    String sDistText = GetUnitsText( m_uom, MT_DIST1_LONG );

    String sTotalLength;
    sTotalLength.printf( L"%d.%02d", m_iTotalLength / 100, m_iTotalLength % 100 );

    int confRes = MessageDlg( "You are about to import " + IntToStr( m_iCasingCount ) +
                              " casings with a total length of " + sTotalLength + " " + sDistText + "."
                              "\r  Click Yes to import this data into the job."
                              "\r  Click Cancel to review the data.",
                              mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    if( confRes == mrYes )
        ModalResult = mrOk;
}


void __fastcall TLoadCasingsDlg::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    // Catch the user closing the dialog with the cancel "X"
    if( ModalResult == mrOk )
    {
        // User will have clicked Import. Everything is good. Can close without a message.
        CanClose = true;
    }
    else if( ModalResult == mrClose )
    {
        // If we have a file name, then it there is a problem with its contents.
        // Warn the user that no casings will be loaded.
        if( CasingFileNameEdit->Text.Length() == 0 )
        {
            CanClose = true;
        }
        else
        {
            int confRes = MessageDlg( "You have loaded a casing file but there are errors in it."
                                      "\r  Press Yes if you want to close this dialog without loading any casing data."
                                      "\r  Click Cancel to review the data.",
                                      mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

            if( confRes == mrYes )
                CanClose = true;
            else
                CanClose = false;
        }
    }
    else
    {
        // User will have pressed the "X" button. Cases:
        //   - file name is empty: can close without prompt
        //   - have file name, but not casing list: have bad file loaded, warn user
        //   - have file name and casing list: user has to click Import to use the list
        if( CasingFileNameEdit->Text.Length() == 0 )
        {
            CanClose = true;
        }
        else if( m_sCasingsList.Length() == 0 )
        {
            int confRes = MessageDlg( "You have loaded a casing file but there are errors in it."
                                      "\r  Press Yes if you want to close this dialog without loading any casing data."
                                      "\r  Click Cancel to review the data.",
                                      mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

            if( confRes == mrYes )
                CanClose = true;
            else
                CanClose = false;
        }
        else
        {
            int confRes = MessageDlg( "You have loaded a valid casing file. Are you sure you want to "
                                      "cancel this operation?"
                                      "\r  Press Yes if you want to close this dialog without loading any casing data."
                                      "\r  Click Cancel to review the data.",
                                      mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

            if( confRes == mrYes )
                CanClose = true;
            else
                CanClose = false;
        }
    }
}

