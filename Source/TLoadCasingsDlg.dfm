object LoadCasingsDlg: TLoadCasingsDlg
  Left = 0
  Top = 0
  Caption = 'Load Casings'
  ClientHeight = 365
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  DesignSize = (
    554
    365)
  PixelsPerInch = 96
  TextHeight = 13
  object CloseBtn: TButton
    Left = 465
    Top = 332
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    ModalResult = 8
    TabOrder = 0
  end
  object CasingDataGrid: TStringGrid
    Left = 16
    Top = 72
    Width = 524
    Height = 249
    Anchors = [akLeft, akTop, akRight]
    ColCount = 2
    DefaultColWidth = 128
    DefaultDrawing = False
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing]
    TabOrder = 2
    OnDrawCell = CasingDataGridDrawCell
    ColWidths = (
      256
      128)
  end
  object CasingFileGB: TGroupBox
    Left = 16
    Top = 8
    Width = 524
    Height = 52
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Casing Data File '
    TabOrder = 1
    DesignSize = (
      524
      52)
    object CasingFileNameEdit: TEdit
      Left = 16
      Top = 21
      Width = 369
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
    end
    object LoadCasingFileBtn: TButton
      Left = 393
      Top = 19
      Width = 58
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Select'
      TabOrder = 1
      OnClick = LoadCasingFileBtnClick
    end
    object ClearFileBtn: TButton
      Left = 457
      Top = 19
      Width = 58
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Clear'
      TabOrder = 2
      OnClick = ClearFileBtnClick
    end
  end
  object HelpBtn: TButton
    Left = 16
    Top = 332
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Help'
    TabOrder = 3
    OnClick = HelpBtnClick
  end
  object OKBtn: TButton
    Left = 376
    Top = 332
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Import'
    Default = True
    TabOrder = 4
    OnClick = OKBtnClick
  end
  object FileOpenDlg: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'CSV Files (*.csv)'
        FileMask = '*.csv'
      end
      item
        DisplayName = 'Text Files (*.txt)'
        FileMask = '*.txt'
      end
      item
        DisplayName = 'All Files (*.*)'
        FileMask = '*.*'
      end>
    Options = [fdoPathMustExist, fdoFileMustExist]
    Title = 'Select Casing Data File'
    Left = 296
    Top = 65528
  end
end
