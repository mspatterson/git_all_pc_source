//---------------------------------------------------------------------------
#ifndef TLoadCasingsDlgH
#define TLoadCasingsDlgH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.Grids.hpp>
#include "UnitsOfMeasure.h"
//---------------------------------------------------------------------------
class TLoadCasingsDlg : public TForm
{
__published:	// IDE-managed Components
    TButton *CloseBtn;
    TStringGrid *CasingDataGrid;
    TGroupBox *CasingFileGB;
    TFileOpenDialog *FileOpenDlg;
    TButton *HelpBtn;
    TEdit *CasingFileNameEdit;
    TButton *LoadCasingFileBtn;
    TButton *ClearFileBtn;
    TButton *OKBtn;
    void __fastcall FormShow(TObject *Sender);
    void __fastcall LoadCasingFileBtnClick(TObject *Sender);
    void __fastcall CasingDataGridDrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State);
    void __fastcall HelpBtnClick(TObject *Sender);
    void __fastcall ClearFileBtnClick(TObject *Sender);
    void __fastcall OKBtnClick(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);

private:
    String m_sCasingsList;
    int    m_iTotalLength;  // In units of 100's of current units of measure
    int    m_iCasingCount;

    UNITS_OF_MEASURE m_uom;

    typedef enum
    {
        eLCR_Good,
        eLCR_BadFile,
        eLCR_BadContents
    } eLoadCasingResult;

    eLoadCasingResult LoadCasingsFile( const String& sFileName, String& sErrMsg );

public:
    __fastcall TLoadCasingsDlg(TComponent* Owner);

    static bool ShowDlg( TComponent* Owner, UNITS_OF_MEASURE uom, String& sLoadedFile, String& sCasingList );
       // Casing lengths are returned sCasingList as a string of CSV
       // items. Each item is an integer representing a length in 100's
       // of the current units of measure. This string is only populated
       // if a valid file has been loaded. The file name loaded is returned
       // in sLoadedFile, and is returned whether or not the file name
       // was valid. This method returns true only if a file was loaded
       // that contained at least one casing length.
};
//---------------------------------------------------------------------------
extern PACKAGE TLoadCasingsDlg *LoadCasingsDlg;
//---------------------------------------------------------------------------
#endif
