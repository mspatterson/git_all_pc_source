#include <vcl.h>
#pragma hdrstop

#include "ViewConnsDlg.h"
#include "JobDetailsDlg.h"
#include "Messages.h"
#include "ApplUtils.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TViewConnsForm *ViewConnsForm;


//
// Class Implementation
//

__fastcall TViewConnsForm::TViewConnsForm(TComponent* Owner) : TForm(Owner)
{
    // Init the conns list view sort handling. Assign the column tags
    // first because the TLVConnSorter uses them in its constructor.
    ConnsLV->Columns->Items[0]->Tag = LVC_CONN_NBR;
    ConnsLV->Columns->Items[1]->Tag = LVC_SEQ_NBR;
    ConnsLV->Columns->Items[2]->Tag = LVC_SECT_NBR;
    ConnsLV->Columns->Items[3]->Tag = LVC_LENGTH;
    ConnsLV->Columns->Items[4]->Tag = LVC_RESULT;
    ConnsLV->Columns->Items[5]->Tag = LVC_DATE_TIME;
    ConnsLV->Columns->Items[6]->Tag = LVC_COMMENT;

    m_lvSorter = new TLVConnSorter( ConnsLV );

    // Disable the remove button (It will be re-enabled if a valid connection is selected)
    RemoveConnBtn->Enabled = false;
}


void TViewConnsForm::ShowConnections( TJob* currJob )
{
    m_currJob = currJob;

    RefreshConnectionsList();

    // Reset sort order on show
    m_lvSorter->Ascending  = true;
    m_lvSorter->SortColumn = LVC_CONN_NBR;

    Show();
}


void __fastcall TViewConnsForm::CreateParams(Controls::TCreateParams &Params)
{
    TForm::CreateParams(Params);
    Params.ExStyle   = Params.ExStyle | WS_EX_APPWINDOW;
    Params.WndParent = ParentWindow;
}


void TViewConnsForm::Refresh( void )
{
    if( Visible )
        RefreshConnectionsList();
}


void __fastcall TViewConnsForm::RefreshBtnClick(TObject *Sender)
{
    RefreshConnectionsList();
}


void TViewConnsForm::RefreshConnectionsList( void )
{
    // Note the currently selected item so that we can re-select it
    int iSavedConnID = -1;
    LV_SORTER_REC* pSaved = GetSelItem();

    if( pSaved != NULL )
        iSavedConnID = pSaved->iConnID;

    // Clear current items.
    m_lvSorter->ClearListviewItems();

    // Rebuild the list with the current set of connections
    for( int iConn = 0; iConn < m_currJob->NbrConnRecs; iConn++ )
    {
        CONNECTION_INFO connInfo;
        m_currJob->GetConnection( iConn, connInfo );

        TListItem* newItem = ConnsLV->Items->Add();

        newItem->Caption = IntToStr( connInfo.connNbr );
        newItem->SubItems->Add( IntToStr( connInfo.seqNbr ) );
        newItem->SubItems->Add( IntToStr( connInfo.sectionNbr ) );
        newItem->SubItems->Add( FloatToStrF( connInfo.length, ffFixed, 7, 2 ) );
        newItem->SubItems->Add( ConnResultText[ connInfo.crResult ] );
        newItem->SubItems->Add( connInfo.dateTime.DateTimeString() );
        newItem->SubItems->Add( connInfo.comment );

        // Save the connection in a LV_SORTER_REC. For the connections
        // listview we don't need to have a pipe inv rec reference
        LV_SORTER_REC* pSortRec = new LV_SORTER_REC;

        pSortRec->iCreateNbr  = connInfo.creationNbr;
        pSortRec->iConnNbr    = connInfo.connNbr;
        pSortRec->iSeqNbr     = connInfo.seqNbr;
        pSortRec->iSectNbr    = connInfo.sectionNbr;
        pSortRec->dtConn      = connInfo.dateTime;
        pSortRec->iConnResult = connInfo.crResult;
        pSortRec->fLength     = connInfo.length;
        pSortRec->sComment    = connInfo.comment;
        pSortRec->iConnID     = connInfo.creationNbr;
        pSortRec->iPipeInvID  = 0;   // Pipe inv ID not used in this dialog

        newItem->Data = pSortRec;

        // Try to restore the previous selection
        if( connInfo.creationNbr == iSavedConnID )
        {
            ConnsLV->ItemIndex = iConn;
            newItem->MakeVisible( false );
        }
    }
}


void __fastcall TViewConnsForm::ViewConnBtnClick(TObject *Sender)
{
    // Display the selected connection on the main form
    LV_SORTER_REC* pSortRec = GetSelItem();

    if( pSortRec == NULL )
    {
        Beep();
        return;
    }

    Application->MainForm->Perform( WM_SHOW_CONN, pSortRec->iConnID, 0 );
}


void __fastcall TViewConnsForm::ViewSectBtnClick(TObject *Sender)
{
    // Display the job details dialog (read-only mode)
    LV_SORTER_REC* pSortRec = GetSelItem();

    if( pSortRec == NULL )
    {
        Beep();
        return;
    }

    JobDetailsForm->ViewSection( m_currJob, pSortRec->iSectNbr );
}


LV_SORTER_REC* TViewConnsForm::GetSelItem( void )
{
    if( ConnsLV->ItemIndex < 0 )
        return NULL;

    TListItem* selItem = ConnsLV->Items->Item[ ConnsLV->ItemIndex ];

    LV_SORTER_REC* pSortRec = (LV_SORTER_REC*)( selItem->Data );

    return pSortRec;
}


void __fastcall TViewConnsForm::RemoveConnBtnClick(TObject *Sender)
{
    LV_SORTER_REC* pSortRec = GetSelItem();

    if( pSortRec == NULL )
    {
        Beep();
        return;
    }

    String sRemoveError = "";

    if( !m_currJob->CanRemove( pSortRec->iConnID, sRemoveError ) )
    {
        MessageDlg( sRemoveError, mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    int mrResult = MessageDlg( "Removing a connection is permanent and cannot be undone."
                               "\r  Click Yes to remove this connection."
                               "\r  Press Cancel to leave this connection in the job.",
                               mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    if( mrResult != mrYes )
        return;

    Application->MainForm->Perform( WM_REMOVE_CONN, pSortRec->iConnID, 0 );
}


void __fastcall TViewConnsForm::ConnsLVSelectItem( TObject *Sender, TListItem *Item, bool Selected )
{
    // If the item is not selected, can't do anything
    if( !Selected )
    {
        ViewConnBtn->Enabled   = false;
        ViewSectBtn->Enabled   = false;
        RemoveConnBtn->Enabled = false;

        return;
    }

    // Check that we have a selected item
    LV_SORTER_REC* pSortRec = GetSelItem();

    // Ensure we got a connection
    if( pSortRec == NULL )
    {
        ViewConnBtn->Enabled   = false;
        ViewSectBtn->Enabled   = false;
        RemoveConnBtn->Enabled = false;

        return;
    }

    // Can always view a records section or graph
    ViewConnBtn->Enabled = true;
    ViewSectBtn->Enabled = true;

    // This string shouldn't ever be needed, keep it to be safe
    String sRemoveError = "";

    // If set the ability to remove if the Job allows it
    RemoveConnBtn->Enabled = m_currJob->CanRemove( pSortRec->iConnID, sRemoveError );
}
