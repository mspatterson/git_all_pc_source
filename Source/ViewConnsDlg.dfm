object ViewConnsForm: TViewConnsForm
  Left = 0
  Top = 0
  Caption = 'View Connections'
  ClientHeight = 310
  ClientWidth = 735
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    735
    310)
  PixelsPerInch = 96
  TextHeight = 13
  object ConnsLV: TListView
    Left = 8
    Top = 8
    Width = 718
    Height = 263
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Conn Nbr'
        Width = 70
      end
      item
        Caption = 'Seq Nbr'
        Width = 70
      end
      item
        Caption = 'Section'
        Width = 70
      end
      item
        Caption = 'Length'
      end
      item
        Caption = 'Result'
        Width = 90
      end
      item
        Caption = 'Date / Time'
        Width = 140
      end
      item
        Caption = 'Comment'
        Width = 220
      end>
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    SortType = stData
    TabOrder = 0
    ViewStyle = vsReport
    OnSelectItem = ConnsLVSelectItem
  end
  object RefreshBtn: TButton
    Left = 652
    Top = 277
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Refresh'
    TabOrder = 1
    OnClick = RefreshBtnClick
  end
  object ViewConnBtn: TButton
    Left = 8
    Top = 277
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'View Graph'
    TabOrder = 2
    OnClick = ViewConnBtnClick
  end
  object ViewSectBtn: TButton
    Left = 109
    Top = 277
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'View Section'
    TabOrder = 3
    OnClick = ViewSectBtnClick
  end
  object RemoveConnBtn: TButton
    Left = 210
    Top = 277
    Width = 95
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Remove Conn'
    TabOrder = 4
    OnClick = RemoveConnBtnClick
  end
end
