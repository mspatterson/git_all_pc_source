#ifndef ViewConnsDlgH
#define ViewConnsDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>
#include "JobManager.h"
#include "ConnLVUtils.h"


class TViewConnsForm : public TForm
{
__published:    // IDE-managed Components
    TListView *ConnsLV;
    TButton *RefreshBtn;
    TButton *ViewConnBtn;
    TButton *ViewSectBtn;
    TButton *RemoveConnBtn;
    void __fastcall RefreshBtnClick(TObject *Sender);
    void __fastcall ViewConnBtnClick(TObject *Sender);
    void __fastcall ViewSectBtnClick(TObject *Sender);
    void __fastcall RemoveConnBtnClick(TObject *Sender);
    void __fastcall CreateParams(Controls::TCreateParams &Params);
    void __fastcall ConnsLVSelectItem(TObject *Sender, TListItem *Item, bool Selected);



private:
    TJob* m_currJob;

    TLVConnSorter* m_lvSorter;

    void RefreshConnectionsList( void );

    LV_SORTER_REC* GetSelItem( void );

public:
    __fastcall TViewConnsForm(TComponent* Owner);

    void ShowConnections( TJob* currJob );
    void Refresh( void );
};

extern PACKAGE TViewConnsForm *ViewConnsForm;

#endif
