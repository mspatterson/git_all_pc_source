#include <vcl.h>
#pragma hdrstop

#include <DateUtils.hpp>

#include "ZeroWTTTSDlg.h"
#include "SubDeviceWTTTS.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TZeroWTTTSForm *ZeroWTTTSForm;


//
// Private Declarations
//

static const WORD ZeroStreamInterval = 10;    // msecs

const int MAX_TEMP_COMP = 500;

typedef enum {
    CS_IDLE,
    CS_RUNNING,
    CS_CANCELLING
} CAL_STATES;


__fastcall TZeroWTTTSForm::TZeroWTTTSForm(TComponent* Owner) : TForm(Owner)
{
    m_poller      = NULL;
    m_canStartCal = false;

    m_lastCalPktTime = 0;

    ClearStatusPanels();

    StartZeroBtn->Enabled = false;
}


void TZeroWTTTSForm::ShowDlg( TCommPoller* pPoller )
{
    // Must have a poller to show this dialog
    if( pPoller == NULL )
        return;

    // The temperature must be stable for a zero to be performed.
    // Check for that first.
    TSubDevice::TEMP_COMP_VALUES tempInfo;
    pPoller->GetTempCompInfo( tempInfo );

    if( abs( (int)tempInfo.fTotalTensionOffset ) > MAX_TEMP_COMP )
    {
        int iConfRes = MessageDlg( "Zeroing the TesTORK at this time may not be accurate because its temperature is not stable."
                                   "\r  Press Yes to zero the TesTORK anyway."
                                   "\r  Press Cancel to zero at a later time.", mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

        if( iConfRes != mrYes )
            return;
    }

    // Create the form, set its props, and then show the dialog
    TZeroWTTTSForm* currForm = new TZeroWTTTSForm( NULL );

    currForm->PollObject = pPoller;

    try
    {
        currForm->ShowModal();
    }
    catch( ... )
    {
    }

    delete currForm;
}


void __fastcall TZeroWTTTSForm::FormShow(TObject *Sender)
{
    // Set defaults
    NbrSamplesCombo->ItemIndex = 2;
    m_gyroPrec = 3;

    // Go to streaming mode immediately on showing this form, as
    // we display data in the edits corresponding to the last
    // packet values.
    if( !m_poller->StartDataCollection( ZeroStreamInterval ) )
        MessageDlg( "Data collection could not be started. You will not be able to zero the TesTORK.", mtError, TMsgDlgButtons() << mbOK, 0 );

    // Enable the timer and force a call right away. Note that the
    // state machine won't allow calibration to start if the start
    // data collection call above failed.
    PollTimer->Enabled = true;
    PollTimerTimer( PollTimer );
}


void __fastcall TZeroWTTTSForm::CloseBtnClick(TObject *Sender)
{
    // Can close if we are currently idle
    if( StartZeroBtn->Tag == CS_IDLE )
        ModalResult = mrClose;
    else
        Beep();
}


void __fastcall TZeroWTTTSForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    // Can't close if we are currently zeroing
    if( StartZeroBtn->Tag != CS_IDLE )
    {
        CanClose = false;
        Beep();
    }
}


void __fastcall TZeroWTTTSForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    PollTimer->Enabled = false;

    // Stop streaming
    m_poller->StopDataCollection();
}


void __fastcall TZeroWTTTSForm::PollTimerTimer(TObject *Sender)
{
    // Populate the current values edits if the time of the
    // data has changed.
    DWORD pktTime;
    BYTE  seqNbr;
    TSubDevice::DEVICE_CAL_READING lastCalReading;

    if( m_poller->GetLastDataCal( pktTime, seqNbr, lastCalReading ) )
    {
        // If the packet time has changed, we have new data
        if( pktTime != m_lastCalPktTime )
        {
            m_lastCalPktTime = pktTime;

            Tq045Edit->Text = IntToStr( lastCalReading.torque045 );
            Tq225Edit->Text = IntToStr( lastCalReading.torque225 );

            Tension000Edit->Text = IntToStr( lastCalReading.tension000 );
            Tension090Edit->Text = IntToStr( lastCalReading.tension090 );
            Tension180Edit->Text = IntToStr( lastCalReading.tension180 );
            Tension270Edit->Text = IntToStr( lastCalReading.tension270 );

            GyroEdit->Text = FloatToStrF( (float)lastCalReading.gyro / 1000000.0, ffFixed, 7, m_gyroPrec );

            CurrValuesGB->Caption = " Current Values at " + Now().FormatString( "nn:ss " );
        }
    }

    // Can only start the call process if the device is streaming
    m_canStartCal = m_poller->DeviceIsStreaming;

    // If an item is selected in the zero options gb and we can start
    // the cal, then the start cal button is enabled.
    StartZeroBtn->Enabled = m_canStartCal && HaveItemsSelected();
}


bool TZeroWTTTSForm::HaveItemsSelected( void )
{
    // Return true if any of the checkboxes in the Zero Items group box
    // are checked.
    for( int iCtrl = 0; iCtrl < ItemsToZeroGB->ControlCount; iCtrl++ )
    {
        TCheckBox* currCB = dynamic_cast<TCheckBox*>( ItemsToZeroGB->Controls[iCtrl] );

        if( currCB != NULL )
        {
            if( currCB->Checked )
                return true;
        }
    }

    // Fall through means no checkbox is checked
    return false;
}


void __fastcall TZeroWTTTSForm::ZeroTqCBClick(TObject *Sender)
{
    // If an item is selected in the zero options gb and we can start
    // the cal, then the start cal button is enabled.
    StartZeroBtn->Enabled = m_canStartCal && HaveItemsSelected();
}


void __fastcall TZeroWTTTSForm::StartZeroBtnClick(TObject *Sender)
{
    // Tool is idle and at least one zeroing option must be
    // selected for this event to occur.

    // Check first if we're already running a zero operation
    if( StartZeroBtn->Tag == CS_CANCELLING )
        return;

    if( StartZeroBtn->Tag == CS_RUNNING )
    {
        int confResult = MessageDlg( "Are you sure you want to cancel this zeroing operation?"
                                     "\r  Press Yes to cancel the zeroing."
                                     "\r  Press No to continue with the zeroing.",
                                     mtConfirmation, TMsgDlgButtons() << mbYes << mbNo, 0 );

        if( confResult != mrYes )
            return;

        StartZeroBtn->Caption = "Cancelling";
        StartZeroBtn->Tag = CS_CANCELLING;

        return;
    }

    // Reset progress indications
    ClearStatusPanels();
    SetStatusPanel( SP_SETTLING, ST_UNDERWAY, "" );

    // Based on the selected items, ask the user to confirm the tool is
    // settled in a zero state.
    TStringList* settlePrompts = new TStringList();

    if( ZeroTqCB->Checked )
        settlePrompts->Add( "is under no torque" );

    if( ZeroTensionCB->Checked )
        settlePrompts->Add( "is under no tension" );

    if( ZeroGyroCB->Checked )
        settlePrompts->Add( "is not moving" );

    String settlePrompt = "Please ensure the TesTORK ";

    for( int iPrompt = 0; iPrompt < settlePrompts->Count; iPrompt++ )
    {
        if( iPrompt == 0 )
        {
            // The first prompt always just gets tacked on
            settlePrompt = settlePrompt + settlePrompts->Strings[iPrompt];
        }
        else if( iPrompt == settlePrompts->Count - 1 )
        {
            // There are multiple promts and this is the last one.
            settlePrompt = settlePrompt + ", and " + settlePrompts->Strings[iPrompt];
        }
        else
        {
            // There are multiple promts and this is not the last one.
            settlePrompt = settlePrompt + ", " + settlePrompts->Strings[iPrompt];
        }
    }

    delete settlePrompts;

    settlePrompt = settlePrompt + ".\r  Press OK to continue.\r  Press Cancel to reconsider.";

    int promptResult = MessageDlg( settlePrompt, mtConfirmation, TMsgDlgButtons() << mbOK << mbCancel, 0 );

    if( promptResult != mrOk )
    {
        SetStatusPanel( SP_SETTLING, ST_CANCELLED, "" );
        return;
    }

    // User has confirmed the tool is idle. Okay to start the process
    SetStatusPanel( SP_SETTLING,   ST_DONE,     "" );
    SetStatusPanel( SP_COLLECTING, ST_UNDERWAY, "(waiting for data)" );

    // User can now press the start button to cancel the operation
    CloseBtn->Enabled     = false;
    StartZeroBtn->Caption = "Cancel";
    StartZeroBtn->Tag     = CS_RUNNING;

    // Allocate an array to hold the gyro values in. Over-allocate the
    // array for safety
    int collectionTime = NbrSamplesCombo->Items->Strings[ NbrSamplesCombo->ItemIndex ].ToInt();
    int expectedPkts   = collectionTime * 1000 / ZeroStreamInterval;
    int maxGyroPkts    = expectedPkts + expectedPkts / 4;

    GYRO_READING* pGyroReadings = new GYRO_READING[maxGyroPkts];
    int*          pAvgDeltas    = new int[maxGyroPkts];

    try
    {
        // Initialize recording variables. Note that the gyro readings taken
        // may not equal samples collected because of packet lost over the air.
        int samplesCollected = 0;
        int nbrGyroReadings  = 0;

        DWORD lastPktTime = 0;

        struct {
            __int64 torque045;
            __int64 torque225;
            __int64 tension000;
            __int64 tension090;
            __int64 tension180;
            __int64 tension270;
        } readingAvgs;

        memset( &readingAvgs, 0, sizeof( readingAvgs ) );

        TSubDevice::DEVICE_RAW_READING lastRawPkt;

        // Loop here for the selected amount of time
        TDateTime endTime = IncSecond( Now(), collectionTime );

        while( endTime >= Now() )
        {
            // Check for process being cancelled
            if( StartZeroBtn->Tag != CS_RUNNING )
            {
                SetStatusPanel( SP_COLLECTING, ST_CANCELLED, "" );
                Abort();
            }

            DWORD currPktTime;
            BYTE  currSeqNbr;

            if( m_poller->GetLastDataRaw( currPktTime, currSeqNbr, lastRawPkt ) )
            {
                // Poller reporting a pkt. Check if it is a new one
                if( currPktTime > lastPktTime )
                {
                    // Save gyro info.
                    if( nbrGyroReadings < maxGyroPkts )
                    {
                        pGyroReadings[nbrGyroReadings].pktTime   = currPktTime;
                        pGyroReadings[nbrGyroReadings].seqNbr    = currSeqNbr;
                        pGyroReadings[nbrGyroReadings].gyroValue = lastRawPkt.gyro;

                        nbrGyroReadings++;
                    }

                    // Accumulate this packet's contents in the averaging packet,
                    // and remember its time.
                    lastPktTime = currPktTime;
                    samplesCollected++;

                    UnicodeString statusMsg;
                    statusMsg.printf( L"(%d packets)", samplesCollected );

                    SetStatusPanel( SP_COLLECTING, ST_UNDERWAY, statusMsg );

                    readingAvgs.torque045  += lastRawPkt.torque045;
                    readingAvgs.torque225  += lastRawPkt.torque225;
                    readingAvgs.tension000 += lastRawPkt.tension000;
                    readingAvgs.tension090 += lastRawPkt.tension090;
                    readingAvgs.tension180 += lastRawPkt.tension180;
                    readingAvgs.tension270 += lastRawPkt.tension270;
                }
            }

            // Let the appl process its messages while in this loop
            Application->ProcessMessages();
        }

        // Data collection complete. Confirm we received at least one packet
        if( samplesCollected == 0 )
        {
            MessageDlg( "Zeroing cannot be performed because no data samples were collected from the TesTORK.", mtError, TMsgDlgButtons() << mbOK, 0 );
            SetStatusPanel( SP_COLLECTING, ST_FAILED, "" );

            Abort();
        }
        else if( samplesCollected == 1 )
        {
            if( MessageDlg( "Only one sample was collected from the TesTORK. Do you want to continue with zeroing?"
                            "\r  Press Yes to continue."
                            "\r  Press Cancel to stop the zeroing.",
                            mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 ) != mrYes )
            {
                SetStatusPanel( SP_COLLECTING, ST_CANCELLED, "" );
                Abort();
            }
        }

        UnicodeString pktsRxdText;
        pktsRxdText.printf( L"(%d packets)", samplesCollected );

        SetStatusPanel( SP_COLLECTING, ST_DONE, pktsRxdText );

        // Need the device to be idle for cal data to be uploaded
        if( !m_poller->StopDataCollection() )
        {
            MessageDlg( "Data collection could not be stopped. The zeroing operation cannot be completed.", mtError, TMsgDlgButtons() << mbOK, 0 );
            SetStatusPanel( SP_UPLOADING, ST_FAILED, "" );

            Abort();
        }

        // Wait for the device to go idle
        TDateTime timeoutTime = IncSecond( Now(), 15 );

        while( !m_poller->DeviceIsIdle )
        {
            // Let the appl process its messages while in this loop
            Application->ProcessMessages();

            if( Now() > timeoutTime )
            {
                MessageDlg( "Data collection could not be stopped. The zeroing operation cannot be completed.", mtError, TMsgDlgButtons() << mbOK, 0 );
                SetStatusPanel( SP_UPLOADING, ST_FAILED, "" );

                Abort();
            }

            if( StartZeroBtn->Tag != CS_RUNNING )
            {
                SetStatusPanel( SP_UPLOADING, ST_CANCELLED, "" );
                Abort();
            }
        }

        SetStatusPanel( SP_UPLOADING, ST_UNDERWAY, "" );

        // Average all of the data next.
        readingAvgs.torque045  /= samplesCollected;
        readingAvgs.torque225  /= samplesCollected;
        readingAvgs.tension000 /= samplesCollected;
        readingAvgs.tension090 /= samplesCollected;
        readingAvgs.tension180 /= samplesCollected;
        readingAvgs.tension270 /= samplesCollected;

        // Zero for the gyro is the overall change in gyro over the
        // zeroing period, in units of raw counts per second. Only
        // need to do this if we are zeroing the gyro
        int gyroZero = 0;

        if( ZeroGyroCB->Checked )
        {
            // Make sure we got some readings
            if( nbrGyroReadings == 0 )
            {
                MessageDlg( "No gyro readings were received. Zeroing cannot be completed.", mtError, TMsgDlgButtons() << mbOK, 0 );
                SetStatusPanel( SP_UPLOADING, ST_FAILED, "" );

                Abort();
            }

            // Next determine the gross average delta
            int grossAvg = GetAverageGyroReading( pGyroReadings, nbrGyroReadings );

            // Determine the area under the calibrated curve using the gross average
            int gyroArea = CalcGyroArea( pGyroReadings, nbrGyroReadings, grossAvg );

            // If the area decreases with increasing average, search in that direction
            // for a min.
            if( CalcGyroArea( pGyroReadings, nbrGyroReadings, grossAvg - 1 ) < gyroArea )
            {
                int newArea = CalcGyroArea( pGyroReadings, nbrGyroReadings, grossAvg - 1 );

                while( newArea < gyroArea )
                {
                    grossAvg--;
                    newArea = CalcGyroArea( pGyroReadings, nbrGyroReadings, grossAvg - 1 );
                }
            }
            else
            {
                int newArea = CalcGyroArea( pGyroReadings, nbrGyroReadings, grossAvg + 1 );

                while( newArea < gyroArea )
                {
                    grossAvg++;
                    newArea = CalcGyroArea( pGyroReadings, nbrGyroReadings, grossAvg + 1 );
                }
            }

            // The average has to be adjusted to a value for a 10 msec interval
            // before saving to the device.
            gyroZero = grossAvg / ( ZeroStreamInterval / 10 );
        }

        // We need only pass a list of the cal items that need to be updated to the comms mgr
        TStringList* pCaptions = new TStringList();
        TStringList* pValues   = new TStringList();

        if( ZeroTqCB->Checked )
        {
            pCaptions->Add( TWTTTSSub::CalFactorCaptions[ TWTTTSSub::CI_TORQUE000_OFFSET ] );
            pValues->Add  ( readingAvgs.torque045 );

            pCaptions->Add( TWTTTSSub::CalFactorCaptions[ TWTTTSSub::CI_TORQUE180_OFFSET ] );
            pValues->Add  ( readingAvgs.torque225 );
        }

        if( ZeroTensionCB->Checked )
        {
            pCaptions->Add( TWTTTSSub::CalFactorCaptions[ TWTTTSSub::CI_TENSION000_OFFSET ] );
            pValues->Add  ( readingAvgs.tension000 );

            pCaptions->Add( TWTTTSSub::CalFactorCaptions[ TWTTTSSub::CI_TENSION090_OFFSET ] );
            pValues->Add  ( readingAvgs.tension090 );

            pCaptions->Add( TWTTTSSub::CalFactorCaptions[ TWTTTSSub::CI_TENSION180_OFFSET ] );
            pValues->Add  ( readingAvgs.tension180 );

            pCaptions->Add( TWTTTSSub::CalFactorCaptions[ TWTTTSSub::CI_TENSION270_OFFSET ] );
            pValues->Add  ( readingAvgs.tension270 );
        }

        if( ZeroGyroCB->Checked )
        {
            pCaptions->Add( TWTTTSSub::CalFactorCaptions[ TWTTTSSub::CI_GYRO_OFFSET ] );
            pValues->Add  ( gyroZero );
        }

        if( !m_poller->SetCalDataFormatted( pCaptions, pValues ) )
        {
            MessageDlg( "Could not set calibration data.", mtError, TMsgDlgButtons() << mbOK, 0 );
            SetStatusPanel( SP_UPLOADING, ST_FAILED, "" );

            delete pCaptions;
            delete pValues;

            Abort();
        }

        delete pCaptions;
        delete pValues;

        if( !m_poller->WriteCalDataToDevice() )
        {
            MessageDlg( "Could not write calibration data to the TesTORK.", mtError, TMsgDlgButtons() << mbOK, 0 );
            SetStatusPanel( SP_UPLOADING, ST_FAILED, "" );

            Abort();
        }

        // Now wait up to 30 seconds for the cal to upload. Device will return to idle
        // when upload is done.
        timeoutTime = IncSecond( Now(), 30 );

        int iLastProgress = -1;

        while( !m_poller->DeviceIsIdle )
        {
            // Let the appl process its messages while in this loop
            Application->ProcessMessages();

            // Update progress. Because the polling runs in a separate thread,
            // take a copy of the current value so it doesn't change while we're
            // working with it.
            int iCurrProgress = m_poller->ConfigUploadProgress;

            if( iCurrProgress != iLastProgress )
            {
                String sProgress = "(" + IntToStr( iCurrProgress ) + "%)";

                SetStatusPanel( SP_UPLOADING, ST_UNDERWAY, sProgress );

                iLastProgress = iCurrProgress;
            }

            if( Now() > timeoutTime )
            {
                MessageDlg( "Upload of the new calibration data to the TesTORK failed. Zeroing cannot be completed.", mtError, TMsgDlgButtons() << mbOK, 0 );
                SetStatusPanel( SP_UPLOADING, ST_FAILED, "" );

                Abort();
            }

            if( StartZeroBtn->Tag != CS_RUNNING )
            {
                SetStatusPanel( SP_UPLOADING, ST_CANCELLED, "" );
                Abort();
            }
        }

        SetStatusPanel( SP_UPLOADING, ST_DONE, "" );

        // Finally, restart streaming mode
        m_poller->StartDataCollection( ZeroStreamInterval );

        SetStatusPanel( SP_DONE, ST_DONE, "" );
    }
    catch( ... )
    {
    }

    // Clean-up
    if( pGyroReadings != NULL )
        delete [] pGyroReadings;

    if( pAvgDeltas != NULL )
        delete[] pAvgDeltas;

    // On exit, restore the start button state
    StartZeroBtn->Caption = "Start";
    StartZeroBtn->Tag     = CS_IDLE;
    CloseBtn->Enabled     = true;
}


void TZeroWTTTSForm::ClearStatusPanels( void )
{
    SetStatusPanel( SP_SETTLING,   ST_PENDING, "" );
    SetStatusPanel( SP_COLLECTING, ST_PENDING, "" );
    SetStatusPanel( SP_UPLOADING,  ST_PENDING, "" );
    SetStatusPanel( SP_DONE,       ST_PENDING, "" );
}


void TZeroWTTTSForm::SetStatusPanel( STATUS_PANEL aPanel, STATUS_TEXT itsText, const String& extraInfo )
{
    // Determine the panel being updated
    TPanel* currPanel = NULL;

    switch( aPanel )
    {
       case SP_SETTLING:    currPanel = ToolSettlePanel;      break;
       case SP_COLLECTING:  currPanel = CollectingDataPanel;  break;
       case SP_UPLOADING:   currPanel = UploadCalPanel;       break;
       case SP_DONE:        currPanel = CalDonePanel;         break;
    }

    if( currPanel == NULL )
        return;

    switch( itsText )
    {
        case ST_PENDING:    currPanel->Caption = " Pending";    break;
        case ST_UNDERWAY:   currPanel->Caption = " Underway";   break;
        case ST_DONE:       currPanel->Caption = " Done";       break;
        case ST_CANCELLED:  currPanel->Caption = " Cancelled";  break;
        case ST_FAILED:     currPanel->Caption = " Failed";     break;
        default:            currPanel->Caption = " ???";        break;
    }

    if( extraInfo.Length() > 0 )
        currPanel->Caption = currPanel->Caption + " " + extraInfo;
}


bool TZeroWTTTSForm::CalcReadingToReadingDelta( const GYRO_READING& currReading, const GYRO_READING& prevReading, int& delta )
{
    int currSeqNbr = currReading.seqNbr;
    int lastSeqNbr = prevReading.seqNbr;

    int pktCount;

    if( currSeqNbr >= lastSeqNbr )
        pktCount = currSeqNbr - lastSeqNbr;
    else
        pktCount = 256 - lastSeqNbr + currSeqNbr;

    if( pktCount > 0 )
    {
        // Watch for gyro wrap - ignore the delta in that case
        delta = ( currReading.gyroValue - prevReading.gyroValue ) / pktCount;

        if( abs( delta ) < 0x100000 )
            return true;
    }

    // Fall through means we have an invalid delta here
    return false;
}


int TZeroWTTTSForm::GetAverageGyroReading( const GYRO_READING* pReadings, int nbrReadings )
{
    // Iterate through all readings to determine an average.
    if( nbrReadings < 2 )
        return 0;

    if( pReadings == NULL )
        return 0;

    __int64 totalDelta = 0;

    for( int iReading = 1; iReading < nbrReadings; iReading++ )
    {
        int deltaGyro;

        if( CalcReadingToReadingDelta( pReadings[iReading], pReadings[iReading-1], deltaGyro ) )
            totalDelta += deltaGyro;
    }

    // Note: pReadings[] array contains (nbrReadings-1) difference
    return( (int)( totalDelta / (__int64)(nbrReadings - 1) ) );
}


int TZeroWTTTSForm::CalcGyroArea( const GYRO_READING* pReadings, int nbrReadings, int gyroOffset )
{
    // Iterate through all readings, calculating a calibrated gyro value and
    // then accumulating its difference from zero.
    int calGyro  = 0;
    int gyroArea = 0;

    for( int iReading = 1; iReading < nbrReadings; iReading++ )
    {
        int deltaGyro;

        if( CalcReadingToReadingDelta( pReadings[iReading], pReadings[iReading-1], deltaGyro ) )
        {
            int adjustedGyro = deltaGyro - gyroOffset;

            calGyro += adjustedGyro;

            gyroArea += abs( adjustedGyro );
        }
    }

    return gyroArea;
}


void __fastcall TZeroWTTTSForm::GyroEditDblClick(TObject *Sender)
{
    // Change the default precision for the gyro on double-click
    if( m_gyroPrec == 3 )
        m_gyroPrec = 6;
    else
        m_gyroPrec = 3;
}

