object ZeroWTTTSForm: TZeroWTTTSForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Zero TesTORK Readings'
  ClientHeight = 466
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  DesignSize = (
    371
    466)
  PixelsPerInch = 96
  TextHeight = 13
  object CloseBtn: TButton
    Left = 289
    Top = 435
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Close'
    TabOrder = 0
    OnClick = CloseBtnClick
  end
  object CurrValuesGB: TGroupBox
    Left = 8
    Top = 8
    Width = 356
    Height = 137
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Current Values '
    TabOrder = 1
    object Label2: TLabel
      Left = 11
      Top = 25
      Width = 55
      Height = 13
      Caption = 'Torque 045'
      FocusControl = Tq045Edit
    end
    object Label1: TLabel
      Left = 11
      Top = 52
      Width = 55
      Height = 13
      Caption = 'Torque 225'
      FocusControl = Tq225Edit
    end
    object Label3: TLabel
      Left = 187
      Top = 25
      Width = 58
      Height = 13
      Caption = 'Tension 000'
      FocusControl = Tension000Edit
    end
    object Label4: TLabel
      Left = 187
      Top = 52
      Width = 58
      Height = 13
      Caption = 'Tension 090'
      FocusControl = Tension090Edit
    end
    object Label5: TLabel
      Left = 187
      Top = 79
      Width = 58
      Height = 13
      Caption = 'Tension 180'
      FocusControl = Tension180Edit
    end
    object Label6: TLabel
      Left = 187
      Top = 106
      Width = 58
      Height = 13
      Caption = 'Tension 270'
      FocusControl = Tension270Edit
    end
    object Label7: TLabel
      Left = 11
      Top = 106
      Width = 23
      Height = 13
      Caption = 'Gyro'
      FocusControl = GyroEdit
    end
    object Tq045Edit: TEdit
      Left = 80
      Top = 22
      Width = 81
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
      Text = '0'
    end
    object Tq225Edit: TEdit
      Left = 80
      Top = 49
      Width = 81
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
      Text = '0'
    end
    object Tension000Edit: TEdit
      Left = 256
      Top = 22
      Width = 81
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 2
      Text = '0'
    end
    object Tension090Edit: TEdit
      Left = 256
      Top = 49
      Width = 81
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 3
      Text = '0'
    end
    object Tension180Edit: TEdit
      Left = 256
      Top = 76
      Width = 81
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 4
      Text = '0'
    end
    object Tension270Edit: TEdit
      Left = 256
      Top = 103
      Width = 81
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 5
      Text = '0'
    end
    object GyroEdit: TEdit
      Left = 80
      Top = 103
      Width = 81
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 6
      Text = '0'
      OnDblClick = GyroEditDblClick
    end
  end
  object ItemsToZeroGB: TGroupBox
    Left = 8
    Top = 151
    Width = 356
    Height = 70
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Readings to Zero '
    TabOrder = 2
    object ZeroTqCB: TCheckBox
      Left = 11
      Top = 20
      Width = 97
      Height = 17
      Caption = 'Torque'
      TabOrder = 0
      OnClick = ZeroTqCBClick
    end
    object ZeroTensionCB: TCheckBox
      Left = 141
      Top = 20
      Width = 97
      Height = 17
      Caption = 'Tension'
      TabOrder = 2
      OnClick = ZeroTqCBClick
    end
    object ZeroGyroCB: TCheckBox
      Left = 11
      Top = 43
      Width = 90
      Height = 17
      Caption = 'Gyro'
      TabOrder = 1
    end
  end
  object ExecCB: TGroupBox
    Left = 8
    Top = 225
    Width = 356
    Height = 51
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Execute '
    TabOrder = 3
    DesignSize = (
      356
      51)
    object Label13: TLabel
      Left = 12
      Top = 21
      Width = 98
      Height = 13
      Caption = 'Collect Readings For'
    end
    object Label14: TLabel
      Left = 210
      Top = 21
      Width = 29
      Height = 13
      Caption = '(secs)'
    end
    object NbrSamplesCombo: TComboBox
      Left = 141
      Top = 18
      Width = 60
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 0
      Text = '10'
      Items.Strings = (
        '10'
        '20'
        '30'
        '60')
    end
    object StartZeroBtn: TButton
      Left = 270
      Top = 16
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Start'
      TabOrder = 1
      OnClick = StartZeroBtnClick
    end
  end
  object ProgressGB: TGroupBox
    Left = 8
    Top = 282
    Width = 354
    Height = 145
    Caption = ' Progress'
    Enabled = False
    TabOrder = 4
    object Panel1: TPanel
      Left = 12
      Top = 20
      Width = 185
      Height = 25
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      Caption = ' Waiting for tool to settle'
      TabOrder = 0
    end
    object ToolSettlePanel: TPanel
      Left = 203
      Top = 20
      Width = 140
      Height = 25
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      TabOrder = 1
    end
    object Panel3: TPanel
      Left = 12
      Top = 51
      Width = 185
      Height = 25
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      Caption = ' Collecting data'
      TabOrder = 2
    end
    object CollectingDataPanel: TPanel
      Left = 203
      Top = 51
      Width = 140
      Height = 25
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      TabOrder = 3
    end
    object Panel5: TPanel
      Left = 12
      Top = 82
      Width = 185
      Height = 25
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      Caption = ' Uploading new calibration'
      TabOrder = 4
    end
    object UploadCalPanel: TPanel
      Left = 203
      Top = 82
      Width = 140
      Height = 25
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      TabOrder = 5
    end
    object Panel7: TPanel
      Left = 12
      Top = 113
      Width = 185
      Height = 25
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      Caption = ' Calibration complete'
      TabOrder = 6
    end
    object CalDonePanel: TPanel
      Left = 203
      Top = 113
      Width = 140
      Height = 25
      Alignment = taLeftJustify
      BevelOuter = bvLowered
      TabOrder = 7
    end
  end
  object PollTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = PollTimerTimer
    Left = 12
    Top = 432
  end
end
