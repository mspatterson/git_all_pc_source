#ifndef ZeroWTTTSDlgH
#define ZeroWTTTSDlgH

#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include "CommsMgr.h"

class TZeroWTTTSForm : public TForm
{
__published:    // IDE-managed Components
    TButton *CloseBtn;
    TGroupBox *CurrValuesGB;
    TEdit *Tq045Edit;
    TLabel *Label2;
    TLabel *Label1;
    TEdit *Tq225Edit;
    TEdit *Tension000Edit;
    TLabel *Label3;
    TLabel *Label4;
    TEdit *Tension090Edit;
    TLabel *Label5;
    TEdit *Tension180Edit;
    TLabel *Label6;
    TEdit *Tension270Edit;
    TTimer *PollTimer;
    TGroupBox *ItemsToZeroGB;
    TCheckBox *ZeroTqCB;
    TCheckBox *ZeroTensionCB;
    TGroupBox *ExecCB;
    TComboBox *NbrSamplesCombo;
    TButton *StartZeroBtn;
    TLabel *Label13;
    TGroupBox *ProgressGB;
    TLabel *Label14;
    TCheckBox *ZeroGyroCB;
    TLabel *Label7;
    TEdit *GyroEdit;
    TPanel *Panel1;
    TPanel *ToolSettlePanel;
    TPanel *Panel3;
    TPanel *CollectingDataPanel;
    TPanel *Panel5;
    TPanel *UploadCalPanel;
    TPanel *Panel7;
    TPanel *CalDonePanel;
    void __fastcall PollTimerTimer(TObject *Sender);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall ZeroTqCBClick(TObject *Sender);
    void __fastcall StartZeroBtnClick(TObject *Sender);
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
    void __fastcall GyroEditDblClick(TObject *Sender);
    void __fastcall CloseBtnClick(TObject *Sender);

private:
    TCommPoller* m_poller;
    bool         m_canStartCal;
    DWORD        m_lastCalPktTime;

    int          m_gyroPrec;

    bool HaveItemsSelected( void );

    typedef enum {
       SP_SETTLING,
       SP_COLLECTING,
       SP_UPLOADING,
       SP_DONE
    } STATUS_PANEL;

    typedef enum {
       ST_PENDING,
       ST_UNDERWAY,
       ST_DONE,
       ST_CANCELLED,
       ST_FAILED
    } STATUS_TEXT;

    void ClearStatusPanels( void );
    void SetStatusPanel( STATUS_PANEL aPanel, STATUS_TEXT itsText, const String& extraInfo );

    typedef struct {
        DWORD pktTime;
        BYTE  seqNbr;
        int   gyroValue;
    } GYRO_READING;

    bool CalcReadingToReadingDelta( const GYRO_READING& currReading, const GYRO_READING& prevReading, int& delta );
    int  GetAverageGyroReading( const GYRO_READING* pReadings, int nbrReadings );
    int  CalcGyroArea( const GYRO_READING* pReadings, int nbrReadings, int gyroOffset );

public:
    __fastcall TZeroWTTTSForm(TComponent* Owner);

    __property TCommPoller* PollObject = { read = m_poller, write = m_poller };

    // This is not an auto-created form. Must call the static ShowDlg() method
    // to construct, show, and then destroy the dialog.
    static void ShowDlg( TCommPoller* poller );
};

extern PACKAGE TZeroWTTTSForm *ZeroWTTTSForm;

#endif
