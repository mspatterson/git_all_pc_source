#include <vcl.h>
#pragma hdrstop

#include <FileCtrl.hpp>

#include "main.h"
#include "ApplUtils.h"
#include "JobsDlg.h"
#include "JobDetailsDlg.h"
#include "MainCommentsDlg.h"
#include "ViewConnsDlg.h"
#include "CommsMgr.h"
#include "HelperFunctions.h"
#include "PasswordInputDlg.h"
#include "SystemSettingsDlg.h"
#include "RptConnDlg.h"
#include "RptCommentDlg.h"
#include "RptSectionsDlg.h"
#include "RptSecSummaryDlg.h"
#include "RptStatsDlg.h"
#include "RptCalRecDlg.h"
#include "GraphSettingDlg.h"
#include "ZeroWTTTSDlg.h"
#include "SubDeviceWTTTS.h"    // <--- only required for accessing calibration rec captions
#include "TAlarmDlg.h"
#include "THardwareStatusDlg.h"

#pragma package(smart_init)
#pragma link "AdvSmoothToggleButton"
#pragma link "AdvSmoothProgressBar"
#pragma resource "*.dfm"


TTesTORKMgrMainForm *TesTORKMgrMainForm;


//
// Private Declarations
//

static const WORD CirculateStreamInterval  = 1000;  // msecs
static const WORD ConnectionStreamInterval = 10;    // msecs

static const String PasswordDlgCaption( "Administrator Password Required" );
static const String BadPWEnteredMsg( "You have entered the incorrect password." );

static const TColor clStatusGreen = (TColor)0x0FAF03;


// During real time data collection, a text box is displayed showing te current
// values. The text prefixes for these values are fixed
typedef enum {
    GHS_POINT_NBR,
    GHS_TORQUE,
    GHS_RPM,
    GHS_TIME,
    GHS_TURNS,
    GHS_GYRO,
    NBR_GRAPH_HINT_STRINGS
} GRAPH_HINT_STRING;

static UnicodeString hintPrefixes[NBR_GRAPH_HINT_STRINGS] = {
    L"Points: ",
    L"Torque: ",
    L"RPM: ",
    L"Time: ",
    L"Turns: ",
    L"Gyro: ",
};

// Info panel constants
const int INFO_PANEL_LARGE_WIDTH  = 185;
const int INFO_PANEL_SMALL_WIDTH  = 35;
const int SIDEBAR_SHRINK_DELAY    = 10;

// Graph panel margins, in pixels
const int defaultTopMargin    = 15;
const int defaultLeftMargin   = 15;
const int defaultBottomMargin =  5;
const int defaultRightMargin  = 15;


//
// Class Functions
//

__fastcall TTesTORKMgrMainForm::TTesTORKMgrMainForm(TComponent* Owner) : TForm(Owner)
{
    // COM is used by some modules - init it now
    CoInitialize( NULL );

#ifdef DEVELOPMENT_VER_ONLY
    MessageDlg( "This is a development version of TesTORK Manager and is not to be distributed to general users.", mtWarning, TMsgDlgButtons() << mbOK, 0 );
#endif

    // Set main state
    MainState = MS_BOOTING;

    // Set the Sidebar Options
    InitializeSidebar();

    // Constrain the min size of the main window. The target computers
    // have a min resolution of 1027x768. Query the work area and
    // constrain the min size accordingly.
    RECT rWork = { 0 };

    if( SystemParametersInfo( SPI_GETWORKAREA, 0, &rWork, 0 ) )
    {
        if( rWork.right < 1024 )
            Constraints->MinWidth = rWork.right;
        else
            Constraints->MinWidth = 1024;

        if( rWork.bottom < 768 )
            Constraints->MinHeight = rWork.bottom;
        else
            Constraints->MinHeight = 768;
    }

    // Hook application events
    Application->OnModalBegin = AppModalStart;
    Application->OnModalEnd   = AppModalEnd;

    Application->OnMinimize = OnAppMinimize;
    Application->OnRestore  = OnAppRestore;

    // Set the process priority
    DWORD currPriority = GetPriorityClass( GetCurrentProcess() );
    DWORD reqdPriority = GetProcessPriority();

    if( currPriority != reqdPriority )
    {
        if( !SetPriorityClass( GetCurrentProcess(), reqdPriority ) )
        {
            DWORD dwError = GetLastError();

            MessageDlg( "Could not set process priority, error 0x" + IntToHex( (int)dwError, 8 ), mtError, TMsgDlgButtons() << mbOK, 0 );
        }
    }

    // Create the averaging object
    m_rpmAvg = new TMovingAverage( GetRPMAvgingValue(), 0.0 /*no variance*/ );

    // Reset our last battery level alarm time so that we alarm right away
    m_lastBattCheckTick = 0;
    m_lastBattAlarmTime = 0;

    // The poller object is needed immediately, even if its port has not
    // yet been defined. Construct it now.
    m_devPoller = new TCommPoller( Handle );

    if( !GetAutoAssignCommPorts() )
    {
        // Spin here, waiting for the comm init to complete. This will
        // complete quickly.
        while( m_devPoller->PollerState == TCommPoller::PS_INITIALIZING )
            ;

        switch( m_devPoller->PollerState )
        {
            case TCommPoller::PS_BR_PORT_SCAN:
            case TCommPoller::PS_TT_PORT_SCAN:
            case TCommPoller::PS_TT_CHAN_SCAN:
            case TCommPoller::PS_RUNNING:
                // Normal conditions
                break;

            default:
                // Any other outcome - show the system settings dlg
                PostMessage( Handle, WM_SHOW_SYS_SETTINGS_DLG, 0, 0 );
                break;
        }
    }

    // Check that we have a valid job directory defined.
    if( !DirectoryExists( GetJobDataDir() ) )
    {
        MessageDlg( "You must set the job data directory before proceeding.", mtInformation, TMsgDlgButtons() << mbOK, 0 );

        String newDir;

        if( SelectDirectory( "Select Job Directory", "Desktop", newDir, TSelectDirExtOpts() << sdNewUI << sdNewFolder, NULL ) )
        {
            SetJobDataDir( newDir );
        }
        else
        {
            throw Exception( "Program terminating: job directory is not valid." );
        }
    }

    // Settings are good at this point. Load default units of measure
    m_defaultUOM = (UNITS_OF_MEASURE) GetDefaultUnitsOfMeasure();

    // Next, set the program in use flag so that we can
    // detect if the program crashed on the next launch.
    m_lastShutdownType = GetLastShutdown();
    SetProgramInUse();

    m_currJob = NULL;

    // Load the Tesco image
    m_iconBmp = new Graphics::TBitmap();

    try
    {
        TescoAboutImage->Picture->Bitmap->LoadFromResourceName( (int)HInstance, L"TESCO_LOGO_BMP" );

        m_iconBmp->LoadFromResourceName( (int)HInstance, L"TESCO_LOGO_BMP" );
    }
    catch( ... )
    {
        delete m_iconBmp;
        m_iconBmp = NULL;
    }

    WTTTSVerLabel->Caption = "Version " + GetApplVerAsString();

    // Init animation support
    m_commsStatus.iBattLevelImageIndex = 0;

    CommsProgBar->Position = 0;
    CommsProgBar->Hint     = "";
    CommsProgBar->MarqueeColor = clBtnFace;
    CommsProgBar->Appearance->ValueFormat = " ";
    CommsProgBar->Appearance->ProgressFill->Color   = clBtnFace;
    CommsProgBar->Appearance->ProgressFill->ColorTo = clBtnFace;

    // Load our custom cursors
    Screen->Cursors[CC_HANDGRAB] = LoadCursor( HInstance, L"HANDGRAB_CUR" );
    Screen->Cursors[CC_HANDFLAT] = LoadCursor( HInstance, L"HANDFLAT_CUR" );
    Screen->Cursors[CC_ZOOM]     = LoadCursor( HInstance, L"ZOOM_CUR"     );

    // Init the conns listview sort handling. Assign the column tags
    // first because the TLVConnSorter uses them in its constructor.
    ConnsLV->Columns->Items[0]->Tag = LVC_CONN_NBR;
    ConnsLV->Columns->Items[1]->Tag = LVC_SEQ_NBR;
    ConnsLV->Columns->Items[2]->Tag = LVC_SECT_NBR;
    ConnsLV->Columns->Items[3]->Tag = LVC_RESULT;

    // Init the pipe tally listview.
    PipeTallyLV->Columns->Items[0]->Tag = LVC_CONN_NBR;
    PipeTallyLV->Columns->Items[1]->Tag = LVC_LENGTH;
    PipeTallyLV->Columns->Items[2]->Tag = LVC_SECT_NBR;
    PipeTallyLV->Columns->Items[3]->Tag = LVC_DATE_TIME;

    m_lvSorter    = new TLVConnSorter( ConnsLV );
    m_tallySorter = new TLVConnSorter( PipeTallyLV );

    // Default for connection viewer is descending order
    m_lvSorter->Ascending = false;

    // Hook static event notifiers from the Job class
    TJob::SetOnConnRecsUpdatedHandler ( DoOnConnsUpdated );
    TJob::SetOnPipeTallyUpdatedHandler( DoOnPipeTallyUpdated );

    // Init hardware status LV
    InitCommStatsGrid();

    // Report init
    InitRptSelectionCombo();

    // Init auto-record state
    m_eAutoRecState = eARS_Disabled;
}


void __fastcall TTesTORKMgrMainForm::FormDestroy(TObject *Sender)
{
    // Clean up here
    if( m_currJob != NULL )
        delete m_currJob;

    if( m_iconBmp != NULL )
        delete m_iconBmp;

    if( m_devPoller != NULL )
        delete m_devPoller;

    if( m_rpmAvg != NULL )
        delete m_rpmAvg;

    CoUninitialize();
}


void __fastcall TTesTORKMgrMainForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    switch( MainState )
    {
        case MS_STREAM_CHECK:
        case MS_CIRCULATING:
            MessageDlg( L"You cannot close the program while monitoring data.", mtError, TMsgDlgButtons() << mbOK, 0 );
            CanClose = false;
            break;

        case MS_CONNECTING:
        case MS_RESULT_WAIT:
            MessageDlg( L"You cannot close the program while a connection is in progress.", mtError, TMsgDlgButtons() << mbOK, 0 );
            CanClose = false;
            break;

        default:
            // Okay to close in any other state

            // Check to see if we need to save a backup
            if( m_currJob != NULL )
                m_currJob->SaveBackup();

            CanClose = true;
            break;
    }
}


void __fastcall TTesTORKMgrMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
    // When closing, note we have shut down gracefully
    SetProgramShutdownGood();
}


void __fastcall TTesTORKMgrMainForm::FormShow(TObject *Sender)
{
    // Set the default size of the graphs
    int newSize = GetCtrlSize( CN_MAIN_PG_CTRL );

    if( newSize > 0 )
        NavPanel->Width = newSize;

    newSize = GetCtrlSize( CN_MAIN_GRAPH_HEIGHT );

    if( newSize > 0 )
        TurnsChart->Height = newSize;

    // Set default chart properties. Zoom requires the Ctrl key being pressed
    // at the same time as the left mouse button is down. Panning occurs on
    // the left mouse button down alone (the graph control doesn't support
    // qualification of pan with a key)
    TurnsChart->ScrollMouseButton = mbLeft;
    TimeChart->ScrollMouseButton  = mbLeft;

    TurnsChart->Zoom->Allow       = false;
    TurnsChart->Zoom->MouseButton = mbLeft;
    TurnsChart->Zoom->Pen->Color  = clBlack;

    TimeChart->Zoom->Allow       = false;
    TimeChart->Zoom->MouseButton = mbLeft;
    TimeChart->Zoom->Pen->Color  = clBlack;

    TurnsChart->MarginTop    = defaultTopMargin;
    TurnsChart->MarginLeft   = defaultLeftMargin;
    TurnsChart->MarginBottom = defaultBottomMargin;
    TurnsChart->MarginRight  = defaultRightMargin;

    TimeChart->MarginTop    = defaultTopMargin;
    TimeChart->MarginLeft   = defaultLeftMargin;
    TimeChart->MarginBottom = defaultBottomMargin;
    TimeChart->MarginRight  = defaultRightMargin;

    SetupGraphColours();

    // Position the system status panel based on whether there is a logo or not
    if( m_iconBmp != NULL )
    {
        SysStatusPanel->Left = m_iconBmp->Width + 90;
        SysStatusPanel->Top  = TorqueBtnPanel->Top;
    }
    else
    {
        SysStatusPanel->Left = TurnsChart->Width - ( TorqueBtnPanel->Left + TorqueBtnPanel->Width );
        SysStatusPanel->Top  = TorqueBtnPanel->Top;
    }

    // For some reason, the Comms progress bar seems to snap its width
    // back to a default width. Make sure it is the correct width.
    const int CommsProgBarMargin = 9;

    CommsProgBar->Left  = CommsProgBarMargin;
    CommsProgBar->Width = SysStatusPaintBox->Left - CommsProgBar->Left - CommsProgBarMargin;

    // Set AdvSmoothBtn options
    SetSmoothBtnProps();

    // All forms will by now have been created. Do any init required
    // by them now
    TConnectionForm::CONN_DLG_CALLBACKS connDlgCallbacks;

    connDlgCallbacks.connDlgEvent  = ConnectionDlgEvent;
    connDlgCallbacks.mainStatusReq = ConnectionDlgStatusReq;

    ConnectionForm->SetEventCallbacks( connDlgCallbacks );

    CirculatingDlg->SetEventCallbacks( CircDlgEvent );

    AlarmDlg->SetAckEventMessage( Handle, WM_SHOW_ALARM_ACK );

    // Handle last shutdown processing here. If last shutdown was
    // good then call OpenJob( L�� ), otherwise get name of the last
    // job that was open and call OpenJob() with that.
    if( m_lastShutdownType == SDT_GOOD )
        OpenJob( L"" );
    else
        OpenJob( GetMostRecentJob() );

    // Okay to start our timers now
    SysTimer->Enabled  = true;
    PollTimer->Enabled = true;
}


void __fastcall TTesTORKMgrMainForm::FormResize(TObject *Sender)
{
    // If the Nav Panel is floating, be sure to re-align it
    if( NavPanelState != eNPS_Pinned )
    {
        NavPanel->Height = ClientHeight - StatusBar->Height;

        // If the Nav Panel is visible or hidden, just be sure
        // to reset its Left prop. Otherwise, if its in the
        // process of showing or hiding, force it back to
        // being visible
        if( NavPanelState == eNPS_Visible )
            NavPanel->Left = ClientWidth - NavPanel->Width;
        else if( NavPanelState == eNPS_Hidden )
            NavPanel->Left = PinnedNavSpacerPanel->Left;
        else if( NavPanelState == eNPS_Hiding )
            ExpandSidebar();
    }

    StatusBar->Panels->Items[0]->Width = StatusBar->Width - 180;

    // PDS status only visible if the feature is enabled
    PDSPipeGB->Visible = GetPDSMonEnabled();
}


void __fastcall TTesTORKMgrMainForm::SysTimerTimer(TObject *Sender)
{
    // Update status bar and comm stats
    RefreshStatus();

    // Update the Nav Panel slider
    UpdateNavPanelSlider();

    // Update the interlock status
    switch( m_devPoller->InterlockState )
    {
        case TCommPoller::IS_NO_HARDWARE:
            CDSPanel->Caption   = "N/A";
            SlipsPanel->Caption = "N/A";
            CDSPanel->Color     = clBtnFace;
            SlipsPanel->Color   = clBtnFace;
            break;

        case TCommPoller::IS_DISABLED:
            CDSPanel->Caption   = "Disabled";
            SlipsPanel->Caption = "Disabled";
            CDSPanel->Color     = clYellow;
            SlipsPanel->Color   = clYellow;
            break;

        default:
            // In any remaining states, get the state of each interlock
            // from the last radio packet
            {
                bool bSlipsLocked;
                bool bCDSLocked;

                m_devPoller->GetInterlockState( bSlipsLocked, bCDSLocked );

                if( bSlipsLocked && bCDSLocked )
                {
                    CDSPanel->Caption   = "Locked";
                    SlipsPanel->Caption = "Locked";
                    CDSPanel->Color     = clRed;
                    SlipsPanel->Color   = clRed;
                }
                else if( bSlipsLocked )
                {
                    CDSPanel->Caption   = "Enabled";
                    SlipsPanel->Caption = "Locked";
                    CDSPanel->Color     = clLime;
                    SlipsPanel->Color   = clRed;
                }
                else if( bCDSLocked )
                {
                    CDSPanel->Caption   = "Locked";
                    SlipsPanel->Caption = "Released";
                    CDSPanel->Color     = clRed;
                    SlipsPanel->Color   = clLime;
                }
                else
                {
                    // This is an invalid state!
                    CDSPanel->Caption   = "Enabled";
                    SlipsPanel->Caption = "Released";
                    CDSPanel->Color     = clYellow;
                    SlipsPanel->Color   = clYellow;
                }
            }
            break;
    }

    // Update the interlock status
    switch( m_devPoller->PDSPipeState )
    {
        case TCommPoller::PDS_OPEN:
            PDSStatePanel->Caption = "Open";
            PDSStatePanel->Color   = clYellow;
            break;

        case TCommPoller::PDS_CLOSED:
            PDSStatePanel->Caption = "Closed";
            PDSStatePanel->Color   = clGreen;
            break;

        default:
            PDSStatePanel->Caption = "N/A";
            PDSStatePanel->Color   = clBtnFace;
            break;
    }

    // Update auto-record status display
    switch( m_eAutoRecState )
    {
        case eARS_Ready:
            AutoRecStatusPanel->Caption = "Ready";
            AutoRecStatusPanel->Color   = clGreen;
            break;

        case eARS_Recording:
            AutoRecStatusPanel->Caption = "Recording";
            AutoRecStatusPanel->Color   = clGreen;
            break;

        case eARS_Paused:
            AutoRecStatusPanel->Caption = "Paused";
            AutoRecStatusPanel->Color   = clYellow;
            break;

        case eARS_Manual:
            AutoRecStatusPanel->Caption = "Manual";
            AutoRecStatusPanel->Color   = clYellow;
            break;

        default:
            AutoRecStatusPanel->Caption = "Disabled";
            AutoRecStatusPanel->Color   = clBtnFace;
            break;
    }
}


void __fastcall TTesTORKMgrMainForm::OpenJobBtnClick(TObject *Sender)
{
    OpenJob( L"" /* no job name suggested */ );
}


void TTesTORKMgrMainForm::OpenJob( String suggestedJobName )
{
    // Release any current job, then switch to the loading state
    ConnectionForm->CurrentJob = NULL;

    if( m_currJob != NULL )
    {
        delete m_currJob;
        m_currJob = NULL;
    }

    MainState = MS_LOADING;

    // Try to open a job now
    UNITS_OF_MEASURE jobUOM = m_defaultUOM;

    String   jobToOpen = suggestedJobName;
    JOB_INFO jobInfo;

    // If the job name is valid, open the job. If the job name is blank or
    // invalid, show the job dialog to allow the user to select a job.
    if( GetJobInfo( jobToOpen, jobInfo ) )
        m_currJob = new TJob( jobToOpen );
    else if( SelJobsForm->OpenJob( jobToOpen, GetMostRecentJob() ) )
        m_currJob = new TJob( jobToOpen );

    ConnectionForm->CurrentJob = m_currJob;

    if( m_currJob != NULL )
    {
        AddToMostRecentJobsList( jobToOpen );

        // Refresh views
        ValidateNextSection();
        RefreshConnectionsList();
        PrepareForConnection();

        // Override default units of measure with job units
        jobUOM = m_currJob->Units;

        // When opening a job, must re-enable the interlock function
        if( GetInterlockEn() == false )
        {
            SaveInterlockEn( true );
            m_devPoller->RefreshSettings( jobUOM );
        }

        // Enter sim mode, if simulating
        if( m_devPoller->UsingTesTORKSim )
        {
            MessageDlg( "You are currently in Simulation Mode. Any changes made to this job will not be saved.",
                        mtWarning, TMsgDlgButtons() << mbOK, 0 );

            EnterSimMode( true );
        }
    }
    else
    {
        // Clear pipe tally totals
        NbrPassedEdit->Caption = L"";
        TotLengthEdit->Caption = L"";

        m_lvSorter->ClearListviewItems();
        m_tallySorter->ClearListviewItems();

        // Clear the left panel values
        DisplayConnection( -1 );
    }

    // Okay to go to idle now
    MainState = MS_IDLE;

    // Update auto-record state
    UpdateAutoRecordState();

    // Update torque captions on both graphs
    String torqueCaption = "Torque (" + GetUnitsText( jobUOM, MT_TORQUE_SHORT ) + ")";

    TurnsChart->LeftAxis->Title->Caption = torqueCaption;
    TimeChart->LeftAxis->Title->Caption  = torqueCaption;

    RefreshCaption();

    // Refresh report selections
    RptTypeComboClick( RptTypeCombo );

    // Clear graphs
    ResetGraphs();

    // Select the most recent connection if any exist
    if( ConnsLV->Items->Count > 0 )
        ConnsLV->ItemIndex = 0;

    NavPgCtrl->ActivePage = ConnsSheet;
}


void __fastcall TTesTORKMgrMainForm::FinishJobItemClick(TObject *Sender)
{
    // Sets the current job's status to completed
    if( m_currJob == NULL )
        return;

    if( m_currJob->Completed )
        return;

    int mrResult = MessageDlg( "You are about to mark this job as completed. You will not be able to make any more changes to the job file."
                               "\r  Click Yes to complete this job.\r  Click Cancel to leave this job open.",
                               mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    if( mrResult != mrYes )
        return;

    // The sys admin password is required to perform this task
    String newPassword;

    if( !TPasswordInputForm::CheckPassword( PasswordDlgCaption, GetSysAdminPassword(), newPassword ) )
        return;

    // If newPassword is not empty, it was changed by the user
    if( newPassword.Length() > 0 )
        SaveSysAdminPassword( newPassword );

    m_currJob->AddMainComment( "Job completed" );
    m_currJob->Completed = true;

    // Force a change to the job status to be sure all controls are properly updated
    MainState = MS_LOADING;
    MainState = MS_IDLE;

    RefreshCaption();
}


void __fastcall TTesTORKMgrMainForm::ReopenJobItemClick(TObject *Sender)
{
    // This handler changes the state of a completed job to
    ReopenJob( true );
}


void TTesTORKMgrMainForm::ReopenJob( bool needPassword )
{
    // Sets the current job's status to in-progress (or open)
    if( m_currJob == NULL )
        return;

    if( m_currJob->Completed == false )
        return;

    int mrResult = MessageDlg( "This job has been completed. If you re-open it, changes will be allowed to the data."
                               "\r  Click Yes to proceed and re-open this job.\r  Click Cancel to leave this job closed.",
                               mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    if( mrResult != mrYes )
        return;

    if( needPassword )
    {
        String newPassword;

        if( !TPasswordInputForm::CheckPassword( PasswordDlgCaption, GetSysAdminPassword(), newPassword ) )
            return;

        // If newPassword is not empty, it was changed by the user
        if( newPassword.Length() > 0 )
            SaveSysAdminPassword( newPassword );
    }

    m_currJob->Completed = false;
    m_currJob->AddMainComment( "Job re-opened" );

    // Force a change to the job status to be sure all controls are properly updated
    MainState = MS_LOADING;
    MainState = MS_IDLE;

    RefreshCaption();
}


void TTesTORKMgrMainForm::RefreshCaption( void )
{
    UnicodeString mainCaption = "TESCO TesTORK Wireless Torque Turn Tension System";

    if( m_currJob != NULL )
    {
        mainCaption = mainCaption + " - " + m_currJob->Client + ", " + m_currJob->Location;

        if( m_currJob->Completed )
            mainCaption = mainCaption + " (completed)";
    }

    Caption = mainCaption;
}


void __fastcall TTesTORKMgrMainForm::CommentBtnClick(TObject *Sender)
{
    // This event only fires if m_currJob is not NULL.
    MainCommentForm->ShowMainComments( m_currJob );
}


void __fastcall TTesTORKMgrMainForm::ConnCtrlBtnClick(TObject *Sender)
{
    ConnectionForm->ShowForm();
}


void __fastcall TTesTORKMgrMainForm::CircCtrlBtnClick(TObject *Sender)
{
    CirculatingDlg->ShowForm();
}


void __fastcall TTesTORKMgrMainForm::ShowConnsBtnClick(TObject *Sender)
{
    ViewConnsForm->ShowConnections( m_currJob );
}


void __fastcall TTesTORKMgrMainForm::NewSectBtnClick(TObject *Sender)
{
    // We are adding a new section. Confirm last connection passed.
    int  lastConnNbr = 0;
    int  lastSeqNbr  = 0;
    bool lastConnPassed = true;

    for( int iConn = 0; iConn < m_currJob->NbrConnRecs; iConn++ )
    {
        CONNECTION_INFO connInfo;

        if( m_currJob->GetConnection( iConn, connInfo ) )
        {
            if( connInfo.connNbr > lastConnNbr )
            {
                lastConnNbr    = connInfo.connNbr;
                lastSeqNbr     = connInfo.seqNbr;
                lastConnPassed = ( ( connInfo.crResult == CR_PASSED ) || ( connInfo.crResult == CR_FORCE_PASSED ) );
            }
            else if( connInfo.connNbr == lastConnNbr )
            {
                if( connInfo.seqNbr > lastSeqNbr )
                {
                    lastSeqNbr     = connInfo.seqNbr;
                    lastConnPassed = ( ( connInfo.crResult == CR_PASSED ) || ( connInfo.crResult == CR_FORCE_PASSED ) );
                }
            }
        }
    }

    if( !lastConnPassed )
    {
        int mrResult = MessageDlg( "The last connection was not successful. Are you sure you want to start a new section?"
                                   "\r  Press Yes to start a new section."
                                   "\r  Press No to reconsider.",
                                   mtConfirmation, TMsgDlgButtons() << mbYes << mbNo, 0 );

        if( mrResult != mrYes )
            return;
    }

    // Show job details dlg
    // If the user exits/cancels, avoid any extra work
    if( !JobDetailsForm->AddSection( m_currJob ) )
        return;

    // Refresh display with the section info
    PrepareForConnection();

    // Allow editing of the new section
    SetSectionButton( EditSectBtn );

    // Force a redraw on the chart
    TurnsChart->Invalidate();
    TimeChart->Invalidate();
}


void __fastcall TTesTORKMgrMainForm::EditSectBtnClick(TObject *Sender)
{
    // The current section is in a state where it can be edited.
    // Show the dialog
    JobDetailsForm->EditSection( m_currJob, m_currSection );

    bool canEditSect = false;

    if( !m_currJob->Completed )
    {
        SECTION_INFO currSection;

        if( m_currJob->GetSectionRec( m_currJob->CurrSectIndex, currSection ) )
        {
            if( !m_currJob->SectionHasConns[ currSection.sectionNbr ] )
                canEditSect = true;
        }
    }

    // Reset the button to the correct functionality
    if( canEditSect )
        SetSectionButton( EditSectBtn );
    else
        SetSectionButton( NewSectBtn );

    // Refresh display with the section info
    PrepareForConnection();

    // Force a redraw on the chart
    TurnsChart->Invalidate();
    TimeChart->Invalidate();
}


void TTesTORKMgrMainForm::DisplaySection( SECTION_INFO* pSection )
{
    if( pSection == NULL )
    {
        // If no section info is passed, clear the section-related displays
        SectNbrEdit->Caption = L"";

        // Populate the fixed-value boxes in the status boxes
        TqMaxEdit->Caption = L"";
        TqOptEdit->Caption = L"";
        TqMinEdit->Caption = L"";

        ShldrMaxEdit->Caption = L"";
        ShldrMinEdit->Caption = L"";

        TurnsMaxEdit->Caption = L"";
        TurnsMinEdit->Caption = L"";
    }
    else
    {
        // Have a section here. If we have a section, we must have a job as well.
        bool canEditSect = false;

        if( !m_currJob->Completed )
        {
            SECTION_INFO currSection;

            if( m_currJob->GetSectionRec( m_currJob->CurrSectIndex, currSection ) )
            {
                if( !m_currJob->SectionHasConns[ currSection.sectionNbr ] )
                    canEditSect = true;
            }
        }

        EditSectBtn->Enabled = canEditSect;

        SectNbrEdit->Caption = IntToStr( pSection->sectionNbr );

        // Populate the fixed-value boxes in the status boxes
        TqMaxEdit->Caption = FloatToStrF( pSection->peakTargetMaxTorque, ffFixed, 7, 0 );
        TqOptEdit->Caption = FloatToStrF( pSection->peakTargetOptTorque, ffFixed, 7, 0 );
        TqMinEdit->Caption = FloatToStrF( pSection->peakTargetMinTorque, ffFixed, 7, 0 );

        if( pSection->shoulderMaxTorque == 0.0 )
            ShldrMaxEdit->Caption = L"";
        else
            ShldrMaxEdit->Caption = FloatToStrF( pSection->shoulderMaxTorque, ffFixed, 7, 0 );

        if( pSection->shoulderMinTorque == 0.0 )
            ShldrMinEdit->Caption = L"";
        else
            ShldrMinEdit->Caption = FloatToStrF( pSection->shoulderMinTorque, ffFixed, 7, 0 );

        TurnsMaxEdit->Caption = TurnsToText( pSection->peakTargetTurns + pSection->peakTargetDelta );
        TurnsMinEdit->Caption = TurnsToText( pSection->peakTargetTurns - pSection->peakTargetDelta );
    }
}


void TTesTORKMgrMainForm::ValidateNextSection( void )
{
    // When connections are removed, it is possible to remove through sections.
    // In that case, as connections are re-done check if we should be asking the
    // user to advence the section number.
    //
    // Caller must be sure to refresh the current section after calling this
    // function, in case the current section changes

    // If the current section index is the last section record, then no further
    // check is required.
    if( m_currJob == NULL )
        return;

    if( m_currJob->NbrConnRecs <= 1 )
        return;

    if( m_currJob->CurrSectIndex == m_currJob->NbrSections - 1 )
        return;

    CONNECTION_INFO currConn;
    m_currJob->GetConnection( m_currJob->NbrConnRecs - 1, currConn );

    // Only need to verify if the current connection is good
    if( !ConnGood( &currConn ) )
        return;

    // Check for the next higher connection number. If it exists, and its
    // section number differs from the current section, ask the user what
    // to do.
    CONNECTION_INFO nextConn;
    bool haveNextConn = false;

    for( int iConn = 0; iConn < m_currJob->NbrConnRecs; iConn++ )
    {
        CONNECTION_INFO connInfo;
        m_currJob->GetConnection( iConn, connInfo );

        if( connInfo.connNbr == currConn.connNbr + 1 )
        {
            // Found a 'next' connection. Note this but keep looking as
            // there could be more 'next' connection recs.
            nextConn = connInfo;
            haveNextConn = true;
        }
    }

    if( !haveNextConn )
        return;

    // If the next connection's section number is one more than the current
    // section number (which is the current index + 1), then ask the user what
    // to do.
    int currSection = m_currJob->CurrSectIndex + 1;

    if( nextConn.sectionNbr == currSection + 1 )
    {
        int mrResult = MessageDlg( "Connection " + IntToStr( nextConn.connNbr ) + " was previously "
                                   "made in section " + IntToStr( nextConn.sectionNbr ) + ", but you "
                                   "are currently in section " + IntToStr( currSection ) + "."
                                   "\r  Press Yes if you would like to switch to section " +
                                   IntToStr( nextConn.sectionNbr ) + "."
                                   "\r  Press No to continue using section " + IntToStr( currSection ) + "."
                                   "\r\rNote that you can create a new section for this connection by clicking No, "
                                   "and then clicking on the New Section button.",
                                   mtConfirmation, TMsgDlgButtons() << mbYes << mbNo, 0 );

        if( mrResult == mrYes )
            m_currJob->SyncToSection( nextConn.sectionNbr );
    }
}


void TTesTORKMgrMainForm::RefreshStatus( void )
{
    StatusBar->Panels->Items[1]->Text = Now().DateTimeString();

    UpdateCommStatus();

    // Display status info based on state
    switch( MainState )
    {
        case MS_SETTINGS_ONLY:
            StatusBar->Panels->Items[0]->Text = L" No job open";
            break;

        case MS_IDLE:
            if( m_currJob == NULL )
                StatusBar->Panels->Items[0]->Text = L" Error: in MS_IDLE with no job";
            else if( ConnsLV->ItemIndex >= 0 )
                StatusBar->Panels->Items[0]->Text = L" Displaying connection " + ConnsLV->Items->Item[ConnsLV->ItemIndex]->Caption;
            else if( m_currJob->Completed )
                StatusBar->Panels->Items[0]->Text = L" View-only mode";
            else
                StatusBar->Panels->Items[0]->Text = L" Ready for connection";
            break;

        case MS_STREAM_CHECK:
        case MS_CIRCULATING:
            StatusBar->Panels->Items[0]->Text = L" Monitoring real time data";
            break;

        case MS_CONNECTING:
            if( m_nbrConnPts < MAX_NBR_CONN_PTS )
                StatusBar->Panels->Items[0]->Text = L" Monitoring connection data, " + IntToStr( m_nbrConnPts ) + L" points displayed";
            else
                StatusBar->Panels->Items[0]->Text = L" Connection data collection stopped - maximum points collected";
            break;

        case MS_RESULT_WAIT:
            if( m_shldrSet )
                StatusBar->Panels->Items[0]->Text = L" Waiting on connection result entry";
            else
                StatusBar->Panels->Items[0]->Text = L" Waiting for shoulder to be set";
            break;
    }
}


void TTesTORKMgrMainForm::EnterSimMode( bool bIsSimulating )
{
    // If we want to do any extra handling/captions, do it here
    if( m_currJob != NULL )
    {
        // Can only put a job into sim mode; can't clear that flag
        if( bIsSimulating )
            m_currJob->InSimMode = true;
    }
}


void __fastcall TTesTORKMgrMainForm::OnAppMinimize( TObject *Sender )
{
    if( ConnectionForm->Visible )
         ConnectionForm->Hide();

    if( CirculatingDlg->Visible )
         CirculatingDlg->Hide();
}


void __fastcall TTesTORKMgrMainForm::OnAppRestore( TObject *Sender )
{
    if( ConnectionForm->IsActive )
        ConnectionForm->Show();

    if( CirculatingDlg->IsActive )
         CirculatingDlg->Show();
}


void TTesTORKMgrMainForm::SetSmoothBtnProps( void )
{
    // Set the colours and other props of any Adv Smooth btns
    for( int iCtrl = 0; iCtrl < HWSheet->ControlCount; iCtrl++ )
    {
        TAdvSmoothToggleButton* pBtn = dynamic_cast<TAdvSmoothToggleButton*>( HWSheet->Controls[iCtrl] );

        SetSmoothBtnProps( pBtn );
    }

    for( int iCtrl = 0; iCtrl < ControlGB->ControlCount; iCtrl++ )
    {
        TAdvSmoothToggleButton* pBtn = dynamic_cast<TAdvSmoothToggleButton*>( ControlGB->Controls[iCtrl] );

        SetSmoothBtnProps( pBtn );
    }

    for( int iCtrl = 0; iCtrl < ConnsSheet->ControlCount; iCtrl++ )
    {
        TAdvSmoothToggleButton* pBtn = dynamic_cast<TAdvSmoothToggleButton*>( ConnsSheet->Controls[iCtrl] );

        SetSmoothBtnProps( pBtn );
    }

    for( int iCtrl = 0; iCtrl < PipeTallySheet->ControlCount; iCtrl++ )
    {
        TAdvSmoothToggleButton* pBtn = dynamic_cast<TAdvSmoothToggleButton*>( PipeTallySheet->Controls[iCtrl] );

        SetSmoothBtnProps( pBtn );
    }

    for( int iCtrl = 0; iCtrl < RptGB->ControlCount; iCtrl++ )
    {
        TAdvSmoothToggleButton* pBtn = dynamic_cast<TAdvSmoothToggleButton*>( RptGB->Controls[iCtrl] );

        SetSmoothBtnProps( pBtn );
    }
}


void TTesTORKMgrMainForm::SetSmoothBtnProps( TAdvSmoothToggleButton* pBtn )
{
    if( pBtn != NULL )
    {
        pBtn->BorderColor      = clTESCOBlue;
        pBtn->BorderInnerColor = clTESCOBlue;
        pBtn->ColorDisabled    = clBtnDisabled;
    }
}


void __fastcall TTesTORKMgrMainForm::AppModalStart( TObject* Sender )
{
    // Event handler called when a modal form is being displayed.
    UpdateAutoRecordState();
}


void __fastcall TTesTORKMgrMainForm::AppModalEnd( TObject* Sender )
{
    // Event handler called when a modal form is closed.
    UpdateAutoRecordState();
}


void __fastcall TTesTORKMgrMainForm::WMShowConn( TMessage &Message )
{
    // Message sent by child forms to force the display of a connection.
    // Find the connection in the list view, select that item, then
    // call the OnSelect handler. WParam of message holds the creation
    // nbr of the CONNECTION_INFO struct.

    // Scan for the same item in the ConnsLV. If found, select it and
    // invoke the click handler.
    for( int iItem = 0; iItem < ConnsLV->Items->Count; iItem++ )
    {
        LV_SORTER_REC* lvInfo = (LV_SORTER_REC*)( ConnsLV->Items->Item[iItem]->Data );

        if( lvInfo->iCreateNbr == Message.WParam )
        {
            ConnsLV->ItemIndex = iItem;

            ConnsLVSelectItem( ConnsLV, ConnsLV->Items->Item[iItem], true );

            Message.Result = 1;

            return;
        }
    }

    // Fall through means item not found
    Message.Result = 0;
}


void __fastcall TTesTORKMgrMainForm::WMCopyConnToClip( TMessage &Message )
{
    // Message sent by child forms to force the display of a connection
    // and then copying of the graph to the clipboard as a WMF image.

    // First, clear the clipboard
    EmptyClipboard();

    // Now display of the connection. Message.Result will tell us if
    // that was success.
    WMShowConn( Message );

    if( Message.Result == 1 )
    {
        TurnsChart->BufferedDisplay = false;
        TurnsChart->CopyToClipboardMetafile( true );
        TurnsChart->BufferedDisplay = true;
    }
}


void __fastcall TTesTORKMgrMainForm::WMRemoveConn( TMessage &Message )
{
    if( ( m_currJob == NULL ) || ( m_currJob->Completed ) )
    {
        // Should never happen
        Beep();
        return;
    }

    int iConnID = (int)Message.WParam;

    CONNECTION_INFO connInfo;

    if( !m_currJob->GetConnectionByID( iConnID, connInfo ) )
    {
        // Should never happen
        Beep();
        return;
    }

    String comment;

    if( !InputQuery( "Remove Connection " + IntToStr( connInfo.connNbr ), "Reason: ", comment ) )
        return;

    if( Trim( comment ).Length() == 0 )
    {
        MessageDlg( "You must provide a comment when removing a connection.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    // Determine if the user would like to add this connection back into inventory
    int iConfRes = MessageDlg( "Will this casing length be re-used?"
                               "\r  Press Yes to keep this length in the Pipe Tally."
                               "\r  Press No to remove it from the Pipe Tally.", mtConfirmation, TMsgDlgButtons() << mbYes << mbNo, 0 );

    bool bAddToPipeTally = ( iConfRes == mrYes );

    if( !m_currJob->RemoveConnection( iConnID, comment, bAddToPipeTally ) )
    {
        MessageDlg( "This connection cannot be removed.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    PrepareForConnection();
}


void __fastcall TTesTORKMgrMainForm::WMCompleteJob( TMessage &Message )
{
    FinishJobItemClick( FinishJobItem );
}


void __fastcall TTesTORKMgrMainForm::WMReopenJob( TMessage &Message )
{
    ReopenJob( false );
}


void __fastcall TTesTORKMgrMainForm::WMShowSysSettingsDlg( TMessage &Message )
{
    // This event is only called from the constructor when there is an error
    // with the comm settings.
    MessageDlg( "The device port could not be opened (" + m_devPoller->ConnectResult + "). You will have to modify the port settings before continuing.",
                mtError, TMsgDlgButtons() << mbOK, 0 );

    SysSettingsBtnClick( SysSettingsBtn );
}


void __fastcall TTesTORKMgrMainForm::WMShowAlarmEvent( TMessage &Message )
{
    // Determine if this event is an alarmable event
    TAlarmDlg::EventType eType = (TAlarmDlg::EventType)Message.WParam;

    bool bIsAlarm = true;

    switch( eType )
    {
        case TAlarmDlg::eET_Test:           bIsAlarm = true;  break;
        case TAlarmDlg::eET_LowBattery:     bIsAlarm = true;  break;
        case TAlarmDlg::eET_BadConnLength:  bIsAlarm = true;  break;
        case TAlarmDlg::eET_NoShoulder:     bIsAlarm = true;  break;
        case TAlarmDlg::eET_ConnFailed:     bIsAlarm = true;  break;
        case TAlarmDlg::eET_NoInventory:    bIsAlarm = true;  break;
        case TAlarmDlg::eET_CrossThread:    bIsAlarm = true;  break;
        case TAlarmDlg::eET_BaseRadioLost:  bIsAlarm = true;  break;
        case TAlarmDlg::eET_TesTORKLost:    bIsAlarm = true;  break;
        default:                            bIsAlarm = false; break;
    }

    // Log the alarm if we have an open job
    if( ( m_currJob != NULL ) && !m_currJob->Completed )
        m_currJob->AddMainComment( "Alarm reported: " + AlarmDlg->EventShortDesc( eType ) );

    // Ring the alarm output if required
    if( ( m_devPoller != NULL ) && bIsAlarm )
        m_devPoller->SetAlarmOutput( 0xFFFFFFFF );

    // This will cause the dialog to go modal
    AlarmDlg->DisplayEvent( eType, bIsAlarm );
}


void __fastcall TTesTORKMgrMainForm::WMAlarmAck( TMessage &Message )
{
    TAlarmDlg::EventType eType = (TAlarmDlg::EventType)Message.WParam;

    // Write an entry to the job log
    if( ( m_currJob != NULL ) && !m_currJob->Completed )
        m_currJob->AddMainComment( "Alarm acknowledged: " + AlarmDlg->EventShortDesc( eType ) );

    // Clear the alarm output
    if( m_devPoller != NULL )
        m_devPoller->SetAlarmOutput( 0 );
}


void __fastcall TTesTORKMgrMainForm::WMCommsDebugMsg( TMessage &Message )
{
    #ifdef SHOW_COMMS_DEBUG_MSGS
        NbrPassedEdit->Caption = IntToHex( (int)Message.WParam, 8 );
    #endif
}


void __fastcall TTesTORKMgrMainForm::WMPollerEvent( TMessage &Message )
{
    // Comms Mgr posts this message when an event has occurred. WParam
    // will indicate the event type.
    if( Message.WParam == TCommPoller::ePE_CommsLost )
    {
        if( m_devPoller->PollerState == TCommPoller::PS_BR_PORT_SCAN )
        {
            :: PostMessage( Handle, WM_SHOW_ALARM_EVENT, TAlarmDlg::eET_BaseRadioLost, 0 );
        }
        else if( ( m_devPoller->PollerState == TCommPoller::PS_TT_PORT_SCAN ) || ( m_devPoller->PollerState == TCommPoller::PS_TT_CHAN_SCAN ) )
        {
            :: PostMessage( Handle, WM_SHOW_ALARM_EVENT, TAlarmDlg::eET_TesTORKLost, 0 );
        }

        // Need to pass this information along to the circ and conn dlgs
        if( MainState != MS_BOOTING )
        {
            ConnectionForm->CanStartConn = false;
            CirculatingDlg->CanStartCirc = false;

            if( MainState == MS_CONNECTING )
            {
                // If we are connecting and comms have been lost, must stop the
                // connection immediately.
                ConnectionForm->ForceStop();
            }
        }
    }
    else if( Message.WParam == TCommPoller::ePE_CommsUp )
    {
        // Need to pass this information along to the circ and conn dlgs
        if( MainState != MS_BOOTING )
        {
            ConnectionForm->CanStartConn = ( MainState == MS_IDLE );
            CirculatingDlg->CanStartCirc = ( MainState == MS_IDLE );
        }
    }
    else if( Message.WParam == TCommPoller::ePE_AlarmChange )
    {
        // Nothing need be done here
    }
}


void __fastcall TTesTORKMgrMainForm::PollTimerTimer(TObject *Sender )
{
    bool readingsAdded = false;

    String logFileName;
    bool loggingData = GetDataLogging( DLT_CHART_DATA, logFileName );

    // Loop here while reading samples from the comms manager. Note that the
    // RPM reported here is one calculated by the TT object, and it may not
    // match exactly the RPM calculated by main and shown on the graphs due
    // to averaging. However, because RPM data is not stored in the job file
    // main needs to be able to re-calc RPM when displaying historical records.
    // For now, we only use the RPM reported by the comms manager for controlling
    // the auto-record capabilities.
    WTTTS_READING nextReading;
    int rpm;

    while( m_devPoller->GetNextSample( nextReading, rpm ) )
    {
        // Give the sample to the connection dialog in case we're in auto-record mode
        bool bDiscardIfConnecting = ConnectionForm->ProcessSample( nextReading, rpm );
        bool bDiscardIfCircing    = CirculatingDlg->ProcessSample( nextReading, rpm );

        // Only process the points in a 'live' mode
        if( MainState == MS_STREAM_CHECK )
            readingsAdded |= AddPointToCharts( nextReading );
        else if( ( MainState == MS_CONNECTING ) && !bDiscardIfConnecting )
            readingsAdded |= AddPointToCharts( nextReading );
        else if( ( MainState == MS_CIRCULATING ) && !bDiscardIfCircing )
            readingsAdded |= AddPointToCharts( nextReading );

        // Log the sample in any mode
        if( loggingData )
            LogCalRecord( nextReading, logFileName );
    }

    if( readingsAdded )
    {
        TurnsChart->Invalidate();
        TimeChart->Invalidate();

        UpdateConnStats();
    }
}


void __fastcall TTesTORKMgrMainForm::SysStatusPaintBoxPaint(TObject *Sender)
{
    BatteryLevelImageList->Draw( SysStatusPaintBox->Canvas, 5, 0, m_commsStatus.iBattLevelImageIndex );
}


void __fastcall TTesTORKMgrMainForm::TurnsChartAfterDraw(TObject *Sender)
{
    // Draw the torque boxes. Only do so if a job is open
    if( m_currJob != NULL )
    {
        // Set the clipping region first
        HRGN graphRgn = ::CreateRectRgn( TurnsChart->ChartRect.Left,
                                         TurnsChart->ChartRect.Top,
                                         TurnsChart->ChartRect.Right,
                                         TurnsChart->ChartRect.Bottom );

        ::SelectClipRgn( TurnsChart->Canvas->Handle, graphRgn );

        int maxTorqueY = TurnsChart->LeftAxis->CalcYPosValue( m_currSection.peakTargetMaxTorque );
        int minTorqueY = TurnsChart->LeftAxis->CalcYPosValue( m_currSection.peakTargetMinTorque );
        int optTorqueY = TurnsChart->LeftAxis->CalcYPosValue( m_currSection.peakTargetOptTorque );

        // Draw horizontal reference lines first
        DASHED_LINE_PARAMS lineParams;
        lineParams.color  = clRed;
        lineParams.width  = 2;
        lineParams.length = 8;

        DrawSimpleHzDashedLine( TurnsChart->Canvas, lineParams, 0, TurnsChart->Width, maxTorqueY );
        DrawSimpleHzDashedLine( TurnsChart->Canvas, lineParams, 0, TurnsChart->Width, minTorqueY );

        if( GetShowShldrCursors() )
        {
            if( m_currSection.shoulderMaxTorque > 0.0 )
            {
                int shldrTq = TurnsChart->LeftAxis->CalcYPosValue( m_currSection.shoulderMaxTorque );

                DrawSimpleHzDashedLine( TurnsChart->Canvas, lineParams, 0, TurnsChart->Width, shldrTq );
            }

            if( m_currSection.shoulderMinTorque > 0.0 )
            {
                int shldrTq = TurnsChart->LeftAxis->CalcYPosValue( m_currSection.shoulderMinTorque );

                DrawSimpleHzDashedLine( TurnsChart->Canvas, lineParams, 0, TurnsChart->Width, shldrTq );
            }
        }

        lineParams.color = TorqueByTurnsSeries->Color;

        DrawSimpleHzDashedLine( TurnsChart->Canvas, lineParams, 0, TurnsChart->Width, optTorqueY );

        // Draw the torque target box next
        TurnsChart->Canvas->Pen->Color = clRed;
        TurnsChart->Canvas->Pen->Width = 2;
        TurnsChart->Canvas->Pen->Style = psSolid;

        TPoint rectPts[4];

        rectPts[0].X = TurnsChart->BottomAxis->CalcXPosValue( m_currSection.peakTargetTurns - m_currSection.peakTargetDelta );
        rectPts[0].Y = maxTorqueY;

        rectPts[1].X = TurnsChart->BottomAxis->CalcXPosValue( m_currSection.peakTargetTurns + m_currSection.peakTargetDelta );
        rectPts[1].Y = rectPts[0].Y;

        rectPts[2].X = rectPts[1].X;
        rectPts[2].Y = minTorqueY;

        rectPts[3].X = rectPts[0].X;
        rectPts[3].Y = rectPts[2].Y;

        TurnsChart->Canvas->MoveTo( rectPts[0].X, rectPts[0].Y );
        TurnsChart->Canvas->LineTo( rectPts[1].X, rectPts[1].Y );
        TurnsChart->Canvas->LineTo( rectPts[2].X, rectPts[2].Y );
        TurnsChart->Canvas->LineTo( rectPts[3].X, rectPts[3].Y );
        TurnsChart->Canvas->LineTo( rectPts[0].X, rectPts[0].Y );

        if( m_shldrSet )
        {
            // Draw shoulder pt
            TurnsChart->Canvas->Pen->Color = clBlue;
            TurnsChart->Canvas->Pen->Width = 2;
            TurnsChart->Canvas->Pen->Style = psSolid;

            // Find the point nearest to the mouse down point
            const int boxSize = GetShoulderBoxSize();

            TPoint rectPts[4];

            rectPts[0].X = TurnsChart->BottomAxis->CalcXPosValue( m_shldrPt ) - boxSize / 2;
            rectPts[0].Y = TurnsChart->LeftAxis->CalcYPosValue  ( m_shldrTq ) - boxSize / 2;

            rectPts[1].X = rectPts[0].X + boxSize;
            rectPts[1].Y = rectPts[0].Y;

            rectPts[2].X = rectPts[1].X;
            rectPts[2].Y = rectPts[0].Y + boxSize;

            rectPts[3].X = rectPts[0].X;
            rectPts[3].Y = rectPts[2].Y;

            TurnsChart->Canvas->MoveTo( rectPts[0].X, rectPts[0].Y );
            TurnsChart->Canvas->LineTo( rectPts[1].X, rectPts[1].Y );
            TurnsChart->Canvas->LineTo( rectPts[2].X, rectPts[2].Y );
            TurnsChart->Canvas->LineTo( rectPts[3].X, rectPts[3].Y );
            TurnsChart->Canvas->LineTo( rectPts[0].X, rectPts[0].Y );
        }

        // Release the region
        ::SelectClipRgn( TurnsChart->Canvas->Handle, NULL );
        ::DeleteObject( graphRgn );
    }

    // Draw the Tesco logo, upper left hand corner
    int itemXPos = TurnsChart->BottomAxis->CalcXPosValue( TurnsChart->BottomAxis->Minimum ) + 2 /*border width*/;
    int itemYPos = TurnsChart->LeftAxis->CalcYPosValue  ( TurnsChart->LeftAxis->Maximum   ) + 2 /*border width*/;

    if( m_iconBmp != NULL )
    {
        BitBlt( TurnsChart->Canvas->Handle, itemXPos, itemYPos, m_iconBmp->Width, m_iconBmp->Height,
                m_iconBmp->Canvas->Handle, 0, 0, SRCCOPY );

        // Bump the top margin down now for the next item to be displayed
        itemYPos += m_iconBmp->Height + 20;
    }
    else
    {
        itemYPos = SysStatusPanel->Top + SysStatusPanel->Height + 20;
    }

    // If we are in a live mode, then draw a current values box
    if( ( MainState == MS_CONNECTING ) || ( MainState == MS_CIRCULATING ) || ( MainState == MS_STREAM_CHECK ) )
    {
        TurnsChart->Canvas->Pen->Color = clWhite;
        TurnsChart->Canvas->Pen->Width = 1;
        TurnsChart->Canvas->Pen->Style = psClear;

        TurnsChart->Canvas->Brush->Color = clWebPaleGoldenrod;
        TurnsChart->Canvas->Brush->Style = bsSolid;

        TurnsChart->Canvas->Font->Color  = clBlack;
        TurnsChart->Canvas->Font->Style  = TFontStyles();
        TurnsChart->Canvas->Font->Size   = 8;
        TurnsChart->Canvas->Font->Name   = L"Arial";

        // Determine the hint box dimensions. We could find the longest
        // hint string, but instead we know it will be the torque line
        // so just size the box using it.
        const int boxMargin  = 5;
        const int textHeight = 12 * TurnsChart->Canvas->TextHeight( String( L"X" ) ) / 10;

        int valueIndent = TurnsChart->Canvas->TextWidth( hintPrefixes[GHS_TORQUE] );
        int valueWidth  = TurnsChart->Canvas->TextWidth( String( L"XXXXXXX" ) );

        int prefixLeft = itemXPos + boxMargin;
        int valueLeft  = itemXPos + boxMargin + valueIndent;

        int boxWidth  = valueIndent + valueWidth + 2 * boxMargin;
        int boxHeight = textHeight * NBR_GRAPH_HINT_STRINGS + 2 * boxMargin;

        // Calc overall height of text box
        TRect hintRect( itemXPos, itemYPos, itemXPos + boxWidth, itemYPos + boxHeight );
        TurnsChart->Canvas->Rectangle( hintRect );

        // Now draw all the strings. If we don't have a point yet, still draw the
        // box but we show that the values are missing.
        int textTop = itemYPos + boxMargin;

        for( int iStr = 0; iStr < NBR_GRAPH_HINT_STRINGS; iStr++ )
        {
            String valueText;

            // We always plot the number of points received regardless
            // of whether we have a packet
            if( iStr == GHS_POINT_NBR )
            {
                valueText = IntToStr( TorqueByTurnsSeries->Count() );
            }
            else if( m_lastWTTTSPkt.havePoint )
            {
                switch( iStr )
                {
                    case GHS_TORQUE:     valueText = FloatToStrF( m_lastWTTTSPkt.torqueValue, ffFixed, 7, 0 );    break;
                    case GHS_RPM:        valueText = FloatToStrF( m_lastWTTTSPkt.rpmValue,    ffFixed, 7, 0 );    break;
                    case GHS_TIME:       valueText = FloatToStrF( m_lastWTTTSPkt.timeValue,   ffFixed, 7, 1 );    break;
                    case GHS_TURNS:      valueText = FloatToStrF( m_lastWTTTSPkt.turnValue,   ffFixed, 7, 3 );    break;
                    case GHS_GYRO:       valueText = FloatToStrF( m_lastWTTTSPkt.gyroValue,   ffFixed, 7, 3 );    break;
                    default:             valueText = L"???";                                                      break;
                }
            }
            else
            {
                // In this case, we're not plotting the point count and
                // we've not yet received a packet
                valueText = "---";
            }

            TurnsChart->Canvas->TextOut( prefixLeft, textTop, hintPrefixes[ iStr ] );
            TurnsChart->Canvas->TextOut( valueLeft,  textTop, valueText );

            textTop += textHeight;
        }
    }
}


void __fastcall TTesTORKMgrMainForm::ChartMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift, int X, int Y)
{
    // We only operate on left mouse button actions
    if( Button != mbLeft )
        return;

    // On mouse down, if in shoulder mode the user will be setting a shoulder point.
    if( ConnectionForm->InShoulderMode )
    {
        if( Sender == TurnsChart )
        {
            // Record the shoulder point. Determining the turns at the mouse
            // down point is easy. However, there may not be a data value at
            // the mouse point, so need to extrapolate its value. User must
            // click along the curve either on a point or between two points.
            float turnsPt  = TorqueByTurnsSeries->XScreenToValue( X );
            float torquePt = 0.0;

            // First, check if we've landed on a point
            int ptIndex = TorqueByTurnsSeries->XValues->Locate( turnsPt );

            if( ptIndex >= 0 )
            {
                torquePt = TorqueByTurnsSeries->YValue[ptIndex];
            }
            else
            {
                // Must extrapolate here
                for( int iPt = 0; iPt < TorqueByTurnsSeries->XValues->Count - 1; iPt++ )
                {
                    if( ( TorqueByTurnsSeries->XValue[iPt] <= turnsPt ) && ( TorqueByTurnsSeries->XValue[iPt+1] > turnsPt ) )
                    {
                        float deltaY = TorqueByTurnsSeries->YValue[iPt+1] - TorqueByTurnsSeries->YValue[iPt];
                        float deltaX = TorqueByTurnsSeries->XValue[iPt+1] - TorqueByTurnsSeries->XValue[iPt];

                        if( deltaX == 0.0 )
                        {
                            torquePt = ( TorqueByTurnsSeries->YValue[iPt] + TorqueByTurnsSeries->YValue[iPt+1] ) / 2.0;
                        }
                        else
                        {
                            float portion = deltaY / deltaX * ( turnsPt - TorqueByTurnsSeries->XValue[iPt] );

                            torquePt = TorqueByTurnsSeries->YValue[iPt] + portion;
                        }

                        break;
                    }
                }
            }

            if( torquePt > 0.0 )
            {
                m_shldrPt  = turnsPt;
                m_shldrTq  = torquePt;

                m_shldrSet = true;

                TurnsChart->Invalidate();

                UpdateConnStats();
                SuggestConnectionResult();

                ConnectionForm->ShoulderSet();
            }
        }

        return;
    }

    // Fall through to here means the left mouse button is down, but user is
    // not setting the shoulder. Show pan / zoom cursor, if required.
    TChart* aChart = dynamic_cast<TChart*>( Sender );

    if( aChart != NULL )
    {
        bool panning   = false;
        bool zoomingIn = false;

        if( aChart == TurnsChart )
        {
            panning   = TqGraphPanBtn->Down;
            zoomingIn = TqGraphZmInBtn->Down;
        }
        else
        {
            panning   = TimeGraphPanBtn->Down;
            zoomingIn = TimeGraphZmInBtn->Down;
        }

        TCursor newCursor = crDefault;

        if( panning )
            newCursor = CC_HANDGRAB;
        else if( zoomingIn )
            newCursor = CC_ZOOM;
        else
            newCursor = crDefault;

        if( aChart->Cursor != newCursor )
            aChart->Cursor = newCursor;

        aChart->OriginalCursor = aChart->Cursor;
    }
}


void __fastcall TTesTORKMgrMainForm::ChartMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
    // In shoulder mode, show the hand point cursor
    if( ConnectionForm->InShoulderMode )
    {
        if( Sender == TurnsChart )
        {
            TurnsChart->Cursor         = crHandPoint;
            TurnsChart->OriginalCursor = crHandPoint;
        }
        else if( Sender == TimeChart )
        {
            TimeChart->Cursor         = crDefault;
            TimeChart->OriginalCursor = crDefault;
        }

        return;
    }

    TChart* aChart = dynamic_cast<TChart*>( Sender );

    if( aChart != NULL )
    {
        // Left mouse button is down. Cursor depends on the mode we are in. You
        // can zoom while in pan mode, so zoom is priority.
        bool panning   = false;
        bool zoomingIn = false;

        if( aChart == TurnsChart )
        {
            panning   = TqGraphPanBtn->Down;
            zoomingIn = TqGraphZmInBtn->Down;
        }
        else
        {
            panning   = TimeGraphPanBtn->Down;
            zoomingIn = TimeGraphZmInBtn->Down;
        }

        TCursor newCursor = crDefault;

        if( panning )
            newCursor = CC_HANDGRAB;
        else if( zoomingIn )
            newCursor = CC_ZOOM;
        else
            newCursor = crDefault;

        if( aChart->Cursor != newCursor )
            aChart->Cursor = newCursor;

        aChart->OriginalCursor = aChart->Cursor;
    }
}


void TTesTORKMgrMainForm::SetMainState( MAIN_STATE newState )
{
    // Set button states based on the main state. Generally we only do work
    // on a change of state. However, if the new state is MS_LOADING or
    // MS_BOOTING, then force the code to run.
    if( ( m_mainState != newState ) || ( newState == MS_LOADING ) || ( newState == MS_BOOTING ) )
    {
        // Check first if we have an open job, whether it can be changed.
        bool canChangeJob = false;

        if( m_currJob != NULL )
            canChangeJob = !m_currJob->Completed;

        // Determine if we can edit a section
        bool canEditSect = false;

        if( canChangeJob )
        {
            SECTION_INFO currSection;

            if( m_currJob->GetSectionRec( m_currJob->CurrSectIndex, currSection ) )
            {
                if( !m_currJob->SectionHasConns[ currSection.sectionNbr ] )
                    canEditSect = true;
            }
        }

        // If we can edit the section set our button to that, otherwise new
        if( canEditSect )
            SetSectionButton( EditSectBtn );
        else
            SetSectionButton( NewSectBtn );

        // Check for going to idle. If we don't have a job, force that to
        // settings only.
        if( ( newState == MS_IDLE ) && ( m_currJob == NULL ) )
            newState = MS_SETTINGS_ONLY;

        // Update our state so any of the logic that follows acts on the new state
        m_mainState = newState;

        // Now enable controls based on new state.
        switch( newState )
        {
            case MS_BOOTING:
            case MS_LOADING:
                // Can't do anything while app is booting or project is loading
                OpenJobItem->Enabled      = false;
                FinishJobItem->Enabled    = false;
                ReopenJobItem->Enabled    = false;

                CircCtrlBtn->Enabled      = false;
                ConnCtrlBtn->Enabled      = false;
                CommentBtn->Enabled       = false;

                ConnsLV->Enabled          = false;
                ShowConnsBtn->Enabled     = false;
                NewSectBtn->Enabled       = false;
                EditSectBtn->Enabled      = false;
                NewEditSectBtn->Enabled   = false;

                RptPreviewBtn->Enabled    = false;
                SysSettingsBtn->Enabled   = false;
                ILockBypassBtn->Enabled   = false;
                StreamChkBtn->Enabled     = false;
                ZeroReadingsBtn->Enabled  = false;

                GraphSettingBtn->Enabled  = false;

                EnablePipeTallyBtns( true /*force disable*/ );

                break;

            case MS_SETTINGS_ONLY:
                // When in settings mode, cannot do any connection work
                OpenJobItem->Enabled      = true;
                FinishJobItem->Enabled    = false;
                ReopenJobItem->Enabled    = false;

                CircCtrlBtn->Enabled      = true;
                ConnCtrlBtn->Enabled      = false;
                CommentBtn->Enabled       = false;

                ConnsLV->Enabled          = false;
                ShowConnsBtn->Enabled     = false;
                NewSectBtn->Enabled       = false;
                EditSectBtn->Enabled      = false;
                NewEditSectBtn->Enabled   = false;

                RptPreviewBtn->Enabled    = false;
                SysSettingsBtn->Enabled   = true;
                ILockBypassBtn->Enabled   = true;
                StreamChkBtn->Enabled     = true;
                ZeroReadingsBtn->Enabled  = true;

                GraphSettingBtn->Enabled  = true;

                EnablePipeTallyBtns( true /*force disable*/ );

                break;

            case MS_IDLE:
                // When idle, users can browse, do reports, change settings, and start connections
                OpenJobItem->Enabled     = true;
                FinishJobItem->Enabled   = ( m_currJob != NULL ) && ( m_currJob->Completed == false );
                ReopenJobItem->Enabled   = ( m_currJob != NULL ) && ( m_currJob->Completed == true );

                CircCtrlBtn->Enabled     = true;
                ConnCtrlBtn->Enabled     = canChangeJob;
                CommentBtn->Enabled      = canChangeJob;

                ConnsLV->Enabled         = true;
                ShowConnsBtn->Enabled    = true;
                NewSectBtn->Enabled      = canChangeJob;
                EditSectBtn->Enabled     = canEditSect;
                NewEditSectBtn->Enabled  = ( canChangeJob || canEditSect );

                RptPreviewBtn->Enabled   = true;
                SysSettingsBtn->Enabled  = true;
                ILockBypassBtn->Enabled  = true;
                StreamChkBtn->Enabled    = true;
                ZeroReadingsBtn->Enabled = true;

                GraphSettingBtn->Enabled = true;

                EnablePipeTallyBtns( false /*enable based on tally lv state*/ );

                break;

            case MS_STREAM_CHECK:
            case MS_CIRCULATING:
                // When monitoring, can't do anything with the software except
                // stop the monitor
                OpenJobItem->Enabled     = false;
                FinishJobItem->Enabled   = false;
                ReopenJobItem->Enabled   = false;

                CircCtrlBtn->Enabled     = ( newState == MS_CIRCULATING );
                ConnCtrlBtn->Enabled     = false;
                CommentBtn->Enabled      = canChangeJob;

                ConnsLV->Enabled         = false;
                ShowConnsBtn->Enabled    = false;
                NewSectBtn->Enabled      = false;
                EditSectBtn->Enabled     = false;
                NewEditSectBtn->Enabled  = false;

                RptPreviewBtn->Enabled   = false;
                SysSettingsBtn->Enabled  = false;
                ILockBypassBtn->Enabled  = true;
                StreamChkBtn->Enabled    = ( newState == MS_STREAM_CHECK );
                ZeroReadingsBtn->Enabled = false;

                GraphSettingBtn->Enabled = false;

                EnablePipeTallyBtns( false /*enable based on tally lv state*/ );

                break;

            case MS_CONNECTING:
                // When in a connection, can only stop the connection
                // Also allow the Conn dialog to get shown, in case it
                // somehow got closed
                OpenJobItem->Enabled     = false;
                FinishJobItem->Enabled   = false;
                ReopenJobItem->Enabled   = false;

                CircCtrlBtn->Enabled     = false;
                ConnCtrlBtn->Enabled     = true;
                CommentBtn->Enabled      = false;

                ConnsLV->Enabled         = false;
                ShowConnsBtn->Enabled    = false;
                NewSectBtn->Enabled      = false;
                EditSectBtn->Enabled     = false;
                NewEditSectBtn->Enabled  = false;

                RptPreviewBtn->Enabled   = false;
                SysSettingsBtn->Enabled  = false;
                ILockBypassBtn->Enabled  = true;
                StreamChkBtn->Enabled    = false;
                ZeroReadingsBtn->Enabled = false;

                GraphSettingBtn->Enabled = false;

                EnablePipeTallyBtns( true /*cannot edit pipe tally while connecting*/ );

                break;

            case MS_RESULT_WAIT:
                // When waiting for a result, can only save
                OpenJobItem->Enabled     = false;
                FinishJobItem->Enabled   = false;
                ReopenJobItem->Enabled   = false;

                CircCtrlBtn->Enabled     = false;
                ConnCtrlBtn->Enabled     = true;
                CommentBtn->Enabled      = false;

                ConnsLV->Enabled         = false;
                ShowConnsBtn->Enabled    = false;
                NewSectBtn->Enabled      = false;
                EditSectBtn->Enabled     = false;
                NewEditSectBtn->Enabled  = false;

                RptPreviewBtn->Enabled   = false;
                SysSettingsBtn->Enabled  = false;
                ILockBypassBtn->Enabled  = true;
                StreamChkBtn->Enabled    = false;
                ZeroReadingsBtn->Enabled = false;

                GraphSettingBtn->Enabled = false;

                EnablePipeTallyBtns( true /*cannot edit pipe tally while connecting*/ );

                break;
        }

        // The Start/Stop toggle shortcut is enabled if either the Conn or Circ
        // buttons are enabled
        StartStopToggleItem->Enabled = ( CircCtrlBtn->Enabled || ConnCtrlBtn->Enabled );

        // Update auto-record state based on new system state
        UpdateAutoRecordState();

        // Lastly, let the connection and circ dialogs know if they can start.
        // Do this only if we are not in the booting state
        if( m_mainState != MS_BOOTING )
        {
            // The PollerState is volatile, so capture its state once so that
            // both forms get the same value
            bool bReceiving  = ( m_devPoller->PollerState == TCommPoller::PS_RUNNING );
            bool bSystemIdle = ( MainState == MS_IDLE );

            ConnectionForm->CanStartConn = bSystemIdle && bReceiving;
            CirculatingDlg->CanStartCirc = bSystemIdle && bReceiving;
        }
    }
}


//
// Connections Listview Support Routines
//

void __fastcall TTesTORKMgrMainForm::DoOnConnsUpdated( TObject* sender )
{
    // First, clear current items.
    m_lvSorter->ClearListviewItems();

    // Rebuild the list with the current set of connections
    for( int iConn = 0; iConn < m_currJob->NbrConnRecs; iConn++ )
    {
        CONNECTION_INFO connInfo;
        m_currJob->GetConnection( iConn, connInfo );

        TListItem* newItem = ConnsLV->Items->Add();

        newItem->Caption = IntToStr( connInfo.connNbr );
        newItem->SubItems->Add( IntToStr( connInfo.seqNbr ) );
        newItem->SubItems->Add( IntToStr( connInfo.sectionNbr ) );
        newItem->SubItems->Add( ConnResultText[ connInfo.crResult ] );

        // Save the connection in a LV_SORTER_REC. For the connections
        // listview we don't need to have a pipe inv rec reference
        LV_SORTER_REC* pSortRec = new LV_SORTER_REC;

        pSortRec->iCreateNbr  = connInfo.creationNbr;
        pSortRec->iConnNbr    = connInfo.connNbr;
        pSortRec->iSeqNbr     = connInfo.seqNbr;
        pSortRec->iSectNbr    = connInfo.sectionNbr;
        pSortRec->dtConn      = connInfo.dateTime;
        pSortRec->iConnResult = connInfo.crResult;
        pSortRec->fLength     = connInfo.length;
        pSortRec->sComment    = connInfo.comment;
        pSortRec->iConnID     = connInfo.creationNbr;
        pSortRec->iPipeInvID  = 0;

        newItem->Data = pSortRec;
    }

    // Also notify the connections dialog of the change
    ViewConnsForm->Refresh();
}


void __fastcall TTesTORKMgrMainForm::DoOnPipeTallyUpdated( TObject* sender )
{
    // First, clear current items.
    m_tallySorter->ClearListviewItems();

    // Rebuild the pipe tally
    for( int iRec = 0; iRec < m_currJob->TotalNbrTallyRecs; iRec++ )
    {
        PIPE_TALLY_REC tallyRec;

        if( m_currJob->GetTallyRec( iRec, tallyRec ) )
        {
            TListItem* newItem = PipeTallyLV->Items->Add();

            if( tallyRec.iConnInfoRecNbr != 0 )
            {
                newItem->Caption = IntToStr( tallyRec.iConnNbr );
                newItem->SubItems->Add( FloatToStrF( tallyRec.fLength, ffFixed, 7, 2 ) );
                newItem->SubItems->Add( IntToStr( tallyRec.iSection ) );
                newItem->SubItems->Add( tallyRec.dtConn.DateTimeString() );
            }
            else
            {
                newItem->Caption = "---";
                newItem->SubItems->Add( FloatToStrF( tallyRec.fLength, ffFixed, 7, 2 ) );
                newItem->SubItems->Add( "" );
                newItem->SubItems->Add( "" );
            }

            // Save the connection in a LV_SORTER_REC. Need to look up connection
            // info for that.
            LV_SORTER_REC* pSortRec = new LV_SORTER_REC;

            CONNECTION_INFO connInfo;

            if( m_currJob->GetConnectionByID( tallyRec.iConnInfoRecNbr, connInfo ) )
            {
                pSortRec->iCreateNbr  = connInfo.creationNbr;
                pSortRec->iConnNbr    = connInfo.connNbr;
                pSortRec->iSeqNbr     = connInfo.seqNbr;
                pSortRec->iSectNbr    = connInfo.sectionNbr;
                pSortRec->dtConn      = connInfo.dateTime;
                pSortRec->iConnResult = connInfo.crResult;
                pSortRec->fLength     = connInfo.length;
                pSortRec->sComment    = connInfo.comment;
                pSortRec->iConnID     = connInfo.creationNbr;
                pSortRec->iPipeInvID  = tallyRec.iPipeInvRecNbr;
            }
            else
            {
                pSortRec->iCreateNbr  = 0;
                pSortRec->iConnNbr    = 0;
                pSortRec->iSeqNbr     = tallyRec.iInvSeqNbr;
                pSortRec->iSectNbr    = 0;
                pSortRec->dtConn      = (TDateTime)0.0;
                pSortRec->iConnResult = 0;
                pSortRec->fLength     = tallyRec.fLength;
                pSortRec->sComment    = "";
                pSortRec->iConnID     = 0;
                pSortRec->iPipeInvID  = tallyRec.iPipeInvRecNbr;
            }

            newItem->Data = pSortRec;
        }
    }

    NbrPassedEdit->Caption = IntToStr( m_currJob->NbrPassedTallyRecs );
    TotLengthEdit->Caption = FloatToStrF( m_currJob->PipeTallyLength, ffFixed, 7, 1 );
}


void TTesTORKMgrMainForm::RefreshConnectionsList( void )
{
    DoOnConnsUpdated( NULL );
    DoOnPipeTallyUpdated( NULL );

    // Force a click event on the pipe tally LV to refresh its buttons
    PipeTallyLVSelectItem( PipeTallyLV, PipeTallyLV->Selected, true );
}


void __fastcall TTesTORKMgrMainForm::ConnsLVSelectItem(TObject *Sender, TListItem *Item, bool Selected)
{
    if( ( Item != NULL ) && Selected )
    {
        LV_SORTER_REC* pSortRec = (LV_SORTER_REC*)( Item->Data );

        if( pSortRec != NULL )
            DisplayConnection( pSortRec->iConnID );
    }
}


void __fastcall TTesTORKMgrMainForm::PipeTallyLVSelectItem(TObject *Sender, TListItem *Item, bool Selected)
{
    if( ( Item != NULL ) && Selected )
    {
        LV_SORTER_REC* pSortRec = (LV_SORTER_REC*)( Item->Data );

        if( pSortRec != NULL )
            DisplayConnection( pSortRec->iConnID );
    }

    // Now update the Pipe Tally buttons
    EnablePipeTallyBtns( ( m_currJob == NULL ) || ( m_currJob->Completed ) );
}


void TTesTORKMgrMainForm::EnablePipeTallyBtns( bool forceDisabled )
{
    if( forceDisabled || ( m_currJob == NULL ) || ( m_currJob->Completed ) )
    {
        AddPipeInvRecBtn->Enabled = false;
        ModPipeInvRecBtn->Enabled = false;
        DelPipeInvRecBtn->Enabled = false;
        InsPipeInvRecBtn->Enabled = false;
    }
    else
    {
        TJob::InvActionSet invActions;

        if( PipeTallyLV->Selected != NULL )
        {
            LV_SORTER_REC* pSortRec = (LV_SORTER_REC*)PipeTallyLV->Selected->Data;

            if( pSortRec != NULL )
                m_currJob->GetInvRecActions( pSortRec->iPipeInvID, invActions );
        }
        else
        {
            // If the pipe tally is empty or nothing is selected, we can always add
            invActions = TJob::InvActionSet() << TJob::eIA_CanAdd;
        }

        // Override actions based on main state
        switch( MainState )
        {
            case MS_IDLE:
            case MS_STREAM_CHECK:
            case MS_CIRCULATING:
                // Pipe tally operations allowed in these states
                AddPipeInvRecBtn->Enabled = invActions.Contains( TJob::eIA_CanAdd );
                ModPipeInvRecBtn->Enabled = invActions.Contains( TJob::eIA_CanEdit );
                DelPipeInvRecBtn->Enabled = invActions.Contains( TJob::eIA_CanDelete );
                InsPipeInvRecBtn->Enabled = invActions.Contains( TJob::eIA_CanInsert );
                break;

            default:
                // Pipe tally operations not allowed in any other states
                AddPipeInvRecBtn->Enabled = false;
                ModPipeInvRecBtn->Enabled = false;
                DelPipeInvRecBtn->Enabled = false;
                InsPipeInvRecBtn->Enabled = false;
                break;
        }
    }
}


static bool ShowAddEditCasingDlg( TJob* pJob, float& fNewLength, const String& sCaption, const String& sPrompt )
{
    // Enabling logic ensures we have a valid job if the add/mod/ins/del buttons
    // are clicked, but it doesn't hurt to be a bit paranoid
    if( ( pJob == NULL ) || ( pJob->Completed ) )
    {
        Beep();
        return false;
    }

    String sNewLength;

    if( fNewLength > 0.0 )
        sNewLength = FloatToStrF( fNewLength, ffFixed, 7, 2 );

    if( !InputQuery( sCaption, sPrompt, sNewLength ) )
        return false;

    // Entered length must be > 0.0 and less than max length
    fNewLength = StrToFloatDef( sNewLength, 0.0 );

    if( ( fNewLength > 0.0 ) && ( fNewLength <= TJob::MaxCasingLength( pJob->Units ) ) )
        return true;

    // Fall through means bad length entered
    MessageDlg( "The length you have entered is not valid.", mtError, TMsgDlgButtons() << mbOK, 0 );

    return false;
}


void __fastcall TTesTORKMgrMainForm::AddPipeInvRecBtnClick(TObject *Sender)
{
    // Add an casing segment to the end of the pipe tally list.
    float fNewLength = 0.0;

    if( !ShowAddEditCasingDlg( m_currJob, fNewLength, "Add Casing", "Enter casing length to add: " ) )
        return;

    // On any change of inventory, refresh the Connection dlg length
    if( m_currJob->AddInventoryRec( fNewLength ) )
        ConnectionForm->PrepareForConnection( false /*do not force user to change length*/ );
    else
        Beep();

    // Update auto-record state after changing inventory
    UpdateAutoRecordState();
}


void __fastcall TTesTORKMgrMainForm::InsPipeInvRecBtnClick(TObject *Sender)
{
    // Insert an inventory rec above the currently selected rec
    if( PipeTallyLV->Selected == NULL )
    {
        Beep();
        return;
    }

    LV_SORTER_REC* pSortRec = (LV_SORTER_REC*)PipeTallyLV->Selected->Data;

    if( pSortRec == NULL )
    {
        Beep();
        return;
    }

    float fNewLength = pSortRec->fLength;

    if( !ShowAddEditCasingDlg( m_currJob, fNewLength, "Insert Casing", "Enter casing length to insert: " ) )
        return;

    // On any change of inventory, refresh the Connection dlg length
    if( m_currJob->InsertInventoryRec( pSortRec->iPipeInvID, fNewLength ) )
        ConnectionForm->PrepareForConnection( false /*do not force user to change length*/ );
    else
        Beep();

    // Update auto-record state after changing inventory
    UpdateAutoRecordState();
}


void __fastcall TTesTORKMgrMainForm::ModPipeInvRecBtnClick(TObject *Sender)
{
    // Modify the length of the currently selected rec
    if( PipeTallyLV->Selected == NULL )
    {
        Beep();
        return;
    }

    LV_SORTER_REC* pSortRec = (LV_SORTER_REC*)PipeTallyLV->Selected->Data;

    if( pSortRec == NULL )
    {
        Beep();
        return;
    }

    float fCurrLength = pSortRec->fLength;

    if( !ShowAddEditCasingDlg( m_currJob, fCurrLength, "Edit Casing Length", "Enter casing length: " ) )
        return;

    // On any change of inventory, refresh the Connection dlg length
    if( m_currJob->ModifyInventoryRec( pSortRec->iPipeInvID, fCurrLength ) )
        ConnectionForm->PrepareForConnection( false /*do not force user to change length*/ );
    else
        Beep();

    // Update auto-record state after changing inventory
    UpdateAutoRecordState();
}


void __fastcall TTesTORKMgrMainForm::DelPipeInvRecBtnClick(TObject *Sender)
{
    // Delete the currently selected rec
    if( PipeTallyLV->Selected == NULL )
    {
        Beep();
        return;
    }

    LV_SORTER_REC* pSortRec = (LV_SORTER_REC*)PipeTallyLV->Selected->Data;

    if( pSortRec == NULL )
    {
        Beep();
        return;
    }

    String sCasingLength = FloatToStrF( pSortRec->fLength, ffFixed, 7, 2 );

    int confResult = MessageDlg( "Are you sure you want to delete the selected casing with a length of " +
                                 sCasingLength + " " + GetUnitsText( m_currJob->Units, MT_DIST1_LONG ) +
                                 "? This action is permanent and cannot be undone."
                                 "\r  Press Yes to delete the casing."
                                 "\r  Press Cancel to reconsider.", mtConfirmation, TMsgDlgButtons() << mbYes << mbCancel, 0 );

    if( confResult != mrYes )
        return;

    // On any change of inventory, refresh the Connection dlg length
    if( m_currJob->DeleteInventoryRec( pSortRec->iPipeInvID ) )
        ConnectionForm->PrepareForConnection( false /*do not force user to change length*/ );
    else
        Beep();

    // Update auto-record state after changing inventory
    UpdateAutoRecordState();
}


void TTesTORKMgrMainForm::DisplayConnection( int iConnID )
{
    if( ( MainState == MS_STREAM_CHECK ) || ( MainState == MS_CIRCULATING ) )
    {
        MessageDlg( "You cannot display connections while recording data.", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    if( ( MainState == MS_CONNECTING ) || ( MainState == MS_RESULT_WAIT ) )
    {
        // Nag the user, but only if we're not auto-recording
        if( ConnectionForm->AutoRecordedConn )
            Beep();
        else
            MessageDlg( "You cannot display another connection while a new connection is in progress!", mtError, TMsgDlgButtons() << mbOK, 0 );

        return;
    }

    m_nbrConnPts      = 0;
    m_mostNegPosition = 0.0;

    m_pkTorque   = 0.0;
    m_pkTorquePt = 0.0;

    m_shldrSet = false;
    m_shldrPt  = 0.0;
    m_shldrTq  = 0.0;

    m_rpmAvg->Reset();

    if( m_currJob == NULL )
    {
        DisplaySection( NULL );
    }
    else
    {
        // By default, load the current section
        m_currJob->GetSectionRec( m_currJob->CurrSectIndex, m_currSection );

        DisplaySection( &m_currSection );

        CONNECTION_INFO connInfo;

        if( m_currJob->GetConnectionByID( iConnID, connInfo ) )
        {
            // A valid item has been selected. Get the connection information and populate
            // the graph.
            m_currJob->GetSectionRec( connInfo.sectionNbr - 1,  m_currSection );

            m_shldrSet = connInfo.shoulderSet;
            m_shldrPt  = connInfo.shoulderPt;
            m_shldrTq  = connInfo.shoulderTq;

            ConnectionForm->ShowConnection( &connInfo );

            // Reset the graphs before we re-draw the points
            ResetGraphs();

            // We have to copy the points to a temp array, because the function
            // AddPointToCharts() saves each 'new' point to the m_connPts[] array.
            int nbrPts = m_currJob->GetConnectionPts( connInfo.creationNbr, 0, MAX_NBR_CONN_PTS );

            if( nbrPts > 0 )
            {
                WTTTS_READING* tempPts = new WTTTS_READING[nbrPts];

                try
                {
                    m_currJob->GetConnectionPts( connInfo.creationNbr, tempPts, nbrPts );

                    for( int iPt = 0; iPt < nbrPts; iPt++ )
                        AddPointToCharts( tempPts[iPt] );
                }
                __finally
                {
                    delete [] tempPts;
                }
            }
        }
    }

    TurnsChart->Invalidate();
    TimeChart->Invalidate();

    UpdateConnStats();

    // Force an immediate refresh of the status bar
    RefreshStatus();
}


bool TTesTORKMgrMainForm::StartLogging( LOGGING_TYPE newType )
{
    // Do common work required to clear the graph and start logging data
    // User has started a connection. Clear point info vars first, then
    // reset the graphs.
    m_nbrConnPts      = 0;
    m_mostNegPosition = 0.0;

    m_pkTorque   = 0.0;
    m_pkTorquePt = 0.0;

    m_shldrSet = false;
    m_shldrPt  = 0.0;
    m_shldrTq  = 0.0;

    m_lastWTTTSPkt.havePoint = false;

    m_rpmAvg->Reset();

    ResetGraphs();

    // Start data collection
    if( newType == LT_CIRCULATING )
    {
        // Try to start sampling - abort on an error
        if( !m_devPoller->StartCirculatingMode() )
        {
            MessageDlg( "Circulating Mode could not be started.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return false;
        }
    }
    else
    {
        // Try to start sampling - abort on an error
        if( !m_devPoller->StartDataCollection( ConnectionStreamInterval ) )
        {
            MessageDlg( "Streaming Mode could not be started.", mtError, TMsgDlgButtons() << mbOK, 0 );
            return false;
        }
    }

    // Set the main state
    switch( newType )
    {
        case LT_START_TOOL:   MainState = MS_STREAM_CHECK; break;
        case LT_CONNECTION:   MainState = MS_CONNECTING;   break;
        case LT_CIRCULATING:  MainState = MS_CIRCULATING;  break;
    }

    return true;
}


//
// New Connection Management. Connection controls are implemented in
// the Connection dialog. However, that dialog just calls event
// handlers in this module to execute its functions.
//

bool __fastcall TTesTORKMgrMainForm::ConnectionDlgEvent( CONN_DLG_EVENT anEvent )
{
    // This function is called by the connection manager dialog when
    // an event has occurred that requires the attention of main.
    // Return true to indicate that the event was successfully
    // handled. Return false to indicate the event can not be
    // currently processed.
    bool bSuccess = false;

    switch( anEvent )
    {
        case CDE_START_CONNECTION:

            if( ( MainState == MS_SETTINGS_ONLY ) || ( MainState == MS_IDLE ) )
            {
                // Close other modeless forms first
                ViewConnsForm->Close();
                CirculatingDlg->Close();

                // Can't stream check while connecting. MainState changing while
                // also disable this, but do it now to prevent a race condition
                StreamChkBtn->Enabled = false;

                // Be sure we have the correct section
                PrepareForConnection( false /*don't reset the length edit*/ );

                bSuccess = StartLogging( LT_CONNECTION );
            }

            break;

        case CDE_WAIT_ON_RESULT:

            if( MainState == MS_CONNECTING )
            {
                m_devPoller->StopDataCollection();

                MainState = MS_RESULT_WAIT;

                // Data collection has been stopped. If in auto-shoulder mode,
                // check for a shoulder point. Don't nag on error though if
                // we're in an auto-recorded connection.
                if( m_currSection.autoShldrEnabled && m_currSection.hasShoulder )
                    FindShoulder( !ConnectionForm->AutoRecordedConn );

                // Make a final refresh of the connection stats
                UpdateConnStats();

                // Suggest a connection result
                SuggestConnectionResult();

                bSuccess = true;
            }

            break;

        case CDE_SHOULDER_MODE:

            // Can only enter shoulder mode if we are in the result wait state
            if( MainState == MS_RESULT_WAIT )
            {
                // When in shoulder mode, zoom and pan are disabled.
                TqGraphPanBtn->Enabled = false;

                TurnsChart->Cursor         = crHandPoint;
                TurnsChart->OriginalCursor = crHandPoint;

                TimeChart->Cursor          = crDefault;
                TimeChart->OriginalCursor  = crDefault;

                bSuccess = true;
            }

            break;

        case CDE_SHOULDER_OFF:

            // Can only exit shoulder mode if we are in the result wait state
            if( MainState == MS_RESULT_WAIT )
            {
                TqGraphPanBtn->Enabled = true;

                TurnsChart->Cursor         = crDefault;
                TurnsChart->OriginalCursor = crDefault;

                TimeChart->Cursor         = crDefault;
                TimeChart->OriginalCursor = crDefault;

                bSuccess = true;
            }

            break;

        case CDE_CONN_DONE:
            {
                // Before doing anything, remember if we were currently auto-recording.
                // The state changes below can change the state, but we need to raise
                // an alarm if we are auto-recording and there's no inventory left.
                bool bWasAutoRecording = ( m_eAutoRecState != eARS_Disabled );

                // Set the main state to idle before refreshing screen elements.
                // This prevents bogus messages from being displayed while these
                // elements refresh.
                MainState = MS_IDLE;

                // Make sure data collection has been stopped
                m_devPoller->StopDataCollection();

                // Re-enable pan/zoom
                TqGraphPanBtn->Enabled = true;

                // Clear the graph cursors. Since the mouse is over the Set Shoulder
                // button at the moment, we can revert to a default cursor. The
                // correct cursor will appear when the user mouses over the graphs.
                TurnsChart->Cursor         = crDefault;
                TurnsChart->OriginalCursor = crDefault;

                TimeChart->Cursor         = crDefault;
                TimeChart->OriginalCursor = crDefault;

                // Prepare for next connection
                ValidateNextSection();
                PrepareForConnection();

                // If we were auto-recording and there's no more inventory, raise an alarm.
                if( bWasAutoRecording && !m_currJob->HavePipeInventory )
                    ::PostMessage( Handle, WM_SHOW_ALARM_EVENT, TAlarmDlg::eET_NoInventory, 0 );

                bSuccess = true;
            }
            break;
    }

    // Refresh the charts for every event call
    TurnsChart->Invalidate();
    TimeChart->Invalidate();

    return bSuccess;
}


void __fastcall TTesTORKMgrMainForm::ConnectionDlgStatusReq( MAIN_STATUS_DATA& statusData )
{
    // This function is called by the connection manager dialog when
    // it needs to know the current status of the main form.
    statusData.isDataStreaming    = ( MainState == MS_STREAM_CHECK ) || ( MainState == MS_CONNECTING );
    statusData.nbrDataPtsRecorded = m_nbrConnPts;
    statusData.dataPts            = m_connPts;
    statusData.autoShldrMode      = m_currSection.autoShldrEnabled;
    statusData.hasShoulder        = m_currSection.hasShoulder;
    statusData.shoulderSet        = m_shldrSet;
    statusData.shoulderPt         = m_shldrPt;
    statusData.shoulderTq         = m_shldrTq;
    statusData.mostNegPt          = m_mostNegPosition;
    statusData.pkTorque           = m_pkTorque;
    statusData.pkTorquePt         = m_pkTorquePt;

    if( m_devPoller->DeviceType == DT_WTTTS )
    {
        BATTERY_INFO battInfo;

        if( m_devPoller->GetDeviceBatteryInfo( battInfo ) )
            statusData.battVolts = battInfo.currVolts;
        else
            statusData.battVolts = 0.0;
    }
    else
    {
        statusData.battVolts = 0.0;
    }
}


void TTesTORKMgrMainForm::UpdateAutoRecordState( void )
{
    // If the app is booting, then auto-rec is disabled
    if( MainState == MS_BOOTING )
    {
        m_eAutoRecState = eARS_Disabled;
        return;
    }

    // Fall through means boot is complete. Evaluate system state
    if( GetAutoRecordEnabled() && ( m_currJob != NULL ) && m_currJob->CanAutoRecord && m_currSection.autoRecordOn )
    {
        // Auto-record is enabled in this case
        if( Application->ModalLevel > 0 )
        {
            // Dialog showing - pause auto-record
            m_eAutoRecState = eARS_Paused;
        }
        else
        {
            // No dialog visible. Auto-rec state will be based on system state
            switch( MainState )
            {
                case MS_IDLE:
                    m_eAutoRecState = eARS_Ready;
                    break;

                case MS_STREAM_CHECK:
                case MS_CIRCULATING:
                    m_eAutoRecState = eARS_Paused;
                    break;

                case MS_CONNECTING:
                case MS_RESULT_WAIT:
                    // Base state on connection dlg info
                    if( ConnectionForm->AutoRecordedConn )
                        m_eAutoRecState = eARS_Recording;
                    else
                        m_eAutoRecState = eARS_Manual;
                    break;

                default:
                    // In any other state, auto-rec is disabled
                    m_eAutoRecState = eARS_Disabled;
            }
        }
    }
    else
    {
        m_eAutoRecState = eARS_Disabled;
    }

    // Now update the connection dialog based on the current auto-rec state
    // The only state where we can start auto-recording is if we're in the
    // ready state.
    if( m_eAutoRecState == eARS_Ready )
        ConnectionForm->AutoRecordEnabled = true;
    else
        ConnectionForm->AutoRecordEnabled = false;
}


static String GetValueFromLists( TWTTTSSub::CALIBRATION_ITEM calItemEnum, TStringList* pCaptions, TStringList* pValues )
{
    // Helper function used only in PrepareForConnection() below
    UnicodeString sKey = TWTTTSSub::CalFactorCaptions[ calItemEnum ];

    int itemIndex = pCaptions->IndexOf( sKey );

    if( ( itemIndex < 0 ) || ( itemIndex >= pValues->Count ) )
        return "";

    return pValues->Strings[itemIndex];
}


void TTesTORKMgrMainForm::PrepareForConnection( bool resetLengthEdit )
{
    // Load the current section
    if( m_currJob == NULL )
    {
        DisplaySection( NULL );
    }
    else
    {
        m_currJob->GetSectionRec( m_currJob->CurrSectIndex, m_currSection );
        DisplaySection( &m_currSection );

        // Because connections are recorded to the database, check that the
        // currently connected TesTORK is the same as the previous one in
        // the database. Only do this check if we are actually connected to
        // an actual device.
        //
        // Calibration records are only stored for WTTTS devices, and only
        // if the device has downloaded its calibration
        if( ( m_devPoller->DeviceType == DT_WTTTS ) && ( m_devPoller->DeviceIsCalibrated ) )
        {
            DEVICE_HW_INFO devHWInfo;

            if( !m_devPoller->GetDeviceHWInfo( devHWInfo ) )
            {
                devHWInfo.housingSN = "";
                devHWInfo.mfgSN     = "";
                devHWInfo.fwRev     = "";
                devHWInfo.cfgStraps = "";
                devHWInfo.mfgDate   = "";
                devHWInfo.mfgInfo   = "";
            }

            // Get the current calibration values. They are passed as
            // a string list, which is a pain to convert to the 'fixed'
            // structure job manager requires.
            TStringList* pCaptions = new TStringList();
            TStringList* pValues   = new TStringList();

            m_devPoller->GetCalDataFormatted( pCaptions, pValues );

            CALIBRATION_REC currCalRec;

            currCalRec.creationNbr      = 0;
            currCalRec.housingSN        = devHWInfo.housingSN;
            currCalRec.mfgSN            = devHWInfo.mfgSN;
            currCalRec.fwRev            = devHWInfo.fwRev;
            currCalRec.cfgStraps        = devHWInfo.cfgStraps;
            currCalRec.mfgInfo          = devHWInfo.mfgInfo;
            currCalRec.mfgDate          = devHWInfo.mfgDate;
            currCalRec.lastCalDate      = GetValueFromLists( TWTTTSSub::CI_CALIBRATION_DATE,  pCaptions, pValues );
            currCalRec.torque000Offset  = GetValueFromLists( TWTTTSSub::CI_TORQUE000_OFFSET,  pCaptions, pValues );
            currCalRec.torque000Span    = GetValueFromLists( TWTTTSSub::CI_TORQUE000_SPAN,    pCaptions, pValues );
            currCalRec.torque180Offset  = GetValueFromLists( TWTTTSSub::CI_TORQUE180_OFFSET,  pCaptions, pValues );
            currCalRec.torque180Span    = GetValueFromLists( TWTTTSSub::CI_TORQUE180_SPAN,    pCaptions, pValues );
            currCalRec.tension000Offset = GetValueFromLists( TWTTTSSub::CI_TENSION000_OFFSET, pCaptions, pValues );
            currCalRec.tension000Span   = GetValueFromLists( TWTTTSSub::CI_TENSION000_SPAN,   pCaptions, pValues );
            currCalRec.tension090Offset = GetValueFromLists( TWTTTSSub::CI_TENSION090_OFFSET, pCaptions, pValues );
            currCalRec.tension090Span   = GetValueFromLists( TWTTTSSub::CI_TENSION090_SPAN,   pCaptions, pValues );
            currCalRec.tension180Offset = GetValueFromLists( TWTTTSSub::CI_TENSION180_OFFSET, pCaptions, pValues );
            currCalRec.tension180Span   = GetValueFromLists( TWTTTSSub::CI_TENSION180_SPAN,   pCaptions, pValues );
            currCalRec.tension270Offset = GetValueFromLists( TWTTTSSub::CI_TENSION270_OFFSET, pCaptions, pValues );
            currCalRec.tension270Span   = GetValueFromLists( TWTTTSSub::CI_TENSION270_SPAN,   pCaptions, pValues );
            currCalRec.xTalkTqTq        = GetValueFromLists( TWTTTSSub::CI_XTALK_TQ_TQ,       pCaptions, pValues );
            currCalRec.xTalkTqTen       = GetValueFromLists( TWTTTSSub::CI_XTALK_TQ_TEN,      pCaptions, pValues );
            currCalRec.xTalkTenTq       = GetValueFromLists( TWTTTSSub::CI_XTALK_TEN_TQ,      pCaptions, pValues );
            currCalRec.xTalkTenTen      = GetValueFromLists( TWTTTSSub::CI_XTALK_TEN_TEN,     pCaptions, pValues );
            currCalRec.gyroOffset       = GetValueFromLists( TWTTTSSub::CI_GYRO_OFFSET,       pCaptions, pValues );
            currCalRec.gyroSpan         = GetValueFromLists( TWTTTSSub::CI_GYRO_SPAN,         pCaptions, pValues );
            currCalRec.pressureOffset   = GetValueFromLists( TWTTTSSub::CI_PRESSURE_OFFSET,   pCaptions, pValues );
            currCalRec.pressureSpan     = GetValueFromLists( TWTTTSSub::CI_PRESSURE_SPAN,     pCaptions, pValues );
            currCalRec.battCapacity     = GetValueFromLists( TWTTTSSub::CI_BATTERY_CAPACITY,  pCaptions, pValues );
            currCalRec.battType         = GetValueFromLists( TWTTTSSub::CI_BATTERY_TYPE,      pCaptions, pValues );

            delete pCaptions;
            delete pValues;

            m_currJob->AddCalibrationRec( currCalRec );
        }
    }

    // Prep the connection dialog
    ConnectionForm->PrepareForConnection( resetLengthEdit );

    // Set the auto-record flag in the connection dialog
    UpdateAutoRecordState();
}


void TTesTORKMgrMainForm::UpdateConnStats( void )
{
    if( m_currJob == NULL )
    {
        // In this case, we are just monitoring data. There are no
        // torque or shoulder params to display
        TqMaxEdit->Caption  = L"";
        TqOptEdit->Caption  = L"";
        TqMinEdit->Caption  = L"";
        TqPeakEdit->Caption = FloatToStrF( m_pkTorque, ffFixed, 7, 0 );

        ShldrMaxEdit->Caption = L"";
        ShldrMinEdit->Caption = L"";
        ShldrEdit->Caption    = L"";

        TqDeltaEdit->Caption = L"";

        TurnsMaxEdit->Caption     = L"";
        TurnsMinEdit->Caption     = L"";
        TurnsAtPeakEdit->Caption  = TurnsToText( m_pkTorquePt );
        TurnsAtShldrEdit->Caption = L"";
        PostShldrEdit->Caption    = L"";
    }
    else
    {
        TqMaxEdit->Caption = FloatToStrF( m_currSection.peakTargetMaxTorque, ffFixed, 7, 0 );
        TqOptEdit->Caption = FloatToStrF( m_currSection.peakTargetOptTorque, ffFixed, 7, 0 );
        TqMinEdit->Caption = FloatToStrF( m_currSection.peakTargetMinTorque, ffFixed, 7, 0 );

        if( m_currSection.shoulderMaxTorque == 0.0 )
            ShldrMaxEdit->Caption = L"";
        else
            ShldrMaxEdit->Caption = FloatToStrF( m_currSection.shoulderMaxTorque, ffFixed, 7, 0 );

        if( m_currSection.shoulderMinTorque == 0.0 )
            ShldrMinEdit->Caption = L"";
        else
            ShldrMinEdit->Caption = FloatToStrF( m_currSection.shoulderMinTorque, ffFixed, 7, 0 );

        TqPeakEdit->Caption = FloatToStrF( m_pkTorque, ffFixed, 7, 0 );

        if( m_shldrSet )
        {
            ShldrEdit->Caption   = FloatToStrF( m_shldrTq, ffFixed, 7, 0 );
            TqDeltaEdit->Caption = FloatToStrF( m_pkTorque - m_shldrTq, ffFixed, 7, 0 );

            TurnsAtShldrEdit->Caption = TurnsToText( m_shldrPt );
            PostShldrEdit->Caption    = TurnsToText( m_pkTorquePt - m_shldrPt );
        }
        else
        {
            ShldrEdit->Caption        = L"";
            TqDeltaEdit->Caption      = L"";

            TurnsAtShldrEdit->Caption = L"";
            PostShldrEdit->Caption    = L"";
        }

        TurnsMaxEdit->Caption    = TurnsToText( m_currSection.peakTargetTurns + m_currSection.peakTargetDelta );
        TurnsMinEdit->Caption    = TurnsToText( m_currSection.peakTargetTurns - m_currSection.peakTargetDelta );
        TurnsAtPeakEdit->Caption = TurnsToText( m_pkTorquePt );
    }
}


void __fastcall TTesTORKMgrMainForm::ExportCurvePoints1Click(TObject *Sender)
{
    if( m_nbrConnPts == 0 )
    {
        MessageDlg( "There are no data points to export!", mtError, TMsgDlgButtons() << mbOK, 0 );
        return;
    }

    if( SavePtsDlg->Execute() )
    {
        DeleteFile( SavePtsDlg->FileName );

        TFileStream* outStream = NULL;

        try
        {
            outStream = new TFileStream( SavePtsDlg->FileName, fmCreate );

            // Output header line
            AnsiString sLine = "Time,Turns,Torque,Tension\r";
            outStream->Write( sLine.c_str(), sLine.Length() );

            // Output each point now
            for( int iPt = 0; iPt < m_nbrConnPts; iPt++ )
            {
                WTTTS_READING currReading = m_connPts[iPt];

                sLine.printf( "%d,%d,%f,%f\r", currReading.msecs, currReading.rotation, currReading.torque, currReading.tension );
                outStream->Write( sLine.c_str(), sLine.Length() );
            }
        }
        catch( ... )
        {
            MessageDlg( "Export points failed!", mtError, TMsgDlgButtons() << mbOK, 0 );
        }

        if( outStream != NULL )
            delete outStream;
    }
}


void __fastcall TTesTORKMgrMainForm::HideNegativeTurnsItemClick(TObject *Sender)
{
    // Redraw the currently selected graph
    if( ConnsLV->ItemIndex >= 0 )
        ConnsLVSelectItem( ConnsLV, ConnsLV->Items->Item[ ConnsLV->ItemIndex ], true );
}


void __fastcall TTesTORKMgrMainForm::RecalculateShoulder1Click(TObject *Sender)
{
    FindShoulder();
}


void __fastcall TTesTORKMgrMainForm::ClearStatsItemClick(TObject *Sender)
{
    m_devPoller->CommsClearStats();
    UpdateCommStatus();
}


void TTesTORKMgrMainForm::FindShoulder( bool bNagOnNotFound )
{
    float avgTorque = 0.0;
    int   torquePts = 0;

    // First we calculate the average torque
    for( int iPt = 0; iPt < TorqueByTurnsSeries->XValues->Count; iPt++ )
    {
        avgTorque += TorqueByTurnsSeries->YValue[iPt];
        torquePts++;
    }

    // If no points are available, no shoulder can be determined
    if( torquePts == 0 )
        return;

    avgTorque /= torquePts;

    float pkTorque = 0.0;

    // Calculate the peak so we can use it later
    for( int iPt = 0; iPt < TorqueByTurnsSeries->XValues->Count; iPt++ )
    {
        if( TorqueByTurnsSeries->YValue[iPt] >= pkTorque )
            pkTorque = TorqueByTurnsSeries->YValue[iPt];
    }

    int   iStartPoint   = 1;
    int   iEndPoint     = TorqueByTurnsSeries->XValues->Count - 1;
    int   iSlopeCounter = 0;
    int   iOrigPt       = 0;
    float fOrigTq       = 0;
    float fSlope        = 0;
    bool  bTracking     = false;

    // Work from the start of the graph, search for the shoulder point
    for( iStartPoint; iStartPoint < iEndPoint; iStartPoint++ )
    {
        // Calculate the slope around the current point
        fSlope = CalcSlope( TorqueByTurnsSeries, iStartPoint, 15 );

        // Check if the slope is steep enough and that the current point has a
        // higher torque than the average of the pre-shoulder area
        if( ( fSlope > ( ( pkTorque * 0.5 ) + ( iSlopeCounter * ( pkTorque * 0.005 ) ) ) ) && ( TorqueByTurnsSeries->YValue[iStartPoint] > avgTorque ) )
        {
            iSlopeCounter++;

            // If we haven't started tracking the current area, do so and save
            // the current point and torque for later
            if( !bTracking )
            {
                bTracking = true;
                fOrigTq   = TorqueByTurnsSeries->YValue[iStartPoint];
                iOrigPt   = iStartPoint;
            }

            // Calculate the difference in torque from the first tracked point
            float fDeltaTq = TorqueByTurnsSeries->YValue[iStartPoint] - fOrigTq;

            // If the change in torque is greater than 15% of the peak torque
            // we've found a good shoulder
            if( fDeltaTq >= ( pkTorque * 0.15 ) && ( iSlopeCounter > 30 ) )
            {
                m_shldrSet  = true;
                iStartPoint = iOrigPt;
                break;
            }
        }
        else
        {
            // Stop tracking incase we had started
            bTracking = false;
            iSlopeCounter = 0;
        }
    }

    // If the shoulder was set, show it
    if( m_shldrSet )
    {
        m_shldrPt  = TorqueByTurnsSeries->XValue[iStartPoint];
        m_shldrTq  = TorqueByTurnsSeries->YValue[iStartPoint];

        TurnsChart->Invalidate();
    }

    // If the algorithm couldn't find a shoulder point, advise the user
    if( !m_shldrSet && bNagOnNotFound )
        MessageDlg( "A shoulder point could not be found. You will have to manually set the shoulder point.", mtWarning, TMsgDlgButtons() << mbOK, 0 );

}


float TTesTORKMgrMainForm::CalcSlope( TLineSeries* pData, int iPt, int iPtRange )
{
    // Be safe, don't calculate any points outside of our range
    if( ( iPt - iPtRange < 0 ) || ( iPt + iPtRange >= pData->XValues->Count ) )
        return 0;

    // Calculate the Delta values
    float fDeltaX = pData->XValue[iPt + iPtRange] - pData->XValue[iPt - iPtRange];
    float fDeltaY = pData->YValue[iPt + iPtRange] - pData->YValue[iPt - iPtRange];

    // Never divide by 0
    if( fDeltaX == 0 )
        return 0;

    // Return the slope
    return ( fDeltaY / fDeltaX );
}


float TTesTORKMgrMainForm::CalcAvgSlope( TLineSeries* pData, int iPt, int iPtRange )
{
    // Be safe, don't calculate any points outside of our range
    if( ( iPt - iPtRange < 0 ) || ( iPt + iPtRange >= pData->XValues->Count ) )
        return 0;

    float fAvgY = 0;

    // Add all the Y values within the range
    for( int iCurrPt = iPt - iPtRange; iCurrPt < iPt + iPtRange; iCurrPt++ )
        fAvgY += pData->YValue[iCurrPt];

    // Use the Avg Y to calculate the avg delta Y
    fAvgY = ( fAvgY / ( iPtRange * 2 ) );
    fAvgY = fAvgY - pData->YValue[iPt];

    // We only use Y in this calc as all changes in X should be constant
    return ( fAvgY / ( pData->XValue[iPt + 1] - pData->XValue[iPt] ) );
}


void TTesTORKMgrMainForm::SuggestConnectionResult( void )
{
    // Suggest connection results based on the peak torque position and
    // the shoulder point (if set). Note that this function can only be
    // called if a job is open, and therefore that means m_currSection
    // will be valid.
    if( !m_shldrSet && m_currSection.hasShoulder )
    {
        // Shoulder has not yet been set. Cannot suggest a result, but
        // the user can force a result.
        ConnectionForm->SetResultOptions( ConnResultSet() << CR_FORCE_PASSED << CR_FAILED );
        return;
    }

    // Shoulder has been set. Check that peak torque meets the requirements.
    float pkTorque  = 0.0;
    float turnsAtPk = 0.0;

    for( int iPt = 0; iPt < TorqueByTurnsSeries->Count(); iPt++ )
    {
        if( pkTorque < TorqueByTurnsSeries->YValue[iPt] )
        {
            pkTorque  = TorqueByTurnsSeries->YValue[iPt];
            turnsAtPk = TorqueByTurnsSeries->XValue[iPt];
        }
    }

    if( ( pkTorque < m_currSection.peakTargetMinTorque )
          ||
        ( pkTorque > m_currSection.peakTargetMaxTorque )
          ||
        ( turnsAtPk < m_currSection.peakTargetTurns - m_currSection.peakTargetDelta )
          ||
        ( turnsAtPk > m_currSection.peakTargetTurns + m_currSection.peakTargetDelta )
      )
    {
        // Peak torque doesn't meet spec
        ConnectionForm->SetResultOptions( ConnResultSet() << CR_FORCE_PASSED << CR_FAILED );
        return;
    }

    // Check that the shoulder meets spec. Shoulder values that are zero
    // are not used in this evaluation. Only do this if the section has a shoulder
    bool haveGoodConn = true;

    if( m_currSection.hasShoulder )
    {
        if( m_currSection.shoulderMinTorque != 0.0 )
        {
            if( m_shldrTq < m_currSection.shoulderMinTorque )
                haveGoodConn = false;
        }

        if( m_currSection.shoulderMaxTorque != 0.0 )
        {
            if( m_shldrTq > m_currSection.shoulderMaxTorque )
                haveGoodConn = false;
        }

        if( m_currSection.shoulderPostTurns != 0.0 )
        {
            if( turnsAtPk < m_shldrPt + m_currSection.shoulderPostTurns - m_currSection.shoulderDelta )
                haveGoodConn = false;

            if( turnsAtPk > m_shldrPt + m_currSection.shoulderPostTurns + m_currSection.shoulderDelta )
                haveGoodConn = false;
        }
    }

    if( haveGoodConn )
        ConnectionForm->SetResultOptions( ConnResultSet() << CR_PASSED << CR_FORCE_FAILED );
    else
        ConnectionForm->SetResultOptions( ConnResultSet() << CR_FAILED << CR_FORCE_PASSED );
}


void TTesTORKMgrMainForm::ResetGraphs( void )
{
    TurnsChart->Series[0]->Clear();
    TurnsChart->Series[1]->Clear();
    TurnsChart->Series[2]->Clear();

    TimeChart->Series[0]->Clear();
    TimeChart->Series[1]->Clear();
    TimeChart->Series[2]->Clear();

    TurnsChart->UndoZoom();
    TimeChart->UndoZoom();

    RestoreAxisMaximums();

    TurnsChart->Invalidate();
    TimeChart->Invalidate();
}


void TTesTORKMgrMainForm::RestoreAxisMaximums( void )
{
    // Restores the graph axis max values to either the system default values
    // (if there are no points), or to the scrolled position of the chart
    // (if there are points). Caller must invalidate both graphs for the
    // changes to be displayed.
    if( m_currJob != NULL )
    {
        GetGraphAxisSpan( m_currJob->Units, m_axisMaxVals );
        GetGraphAxisIncrements( m_currJob->Units, m_axisIncrVals );
    }
    else
    {
        GetGraphAxisSpan( m_defaultUOM, m_axisMaxVals );
        GetGraphAxisIncrements( m_defaultUOM, m_axisIncrVals );
    }

    if( ( m_axisMaxVals.bAutoCalTq ) && ( m_currJob != NULL ) && ( m_currSection.peakTargetMaxTorque != 0 ) )
    {
        TurnsChart->LeftAxis->Maximum = 1.2 * m_currSection.peakTargetMaxTorque;
        TimeChart->LeftAxis->Maximum  = 1.2 * m_currSection.peakTargetMaxTorque;
    }
    else
    {
        TurnsChart->LeftAxis->Maximum = m_axisMaxVals.torque;
        TimeChart->LeftAxis->Maximum  = m_axisMaxVals.torque;
    }

    TurnsChart->BottomAxis->Minimum = 0;
    TurnsChart->BottomAxis->Maximum = (float)m_axisMaxVals.turns;

    TimeChart->BottomAxis->Minimum = 0;
    TimeChart->BottomAxis->Maximum = m_axisMaxVals.time;

    TurnsChart->RightAxis->Maximum = m_axisMaxVals.RPM;
    TimeChart->RightAxis->Maximum  = m_axisMaxVals.RPM;
    TurnsChart->RightAxis->Minimum = 0;
    TimeChart->RightAxis->Minimum  = 0;

    TurnsChart->CustomAxes->Items[0]->Maximum = m_axisMaxVals.tension;
    TimeChart->CustomAxes->Items[0]->Maximum  = m_axisMaxVals.tension;
    TurnsChart->CustomAxes->Items[0]->Minimum = ( -1 * m_axisMaxVals.tension );
    TimeChart->CustomAxes->Items[0]->Minimum  = ( -1 * m_axisMaxVals.tension );

    // For the turns chart, set the minor ticks
    if( TurnsChart->BottomAxis->Maximum <= 2.0 )
        TurnsChart->BottomAxis->MinorTickCount = 3;
    else
        TurnsChart->BottomAxis->MinorTickCount = 1;

    // If any points are present, scroll the graph to show the rightmost
    // data points
    ScrollDataIntoView( TurnsChart, m_axisMaxVals.turns, m_axisIncrVals.turns );
    ScrollDataIntoView( TimeChart,  m_axisMaxVals.time,  m_axisIncrVals.time  );
}


void TTesTORKMgrMainForm::EndPanZoomMode( void )
{
    // Make sure buttons are in their 'off' state
    TqGraphPanBtn->Down      = false;
    TqGraphZmInBtn->Down     = false;
    TqGraphZmOutBtn->Enabled = false;

    TimeGraphPanBtn->Down      = false;
    TimeGraphZmInBtn->Down     = false;
    TimeGraphZmOutBtn->Enabled = false;

    TurnsChart->AllowPanning = Vcltee::Teeprocs::pmNone;
    TurnsChart->Zoom->Allow  = false;
    TurnsChart->Cursor       = crDefault;

    TimeChart->AllowPanning = Vcltee::Teeprocs::pmNone;
    TimeChart->Zoom->Allow  = false;
    TimeChart->Cursor       = crDefault;

    TurnsChart->UndoZoom();
    TimeChart->UndoZoom();

    RestoreAxisMaximums();

    TurnsChart->Invalidate();
    TimeChart->Invalidate();
}


void TTesTORKMgrMainForm::ScrollDataIntoView( TChart* aChart, float xAxisSpan, float xAxisIncr )
{
    // The graphs always display xAxisSpan worth of content. If a point value
    // exceeds the axis max, shift the axis right until until it is in view.
    // For the purposes of this function, chart series 0 is always used.
    float axisMin = 0;
    float axisMax = xAxisSpan;

    TChartSeries* pSeries = aChart->Series[0];

    if( pSeries->Count() > 0 )
    {
        float maxXValue = pSeries->XValue[ pSeries->Count() - 1 ];

        while( axisMax < maxXValue )
        {
            axisMax += xAxisIncr;
            axisMin += xAxisIncr;
        }
    }

    // When adjusting the axis min / max, use the method not the discrete
    // properties as max must always be >= min. Also, when setting the axis
    // max, add a small amount to the actual number. This fixes a graph bug
    // where it won't draw the grid line for the max value.
    aChart->BottomAxis->SetMinMax( axisMin, axisMax + 0.1 );
/*
    if( axisMax >= aChart->BottomAxis->Minimum )
    {
        aChart->BottomAxis->Maximum = axisMax + 0.1;
        aChart->BottomAxis->Minimum = axisMin;
    }
    else
    {
        aChart->BottomAxis->Minimum = axisMin;
        aChart->BottomAxis->Maximum = axisMax + 0.1;
    }
*/
}


void TTesTORKMgrMainForm::SetupGraphColours( void )
{
    TColor rpmColor;
    TColor tenColor;
    TColor torqueColor;

    GetGraphColors( rpmColor, torqueColor, tenColor );

    TorqueByTurnsSeries->Color = torqueColor;
    TorqueByTimeSeries->Color  = torqueColor;

    RPMByTimeSeries->Color  = rpmColor;
    RPMByTurnsSeries->Color = rpmColor;

    TenByTimeSeries->Color  = tenColor;
    TenByTurnsSeries->Color = tenColor;

    TurnsChart->LeftAxis->Title->Font->Color = torqueColor;
    TimeChart->LeftAxis->Title->Font->Color  = torqueColor;

    TurnsChart->RightAxis->Title->Font->Color = rpmColor;
    TimeChart->RightAxis->Title->Font->Color  = rpmColor;

    TurnsChart->CustomAxes->Items[0]->Title->Font->Color = tenColor;
    TimeChart->CustomAxes->Items[0]->Title->Font->Color  = tenColor;
}


bool TTesTORKMgrMainForm::AddPointToCharts( const WTTTS_READING& aReading )
{
    // If we are connecting, we fill the points buffer only to its max.
    // Otherwise, if the buffer is full we scroll off the last point to make
    // room for the new point.
    if( m_nbrConnPts >= MAX_NBR_CONN_PTS )
    {
        if( MainState == MS_CONNECTING )
            return false;
    }

    // Calculate RPM. We need one prior point to calc RPM.
    float RPM;

    if( m_nbrConnPts == 0 )
    {
        RPM = 0.0;
    }
    else
    {
        // Calculating the RPM from reading to reading causes a lot of jitter
        // in the value. Instead, compare the current reading to a reading
        // in the past
        int readingsPerSec    = 1000 / ConnectionStreamInterval;
        int pastReadingOffset = readingsPerSec / 2;

        WTTTS_READING* pLastReading;

        if( m_nbrConnPts > pastReadingOffset )
            pLastReading = &( m_connPts[ m_nbrConnPts - pastReadingOffset ] );
        else
            pLastReading = &( m_connPts[0] );

        float deltaTurns = (float)( aReading.rotation - pLastReading->rotation ) / 1000000.0;
        float deltaMin   = (float)( aReading.msecs    - pLastReading->msecs    ) / 1000.0 / 60.0;

        if( deltaMin != 0.0 )
        {
            float newRPM = fabs( deltaTurns ) / deltaMin;

            m_rpmAvg->AddValue( newRPM );

            RPM = m_rpmAvg->AverageValue;
        }
    }

    // Rotation is given in millionths of a turn.
    float turns = (float)( aReading.rotation ) / 1000000.0;

    // Check to see we've passed the negative turns filter point.
    // Note that we still record negative values, we just don't
    // display them until we reach the required number of turns.
    bool displayPt = true;

    if( HideNegativeTurnsItem->Checked )
    {
        if( turns < m_mostNegPosition )
            displayPt = false;
    }

    // Check to see if the turn is turning in the loosening direction.
    if( turns < m_mostNegPosition )
        m_mostNegPosition = turns;

    // At this point we can adjust the turns value by the most
    // neg turns position. Note that we do not change the raw
    // values rec, just our locally calculated value.
    turns -= m_mostNegPosition;

    // Check for peak torque. Record it if necessary.
    if( aReading.torque > m_pkTorque )
    {
        m_pkTorque   = aReading.torque;
        m_pkTorquePt = turns;
    }

    // Add the new point. If the array is full, make room for the point.
    // Do this check individually for the storage array and each graph.
    if( m_nbrConnPts >= MAX_NBR_CONN_PTS )
    {
        memmove( m_connPts, &( m_connPts[1] ), ( MAX_NBR_CONN_PTS - 1 ) * sizeof( WTTTS_READING ) );

        m_nbrConnPts = MAX_NBR_CONN_PTS - 1;
    }

    if( TorqueByTimeSeries->Count() >= MAX_NBR_CONN_PTS )
    {
        TorqueByTimeSeries->Delete( 0 );
        RPMByTimeSeries->Delete( 0 );
        TenByTimeSeries->Delete( 0 );
    }

    if( TorqueByTurnsSeries->Count() >= MAX_NBR_CONN_PTS )
    {
        // Also delete the oldest point from the graph series as well.
        if( displayPt )
        {
            TorqueByTurnsSeries->Delete( 0 );
            RPMByTurnsSeries->Delete( 0 );
            TenByTurnsSeries->Delete( 0 );
        }
    }

    // Save the new reading now
    m_connPts[m_nbrConnPts] = aReading;
    m_nbrConnPts++;

    // Add the point to the charts now
    if( displayPt )
    {
        // Check for axis overrun first. In this release we only check for
        // x-axis overruns, and in that case we 'slide' the data across.
        // This preserves the aspect ratio of the graphs.
        if( turns > TurnsChart->BottomAxis->Maximum )
            ScrollDataIntoView( TurnsChart, m_axisMaxVals.turns, m_axisIncrVals.turns );

        // Now add the new points
        TorqueByTurnsSeries->AddXY( turns, aReading.torque );
        RPMByTurnsSeries->AddXY( turns, RPM );
        TenByTurnsSeries->AddXY( turns, aReading.tension );
    }

    // Repeat for the time chart
    float timeOfPt = aReading.msecs / 1000.0;

    if( timeOfPt > TimeChart->BottomAxis->Maximum )
        ScrollDataIntoView( TimeChart, m_axisMaxVals.time, m_axisIncrVals.time );

    TorqueByTimeSeries->AddXY( timeOfPt, aReading.torque );
    RPMByTimeSeries->AddXY( timeOfPt, RPM );
    TenByTimeSeries->AddXY( timeOfPt, aReading.tension );

    // Record that we've received a packet from the WTTTS
    m_lastWTTTSPkt.havePoint   = true;
    m_lastWTTTSPkt.turnValue   = turns;
    m_lastWTTTSPkt.gyroValue   = (float)( aReading.rotation ) / 1000000.0;
    m_lastWTTTSPkt.timeValue   = aReading.msecs / 1000.0;
    m_lastWTTTSPkt.torqueValue = aReading.torque;
    m_lastWTTTSPkt.rpmValue    = RPM;

    return true;
}


void __fastcall TTesTORKMgrMainForm::TqGraphRPMTenBtnClick(TObject *Sender)
{
    // The button rotates between three states, inc the current state
    TqGraphRPMTenBtn->Tag++;

    // If our button has gone through all states, wrap around
    if( TqGraphRPMTenBtn->Tag == eGraphButtonStates )
        TqGraphRPMTenBtn->Tag = eGraphShowBoth;

    // Handle the button press based on the state of the tag
    switch( TqGraphRPMTenBtn->Tag )
    {
        case eGraphShowBoth:
            RPMByTurnsSeries->Visible = true;
            TenByTurnsSeries->Visible = true;
            RPMByTimeSeries->Visible  = true;
            TenByTimeSeries->Visible  = true;

            // RPM is shown, use the default margin
            TurnsChart->MarginRight   = defaultRightMargin;
            TimeChart->MarginRight    = defaultRightMargin;

            break;

        case eGraphShowRPM:
            RPMByTurnsSeries->Visible = true;
            TenByTurnsSeries->Visible = false;
            RPMByTimeSeries->Visible  = true;
            TenByTimeSeries->Visible  = false;

            // RPM is shown, use the default margin
            TurnsChart->MarginRight   = defaultRightMargin;
            TimeChart->MarginRight    = defaultRightMargin;

            break;

        case eGraphShowTension:
            RPMByTurnsSeries->Visible = false;
            TenByTurnsSeries->Visible = true;
            RPMByTimeSeries->Visible  = false;
            TenByTimeSeries->Visible  = true;

            // RPM is hidden, calculate the margin
            TurnsChart->MarginRight   = TurnsChart->Width - TurnsChart->RightAxis->PosAxis;
            TimeChart->MarginRight    = TimeChart->Width  - TimeChart->RightAxis->PosAxis;

            break;

        case eGraphShowNone:
        default:
            RPMByTurnsSeries->Visible = false;
            TenByTurnsSeries->Visible = false;
            RPMByTimeSeries->Visible  = false;
            TenByTimeSeries->Visible  = false;

            // RPM is hidden, calculate the margin
            TurnsChart->MarginRight   = TurnsChart->Width - TurnsChart->RightAxis->PosAxis;
            TimeChart->MarginRight    = TimeChart->Width  - TimeChart->RightAxis->PosAxis;

            break;
    }

    // Save current state to registry; on first graph show, restore this state
    /* to do */

    TqGraphRPMTenBtn->Down = false;

    RestoreAxisMaximums();

    TurnsChart->Invalidate();
}


void __fastcall TTesTORKMgrMainForm::GraphPanBtnClick(TObject *Sender)
{
    // When panning is enabled, zooming gets disabled and vice-versa.
    // Whenever the user clicks this button, zoom mode must always
    // be off.
    TChart*       aChart;
    TSpeedButton* panBtn;
    TSpeedButton* zoomOutBtn;

    if( Sender == TqGraphPanBtn )
    {
        aChart     = TurnsChart;
        panBtn     = TqGraphPanBtn;
        zoomOutBtn = TqGraphZmOutBtn;
    }
    else if( Sender == TimeGraphPanBtn )
    {
        aChart     = TimeChart;
        panBtn     = TimeGraphPanBtn;
        zoomOutBtn = TimeGraphZmOutBtn;
    }
    else
    {
        return;
    }

    zoomOutBtn->Enabled = false;
    aChart->Zoom->Allow = false;

    if( panBtn->Down )
    {
        aChart->AllowPanning = Vcltee::Teeprocs::pmBoth;
        aChart->Cursor       = CC_HANDGRAB;
    }
    else
    {
        // This case means both the pan and zoom buttons must be up
        EndPanZoomMode();
    }
}


void __fastcall TTesTORKMgrMainForm::GraphZmInBtnClick(TObject *Sender)
{
    // Whether this button is being clicked up or down, pan
    // mode gets turned off
    TChart*       aChart;
    TSpeedButton* zoomInBtn;
    TSpeedButton* zoomOutBtn;

    if( Sender == TqGraphZmInBtn )
    {
        aChart     = TurnsChart;
        zoomInBtn  = TqGraphZmInBtn;
        zoomOutBtn = TqGraphZmOutBtn;
    }
    else if( Sender == TimeGraphZmInBtn )
    {
        aChart     = TimeChart;
        zoomInBtn  = TimeGraphZmInBtn;
        zoomOutBtn = TimeGraphZmOutBtn;
    }
    else
    {
        return;
    }

    aChart->AllowPanning = Vcltee::Teeprocs::pmNone;

    // Can only zoom out if zoom mode is enabled
    zoomOutBtn->Enabled = zoomInBtn;

    if( zoomInBtn->Down )
    {
        aChart->Zoom->Allow = true;
        aChart->Cursor      = CC_ZOOM;
    }
    else
    {
        EndPanZoomMode();
    }
}


void __fastcall TTesTORKMgrMainForm::GraphZmOutBtnClick(TObject *Sender)
{
    if( Sender == TqGraphZmOutBtn )
    {
        if( TqGraphZmOutBtn->Down )
        {
            TurnsChart->ZoomPercent( 75 );
            TqGraphZmOutBtn->Down = false;
        }
    }
    else if( Sender == TimeGraphZmOutBtn )
    {
        if( TimeGraphZmOutBtn->Down )
        {
            TimeChart->ZoomPercent( 75 );
            TimeGraphZmOutBtn->Down = false;
        }
    }
}


void __fastcall TTesTORKMgrMainForm::GraphSettingBtnClick(TObject *Sender)
{
    UNITS_OF_MEASURE currUOM;

    if( m_currJob == NULL )
        currUOM = m_defaultUOM;
    else
        currUOM = m_currJob->Units;

    if( TGraphSettingForm::ShowDlg( currUOM ) )
    {
        RestoreAxisMaximums();
        SetupGraphColours();

        // Need to recreate the averaging object if the averaging window
        // has changed
        if( m_rpmAvg->AverageSize != GetRPMAvgingValue() )
        {
            delete m_rpmAvg;

            m_rpmAvg = new TMovingAverage( GetRPMAvgingValue(), 0.0 /*no variance*/ );

            MessageDlg( "You have changed the number of samples to average RPM data over. "
                        "This new value will be used on the next graph plotted.",
                        mtInformation, TMsgDlgButtons() << mbOK, 0 );
        }

        // Force a redraw of the charts
        TurnsChart->Invalidate();
        TimeChart->Invalidate();
    }
}


//
// Circulation Management
//

bool __fastcall TTesTORKMgrMainForm::CircDlgEvent( eCircEvent anEvent )
{
    bool bSuccess = false;

    switch( anEvent )
    {
        case eCircEvt_StartCirc:

            if( ( MainState == MS_SETTINGS_ONLY ) || ( MainState == MS_IDLE ) )
            {
                // Close other modeless forms first
                ViewConnsForm->Close();
                ConnectionForm->Close();

                bSuccess = StartLogging( LT_CIRCULATING );
            }

            break;

        case eCircEvt_StopCirc:

            if( MainState == MS_CIRCULATING )
            {
                // Can go idle now
                m_devPoller->StopCirculatingMode();

                MainState = MS_IDLE;

                bSuccess = true;
            }

            break;
    }

    // Regardless of the event, force the graphs to repaint
    TurnsChart->Invalidate();
    TimeChart->Invalidate();

    return bSuccess;
}


//
// Splitter Functions
//

void __fastcall TTesTORKMgrMainForm::HorzSplitterMoved(TObject *Sender)
{
    // After moving the splitter, save the width of the page control.
    // Only do so though if the Nav Panel is pinned
    if( NavPanelState == eNPS_Pinned )
        SaveCtrlSize( CN_MAIN_PG_CTRL, NavPanel->Width );
}


void __fastcall TTesTORKMgrMainForm::VertSplitterMoved(TObject *Sender)
{
    // After moving the splitter, save the height of the turns chart
    SaveCtrlSize( CN_MAIN_GRAPH_HEIGHT, TurnsChart->Height );
}


//
// Comms Handlers
//

#define NBR_COMM_STATUS_ITEMS 18

const STATS_GRID_ENTRY sgeMainItems[NBR_COMM_STATUS_ITEMS] =
{
    SGE_WIRED_DEV_STAT,
    SGE_NETWORK_STAT,
    SGE_DEV_TYPE,
    SGE_DEV_STATUS,
    SGE_HOUSING_SN,
    SGE_RSSI,
    SGE_CTRL_PKT_ERRORS,
    SGE_DATA_PKT_ERRORS,
    SGE_RAW_LOGGING_STATE,
    SGE_CAL_LOGGING_STATE,
    SGE_RFC_LOGGING_STATE,
    SGE_CHART_LOGGING_STATE,
    SGE_BATT_TYPE,
    SGE_BATT_VOLTS,
    SGE_BATT_CAP_LEFT,
    SGE_AVG_TENSION,
    SGE_AVG_TORQUE,
    SGE_RPM,
};


void TTesTORKMgrMainForm::InitCommStatsGrid( void )
{
    for( int iItem = 0; iItem < NBR_COMM_STATUS_ITEMS; iItem++ )
        CommsStatusView->InsertRow( CommStatusGridKeys[ sgeMainItems[iItem] ], L"", true );
}


void TTesTORKMgrMainForm::UpdateCommStatus( void )
{
    // First update the comm stats hardware dlg
    HardwareStatusDlg->UpdateStatus( m_currJob, m_devPoller );

    // Now sync the subset of items we display with its display
    HardwareStatusDlg->SyncDisplay( CommsStatusView );

    // If we are currently recving data, see if its time to check the battery level
    if( m_devPoller->DeviceIsRecving )
    {
        // If we haven't had an alarm in the last 30 minutes, check the battery
        if( time( NULL ) >= m_lastBattAlarmTime + 30 * 60 /* 30 minutes */ )
        {
            // Check the battery once per minute
            if( HaveTimeout( m_lastBattCheckTick, 60 * 1000 /*once per minute */ ) )
            {
                m_lastBattCheckTick = GetTickCount();

                // Check battery info
                BATTERY_INFO battInfo;

                if( m_devPoller->GetDeviceBatteryInfo( battInfo ) )
                {
                    // If the current voltage is below the minimum allowed, then
                    // post a message that will show the user a warning dialog
                    float fMinVBatt = GetMinBattLevel();

                    if( battInfo.avgVolts < fMinVBatt )
                    {
                        // Post the message to show the warning
                        PostMessage( Handle, WM_SHOW_ALARM_EVENT, TAlarmDlg::eET_LowBattery, 0 );

                        m_lastBattAlarmTime = time( NULL );
                    }
                }
            }
        }
    }

    // Update the status text based on the state of the base radio and TesTORK
    int iBattLevelImage = 0;  // No enums defined for this image list

    String sStatus;

    if( m_devPoller != NULL )
    {
        switch( m_devPoller->PollerState )
        {
            case TCommPoller::PS_INITIALIZING:
                // Thread is starting
                SetStatusProgress( "Start-up", "", clBtnFace, false, 0 );
                sStatus = "System Initializing";
                break;

            case TCommPoller::PS_BR_PORT_SCAN:
                // Looking for base radio comm port.
                SetStatusProgress( "Base Radio", "Checking ports for Base Radio", clStatusGreen, true, 0 );
                sStatus = "Scanning for Base Radio";
                break;

            case TCommPoller::PS_TT_PORT_SCAN:
            case TCommPoller::PS_TT_CHAN_SCAN:
                // Looking for TT comm port or channel
                if( m_devPoller->PollerState == TCommPoller::PS_TT_PORT_SCAN )
                    SetStatusProgress( "TesTORK", "Checking ports for TesTORK", clStatusGreen, true, 0 );
                else
                    SetStatusProgress( "TesTORK", "Scanning radio channels for TesTORK", clStatusGreen, true, 0 );

                sStatus = "Scanning for TesTORK";
                break;

            case TCommPoller::PS_RUNNING:
                // Ports open and have TesTORK. Base status on RSSI level
                {
                    String sRSSI = "RSSI " + IntToStr( m_devPoller->TesTORKRSSI ) + "%";
                    SetStatusProgress( "TesTORK", sRSSI, clStatusGreen, false, m_devPoller->TesTORKRSSI );
                }
                sStatus = "Connected to TesTORK";
                break;

            default:
                // Comm error!
                SetStatusProgress( "Sys Error", "System Error", clRed, true, 0 );
                sStatus = "System Error";
                break;
        }
    }
    else
    {
        sStatus = "No Comms";
    }

    // Update battery visual
    if( ( m_devPoller != NULL ) && ( m_devPoller->PollerState == TCommPoller::PS_RUNNING ) )
    {
        BATTERY_INFO battInfo;

        if( m_devPoller->GetDeviceBatteryInfo( battInfo ) )
        {
            // Update the icon
            iBattLevelImage = m_devPoller->BatteryLevel;

            // Set the volts value
            sStatus += ", Battery " + FloatToStrF( battInfo.avgVolts, ffFixed, 7, 2 ) + " Volts";
        }
        else
        {
            sStatus += ", Battery Level Unknown";
        }
    }
    else
    {
        sStatus += ", Battery Level Unknown";
    }

    // Set the hint message
    SysStatusPaintBox->Hint = sStatus;

    // Now refresh the paintbox
    m_commsStatus.iBattLevelImageIndex = iBattLevelImage;

    SysStatusPaintBox->Invalidate();
}


void TTesTORKMgrMainForm::SetStatusProgress( const String& sText, const String& sHint, TColor cBar, bool bScanning, int iPct )
{
    CommsProgBar->Appearance->ValueFormat = sText;

    if( bScanning )
    {
        CommsProgBar->Style    = Advsmoothprogressbar::pbstMarquee;
        CommsProgBar->Position = 0;
        CommsProgBar->MarqueeColor = cBar;
        CommsProgBar->MarqueeSize  = CommsProgBar->Maximum;
        CommsProgBar->MarqueeInterval = 50;
    }
    else
    {
        TColor cColorTo;

        if( cBar == clStatusGreen )
            cColorTo = clLime;
        else
            cColorTo = cBar;

        // Validate percent, just in case...
        double dPct = iPct;

        if( dPct < CommsProgBar->Minimum )
            dPct = CommsProgBar->Minimum;
        else if( dPct > CommsProgBar->Maximum )
            dPct = CommsProgBar->Maximum ;

        CommsProgBar->Style    = Advsmoothprogressbar::pbstNormal;
        CommsProgBar->Position = dPct;
        CommsProgBar->GlowAnimation     = false;
        CommsProgBar->ProgressAnimation = false;
        CommsProgBar->Appearance->ProgressFill->Color   = cBar;
        CommsProgBar->Appearance->ProgressFill->ColorTo = cColorTo;
    }
}


void __fastcall TTesTORKMgrMainForm::ILockBypassBtnClick(TObject *Sender)
{
    // Check that the user wants to continue
    int mrResult = MessageDlg( "You are about to bypass the Interlock."
                               "\r  Press OK to continue."
                               "\r  Press Cancel to reconsider.", mtWarning, TMsgDlgButtons() << mbOK << mbCancel, 0 );

    if( mrResult == mrOk )
    {
        String sComment = "";
        if( !InputQuery( L"Bypassing Interlock", L"Reason:", sComment ) )
            return;

        // Bypassing the interlock, add a comment to record the event
        if( m_currJob != NULL )
            m_currJob->AddMainComment( "ZWI Bypassed by User - Reason: " + sComment );

        // Bypass for 10 seconds
        m_devPoller->TempDisableZWI( (DWORD)10000 );
    }
}


void __fastcall TTesTORKMgrMainForm::StreamChkBtnClick(TObject *Sender)
{
    // Toggle the button state first
    if( StreamChkBtn->Down )
    {
        if( ( MainState == MS_SETTINGS_ONLY ) || ( MainState == MS_IDLE ) )
        {
            if( StartLogging( LT_START_TOOL ) )
                StreamChkBtn->Caption = "Stop Stream Check";
        }
        else
        {
            StreamChkBtn->Down = false;
        }
    }
    else
    {
        m_devPoller->StopDataCollection();

        // Can go idle now
        MainState = MS_IDLE;

        StreamChkBtn->Caption = "Start Stream Check";
    }
}


void __fastcall TTesTORKMgrMainForm::CommsStatusViewSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect)
{
    // This grid is read-only
    CanSelect = false;
}


bool TTesTORKMgrMainForm::LogCalRecord( const WTTTS_READING& aReading, const String& logFileName )
{
    // Write the record to file as text. Return true if the record
    // was logged okay, false otherwise.
    bool loggedOk = false;

    TFileStream* logStream = NULL;

    try
    {
        if( FileExists( logFileName ) )
        {
            // File exists. Open it and advance pointer to end of file
            logStream = new TFileStream( logFileName, fmOpenReadWrite );

            logStream->Position = logStream->Size;
        }
        else
        {
            // File doesn't exist. Create it and write a header line.
            logStream = new TFileStream( logFileName, fmCreate );

            AnsiString sHeader( "time,elapsed_msecs,rotation,torque,tension\r" );

            logStream->Write( sHeader.c_str(), sHeader.Length() );
        }

        AnsiString sTime = Time().TimeString();

        AnsiString sLine;
        sLine.printf( "%s,%u,%i,%f,%f\r", sTime.c_str(), aReading.msecs, aReading.rotation, aReading.torque, aReading.tension );

        logStream->Write( sLine.c_str(), sLine.Length() );

        loggedOk = true;
    }
    catch( ... )
    {
        loggedOk = false;
    }

    if( logStream != NULL )
        delete logStream;

    return loggedOk;
}


void __fastcall TTesTORKMgrMainForm::SysSettingsBtnClick(TObject *Sender)
{
    String newPassword;

    if( !TPasswordInputForm::CheckPassword( PasswordDlgCaption, GetSysAdminPassword(), newPassword ) )
        return;

    // If newPassword is not empty, it was changed by the user
    if( newPassword.Length() > 0 )
        SaveSysAdminPassword( newPassword );

    // Remember the current state of the interlock enable, in case it gets changed
    bool interlockWasEnabled = GetInterlockEn();

    // Show the dlg, record if important settings changed
    DWORD dwSettingsChanged = TSystemSettingsForm::ShowDlg( m_devPoller, m_currJob );

    // Get current job unit type and then update the comms manager
    UNITS_OF_MEASURE currUOM;

    if( m_currJob == NULL )
        currUOM = m_defaultUOM;
    else
        currUOM = m_currJob->Units;

    m_devPoller->RefreshSettings( currUOM );

    // Only process interlock state changes if we are connected
    // to an actual base radio
    if( m_devPoller->InterlockState != TCommPoller::IS_NO_HARDWARE )
    {
        // If the interlock state changed while a job is open, record
        // that in the job's log
        if( ( m_currJob != NULL ) && ( m_currJob->Completed == false ) )
        {
            if( interlockWasEnabled != GetInterlockEn() )
            {
                if( GetInterlockEn() )
                {
                    // Interlock is now enabled - put an automatically
                    // generated comment in the log
                    m_currJob->AddMainComment( "Interlocks enabled" );
                }
                else
                {
                    // Interlock is being disabled. Operator will have to
                    // add a comment to the log.
                    MessageDlg( "You have disabled the Interlock System. You must "
                                "enter a comment describing why this was done.",
                                mtInformation, TMsgDlgButtons() << mbOK, 0 );

                    m_currJob->AddMainComment( "Interlocks DISABLED" );

                    MainCommentForm->ShowMainComments( m_currJob, true );
                }
            }
        }
    }

    // Reload the poller if the comms or misc settings were changed
    if( ( dwSettingsChanged & TSystemSettingsForm::COMMS_SETTINGS_CHANGED ) || ( dwSettingsChanged & TSystemSettingsForm::MISC_SETTINGS_CHANGED ) )
    {
        delete m_devPoller;
        m_devPoller = new TCommPoller( Handle );

        // Update the visibility of the PDS Pipe box. Do this by invoking the
        // form resize handler
        FormResize( this );

        // Check for a change in simulation mode
        if( m_devPoller->UsingTesTORKSim )
        {
            // In sim mode. If the job isn't, then warn the user about changes being lost
            if( m_currJob != NULL )
            {
                if( m_currJob->InSimMode == false )
                {
                    // We are now in sim mode and the currently open job is not in sim mode. Warn the user
                    MessageDlg( "You have entered Simulation Mode. Any changes made to the currently open job "
                                "will not be saved.", mtWarning, TMsgDlgButtons() << mbOK, 0 );
                }
            }
        }
        else
        {
            // Not in sim mode. If the current job is, warn the user and close the job
            if( m_currJob != NULL )
            {
                if( m_currJob->InSimMode )
                {
                    MessageDlg( "You have left Simulation Mode. The current job must be closed and any changes made to it will be lost. "
                                "Press OK to continue...", mtInformation, TMsgDlgButtons() << mbOK, 0 );

                    OpenJob( "" );
                }
            }
        }

        EnterSimMode( m_devPoller->UsingTesTORKSim );
    }

    // Update the auto-connect state after dismissing the dialog
    UpdateAutoRecordState();
}


void __fastcall TTesTORKMgrMainForm::TestAlarmBtnClick(TObject *Sender)
{
    // Post a test alarm message
    ::PostMessage( Handle, WM_SHOW_ALARM_EVENT, TAlarmDlg::eET_Test, 0 );
}


void __fastcall TTesTORKMgrMainForm::ShowDiagDataBtnClick(TObject *Sender)
{
    if( m_devPoller == NULL )
    {
        Beep();
        return;
    }

    // Make sure we are the dlg's parent, then show it modelessly
    HardwareStatusDlg->Parent = this;

    ShowModelessForm( HardwareStatusDlg );
}


void __fastcall TTesTORKMgrMainForm::ZeroReadingsBtnClick(TObject *Sender)
{
    // Poller cannot be NULL if this button was enabled
    TZeroWTTTSForm::ShowDlg( m_devPoller );
}


void __fastcall TTesTORKMgrMainForm::StartStopToggleItemClick(TObject *Sender)
{
    // Pass the Toggle click based on the shown dialogs, if both are visible
    // do nothing.
    if( ConnectionForm->Showing && !CirculatingDlg->Showing )
        ConnectionForm->StartStopItemClick( ConnectionForm->StartStopItem );
    else if( CirculatingDlg->Showing && !ConnectionForm->Showing )
        CirculatingDlg->StartStopItemClick( CirculatingDlg->StartStopItem );
}


//---------------------------------------------------------------------------
//
// Preview Reports Group
//

// The following enum defines the types of reports available. Reports will
// be displayed in the report selection combo in the order defined here.
typedef enum {
    MRT_JOB_STATS,
    MRT_SECTION_DETAILED,
    MRT_SECTION_SUMMARY,
    MRT_CONNECTIONS,
    MRT_JOB_COMMENTS,
    MRT_CALIBRATION_INFORMATION,
    NBR_MAIN_RPT_TYPES
} MAIN_RPT_TYPE;

// The following array defines the report name shown to the user
// for each of the defined report types.
static UnicodeString RptSelectionText[NBR_MAIN_RPT_TYPES] = {
  "Job Statistics",
  "Sections (detail)",
  "Sections (summary)",
  "Connections",
  "Job Comments",
  "Calibration Records",
};


void TTesTORKMgrMainForm::InitRptSelectionCombo( void )
{
    RptTypeCombo->Items->Clear();

    for( int iItem = 0; iItem < NBR_MAIN_RPT_TYPES; iItem++ )
        RptTypeCombo->Items->Add( RptSelectionText[iItem] );

    RptTypeCombo->ItemIndex = 0;
}


void TTesTORKMgrMainForm::SetSectionButton( TButton* pButtonStyle )
{
    if( ( pButtonStyle->Name == "EditSectBtn" ) || ( pButtonStyle->Name == "NewSectBtn" ) )
    {
        NewEditSectBtn->Caption = pButtonStyle->Caption;
        NewEditSectBtn->OnClick = pButtonStyle->OnClick;
        NewEditSectBtn->Enabled = pButtonStyle->Enabled;
    }
}


void __fastcall TTesTORKMgrMainForm::RptPreviewBtnClick(TObject *Sender)
{
    // Print the report accoring user's choice
    switch( RptTypeCombo->ItemIndex )
    {
        case MRT_SECTION_DETAILED:

            if( RptRangeRB->Checked )
                RptSectionsForm->ShowReport( m_currJob, RptFromEdit->Text.ToInt(), RptToEdit->Text.ToInt() );
            else
                RptSectionsForm->ShowReport( m_currJob );

            break;

        case MRT_SECTION_SUMMARY:

            if( RptRangeRB->Checked )
                RptSecSummaryForm->ShowReport( m_currJob, RptFromEdit->Text.ToInt(), RptToEdit->Text.ToInt() );
            else
                RptSecSummaryForm->ShowReport( m_currJob );

            break;

        case MRT_CONNECTIONS:

            if( RptRangeRB->Checked )
                RptConnForm->ShowReport( m_currJob, RptFromEdit->Text.ToInt(), RptToEdit->Text.ToInt() );
            else
                RptConnForm->ShowReport( m_currJob );

            break;

        case MRT_JOB_STATS:
            RptStatsForm->ShowPlots( m_currJob );
            break;

        case MRT_JOB_COMMENTS:
            CommentReportForm->ShowReport( m_currJob );
            break;

        case MRT_CALIBRATION_INFORMATION:

            if( RptRangeRB->Checked )
                RptCalRecForm->ShowReport( m_currJob, RptFromEdit->Text.ToInt(), RptToEdit->Text.ToInt() );
            else
                RptCalRecForm->ShowReport( m_currJob );

            break;
    }
}


void __fastcall TTesTORKMgrMainForm::RptTypeComboClick(TObject *Sender)
{
    // Populate the range boxes based on the current report selection
    if( m_currJob != NULL )
    {
        switch( RptTypeCombo->ItemIndex )
        {
            case MRT_JOB_STATS:
            case MRT_JOB_COMMENTS:
                // No range reported on these reports
                RptFromEdit->Text = "";
                RptToEdit->Text   = "";
                break;

            case MRT_SECTION_DETAILED:
            case MRT_SECTION_SUMMARY:
                RptFromEdit->Text = "1";
                RptToEdit->Text   = IntToStr( m_currJob->NbrSections );
                break;

            case MRT_CONNECTIONS:
                // Determining the range of connection numbers is not trivial,
                // as there is no correlation between the number of connection
                // records and connection numbers.
                {
                    int minConnNbr = m_currJob->FirstConn;
                    int maxConnNbr = m_currJob->FirstConn;

                    for( int iConn = 0; iConn < m_currJob->NbrConnRecs; iConn++ )
                    {
                        CONNECTION_INFO connectionInfo;
                        m_currJob->GetConnection( iConn, connectionInfo );

                        if( connectionInfo.connNbr < minConnNbr )
                            minConnNbr = connectionInfo.connNbr;

                        if( connectionInfo.connNbr > maxConnNbr )
                            maxConnNbr = connectionInfo.connNbr;
                    }

                    RptFromEdit->Text = IntToStr( minConnNbr );
                    RptToEdit->Text   = IntToStr( maxConnNbr );
                }
                break;

            case MRT_CALIBRATION_INFORMATION:
                RptFromEdit->Text = "1";
                RptToEdit->Text   = IntToStr( m_currJob->NbrCalibrationRecs );
                break;
        }

        RptAllRB->Checked = true;
        RptAllRBClick( RptAllRB );
    }
}


void __fastcall TTesTORKMgrMainForm::RptRangeRBClick(TObject *Sender)
{
    // Allow user to input number into Text box
    RptFromEdit->Color    = clWindow;
    RptToEdit->Color      = clWindow;
    RptFromEdit->ReadOnly = false;
    RptToEdit->ReadOnly   = false;
}


void __fastcall TTesTORKMgrMainForm::RptAllRBClick(TObject *Sender)
{
    // Prevent user to input number into Text box
    // while user select "All" option
    RptFromEdit->Color    = clBtnFace;
    RptToEdit->Color      = clBtnFace;
    RptFromEdit->ReadOnly = True;
    RptToEdit->ReadOnly   = True;
}


//
// Info Panel Handlers
//

void __fastcall TTesTORKMgrMainForm::InfoSizeChangePanelClick(TObject *Sender)
{
    // Check which size the InfoPanel currently is
    if( InfoPanel->Width == INFO_PANEL_LARGE_WIDTH )
    {
        // Currently Large, shrink
        InfoPanel->Width = INFO_PANEL_SMALL_WIDTH;

        InfoSizeChangePanel->Alignment = taLeftJustify;
        InfoSizeChangePanel->Caption   = "�";
    }
    else
    {
        // Current Small, expand
        InfoPanel->Width = INFO_PANEL_LARGE_WIDTH;

        InfoSizeChangePanel->Alignment = taRightJustify;
        InfoSizeChangePanel->Caption   = "�";
    }

}


void TTesTORKMgrMainForm::InitializeSidebar( void )
{
    // Set the default sidebar settings
    NavPanelState = eNPS_Pinned;
    m_dwNavPanelHideTimer = 0;
}


void TTesTORKMgrMainForm::UpdateNavPanelSlider( void )
{
    // If we are in auto-hide mode and the mouse is anywhere over the Nav Panel,
    // then reset the auto-hide timer. Note that we only auto-hide if the Nav
    // Panel is floating (eg, not aligned to right)
    if( AutoHideSidebarItem->Checked && ( NavPanelState != eNPS_Pinned ) )
    {
        // Get mouse position - this is in global coords
        TPoint ptMouse = Mouse->CursorPos;

        // Determine if the mouse is over the Nav Panel
        TPoint ptPanel = NavPanel->ScreenToClient( ptMouse );

        if( ( ptPanel.X > 0 ) && ( ptPanel.Y > 0 ) && ( ptPanel.X < NavPanel->Width ) && ( ptPanel.Y < NavPanel->Height ) )
        {
            // We are in the panel, reset the timer. Make sure it is expanded.
            ExpandSidebar();
        }
        else
        {
            // We are outside of the panel - if the timer has expired
            // then hide it
            if( HaveTimeout( m_dwNavPanelHideTimer, 10000 ) )
                ShrinkSidebar();
        }
    }
    else
    {
        // In the pinned state, just keep resetting the timer
        m_dwNavPanelHideTimer = GetTickCount();
    }
}


void __fastcall TTesTORKMgrMainForm::NavPanelMouseEnter(TObject *Sender)
{
    // When the mouse enters the nav panel area, immediately show it
    // if hiding or hidden
    if( ( NavPanelState == eNPS_Hiding ) || ( NavPanelState == eNPS_Hidden ) )
    {
        ExpandSidebar();
    }
}


void TTesTORKMgrMainForm::SetNavPanelState( eNavPanelState eNewState )
{
    switch( eNewState )
    {
        case eNPS_Visible:
        case eNPS_Showing:

            PinButton->Caption      = "Click to Pin";
            PinSidebarItem->Caption = "Pin Sidebar";

            ExpandShrinkItem->Caption = "Shrink Sidebar";
            ExpandShrinkItem->Enabled = true;

            ShrinkButton->Caption = "  �";
            ShrinkButton->Enabled = true;
            ShrinkButton->Visible = true;
            break;

        case eNPS_Hiding:
        case eNPS_Hidden:

            PinButton->Caption      = "Click to Pin";
            PinSidebarItem->Caption = "Pin Sidebar";

            ExpandShrinkItem->Caption = "Expand Sidebar";
            ExpandShrinkItem->Enabled = true;

            ShrinkButton->Caption = "  �";
            ShrinkButton->Enabled = true;
            ShrinkButton->Visible = true;
            break;

        default:
            // Assume we are pinned in this state
            eNewState = eNPS_Pinned;

            PinButton->Caption      = "Click to Float";
            PinSidebarItem->Caption = "Float Sidebar";

            ExpandShrinkItem->Caption = "Shrink Sidebar";
            ExpandShrinkItem->Enabled = false;

            ShrinkButton->Caption = "  �";
            ShrinkButton->Enabled = false;
            ShrinkButton->Visible = false;
            break;
    }

    m_eNavPanelState = eNewState;
}


void __fastcall TTesTORKMgrMainForm::PinButtonClick(TObject *Sender)
{
    if( NavPanelState == eNPS_Pinned )
    {
        // Make sure we're correctly positioned on the screen
        NavPanel->Align  = alNone;
        NavPanel->Left   = TesTORKMgrMainForm->ClientWidth - NavPanel->Width;
        NavPanel->Height = ClientHeight - StatusBar->Height;

        PinnedNavSpacerPanel->Visible = true;
        HorzSplitter->Visible         = false;

        // Make sure the nav spacer is always at the back
        PinnedNavSpacerPanel->SendToBack();

        NavPanelState = eNPS_Visible;
    }
    else
    {
        // Set out state first in case we're in a show/hide loop.
        // That will cause the loop to exit
        NavPanelState = eNPS_Pinned;

        PinnedNavSpacerPanel->Visible = false;
        HorzSplitter->Visible         = true;

        NavPanel->Align = alRight;

        HorzSplitter->Left = NavPanel->Left - HorzSplitter->Width;
    }
}


void __fastcall TTesTORKMgrMainForm::WMShowHideNavPanel( TMessage &Message )
{
    // This message is posted when the Nav Panel should be shrunk or
    // revealed. This message is only valid if the Nav Panel is not
    // in the pinned state. The WParam tells us if we should show
    // or hide the panel (0 == hide else show).

    // Set message result to zero regardless of the actions below
    Message.Result = 0;

    if( NavPanelState == eNPS_Pinned )
        return;

    // If the panel state is showing or hiding, then another message has
    // been posted potentially asking us to change directions. In that
    // case, change our state and exit immediately. Also note that a
    // request to hide will never override a current action of showing.
    bool bHidePanel = ( Message.WParam == 0 );

    if( bHidePanel )
    {
        if( NavPanelState != eNPS_Visible )
            return;

        // We are visible, so hide the panel
        NavPanelState = eNPS_Hiding;
    }
    else
    {
       // Want to show the panel, do so if hiding or hidden. Otherwise
       // do nothing
       if( ( NavPanelState == eNPS_Hiding ) || ( NavPanelState == eNPS_Hidden ) )
           NavPanelState = eNPS_Showing;
       else
           return;
    }

    // Fall through means we need to hide or show the panel. Do this
    // in an Application->ProcessMsgs() spin loop.
    DWORD dwAnimationTick = GetTickCount();

    int iTargetLeftOnShow = ClientWidth - GetCtrlSize( CN_MAIN_PG_CTRL );
    int iTargetLeftOnHide = PinnedNavSpacerPanel->Left;

    const int PanelLeftStepChange = 15;

    while( ( NavPanelState == eNPS_Showing ) || ( NavPanelState == eNPS_Hiding ) )
    {
        // If the state has changed to pinned, bail immediately
        if( NavPanelState == eNPS_Pinned )
            break;

        while( !HaveTimeout( dwAnimationTick, 10 ) )
            Application->ProcessMessages();

        dwAnimationTick = GetTickCount();

        if( NavPanelState == eNPS_Hiding )
        {
            if( NavPanel->Left + PanelLeftStepChange > iTargetLeftOnHide )
            {
                NavPanel->Left = iTargetLeftOnHide;

                NavPanelState = eNPS_Hidden;
                break;
            }
            else
            {
                NavPanel->Left += PanelLeftStepChange;
            }
        }
        else
        {
            if( NavPanel->Left - PanelLeftStepChange < iTargetLeftOnShow )
            {
                NavPanel->Left = iTargetLeftOnShow;

                NavPanelState = eNPS_Visible;
                break;
            }
            else
            {
                NavPanel->Left -= PanelLeftStepChange;
            }
        }

        Application->ProcessMessages();
    }
}


void __fastcall TTesTORKMgrMainForm::AutoHideSidebarItemClick(TObject *Sender)
{
    // If we're currently shrinking, stop and reset the sidebar
    if( m_eNavPanelState == eNPS_Hiding )
        ExpandSidebar();
}


void __fastcall TTesTORKMgrMainForm::ShrinkButtonClick(TObject *Sender)
{
    if( ( m_eNavPanelState == eNPS_Visible ) || ( m_eNavPanelState == eNPS_Showing ) )
        ShrinkSidebar();
    else if( ( m_eNavPanelState == eNPS_Hidden ) || ( m_eNavPanelState == eNPS_Hiding ) )
        ExpandSidebar();
}


void TTesTORKMgrMainForm::ExpandSidebar( void )
{
    ::PostMessage( Handle, WM_SHOW_HIDE_NAV_PANEL, 1, 0 );

    m_dwNavPanelHideTimer = GetTickCount();
}


void TTesTORKMgrMainForm::ShrinkSidebar( void )
{
    ::PostMessage( Handle, WM_SHOW_HIDE_NAV_PANEL, 0, 0 );
}


