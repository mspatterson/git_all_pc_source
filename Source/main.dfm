object TesTORKMgrMainForm: TTesTORKMgrMainForm
  Left = 0
  Top = 0
  Anchors = [akLeft, akRight, akBottom]
  Caption = 'TESCO TesTORK Wireless Torque Turn System'
  ClientHeight = 729
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 768
  Constraints.MinWidth = 1024
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = HiddenPopUp
  Position = poDesigned
  WindowState = wsMaximized
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object HorzSplitter: TSplitter
    Left = 705
    Top = 0
    Height = 710
    Align = alRight
    Color = clGray
    ParentColor = False
    OnMoved = HorzSplitterMoved
    ExplicitLeft = 510
    ExplicitHeight = 687
  end
  object InfoPanel: TPanel
    Left = 0
    Top = 0
    Width = 185
    Height = 710
    Align = alLeft
    BevelKind = bkSoft
    BevelOuter = bvNone
    BorderStyle = bsSingle
    ParentBackground = False
    TabOrder = 0
    DesignSize = (
      177
      702)
    object ConnGB: TGroupBox
      Left = 7
      Top = 15
      Width = 162
      Height = 75
      Caption = ' Connection Tally '
      TabOrder = 0
      object Label5: TLabel
        Left = 11
        Top = 24
        Width = 45
        Height = 13
        Caption = '# Passed'
      end
      object Label6: TLabel
        Left = 11
        Top = 49
        Width = 29
        Height = 13
        Caption = 'Depth'
      end
      object NbrPassedEdit: TPanel
        Left = 74
        Top = 21
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        ParentBackground = False
        TabOrder = 0
      end
      object TotLengthEdit: TPanel
        Left = 74
        Top = 46
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 1
      end
    end
    object TorqueGB: TGroupBox
      Left = 7
      Top = 138
      Width = 162
      Height = 226
      Caption = ' Torque '
      TabOrder = 2
      object Label7: TLabel
        Left = 11
        Top = 24
        Width = 20
        Height = 13
        Caption = 'Max'
      end
      object Label8: TLabel
        Left = 11
        Top = 49
        Width = 23
        Height = 13
        Caption = 'Peak'
      end
      object Label11: TLabel
        Left = 11
        Top = 74
        Width = 36
        Height = 13
        Caption = 'Optimal'
      end
      object Label15: TLabel
        Left = 11
        Top = 124
        Width = 47
        Height = 13
        Caption = 'Shldr Max'
      end
      object Label1: TLabel
        Left = 11
        Top = 149
        Width = 42
        Height = 13
        Caption = 'Shoulder'
      end
      object Label14: TLabel
        Left = 11
        Top = 174
        Width = 43
        Height = 13
        Caption = 'Shldr Min'
      end
      object Label16: TLabel
        Left = 11
        Top = 199
        Width = 25
        Height = 13
        Caption = 'Delta'
      end
      object Label17: TLabel
        Left = 11
        Top = 99
        Width = 16
        Height = 13
        Caption = 'Min'
      end
      object TqMaxEdit: TPanel
        Left = 74
        Top = 21
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 0
      end
      object TqPeakEdit: TPanel
        Left = 74
        Top = 46
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 1
      end
      object TqOptEdit: TPanel
        Left = 74
        Top = 71
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 2
      end
      object TqMinEdit: TPanel
        Left = 74
        Top = 96
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 3
      end
      object ShldrMaxEdit: TPanel
        Left = 74
        Top = 121
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 4
      end
      object ShldrEdit: TPanel
        Left = 74
        Top = 146
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 5
      end
      object ShldrMinEdit: TPanel
        Left = 74
        Top = 171
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 6
      end
      object TqDeltaEdit: TPanel
        Left = 74
        Top = 196
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 7
      end
    end
    object TurnsGB: TGroupBox
      Left = 7
      Top = 366
      Width = 162
      Height = 151
      Caption = ' Turns '
      TabOrder = 3
      object Label9: TLabel
        Left = 11
        Top = 24
        Width = 20
        Height = 13
        Caption = 'Max'
      end
      object Label10: TLabel
        Left = 11
        Top = 49
        Width = 37
        Height = 13
        Caption = 'At Peak'
      end
      object Label12: TLabel
        Left = 11
        Top = 74
        Width = 16
        Height = 13
        Caption = 'Min'
      end
      object Label18: TLabel
        Left = 11
        Top = 99
        Width = 38
        Height = 13
        Caption = 'At Shldr'
      end
      object Label19: TLabel
        Left = 11
        Top = 124
        Width = 48
        Height = 13
        Caption = 'Post Shldr'
      end
      object TurnsMaxEdit: TPanel
        Left = 74
        Top = 21
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 0
      end
      object TurnsAtPeakEdit: TPanel
        Left = 74
        Top = 46
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 1
      end
      object TurnsMinEdit: TPanel
        Left = 74
        Top = 71
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 2
      end
      object TurnsAtShldrEdit: TPanel
        Left = 74
        Top = 96
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 3
      end
      object PostShldrEdit: TPanel
        Left = 74
        Top = 121
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 4
      end
    end
    object SectNbrGB: TGroupBox
      Left = 7
      Top = 92
      Width = 162
      Height = 44
      TabOrder = 1
      object Label20: TLabel
        Left = 11
        Top = 17
        Width = 46
        Height = 13
        Caption = 'Section #'
      end
      object SectNbrEdit: TPanel
        Left = 74
        Top = 14
        Width = 79
        Height = 21
        BevelKind = bkSoft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 0
      end
    end
    object InterlockStatusGB: TGroupBox
      Left = 7
      Top = 520
      Width = 162
      Height = 65
      Caption = ' Interlock '
      TabOrder = 4
      object CDSLB: TLabel
        Left = 33
        Top = 16
        Width = 20
        Height = 13
        Caption = 'CDS'
      end
      object SlipsLB: TLabel
        Left = 107
        Top = 16
        Width = 21
        Height = 13
        Caption = 'Slips'
      end
      object CDSPanel: TPanel
        Left = 11
        Top = 31
        Width = 68
        Height = 25
        BevelKind = bkTile
        BevelOuter = bvNone
        Caption = 'Disabled'
        TabOrder = 0
      end
      object SlipsPanel: TPanel
        Left = 85
        Top = 31
        Width = 68
        Height = 25
        BevelKind = bkTile
        BevelOuter = bvNone
        Caption = 'Disabled'
        TabOrder = 1
      end
    end
    object InfoSizeChangePanel: TPanel
      Left = 7
      Top = 2
      Width = 162
      Height = 15
      Alignment = taRightJustify
      BevelOuter = bvNone
      Caption = #171
      TabOrder = 5
      OnClick = InfoSizeChangePanelClick
    end
    object PDSPipeGB: TGroupBox
      Left = 7
      Top = 588
      Width = 162
      Height = 53
      Caption = ' PDS Pipe '
      TabOrder = 6
      object PDSStatePanel: TPanel
        Left = 11
        Top = 18
        Width = 142
        Height = 25
        BevelKind = bkTile
        BevelOuter = bvNone
        Caption = 'PDS Pipe State'
        TabOrder = 0
      end
    end
    object AutoRecStatusGB: TGroupBox
      Left = 7
      Top = 555
      Width = 162
      Height = 53
      Anchors = [akLeft, akBottom]
      Caption = ' Auto-Record Status '
      TabOrder = 7
      object AutoRecStatusPanel: TPanel
        Left = 11
        Top = 18
        Width = 142
        Height = 25
        BevelKind = bkTile
        BevelOuter = bvNone
        Caption = 'Disabled'
        TabOrder = 0
      end
    end
  end
  object GraphPanel: TPanel
    Left = 185
    Top = 0
    Width = 520
    Height = 710
    Align = alClient
    BevelOuter = bvNone
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 1
    object VertSplitter: TSplitter
      Left = 0
      Top = 300
      Width = 520
      Height = 3
      Cursor = crVSplit
      Align = alTop
      Color = clGray
      ParentColor = False
      OnMoved = VertSplitterMoved
      ExplicitLeft = -7
      ExplicitTop = 280
      ExplicitWidth = 640
    end
    object TurnsChart: TChart
      Left = 0
      Top = 0
      Width = 520
      Height = 300
      AllowPanning = pmNone
      BackImage.Inside = True
      BackWall.Pen.Width = 2
      BackWall.Size = 1
      BottomWall.Size = 1
      BottomWall.Visible = False
      Foot.Visible = False
      LeftWall.Size = 1
      LeftWall.Visible = False
      Legend.Visible = False
      MarginBottom = 5
      MarginLeft = 15
      MarginRight = 15
      MarginTop = 15
      MarginUnits = muPixels
      RightWall.Color = clBlack
      RightWall.Size = 1
      ScrollMouseButton = mbLeft
      SubFoot.Visible = False
      SubFoot.VertMargin = 0
      SubTitle.Visible = False
      Title.Alignment = taLeftJustify
      Title.Font.Color = clGray
      Title.Font.Style = [fsBold]
      Title.Font.Emboss.Color = clFuchsia
      Title.Text.Strings = (
        'Torque')
      Title.Visible = False
      AxisBehind = False
      BottomAxis.Automatic = False
      BottomAxis.AutomaticMaximum = False
      BottomAxis.AutomaticMinimum = False
      BottomAxis.ExactDateTime = False
      BottomAxis.Increment = 1.000000000000000000
      BottomAxis.Maximum = 10.100000000000000000
      BottomAxis.MinorGrid.Color = clSilver
      BottomAxis.MinorGrid.Style = psDot
      BottomAxis.MinorGrid.Visible = True
      BottomAxis.MinorTickCount = 1
      BottomAxis.EndPosition = 95.000000000000000000
      BottomAxis.Title.Caption = 'Turns'
      BottomAxis.Title.Font.Color = clGray
      BottomAxis.Title.Font.Style = [fsBold]
      BottomAxis.Title.Color = clGray
      CustomAxes = <
        item
          Automatic = False
          AutomaticMaximum = False
          AutomaticMinimum = False
          Axis.Visible = False
          ExactDateTime = False
          Grid.Visible = False
          Horizontal = False
          OtherSide = False
          LabelsOnAxis = False
          LabelsSeparation = 0
          LabelsSize = 6
          Maximum = 10000.000000000000000000
          Minimum = -10000.000000000000000000
          MinorTicks.Visible = False
          PositionPercent = 99.000000000000000000
          TickInnerLength = 3
          Ticks.Width = 0
          Ticks.Visible = False
          Title.Caption = 'Tension'
          Title.Font.Color = 33023
          Title.Font.Style = [fsBold]
          ZPosition = 100.000000000000000000
        end>
      Frame.Width = 2
      LeftAxis.Automatic = False
      LeftAxis.AutomaticMaximum = False
      LeftAxis.AutomaticMinimum = False
      LeftAxis.ExactDateTime = False
      LeftAxis.Increment = 1.000000000000000000
      LeftAxis.Maximum = 16000.000000000000000000
      LeftAxis.Title.Caption = 'Torque'
      LeftAxis.Title.Font.Color = 8404992
      LeftAxis.Title.Font.Style = [fsBold]
      RightAxis.Automatic = False
      RightAxis.AutomaticMaximum = False
      RightAxis.AutomaticMinimum = False
      RightAxis.Grid.Visible = False
      RightAxis.Maximum = 40.000000000000000000
      RightAxis.MinorTicks.Visible = False
      RightAxis.Title.Angle = 90
      RightAxis.Title.Caption = 'RPM'
      RightAxis.Title.Font.Color = 16384
      RightAxis.Title.Font.Style = [fsBold]
      TopAxis.Visible = False
      View3D = False
      Zoom.Allow = False
      Zoom.UpLeftZooms = True
      OnAfterDraw = TurnsChartAfterDraw
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      OnMouseDown = ChartMouseDown
      OnMouseMove = ChartMouseMove
      DesignSize = (
        520
        300)
      DefaultCanvas = 'TGDIPlusCanvas'
      PrintMargins = (
        15
        8
        15
        8)
      ColorPaletteIndex = 13
      object TorqueBtnPanel: TPanel
        Left = 259
        Top = 17
        Width = 163
        Height = 44
        Anchors = [akTop, akRight]
        BevelInner = bvLowered
        BevelKind = bkTile
        BevelWidth = 2
        ShowCaption = False
        TabOrder = 0
        object TqGraphRPMTenBtn: TSpeedButton
          Left = 7
          Top = 8
          Width = 26
          Height = 26
          Hint = 'Toggle Right Axis'
          AllowAllUp = True
          GroupIndex = 1
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00337333733373
            3373337F3F7F3F7F3F7F33737373737373733F7F7F7F7F7F7F7F770000000000
            000077777777777777773303333333333333337FF333333F33333709333333C3
            333337773F3FF373F333330393993C3C33333F7F7F77F7F7FFFF77079797977C
            77777777777777777777330339339333C333337FF73373F37F33370C333C3933
            933337773F3737F37FF33303C3C33939C9333F7F7F7FF7F777FF7707C7C77797
            7C97777777777777777733033C3333333C33337F37F33333373F37033C333333
            33C3377F37333333337333033333333333333F7FFFFFFFFFFFFF770777777777
            7777777777777777777733333333333333333333333333333333}
          Margin = 1
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = TqGraphRPMTenBtnClick
        end
        object TqGraphPanBtn: TSpeedButton
          Left = 37
          Top = 8
          Width = 26
          Height = 26
          Hint = 'Enable Panning'
          AllowAllUp = True
          GroupIndex = 3
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333337773F333333333330FFF03333333333733373F3333333330FFFFF03
            33333337F33337F3333333330FFFFF0333333337F33337F3333333330FFFFF03
            33333337FFFFF7F3333333330777770333333337777777F3333333330FF7FF03
            33333337F37F37F3333333330FF7FF03333333373F7FF7333333333330000033
            33333333777773FFF33333333330330007333333337F37777F33333333303033
            307333333373733377F33333333303333303333333F7F33337F3333333330733
            330333333F777F3337F333333370307330733333F77377FF7733333337033300
            0733333F77333777733333337033333333333337733333333333}
          Margin = 1
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = GraphPanBtnClick
        end
        object TqGraphZmOutBtn: TSpeedButton
          Left = 97
          Top = 8
          Width = 26
          Height = 26
          Hint = 'Zoom Out'
          GroupIndex = 4
          Enabled = False
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            333333773337777333333078F8F87033333337F3333337F33333778F8F8F8773
            333337333333373F333307F8F8F8F70333337F33FFFFF37F3333078999998703
            33337F377777337F333307F8F8F8F703333373F3333333733333778F8F8F8773
            333337F3333337F333333078F8F870333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          Margin = 1
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = GraphZmOutBtnClick
        end
        object TqGraphZmInBtn: TSpeedButton
          Left = 67
          Top = 8
          Width = 26
          Height = 26
          Hint = 'Enable Zoom Mode'
          AllowAllUp = True
          GroupIndex = 3
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
            3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
            33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
            333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = GraphZmInBtnClick
        end
        object GraphSettingBtn: TSpeedButton
          Left = 127
          Top = 8
          Width = 26
          Height = 26
          Hint = 'Graph Settings'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555550FF0559
            1950555FF75F7557F7F757000FF055591903557775F75557F77570FFFF055559
            1933575FF57F5557F7FF0F00FF05555919337F775F7F5557F7F700550F055559
            193577557F7F55F7577F07550F0555999995755575755F7FFF7F5570F0755011
            11155557F755F777777555000755033305555577755F75F77F55555555503335
            0555555FF5F75F757F5555005503335505555577FF75F7557F55505050333555
            05555757F75F75557F5505000333555505557F777FF755557F55000000355557
            07557777777F55557F5555000005555707555577777FF5557F55553000075557
            0755557F7777FFF5755555335000005555555577577777555555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = GraphSettingBtnClick
        end
      end
      object SysStatusPanel: TPanel
        Left = 34
        Top = 67
        Width = 163
        Height = 44
        BevelInner = bvLowered
        BevelKind = bkTile
        BevelWidth = 2
        ShowCaption = False
        TabOrder = 1
        DesignSize = (
          159
          40)
        object SysStatusPaintBox: TPaintBox
          Left = 95
          Top = 4
          Width = 60
          Height = 32
          Align = alRight
          Color = clBtnFace
          ParentColor = False
          ParentShowHint = False
          ShowHint = True
          OnPaint = SysStatusPaintBoxPaint
          ExplicitLeft = 123
        end
        object CommsProgBar: TAdvSmoothProgressBar
          Left = 9
          Top = 8
          Width = 72
          Height = 25
          Step = 10.000000000000000000
          Maximum = 100.000000000000000000
          Position = 50.000000000000000000
          GlowAnimation = False
          ProgressAnimation = False
          Appearance.Transparent = True
          Appearance.Shadows = False
          Appearance.Overlays = False
          Appearance.BackGroundFill.Color = 16250613
          Appearance.BackGroundFill.ColorTo = 16250613
          Appearance.BackGroundFill.ColorMirror = clNone
          Appearance.BackGroundFill.ColorMirrorTo = clNone
          Appearance.BackGroundFill.GradientType = gtVertical
          Appearance.BackGroundFill.GradientMirrorType = gtSolid
          Appearance.BackGroundFill.BorderColor = 15000546
          Appearance.BackGroundFill.Rounding = 0
          Appearance.BackGroundFill.ShadowOffset = 0
          Appearance.BackGroundFill.Glow = gmNone
          Appearance.ProgressFill.Color = clLime
          Appearance.ProgressFill.ColorTo = clBlue
          Appearance.ProgressFill.ColorMirror = clNone
          Appearance.ProgressFill.ColorMirrorTo = clNone
          Appearance.ProgressFill.GradientType = gtHorizontal
          Appearance.ProgressFill.GradientMirrorType = gtVertical
          Appearance.ProgressFill.BorderColor = clNone
          Appearance.ProgressFill.Rounding = 0
          Appearance.ProgressFill.ShadowOffset = 0
          Appearance.ProgressFill.Glow = gmNone
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -12
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Appearance.ProgressFont.Charset = DEFAULT_CHARSET
          Appearance.ProgressFont.Color = clWindowText
          Appearance.ProgressFont.Height = -12
          Appearance.ProgressFont.Name = 'Tahoma'
          Appearance.ProgressFont.Style = []
          Appearance.ValueFormat = 'Base Radio'
          Appearance.ValueVisible = True
          Version = '1.8.0.0'
          Style = pbstMarquee
          MarqueeInterval = 50
          MarqueeSize = 75
          Anchors = [akLeft, akTop, akRight]
          ParentShowHint = False
          ShowHint = True
          TabStop = False
        end
      end
      object TorqueByTurnsSeries: TLineSeries
        SeriesColor = 8404992
        Brush.BackColor = clDefault
        LinePen.Width = 3
        Pointer.Brush.Color = clRed
        Pointer.Brush.BackColor = clRed
        Pointer.Brush.Gradient.EndColor = clRed
        Pointer.Dark3D = False
        Pointer.Draw3D = False
        Pointer.Gradient.EndColor = clRed
        Pointer.HorizSize = 8
        Pointer.InflateMargins = False
        Pointer.Pen.Color = clRed
        Pointer.Pen.Width = 2
        Pointer.Pen.EndStyle = esFlat
        Pointer.Style = psStar
        Pointer.VertSize = 8
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          0019000000000000000000000000000000000014400000000000001440000000
          0000001C4000000000000024400000000000002E400000000000003940000000
          0000003940000000000000344000000000000024400000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000}
      end
      object RPMByTurnsSeries: TLineSeries
        SeriesColor = 16384
        Title = 'RPMByTurnsSeries'
        VertAxis = aRightAxis
        Brush.BackColor = clDefault
        LinePen.Width = 2
        Pointer.Brush.Gradient.EndColor = 16384
        Pointer.Gradient.EndColor = 16384
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          000A000000000000000000000000000000000000400000000000001040000000
          0000002040000000000000204000000000000020400000000000001840000000
          0000001440000000000000F03F0000000000000000}
      end
      object TenByTurnsSeries: TLineSeries
        SeriesColor = 33023
        Title = 'TensionByTurnsSeries'
        VertAxis = aCustomVertAxis
        Brush.BackColor = clDefault
        Pointer.HorizSize = 6
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.VertSize = 6
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        CustomVertAxis = 0
        Data = {
          000A000000000000000000000000000000000000400000000000002440000000
          000000284000000000000024400000000000002C400000000000002C40000000
          0000002C4000000000000020400000000000001840}
      end
    end
    object TimeChart: TChart
      Left = 0
      Top = 303
      Width = 520
      Height = 407
      AllowPanning = pmNone
      BackWall.Pen.Width = 2
      Foot.Visible = False
      Legend.Visible = False
      MarginBottom = 5
      MarginLeft = 15
      MarginRight = 15
      MarginTop = 15
      MarginUnits = muPixels
      SubFoot.Visible = False
      SubTitle.Visible = False
      Title.Alignment = taLeftJustify
      Title.Font.Color = clGray
      Title.Font.Style = [fsBold]
      Title.Text.Strings = (
        'Turns')
      Title.Visible = False
      Title.VertMargin = 1
      BottomAxis.Automatic = False
      BottomAxis.AutomaticMaximum = False
      BottomAxis.AutomaticMinimum = False
      BottomAxis.Maximum = 30.000000000000000000
      BottomAxis.EndPosition = 95.000000000000000000
      BottomAxis.Title.Caption = 'Time'
      BottomAxis.Title.Font.Color = clGray
      BottomAxis.Title.Font.Style = [fsBold]
      BottomAxis.Title.ShapeStyle = fosRoundRectangle
      CustomAxes = <
        item
          Automatic = False
          AutomaticMaximum = False
          AutomaticMinimum = False
          Axis.Visible = False
          Grid.Visible = False
          Horizontal = False
          OtherSide = False
          LabelsOnAxis = False
          LabelsSeparation = 0
          LabelsSize = 6
          Maximum = 10000.000000000000000000
          Minimum = -10000.000000000000000000
          PositionPercent = 99.000000000000000000
          TickInnerLength = 3
          Ticks.Visible = False
          Title.Caption = 'Tension'
          Title.Font.Color = 33023
          Title.Font.Style = [fsBold]
        end>
      Frame.Width = 2
      LeftAxis.Automatic = False
      LeftAxis.AutomaticMaximum = False
      LeftAxis.AutomaticMinimum = False
      LeftAxis.ExactDateTime = False
      LeftAxis.Increment = 2.000000000000000000
      LeftAxis.Maximum = 16000.000000000000000000
      LeftAxis.Title.Caption = 'Torque'
      LeftAxis.Title.Font.Color = 8404992
      LeftAxis.Title.Font.Style = [fsBold]
      RightAxis.Automatic = False
      RightAxis.AutomaticMaximum = False
      RightAxis.AutomaticMinimum = False
      RightAxis.Grid.Visible = False
      RightAxis.Maximum = 40.000000000000000000
      RightAxis.Title.Angle = 90
      RightAxis.Title.Caption = 'RPM'
      RightAxis.Title.Font.Color = 16384
      RightAxis.Title.Font.Style = [fsBold]
      TopAxis.Visible = False
      View3D = False
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      OnMouseDown = ChartMouseDown
      OnMouseMove = ChartMouseMove
      DesignSize = (
        520
        407)
      DefaultCanvas = 'TGDIPlusCanvas'
      ColorPaletteIndex = 13
      object TimeBtnPanel: TPanel
        Left = 317
        Top = 19
        Width = 105
        Height = 44
        Anchors = [akTop, akRight]
        BevelInner = bvLowered
        BevelKind = bkTile
        BevelWidth = 2
        ShowCaption = False
        TabOrder = 0
        object TimeGraphPanBtn: TSpeedButton
          Left = 6
          Top = 8
          Width = 26
          Height = 26
          Hint = 'Enable Panning'
          AllowAllUp = True
          GroupIndex = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333337773F333333333330FFF03333333333733373F3333333330FFFFF03
            33333337F33337F3333333330FFFFF0333333337F33337F3333333330FFFFF03
            33333337FFFFF7F3333333330777770333333337777777F3333333330FF7FF03
            33333337F37F37F3333333330FF7FF03333333373F7FF7333333333330000033
            33333333777773FFF33333333330330007333333337F37777F33333333303033
            307333333373733377F33333333303333303333333F7F33337F3333333330733
            330333333F777F3337F333333370307330733333F77377FF7733333337033300
            0733333F77333777733333337033333333333337733333333333}
          Margin = 1
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = GraphPanBtnClick
        end
        object TimeGraphZmOutBtn: TSpeedButton
          Left = 68
          Top = 8
          Width = 26
          Height = 26
          Hint = 'Zoom Out'
          AllowAllUp = True
          GroupIndex = 3
          Enabled = False
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            333333773337777333333078F8F87033333337F3333337F33333778F8F8F8773
            333337333333373F333307F8F8F8F70333337F33FFFFF37F3333078999998703
            33337F377777337F333307F8F8F8F703333373F3333333733333778F8F8F8773
            333337F3333337F333333078F8F870333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          Margin = 1
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = GraphZmOutBtnClick
        end
        object TimeGraphZmInBtn: TSpeedButton
          Left = 38
          Top = 8
          Width = 26
          Height = 26
          Hint = 'Enable Zoom Mode'
          AllowAllUp = True
          GroupIndex = 2
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33033333333333333F7F3333333333333000333333333333F777333333333333
            000333333333333F777333333333333000333333333333F77733333333333300
            033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
            33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
            3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
            33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
            333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
            333333773FF77333333333370007333333333333777333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = GraphZmInBtnClick
        end
      end
      object TorqueByTimeSeries: TLineSeries
        SeriesColor = 8404992
        Title = 'TorqueSeries'
        Brush.BackColor = clDefault
        LinePen.Width = 3
        Pointer.Brush.Gradient.EndColor = 8404992
        Pointer.Gradient.EndColor = 8404992
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          00190000000000000000000000000000000000E03F000000000000E83F000000
          000000F03F000000000000F83F00000000000000400000000000000040000000
          0000000240000000000000024000000000000000400000000000000440000000
          0000000840000000000000084000000000000008400000000000001240000000
          0000001240000000000000124000000000000012400000000000001240000000
          0000001240000000000000124000000000000000000000000000000000000000
          00000000000000000000000000}
      end
      object RPMByTimeSeries: TLineSeries
        SeriesColor = 16384
        Title = 'TurnsSeries'
        VertAxis = aRightAxis
        Brush.BackColor = clDefault
        LinePen.Width = 2
        Pointer.Brush.Gradient.EndColor = clGreen
        Pointer.Gradient.EndColor = clGreen
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Y'
        YValues.Order = loNone
        Data = {
          000A0000000000000000000000000000000000E03F000000000000F83F000000
          0000000440000000000000104000000000000018400000000000001C40000000
          000000214000000000000021400000000000002140}
      end
      object TenByTimeSeries: TLineSeries
        SeriesColor = 33023
        Title = 'TensionSeries'
        VertAxis = aCustomVertAxis
        Brush.BackColor = clDefault
        Pointer.HorizSize = 6
        Pointer.InflateMargins = True
        Pointer.Style = psRectangle
        Pointer.VertSize = 6
        XValues.Name = 'X'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loNone
        CustomVertAxis = 0
        Data = {
          000A000000000000000000004000000000000010400000000000001440000000
          000000184000000000000018400000000000001C400000000000002040000000
          0000001C4000000000000018400000000000001840}
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 710
    Width = 1008
    Height = 19
    Panels = <
      item
        Width = 600
      end
      item
        Width = 50
      end>
  end
  object NavPanel: TPanel
    Left = 708
    Top = 0
    Width = 270
    Height = 710
    Align = alRight
    TabOrder = 3
    OnMouseEnter = NavPanelMouseEnter
    object NavPgCtrl: TPageControl
      Left = 1
      Top = 21
      Width = 268
      Height = 688
      ActivePage = HWSheet
      Align = alClient
      DoubleBuffered = False
      ParentDoubleBuffered = False
      PopupMenu = NavPgPopUp
      Style = tsButtons
      TabOrder = 0
      object ConnsSheet: TTabSheet
        Caption = 'Connections'
        DoubleBuffered = False
        ParentDoubleBuffered = False
        DesignSize = (
          260
          657)
        object NewSectBtn: TButton
          Left = 222
          Top = 414
          Width = 26
          Height = 40
          Anchors = [akLeft, akBottom]
          Caption = 'New Section'
          TabOrder = 1
          Visible = False
          OnClick = NewSectBtnClick
        end
        object ConnsLV: TListView
          Left = 0
          Top = 0
          Width = 260
          Height = 373
          Align = alTop
          Anchors = [akLeft, akTop, akRight, akBottom]
          Columns = <
            item
              Caption = 'Conn'
            end
            item
              Caption = 'Seq'
              Width = 45
            end
            item
              Caption = 'Sect'
              Width = 45
            end
            item
              Caption = 'Result'
              Width = 90
            end>
          HideSelection = False
          ReadOnly = True
          RowSelect = True
          SortType = stData
          TabOrder = 0
          ViewStyle = vsReport
          OnSelectItem = ConnsLVSelectItem
        end
        object ControlGB: TGroupBox
          Left = 0
          Top = 485
          Width = 260
          Height = 172
          Align = alBottom
          Caption = ' Control '
          DoubleBuffered = False
          ParentDoubleBuffered = False
          TabOrder = 3
          DesignSize = (
            260
            172)
          object ConnCtrlBtn: TAdvSmoothToggleButton
            Left = 13
            Top = 18
            Width = 234
            Height = 44
            AllowAllUp = True
            BorderColor = clBlue
            BorderInnerColor = clBlue
            AutoToggle = False
            Appearance.Font.Charset = DEFAULT_CHARSET
            Appearance.Font.Color = clWindowText
            Appearance.Font.Height = -15
            Appearance.Font.Name = 'Tahoma'
            Appearance.Font.Style = []
            Caption = 'Connection'
            Version = '1.4.3.0'
            Status.Caption = '0'
            Status.Appearance.Fill.Color = clRed
            Status.Appearance.Fill.ColorMirror = clNone
            Status.Appearance.Fill.ColorMirrorTo = clNone
            Status.Appearance.Fill.GradientType = gtSolid
            Status.Appearance.Fill.GradientMirrorType = gtSolid
            Status.Appearance.Fill.BorderColor = clGray
            Status.Appearance.Fill.Rounding = 0
            Status.Appearance.Fill.ShadowOffset = 0
            Status.Appearance.Fill.Glow = gmNone
            Status.Appearance.Font.Charset = DEFAULT_CHARSET
            Status.Appearance.Font.Color = clWhite
            Status.Appearance.Font.Height = -11
            Status.Appearance.Font.Name = 'Tahoma'
            Status.Appearance.Font.Style = []
            ParentFont = False
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
            OnClick = ConnCtrlBtnClick
          end
          object CircCtrlBtn: TAdvSmoothToggleButton
            Left = 15
            Top = 69
            Width = 234
            Height = 44
            AllowAllUp = True
            BorderColor = clBlue
            BorderInnerColor = clBlue
            AutoToggle = False
            Appearance.Font.Charset = DEFAULT_CHARSET
            Appearance.Font.Color = clWindowText
            Appearance.Font.Height = -15
            Appearance.Font.Name = 'Tahoma'
            Appearance.Font.Style = []
            Caption = 'Circulating'
            Version = '1.4.3.0'
            Status.Caption = '0'
            Status.Appearance.Fill.Color = clRed
            Status.Appearance.Fill.ColorMirror = clNone
            Status.Appearance.Fill.ColorMirrorTo = clNone
            Status.Appearance.Fill.GradientType = gtSolid
            Status.Appearance.Fill.GradientMirrorType = gtSolid
            Status.Appearance.Fill.BorderColor = clGray
            Status.Appearance.Fill.Rounding = 0
            Status.Appearance.Fill.ShadowOffset = 0
            Status.Appearance.Fill.Glow = gmNone
            Status.Appearance.Font.Charset = DEFAULT_CHARSET
            Status.Appearance.Font.Color = clWhite
            Status.Appearance.Font.Height = -11
            Status.Appearance.Font.Name = 'Tahoma'
            Status.Appearance.Font.Style = []
            ParentFont = False
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 1
            OnClick = CircCtrlBtnClick
          end
          object CommentBtn: TAdvSmoothToggleButton
            Left = 15
            Top = 119
            Width = 234
            Height = 44
            AllowAllUp = True
            BorderColor = clBlue
            BorderInnerColor = clBlue
            AutoToggle = False
            Appearance.Font.Charset = DEFAULT_CHARSET
            Appearance.Font.Color = clWindowText
            Appearance.Font.Height = -15
            Appearance.Font.Name = 'Tahoma'
            Appearance.Font.Style = []
            Caption = 'Job Comment'
            Version = '1.4.3.0'
            Status.Caption = '0'
            Status.Appearance.Fill.Color = clRed
            Status.Appearance.Fill.ColorMirror = clNone
            Status.Appearance.Fill.ColorMirrorTo = clNone
            Status.Appearance.Fill.GradientType = gtSolid
            Status.Appearance.Fill.GradientMirrorType = gtSolid
            Status.Appearance.Fill.BorderColor = clGray
            Status.Appearance.Fill.Rounding = 0
            Status.Appearance.Fill.ShadowOffset = 0
            Status.Appearance.Fill.Glow = gmNone
            Status.Appearance.Font.Charset = DEFAULT_CHARSET
            Status.Appearance.Font.Color = clWhite
            Status.Appearance.Font.Height = -11
            Status.Appearance.Font.Name = 'Tahoma'
            Status.Appearance.Font.Style = []
            ParentFont = False
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 2
            OnClick = CommentBtnClick
          end
        end
        object EditSectBtn: TButton
          Left = 254
          Top = 414
          Width = 24
          Height = 40
          Anchors = [akLeft, akBottom]
          Caption = 'Edit Current Section'
          TabOrder = 2
          Visible = False
          OnClick = EditSectBtnClick
        end
        object ShowConnsBtn: TAdvSmoothToggleButton
          Left = 13
          Top = 383
          Width = 234
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'View All Connections'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 4
          OnClick = ShowConnsBtnClick
        end
        object NewEditSectBtn: TAdvSmoothToggleButton
          Left = 13
          Top = 433
          Width = 234
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'New Section'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 5
          OnClick = NewSectBtnClick
        end
      end
      object PipeTallySheet: TTabSheet
        Caption = 'Pipe Tally'
        DoubleBuffered = False
        ImageIndex = 4
        ParentDoubleBuffered = False
        DesignSize = (
          260
          657)
        object PipeTallyLV: TListView
          Left = 0
          Top = 0
          Width = 260
          Height = 447
          Align = alTop
          Anchors = [akLeft, akTop, akRight, akBottom]
          Columns = <
            item
              Caption = 'Conn'
            end
            item
              Caption = 'Length'
              Width = 45
            end
            item
              Caption = 'Sect'
              Width = 45
            end
            item
              Caption = 'Date/Time'
              Width = 90
            end>
          HideSelection = False
          ReadOnly = True
          RowSelect = True
          SortType = stData
          TabOrder = 0
          ViewStyle = vsReport
          OnSelectItem = PipeTallyLVSelectItem
        end
        object AddPipeInvRecBtn: TAdvSmoothToggleButton
          Left = 13
          Top = 456
          Width = 234
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'Append'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 1
          OnClick = AddPipeInvRecBtnClick
        end
        object InsPipeInvRecBtn: TAdvSmoothToggleButton
          Left = 13
          Top = 506
          Width = 234
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'Insert'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 2
          OnClick = InsPipeInvRecBtnClick
        end
        object ModPipeInvRecBtn: TAdvSmoothToggleButton
          Left = 13
          Top = 556
          Width = 234
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'Modify'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 3
          OnClick = ModPipeInvRecBtnClick
        end
        object DelPipeInvRecBtn: TAdvSmoothToggleButton
          Left = 13
          Top = 607
          Width = 234
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'Delete'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 4
          OnClick = DelPipeInvRecBtnClick
        end
      end
      object RptsSheet: TTabSheet
        Caption = 'Reports'
        ImageIndex = 1
        DesignSize = (
          260
          657)
        object RptGB: TGroupBox
          Left = 7
          Top = 1
          Width = 250
          Height = 226
          Anchors = [akLeft, akTop, akRight]
          Caption = ' Report Selection '
          TabOrder = 0
          DesignSize = (
            250
            226)
          object RptTypeLB: TLabel
            Left = 16
            Top = 32
            Width = 24
            Height = 13
            Caption = 'Type'
          end
          object RptTypeCombo: TComboBox
            Left = 58
            Top = 29
            Width = 177
            Height = 21
            Style = csDropDownList
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
            OnClick = RptTypeComboClick
          end
          object RptRangeGB: TGroupBox
            Left = 16
            Top = 60
            Width = 219
            Height = 105
            Anchors = [akLeft, akTop, akRight]
            Caption = ' Range '
            TabOrder = 1
            DesignSize = (
              219
              105)
            object RptFromLB: TLabel
              Left = 93
              Top = 43
              Width = 24
              Height = 13
              Caption = 'From'
            end
            object RptToLB: TLabel
              Left = 105
              Top = 70
              Width = 12
              Height = 13
              Caption = 'To'
            end
            object RptRangeRB: TRadioButton
              Left = 11
              Top = 43
              Width = 57
              Height = 17
              Caption = 'Range'
              TabOrder = 1
              OnClick = RptRangeRBClick
            end
            object RptAllRB: TRadioButton
              Left = 11
              Top = 21
              Width = 33
              Height = 17
              Caption = 'All'
              Checked = True
              TabOrder = 0
              TabStop = True
              OnClick = RptAllRBClick
            end
            object RptToEdit: TEdit
              Left = 123
              Top = 70
              Width = 77
              Height = 21
              Anchors = [akLeft, akTop, akRight]
              Color = clBtnFace
              NumbersOnly = True
              ReadOnly = True
              TabOrder = 3
            end
            object RptFromEdit: TEdit
              Left = 123
              Top = 43
              Width = 77
              Height = 21
              Anchors = [akLeft, akTop, akRight]
              Color = clBtnFace
              NumbersOnly = True
              ReadOnly = True
              TabOrder = 2
            end
          end
          object RptPreviewBtn: TAdvSmoothToggleButton
            Left = 16
            Top = 172
            Width = 219
            Height = 44
            AllowAllUp = True
            BorderColor = clBlue
            BorderInnerColor = clBlue
            AutoToggle = False
            Appearance.Font.Charset = DEFAULT_CHARSET
            Appearance.Font.Color = clWindowText
            Appearance.Font.Height = -15
            Appearance.Font.Name = 'Tahoma'
            Appearance.Font.Style = []
            Caption = 'Preview'
            Version = '1.4.3.0'
            Status.Caption = '0'
            Status.Appearance.Fill.Color = clRed
            Status.Appearance.Fill.ColorMirror = clNone
            Status.Appearance.Fill.ColorMirrorTo = clNone
            Status.Appearance.Fill.GradientType = gtSolid
            Status.Appearance.Fill.GradientMirrorType = gtSolid
            Status.Appearance.Fill.BorderColor = clGray
            Status.Appearance.Fill.Rounding = 0
            Status.Appearance.Fill.ShadowOffset = 0
            Status.Appearance.Fill.Glow = gmNone
            Status.Appearance.Font.Charset = DEFAULT_CHARSET
            Status.Appearance.Font.Color = clWhite
            Status.Appearance.Font.Height = -11
            Status.Appearance.Font.Name = 'Tahoma'
            Status.Appearance.Font.Style = []
            ParentFont = False
            Anchors = [akLeft, akRight, akBottom]
            TabOrder = 2
            OnClick = RptPreviewBtnClick
          end
        end
      end
      object HWSheet: TTabSheet
        Caption = 'Hardware'
        ImageIndex = 2
        DesignSize = (
          260
          657)
        object CommsStatusView: TValueListEditor
          Left = 3
          Top = 6
          Width = 255
          Height = 323
          Anchors = [akLeft, akTop, akRight, akBottom]
          DrawingStyle = gdsGradient
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goThumbTracking]
          TabOrder = 0
          TitleCaptions.Strings = (
            'Item'
            'Value')
          OnSelectCell = CommsStatusViewSelectCell
          ColWidths = (
            129
            120)
        end
        object ShowDiagDataBtn: TAdvSmoothToggleButton
          Left = 3
          Top = 610
          Width = 254
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'Show Diagostic Data'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 6
          OnClick = ShowDiagDataBtnClick
        end
        object TestAlarmBtn: TAdvSmoothToggleButton
          Left = 3
          Top = 560
          Width = 254
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'Test Alarm'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 5
          OnClick = TestAlarmBtnClick
        end
        object SysSettingsBtn: TAdvSmoothToggleButton
          Left = 3
          Top = 510
          Width = 254
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'System Settings'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 4
          OnClick = SysSettingsBtnClick
        end
        object ILockBypassBtn: TAdvSmoothToggleButton
          Left = 3
          Top = 448
          Width = 254
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'Interlock Bypass'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 3
          OnClick = ILockBypassBtnClick
        end
        object ZeroReadingsBtn: TAdvSmoothToggleButton
          Left = 3
          Top = 385
          Width = 254
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          AutoToggle = False
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'Zero Readings'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 2
          OnClick = ZeroReadingsBtnClick
        end
        object StreamChkBtn: TAdvSmoothToggleButton
          Left = 3
          Top = 335
          Width = 254
          Height = 44
          AllowAllUp = True
          BorderColor = clBlue
          BorderInnerColor = clBlue
          Appearance.Font.Charset = DEFAULT_CHARSET
          Appearance.Font.Color = clWindowText
          Appearance.Font.Height = -15
          Appearance.Font.Name = 'Tahoma'
          Appearance.Font.Style = []
          Caption = 'Start Stream Check'
          Version = '1.4.3.0'
          Status.Caption = '0'
          Status.Appearance.Fill.Color = clRed
          Status.Appearance.Fill.ColorMirror = clNone
          Status.Appearance.Fill.ColorMirrorTo = clNone
          Status.Appearance.Fill.GradientType = gtSolid
          Status.Appearance.Fill.GradientMirrorType = gtSolid
          Status.Appearance.Fill.BorderColor = clGray
          Status.Appearance.Fill.Rounding = 0
          Status.Appearance.Fill.ShadowOffset = 0
          Status.Appearance.Fill.Glow = gmNone
          Status.Appearance.Font.Charset = DEFAULT_CHARSET
          Status.Appearance.Font.Color = clWhite
          Status.Appearance.Font.Height = -11
          Status.Appearance.Font.Name = 'Tahoma'
          Status.Appearance.Font.Style = []
          ParentFont = False
          Anchors = [akLeft, akRight, akBottom]
          TabOrder = 1
          OnClick = StreamChkBtnClick
        end
      end
      object AboutSheet: TTabSheet
        Caption = 'About TesTORK'
        ImageIndex = 3
        object TescoAboutImage: TImage
          Left = 15
          Top = 9
          Width = 82
          Height = 72
          AutoSize = True
        end
        object Label21: TLabel
          Left = 15
          Top = 106
          Width = 161
          Height = 16
          Caption = 'TESCO TesTORK Wireless '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object WTTTSVerLabel: TLabel
          Left = 15
          Top = 141
          Width = 35
          Height = 13
          Caption = 'Version'
        end
        object Label22: TLabel
          Left = 15
          Top = 186
          Width = 93
          Height = 13
          Caption = 'TESCO Corporation'
        end
        object Label23: TLabel
          Left = 16
          Top = 207
          Width = 111
          Height = 13
          Caption = '5616 - 80th Avenue SE'
        end
        object Label24: TLabel
          Left = 16
          Top = 228
          Width = 97
          Height = 13
          Caption = 'Calgary, AB Canada'
        end
        object Label25: TLabel
          Left = 16
          Top = 249
          Width = 41
          Height = 13
          Caption = 'T2C 4N5'
        end
        object Label26: TLabel
          Left = 16
          Top = 270
          Width = 83
          Height = 13
          Caption = '+1 403 723 7902'
        end
        object Label27: TLabel
          Left = 13
          Top = 123
          Width = 183
          Height = 16
          Caption = 'Torque Turn Tension System'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object NavPanelManager: TPanel
      Left = 1
      Top = 1
      Width = 268
      Height = 20
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object PinButton: TPanel
        Left = 33
        Top = 0
        Width = 235
        Height = 20
        Align = alClient
        AutoSize = True
        BevelOuter = bvNone
        Caption = 'Click to Float'
        Padding.Right = 10
        TabOrder = 0
        OnClick = PinButtonClick
        OnDblClick = PinButtonClick
      end
      object ShrinkButton: TPanel
        Left = 0
        Top = 0
        Width = 33
        Height = 20
        Align = alLeft
        Alignment = taLeftJustify
        BevelOuter = bvNone
        Caption = '  '#187
        TabOrder = 1
        OnClick = ShrinkButtonClick
      end
    end
  end
  object PinnedNavSpacerPanel: TPanel
    Left = 978
    Top = 0
    Width = 30
    Height = 710
    Align = alRight
    ParentShowHint = False
    ShowCaption = False
    ShowHint = True
    TabOrder = 2
    Visible = False
  end
  object PollTimer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = PollTimerTimer
    Left = 184
  end
  object SysTimer: TTimer
    Enabled = False
    Interval = 250
    OnTimer = SysTimerTimer
    Left = 144
  end
  object HiddenPopUp: TPopupMenu
    AutoPopup = False
    Left = 193
    Top = 63
    object OpenJobItem: TMenuItem
      Caption = 'Open Job'
      ShortCut = 24655
      OnClick = OpenJobBtnClick
    end
    object FinishJobItem: TMenuItem
      Caption = 'Mark Job as Finished'
      ShortCut = 24646
      OnClick = FinishJobItemClick
    end
    object ReopenJobItem: TMenuItem
      Caption = 'Reopen Finished Job'
      ShortCut = 24658
      OnClick = ReopenJobItemClick
    end
    object RecalculateShoulder1: TMenuItem
      Caption = 'Recalculate Shoulder'
      ShortCut = 24659
      OnClick = RecalculateShoulder1Click
    end
    object ExportCurvePoints1: TMenuItem
      Caption = 'Export Curve Points'
      ShortCut = 24645
      OnClick = ExportCurvePoints1Click
    end
    object HideNegativeTurnsItem: TMenuItem
      AutoCheck = True
      Caption = 'Hide Negative Turns'
      Checked = True
      ShortCut = 24654
      OnClick = HideNegativeTurnsItemClick
    end
    object ClearStatsItem: TMenuItem
      Caption = 'Clear Stats'
      ShortCut = 24643
      OnClick = ClearStatsItemClick
    end
    object SystemSettings1: TMenuItem
      Caption = 'System Settings'
      ShortCut = 24660
      OnClick = SysSettingsBtnClick
    end
    object StartStopToggleItem: TMenuItem
      Caption = 'Start/Stop Toggle'
      ShortCut = 112
      OnClick = StartStopToggleItemClick
    end
    object ShowHardwareStatus1: TMenuItem
      Caption = 'Show Hardware Status'
      ShortCut = 24648
      Visible = False
      OnClick = ShowDiagDataBtnClick
    end
  end
  object SavePtsDlg: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'Comma Separated Files (*.csv)|*.csv|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Points To File'
    Left = 224
  end
  object NavPgPopUp: TPopupMenu
    Left = 672
    Top = 248
    object PinSidebarItem: TMenuItem
      Caption = 'Float Sidebar'
      OnClick = PinButtonClick
    end
    object ExpandShrinkItem: TMenuItem
      Caption = 'Shrink Sidebar'
      OnClick = ShrinkButtonClick
    end
    object AutoHideSidebarItem: TMenuItem
      AutoCheck = True
      Caption = 'Auto-Hide Sidebar'
      Checked = True
      OnClick = AutoHideSidebarItemClick
    end
  end
  object NavPgAnimator: TTimer
    Enabled = False
    Interval = 20
    Left = 744
    Top = 248
  end
  object BatteryLevelImageList: TImageList
    Height = 32
    Width = 48
    Left = 361
    Top = 416
    Bitmap = {
      494C01010400EC007C0130002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000C000000040000000010020000000000000C0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530064646400FAFAFA000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000303030064646400FAFAFA000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000303030064646400FAFAFA000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000303030064646400FAFAFA005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530052525300646464000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000646464000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000646464000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000646464005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530052525300525253000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000030303000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000030303000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000030303005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530052525300525253000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530052525300525253000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530052525300525253000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530052525300525253000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530052525300525253000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530052525300525253000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530052525300525253000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000030303000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000030303000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000030303005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530052525300646464000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000646464000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000646464000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000646464005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300525253005252
      5300000000000000000052525300525253005252530052525300000000000000
      0000525253005252530064646400FAFAFA000000000000000000000000000000
      000000000000000000001A1ABC001A1ABC001A1ABC001A1ABC001A1ABC001A1A
      BC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000303030064646400FAFAFA000000000000000000000000000000
      0000000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000024C4B70024C4B70024C4B70024C4B70024C4B70024C4
      B700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000303030064646400FAFAFA000000000000000000000000000000
      0000000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000017770700177707001777070017770700177707001777
      0700000000000000000000000000000000000000000000000000000000000000
      0000000000000303030064646400FAFAFA005252530052525300525253005252
      5300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300525253005252
      5300525253005252530052525300525253005252530052525300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      28000000C0000000400000000100010000000000000600000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000003F00000000003F00000000
      003F00000000003F00000000003F00000000003F00000000003F00000000003F
      00000000003F00000000003F00000000003F00000000003F00000000003F0000
      0000003F00000000003F00000000003F0FFFFFFFFC3F0FFFFFFFFC3F0FFFFFFF
      FC3F0FFFFFFFFC3F0FFFFFFFFC3F0FFFFFFFFC3F0FFFFFFFFC3F0FFFFFFFFC3F
      0C0C0C0C0C300C0FFFFFFC300C0C0FFFFC300C0C0C0C0C300C0C0C0C0C300C0F
      FFFFFC300C0C0FFFFC300C0C0C0C0C300C0C0C0C0C300C0FFFFFFC300C0C0FFF
      FC300C0C0C0C0C300C0C0C0C0C300C0FFFFFFC300C0C0FFFFC300C0C0C0C0C30
      0C0C0C0C0C300C0FFFFFFC300C0C0FFFFC300C0C0C0C0C300C0C0C0C0C300C0F
      FFFFFC300C0C0FFFFC300C0C0C0C0C300C0C0C0C0C300C0FFFFFFC300C0C0FFF
      FC300C0C0C0C0C300C0C0C0C0C300C0FFFFFFC300C0C0FFFFC300C0C0C0C0C30
      0C0C0C0C0C300C0FFFFFFC300C0C0FFFFC300C0C0C0C0C300C0C0C0C0C300C0F
      FFFFFC300C0C0FFFFC300C0C0C0C0C300C0C0C0C0C300C0FFFFFFC300C0C0FFF
      FC300C0C0C0C0C300C0C0C0C0C300C0FFFFFFC300C0C0FFFFC300C0C0C0C0C30
      0FFFFFFFFC3F0FFFFFFFFC3F0FFFFFFFFC3F0FFFFFFFFC3F0FFFFFFFFC3F0FFF
      FFFFFC3F0FFFFFFFFC3F0FFFFFFFFC3F00000000003F00000000003F00000000
      003F00000000003F00000000003F00000000003F00000000003F00000000003F
      00000000003F00000000003F00000000003F00000000003F00000000003F0000
      0000003F00000000003F00000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
end
