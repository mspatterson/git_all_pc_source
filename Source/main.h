#ifndef mainH
#define mainH

#include <System.Classes.hpp>
#include <System.DateUtils.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <VCLTee.Chart.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.Series.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.Imaging.jpeg.hpp>
#include <Vcl.Graphics.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ToolWin.hpp>
#include <VclTee.TeeGDIPlus.hpp>
#include <AdvSmoothToggleButton.hpp>
#include <AdvSmoothProgressBar.hpp>
#include "JobManager.h"
#include "Messages.h"
#include "RegistryInterface.h"
#include "ConnLVUtils.h"
#include "CommsMgr.h"
#include "ConnectionDlg.h"
#include "TCirculatingDlg.h"
#include "Averaging.h"


class TTesTORKMgrMainForm : public TForm
{
__published:    // IDE-managed Components
    TPanel *InfoPanel;
    TPanel *GraphPanel;
    TChart *TurnsChart;
    TChart *TimeChart;
    TSplitter *VertSplitter;
    TGroupBox *ConnGB;
    TLabel *Label5;
    TLabel *Label6;
    TPanel *NbrPassedEdit;
    TPanel *TotLengthEdit;
    TPanel *SectNbrEdit;
    TPanel *TqMaxEdit;
    TPanel *TqPeakEdit;
    TPanel *TqOptEdit;
    TPanel *TqMinEdit;
    TPanel *ShldrMaxEdit;
    TPanel *ShldrEdit;
    TPanel *ShldrMinEdit;
    TPanel *TqDeltaEdit;
    TPanel *TurnsMaxEdit;
    TPanel *TurnsAtPeakEdit;
    TPanel *TurnsMinEdit;
    TPanel *TurnsAtShldrEdit;
    TPanel *PostShldrEdit;
    TGroupBox *TorqueGB;
    TLabel *Label7;
    TLabel *Label8;
    TGroupBox *TurnsGB;
    TLabel *Label11;
    TLabel *Label9;
    TLabel *Label10;
    TLabel *Label12;
    TLineSeries *TorqueByTurnsSeries;
    TLineSeries *TorqueByTimeSeries;
    TLineSeries *RPMByTimeSeries;
    TLineSeries *RPMByTurnsSeries;
    TTimer *PollTimer;
    TPageControl *NavPgCtrl;
    TTabSheet *ConnsSheet;
    TTabSheet *RptsSheet;
    TButton *NewSectBtn;
    TTabSheet *HWSheet;
    TTabSheet *AboutSheet;
    TStatusBar *StatusBar;
    TSplitter *HorzSplitter;
    TTabSheet *PipeTallySheet;
    TListView *ConnsLV;
    TTimer *SysTimer;
    TLabel *Label15;
    TListView *PipeTallyLV;
    TLabel *Label1;
    TLabel *Label14;
    TLabel *Label16;
    TLabel *Label17;
    TLabel *Label18;
    TLabel *Label19;
    TPopupMenu *HiddenPopUp;
    TMenuItem *OpenJobItem;
    TMenuItem *FinishJobItem;
    TMenuItem *ReopenJobItem;
    TGroupBox *SectNbrGB;
    TLabel *Label20;
    TValueListEditor *CommsStatusView;
    TImage *TescoAboutImage;
    TLabel *Label21;
    TLabel *WTTTSVerLabel;
    TLabel *Label22;
    TLabel *Label23;
    TLabel *Label24;
    TLabel *Label25;
    TLabel *Label26;
    TLabel *Label27;
    TGroupBox *RptGB;
    TLabel *RptTypeLB;
    TComboBox *RptTypeCombo;
    TGroupBox *RptRangeGB;
    TLabel *RptFromLB;
    TLabel *RptToLB;
    TRadioButton *RptRangeRB;
    TRadioButton *RptAllRB;
    TEdit *RptToEdit;
    TEdit *RptFromEdit;
    TGroupBox *ControlGB;
    TButton *EditSectBtn;
    TMenuItem *RecalculateShoulder1;
    TMenuItem *ExportCurvePoints1;
    TSaveDialog *SavePtsDlg;
    TPanel *TorqueBtnPanel;
    TSpeedButton *TqGraphRPMTenBtn;
    TSpeedButton *TqGraphPanBtn;
    TSpeedButton *TqGraphZmOutBtn;
    TSpeedButton *TqGraphZmInBtn;
    TPanel *TimeBtnPanel;
    TSpeedButton *TimeGraphPanBtn;
    TSpeedButton *TimeGraphZmOutBtn;
    TSpeedButton *TimeGraphZmInBtn;
    TMenuItem *HideNegativeTurnsItem;
    TSpeedButton *GraphSettingBtn;
    TGroupBox *InterlockStatusGB;
    TPanel *CDSPanel;
    TLabel *CDSLB;
    TPanel *SlipsPanel;
    TLabel *SlipsLB;
    TPanel *InfoSizeChangePanel;
    TPopupMenu *NavPgPopUp;
    TMenuItem *PinSidebarItem;
    TMenuItem *AutoHideSidebarItem;
    TTimer *NavPgAnimator;
    TMenuItem *ExpandShrinkItem;
    TPanel *NavPanel;
    TPanel *ShrinkButton;
    TPanel *PinButton;
    TPanel *NavPanelManager;
    TLineSeries *TenByTurnsSeries;
    TLineSeries *TenByTimeSeries;
    TGroupBox *PDSPipeGB;
    TPanel *PDSStatePanel;
    TImageList *BatteryLevelImageList;
    TMenuItem *ClearStatsItem;
    TMenuItem *SystemSettings1;
    TMenuItem *StartStopToggleItem;
    TAdvSmoothToggleButton *ShowDiagDataBtn;
    TAdvSmoothToggleButton *TestAlarmBtn;
    TAdvSmoothToggleButton *SysSettingsBtn;
    TAdvSmoothToggleButton *ILockBypassBtn;
    TAdvSmoothToggleButton *ZeroReadingsBtn;
    TAdvSmoothToggleButton *StreamChkBtn;
    TAdvSmoothToggleButton *ConnCtrlBtn;
    TAdvSmoothToggleButton *CircCtrlBtn;
    TAdvSmoothToggleButton *CommentBtn;
    TAdvSmoothToggleButton *ShowConnsBtn;
    TAdvSmoothToggleButton *NewEditSectBtn;
    TAdvSmoothToggleButton *AddPipeInvRecBtn;
    TAdvSmoothToggleButton *InsPipeInvRecBtn;
    TAdvSmoothToggleButton *ModPipeInvRecBtn;
    TAdvSmoothToggleButton *DelPipeInvRecBtn;
    TAdvSmoothToggleButton *RptPreviewBtn;
    TPanel *SysStatusPanel;
    TPaintBox *SysStatusPaintBox;
    TMenuItem *ShowHardwareStatus1;
    TPanel *PinnedNavSpacerPanel;
    TAdvSmoothProgressBar *CommsProgBar;
    TGroupBox *AutoRecStatusGB;
    TPanel *AutoRecStatusPanel;
    void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
    void __fastcall FormDestroy(TObject *Sender);
    void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
    void __fastcall FormShow(TObject *Sender);
    void __fastcall CommentBtnClick(TObject *Sender);
    void __fastcall ShowConnsBtnClick(TObject *Sender);
    void __fastcall NewSectBtnClick(TObject *Sender);
    void __fastcall PollTimerTimer(TObject *Sender);
    void __fastcall TurnsChartAfterDraw(TObject *Sender);
    void __fastcall ConnsLVSelectItem(TObject *Sender, TListItem *Item, bool Selected);
    void __fastcall SysTimerTimer(TObject *Sender);
    void __fastcall FormResize(TObject *Sender);
    void __fastcall HorzSplitterMoved(TObject *Sender);
    void __fastcall VertSplitterMoved(TObject *Sender);
    void __fastcall ChartMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
    void __fastcall ChartMouseMove(TObject *Sender, TShiftState Shift, int X,
          int Y);
    void __fastcall OpenJobBtnClick(TObject *Sender);
    void __fastcall FinishJobItemClick(TObject *Sender);
    void __fastcall ReopenJobItemClick(TObject *Sender);
    void __fastcall SysSettingsBtnClick(TObject *Sender);
    void __fastcall ILockBypassBtnClick(TObject *Sender);
    void __fastcall CommsStatusViewSelectCell(TObject *Sender, int ACol, int ARow,
          bool &CanSelect);
    void __fastcall RptPreviewBtnClick(TObject *Sender);
    void __fastcall RptRangeRBClick(TObject *Sender);
    void __fastcall RptAllRBClick(TObject *Sender);
    void __fastcall RptTypeComboClick(TObject *Sender);
    void __fastcall EditSectBtnClick(TObject *Sender);
    void __fastcall ConnCtrlBtnClick(TObject *Sender);
    void __fastcall CircCtrlBtnClick(TObject *Sender);
    void __fastcall RecalculateShoulder1Click(TObject *Sender);
    void __fastcall ExportCurvePoints1Click(TObject *Sender);
    void __fastcall TqGraphRPMTenBtnClick(TObject *Sender);
    void __fastcall GraphPanBtnClick(TObject *Sender);
    void __fastcall GraphZmOutBtnClick(TObject *Sender);
    void __fastcall GraphZmInBtnClick(TObject *Sender);
    void __fastcall HideNegativeTurnsItemClick(TObject *Sender);
    void __fastcall GraphSettingBtnClick(TObject *Sender);
    void __fastcall ZeroReadingsBtnClick(TObject *Sender);
    void __fastcall InfoSizeChangePanelClick(TObject *Sender);
    void __fastcall AutoHideSidebarItemClick(TObject *Sender);
    void __fastcall PinButtonClick(TObject *Sender);
    void __fastcall ShrinkButtonClick(TObject *Sender);
    void __fastcall StreamChkBtnClick(TObject *Sender);
    void __fastcall ClearStatsItemClick(TObject *Sender);
    void __fastcall StartStopToggleItemClick(TObject *Sender);
    void __fastcall PipeTallyLVSelectItem(TObject *Sender, TListItem *Item, bool Selected);
    void __fastcall AddPipeInvRecBtnClick(TObject *Sender);
    void __fastcall InsPipeInvRecBtnClick(TObject *Sender);
    void __fastcall ModPipeInvRecBtnClick(TObject *Sender);
    void __fastcall DelPipeInvRecBtnClick(TObject *Sender);
    void __fastcall TestAlarmBtnClick(TObject *Sender);
    void __fastcall ShowDiagDataBtnClick(TObject *Sender);
    void __fastcall SysStatusPaintBoxPaint(TObject *Sender);
    void __fastcall NavPanelMouseEnter(TObject *Sender);


private:

    Graphics::TBitmap* m_iconBmp;

    SHUT_DOWN_TYPE   m_lastShutdownType;
    UNITS_OF_MEASURE m_defaultUOM;

    TJob*            m_currJob;

    void OpenJob( UnicodeString suggestedJobName );
    void ReopenJob( bool needPassword );
    void RefreshCaption( void );

    // Section support
    SECTION_INFO m_currSection;

    void DisplaySection( SECTION_INFO* pSection );
    void ValidateNextSection( void );

    // Status helpers
    void RefreshStatus( void );

    // Auto-record helpers
    enum {
        eARS_Disabled,     // Auto-recording disabled in either registry or section
        eARS_Ready,        // Auto-recording enabled and ready to start
        eARS_Recording,    // Auto-recording in progress
        eARS_Manual,       // Auto-recording enabled, but user-initiated connection underway
        eARS_Paused        // Auto-recording paused due to alarm or user activity
    } m_eAutoRecState;

    void UpdateAutoRecordState( void );

    void EnterSimMode( bool bIsSimulating );

    // Minimize / restore event handling
    void __fastcall OnAppMinimize( TObject *Sender );
    void __fastcall OnAppRestore ( TObject *Sender );

    // Sidebar Menu Options
    typedef enum {
       eNPS_Pinned,
       eNPS_Visible,
       eNPS_Hiding,
       eNPS_Hidden,
       eNPS_Showing
    } eNavPanelState;

    eNavPanelState m_eNavPanelState;

    DWORD m_dwNavPanelHideTimer;

    void InitializeSidebar( void );
    void UpdateNavPanelSlider( void );
    void SetNavPanelState( eNavPanelState eNewState );
    void ExpandSidebar( void );
    void ShrinkSidebar( void );

    __property eNavPanelState NavPanelState = { read = m_eNavPanelState, write = SetNavPanelState };
        // The Nav Panel state affects the availability / status of other controls,
        // so use a property to update it.

    // Connection listview management
    TLVConnSorter* m_lvSorter;
    TLVConnSorter* m_tallySorter;

    void RefreshConnectionsList( void );

    void __fastcall DoOnConnsUpdated    ( TObject* sender );
    void __fastcall DoOnPipeTallyUpdated( TObject* sender );

    void EnablePipeTallyBtns( bool forceDisabled );

    // Graph handling
    typedef enum{
        eGraphShowBoth,
        eGraphShowRPM,
        eGraphShowTension,
        eGraphShowNone,
        eGraphButtonStates
    } GRAPH_BUTTON_STATES;

    void ResetGraphs( void );
    void RestoreAxisMaximums( void );
    void EndPanZoomMode( void );
    void ScrollDataIntoView( TChart* aChart, float xAxisSpan, float xAxisIncr );
    void SetupGraphColours( void );

    bool AddPointToCharts( const WTTTS_READING& aReading );

    GRAPH_AXIS_VALUES m_axisMaxVals;
    GRAPH_AXIS_VALUES m_axisIncrVals;

    typedef struct {
        bool  havePoint;      // Structure used to hold the last packet
        float turnValue;      // received from the TesTORK. The contents
        float gyroValue;      // of this structure are displayed in the
        float timeValue;      // 'hint' box on the torque graph
        float torqueValue;
        float rpmValue;
    } LAST_WTTTS_PKT;

    LAST_WTTTS_PKT m_lastWTTTSPkt;

    // Connection process handlers
    void  PrepareForConnection( bool resetLengthEdit = true );
    void  UpdateConnStats( void );
    void  FindShoulder( bool bNagOnNotFound = true );
    float CalcSlope( TLineSeries* pData, int iPt, int iPtRange );
    float CalcAvgSlope( TLineSeries* pData, int iPt, int iPtRange );
    void  SuggestConnectionResult( void );

    // Connection management
    bool __fastcall ConnectionDlgEvent( CONN_DLG_EVENT anEvent );
    void __fastcall ConnectionDlgStatusReq( MAIN_STATUS_DATA& statusData );

    void DisplayConnection( int iConnID );

    // Data logging
    #define MAX_NBR_CONN_PTS  ( 5 /*minutes*/ * 60 /*secs/min*/ * 100 /*samples/sec*/ )
    int           m_nbrConnPts;
    WTTTS_READING m_connPts[MAX_NBR_CONN_PTS];

    bool          m_shldrSet;
    float         m_shldrPt;
    float         m_shldrTq;

    float         m_mostNegPosition;

    float         m_pkTorque;
    float         m_pkTorquePt;

    TMovingAverage* m_rpmAvg;

    typedef enum {
        LT_START_TOOL,
        LT_CONNECTION,
        LT_CIRCULATING,
        NBR_LOGGING_TYPES
    } LOGGING_TYPE;

    bool StartLogging( LOGGING_TYPE newType );

    // Circulation management
    bool __fastcall CircDlgEvent( eCircEvent anEvent );

    // Comms Handlers
    TCommPoller* m_devPoller;

    DWORD        m_lastBattCheckTick;
    time_t       m_lastBattAlarmTime;

    void InitCommStatsGrid( void );
    void UpdateCommStatus( void );

    bool LogCalRecord( const WTTTS_READING& aReading, const String& logFileName );

    // Main state defs, vars, and handlers. Note: could have a MS_BOOTING state,
    // which m_mainState is forced to in the main form constructor. This would
    // help ensure code that executes only on a state change will execute when
    // the system goes to either the settings only or idle state.
    typedef enum {
        MS_BOOTING,         // Application is booting up, state only used until FormShow event occurs
        MS_LOADING,         // Project being loaded
        MS_SETTINGS_ONLY,   // No project open, can only modify system settings
        MS_IDLE,            // Project open, but might be in read-only mode
        MS_STREAM_CHECK,    // Tool in streaming mode, data being displayed but not recorded
        MS_CONNECTING,      // Tool in streaming mode and software recording the data
        MS_RESULT_WAIT,     // Connection complete - waiting on shoulder and connection being saved
        MS_CIRCULATING,     // Tool in low-speed reporting mode, data being displayed but not recorded
        NBR_MAIN_STATES
    } MAIN_STATE;

    MAIN_STATE m_mainState;

    void SetMainState( MAIN_STATE newState );

    __property MAIN_STATE MainState = { read = m_mainState, write = SetMainState };

    // Custom cursors
    typedef enum {
        CC_HANDGRAB = 1,
        CC_HANDFLAT = 2,
        CC_ZOOM     = 3
    } CUSTOM_CURSOR;

    // Visual support
    void SetSmoothBtnProps( void );
    void SetSmoothBtnProps( TAdvSmoothToggleButton* pBtn );

    // Status support
    struct {
        int iBattLevelImageIndex;
    } m_commsStatus;

    void SetStatusProgress( const String& sText, const String& sHint, TColor cBar, bool bScanning, int iPct );

    // Reports
    void InitRptSelectionCombo( void );

    // New / Edit Section button swapper
    void SetSectionButton( TButton* pButtonStyle );

    /* test */
    void __fastcall AppModalStart( TObject* Sender );
    void __fastcall AppModalEnd( TObject* Sender );
    /* end test */

protected:
    void __fastcall WMShowConn          ( TMessage &Message );
    void __fastcall WMRemoveConn        ( TMessage &Message );
    void __fastcall WMCompleteJob       ( TMessage &Message );
    void __fastcall WMReopenJob         ( TMessage &Message );
    void __fastcall WMCopyConnToClip    ( TMessage &Message );
    void __fastcall WMShowAlarmEvent    ( TMessage &Message );
    void __fastcall WMAlarmAck          ( TMessage &Message );
    void __fastcall WMShowSysSettingsDlg( TMessage &Message );
    void __fastcall WMPollerEvent       ( TMessage &Message );
    void __fastcall WMCommsDebugMsg     ( TMessage &Message );
    void __fastcall WMShowHideNavPanel  ( TMessage &Message );


    BEGIN_MESSAGE_MAP
      MESSAGE_HANDLER( WM_SHOW_CONN,             TMessage, WMShowConn       )
      MESSAGE_HANDLER( WM_REMOVE_CONN,           TMessage, WMRemoveConn     )
      MESSAGE_HANDLER( WM_COMPLETE_JOB,          TMessage, WMCompleteJob    )
      MESSAGE_HANDLER( WM_REOPEN_JOB,            TMessage, WMReopenJob      )
      MESSAGE_HANDLER( WM_COPY_CONN_TO_CLIP,     TMessage, WMCopyConnToClip )
      MESSAGE_HANDLER( WM_SHOW_ALARM_EVENT,      TMessage, WMShowAlarmEvent     )
      MESSAGE_HANDLER( WM_SHOW_ALARM_ACK,        TMessage, WMAlarmAck           )
      MESSAGE_HANDLER( WM_SHOW_SYS_SETTINGS_DLG, TMessage, WMShowSysSettingsDlg )
      MESSAGE_HANDLER( WM_POLLER_EVENT,          TMessage, WMPollerEvent        )
      MESSAGE_HANDLER( WM_COMMS_DEBUG_EVENT,     TMessage, WMCommsDebugMsg      )
      MESSAGE_HANDLER( WM_SHOW_HIDE_NAV_PANEL,   TMessage, WMShowHideNavPanel   )

    END_MESSAGE_MAP(TForm)

public:
    __fastcall TTesTORKMgrMainForm(TComponent* Owner);

};

extern PACKAGE TTesTORKMgrMainForm *TesTORKMgrMainForm;

#endif
