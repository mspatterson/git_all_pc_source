//
// Custom Messages passed by classes to the main form
//

#ifndef Messages_h
#define Messages_h

    #define WM_SHOW_CONN             ( WM_APP + 1 )
         // WParam: Connection creation number
         // LParam: RFU

    #define WM_REMOVE_CONN           ( WM_APP + 2 )
         // WParam: Connection number
         // LParam: RFU

    #define WM_COMPLETE_JOB          ( WM_APP + 3 )
         // WParam: RFU
         // LParam: RFU

    #define WM_REOPEN_JOB            ( WM_APP + 4 )
         // WParam: RFU
         // LParam: RFU

    #define WM_COPY_CONN_TO_CLIP     ( WM_APP + 5 )
         // WParam: Connection creation number
         // LParam: RFU

    #define WM_COPY_GRAPH_TO_CLIP    ( WM_APP + 6 )
         // WParam: Job Summary graph index
         // LParam: RFU

    #define WM_SHOW_SYS_SETTINGS_DLG ( WM_APP + 7 )
         // WParam: RFU
         // LParam: RFU

    #define WM_SHOW_ALARM_EVENT       ( WM_APP + 8 )
         // WParam: Alarm type enum
         // LParam: RFU

    #define WM_POLLER_EVENT          ( WM_APP + 9 )
         // WParam: Indicates the event type
         // LParam: Additional info depending on event type

    #define WM_COMMS_DEBUG_EVENT     ( WM_APP + 10 )
         // WParam: RFU
         // LParam: RFU

    #define WM_SHOW_HIDE_NAV_PANEL   ( WM_APP + 11 )
         // WParam: 1 = show Nav Panel, 0 = hide Nav Panel
         // LParam: RFU

    #define WM_SHOW_ALARM_ACK        ( WM_APP + 12 )
         // WParam: Alarm type enum
         // LParam: RFU

#endif

 