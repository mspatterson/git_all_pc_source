#include <vcl.h>
#pragma hdrstop

#include "JobExportMain.h"
#include "JobManager.h"

#pragma package(smart_init)
#pragma resource "*.dfm"


TJobExportMainForm *JobExportMainForm;


typedef enum {
    eCGC_RecNbr,
    eCGC_CreationNbr,
    eCGC_ConnNbr,
    eCGC_SeqNbr,
    eCGC_SectionNbr,
    eCGC_CalRecNbr,
    eCGC_BattVolts,
    eCGC_Length,
    eCGC_ShoulderSet,
    eCGC_ShoulderPt,
    eCGC_ShoulderTq,
    eCGC_PeakTq,
    eCGC_PeakTqPt,
    eCGC_Result,
    eCGC_NbrReadings,
    eCGC_NbrCols
} ConnGridColumn;

__fastcall TJobExportMainForm::TJobExportMainForm(TComponent* Owner) : TForm(Owner)
{
    // Setup the connections grid
    ConnGrid->ColCount = eCGC_NbrCols;

    ConnGrid->Cells[eCGC_RecNbr][0]      = "Rec Nbr";
    ConnGrid->Cells[eCGC_CreationNbr][0] = "Creation Nbr";
    ConnGrid->Cells[eCGC_ConnNbr][0]     = "Conn Nbr";
    ConnGrid->Cells[eCGC_SeqNbr][0]      = "Seq Nbr";
    ConnGrid->Cells[eCGC_SectionNbr][0]  = "Sect Nbr";
    ConnGrid->Cells[eCGC_CalRecNbr][0]   = "Cal Rec Nbr";
    ConnGrid->Cells[eCGC_BattVolts][0]   = "Batt V";
    ConnGrid->Cells[eCGC_Length][0]      = "Length";
    ConnGrid->Cells[eCGC_ShoulderSet][0] = "Shoulder";
    ConnGrid->Cells[eCGC_ShoulderPt][0]  = "Sh Pt";
    ConnGrid->Cells[eCGC_ShoulderTq][0]  = "Sh Tq";
    ConnGrid->Cells[eCGC_PeakTq][0]      = "Peak Tq";
    ConnGrid->Cells[eCGC_PeakTqPt][0]    = "Peak Tq Pt";
    ConnGrid->Cells[eCGC_Result][0]      = "Result";
    ConnGrid->Cells[eCGC_NbrReadings][0] = "Nbr Readings";
}


void __fastcall TJobExportMainForm::OpenJobBtnClick(TObject *Sender)
{
    if( OpenDlg->Execute() )
    {
        JobNameEdit->Text = OpenDlg->FileName;

        // Clear the connection grid first
        ConnGrid->RowCount = 2;
        ConnGrid->Rows[1]->Clear();

        // Now get the job info
        JOB_INFO jobInfo;

        if( GetJobInfo( JobNameEdit->Text, jobInfo ) )
        {
            ClientNameEdit->Text = jobInfo.clientName;
            ClientLocnEdit->Text = jobInfo.location;
            JobDateEdit->Text    = jobInfo.jobStartTime.DateString();

            // Try to load the job to get the connection count
            TJob* pJob = NULL;

            try
            {
                pJob = new TJob( JobNameEdit->Text );

                if( !pJob->Loaded )
                {
                    MessageDlg( "An error occurred while loading the job.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    Abort();
                }

                NbrRecsEdit->Text = IntToStr( pJob->NbrConnRecs );

                if( pJob->NbrConnRecs == 0 )
                {
                    MessageDlg( "There are no connections recorded in this job.", mtError, TMsgDlgButtons() << mbOK, 0 );
                    Abort();
                }

                ConnGrid->RowCount = ConnGrid->FixedRows + pJob->NbrConnRecs;

                for( int iConn = 0; iConn < pJob->NbrConnRecs; iConn++ )
                {
                    int iRow = ConnGrid->FixedRows + iConn;

                    ConnGrid->Rows[iRow]->Clear();

                    CONNECTION_INFO connInfo;

                    if( pJob->GetConnection( iConn, connInfo ) )
                    {
                        int nbrPts = pJob->GetConnectionPts( connInfo.creationNbr, NULL, 0 );

                        ConnGrid->Cells[eCGC_RecNbr][iRow]      = IntToStr( iConn + 1 );
                        ConnGrid->Cells[eCGC_CreationNbr][iRow] = IntToStr( connInfo.creationNbr );
                        ConnGrid->Cells[eCGC_ConnNbr][iRow]     = IntToStr( connInfo.connNbr );
                        ConnGrid->Cells[eCGC_SeqNbr][iRow]      = IntToStr( connInfo.seqNbr );
                        ConnGrid->Cells[eCGC_SectionNbr][iRow]  = IntToStr( connInfo.sectionNbr );
                        ConnGrid->Cells[eCGC_CalRecNbr][iRow]   = IntToStr( connInfo.calRecNbr );
                        ConnGrid->Cells[eCGC_BattVolts][iRow]   = FloatToStr( connInfo.battVolts );
                        ConnGrid->Cells[eCGC_Length][iRow]      = FloatToStr( connInfo.length );
                        ConnGrid->Cells[eCGC_ShoulderSet][iRow] = connInfo.shoulderSet ? String( "Yes" ) : String( "No" );
                        ConnGrid->Cells[eCGC_ShoulderPt][iRow]  = FloatToStr( connInfo.shoulderPt );
                        ConnGrid->Cells[eCGC_ShoulderTq][iRow]  = FloatToStr( connInfo.shoulderTq );
                        ConnGrid->Cells[eCGC_PeakTq][iRow]      = FloatToStr( connInfo.pkTorque );
                        ConnGrid->Cells[eCGC_PeakTqPt][iRow]    = FloatToStr( connInfo.pkTorquePt );
                        ConnGrid->Cells[eCGC_Result][iRow]      = ConnResultText[ connInfo.crResult ];
                        ConnGrid->Cells[eCGC_NbrReadings][iRow] = IntToStr( nbrPts );
                    }
                }

                ExportDataBtn->Enabled = true;
                ExportListBtn->Enabled = true;
            }
            catch( ... )
            {
                NbrRecsEdit->Text = "";

                ExportDataBtn->Enabled = false;
                ExportListBtn->Enabled = false;
            }

            if( pJob != NULL )
                delete pJob;
        }
    }
}


void __fastcall TJobExportMainForm::ExportDataBtnClick(TObject *Sender)
{
    // Button only enabled if we have a job selected
    if( !SaveDlg->Execute() )
        return;

    // Load the job
    TJob* pJob = new TJob( JobNameEdit->Text );

    if( !pJob->Loaded )
    {
        MessageDlg( "An error occurred while loading the job.", mtError, TMsgDlgButtons() << mbOK, 0 );

        delete pJob;
        return;
    }

    int iNbrConnRecs = pJob->NbrConnRecs;

    if( iNbrConnRecs == 0 )
    {
        MessageDlg( "There are no connections recorded in this job.", mtError, TMsgDlgButtons() << mbOK, 0 );

        delete pJob;
        return;
    }

    // Get the record number - it will be one-based
    int recNbr = RecNbrEdit->Text.ToIntDef( 0 );

    if( ( recNbr <= 0 ) || ( recNbr > iNbrConnRecs ) )
    {
        MessageDlg( "You have entered an invalid record number.", mtError, TMsgDlgButtons() << mbOK, 0 );

        delete pJob;
        return;
    }

    TFileStream* pOutStream = NULL;

    bool bFileCreated = false;

    try
    {
        // Everything looks good. Create the export. User will have been warned if
        // the file already exists, so delete any existing version first
        DeleteFile( SaveDlg->FileName );

        pOutStream = new TFileStream( SaveDlg->FileName, fmCreate );

        // Create the header line
        AnsiString sHdr;
        sHdr.printf( "Msecs,Rotation,Torque,Tension\r" );

        pOutStream->Write( sHdr.c_str(), sHdr.Length() );

        CONNECTION_INFO connInfo;

        if( pJob->GetConnection( recNbr - 1, connInfo ) )
        {
            int nbrPts = pJob->GetConnectionPts( connInfo.creationNbr, NULL, 0 );

            if( nbrPts > 0 )
            {
                WTTTS_READING* pReadings = new WTTTS_READING[nbrPts];

                pJob->GetConnectionPts( connInfo.creationNbr, pReadings, nbrPts );

                for( int iPt = 0; iPt < nbrPts; iPt++ )
                {
                    AnsiString sPt;
                    sPt.printf("%u,%i,%f,%f\r", pReadings[iPt].msecs, pReadings[iPt].rotation, pReadings[iPt].torque, pReadings[iPt].tension );

                    pOutStream->Write( sPt.c_str(), sPt.Length() );
                }

                if( pReadings != NULL )
                    delete [] pReadings;
            }
        }

        bFileCreated = true;
    }
    catch( ... )
    {
    }

    if( bFileCreated )
        MessageDlg( "Export was successful.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
    else
        MessageDlg( "The export failed.", mtError, TMsgDlgButtons() << mbOK, 0 );

    if( pOutStream != NULL )
        delete pOutStream;

    delete pJob;
}


void __fastcall TJobExportMainForm::ExportListBtnClick(TObject *Sender)
{
    // Button only enabled if we have a job selected
    if( !SaveDlg->Execute() )
        return;

    // Now load the job
    TJob* pJob = new TJob( JobNameEdit->Text );

    if( !pJob->Loaded )
    {
        MessageDlg( "An error occurred while loading the job.", mtError, TMsgDlgButtons() << mbOK, 0 );

        delete pJob;
        return;
    }

    int iNbrConnRecs = pJob->NbrConnRecs;

    if( iNbrConnRecs == 0 )
    {
        MessageDlg( "There are no connections recorded in this job.", mtError, TMsgDlgButtons() << mbOK, 0 );

        delete pJob;
        return;
    }

    TFileStream* pOutStream = NULL;

    bool bFileCreated = false;

    try
    {
        // Everything looks good. Create the export. User will have been warned if
        // the file already exists, so delete any existing version first
        DeleteFile( SaveDlg->FileName );

        pOutStream = new TFileStream( SaveDlg->FileName, fmCreate );

        // Create the header line
        AnsiString sHdr;
        sHdr.printf( "CreationNbr,ConnNbr,SeqNbr,SectionNbr,CalRecNbr,BattVolts,Length,ShoulderSet,ShoulderPt,ShoulderTq,PeakTq,PeakTqPt,Result,NbrDataPoints\r" );

        pOutStream->Write( sHdr.c_str(), sHdr.Length() );

        for( int iConn = 0; iConn < iNbrConnRecs; iConn++ )
        {
            CONNECTION_INFO connInfo;

            if( pJob->GetConnection( iConn, connInfo ) )
            {
                // Get the number of readings
                int nbrPts = pJob->GetConnectionPts( connInfo.creationNbr, NULL, 0 );

                // Create the output string
                AnsiString sConnResult = ConnResultText[connInfo.crResult];

                AnsiString sOut;
                sOut.printf( "%d,%d,%d,%d,%d,%f,%f,%d,%f,%f,%f,%f,%s,%d\r",
                             connInfo.creationNbr, connInfo.connNbr,    connInfo.seqNbr,
                             connInfo.sectionNbr,  connInfo.calRecNbr,
                             connInfo.battVolts,   connInfo.length,     connInfo.shoulderSet,
                             connInfo.shoulderPt,  connInfo.shoulderTq,
                             connInfo.pkTorque,    connInfo.pkTorquePt,
                             sConnResult.c_str(),  nbrPts );

                pOutStream->Write( sOut.c_str(), sOut.Length() );
            }
        }

        bFileCreated = true;
    }
    catch( ... )
    {
    }

    if( bFileCreated )
        MessageDlg( "Export was successful.", mtInformation, TMsgDlgButtons() << mbOK, 0 );
    else
        MessageDlg( "The export failed.", mtError, TMsgDlgButtons() << mbOK, 0 );

    if( pOutStream != NULL )
        delete pOutStream;

    delete pJob;
}

