object JobExportMainForm: TJobExportMainForm
  Left = 0
  Top = 0
  Caption = 'TesTORK Job Exporter'
  ClientHeight = 467
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    634
    467)
  PixelsPerInch = 96
  TextHeight = 13
  object JobFileGB: TGroupBox
    Left = 8
    Top = 8
    Width = 617
    Height = 57
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Job File '
    TabOrder = 0
    DesignSize = (
      617
      57)
    object Label5: TLabel
      Left = 16
      Top = 27
      Width = 42
      Height = 13
      Caption = 'Filename'
    end
    object JobNameEdit: TEdit
      Left = 76
      Top = 24
      Width = 489
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
    end
    object OpenJobBtn: TButton
      Left = 571
      Top = 22
      Width = 38
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '...'
      TabOrder = 1
      OnClick = OpenJobBtnClick
    end
  end
  object JobPropsGB: TGroupBox
    Left = 8
    Top = 71
    Width = 617
    Height = 327
    Anchors = [akLeft, akTop, akRight]
    Caption = ' Job Properties '
    TabOrder = 1
    DesignSize = (
      617
      327)
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 27
      Height = 13
      Caption = 'Client'
    end
    object Label2: TLabel
      Left = 16
      Top = 55
      Width = 40
      Height = 13
      Caption = 'Location'
    end
    object Label3: TLabel
      Left = 352
      Top = 24
      Width = 43
      Height = 13
      Caption = 'Job Date'
    end
    object Label8: TLabel
      Left = 352
      Top = 55
      Width = 43
      Height = 13
      Caption = 'Nbr Recs'
    end
    object Label7: TLabel
      Left = 16
      Top = 92
      Width = 39
      Height = 15
      Caption = 'Records'
    end
    object ClientNameEdit: TEdit
      Left = 76
      Top = 21
      Width = 255
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 0
    end
    object ClientLocnEdit: TEdit
      Left = 76
      Top = 52
      Width = 255
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 1
    end
    object JobDateEdit: TEdit
      Left = 404
      Top = 21
      Width = 161
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 2
    end
    object NbrRecsEdit: TEdit
      Left = 404
      Top = 52
      Width = 161
      Height = 21
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 3
    end
    object ConnGrid: TStringGrid
      Left = 75
      Top = 86
      Width = 534
      Height = 202
      Anchors = [akLeft, akTop, akRight, akBottom]
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
      TabOrder = 4
    end
    object ExportListBtn: TButton
      Left = 75
      Top = 294
      Width = 161
      Height = 25
      Caption = 'Export Connection List'
      Enabled = False
      TabOrder = 5
      OnClick = ExportListBtnClick
    end
  end
  object ExportConnsGB: TGroupBox
    Left = 8
    Top = 402
    Width = 618
    Height = 57
    Anchors = [akLeft, akTop, akRight, akBottom]
    Caption = ' Export Connection '
    TabOrder = 2
    ExplicitHeight = 92
    object Label6: TLabel
      Left = 256
      Top = 26
      Width = 34
      Height = 13
      Caption = 'Record'
    end
    object ExportDataBtn: TButton
      Left = 76
      Top = 21
      Width = 161
      Height = 25
      Caption = 'Export Connection Data'
      Enabled = False
      TabOrder = 0
      OnClick = ExportDataBtnClick
    end
    object RecNbrEdit: TEdit
      Left = 297
      Top = 23
      Width = 76
      Height = 21
      TabOrder = 1
    end
  end
  object OpenDlg: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = 'TesTORK Job Files (*.wjb)'
        FileMask = '*.wjb'
      end
      item
        DisplayName = 'All Files (*.*)'
        FileMask = '*.*'
      end>
    Options = [fdoPathMustExist, fdoFileMustExist]
    Title = 'Open Job File'
    Left = 600
  end
  object SaveDlg: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV Files (*.csv)|*.csv|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = 'Save Connection Data'
    Left = 592
    Top = 151
  end
end
