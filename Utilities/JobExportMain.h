//---------------------------------------------------------------------------

#ifndef JobExportMainH
#define JobExportMainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.Grids.hpp>
//---------------------------------------------------------------------------
class TJobExportMainForm : public TForm
{
__published:	// IDE-managed Components
    TFileOpenDialog *OpenDlg;
    TGroupBox *JobFileGB;
    TEdit *JobNameEdit;
    TButton *OpenJobBtn;
    TGroupBox *JobPropsGB;
    TGroupBox *ExportConnsGB;
    TSaveDialog *SaveDlg;
    TLabel *Label1;
    TLabel *Label2;
    TLabel *Label3;
    TEdit *ClientNameEdit;
    TLabel *Label5;
    TEdit *ClientLocnEdit;
    TEdit *JobDateEdit;
    TLabel *Label6;
    TLabel *Label8;
    TEdit *NbrRecsEdit;
    TButton *ExportDataBtn;
    TEdit *RecNbrEdit;
    TStringGrid *ConnGrid;
    TLabel *Label7;
    TButton *ExportListBtn;
    void __fastcall OpenJobBtnClick(TObject *Sender);
    void __fastcall ExportDataBtnClick(TObject *Sender);
    void __fastcall ExportListBtnClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
    __fastcall TJobExportMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TJobExportMainForm *JobExportMainForm;
//---------------------------------------------------------------------------
#endif
